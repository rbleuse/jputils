import org.gradle.api.tasks.testing.logging.TestExceptionFormat
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    java
    kotlin("jvm") version "1.4.32"
    `maven-publish`
    signing
}

defaultTasks("clean", "build")

group = "com.gitlab.mvysny.jputils"
version = "2.4-SNAPSHOT"

java {
    sourceCompatibility = JavaVersion.VERSION_1_7
    targetCompatibility = JavaVersion.VERSION_1_7
}

tasks.withType<JavaCompile> {
    options.encoding = "UTF-8"
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.6"  // retain compatibility with Android
    if (System.getProperty("kotlin.jdkHome") != null) {
        kotlinOptions.jdkHome = System.getProperty("kotlin.jdkHome")
    }
}

repositories {
    mavenCentral()
}

dependencies {
    compileOnly("com.google.android:android:4.1.1.4")
    implementation("org.slf4j:slf4j-api:1.7.30")
    implementation("com.github.mvysny.dirutils:dirutils:2.0")
    api(kotlin("stdlib"))
    // Android has android.util.Base64 starting from SDK 8; Java 8 has java.util.Base64 but that's Android since SDK 26.
    // Since I want to run on Android SDK 15 and Java 8, I have to use an external library.
    // The library is 17k which is fine.
    implementation("net.iharder:base64:2.3.9")
    // don't add any further dependencies, to keep Aedict's size at check.

    // tests
    testImplementation("com.github.mvysny.dynatest:dynatest-engine:0.19") {
        // we need to use an older version of junit5 since 5.6.2 is not compatible with java 7
        exclude(group = "org.junit.jupiter")
        exclude(group = "org.junit.platform")
    }
    // we need to use an older version of junit5 since 5.6.2 is not compatible with java 7
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.5.2")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.5.2")
    testImplementation("org.slf4j:slf4j-simple:1.7.30")
    testImplementation(kotlin("test"))
}

// following https://dev.to/kengotoda/deploying-to-ossrh-with-gradle-in-2020-1lhi
java {
    withJavadocJar()
    withSourcesJar()
}

tasks.withType<Javadoc> {
    isFailOnError = false
}

publishing {
    repositories {
        maven {
            setUrl("https://oss.sonatype.org/service/local/staging/deploy/maven2/")
            credentials {
                username = project.properties["ossrhUsername"] as String? ?: "Unknown user"
                password = project.properties["ossrhPassword"] as String? ?: "Unknown user"
            }
        }
    }
    publications {
        create("mavenJava", MavenPublication::class.java).apply {
            groupId = project.group.toString()
            this.artifactId = "jputils"
            version = project.version.toString()
            pom {
                description.set("JPUtils: A set of utility functions for the Japanese language")
                name.set("JPUtils")
                url.set("https://gitlab.com/mvysny/jputils")
                licenses {
                    license {
                        name.set("GNU GPL v3")
                        url.set("https://www.gnu.org/licenses/gpl-3.0.en.html")
                        distribution.set("repo")
                    }
                }
                developers {
                    developer {
                        id.set("mavi")
                        name.set("Martin Vysny")
                        email.set("martin@vysny.me")
                    }
                }
                scm {
                    url.set("https://gitlab.com/mvysny/jputils")
                }
            }
            from(components["java"])
        }
    }
}

signing {
    sign(publishing.publications["mavenJava"])
}

tasks.withType<Test> {
    useJUnitPlatform()
    testLogging {
        // to see the exceptions of failed tests in the CI console.
        exceptionFormat = TestExceptionFormat.FULL
    }
}
