/*
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.util

import org.junit.jupiter.api.Test
import java.text.ParseException
import kotlin.test.assertFailsWith
import kotlin.test.expect

class JapaneseImperialDateTest {
    @Test
    fun basic() {
        expect("1990 (Heisei 2)") { JapaneseImperialDate(1990).toString() }
        expect("1989 (Heisei 1)") { JapaneseImperialDate(1989).toString() }
        expect("1868 (Meiji 1)") { JapaneseImperialDate(1868).toString() }
        expect("1870 (Meiji 3)") { JapaneseImperialDate(1870).toString() }
        expect("1877 (Meiji 10)") { JapaneseImperialDate(1877).toString() }
    }

    @Test
    fun parseJpNumber() {
        expect(1) { "一".parseJpNumber() }
        expect(2) { "二".parseJpNumber() }
        expect(10) { "十".parseJpNumber() }
        expect(11) { "十一".parseJpNumber() }
        expect(21) { "二十一".parseJpNumber() }
    }

    private fun String.parsei() = JapaneseImperialDate.parseJp(this).gregorianYear

    @Test
    fun parse() {
        expect(1990) { "平成 2".parsei() }
        expect(1989) { "平成1".parsei() }
        expect(1868) { "明治1".parsei() }
        expect(1877) { "明治10".parsei() }
        expect(1870) { "明治3".parsei() }
        expect(1868) { "明治一".parsei() }
        expect(1989) { "平成1年".parsei() }
        expect(1869) { "明治二年".parsei() }
        expect(1889) { "明治二十二年".parsei() }
    }

    @Test
    fun parseOutOfRangeYear() {
        assertFailsWith(ParseException::class) { "平成 200".parsei() }
        assertFailsWith(ParseException::class) { "平成20000000000000000000000000000".parsei() }
    }

    @Test
    fun parseFormat() {
        for (i in JapaneseEra.validGregorianYearRange) {
            expect(i) { JapaneseImperialDate(i).imperial.parsei() }
        }
    }
}
