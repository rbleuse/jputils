/*
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.util

import org.junit.jupiter.api.Test
import kotlin.test.expect

class MD5Test {
    @Test
    fun serialization() {
        expect(MD5.EMPTY) { MD5.EMPTY.cloneByWrite() }
        expect(MD5.EMPTY) { MD5.EMPTY.cloneByBoxing() }
        val md5 = MD5(byteArrayOf(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15))
        expect(md5) { md5.cloneByWrite() }
        expect(md5) { md5.cloneByBoxing() }
    }

    @Test
    fun testMd5() {
        expect("693e9af84d3dfcc71e640e005bdc5e2e") { "000000".hexStringToByteArray().md5.toHexString() }
        expect("7036045e94363a99a97680e80131d312") { "0025deadbeef".hexStringToByteArray().md5.toHexString() }
    }

    @Test
    fun toHexString() {
        expect("0025deadbeef") { byteArrayOf(0, 0x25, 0xde.toByte(), 0xad.toByte(), 0xbe.toByte(), 0xef.toByte()).toHex("") }
        expect("000000") { byteArrayOf(0, 0, 0).toHex("") }
        expect("00:00:00:00") { ByteArray(4) { 0 } .toHex() }
        expect("f0:f1:f2:f3") { ByteArray(4) { (it + 0xF0).toByte() } .toHex() }
    }

    @Test
    fun fromHexString() {
        expect("0025deadbeef") { "0025deadbeef".hexStringToByteArray().toHex("") }
        expect("000000") { "000000".hexStringToByteArray().toHex("") }
    }
}