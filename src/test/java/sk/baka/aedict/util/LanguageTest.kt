/*
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.util

import com.github.mvysny.dynatest.DynaTest
import org.junit.jupiter.api.Test
import kotlin.test.expect

class LanguageTest : DynaTest({
    test("simple2To3Conversion") {
        expect("eng") { Language("en").lang3 }
    }

    test("equals") {
        expect(Language("eng")) { Language("en") }
    }

    test("BT") {
        expect("arm") { Language("hye").ISO639BCode }
        expect("hye") { Language("hye").lang3 }
        expect("arm") { Language("arm").ISO639BCode }
        expect("hye") { Language("arm").lang3 }
    }
})
