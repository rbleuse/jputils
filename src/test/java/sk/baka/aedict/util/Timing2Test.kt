/*
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.util

import org.junit.jupiter.api.Test
import kotlin.test.expect

/**
 * @author mavi
 */
class Timing2Test {
    @Test
    fun testSimpleMark() {
        (0..1000).forEach {
            val t = Timing2()
            t.mark("foo")
            t.finish()
        }
        Thread.sleep(1000)
        val t = Timing2().mark("foo").finish()
        expect(true, t) { t.matches("\\dms \\(foo: \\dms\\)".toRegex()) }
    }
}
