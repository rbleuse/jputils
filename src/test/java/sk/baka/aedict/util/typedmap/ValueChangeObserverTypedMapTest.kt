/*
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.util.typedmap

import org.junit.jupiter.api.Test
import kotlin.test.expect
import kotlin.test.fail

/**
 * @author mvy
 */
class ValueChangeObserverTypedMapTest {

    private val keysChanged = mutableListOf<String>()
    private val valuesChanged = mutableListOf<Any?>()

    private val listener: ValueChangeObserverTypedMap.ValueChangedListener = ValueChangeObserverTypedMap.ValueChangedListener { key, value ->
        keysChanged.add(key)
        val bval = box.get(key).orNull()
        if (value != bval) {
            fail("The value has been changed to $value but Box provides $bval")
        }
        valuesChanged.add(value)
    }

    private val box = ValueChangeObserverTypedMap(Box(), listener)

    /**
     * This succeeds.
     */
    @Test
    fun multiplePutSucceeds() {
        box.putByte("f1", 25.toByte())
                .putByte("f1", 25.toByte())
        expect(keysChanged) { arrayOf("f1", "f1").toList() }
        expect(valuesChanged) { arrayOf<Any?>(25.toByte(), 25.toByte()).toList() }
    }

    @Test
    fun multiplePutSucceeds2() {
        box.putByte("f1", 25.toByte())
                .putString("f1", "25")
        expect(keysChanged) { arrayOf("f1", "f1").toList() }
        expect(valuesChanged) { arrayOf<Any?>(25.toByte(), "25").toList() }
    }
}
