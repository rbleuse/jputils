/*
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.util.typedmap

import org.junit.jupiter.api.Test
import sk.baka.aedict.dict.DictCode
import sk.baka.aedict.util.BitSet
import kotlin.test.assertEquals
import kotlin.test.assertTrue
import kotlin.test.expect

/**
 * @author mvy
 */
class BoxDelegateTest {
    @Test
    fun testPropertyRead() {
        val box = Box().putByte("f1", 25.toByte())
                .putInt("f2", 255)
                .putShort("haha", 22.toShort())
                .putString("bla", "foobar")
                .putBoolean("asdasd", true)
                .putByteArray("blaaaa", ByteArray(16))
                .putFloat("fufu", 1.25f)
                .putEnum("enum2", DictCode.ek)
                .putLong("long", 122132L)
                .putBitSet("bitset", BitSet.valueOf(byteArrayOf(1, 2)))
        val d = DelegateObj(box)
        expect(25.toByte()) { d.f1 }
        expect(255) { d.f2 }
        expect(22.toShort()) { d.haha }
        expect("foobar") { d.bla }
        expect(true) { d.asdasd }
        expect(ByteArray(16).toList()) { d.blaaaa.toList() }
        expect(1.25f) { d.fufu }
        expect(DictCode.ek) { d.enum2 }
        expect(122132L) { d.long }
        expect(BitSet.valueOf(byteArrayOf(1, 2))) { d.bitset }
    }

    @Test
    fun testPropertyWrite() {
        val d = DelegateObj(Box())
        d.f1 = 25
        d.f2 = 255
        d.haha = 22
        d.bla = "foobar"
        d.asdasd = true
        d.blaaaa = ByteArray(16)
        d.fufu = 1.25f
        d.enum2 = DictCode.ek
        d.long = 122132L
        d.bitset = BitSet.valueOf(byteArrayOf(1, 2))
        val box2 = d.box
        assertEquals(25.toByte(), box2.get("f1").byteValue().get())
        assertEquals(255, box2.get("f2").integer().get())
        assertEquals(22.toShort(), box2.get("haha").shortValue().get())
        assertEquals("foobar", box2.get("bla").string().get())
        assertTrue(box2.get("asdasd").bool().get())
        expect(ByteArray(16).toList()) { box2.get("blaaaa").byteArray().get().toList() }
        assertEquals(122132L, box2.get("long").longValue().get())
        assertEquals(1.25f, box2.get("fufu").floatValue().get())
        assertEquals(BitSet.valueOf(byteArrayOf(1, 2)), box2.get("bitset").bitSet().get())
    }

    @Test
    fun defVal() {
        expect(25) { DelegateObj(Box()).defval }
        expect(2) { DelegateObj(Box().putInt("defval", 2)).defval }
        expect(2) { val obj = DelegateObj(Box())
            obj.defval = 2
            obj.defval
        }
    }
}

class DelegateObj(val box: Box) {
    var f1: Byte by box
    var f2: Int by box
    var haha: Short by box
    var bla: String by box
    var asdasd: Boolean by box
    var enum2: DictCode by box
    var blaaaa: ByteArray by box
    var fufu: Float by box
    var long: Long by box
    var bitset: BitSet by box
    var defval: Int by box.default(25)
}
