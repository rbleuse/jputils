/*
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.util

import org.junit.jupiter.api.Test
import sk.baka.aedict.dict.Kanjidic2Entry
import sk.baka.aedict.dict.Kanjidic2EntryTest
import sk.baka.aedict.util.typedmap.Box
import sk.baka.android.DirUtils
import sk.baka.android.spi.Java7FS
import java.io.File
import kotlin.test.expect

/**
 * @author mvy
 */
class BoxablesTest {

    @Test
    fun writeReadFile() {
        val temp = File.createTempFile("box", ".bin")
        val b = Box().putBoolean("foo", true)
        Boxables.write(b, temp)
        expect(b) { Boxables.read(Box::class.java, temp) }
    }

    @Test
    fun writeReadFileGZipped() {
        val temp = File.createTempFile("box", ".bin")
        val b = Box().putString("ha", "bla")
        Boxables.writeGzipped(b, temp)
        expect(b) { Boxables.readGZipped(Box::class.java, temp) }
    }

    @Test
    fun writeReadByteArray() {
        val b = Box().putFloat("float", 25f)
        expect(b) { Boxables.read(Box::class.java, Boxables.write(b)) }
    }

    @Test
    fun writeReadString() {
        val b = Kanjidic2EntryTest.full
        val str = b.boxToString(true)
        expect(b) { Kanjidic2Entry.unboxFromString(str, true) }
    }

    companion object {
        init {
            // otherwise DirUtils would use AndroidFS (since we have Android.jar on classpath) and the tests would fail.
            DirUtils.set(Java7FS())
        }
    }
}
