/*
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.util

import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.util.concurrent.ConcurrentLinkedQueue
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicBoolean
import kotlin.test.expect
import kotlin.test.fail

/**
 * @author mavi
 */
class LoaderExTest {

    private val handler = object : UIHandler {
        override fun execute(command: Runnable) {
            command.run()
        }

        override fun checkUIThread() {
        }
    }
    private lateinit var executor: ExecutorService
    private lateinit var l: LoaderEx

    private val errors = ConcurrentLinkedQueue<Throwable>()

    @BeforeEach
    fun createLoader() {
        executor = Executors.newSingleThreadExecutor()
        l = LoaderEx(executor, true, handler)
    }

    @AfterEach
    fun close() {
        l.closeQuietly()
        executor.shutdown()
        executor.awaitTermination(1, TimeUnit.DAYS)
        errors.forEach { throw RuntimeException(it) }
    }

    private val items = LoaderEx.LoadableItems<Int, Unit>()
    private fun callback(block: (LoaderEx.LoadableItems<Int, Unit>?)->Unit) = object : LoaderEx.OnResultCallback<Int, Unit> {
        override fun onResult(result: LoaderEx.LoadableItems<Int, Unit>?) {
            try {
                block(result)
            } catch (t: Throwable) {
                errors.add(t)
            }
        }

        override fun onFailure(cause: Throwable) {
            errors.add(cause)
        }
    }

    private fun expectCanceled() = callback { items -> expect(null) { items } }
    private fun <I, T> LoaderEx.load(loader: (canceled: AtomicBoolean)-> LoaderEx.LoadableItems<I, T>, callback: LoaderEx.OnResultCallback<I, T>) {
        load(object : LoaderEx.LoaderImpl<I, T> {
            override fun loadItemsInBackground(canceled: AtomicBoolean): LoaderEx.LoadableItems<I, T> {
                return loader(canceled)
            }

        }, callback)
    }

    @Test
    fun testSimpleExecute() {
        val called = AtomicBoolean()
        l.load({ items }, callback { called.set(true) })
        expectAwait(50, true, "the callback should have been called") { called.get() }
    }

    @Test
    fun testSchedulingAnotherTaskCancelsOldOne() {
        val launched = AtomicBoolean()
        val interrupted = AtomicBoolean()
        l.load({
            launched.set(true)
            try {
                Thread.sleep(1000)
            } catch (ex: InterruptedException) {
                // expected
                interrupted.set(true)
                throw ex
            }
            fail("Should have been interrupted")
        }, expectCanceled())
        expectAwait(50, true, "the task should have been started already") { launched.get() }

        val called = AtomicBoolean()
        l.load({ items }, callback { called.set(true) })
        expectAwait(50, true, "stary task mal byt interruptnuty a zruseny") { interrupted.get() }
        expectAwait(20, true, "stary task mal byt interruptnuty a zruseny, a novy task uz mal zbehnut") { called.get() }
    }


    @Test
    fun closeCancelsTask() {
        val launched = AtomicBoolean()
        val interrupted = AtomicBoolean()
        l.load({
            launched.set(true)
            try {
                Thread.sleep(1000)
            } catch (ex: InterruptedException) {
                // expected
                interrupted.set(true)
                throw ex
            }
            fail("Should have been interrupted")
        }, expectCanceled())
        expectAwait(50, true, "the task must be launched") { launched.get() }
        l.closeQuietly()

        expectAwait(50, true, "the task must be interrupted") { interrupted.get() }
    }
}

fun <T> expectAwait(atMost: Long, expected: T, msg: String, block: ()->T) {
    val start = System.currentTimeMillis()
    while (System.currentTimeMillis() - start <= atMost) {
        val got = block()
        if (expected == got) return
        Thread.sleep(10)
    }
    expect(expected, msg, block)
}
