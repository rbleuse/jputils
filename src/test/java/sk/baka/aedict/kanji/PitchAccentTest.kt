/*
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.kanji

import org.junit.jupiter.api.Test
import kotlin.test.expect

class PitchAccentTest {
    @Test
    fun testParse() {
        expect("カꜛンケ${nasalMarker}ꜜンカ${nasalMarker}ク") { PitchAccent.parse("カンケンカク", "305", "12000").toString() }
        expect("カꜛワꜜ") { PitchAccent.parse("カワ", "", "2").toString() }
        expect("ꜛカꜜレンチュꜛーキュー") { PitchAccent.parse("カレン・チューキュー", "", "2000001111").toString() }
        expect("カꜛレコ${nasalMarker}ꜜエ") { PitchAccent.parse("カレコエ", "3", "120").toString() }
        expect("ꜛカꜜモノꜛマꜜブ${nasalMarker}チ") { PitchAccent.parse("カモノ・マブチ", "6", "2000200").toString() }
        expect("blablabl${nasalMarker}aa${nasalMarker}") { PitchAccent.parse("blablablaa", "8010", "0").toString() }
    }
}
