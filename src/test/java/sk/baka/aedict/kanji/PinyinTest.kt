/*
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.kanji

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import kotlin.test.expect

/**
 * @author mvy
 */
class PinyinTest {
    @Test
    fun simpleParse() {
        expect("tang") { Pinyin.parse("tang").readingWithToneNumber }
        expect("tang1") { Pinyin.parse("tang1").readingWithToneNumber }
        expect("tang1") { Pinyin.parse("tāng").readingWithToneNumber }
        expect("jiou4") { Pinyin.parse("jiòu").readingWithToneNumber }
        expect("jieo4") { Pinyin.parse("jièo").readingWithToneNumber }
    }

    @Test
    fun simpleParse2() {
        expect("tang") { Pinyin.parse("tang").readingWithDiacritics }
        expect("tǎng") { Pinyin.parse("tang3").readingWithDiacritics }
        expect("tang") { Pinyin.parse("tang5").readingWithDiacritics }
        expect("tǎng") { Pinyin.parse("tǎng").readingWithDiacritics }
        expect("jiǎo") { Pinyin.parse("jiao3").readingWithDiacritics }
        expect("jiao") { Pinyin.parse("jiao5").readingWithDiacritics }
        expect("jièo") { Pinyin.parse("jieo4").readingWithDiacritics }
        expect("jiòu") { Pinyin.parse("jiou4").readingWithDiacritics }
    }

    @Test
    fun invalidParse() {
        Assertions.assertThrows(IllegalArgumentException::class.java) { Pinyin.parse("tāng1") }
    }
}
