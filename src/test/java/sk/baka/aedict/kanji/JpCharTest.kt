/*
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.kanji

import org.junit.jupiter.api.Test
import sk.baka.aedict.util.codePoints
import kotlin.test.expect

class JpCharTest {
    @Test
    fun testKatakana() {
        expect(false) { '艦'.isKatakana }
        expect(false) { 'か'.isKatakana }
        expect(true) { 'キ'.isKatakana }
        expect(true) { 'ー'.isKatakana }
        expect(false) { 'っ'.isKatakana }
        expect(true) { 'ッ'.isKatakana }
        expect(false) { 'ゃ'.isKatakana }
        expect(false) { 'ゅ'.isKatakana }
        expect(false) { 'ょ'.isKatakana }
        expect(true) { 'ャ'.isKatakana }
        expect(true) { 'ュ'.isKatakana }
        expect(true) { 'ョ'.isKatakana }
    }

    @Test
    fun testHiragana() {
        expect(false) { '艦'.isHiragana }
        expect(true) { 'か'.isHiragana }
        expect(false) { 'キ'.isHiragana }
        expect(false) { 'ー'.isHiragana }
        expect(true) { 'っ'.isHiragana }
        expect(true) { 'ゃ'.isHiragana }
        expect(true) { 'ゅ'.isHiragana }
        expect(true) { 'ょ'.isHiragana }
        expect(false) { 'ャ'.isHiragana }
        expect(false) { 'ュ'.isHiragana }
        expect(false) { 'ョ'.isHiragana }
        expect(true) { 'づ'.isHiragana }
    }

    @Test
    fun testHalfwidthKatakana() {
        expect(false) { '艦'.isHalfwidthKatakana }
        expect(false) { 'か'.isHalfwidthKatakana }
        expect(false) { 'キ'.isHalfwidthKatakana }
        expect(true) { 'ｶ'.isHalfwidthKatakana }
    }

    @Test
    fun testToHalfwidthKatakana() {
        expect("ﾊﾟﾊﾟ") { "パパ".toHalfwidthKatakana() }
        expect("ｺﾝﾋﾟｭｰﾀｰ") { "コンピューター".toHalfwidthKatakana() }
        expect("JOZOﾊﾟﾊﾟFOO") { "JOZOパパFOO".toHalfwidthKatakana() }
        expect("FOOBARBAZ") { "FOOBARBAZ".toHalfwidthKatakana() }
    }

    @Test
    fun testToFullwidthKatakana() {
        expect("パパ") { "ﾊﾟﾊﾟ".halfwidthToKatakana() }
        expect("コンピューター") { "ｺﾝﾋﾟｭｰﾀｰ".halfwidthToKatakana() }
        expect("JOZOパパFOO") { "JOZOﾊﾟﾊﾟFOO".halfwidthToKatakana() }
        expect("FOOBARBAZ") { "FOOBARBAZ".halfwidthToKatakana() }
    }

    @Test
    fun testKatakanaToHiragana() {
        expect("さぼる") { "サボる".mapKatakanaToHiragana() }
        expect("しゅじん") { "しゅじん".mapKatakanaToHiragana() }
        expect("bla") { "bla".mapKatakanaToHiragana() }
        expect("新世紀えゔぁんげりおん") { "新世紀エヴァンゲリオン".mapKatakanaToHiragana() }
        expect("ゑゔぁんげりをん新劇場版") { "ヱヴァンゲリヲン新劇場版".mapKatakanaToHiragana() }
        expect("っ") { "っ".mapKatakanaToHiragana() }
        expect("っ") { "ッ".mapKatakanaToHiragana() }
    }

    @Test
    fun testHiraganaToKatakana() {
        expect("サボル") { "サボる".mapHiraganaToKatakana() }
        expect("シュジン") { "しゅじん".mapHiraganaToKatakana() }
        expect("bla") { "bla".mapHiraganaToKatakana() }
        expect("新世紀エヴァンゲリオン") { "新世紀エヴァンゲリオン".mapHiraganaToKatakana() }
        expect("ヱヴァンゲリヲン新劇場版") { "ヱヴァンゲリヲン新劇場版".mapHiraganaToKatakana() }
        expect("ッ") { "っ".mapHiraganaToKatakana() }
        expect("サボル") { "さぼる".mapHiraganaToKatakana() }
        expect("シュジン") { "シュジン".mapHiraganaToKatakana() }
        expect("新世紀エヴァンゲリオン") { "新世紀えゔぁんげりおん".mapHiraganaToKatakana() }
        expect("ヱヴァンゲリヲン新劇場版") { "ヱヴァンゲリヲン新劇場版".mapHiraganaToKatakana() }
    }

    @Test
    fun removeKanaKanji() {
        expect("") { "".removeKanaKanji() }
        expect(" \t ") { " \t ".removeKanaKanji() }
        expect("") { "サボる".removeKanaKanji() }
        expect("") { "新世紀エヴァンゲリオン".removeKanaKanji() }
        expect("abc") { "a新世紀エヴbァンゲリオンc".removeKanaKanji() }
        expect("abc") { "abc".removeKanaKanji() }
        expect("ahoj") { "ahoj".removeKanaKanji() }
        expect("ahj") { "ah司j".removeKanaKanji() }
        expect("") { "司".removeKanaKanji() }
    }

    @Test
    fun replaceKanaKanji() {
        expect("***") { "サボる".replaceKanaKanjiWith('*') }
        expect("aaaaaaaaaaa") { "新世紀エヴァンゲリオン".replaceKanaKanjiWith('a') }
        expect("a?????b??????c") { "a新世紀エヴbァンゲリオンc".replaceKanaKanjiWith('?') }
        expect("abc") { "abc".replaceKanaKanjiWith('8') }
    }

    @Test
    fun testToCodePoints() {
        expect("ahoj".map { it.toInt() }) { "ahoj".codePoints.toList() }
        expect(listOf('a'.toInt(), 'h'.toInt(), '司'.toInt(), 'j'.toInt())) { "ah司j".codePoints.toList() }
        // G-clef with rune value 0x1D11E consists of a UTF-16 surrogate pair: 0xD834 and 0xDD1E
        expect(listOf('a'.toInt(), 'h'.toInt(), 0x1D11E, 'j'.toInt())) { "ah\uD834\uDD1Ej".codePoints.toList() }
    }

    @Test
    fun testContainsKanji() {
        expect(false) { "１８６７ねんけんぽうほう".containsKanji() }
        expect(true) { "新世紀エヴァンゲリオン".containsKanji() }
        expect(false) { "".containsKanji() }
    }

    @Test
    fun testFullwidthToAscii() {
        expect("""abcd!"[P\]^_`""") { "abｃｄ！＂［Ｐ＼］＾＿｀".fromFullwidth() }
        expect(false) { 'a'.isFullwidth() }
        expect(true) { 'Ａ'.isFullwidth() }
    }

    @Test
    fun testSingleKanji() {
        // fixes https://github.com/mvysny/aedict/issues/565
        expect(false) { "\uD83C\uDF4F".isSingleKanji }
        expect(true) { "艦".isSingleKanji }
        expect(false) { "か".isSingleKanji }
        expect(false) { "k".isSingleKanji }
        expect(false) { "艦艦".isSingleKanji }
        expect(false) { "かか".isSingleKanji }
        expect(false) { "ka".isSingleKanji }
        expect(false) { "".isSingleKanji }
    }

    @Test
    fun testKanji() {
        expect(true) { '艦'.isKanji }
        expect(false) { 'ハ'.isKanji }  // this is katakana HA, not the kanji for "8"!!!
        expect(true) { 'ハ'.isKatakana }  // this is katakana HA, not the kanji for "8"!!!
        expect(false) { 'か'.isKanji }
        expect(false) { 'キ'.isKanji }
        expect(false) { 'ー'.isKanji }
        expect(false) { 'っ'.isKanji }
        expect(false) { 'ゃ'.isKanji }
        expect(false) { 'ゅ'.isKanji }
        expect(false) { 'ょ'.isKanji }
        expect(false) { 'ャ'.isKanji }
        expect(false) { 'ュ'.isKanji }
        expect(false) { 'ョ'.isKanji }
        expect(true) { '付'.isKanji }
        expect(false) { '～'.isKanji }
        expect(false) { '０'.isKanji }
        expect(false) { '｜'.isKanji }
        expect(true) { '丨'.isKanji }
        expect(true) { '々'.isKanji }
        expect(false) { 'ゝ'.isKanji } // repetition mark of hiragana
        expect(false) { 0x1f34f.isKanji }
        expect(false) { '％'.isKanji }
        // https://github.com/mvysny/aedict/issues/774
        expect(false) { '안'.isKanji }
        expect(false) { '녕'.isKanji }
        // https@ //github.com/mvysny/aedict/issues/791
        expect(false) { "🇺".isSingleKanji }
        expect(false) { 'Φ'.isKanji }

        // full-width crap such as ０  ～ ｟ ｡ and the like
        (0xFF00..0xFFFF).forEach { cp -> expect(false) { cp.isKanji } }
        // hangul. Fixes https://github.com/mvysny/aedict/issues/774
        (0xA960..0xD7fb).forEach { cp -> expect(false) { cp.isKanji } }
        // random flowers and imagery
        // fixes https://github.com/mvysny/aedict/issues/565
        (0x1f300..0x1ffff).forEach { cp -> expect(false) { cp.isKanji } }

        // fixes https://github.com/mvysny/aedict/issues/873
        expect(true) { '〆'.isKanji }
    }

    @Test
    fun testKanji2() {
        // kanjis.txt: all kanjis, taken from kanjidic2.
        // no need to modify nor update, fully sufficient as a test case
        val kanjis = javaClass.getResourceAsStream("/kanjis.txt").reader().readText()
        expect(13108) { kanjis.codePoints.count() }
        kanjis.codePoints.forEach { cp ->
            expect(true, "$cp is not a kanji!") { cp.isKanji }
        }
    }
}
