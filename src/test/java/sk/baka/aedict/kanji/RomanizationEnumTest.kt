/*
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.kanji

import org.junit.jupiter.api.Test
import kotlin.test.expect

/**
 * Tests the [RomanizationEnum] class.
 * @author Martin Vysny
 */
class RomanizationEnumTest {

    /**
     * Test method for [IRomanization.toHiragana].
     */
    @Test
    fun testHepburnToHiragana() {
        val r = RomanizationEnum.Hepburn.r
        expect("はは") { r.toHiragana("haha") }
        expect("よこはま") { r.toHiragana("yokohama") }
        expect("ずっと") { r.toHiragana("zutto") }
        expect("おちゃ") { r.toHiragana("ocha") }
        expect("そんな") { r.toHiragana("sonna") }
        expect("そんんあ") { r.toHiragana("sonxna") }
        expect("んあ") { r.toHiragana("xna") }
        expect("ぁ") { r.toHiragana("xa") }
        expect("つ") { r.toHiragana("tu") }
        expect("つ") { r.toHiragana("tsu") }  // https://code.google.com/p/aedict/issues/detail?id=206
        expect("ち") { r.toHiragana("chi") }  // https://code.google.com/p/aedict/issues/detail?id=206
        expect("ち") { r.toHiragana("ti") }  // https://code.google.com/p/aedict/issues/detail?id=206
        expect("し") { r.toHiragana("shi") }  // https://code.google.com/p/aedict/issues/detail?id=206
        expect("し") { r.toHiragana("si") }  // https://code.google.com/p/aedict/issues/detail?id=206
        expect("じ") { r.toHiragana("ji") }  // https://code.google.com/p/aedict/issues/detail?id=206
        expect("じ") { r.toHiragana("zi") }  // https://code.google.com/p/aedict/issues/detail?id=206
        expect("ゔぉるゔぉ") { r.toHiragana("voruvo") }  // volvo :-D
        expect("づ") { r.toHiragana("du") }
        expect("づ") { r.toHiragana("dzu") }
        expect("っ") { r.toHiragana("xtu") }
        expect("っ") { r.toHiragana("xtsu") }
    }

    @Test
    fun testNihonShikiToHiragana() {
        val r = RomanizationEnum.NihonShiki.r
        expect("はは") { r.toHiragana("haha") }
        expect("よこはま") { r.toHiragana("yokohama") }
        expect("ずっと") { r.toHiragana("zutto") }
        expect("おちゃ") { r.toHiragana("otya") }
        expect("そんな") { r.toHiragana("sonna") }
        expect("そんんあ") { r.toHiragana("sonxna") }
        expect("んあ") { r.toHiragana("xna") }
        expect("つ") { r.toHiragana("tu") }
        expect("つ") { r.toHiragana("tsu") }  // https://code.google.com/p/aedict/issues/detail?id=206
        expect("ち") { r.toHiragana("chi") }  // https://code.google.com/p/aedict/issues/detail?id=206
        expect("ち") { r.toHiragana("ti") }  // https://code.google.com/p/aedict/issues/detail?id=206
        expect("し") { r.toHiragana("shi") }  // https://code.google.com/p/aedict/issues/detail?id=206
        expect("し") { r.toHiragana("si") }  // https://code.google.com/p/aedict/issues/detail?id=206
        expect("じ") { r.toHiragana("ji") }  // https://code.google.com/p/aedict/issues/detail?id=206
        expect("じ") { r.toHiragana("zi") }  // https://code.google.com/p/aedict/issues/detail?id=206
        expect("ゔぉるゔぉ") { r.toHiragana("voruvo") }  // volvo :-D
        expect("づ") { r.toHiragana("du") }
        expect("づ") { r.toHiragana("dzu") }
        expect("っ") { r.toHiragana("xtu") }
        expect("っ") { r.toHiragana("xtsu") }
    }

    @Test
    fun testKunreiShikiToHiragana() {
        val r = RomanizationEnum.KunreiShiki.r
        expect("はは") { r.toHiragana("haha") }
        expect("よこはま") { r.toHiragana("yokohama") }
        expect("ずっと") { r.toHiragana("zutto") }
        expect("おちゃ") { r.toHiragana("otya") }
        expect("そんな") { r.toHiragana("sonna") }
        expect("そんんあ") { r.toHiragana("sonxna") }
        expect("んあ") { r.toHiragana("xna") }
        expect("つ") { r.toHiragana("tu") }
        expect("つ") { r.toHiragana("tsu") }  // https://code.google.com/p/aedict/issues/detail?id=206
        expect("ち") { r.toHiragana("chi") }  // https://code.google.com/p/aedict/issues/detail?id=206
        expect("ち") { r.toHiragana("ti") }  // https://code.google.com/p/aedict/issues/detail?id=206
        expect("し") { r.toHiragana("shi") }  // https://code.google.com/p/aedict/issues/detail?id=206
        expect("し") { r.toHiragana("si") }  // https://code.google.com/p/aedict/issues/detail?id=206
        expect("じ") { r.toHiragana("ji") }  // https://code.google.com/p/aedict/issues/detail?id=206
        expect("じ") { r.toHiragana("zi") }  // https://code.google.com/p/aedict/issues/detail?id=206
        expect("ゔぉるゔぉ") { r.toHiragana("voruvo") }  // volvo :-D
        expect("づ") { r.toHiragana("du") }
        expect("づ") { r.toHiragana("dzu") }
        expect("っ") { r.toHiragana("xtu") }
        expect("っ") { r.toHiragana("xtsu") }
    }

    /**
     * Test method for [sk.baka.aedict.kanji.RomanizationEnum.toKatakana].
     */
    @Test
    fun testHepburnToKatakana() {
        val r = RomanizationEnum.Hepburn.r
        expect("ミニコン") { r.toKatakana("minikon") }
        expect("コンピュータ") { r.toKatakana("konpyuuta") }
        expect("ボッブ") { r.toKatakana("bobbu") }
        expect("ソンナ") { r.toKatakana("sonna") }
        expect("ソンンア") { r.toKatakana("sonxna") }
        expect("ンア") { r.toKatakana("xna") }
        expect("ンァ") { r.toKatakana("nxa") }
        expect("ファイア") { r.toKatakana("faia") }
        expect("ツ") { r.toKatakana("tsu") }  // https://code.google.com/p/aedict/issues/detail?id=206
        expect("ツ") { r.toKatakana("tu") }  // https://code.google.com/p/aedict/issues/detail?id=206
        expect("チ") { r.toKatakana("chi") }  // https://code.google.com/p/aedict/issues/detail?id=206
        expect("チ") { r.toKatakana("ti") }  // https://code.google.com/p/aedict/issues/detail?id=206
        expect("シ") { r.toKatakana("shi") }  // https://code.google.com/p/aedict/issues/detail?id=206
        expect("シ") { r.toKatakana("si") }  // https://code.google.com/p/aedict/issues/detail?id=206
        expect("ジ") { r.toKatakana("ji") }  // https://code.google.com/p/aedict/issues/detail?id=206
        expect("ジ") { r.toKatakana("zi") }  // https://code.google.com/p/aedict/issues/detail?id=206
        expect("ヴォルヴォ") { r.toKatakana("voruvo") }
        expect("ヅ") { r.toKatakana("du") }
        expect("ヅ") { r.toKatakana("dzu") }
        expect("ジェリ") { r.toKatakana("jeri") }
        expect("ッ") { r.toKatakana("xtu") }
        expect("ッ") { r.toKatakana("xtsu") }
    }

    @Test
    fun testNihonShikiToKatakana() {
        val r = RomanizationEnum.NihonShiki.r
        expect("ミニコン") { r.toKatakana("minikon") }
        expect("コンピュータ") { r.toKatakana("konpyuuta") }
        expect("ボッブ") { r.toKatakana("bobbu") }
        expect("ソンナ") { r.toKatakana("sonna") }
        expect("ソンンア") { r.toKatakana("sonxna") }
        expect("ンア") { r.toKatakana("xna") }
        expect("ンァ") { r.toKatakana("nxa") }
        expect("ファイア") { r.toKatakana("faia") }
        expect("ツ") { r.toKatakana("tsu") }  // https://code.google.com/p/aedict/issues/detail?id=206
        expect("ツ") { r.toKatakana("tu") }  // https://code.google.com/p/aedict/issues/detail?id=206
        expect("チ") { r.toKatakana("chi") }  // https://code.google.com/p/aedict/issues/detail?id=206
        expect("チ") { r.toKatakana("ti") }  // https://code.google.com/p/aedict/issues/detail?id=206
        expect("シ") { r.toKatakana("shi") }  // https://code.google.com/p/aedict/issues/detail?id=206
        expect("シ") { r.toKatakana("si") }  // https://code.google.com/p/aedict/issues/detail?id=206
        expect("ジ") { r.toKatakana("ji") }  // https://code.google.com/p/aedict/issues/detail?id=206
        expect("ジ") { r.toKatakana("zi") }  // https://code.google.com/p/aedict/issues/detail?id=206
        expect("ヴォルヴォ") { r.toKatakana("voruvo") }
        expect("ヅ") { r.toKatakana("du") }
        expect("ヅ") { r.toKatakana("dzu") }
        expect("ジェリ") { r.toKatakana("zyeri") }
        expect("ジェリ") { r.toKatakana("jeri") }
        expect("ッ") { r.toKatakana("xtu") }
        expect("ッ") { r.toKatakana("xtsu") }
    }

    @Test
    fun testKunreiShikiToKatakana() {
        val r = RomanizationEnum.KunreiShiki.r
        expect("ミニコン") { r.toKatakana("minikon") }
        expect("コンピュータ") { r.toKatakana("konpyuuta") }
        expect("ボッブ") { r.toKatakana("bobbu") }
        expect("ソンナ") { r.toKatakana("sonna") }
        expect("ソンンア") { r.toKatakana("sonxna") }
        expect("ンア") { r.toKatakana("xna") }
        expect("ンァ") { r.toKatakana("nxxa") }
        expect("ファイア") { r.toKatakana("faia") }
        expect("ァヰヲヂュ") { r.toKatakana("xxaxixoxzyu") }
        expect("ツ") { r.toKatakana("tu") }
        expect("ツ") { r.toKatakana("tsu") }  // https://code.google.com/p/aedict/issues/detail?id=206
        expect("チ") { r.toKatakana("chi") }  // https://code.google.com/p/aedict/issues/detail?id=206
        expect("チ") { r.toKatakana("ti") }  // https://code.google.com/p/aedict/issues/detail?id=206
        expect("シ") { r.toKatakana("shi") }  // https://code.google.com/p/aedict/issues/detail?id=206
        expect("シ") { r.toKatakana("si") }  // https://code.google.com/p/aedict/issues/detail?id=206
        expect("ジ") { r.toKatakana("ji") }  // https://code.google.com/p/aedict/issues/detail?id=206
        expect("ジ") { r.toKatakana("zi") }  // https://code.google.com/p/aedict/issues/detail?id=206
        expect("ヴォルヴォ") { r.toKatakana("voruvo") }
        expect("ヅ") { r.toKatakana("du") }
        expect("ヅ") { r.toKatakana("dzu") }
        expect("ジェリ") { r.toKatakana("zyeri") }
        expect("ジェリ") { r.toKatakana("jeri") }
        expect("ッ") { r.toKatakana("xtu") }
        expect("ッ") { r.toKatakana("xtsu") }
    }

    /**
     * Test method for [sk.baka.aedict.kanji.RomanizationEnum.toRomaji].
     */
    @Test
    fun testHepburnToRomaji() {
        val r = RomanizationEnum.Hepburn.r
        expect("haha") { r.toRomaji("はは") }
        expect("yokohama") { r.toRomaji("よこはま") }
        expect("zutto") { r.toRomaji("ずっと") }
        expect("minikon") { r.toRomaji("ミニコン") }
        expect("konpyuuta") { r.toRomaji("コンピュータ") }
        expect("ocha") { r.toRomaji("おちゃ") }
        expect("bobbu") { r.toRomaji("ボッブ") }
        expect("sonna") { r.toRomaji("そんな") }
        expect("sonna") { r.toRomaji("そんんあ") }
        expect("na") { r.toRomaji("んあ") }
        expect("faia") { r.toRomaji("ファイア") }
        expect("tsu") { r.toRomaji("ツ") }
        expect("chi") { r.toRomaji("チ") }  // https://code.google.com/p/aedict/issues/detail?id=206
        expect("shi") { r.toRomaji("し") }  // https://code.google.com/p/aedict/issues/detail?id=206
        expect("ji") { r.toRomaji("ジ") }  // https://code.google.com/p/aedict/issues/detail?id=206
        expect("voruvo") { r.toRomaji("ヴォルヴォ") }
        expect("zu") { r.toRomaji("ヅ") }
        expect("zu") { r.toRomaji("づ") }
        expect("aっ,") { r.toRomaji("あっ,") }  // words ending with a small tsu are used in spoken language...
        expect("ッ") { r.toRomaji("ッ") }
        expect("っ") { r.toRomaji("っ") }
        expect("xtsu") { r.toRomaji("ッ", true) }
        expect("xtsu") { r.toRomaji("っ", true) }
        expect("jeri") { r.toRomaji("ジェリ") }
        expect("henai") { r.toRomaji("へんあい") }
        expect("hexnai") { r.toRomaji("へんあい", true) }
    }

    @Test
    fun testNihonShikiToRomaji() {
        val r = RomanizationEnum.NihonShiki.r
        expect("haha") { r.toRomaji("はは") }
        expect("yokohama") { r.toRomaji("よこはま") }
        expect("zutto") { r.toRomaji("ずっと") }
        expect("minikon") { r.toRomaji("ミニコン") }
        expect("konpyuuta") { r.toRomaji("コンピュータ") }
        expect("otya") { r.toRomaji("おちゃ") }
        expect("bobbu") { r.toRomaji("ボッブ") }
        expect("sonna") { r.toRomaji("そんな") }
        expect("sonna") { r.toRomaji("そんんあ") }
        expect("na") { r.toRomaji("んあ") }
        expect("tu") { r.toRomaji("ツ") }
        expect("ti") { r.toRomaji("チ") }  // https://code.google.com/p/aedict/issues/detail?id=206
        expect("si") { r.toRomaji("し") }  // https://code.google.com/p/aedict/issues/detail?id=206
        expect("zi") { r.toRomaji("ジ") }  // https://code.google.com/p/aedict/issues/detail?id=206
        expect("voruvo") { r.toRomaji("ヴォルヴォ") }
        expect("du") { r.toRomaji("ヅ") }
        expect("du") { r.toRomaji("づ") }
        expect("ッ") { r.toRomaji("ッ") }
        expect("っ") { r.toRomaji("っ") }
        expect("xtu") { r.toRomaji("ッ", true) }
        expect("xtu") { r.toRomaji("っ", true) }
        expect("zyeri") { r.toRomaji("ジェリ") }
        expect("henai") { r.toRomaji("へんあい") }
        expect("hexnai") { r.toRomaji("へんあい", true) }
    }

    @Test
    fun testKunreiShikiToRomaji() {
        val r = RomanizationEnum.KunreiShiki.r
        expect("haha") { r.toRomaji("はは") }
        expect("yokohama") { r.toRomaji("よこはま") }
        expect("zutto") { r.toRomaji("ずっと") }
        expect("minikon") { r.toRomaji("ミニコン") }
        expect("konpyuuta") { r.toRomaji("コンピュータ") }
        expect("otya") { r.toRomaji("おちゃ") }
        expect("bobbu") { r.toRomaji("ボッブ") }
        expect("sonna") { r.toRomaji("そんな") }
        expect("sonna") { r.toRomaji("そんんあ") }
        expect("na") { r.toRomaji("んあ") }
        expect("zya") { r.toRomaji("ぢゃ") }
        expect("a") { r.toRomaji("ァ") }
        expect("xzya") { r.getWriting("ぢゃ") }
        expect("xxa") { r.getWriting("ァ") }
        expect("tu") { r.toRomaji("ツ") }
        expect("ti") { r.toRomaji("チ") }  // https://code.google.com/p/aedict/issues/detail?id=206
        expect("si") { r.toRomaji("し") }  // https://code.google.com/p/aedict/issues/detail?id=206
        expect("zi") { r.toRomaji("ジ") }  // https://code.google.com/p/aedict/issues/detail?id=206
        expect("voruvo") { r.toRomaji("ヴォルヴォ") }
        expect("zu") { r.toRomaji("ヅ") }
        expect("zu") { r.toRomaji("づ") }
        expect("ッ") { r.toRomaji("ッ") }
        expect("っ") { r.toRomaji("っ") }
        expect("zyeri") { r.toRomaji("ジェリ") }
        expect("henai") { r.toRomaji("へんあい") }
        expect("hexnai") { r.toRomaji("へんあい", true) }
    }

    /**
     * Test for http://code.google.com/p/aedict/issues/detail?id=117
     */
    @Test
    fun testApostropheSupport() {
        expect("ボンヤリ") { RomanizationEnum.Hepburn.r.toKatakana("bonxyari") }
        expect("ボンヤリ") { RomanizationEnum.NihonShiki.r.toKatakana("bonxyari") }
        expect("ぼんやり") { RomanizationEnum.Hepburn.r.toHiragana("bonxyari") }
        expect("ぼんやり") { RomanizationEnum.NihonShiki.r.toHiragana("bonxyari") }
        expect("ボンヤリ") { RomanizationEnum.Hepburn.r.toKatakana("bon'yari") }
        expect("ボンヤリ") { RomanizationEnum.NihonShiki.r.toKatakana("bon'yari") }
        expect("ぼんやり") { RomanizationEnum.Hepburn.r.toHiragana("bon'yari") }
        expect("ぼんやり") { RomanizationEnum.NihonShiki.r.toHiragana("bon'yari") }
    }

    @Test
    fun testKwaGwa() {
        val r = RomanizationEnum.NihonShiki.r
        expect("くゎ") { r.toHiragana("kwa") }
        expect("ぐゎ") { r.toHiragana("gwa") }
        expect("kワ") { r.toKatakana("kwa") }  // katakana has no KWA or GWA
        expect("gワ") { r.toKatakana("gwa") }
    }

    @Test
    fun testHepburnGetFirstSyllabogramAsHiragana() {
        val r = RomanizationEnum.Hepburn.r
        assertTR(2, "は", r.getFirstRomajiSyllabogramAsHiragana("haha"))
        assertTR(2, "よ", r.getFirstRomajiSyllabogramAsHiragana("yokohama"))
        assertTR(2, "ず", r.getFirstRomajiSyllabogramAsHiragana("zutto"))
        assertTR(1, "お", r.getFirstRomajiSyllabogramAsHiragana("ocha"))
        assertTR(3, "ちゃ", r.getFirstRomajiSyllabogramAsHiragana("chao"))
        assertTR(2, "そ", r.getFirstRomajiSyllabogramAsHiragana("sonna"))
        assertTR(2, "そ", r.getFirstRomajiSyllabogramAsHiragana("sonxna"))
        assertTR(2, "ん", r.getFirstRomajiSyllabogramAsHiragana("xna"))
        assertTR(2, "ぁ", r.getFirstRomajiSyllabogramAsHiragana("xa"))
        assertTR(2, "つ", r.getFirstRomajiSyllabogramAsHiragana("tu"))
        assertTR(3, "つ", r.getFirstRomajiSyllabogramAsHiragana("tsu"))  // https://code.google.com/p/aedict/issues/detail?id=206
        assertTR(3, "ち", r.getFirstRomajiSyllabogramAsHiragana("chi"))  // https://code.google.com/p/aedict/issues/detail?id=206
        assertTR(2, "ち", r.getFirstRomajiSyllabogramAsHiragana("ti"))  // https://code.google.com/p/aedict/issues/detail?id=206
        assertTR(3, "し", r.getFirstRomajiSyllabogramAsHiragana("shi"))  // https://code.google.com/p/aedict/issues/detail?id=206
        assertTR(2, "し", r.getFirstRomajiSyllabogramAsHiragana("si"))  // https://code.google.com/p/aedict/issues/detail?id=206
        assertTR(2, "じ", r.getFirstRomajiSyllabogramAsHiragana("ji"))  // https://code.google.com/p/aedict/issues/detail?id=206
        assertTR(2, "じ", r.getFirstRomajiSyllabogramAsHiragana("zi"))  // https://code.google.com/p/aedict/issues/detail?id=206
        assertTR(2, "ゔぉ", r.getFirstRomajiSyllabogramAsHiragana("voruvo"))  // volvo :-D
        assertTR(2, "づ", r.getFirstRomajiSyllabogramAsHiragana("du"))
        assertTR(3, "づ", r.getFirstRomajiSyllabogramAsHiragana("dzu"))
        assertTR(3, "っ", r.getFirstRomajiSyllabogramAsHiragana("xtu"))
        assertTR(4, "っ", r.getFirstRomajiSyllabogramAsHiragana("xtsu"))
        assertTR(4, "っきょ", r.getFirstRomajiSyllabogramAsHiragana("kkyou"))
        expect(null) { r.getFirstRomajiSyllabogramAsHiragana("qaaa") }
        expect(null) { r.getFirstRomajiSyllabogramAsHiragana("つつつ") }
        expect(null) { r.getFirstRomajiSyllabogramAsHiragana(",") }
    }

    private fun assertTR(sourceLen: Int, expected: String, result: IRomanization.TransformResult?) {
        result!!
        expect(expected) { result.result }
        expect(sourceLen.toLong()) { result.originLength.toLong() }
    }

    @Test
    fun testHepburnGetFirstSyllabogramAsKatakana() {
        val r = RomanizationEnum.Hepburn.r
        assertTR(2, "ミ", r.getFirstRomajiSyllabogramAsKatakana("minikon"))
        assertTR(2, "コ", r.getFirstRomajiSyllabogramAsKatakana("konpyuuta"))
        assertTR(1, "ン", r.getFirstRomajiSyllabogramAsKatakana("npyuuta"))
        assertTR(4, "ピュー", r.getFirstRomajiSyllabogramAsKatakana("pyuuta"))
        assertTR(2, "ボ", r.getFirstRomajiSyllabogramAsKatakana("bobbu"))
        assertTR(3, "ッブ", r.getFirstRomajiSyllabogramAsKatakana("bbu"))
        assertTR(2, "ソ", r.getFirstRomajiSyllabogramAsKatakana("sonna"))
        assertTR(2, "ソ", r.getFirstRomajiSyllabogramAsKatakana("sonxna"))
        assertTR(2, "ン", r.getFirstRomajiSyllabogramAsKatakana("xna"))
        assertTR(1, "ン", r.getFirstRomajiSyllabogramAsKatakana("nxa"))
        assertTR(2, "ファ", r.getFirstRomajiSyllabogramAsKatakana("faia"))
        assertTR(3, "ツ", r.getFirstRomajiSyllabogramAsKatakana("tsu"))  // https://code.google.com/p/aedict/issues/detail?id=206
        assertTR(2, "ツ", r.getFirstRomajiSyllabogramAsKatakana("tu"))  // https://code.google.com/p/aedict/issues/detail?id=206
        assertTR(3, "チ", r.getFirstRomajiSyllabogramAsKatakana("chi"))  // https://code.google.com/p/aedict/issues/detail?id=206
        assertTR(2, "チ", r.getFirstRomajiSyllabogramAsKatakana("ti"))  // https://code.google.com/p/aedict/issues/detail?id=206
        assertTR(3, "シ", r.getFirstRomajiSyllabogramAsKatakana("shi"))  // https://code.google.com/p/aedict/issues/detail?id=206
        assertTR(2, "シ", r.getFirstRomajiSyllabogramAsKatakana("si"))  // https://code.google.com/p/aedict/issues/detail?id=206
        assertTR(2, "ジ", r.getFirstRomajiSyllabogramAsKatakana("ji"))  // https://code.google.com/p/aedict/issues/detail?id=206
        assertTR(2, "ジ", r.getFirstRomajiSyllabogramAsKatakana("zi"))  // https://code.google.com/p/aedict/issues/detail?id=206
        assertTR(2, "ヴォ", r.getFirstRomajiSyllabogramAsKatakana("voruvo"))
        assertTR(2, "ヅ", r.getFirstRomajiSyllabogramAsKatakana("du"))
        assertTR(3, "ヅ", r.getFirstRomajiSyllabogramAsKatakana("dzu"))
    }

    @Test
    fun testKunreiShikiGetFirstKanaSyllabogramAsRomaji() {
        assertTR(1, "ha", RomanizationEnum.KunreiShiki.r.getFirstKanaSyllabogramAsRomaji("はは"))
        assertTR(1, "yo", RomanizationEnum.KunreiShiki.r.getFirstKanaSyllabogramAsRomaji("よこはま"))
        assertTR(1, "zu", RomanizationEnum.KunreiShiki.r.getFirstKanaSyllabogramAsRomaji("ずっと"))
        assertTR(2, "tto", RomanizationEnum.KunreiShiki.r.getFirstKanaSyllabogramAsRomaji("っと"))
        assertTR(1, "mi", RomanizationEnum.KunreiShiki.r.getFirstKanaSyllabogramAsRomaji("ミニコン"))
        assertTR(1, "ko", RomanizationEnum.KunreiShiki.r.getFirstKanaSyllabogramAsRomaji("コンピュータ"))
        assertTR(1, "n", RomanizationEnum.KunreiShiki.r.getFirstKanaSyllabogramAsRomaji("ンピュータ"))
        assertTR(3, "pyuu", RomanizationEnum.KunreiShiki.r.getFirstKanaSyllabogramAsRomaji("ピュータ"))
        assertTR(1, "ta", RomanizationEnum.KunreiShiki.r.getFirstKanaSyllabogramAsRomaji("タ"))
        assertTR(1, "o", RomanizationEnum.KunreiShiki.r.getFirstKanaSyllabogramAsRomaji("おちゃ"))
        assertTR(2, "tya", RomanizationEnum.KunreiShiki.r.getFirstKanaSyllabogramAsRomaji("ちゃお"))
        assertTR(1, "bo", RomanizationEnum.KunreiShiki.r.getFirstKanaSyllabogramAsRomaji("ボッブ"))
        assertTR(2, "bbu", RomanizationEnum.KunreiShiki.r.getFirstKanaSyllabogramAsRomaji("ッブボ"))
        assertTR(1, "so", RomanizationEnum.KunreiShiki.r.getFirstKanaSyllabogramAsRomaji("そんな"))
        assertTR(1, "so", RomanizationEnum.KunreiShiki.r.getFirstKanaSyllabogramAsRomaji("そんんあ"))
        assertTR(1, "n", RomanizationEnum.KunreiShiki.r.getFirstKanaSyllabogramAsRomaji("んあ"))
        assertTR(2, "zya", RomanizationEnum.KunreiShiki.r.getFirstKanaSyllabogramAsRomaji("ぢゃ"))
        assertTR(1, "a", RomanizationEnum.KunreiShiki.r.getFirstKanaSyllabogramAsRomaji("ァ"))
        assertTR(2, "zya", RomanizationEnum.KunreiShiki.r.getFirstKanaSyllabogramAsRomaji("ぢゃ"))
        assertTR(1, "a", RomanizationEnum.KunreiShiki.r.getFirstKanaSyllabogramAsRomaji("ァ"))
        assertTR(1, "tu", RomanizationEnum.KunreiShiki.r.getFirstKanaSyllabogramAsRomaji("ツ"))
        assertTR(1, "ti", RomanizationEnum.KunreiShiki.r.getFirstKanaSyllabogramAsRomaji("チ"))  // https://code.google.com/p/aedict/issues/detail?id=206
        assertTR(1, "si", RomanizationEnum.KunreiShiki.r.getFirstKanaSyllabogramAsRomaji("し"))  // https://code.google.com/p/aedict/issues/detail?id=206
        assertTR(1, "zi", RomanizationEnum.KunreiShiki.r.getFirstKanaSyllabogramAsRomaji("ジ"))  // https://code.google.com/p/aedict/issues/detail?id=206
        assertTR(2, "vo", RomanizationEnum.KunreiShiki.r.getFirstKanaSyllabogramAsRomaji("ヴォルヴォ"))
        assertTR(1, "zu", RomanizationEnum.KunreiShiki.r.getFirstKanaSyllabogramAsRomaji("ヅ"))
        assertTR(1, "zu", RomanizationEnum.KunreiShiki.r.getFirstKanaSyllabogramAsRomaji("づ"))
    }

    @Test
    fun testUntranslatableRomaji() {
        expect(true) { RomanizationEnum.KunreiShiki.r.containsUntranslatableRomaji("mku") }
        expect(true) { RomanizationEnum.NihonShiki.r.containsUntranslatableRomaji("mku") }
        expect(true) { RomanizationEnum.Hepburn.r.containsUntranslatableRomaji("mku") }
        expect(false) { RomanizationEnum.KunreiShiki.r.containsUntranslatableRomaji("見mikuそんんあ") }
        expect(false) { RomanizationEnum.NihonShiki.r.containsUntranslatableRomaji("見mikuし") }
        expect(false) { RomanizationEnum.Hepburn.r.containsUntranslatableRomaji("mikuぢゃ見") }
    }

    /**
     * https://gitlab.com/mvysny/jputils/-/issues/4
     */
    @Test
    fun smallTsuPassthrough() {
        // hiragana
        expect("引xtu越sitekuru") { RomanizationEnum.KunreiShiki.r.toRomaji("引っ越してくる", true) }
        expect("引っ越sitekuru") { RomanizationEnum.KunreiShiki.r.toRomaji("引っ越してくる", false) }
        expect("引xtu越sitekuru") { RomanizationEnum.NihonShiki.r.toRomaji("引っ越してくる", true) }
        expect("引っ越sitekuru") { RomanizationEnum.NihonShiki.r.toRomaji("引っ越してくる", false) }
        expect("引xtsu越shitekuru") { RomanizationEnum.Hepburn.r.toRomaji("引っ越してくる", true) }
        expect("引っ越shitekuru") { RomanizationEnum.Hepburn.r.toRomaji("引っ越してくる", false) }

        // katakana
        expect("引xtu越sitekuru") { RomanizationEnum.KunreiShiki.r.toRomaji("引ッ越してくる", true) }
        expect("引ッ越sitekuru") { RomanizationEnum.KunreiShiki.r.toRomaji("引ッ越してくる", false) }
        expect("引xtu越sitekuru") { RomanizationEnum.NihonShiki.r.toRomaji("引ッ越してくる", true) }
        expect("引ッ越sitekuru") { RomanizationEnum.NihonShiki.r.toRomaji("引ッ越してくる", false) }
        expect("引xtsu越shitekuru") { RomanizationEnum.Hepburn.r.toRomaji("引ッ越してくる", true) }
        expect("引ッ越shitekuru") { RomanizationEnum.Hepburn.r.toRomaji("引ッ越してくる", false) }
    }

    /**
     * https://gitlab.com/mvysny/jputils/-/issues/4
     */
    @Test
    fun smallTsuPassthrough2() {
        // hiragana
        expect("引xtu") { RomanizationEnum.KunreiShiki.r.toRomaji("引っ", true) }
        expect("引っ") { RomanizationEnum.KunreiShiki.r.toRomaji("引っ", false) }
        expect("引xtu") { RomanizationEnum.NihonShiki.r.toRomaji("引っ", true) }
        expect("引っ") { RomanizationEnum.NihonShiki.r.toRomaji("引っ", false) }
        expect("引xtsu") { RomanizationEnum.Hepburn.r.toRomaji("引っ", true) }
        expect("引っ") { RomanizationEnum.Hepburn.r.toRomaji("引っ", false) }

        // katakana
        expect("引xtu") { RomanizationEnum.KunreiShiki.r.toRomaji("引ッ", true) }
        expect("引ッ") { RomanizationEnum.KunreiShiki.r.toRomaji("引ッ", false) }
        expect("引xtu") { RomanizationEnum.NihonShiki.r.toRomaji("引ッ", true) }
        expect("引ッ") { RomanizationEnum.NihonShiki.r.toRomaji("引ッ", false) }
        expect("引xtsu") { RomanizationEnum.Hepburn.r.toRomaji("引ッ", true) }
        expect("引ッ") { RomanizationEnum.Hepburn.r.toRomaji("引ッ", false) }
    }
}
