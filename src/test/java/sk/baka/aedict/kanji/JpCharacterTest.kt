/*
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.kanji

import org.junit.jupiter.api.Test
import kotlin.test.expect

class JpCharacterTest {
    @Test
    fun testKanji() {
        expect(true) { '艦'.isJpChar }
        expect(true) { 'ハ'.isJpChar }  // this is katakana HA, not the kanji for "8"!!!
        expect(true) { 'か'.isJpChar }
        expect(true) { 'キ'.isJpChar }
        expect(true) { 'ー'.isJpChar }
        expect(true) { 'っ'.isJpChar }
        expect(false) { 'A'.isJpChar }
        expect(false) { 'Ａ'.isJpChar }
        expect(true) { 'ゃ'.isJpChar }
        expect(true) { 'ゅ'.isJpChar }
        expect(true) { 'ょ'.isJpChar }
        expect(true) { 'ャ'.isJpChar }
        expect(true) { 'ュ'.isJpChar }
        expect(true) { 'ョ'.isJpChar }
        expect(true) { '付'.isJpChar }
        expect(false) { '～'.isJpChar }
        expect(false) { '０'.isJpChar }
        expect(false) { '｜'.isJpChar }
        expect(true) { '丨'.isJpChar }
        expect(true) { '々'.isJpChar }
        expect(true) { 'ヮ'.isJpChar }
        expect(false) { 'ゝ'.isJpChar } // repetition mark of hiragana
        expect(true) { '〆'.isJpChar }  // it's a kanji: https://github.com/mvysny/aedict/issues/873
    }

    @Test
    fun testCodePoints() {
        expect(true) { '艦'.toInt().isJpChar }
        expect(true) { 'ハ'.toInt().isJpChar }  // this is katakana HA, not the kanji for "8"!!!
        expect(true) { 'か'.toInt().isJpChar }
        expect(true) { 'キ'.toInt().isJpChar }
        expect(true) { 'ー'.toInt().isJpChar }
        expect(true) { 'っ'.toInt().isJpChar }
        expect(false) { 'A'.toInt().isJpChar }
        expect(true) { 'ゃ'.toInt().isJpChar }
        expect(true) { 'ゅ'.toInt().isJpChar }
        expect(true) { 'ょ'.toInt().isJpChar }
        expect(true) { 'ャ'.toInt().isJpChar }
        expect(true) { 'ュ'.toInt().isJpChar }
        expect(true) { 'ョ'.toInt().isJpChar }
        expect(true) { '付'.toInt().isJpChar }
        expect(false) { '～'.toInt().isJpChar }
        expect(false) { '０'.toInt().isJpChar }
        expect(false) { '｜'.toInt().isJpChar }   // this is the pipe char, not a kanji
        expect(true) { '丨'.toInt().isJpChar }   // this is a kanji
        expect(true) { '々'.toInt().isJpChar }
        expect(false) { 'ゝ'.toInt().isJpChar } // repetition mark of hiragana
    }

    @Test
    fun testFullWidthToHalfWidth() {
        expect(JpCharacter('ﾀ')) { JpCharacter('タ').toHalfwidthKatakana() }
        expect(JpCharacter('ﾀ')) { JpCharacter('ﾀ').toHalfwidthKatakana() }
        expect(JpCharacter('ょ')) { JpCharacter('ょ').toHalfwidthKatakana() }
        expect(JpCharacter('タ')) { JpCharacter('ﾀ').toFullwidthKatakana() }
        expect(JpCharacter('タ')) { JpCharacter('タ').toFullwidthKatakana() }
        expect(JpCharacter('ょ')) { JpCharacter('ょ').toFullwidthKatakana() }
    }

    @Test
    fun testCodePoint() {
        val ch = JpCharacter('艦')
        expect(0x8266) { ch.codePoint }
        expect("pra") { ch.base36EncodedCodePoint }
    }

    @Test
    fun testKana() {
        expect(true) { 'ヮ'.isKana }
        expect(false) { '〆'.isKana }
    }
}
