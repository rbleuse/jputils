/*
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.inflection

import com.github.mvysny.dynatest.expectList
import org.junit.jupiter.api.Test

/**
 * @author mvy
 */
class AdjectiveInflectionTest {
    /**
     * https://github.com/mvysny/aedict/issues/595
     */
    @Test
    fun inflectSyunnei() {
        expectList("syunneku nai") { "syunnei".inflectAdji(VerbInflection.Form.NEGATIVE) }
    }

    /**
     * https://github.com/mvysny/aedict/issues/611
     */
    @Test
    fun inflectKanji() {
        expectList("幸福 da") { "幸福".inflect(VerbInflection.Form.PLAIN) }
        expectList("幸福 desu") { "幸福".inflect(VerbInflection.Form.POLITE) }
        expectList("幸福 deha nai") { "幸福".inflect(VerbInflection.Form.NEGATIVE) }
        expectList("幸福 deha arimasen") { "幸福".inflect(VerbInflection.Form.POLITE_NEGATIVE) }
        expectList("幸福 datta") { "幸福".inflect(VerbInflection.Form.PAST_TENSE) }
        expectList("幸福 desita") { "幸福".inflect(VerbInflection.Form.POLITE_PAST) }
        expectList("幸福 deha nakatta") { "幸福".inflect(VerbInflection.Form.NEGATIVE_PAST) }
        expectList("幸福 deha arimasen desita") { "幸福".inflect(VerbInflection.Form.POLITE_PAST_NEGATIVE) }
    }

    @Test
    fun inflectIi() {
        expectList("yoku nai") { "ii".inflectAdjXI(VerbInflection.Form.NEGATIVE) }
        expectList("kakkoyoku nai") { "かっこいい".inflectAdjXI(VerbInflection.Form.NEGATIVE) }
    }

    @Test
    fun inflectKawaii() {
        expectList("kawaiku nai") { "kawaii".inflectAdji(VerbInflection.Form.NEGATIVE) }
        expectList("可愛ku nai") { "可愛い".inflectAdji(VerbInflection.Form.NEGATIVE) }
    }

    @Test
    fun inflectNaoii() {
        expectList("naoyoku nai") { "naoii".inflectAdjXI(VerbInflection.Form.NEGATIVE) }
        expectList("nao良ku nai") { "nao良i".inflectAdjXI(VerbInflection.Form.NEGATIVE) }
    }

    @Test
    fun inflectTsumaranai() {
        expectList("詰ranaku nai") { "詰らない".inflectAdji(VerbInflection.Form.NEGATIVE) }
    }

    private fun String.inflect(form: VerbInflection.Form) = AdjectiveInflection.inflect(this, form, AdjectiveType.AdjNa)
    private fun String.inflectAdji(form: VerbInflection.Form) = AdjectiveInflection.inflect(this, form, AdjectiveType.AdjI)
    private fun String.inflectAdjXI(form: VerbInflection.Form) = AdjectiveInflection.inflect(this, form, AdjectiveType.AdjIX)
}
