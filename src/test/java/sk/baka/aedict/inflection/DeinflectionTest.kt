/*
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.inflection

import com.github.mvysny.dynatest.DynaTest
import com.github.mvysny.dynatest.expectList
import sk.baka.aedict.dict.JMDictEntryTest
import sk.baka.aedict.kanji.*
import java.util.*
import kotlin.test.expect

/**
 * Tests the [sk.baka.aedict.inflection.VerbDeinflector] class.
 * @author Martin Vysny
 */
class DeinflectionTest : DynaTest({

    val resolver = object : VerbDeinflector.ReadingResolver {
        override fun resolve(verbKanji: Kanji): JpCharacter? = when (verbKanji.kanji()) {
            "待" -> JpCharacter('ま')
            "見" -> JpCharacter('み')
            "着" -> JpCharacter("き")
            else -> null
        }
    }

    fun VerbDeinflections.expect(vararg expected: String) {
        val got = deinflectedVerbs.sorted()
        expect(expected.sorted(), this.toString()) { got }
    }

    fun Deinflections.expect(vararg expected: String) {
        val got: List<String> = getTerminalDeinflections().sorted()
        expect(expected.sorted(), this.toString()) { got }
    }

    fun expectDeinflected(vararg expected: String, deinflections: ()->List<String>) {
        for (deinflect in deinflections()) {
            var deinflectNihonShiki = deinflect
            if (!deinflectNihonShiki[0].isHiragana) {
                // convert to nihon-shiki
                deinflectNihonShiki = RomanizationEnum.Hepburn.r.toHiragana(deinflectNihonShiki)
                deinflectNihonShiki = RomanizationEnum.NihonShiki.r.toRomaji(deinflectNihonShiki)
            }
            val d: Deinflections = Deinflector(resolver).deinflect(deinflectNihonShiki, IRomanization.NIHON_SHIKI)
            d.expect(*expected)
        }
    }

    fun expectDeinflectedVerb(vararg expected: String, deinflections: ()->List<String>) {
        for (deinflect in deinflections()) {
            var deinflectNihonShiki = deinflect
            if (deinflectNihonShiki[0].isRomaji) {
                // make sure the romaji is in nihon-shiki since that's what the Deinflector wants.
                deinflectNihonShiki = RomanizationEnum.Hepburn.r.toHiragana(deinflectNihonShiki)
                deinflectNihonShiki = RomanizationEnum.NihonShiki.r.toRomaji(deinflectNihonShiki)
            }
            val d: Deinflections = Deinflector(resolver).deinflect(deinflectNihonShiki, IRomanization.NIHON_SHIKI)
            d.verb.expect(*expected)
        }
    }

    fun expectDeinflectedVerbGroup(verbToDeinflect: String, expected: Map<String, EnumSet<InflectableWordClass>>) {
        val d = Deinflector(resolver).deinflect(verbToDeinflect, IRomanization.NIHON_SHIKI)
        expect(expected.keys) { d.getTerminalDeinflections() }
        expected.forEach { expect(it.value) { d.getWordClass(it.key) }}
    }

    test("NonVerb") {
        val d = VerbDeinflector.deinflect("kirei", IRomanization.IDENTITY, null)
        expect(true, "" + d) { d.isEmpty }
        expectList("kirei") { d.deinflectedVerbs.toList() }
        expectList() { d.get("kirei") }
    }

    group("invalid input") {
        test("ttu") {
            var d = VerbDeinflector.deinflect("っつ", IRomanization.IDENTITY, null)
            expect(true, "" + d) { d.isEmpty }
            expectList("ttu") { d.deinflectedVerbs.toList() }
            expectList() { d["ttu"] }

            d = VerbDeinflector.deinflect("ッツ", IRomanization.IDENTITY, null)
            expect(true, "" + d) { d.isEmpty }
            expectList("ttu") { d.deinflectedVerbs.toList() }
            expectList() { d["ttu"] }
        }

        test("tte") {
            var d = VerbDeinflector.deinflect("って", IRomanization.IDENTITY, null)
            expect(true, "" + d) { d.isEmpty }
            expectList("tte") { d.deinflectedVerbs.toList() }
            expectList() { d["tte"] }
            d = VerbDeinflector.deinflect("ッテ", IRomanization.IDENTITY, null)
            expect(true, "" + d) { d.isEmpty }
            expectList("tte") { d.deinflectedVerbs.toList() }
            expectList() { d["tte"] }
        }

        test("smallXtsu") {
            var d = VerbDeinflector.deinflect("っ", IRomanization.IDENTITY, null)
            expect(true, "" + d) { d.isEmpty }
            expectList("っ") { d.deinflectedVerbs.toList() }
            expect(false) { d.isSuccess }
            d = VerbDeinflector.deinflect("ッ", IRomanization.IDENTITY, null)
            expect(true, "" + d) { d.isEmpty }
            expectList("ッ") { d.deinflectedVerbs.toList() }
            expect(false) { d.isSuccess }
        }

        test("isssi") {
            val d = VerbDeinflector.deinflect("isssi", IRomanization.HEPBURN, null)
            expect(true, "" + d) { d.isEmpty }
            expectList("isssi") { d.deinflectedVerbs.toList() }
        }

        test("qka") {
            val d = VerbDeinflector.deinflect("qka", IRomanization.HEPBURN, null)
            expect(true, "" + d) { d.isEmpty }
            expectList("qka") { d.deinflectedVerbs.toList() }
        }
        test("small") {
            var d = VerbDeinflector.deinflect("ゅ", IRomanization.HEPBURN, null)
            expect(true, "" + d) { d.isEmpty }
            expectList("ゅ") { d.deinflectedVerbs.toList() }
            d = VerbDeinflector.deinflect("ゃ", IRomanization.HEPBURN, null)
            expect(true, "" + d) { d.isEmpty }
            expectList("ゃ") { d.deinflectedVerbs.toList() }
            d = VerbDeinflector.deinflect("ょ", IRomanization.HEPBURN, null)
            expect(true, "" + d) { d.isEmpty }
            expectList("ょ") { d.deinflectedVerbs.toList() }

            d = VerbDeinflector.deinflect("ャ", IRomanization.HEPBURN, null)
            expect(true, "" + d) { d.isEmpty }
            expectList("ャ") { d.deinflectedVerbs.toList() }
            d = VerbDeinflector.deinflect("ュ", IRomanization.HEPBURN, null)
            expect(true, "" + d) { d.isEmpty }
            expectList("ュ") { d.deinflectedVerbs.toList() }
            d = VerbDeinflector.deinflect("ョ", IRomanization.HEPBURN, null)
            expect(true, "" + d) { d.isEmpty }
            expectList("ョ") { d.deinflectedVerbs.toList() }
        }
    }

    test("adjective") {
        expectDeinflected("takai") { listOf("takai", "takakunai", "takakatta", "takaku nakatta", "takai desu",
                "takaku nai desu", "takakuarimasen", "takakatta desu", "takakunakattadesu", "takaku arimasen deshita") }
        expectDeinflected("詰ranai", "詰ru") { listOf("詰らなくない", "詰ranaku nai") }
        expectDeinflected("tumaranai", "tumaru") { listOf("つまらなくない", "tumaranaku nai") }

        expect("{tumaranai=[Adjective]}+Base 1 + nai=tumaranaku nai") { Deinflector(resolver).deinflect("tumaranaku nai", IRomanization.NIHON_SHIKI)["tumaranai"].joinToString() }
        expect("{tumaranai=[Adjective]}+Base 1 + nai=tumaranaku nai, {tumaru=[VerbGodan]}+Base 1 + nai=tumaranai") { Deinflector(resolver).deinflect("tumaranaku nai", IRomanization.NIHON_SHIKI)["tumaru"].joinToString() }
    }

    test("adjectiveII") {
        expectDeinflected("ii") { listOf("ii desu", "yoku nai desu", "yoku arimasen", "yokatta desu", "yoku arimasen deshita") }
    }

    test("deinflectDesu") {
        expectDeinflectedVerb("desu") { listOf("だ", "でわない", "ではない", "じゃない", "だった", "ではなかった",
                "じゃなかった", "で", "です", "ではありません", "じゃありません", "でした", "ではありませんでした", "じゃありませんでした") }
    }

    test("deinflectKuru") {
        // moze byt kuru ale aj 着ru/kiru - to wear.
        expectDeinflectedVerb("kiru", "kuru") { listOf("kite", "kita", "kimasu", "kimasen", "kimashita", "kimasen deshita") }
        expectDeinflectedVerb("kuru") { listOf("kuru", "konai", "konakatta") }
        expectDeinflectedVerb("kuru") { listOf("korareru", "korarenai", "korarete") }
        expectDeinflectedVerb("kuru") { listOf("koi", "koyou") }
        expectDeinflectedVerb("来ru") { listOf("来い", "来masu") }
        expectDeinflectedVerb("出来ru") { listOf("出来ます", "出来nai", "出来ta", "出来te") }
        expectDeinflectedVerb("来ru") { listOf("来ru", "来nai", "来ta", "来nakatta", "来te", "来masu", "来masen", "来mashita", "来masen deshita") }
        expectDeinflectedVerb("来ru") { listOf("来rareru", "来rarenai", "来rarete") }
        expectDeinflectedVerb("来ru") { listOf("来i", "来you") }
    }

    // https://github.com/mvysny/aedict/issues/929
    test("deinflectKiru") {
        // kite can be kuru but also 着ru(kiru) - to wear.
        // for inflection chart see https://www.sljfaq.org/cgi/verb-inflect.cgi?verb=kiru the "vowel stem case"
        expectDeinflectedVerb("kiru", "kuru") { listOf("kite") }
        expectDeinflectedVerb("kiru", "kuru") { listOf("kita") }
        expectDeinflectedVerb("kiru", "kuru") { listOf("kimasu", "kimasen") }
        expectDeinflectedVerb("kiru") { listOf("kinai", "kinakatta", "kirareru", "kirarenai") }
        expectDeinflectedVerb("kiru") { listOf("kiro", "kiyou", "kireba") }
    }

    test("deinflectSuru") {
        expectDeinflectedVerb("suru") { listOf("する", "しない", "した", "しなかった", "して", "します", "しません", "しました", "しませんでした") }
        // http://www.rocketlanguages.com/your-community/japanese-grammar/shite-imasu-and-shitte-imasu/
        // :idea: しています(shite imasu)translates as "I am doing" and comes from the verb する(suru)"to do"
        expectDeinflectedVerb("suru") { listOf("しています", "していませんでした", "していません", "していました", "siyou") }
        expectDeinflectedVerb("sareru") { listOf("される", "されない", "された") }
    }

    test("DeinflectZuru") {
        expectDeinflectedVerb("zuru") { listOf("zuru", "zunai", "zuta", "zunakatta", "zute", "zumasu") }
        expectDeinflectedVerb("zuru") { listOf("zuteいます", "zuていませんでした", "zuていません", "zuていました", "zuyou") }
    }

    test("DeinflectIku") {
        expectDeinflectedVerb("iku") { listOf("iku", "ikanai", "itta", "ikanakatta", "itte", "ikimasu", "ikimasen", "ikimashita", "ikimasen deshita") }
        expectDeinflectedVerb("ikareru") { listOf("ikareru", "ikarenai", "ikareta") }
        expectDeinflectedVerb("iku") { listOf("itteimasen", "itteimasu", "itteimasen deshita") }
        expectDeinflectedVerb("iku") { listOf("itte imasen", "itte imasu", "itte imasen deshita") }
        expectDeinflectedVerb("iku") { listOf("ike") }
    }

    test("DeinflectAru") {
        expectDeinflectedVerb("aru") { listOf("ある", "ない", "なかった", "あります", "ありません", "ありました", "ありませんでした") }
        expectDeinflectedVerb("aru", "au") { listOf("あった", "あって") }
    }

    test("DeinflectTaberu") {
        // a bit weird to deinflect taberu to tabu, however we have to be able
        // to also deinflect aeru to au
        expectDeinflectedVerb("taberu", "tabu") { listOf("taberu", "tabenai", "tabenakatta", "tabemasu", "tabemasen", "tabemashita", "tabemasen deshita") }
        expectDeinflectedVerb("taberu") { listOf("tabeta", "tabete") }
        expectDeinflectedVerb("taberu") { listOf("tabeteiru", "tabeteimasu", "tabeteimasen", "tabete imasu", "tabete imasen deshita") }
        expectDeinflectedVerb("taberu") { listOf("taberareru", "taberarenai", "taberareta") }
    }

    // https://github.com/mvysny/aedict/issues/929
    test("deinflect kanji+masu") {
        expectDeinflected("着ru") { listOf("着masu") }
        expectDeinflectedVerb("着ru") { listOf("着ru", "着nai", "着nakatta", "着masu", "着masen", "着mashita", "着masen deshita") }
        expectDeinflectedVerb("着ru") { listOf("着ta", "着te") }
        expectDeinflectedVerb("着ru") { listOf("着teiru", "着teimasu", "着teimasen", "着te imasu", "着te imasen deshita") }
        expectDeinflectedVerb("着ru") { listOf("着rareru", "着rarenai", "着rareta") }
    }

    test("DeinflectKaku") {
        expectDeinflectedVerb("kaku", "kakiru") { listOf("かきます", "かきません", "かきました", "かきませんでした") }
        expectDeinflectedVerb("kaku", "kairu") { listOf("かいた", "かいて") }
        expectDeinflectedVerb("kaku") { listOf("かく", "かかない", "かかなかった") }
        expectDeinflectedVerb("kaku", "kairu") { listOf("kaiteiru", "kaiteimasu", "kaiteimasen", "kaiteimasendeshita") }
        expectDeinflectedVerb("kaku", "kairu") { listOf("kaite iru", "kaite imasu", "kaite imasen", "kaite imasen deshita") }
        expectDeinflectedVerb("kaku") { listOf("かかれる", "かかれない", "かかれた") }
    }

    test("DeinflectIsogu") {
        expectDeinflectedVerb("isogu") { listOf("isoida", "isoide", "isogu", "isoganai", "isoganakatta") }
        expectDeinflectedVerb("isogu", "isogiru") { listOf("isogimasu", "isogimasen", "isogimashita", "isogimasen deshita") }
        expectDeinflectedVerb("isogu") { listOf("isoideiru", "isoideimasu", "isoideimasen", "isoideimasita", "isoideimasendesita") }
        expectDeinflectedVerb("isogu") { listOf("isoide iru", "isoide imasu", "isoide imasen", "isoide imasita", "isoide imasen desita") }
    }

    test("DeinflectKasu") {
        expectDeinflectedVerb("kasu") { listOf("kasu", "kasanai", "kasanakatta") }
        expectDeinflectedVerb("kasu", "kasuru", "kasiru") { listOf("kashimasu", "kashimasen", "kashimashita", "kashita", "kashite", "kashimasen deshita") }
    }

    test("DeinflectMatsu") {
        expectDeinflectedVerb("matu", "maru", "mau") { listOf("matta", "matte") }
        expectDeinflectedVerb("matu") { listOf("matsu", "matanai", "matanakatta") }
        expectDeinflectedVerb("matu", "matiru") { listOf("machimasu", "machimasen", "machimashita", "machimasen deshita") }
    }

    test("DeinflectShinu") {
        expectDeinflectedVerb("sinu", "simu", "sibu") { listOf("しんで") }
        expectDeinflectedVerb("sinu", "simu", "sibu", "si") { listOf("しんだ") }
        expectDeinflectedVerb("sinu") { listOf("しぬ", "しなない", "しななかった") }
        expectDeinflectedVerb("sinu", "siniru") { listOf("しにます", "しにません", "しにました", "しにませんでした") }
    }

    test("DeinflectMiru") {
        // a bit weird to deinflect taberu to tabu, however we have to be able
        // to also deinflect aeru to au
        expectDeinflectedVerb("miru") { listOf("miru", "minai", "minakatta") }
        expectDeinflectedVerb("miru") { listOf("mita", "mite", "miteita", "miteite") }
        expectDeinflectedVerb("miru", "mu") { listOf("mimasu", "mimasen", "mimashita", "mimasen deshita", "mitai") }
        expectDeinflectedVerb("miru") { listOf("miteta", "miteru") }
    }

    test("DeinflectKau") {
        expectDeinflectedVerb("kau") { listOf("かう", "かわない", "かわなかった") }
        expectDeinflectedVerb("kau", "kairu") { listOf("かいます", "かいません", "かいました", "かいませんでした") }
        expectDeinflectedVerb("kau", "katu", "karu") { listOf("かった", "かって") }
        expectDeinflectedVerb("kau") { listOf("かわれる", "かわれない", "かわれた") }
        expectDeinflectedVerb("買u") { listOf("買われる") }
    }

    // https://github.com/mvysny/aedict/issues/922
    test("DeinflectKaru") {
        // -maru works
        expectDeinflectedVerb("始matu", "始mau", "始maru") { listOf("始まった", "始matta") }
        expectDeinflectedVerb("kimatu", "kimau", "kimaru") { listOf("kimatta") }
        // -karu should work as well
        expectDeinflectedVerb("見tukaru", "見tukatu", "見tukau", "見tui") { listOf("見つかった", "見tukatta") }
        expectDeinflectedVerb("mitukaru", "mitukatu", "mitukau", "mitui") { listOf("mitukatta") }
        expectDeinflectedVerb("助karu", "助katu", "助kau", "助i") { listOf("助かった", "助katta") }
        expectDeinflectedVerb("tasukaru", "tasukatu", "tasukau", "tasui") { listOf("tasukatta") }
    }

    test("DeinflectAu") {
        expectDeinflected("au") { listOf("au", "awanai") }
        expectDeinflected("au", "aru") { listOf("atta") }
        // a bit weird, however we have to be able to also "deinflect" taberu to
        // taberu, along with tabu
        expectDeinflected("au", "aeru") { listOf("aenai") }
    }

    test("DeinflectAsobu") {
        expectDeinflected("asobu", "asobiru") { listOf("あそびます", "あそびませんでした") }
        expectDeinflected("asobu", "asonu", "asomu") { listOf("あそんだ", "あそんで") }
        expectDeinflectedVerb("asobu") { listOf("あそぶ", "あそばない", "あそばなかった") }
    }

    test("DeinflectYomu") {
        expectDeinflectedVerb("yomu", "yomiru") { listOf("yomimasen deshita") }
        expectDeinflectedVerb("yomu") { listOf("yomu", "yomanai", "yomanakatta") }
        expectDeinflectedVerb("yomu", "yonu", "yobu") { listOf("yonde", "yonda") }
    }

    test("DeinflectKaeru") {
        expectDeinflected("kaeru", "kaeriru") { listOf("kaerimasen deshita") }
        expectDeinflected("kaeru", "kau") { listOf("kaeru") }
        expectDeinflectedVerb("kaeru") { listOf("kaeranai", "kaeranakatta") }
        expectDeinflectedVerb("kaeru", "kaeu", "kaetu") { listOf("kaetta", "kaette") }
    }

    test("DeinflectIru") {
        expectDeinflected("iru") { listOf("iranai", "inai", "itai") }
        expectDeinflected("iru", "iriru") { listOf("iritai") }
        expectDeinflected("iru", "ireru") { listOf("irenai") }
    }

    /**
     * https://code.google.com/p/aedict/issues/detail?id=220
     */
    test("DeinflectBug220") {
        expectDeinflected("suru") { listOf("dekiru") }
        expectDeinflected("tou") { listOf("toute", "touta") }
    }

    test("deinflectNda") {
        expectDeinflected("iku") { listOf("ikundesu", "ikun desu", "ikun da") }
        expectDeinflected("ikunu", "ikubu", "ikumu", "iku") { listOf("ikunda") }
        expectDeinflected("iru") { listOf("inaindesu", "inain desu", "inain da") }
        expectDeinflected("inaibu", "inaimu", "inainu", "inai") { listOf("inainda") }
    }

    /**
     * https://github.com/mvysny/aedict/issues/865
     */
    test("deinflectShou") {
        expectDeinflected("起ku", "起kiru") { listOf("起kimasyou", "起きましょう") }
    }

    /**
     * https://github.com/mvysny/aedict/issues/865
     */
    test("remove untranslatable stuff") {
        val d = VerbDeinflector.deinflect("asyou", IRomanization.IDENTITY, null)
        expect("") { d.allDeinflections.joinToString() }
    }

    if (false) {
        test("deinflectTeShimatta") {
            expectDeinflected("tameru") { listOf("ためてしまった") }
        }
    }

    test("deinflectIru") {
        expectDeinflected("iru") { listOf("inai") }
    }

    /**
     * https://code.google.com/p/aedict/issues/detail?id=424
     */
    test("deinflectRareru") {
        expectDeinflected("確kameru") { listOf("確かめられる") }
        expectDeinflected("立tu") { listOf("立たれる") }
        expectDeinflected("買u") { listOf("買われる") }
        expectDeinflected("帰ru") { listOf("帰られる") }
    }

    test("Guess") {
        val d = VerbDeinflector.deinflect("taberu", IRomanization.IDENTITY, null)
        expect(false, "" + d) { d.isEmpty } // ERU deinflection: taberu -> taberu, tabu

        // there's no deinflection steps to deinflect taberu to itself
        expect(0) { d.guess(JMDictEntryTest.dummyNoKanji("たべる"))!!.size }
        // one deinflection step: taberu -> tabu
        expect(1) { d.guess(JMDictEntryTest.dummyNoKanji("たぶ"))!!.size }
    }

    /**
     * https://github.com/mvysny/aedict/issues/494
     */
    test("deinflectIku") {
        expectDeinflected("行ku") { listOf("行け") }
    }

    /**
     * https://github.com/mvysny/aedict/issues/501
     */
    test("deinflectShortVerbs") {
        expectDeinflected("見ru") { listOf("見ました", "見masen desita") }
        expectDeinflected("見ru", "見reru") { listOf("見reru", "見remasita") }
        expectDeinflected("見ru") { listOf("見て") }
        expectDeinflectedVerb("見ru") { listOf("見た", "見たい", "見たかった", "見て") }
        expectDeinflectedVerb("見ru") { listOf("見ていた", "見られた") }
        expectDeinflectedVerb("見ru") { listOf("見てた", "見てru") } // teru = shortened te iru.
        expectDeinflectedVerb("来ru") { listOf("来ました") }
    }

    test("modifyPrefix") {
        var d = VerbDeinflector.deinflect("みmasen", IRomanization.IDENTITY, null)
        d.expect("mu", "miru")
        d = d.replacePrefix(JpCharacter('み'), Kanji("見"))
        d.expect("見ru")
        // replacing invalid prefix will cause an empty deinflections to be created.
        d = d.replacePrefix(JpCharacter('み'), Kanji("見"))
        expect(true) { d.isEmpty }
    }

    /**
     * https://github.com/mvysny/aedict/issues/656
     */
    test("crashJa") {
        val d = VerbDeinflector.deinflect("あまのじゃ", IRomanization.HEPBURN, null)
        d.expect("amanozya")
    }

    test("neverReturnNull") {
        val d = VerbDeinflector.deinflect("mk", IRomanization.HEPBURN, null)
        d.expect("mk")
    }

    /**
     * https://github.com/mvysny/aedict/issues/676
     */
    test("doNotPerformInvalidDeinflections") {
        val d = VerbDeinflector.deinflect("へる", IRomanization.HEPBURN, null)
        d.expect("heru")
    }

    /**
     * Don't deinflect へんあい into heru
     * https://github.com/mvysny/aedict/issues/677
     */
    test("doNotDeinflectHexnai") {
        var d = VerbDeinflector.deinflect("へんあい", IRomanization.HEPBURN, null)
        d.expect("hexnai")
        expect(false) { d.isSuccess }
        expectDeinflectedVerb("xnaberu", "xnabu") { listOf("んあberu", "んあbenai", "んあbenakatta", "んあbemasu", "んあbemasen", "んあbemashita", "んあbemasen deshita") }
        // check that there is x nowhere in the readings
        d = VerbDeinflector.deinflect("んあbemasen deshita", IRomanization.HEPBURN, null)
        val withX = d.allDeinflections.firstOrNull { it.deinflected.keys.any { it.contains('x') } || it.inflected.contains('x') }
        expect(null) { withX }
    }

    /**
     * https://github.com/mvysny/aedict/issues/769
     */
    test("ochitsukase") {
        var d = VerbDeinflector.deinflect("落ち着かせ", IRomanization.HEPBURN, null)
        d.expect("落ti着ku")
        d = VerbDeinflector.deinflect("落ち着かせru", IRomanization.HEPBURN, null)
        d.expect("落ti着ku")
    }

    /**
     * Deinflecting x crashes: https://fabric.io/baka/android/apps/sk.baka.aedict3/issues/596e3abdbe077a4dccc11556
     */
    test("deinflectX") {
        VerbDeinflector.deinflect("x", IRomanization.IDENTITY, null)
        VerbDeinflector.deinflect("xxxx", IRomanization.IDENTITY, null)
        VerbDeinflector.deinflect("xxru", IRomanization.IDENTITY, null)
    }

    /**
     * https://github.com/mvysny/aedict/issues/792
     */
    test("deinflectRo") {
        var d = VerbDeinflector.deinflect("見ろ", IRomanization.HEPBURN, null)
        d.expect("見ru")
        d = VerbDeinflector.deinflect("嗅げ", IRomanization.HEPBURN, null)
        d.expect("嗅gu")
        d = VerbDeinflector.deinflect("kaげ", IRomanization.HEPBURN, null)
        d.expect("kagu")
        d = VerbDeinflector.deinflect("待て", IRomanization.HEPBURN, resolver)
        d.expect("待tu")
        d = VerbDeinflector.deinflect("maて", IRomanization.HEPBURN, null)
        d.expect("matu")
        d = VerbDeinflector.deinflect("噛め", IRomanization.HEPBURN, null)
        d.expect("噛mu")
        d = VerbDeinflector.deinflect("かめ", IRomanization.HEPBURN, null)
        d.expect("kamu")
    }

    test("deinflectVolitional") {
        var d = VerbDeinflector.deinflect("潰そう", IRomanization.HEPBURN, null)
        d.expect("潰su")
        d = VerbDeinflector.deinflect("つぶそう", IRomanization.HEPBURN, null)
        d.expect("tubusu")
        d = VerbDeinflector.deinflect("潰sou", IRomanization.HEPBURN, null)
        d.expect("潰su")
        d = VerbDeinflector.deinflect("tubusou", IRomanization.HEPBURN, null)
        d.expect("tubusu")
        d = VerbDeinflector.deinflect("aou", IRomanization.HEPBURN, null)
        d.expect("au")
        d = VerbDeinflector.deinflect("食beyou", IRomanization.HEPBURN, null)
        d.expect("食beru")
        d = VerbDeinflector.deinflect("見you", IRomanization.HEPBURN, null)
        d.expect("見ru")
        d = VerbDeinflector.deinflect("tabeyou", IRomanization.HEPBURN, null)
        d.expect("taberu")
        d = VerbDeinflector.deinflect("miyou", IRomanization.HEPBURN, null)
        d.expect("miru")

        // invalid deinflections, not deinflected
        d = VerbDeinflector.deinflect("ou", IRomanization.HEPBURN, null)
        d.expect("ou")
        d = VerbDeinflector.deinflect("you", IRomanization.HEPBURN, null)
        d.expect("you")
    }

    // https://github.com/mvysny/aedict/issues/952
    test("deinflect VerbInflection.Form.IF2") {
        expectDeinflectedVerb("taberu") { listOf("tabereba") }
        expectDeinflectedVerb("iu") { listOf("ieba") }
        expectDeinflectedVerb("isogu") { listOf("isogeba") }
        expectDeinflectedVerb("出kakeru") { listOf("出かければ") }

        // irregulars
        expectDeinflectedVerb("kuru") { listOf("kureba") }
        expectDeinflectedVerb("来ru") { listOf("来れば") }
        expectDeinflectedVerb("suru") { listOf("sureba") }
    }

    // https://github.com/mvysny/aedict/issues/947
    test("deinflect VerbInflection.Form.NEGATIVE_CONDITIONAL") {
        // a bit weird to deinflect taberu to tabu, however we have to be able
        // to also deinflect aeru to au
        expectDeinflectedVerb("taberu", "tabu") { listOf("tabenakereba") }
        expectDeinflectedVerb("iu") { listOf("iwanakereba") }
        expectDeinflectedVerb("isogu") { listOf("isoganakereba") }
        expectDeinflectedVerb("出kakeru", "出kaku") { listOf("出かけなければ") }

        // irregulars
        expectDeinflectedVerb("kuru") { listOf("konakereba") }
        expectDeinflectedVerb("来ru") { listOf("来なければ") }
        expectDeinflectedVerb("suru") { listOf("sinakereba") }
    }

    // https://github.com/mvysny/aedict/issues/953
    test("deinflect correct verb groups") {
        // imasu => iru(1)
        expectDeinflectedVerbGroup("imasu", mapOf("iru" to InflectableWordClass.VERB_ICHIDAN))
        // inai => iru(1)
        expectDeinflectedVerbGroup("inai", mapOf("iru" to InflectableWordClass.VERB_ICHIDAN))
        // ita => iru(1)
        expectDeinflectedVerbGroup("ita", mapOf("iru" to InflectableWordClass.VERB_ICHIDAN))
        // itai => iru(1)
        expectDeinflectedVerbGroup("itai", mapOf("iru" to InflectableWordClass.VERB_ICHIDAN))
        // iranai => iru(5)
        expectDeinflectedVerbGroup("iranai", mapOf("iru" to InflectableWordClass.VERB_GODAN))
        // irimasu => iru(5)/iriru(1)
        expectDeinflectedVerbGroup("irimasu", mapOf("iru" to InflectableWordClass.VERB_GODAN, "iriru" to InflectableWordClass.VERB_ICHIDAN))
        // iritai => iru(5)/iriru(1)
        expectDeinflectedVerbGroup("iritai", mapOf("iru" to InflectableWordClass.VERB_GODAN, "iriru" to InflectableWordClass.VERB_ICHIDAN))
        // iretai => iru(5)/ireru(1)
        expectDeinflectedVerbGroup("iretai", mapOf("iru" to InflectableWordClass.VERB_GODAN, "ireru" to InflectableWordClass.VERB_ICHIDAN))
        // irenai => iru(5)/ireru(1)
        expectDeinflectedVerbGroup("irenai", mapOf("iru" to InflectableWordClass.VERB_GODAN, "ireru" to InflectableWordClass.VERB_ICHIDAN))
        // kaketai => kakeru(1)/kaku(5)
        expectDeinflectedVerbGroup("kaketai", mapOf("kaku" to InflectableWordClass.VERB_GODAN, "kakeru" to InflectableWordClass.VERB_ICHIDAN))
        // kaketeimasu => kakeru(1)
        expectDeinflectedVerbGroup("kaketeimasu", mapOf("kakeru" to InflectableWordClass.VERB_ICHIDAN))
        // kaketteimasita => kakeu(5)/kakeru(5)/kaketu(5)
        expectDeinflectedVerbGroup("kaketteimasita", mapOf("kakeu" to InflectableWordClass.VERB_GODAN, "kakeru" to InflectableWordClass.VERB_GODAN, "kaketu" to InflectableWordClass.VERB_GODAN))
        // kakerimasu => kakeru(5)/kakeriru(1)
        expectDeinflectedVerbGroup("kakerimasu", mapOf("kakeru" to InflectableWordClass.VERB_GODAN, "kakeriru" to InflectableWordClass.VERB_ICHIDAN))
        // kakeru => kakeru/kaku(5)
        expectDeinflectedVerbGroup("kakeru", mapOf("kakeru" to EnumSet.noneOf(InflectableWordClass::class.java), "kaku" to InflectableWordClass.VERB_GODAN))
        // taberu => taberu/tabu(5)
        expectDeinflectedVerbGroup("taberu", mapOf("taberu" to EnumSet.noneOf(InflectableWordClass::class.java), "tabu" to InflectableWordClass.VERB_GODAN))
        // tabemasu => taberu(1)/tabu(5)
        expectDeinflectedVerbGroup("tabemasu", mapOf("taberu" to InflectableWordClass.VERB_ICHIDAN, "tabu" to InflectableWordClass.VERB_GODAN))
        // oyogemasu => oyogeru(1)/oyogu(5)
        expectDeinflectedVerbGroup("oyogemasu", mapOf("oyogeru" to InflectableWordClass.VERB_ICHIDAN, "oyogu" to InflectableWordClass.VERB_GODAN))
        // nometai => nomeru(1)/nomu(5)
        expectDeinflectedVerbGroup("nometai", mapOf("nomeru" to InflectableWordClass.VERB_ICHIDAN, "nomu" to InflectableWordClass.VERB_GODAN))
        // nondeita => nomu(5)/nonu(5)/nobu(5)
        expectDeinflectedVerbGroup("nondeita", mapOf("nomu" to InflectableWordClass.VERB_GODAN, "nonu" to InflectableWordClass.VERB_GODAN, "nobu" to InflectableWordClass.VERB_GODAN))
        // nondeimasita => nomu(5)/nonu(5)/nobu(5)
        expectDeinflectedVerbGroup("nondeimasita", mapOf("nomu" to InflectableWordClass.VERB_GODAN, "nonu" to InflectableWordClass.VERB_GODAN, "nobu" to InflectableWordClass.VERB_GODAN))
        // aemasita => au(5)/aeru(1)
        expectDeinflectedVerbGroup("aemasita", mapOf("au" to InflectableWordClass.VERB_GODAN, "aeru" to InflectableWordClass.VERB_ICHIDAN))
        // kimasu => kuru/kiru(1)
        expectDeinflectedVerbGroup("kimasu", mapOf("kuru" to InflectableWordClass.VERB_KURU, "kiru" to InflectableWordClass.VERB_ICHIDAN))
        // kirimasu => kiru(5)/kiriru(1)
        expectDeinflectedVerbGroup("kirimasu", mapOf("kiru" to InflectableWordClass.VERB_GODAN, "kiriru" to InflectableWordClass.VERB_ICHIDAN))
        // kurimasu => kuru(5)/kuriru(1)
        expectDeinflectedVerbGroup("kurimasu", mapOf("kuru" to InflectableWordClass.VERB_GODAN, "kuriru" to InflectableWordClass.VERB_ICHIDAN))
        // simasu => suru
        expectDeinflectedVerbGroup("simasu", mapOf("suru" to InflectableWordClass.VERB_SURU))
        // siteimasu => suru
        expectDeinflectedVerbGroup("siteimasu", mapOf("suru" to InflectableWordClass.VERB_SURU))
        // surimasu => suru(5)/suriru(1)
        expectDeinflectedVerbGroup("surimasu", mapOf("suru" to InflectableWordClass.VERB_GODAN, "suriru" to InflectableWordClass.VERB_ICHIDAN))
    }

    // https://github.com/mvysny/aedict/issues/954 & https://gitlab.com/mvysny/jputils/-/issues/1
    test("deinflect suru verbs") {
        // mensuru
        expectDeinflectedVerbGroup("mensimasu", mapOf("mensuru" to InflectableWordClass.VERB_SURU_CLASS, "mensiru" to InflectableWordClass.VERB_ICHIDAN, "mensu" to InflectableWordClass.VERB_GODAN))
        expectDeinflectedVerbGroup("面します", mapOf("面suru" to InflectableWordClass.VERB_SURU_CLASS, "面siru" to InflectableWordClass.VERB_ICHIDAN, "面su" to InflectableWordClass.VERB_GODAN))
        expectDeinflectedVerbGroup("mensimasita", mapOf("mensuru" to InflectableWordClass.VERB_SURU_CLASS, "mensiru" to InflectableWordClass.VERB_ICHIDAN, "mensu" to InflectableWordClass.VERB_GODAN))

        // sessuru
        expectDeinflectedVerbGroup("sessimasu", mapOf("sessuru" to InflectableWordClass.VERB_SURU_CLASS, "sessiru" to InflectableWordClass.VERB_ICHIDAN, "sessu" to InflectableWordClass.VERB_GODAN))
        expectDeinflectedVerbGroup("接します", mapOf("接suru" to InflectableWordClass.VERB_SURU_CLASS, "接siru" to InflectableWordClass.VERB_ICHIDAN, "接su" to InflectableWordClass.VERB_GODAN))

        // sitsumonsuru
        expectDeinflectedVerbGroup("shitumonsiteimasu", mapOf("situmonsuru" to InflectableWordClass.VERB_SURU_CLASS, "situmonsiru" to InflectableWordClass.VERB_ICHIDAN, "situmonsu" to InflectableWordClass.VERB_GODAN))
        expectDeinflectedVerbGroup("質問しています", mapOf("質問suru" to InflectableWordClass.VERB_SURU_CLASS, "質問siru" to InflectableWordClass.VERB_ICHIDAN, "質問su" to InflectableWordClass.VERB_GODAN))

        // yokusuru
        expectDeinflectedVerbGroup("yokusinakatta", mapOf("yokusinai" to InflectableWordClass.ADJ, "yokusuru" to InflectableWordClass.VERB_SURU_CLASS, "yokusiru" to InflectableWordClass.VERB_ICHIDAN))
        expectDeinflectedVerbGroup("良くしなかった", mapOf("良kusinai" to InflectableWordClass.ADJ, "良kusuru" to InflectableWordClass.VERB_SURU_CLASS, "良kusiru" to InflectableWordClass.VERB_ICHIDAN))
        expectDeinflectedVerbGroup("浴しなかった", mapOf("浴sinai" to InflectableWordClass.ADJ, "浴suru" to InflectableWordClass.VERB_SURU_CLASS, "浴siru" to InflectableWordClass.VERB_ICHIDAN))
        expectDeinflectedVerbGroup("浴できた", mapOf("浴suru" to InflectableWordClass.VERB_SURU_CLASS, "浴dekiru" to InflectableWordClass.VERB_ICHIDAN))

        // hassuru
        expectDeinflectedVerbGroup("hassiyou", mapOf("hassuru" to InflectableWordClass.VERB_SURU_CLASS, "hassiru" to InflectableWordClass.VERB_ICHIDAN))
        expectDeinflectedVerbGroup("hassita", mapOf("hassuru" to InflectableWordClass.VERB_SURU_CLASS, "hassiru" to InflectableWordClass.VERB_ICHIDAN, "hassu" to InflectableWordClass.VERB_GODAN))
    }

    // https://github.com/mvysny/aedict/issues/908
    test("deinflect ~tekuru verbs") {
        // taberu
        expectDeinflectedVerbGroup("tabetekuru",
            mapOf("tabetekuru" to EnumSet.noneOf(InflectableWordClass::class.java), "taberu" to InflectableWordClass.VERB_ICHIDAN))

        // hikkosu
        expectDeinflectedVerbGroup("ひっこしてきます",
            mapOf("hikkositekuru" to InflectableWordClass.VERB_GODAN, "hikkosuru" to InflectableWordClass.VERB_SURU_CLASS,
                "hikkosiru" to InflectableWordClass.VERB_ICHIDAN, "hikkosu" to InflectableWordClass.VERB_GODAN))
        expectDeinflectedVerbGroup("ひっこしてくる",
            mapOf("hikkositekuru" to EnumSet.noneOf(InflectableWordClass::class.java), "hikkosuru" to InflectableWordClass.VERB_SURU_CLASS,
                "hikkosiru" to InflectableWordClass.VERB_ICHIDAN, "hikkosu" to InflectableWordClass.VERB_GODAN))
        expectDeinflectedVerbGroup("ひっこしてきました",
            mapOf("hikkositekuru" to InflectableWordClass.VERB_GODAN, "hikkosuru" to InflectableWordClass.VERB_SURU_CLASS,
                "hikkosiru" to InflectableWordClass.VERB_ICHIDAN, "hikkosu" to InflectableWordClass.VERB_GODAN))
        expectDeinflectedVerbGroup("ひっこしてきた",
            mapOf("hikkositekuru" to InflectableWordClass.VERB_GODAN, "hikkosuru" to InflectableWordClass.VERB_SURU_CLASS,
                "hikkosiru" to InflectableWordClass.VERB_ICHIDAN, "hikkosu" to InflectableWordClass.VERB_GODAN))

        // yattekuru
        expectDeinflectedVerbGroup("やってきます",
            mapOf("yattekuru" to InflectableWordClass.VERB_GODAN, "yatu" to InflectableWordClass.VERB_GODAN,
                "yaru" to InflectableWordClass.VERB_GODAN, "yau" to InflectableWordClass.VERB_GODAN))
        expectDeinflectedVerbGroup("やってくる",
            mapOf("yattekuru" to EnumSet.noneOf(InflectableWordClass::class.java), "yatu" to InflectableWordClass.VERB_GODAN,
                "yaru" to InflectableWordClass.VERB_GODAN, "yau" to InflectableWordClass.VERB_GODAN))
    }

    test("deinflect ~teiku verbs") {
        // taberu
        expectDeinflectedVerbGroup("tabeteiku",
            mapOf("tabeteiku" to EnumSet.noneOf(InflectableWordClass::class.java), "taberu" to InflectableWordClass.VERB_ICHIDAN))

        // hikkosu
        expectDeinflectedVerbGroup("ひっこしていきます",
            mapOf("hikkositeiku" to InflectableWordClass.VERB_GODAN, "hikkosuru" to InflectableWordClass.VERB_SURU_CLASS,
                "hikkosiru" to InflectableWordClass.VERB_ICHIDAN, "hikkosu" to InflectableWordClass.VERB_GODAN))
        expectDeinflectedVerbGroup("ひっこしていく",
            mapOf("hikkositeiku" to EnumSet.noneOf(InflectableWordClass::class.java), "hikkosuru" to InflectableWordClass.VERB_SURU_CLASS,
                "hikkosiru" to InflectableWordClass.VERB_ICHIDAN, "hikkosu" to InflectableWordClass.VERB_GODAN))
        expectDeinflectedVerbGroup("ひっこしていきました",
            mapOf("hikkositeiku" to InflectableWordClass.VERB_GODAN, "hikkosuru" to InflectableWordClass.VERB_SURU_CLASS,
                "hikkosiru" to InflectableWordClass.VERB_ICHIDAN, "hikkosu" to InflectableWordClass.VERB_GODAN))
        expectDeinflectedVerbGroup("ひっこしていった",
            mapOf("hikkositeiku" to InflectableWordClass.VERB_GODAN, "hikkosuru" to InflectableWordClass.VERB_SURU_CLASS,
                "hikkosiru" to InflectableWordClass.VERB_ICHIDAN, "hikkosu" to InflectableWordClass.VERB_GODAN))

        // tsuiteiku
        expectDeinflectedVerbGroup("ついていきます",
            mapOf("tuiteiku" to InflectableWordClass.VERB_GODAN, "tuku" to InflectableWordClass.VERB_GODAN,
                "tuiru" to InflectableWordClass.VERB_ICHIDAN))
        expectDeinflectedVerbGroup("ついていく",
            mapOf("tuiteiku" to EnumSet.noneOf(InflectableWordClass::class.java), "tuku" to InflectableWordClass.VERB_GODAN,
                "tuiru" to InflectableWordClass.VERB_ICHIDAN))
    }

    test("引っ越してくる") {
        expectDeinflectedVerb("引xtu越siru", "引xtu越sitekuru", "引xtu越su", "引xtu越suru") { listOf("引っ越してくる") }
    }

    // https://github.com/mvysny/aedict/issues/923
    test("deiflect 来る with kanjis") {
        expectDeinflectedVerbGroup("出来ます", mapOf("出来ru" to InflectableWordClass.VERB_ICHIDAN))
        expectDeinflectedVerbGroup("出来て", mapOf("出来ru" to InflectableWordClass.VERB_ICHIDAN))
        expectDeinflectedVerbGroup("出来なかった", mapOf("出来ru" to InflectableWordClass.VERB_ICHIDAN, "出来nai" to InflectableWordClass.ADJ))
        expectDeinflectedVerbGroup("出来なければ", mapOf("出来ru" to InflectableWordClass.VERB_ICHIDAN))
        expectDeinflectedVerbGroup("使用出来ません", mapOf("使用出来ru" to InflectableWordClass.VERB_ICHIDAN))
        expectDeinflectedVerbGroup("頭来た", mapOf("頭来ru" to InflectableWordClass.VERB_KURU))
    }

    test("deinflect honorifics") {
        expectDeinflectedVerbGroup("kudasaimasu", mapOf("kudasaru" to InflectableWordClass.VERB_GODAN))
        expectDeinflectedVerbGroup("kudasaimashita", mapOf("kudasaru" to InflectableWordClass.VERB_GODAN))
        expectDeinflectedVerbGroup("kudasai", mapOf("kudasaru" to InflectableWordClass.VERB_GODAN))

        expectDeinflectedVerbGroup("irassyaimasu", mapOf("irassyaru" to InflectableWordClass.VERB_GODAN))
        expectDeinflectedVerbGroup("irassyaimashita", mapOf("irassyaru" to InflectableWordClass.VERB_GODAN))
        expectDeinflectedVerbGroup("irassyai", mapOf("irassyaru" to InflectableWordClass.VERB_GODAN))

        expectDeinflectedVerbGroup("ossyaimasu", mapOf("ossyaru" to InflectableWordClass.VERB_GODAN))
        expectDeinflectedVerbGroup("ossyaimashita", mapOf("ossyaru" to InflectableWordClass.VERB_GODAN))
        expectDeinflectedVerbGroup("ossyai", mapOf("ossyaru" to InflectableWordClass.VERB_GODAN))

        expectDeinflectedVerbGroup("gozaimasu", mapOf("gozaru" to InflectableWordClass.VERB_GENERIC))
        expectDeinflectedVerbGroup("gozaimashita", mapOf("gozaru" to InflectableWordClass.VERB_GENERIC))
        expectDeinflectedVerbGroup("gozai", mapOf("gozaru" to InflectableWordClass.VERB_GENERIC))

        expectDeinflectedVerbGroup("nasaimasu", mapOf("nasaru" to InflectableWordClass.VERB_GODAN))
        expectDeinflectedVerbGroup("nasaimashita", mapOf("nasaru" to InflectableWordClass.VERB_GODAN))
        expectDeinflectedVerbGroup("nasai", mapOf("nasaru" to InflectableWordClass.VERB_GODAN))
    }

    // https://github.com/mvysny/aedict/issues/962
    test("deinflect でんえ") {
        expectDeinflectedVerbGroup("でんえ", mapOf("dexnu" to InflectableWordClass.VERB_GODAN))
    }

    test("deinflect simple command") {
        expectDeinflectedVerbGroup("kinasai", mapOf("kuru" to InflectableWordClass.VERB_KURU, "kiru" to InflectableWordClass.VERB_ICHIDAN))
        expectDeinflectedVerbGroup("tabenasai", mapOf("taberu" to InflectableWordClass.VERB_ICHIDAN))
        expectDeinflectedVerbGroup("nominasai", mapOf("nomu" to InflectableWordClass.VERB_GODAN))
        expectDeinflectedVerbGroup("kirinasai", mapOf("kiru" to InflectableWordClass.VERB_GODAN))
        expectDeinflectedVerbGroup("sinasai", mapOf("suru" to InflectableWordClass.VERB_SURU))
    }

    test("deinflect plain command") {
        expectDeinflectedVerbGroup("koi", mapOf("kuru" to InflectableWordClass.VERB_KURU))
        expectDeinflectedVerbGroup("kiro", mapOf("kiru" to InflectableWordClass.VERB_ICHIDAN))
        expectDeinflectedVerbGroup("tabero", mapOf("taberu" to InflectableWordClass.VERB_ICHIDAN))
        expectDeinflectedVerbGroup("nome", mapOf("nomu" to InflectableWordClass.VERB_GODAN))
        expectDeinflectedVerbGroup("kire", mapOf("kiru" to InflectableWordClass.VERB_GODAN))
        expectDeinflectedVerbGroup("siro", mapOf("suru" to InflectableWordClass.VERB_SURU))
    }

    test("deinflect mild command") {
        expectDeinflectedVerbGroup("tabenaide", mapOf("taberu" to InflectableWordClass.VERB_ICHIDAN, "tabu" to InflectableWordClass.VERB_GODAN))
        expectDeinflectedVerbGroup("minaide", mapOf("miru" to InflectableWordClass.VERB_ICHIDAN))
        expectDeinflectedVerbGroup("isoganaide", mapOf("isogu" to InflectableWordClass.VERB_GODAN))
        expectDeinflectedVerbGroup("hairanaide", mapOf("hairu" to InflectableWordClass.VERB_GODAN))
        expectDeinflectedVerbGroup("sinaide", mapOf("suru" to InflectableWordClass.VERB_SURU))
        expectDeinflectedVerbGroup("面しないで", mapOf("面suru" to InflectableWordClass.VERB_SURU_CLASS, "面siru" to InflectableWordClass.VERB_ICHIDAN))
        expectDeinflectedVerbGroup("konaide", mapOf("kuru" to InflectableWordClass.VERB_KURU))
        expectDeinflectedVerbGroup("kinaide", mapOf("kiru" to InflectableWordClass.VERB_ICHIDAN))
        expectDeinflectedVerbGroup("kiranaide", mapOf("kiru" to InflectableWordClass.VERB_GODAN))
    }

    // https://github.com/mvysny/aedict/issues/958
    test("deinflect causative") {
        expectDeinflectedVerbGroup("suwaseru", mapOf("suu" to InflectableWordClass.VERB_GODAN))
        expectDeinflectedVerbGroup("aisaseru", mapOf("airu" to InflectableWordClass.VERB_ICHIDAN, "aisu" to InflectableWordClass.VERB_GODAN))
        expectDeinflectedVerbGroup("hanasaseru", mapOf("hanasu" to InflectableWordClass.VERB_GODAN, "hanasuru" to InflectableWordClass.VERB_SURU_CLASS))
        expectDeinflectedVerbGroup("kiraseru", mapOf("kiru" to InflectableWordClass.VERB_GODAN))
        expectDeinflectedVerbGroup("tabesaseru", mapOf("taberu" to InflectableWordClass.VERB_ICHIDAN))
        expectDeinflectedVerbGroup("せっしさせる", mapOf("sessiru" to InflectableWordClass.VERB_ICHIDAN, "sessisu" to InflectableWordClass.VERB_GODAN))
        expectDeinflectedVerbGroup("kisaseru", mapOf("kiru" to InflectableWordClass.VERB_ICHIDAN, "kisu" to InflectableWordClass.VERB_GODAN))
        expectDeinflectedVerbGroup("saseru", mapOf("suru" to InflectableWordClass.VERB_SURU))
        expectDeinflectedVerbGroup("面させる", mapOf("面su" to InflectableWordClass.VERB_GODAN, "面suru" to InflectableWordClass.VERB_SURU_CLASS))
        expectDeinflectedVerbGroup("kosaseru", mapOf("kuru" to InflectableWordClass.VERB_KURU))
    }

    test("deinflect causative passive") {
        expectDeinflectedVerbGroup("suwaserareru", mapOf("suu" to InflectableWordClass.VERB_GODAN))
        expectDeinflectedVerbGroup("suwasareru", mapOf("suu" to InflectableWordClass.VERB_GODAN))
        expectDeinflectedVerbGroup("aisaserareru", mapOf("airu" to InflectableWordClass.VERB_ICHIDAN, "aisu" to InflectableWordClass.VERB_GODAN))
        expectDeinflectedVerbGroup("hanasaserareru", mapOf("hanasu" to InflectableWordClass.VERB_GODAN, "hanasuru" to InflectableWordClass.VERB_SURU_CLASS))
        expectDeinflectedVerbGroup("kiraserareru", mapOf("kiru" to InflectableWordClass.VERB_GODAN))
        expectDeinflectedVerbGroup("kirasareru", mapOf("kiru" to InflectableWordClass.VERB_GODAN))
        expectDeinflectedVerbGroup("tabesaserareru", mapOf("taberu" to InflectableWordClass.VERB_ICHIDAN))
        expectDeinflectedVerbGroup("せっしさせられる", mapOf("sessiru" to InflectableWordClass.VERB_ICHIDAN, "sessisu" to InflectableWordClass.VERB_GODAN))
        expectDeinflectedVerbGroup("kisaserareru", mapOf("kiru" to InflectableWordClass.VERB_ICHIDAN, "kisu" to InflectableWordClass.VERB_GODAN))
        expectDeinflectedVerbGroup("saserareru", mapOf("suru" to InflectableWordClass.VERB_SURU))
        expectDeinflectedVerbGroup("面させられる", mapOf("面su" to InflectableWordClass.VERB_GODAN, "面suru" to InflectableWordClass.VERB_SURU_CLASS))
        expectDeinflectedVerbGroup("kosaserareru", mapOf("kuru" to InflectableWordClass.VERB_KURU))
    }
})
