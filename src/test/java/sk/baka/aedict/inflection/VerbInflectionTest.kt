/*
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.inflection

import org.junit.jupiter.api.Test
import kotlin.test.expect

/**
 * Tests the [sk.baka.aedict.inflection.VerbInflection] class.
 * @author Martin Vysny
 */
class VerbInflectionTest {
    @Test
    fun testBase1Inflection() {
        val i = Base1Inflector()
        expect("ika") { i.inflect("iku", VerbType.v5ks) }
        expect("ara") { i.inflect("aru", VerbType.godan) }
        expect("ko") { i.inflect("kuru", VerbType.godan) }
        expect("motteko") { i.inflect("もってくる", VerbType.vk) }
        expect("si") { i.inflect("suru", VerbType.godan) }
        expect("hiikinisi") { i.inflect("ひいきにする", VerbType.godan) }
        expect("kawa") { i.inflect("kau", VerbType.godan) }
        expect("aruka") { i.inflect("aruku", VerbType.godan) }
        expect("isoga") { i.inflect("isogu", VerbType.godan) }
        expect("kasa") { i.inflect("kasu", VerbType.godan) }
        expect("mata") { i.inflect("matu", VerbType.godan) }
        expect("shina") { i.inflect("shinu", VerbType.godan) }
        expect("asoba") { i.inflect("asobu", VerbType.godan) }
        expect("yoma") { i.inflect("yomu", VerbType.godan) }
        expect("kaera") { i.inflect("kaeru", VerbType.godan) }
        expect("tabe") { i.inflect("taberu", VerbType.ichidan) }
        expect("de") { i.inflect("deru", VerbType.ichidan) }
        expect("kudasara") { i.inflect("kudasaru", VerbType.v5aru) }
        expect("osshara") { i.inflect("ossharu", VerbType.v5aru) }
        expect("kure") { i.inflect("kureru", VerbType.v1s) }
        expect("uturiyuka") { i.inflect("うつりゆく", VerbType.v5ks) }
        expect("kotogaa") { i.inflect("kotogaaru", VerbType.v5ri) }
        expect("towa") { i.inflect("tou", VerbType.v5us) }
        expect("aisa") { i.inflect("あいする", VerbType.vss) }
    }

    /**
     * Test for https://github.com/mvysny/aedict/issues/612
     */
    @Test
    fun testBase1InflectionKanji() {
        val i = Base1Inflector()
        expect("買wa") { i.inflect("買う", VerbType.godan) }
        expect("聞ka") { i.inflect("聞く", VerbType.godan) }
        expect("泳ga") { i.inflect("泳ぐ", VerbType.godan) }
        expect("話sa") { i.inflect("話す", VerbType.godan) }
        expect("待ta") { i.inflect("待つ", VerbType.godan) }
        expect("死na") { i.inflect("死ぬ", VerbType.godan) }
        expect("遊ba") { i.inflect("遊ぶ", VerbType.godan) }
        expect("飲ma") { i.inflect("飲む", VerbType.godan) }
        expect("分kara") { i.inflect("分かる", VerbType.godan) }
        expect("食be") { i.inflect("食べる", VerbType.ichidan) }
        expect("〆") { i.inflect("〆ru", VerbType.ichidan) }
        expect("ko") { i.inflect("来ru", VerbType.godan) }
        expect("ko") { i.inflect("来る", VerbType.godan) }
        expect("出teko") { i.inflect("出て来る", VerbType.vk) }
        expect("出") { i.inflect("出ru", VerbType.ichidan) }
        expect("si") { i.inflect("為ru", VerbType.godan) }
        expect("ika") { i.inflect("行ku", VerbType.v5ks) }
        expect("下sara") { i.inflect("下さる", VerbType.v5aru) }
        expect("仰ra") { i.inflect("仰ru", VerbType.v5aru) }
        expect("呉re") { i.inflect("呉れる", VerbType.v1s) }
        expect("移ri行ka") { i.inflect("移り行く", VerbType.v5ks) }
        expect("事gaa") { i.inflect("事がある", VerbType.v5ri) }
        expect("有") { i.inflect("有る", VerbType.v5ri) }
        expect("花mo実mo有") { i.inflect("花mo実mo有ru", VerbType.v5ri) }
        expect("嫌iga在") { i.inflect("嫌iga在ru", VerbType.v5ri) }
        expect("訪wa") { i.inflect("訪う", VerbType.v5us) }
        expect("愛sa") { i.inflect("愛する", VerbType.vss) }
    }

    @Test
    fun testBase2Inflection() {
        val i = Base2Inflector()
        expect("iki") { i.inflect("iku", VerbType.v5ks) }
        expect("ari") { i.inflect("aru", VerbType.godan) }
        expect("ki") { i.inflect("kuru", VerbType.godan) }
        expect("motteki") { i.inflect("もってくる", VerbType.vk) }
        expect("si") { i.inflect("suru", VerbType.godan) }
        expect("hiikinisi") { i.inflect("ひいきにする", VerbType.godan) }
        expect("kai") { i.inflect("kau", VerbType.godan) }
        expect("aruki") { i.inflect("aruku", VerbType.godan) }
        expect("isogi") { i.inflect("isogu", VerbType.godan) }
        expect("kasi") { i.inflect("kasu", VerbType.godan) }
        expect("mati") { i.inflect("matu", VerbType.godan) }
        expect("shini") { i.inflect("shinu", VerbType.godan) }
        expect("asobi") { i.inflect("asobu", VerbType.godan) }
        expect("yomi") { i.inflect("yomu", VerbType.godan) }
        expect("kaeri") { i.inflect("kaeru", VerbType.godan) }
        expect("tabe") { i.inflect("taberu", VerbType.ichidan) }
        expect("de") { i.inflect("deru", VerbType.ichidan) }
        expect("kudasai") { i.inflect("kudasaru", VerbType.v5aru) }
        expect("osshai") { i.inflect("ossharu", VerbType.v5aru) }
        expect("kure") { i.inflect("kureru", VerbType.v1s) }
        expect("uturiyuki") { i.inflect("うつりゆく", VerbType.v5ks) }
        expect("kotogaari") { i.inflect("kotogaaru", VerbType.v5ri) }
        expect("toi") { i.inflect("tou", VerbType.v5us) }
        expect("aisi") { i.inflect("あいする", VerbType.vss) }
    }

    @Test
    fun testBase2InflectionKanji() {
        val i = Base2Inflector()
        expect("買i") { i.inflect("買う", VerbType.godan) }
        expect("聞ki") { i.inflect("聞く", VerbType.godan) }
        expect("泳gi") { i.inflect("泳ぐ", VerbType.godan) }
        expect("話si") { i.inflect("話す", VerbType.godan) }
        expect("待ti") { i.inflect("待つ", VerbType.godan) }
        expect("死ni") { i.inflect("死ぬ", VerbType.godan) }
        expect("遊bi") { i.inflect("遊ぶ", VerbType.godan) }
        expect("飲mi") { i.inflect("飲む", VerbType.godan) }
        expect("分kari") { i.inflect("分かる", VerbType.godan) }
        expect("食be") { i.inflect("食べる", VerbType.ichidan) }
        expect("〆") { i.inflect("〆ru", VerbType.ichidan) }
        expect("ki") { i.inflect("来ru", VerbType.godan) }
        expect("ki") { i.inflect("来る", VerbType.godan) }
        expect("出teki") { i.inflect("出て来る", VerbType.vk) }
        expect("出") { i.inflect("出ru", VerbType.ichidan) }
        expect("si") { i.inflect("為ru", VerbType.godan) }
        expect("iki") { i.inflect("行ku", VerbType.v5ks) }
        expect("下sai") { i.inflect("下さる", VerbType.v5aru) }
        expect("仰i") { i.inflect("仰ru", VerbType.v5aru) }
        expect("呉re") { i.inflect("呉れる", VerbType.v1s) }
        expect("移ri行ki") { i.inflect("移り行く", VerbType.v5ks) }
        expect("事gaari") { i.inflect("事がある", VerbType.v5ri) }
        expect("有ri") { i.inflect("有る", VerbType.v5ri) }
        expect("花mo実mo有ri") { i.inflect("花mo実mo有ru", VerbType.v5ri) }
        expect("嫌iga在ri") { i.inflect("嫌iga在ru", VerbType.v5ri) }
        expect("訪i") { i.inflect("訪う", VerbType.v5us) }
        expect("愛si") { i.inflect("愛する", VerbType.vss) }
    }

    @Test
    fun testBase3Inflection() {
        val i = Base3Inflector()
        expect("iku") { i.inflect("iku", VerbType.v5ks) }
        expect("aru") { i.inflect("aru", VerbType.godan) }
        expect("kuru") { i.inflect("kuru", VerbType.godan) }
        expect("mottekuru") { i.inflect("もってくる", VerbType.vk) }
        expect("suru") { i.inflect("suru", VerbType.godan) }
        expect("hiikinisuru") { i.inflect("ひいきにする", VerbType.godan) }
        expect("kau") { i.inflect("kau", VerbType.godan) }
        expect("aruku") { i.inflect("aruku", VerbType.godan) }
        expect("isogu") { i.inflect("isogu", VerbType.godan) }
        expect("kasu") { i.inflect("kasu", VerbType.godan) }
        expect("matu") { i.inflect("matu", VerbType.godan) }
        expect("shinu") { i.inflect("shinu", VerbType.godan) }
        expect("asobu") { i.inflect("asobu", VerbType.godan) }
        expect("yomu") { i.inflect("yomu", VerbType.godan) }
        expect("kaeru") { i.inflect("kaeru", VerbType.godan) }
        expect("taberu") { i.inflect("taberu", VerbType.ichidan) }
        expect("deru") { i.inflect("deru", VerbType.ichidan) }
        expect("kudasaru") { i.inflect("kudasaru", VerbType.v5aru) }
        expect("ossharu") { i.inflect("ossharu", VerbType.v5aru) }
        expect("kureru") { i.inflect("kureru", VerbType.v1s) }
        expect("uturiyuku") { i.inflect("うつりゆく", VerbType.v5ks) }
        expect("kotogaaru") { i.inflect("kotogaaru", VerbType.v5ri) }
        expect("tou") { i.inflect("tou", VerbType.v5us) }
        expect("aisuru") { i.inflect("あいする", VerbType.vss) }
    }

    @Test
    fun testBase3InflectionKanji() {
        val i = Base3Inflector()
        expect("買u") { i.inflect("買う", VerbType.godan) }
        expect("聞ku") { i.inflect("聞く", VerbType.godan) }
        expect("泳gu") { i.inflect("泳ぐ", VerbType.godan) }
        expect("話su") { i.inflect("話す", VerbType.godan) }
        expect("待tu") { i.inflect("待つ", VerbType.godan) }
        expect("死nu") { i.inflect("死ぬ", VerbType.godan) }
        expect("遊bu") { i.inflect("遊ぶ", VerbType.godan) }
        expect("飲mu") { i.inflect("飲む", VerbType.godan) }
        expect("分karu") { i.inflect("分かる", VerbType.godan) }
        expect("食beru") { i.inflect("食べる", VerbType.ichidan) }
        expect("〆ru") { i.inflect("〆ru", VerbType.ichidan) }
        expect("kuru") { i.inflect("来ru", VerbType.godan) }
        expect("kuru") { i.inflect("来る", VerbType.godan) }
        expect("出tekuru") { i.inflect("出て来る", VerbType.vk) }
        expect("出ru") { i.inflect("出ru", VerbType.ichidan) }
        expect("suru") { i.inflect("為ru", VerbType.godan) }
        expect("iku") { i.inflect("行ku", VerbType.v5ks) }
        expect("下saru") { i.inflect("下さる", VerbType.v5aru) }
        expect("仰ru") { i.inflect("仰ru", VerbType.v5aru) }
        expect("呉reru") { i.inflect("呉れる", VerbType.v1s) }
        expect("移ri行ku") { i.inflect("移り行く", VerbType.v5ks) }
        expect("事gaaru") { i.inflect("事がある", VerbType.v5ri) }
        expect("有ru") { i.inflect("有る", VerbType.v5ri) }
        expect("花mo実mo有ru") { i.inflect("花mo実mo有ru", VerbType.v5ri) }
        expect("嫌iga在ru") { i.inflect("嫌iga在ru", VerbType.v5ri) }
        expect("訪u") { i.inflect("訪う", VerbType.v5us) }
        expect("愛suru") { i.inflect("愛する", VerbType.vss) }
    }

    @Test
    fun testBase4Inflection() {
        val i = Base4Inflector()
        expect("ike") { i.inflect("iku", VerbType.v5ks) }
        expect("are") { i.inflect("aru", VerbType.godan) }
        expect("kure") { i.inflect("kuru", VerbType.godan) }
        expect("mottekure") { i.inflect("もってくる", VerbType.vk) }
        expect("sure") { i.inflect("suru", VerbType.godan) }
        expect("hiikinisure") { i.inflect("ひいきにする", VerbType.godan) }
        expect("kae") { i.inflect("kau", VerbType.godan) }
        expect("aruke") { i.inflect("aruku", VerbType.godan) }
        expect("isoge") { i.inflect("isogu", VerbType.godan) }
        expect("kase") { i.inflect("kasu", VerbType.godan) }
        expect("mate") { i.inflect("matu", VerbType.godan) }
        expect("shine") { i.inflect("shinu", VerbType.godan) }
        expect("asobe") { i.inflect("asobu", VerbType.godan) }
        expect("yome") { i.inflect("yomu", VerbType.godan) }
        expect("kaere") { i.inflect("kaeru", VerbType.godan) }
        expect("tabere") { i.inflect("taberu", VerbType.ichidan) }
        expect("dere") { i.inflect("deru", VerbType.ichidan) }
        expect("kudasari") { i.inflect("kudasaru", VerbType.v5aru) }
        expect("osshari") { i.inflect("ossharu", VerbType.v5aru) }
        expect("kurere") { i.inflect("kureru", VerbType.v1s) }
        expect("uturiyuke") { i.inflect("うつりゆく", VerbType.v5ks) }
        expect("kotogaare") { i.inflect("kotogaaru", VerbType.v5ri) }
        expect("toe") { i.inflect("tou", VerbType.v5us) }
        expect("aisie") { i.inflect("あいする", VerbType.vss) }
    }

    @Test
    fun testBase4InflectionKanji() {
        val i = Base4Inflector()
        expect("買e") { i.inflect("買う", VerbType.godan) }
        expect("聞ke") { i.inflect("聞く", VerbType.godan) }
        expect("泳ge") { i.inflect("泳ぐ", VerbType.godan) }
        expect("話se") { i.inflect("話す", VerbType.godan) }
        expect("待te") { i.inflect("待つ", VerbType.godan) }
        expect("死ne") { i.inflect("死ぬ", VerbType.godan) }
        expect("遊be") { i.inflect("遊ぶ", VerbType.godan) }
        expect("飲me") { i.inflect("飲む", VerbType.godan) }
        expect("分kare") { i.inflect("分かる", VerbType.godan) }
        expect("食bere") { i.inflect("食べる", VerbType.ichidan) }
        expect("〆re") { i.inflect("〆ru", VerbType.ichidan) }
        expect("kure") { i.inflect("来ru", VerbType.vk) }
        expect("kure") { i.inflect("来る", VerbType.godan) }
        expect("出tekure") { i.inflect("出て来る", VerbType.vk) }
        expect("出re") { i.inflect("出ru", VerbType.ichidan) }
        expect("sure") { i.inflect("為ru", VerbType.godan) }
        expect("ike") { i.inflect("行ku", VerbType.v5ks) }
        expect("下sari") { i.inflect("下さる", VerbType.v5aru) }
        expect("仰ri") { i.inflect("仰ru", VerbType.v5aru) }
        expect("呉rere") { i.inflect("呉れる", VerbType.v1s) }
        expect("移ri行ke") { i.inflect("移り行く", VerbType.v5ks) }
        expect("事gaare") { i.inflect("事がある", VerbType.v5ri) }
        expect("有re") { i.inflect("有る", VerbType.v5ri) }
        expect("花mo実mo有re") { i.inflect("花mo実mo有ru", VerbType.v5ri) }
        expect("嫌iga在re") { i.inflect("嫌iga在ru", VerbType.v5ri) }
        expect("訪e") { i.inflect("訪う", VerbType.v5us) }
        expect("愛sie") { i.inflect("愛する", VerbType.vss) }
    }

    @Test
    fun testBase5Inflection() {
        val i = Base5Inflector()
        expect("ikou") { i.inflect("iku", VerbType.v5ks) }
        expect("arou") { i.inflect("aru", VerbType.godan) }
        expect("koyou") { i.inflect("kuru", VerbType.godan) }
        expect("mottekoyou") { i.inflect("もってくる", VerbType.vk) }
        expect("siyou") { i.inflect("suru", VerbType.godan) }
        expect("hiikinisiyou") { i.inflect("ひいきにする", VerbType.godan) }
        expect("kaou") { i.inflect("kau", VerbType.godan) }
        expect("arukou") { i.inflect("aruku", VerbType.godan) }
        expect("isogou") { i.inflect("isogu", VerbType.godan) }
        expect("kasou") { i.inflect("kasu", VerbType.godan) }
        expect("matou") { i.inflect("matu", VerbType.godan) }
        expect("shinou") { i.inflect("shinu", VerbType.godan) }
        expect("asobou") { i.inflect("asobu", VerbType.godan) }
        expect("yomou") { i.inflect("yomu", VerbType.godan) }
        expect("kaerou") { i.inflect("kaeru", VerbType.godan) }
        expect("tabeyou") { i.inflect("taberu", VerbType.ichidan) }
        expect("deyou") { i.inflect("deru", VerbType.ichidan) }
        expect("kudasayou") { i.inflect("kudasaru", VerbType.v5aru) } // ??? @todo overit
        expect("osshayou") { i.inflect("ossharu", VerbType.v5aru) }
        expect("kure") { i.inflect("kureru", VerbType.v1s) }
        expect("uturiyukou") { i.inflect("うつりゆく", VerbType.v5ks) }
        expect("kotogaarou") { i.inflect("kotogaaru", VerbType.v5ri) }
        expect("toou") { i.inflect("tou", VerbType.v5us) }
        expect("aisiro") { i.inflect("あいする", VerbType.vss) }
    }

    @Test
    fun testBase5InflectionKanji() {
        val i = Base5Inflector()
        expect("買ou") { i.inflect("買う", VerbType.godan) }
        expect("聞kou") { i.inflect("聞く", VerbType.godan) }
        expect("泳gou") { i.inflect("泳ぐ", VerbType.godan) }
        expect("話sou") { i.inflect("話す", VerbType.godan) }
        expect("待tou") { i.inflect("待つ", VerbType.godan) }
        expect("死nou") { i.inflect("死ぬ", VerbType.godan) }
        expect("遊bou") { i.inflect("遊ぶ", VerbType.godan) }
        expect("飲mou") { i.inflect("飲む", VerbType.godan) }
        expect("分karou") { i.inflect("分かる", VerbType.godan) }
        expect("食beyou") { i.inflect("食べる", VerbType.ichidan) }
        expect("〆you") { i.inflect("〆ru", VerbType.ichidan) }
        expect("koyou") { i.inflect("来ru", VerbType.vk) }
        expect("koyou") { i.inflect("来る", VerbType.godan) }
        expect("出tekoyou") { i.inflect("出て来る", VerbType.vk) }
        expect("出you") { i.inflect("出ru", VerbType.ichidan) }
        expect("siyou") { i.inflect("為ru", VerbType.godan) }
        expect("ikou") { i.inflect("行ku", VerbType.v5ks) }
        expect("下sayou") { i.inflect("下さる", VerbType.v5aru) } // ??? @todo overit
        expect("仰you") { i.inflect("仰ru", VerbType.v5aru) }
        expect("呉re") { i.inflect("呉れる", VerbType.v1s) }
        expect("移ri行kou") { i.inflect("移り行く", VerbType.v5ks) }
        expect("事gaarou") { i.inflect("事がある", VerbType.v5ri) }
        expect("有rou") { i.inflect("有る", VerbType.v5ri) }
        expect("花mo実mo有rou") { i.inflect("花mo実mo有ru", VerbType.v5ri) }
        expect("嫌iga在rou") { i.inflect("嫌iga在ru", VerbType.v5ri) }
        expect("訪ou") { i.inflect("訪う", VerbType.v5us) }
        expect("愛siro") { i.inflect("愛する", VerbType.vss) }
    }

    @Test
    fun testBaseTeInflection() {
        val i = BaseTeInflector()
        expect("itte") { i.inflect("iku", VerbType.v5ks) }
        expect("atte") { i.inflect("aru", VerbType.godan) }
        expect("kite") { i.inflect("kuru", VerbType.godan) }
        expect("mottekite") { i.inflect("もってくる", VerbType.vk) }
        expect("site") { i.inflect("suru", VerbType.godan) }
        expect("hiikinisite") { i.inflect("ひいきにする", VerbType.godan) }
        expect("katte") { i.inflect("kau", VerbType.godan) }
        expect("aruite") { i.inflect("aruku", VerbType.godan) }
        expect("isoide") { i.inflect("isogu", VerbType.godan) }
        expect("kasite") { i.inflect("kasu", VerbType.godan) }
        expect("matte") { i.inflect("matu", VerbType.godan) }
        expect("shinde") { i.inflect("shinu", VerbType.godan) }
        expect("asonde") { i.inflect("asobu", VerbType.godan) }
        expect("yonde") { i.inflect("yomu", VerbType.godan) }
        expect("kaette") { i.inflect("kaeru", VerbType.godan) }
        expect("tabete") { i.inflect("taberu", VerbType.ichidan) }
        expect("dete") { i.inflect("deru", VerbType.ichidan) }
        expect("kudasatte") { i.inflect("kudasaru", VerbType.v5aru) }
        expect("osshatte") { i.inflect("ossharu", VerbType.v5aru) }
        expect("kurete") { i.inflect("kureru", VerbType.v1s) }
        expect("uturiyutte") { i.inflect("うつりゆく", VerbType.v5ks) }
        expect("kotogaatte") { i.inflect("kotogaaru", VerbType.v5ri) }
        expect("toute") { i.inflect("tou", VerbType.v5us) }
        expect("aisite") { i.inflect("あいする", VerbType.vss) }
    }

    @Test
    fun testBaseTeInflectionKanji() {
        val i = BaseTeInflector()
        expect("買tte") { i.inflect("買う", VerbType.godan) }
        expect("聞ite") { i.inflect("聞く", VerbType.godan) }
        expect("泳ide") { i.inflect("泳ぐ", VerbType.godan) }
        expect("話site") { i.inflect("話す", VerbType.godan) }
        expect("待tte") { i.inflect("待つ", VerbType.godan) }
        expect("死nde") { i.inflect("死ぬ", VerbType.godan) }
        expect("遊nde") { i.inflect("遊ぶ", VerbType.godan) }
        expect("飲nde") { i.inflect("飲む", VerbType.godan) }
        expect("分katte") { i.inflect("分かる", VerbType.godan) }
        expect("食bete") { i.inflect("食べる", VerbType.ichidan) }
        expect("〆te") { i.inflect("〆ru", VerbType.ichidan) }
        expect("kite") { i.inflect("来ru", VerbType.vk) }
        expect("kite") { i.inflect("来る", VerbType.godan) }
        expect("出tekite") { i.inflect("出て来る", VerbType.vk) }
        expect("出te") { i.inflect("出ru", VerbType.ichidan) }
        expect("site") { i.inflect("為ru", VerbType.godan) }
        expect("itte") { i.inflect("行ku", VerbType.v5ks) }
        expect("下satte") { i.inflect("下さる", VerbType.v5aru) }
        expect("仰tte") { i.inflect("仰ru", VerbType.v5aru) }
        expect("呉rete") { i.inflect("呉れる", VerbType.v1s) }
        expect("移ri行tte") { i.inflect("移り行く", VerbType.v5ks) }
        expect("事gaatte") { i.inflect("事がある", VerbType.v5ri) }
        expect("有tte") { i.inflect("有る", VerbType.v5ri) }
        expect("花mo実mo有tte") { i.inflect("花mo実mo有ru", VerbType.v5ri) }
        expect("嫌iga在tte") { i.inflect("嫌iga在ru", VerbType.v5ri) }
        expect("訪ute") { i.inflect("訪う", VerbType.v5us) }
        expect("愛site") { i.inflect("愛する", VerbType.vss) }
    }

    @Test
    fun testBaseTaInflection() {
        val i = BaseTaInflector()
        expect("itta") { i.inflect("iku", VerbType.v5ks) }
        expect("atta") { i.inflect("aru", VerbType.godan) }
        expect("kita") { i.inflect("kuru", VerbType.godan) }
        expect("mottekita") { i.inflect("もってくる", VerbType.vk) }
        expect("sita") { i.inflect("suru", VerbType.godan) }
        expect("hiikinisita") { i.inflect("ひいきにする", VerbType.godan) }
        expect("katta") { i.inflect("kau", VerbType.godan) }
        expect("aruita") { i.inflect("aruku", VerbType.godan) }
        expect("isoida") { i.inflect("isogu", VerbType.godan) }
        expect("kasita") { i.inflect("kasu", VerbType.godan) }
        expect("matta") { i.inflect("matu", VerbType.godan) }
        expect("shinda") { i.inflect("shinu", VerbType.godan) }
        expect("asonda") { i.inflect("asobu", VerbType.godan) }
        expect("yonda") { i.inflect("yomu", VerbType.godan) }
        expect("kaetta") { i.inflect("kaeru", VerbType.godan) }
        expect("tabeta") { i.inflect("taberu", VerbType.ichidan) }
        expect("deta") { i.inflect("deru", VerbType.ichidan) }
        expect("kudasatta") { i.inflect("kudasaru", VerbType.v5aru) }
        expect("osshatta") { i.inflect("ossharu", VerbType.v5aru) }
        expect("kureta") { i.inflect("kureru", VerbType.v1s) }
        expect("uturiyutta") { i.inflect("うつりゆく", VerbType.v5ks) }
        expect("kotogaatta") { i.inflect("kotogaaru", VerbType.v5ri) }
        expect("touta") { i.inflect("tou", VerbType.v5us) }
        expect("aisita") { i.inflect("あいする", VerbType.vss) }
    }

    @Test
    fun testBaseTaInflectionKanji() {
        val i = BaseTaInflector()
        expect("買tta") { i.inflect("買う", VerbType.godan) }
        expect("聞ita") { i.inflect("聞く", VerbType.godan) }
        expect("泳ida") { i.inflect("泳ぐ", VerbType.godan) }
        expect("話sita") { i.inflect("話す", VerbType.godan) }
        expect("待tta") { i.inflect("待つ", VerbType.godan) }
        expect("死nda") { i.inflect("死ぬ", VerbType.godan) }
        expect("遊nda") { i.inflect("遊ぶ", VerbType.godan) }
        expect("飲nda") { i.inflect("飲む", VerbType.godan) }
        expect("分katta") { i.inflect("分かる", VerbType.godan) }
        expect("食beta") { i.inflect("食べる", VerbType.ichidan) }
        expect("〆ta") { i.inflect("〆ru", VerbType.ichidan) }
        expect("kita") { i.inflect("来ru", VerbType.vk) }
        expect("kita") { i.inflect("来る", VerbType.godan) }
        expect("出tekita") { i.inflect("出て来る", VerbType.vk) }
        expect("出ta") { i.inflect("出ru", VerbType.ichidan) }
        expect("sita") { i.inflect("為ru", VerbType.godan) }
        expect("itta") { i.inflect("行ku", VerbType.v5ks) }
        expect("下satta") { i.inflect("下さる", VerbType.v5aru) }
        expect("仰tta") { i.inflect("仰ru", VerbType.v5aru) }
        expect("呉reta") { i.inflect("呉れる", VerbType.v1s) }
        expect("移ri行tta") { i.inflect("移り行く", VerbType.v5ks) }
        expect("事gaatta") { i.inflect("事がある", VerbType.v5ri) }
        expect("有tta") { i.inflect("有る", VerbType.v5ri) }
        expect("花mo実mo有tta") { i.inflect("花mo実mo有ru", VerbType.v5ri) }
        expect("嫌iga在tta") { i.inflect("嫌iga在ru", VerbType.v5ri) }
        expect("訪uta") { i.inflect("訪う", VerbType.v5us) }
        expect("愛sita") { i.inflect("愛する", VerbType.vss) }
    }

    // https://github.com/mvysny/aedict/issues/918
    @Test
    fun testPotentialForm() {
        val i = PotentialInflector()
        expect("taberareru") { i.inflect("taberu", VerbType.ichidan) }
        expect("kirareru") { i.inflect("kiru", VerbType.ichidan) }
        expect("kireru") { i.inflect("kiru", VerbType.godan) }
        expect("oyogeru") { i.inflect("oyogu", VerbType.godan) }
        expect("sueru") { i.inflect("suu", VerbType.godan) }
        expect("dekiru") { i.inflect("suru", VerbType.godan) }
        expect("korareru") { i.inflect("kuru", VerbType.vk) }
    }

    @Test
    fun testTimSenseiLink() {
        expect("http://ww8.tiki.ne.jp/~tmath/language/verbs.htm#01") { VerbInflection.Form.PLAIN.url }
        expect("http://ww8.tiki.ne.jp/~tmath/language/verbs.htm#74") { VerbInflection.Form.EVEN_IF.url }
    }

    /**
     * https://github.com/mvysny/aedict/issues/796
     */
    @Test
    fun testIsPossiblyIchidan() {
        expect(true) { VerbType.isPossiblyIchidan("来ru") }
        expect(true) { VerbType.isPossiblyIchidan("deru") }
        expect(true) { VerbType.isPossiblyIchidan("来る") }
        expect(true) { VerbType.isPossiblyIchidan("見ru") }
        expect(true) { VerbType.isPossiblyIchidan("miru") }
        expect(true) { VerbType.isPossiblyIchidan("見る") }
        expect(true) { VerbType.isPossiblyIchidan("miru") }
        expect(true) { VerbType.isPossiblyIchidan("食べる") }
        expect(true) { VerbType.isPossiblyIchidan("食beru") }
        expect(true) { VerbType.isPossiblyIchidan("taberu") }
        expect(false) { VerbType.isPossiblyIchidan("死ぬ") }
        expect(false) { VerbType.isPossiblyIchidan("行ku") }
        expect(false) { VerbType.isPossiblyIchidan("下さる") }
        expect(false) { VerbType.isPossiblyIchidan("下saru") }
        expect(false) { VerbType.isPossiblyIchidan("kudasaru") }
        expect(false) { VerbType.isPossiblyIchidan("ii") }
        expect(true) { VerbType.isPossiblyIchidan("〆ru") }
    }

    @Test
    fun testSaseruInflection() {
        val i = VerbInflection.Form.LET_HIM
        expect("ikaseru") { i.inflect("iku", VerbType.v5ks) }
        expect("araseru") { i.inflect("aru", VerbType.godan) }
        expect("kosaseru") { i.inflect("kuru", VerbType.vk) }
        expect("mottekosaseru") { i.inflect("mottekuru", VerbType.vk) }
        expect("saseru") { i.inflect("suru", VerbType.godan) }
        expect("hiikinisaseru") { i.inflect("hiikinisuru", VerbType.godan) }
        expect("kawaseru") { i.inflect("kau", VerbType.godan) }
        expect("arukaseru") { i.inflect("aruku", VerbType.godan) }
        expect("isogaseru") { i.inflect("isogu", VerbType.godan) }
        expect("kasaseru") { i.inflect("kasu", VerbType.godan) }
        expect("mataseru") { i.inflect("matu", VerbType.godan) }
        expect("shinaseru") { i.inflect("shinu", VerbType.godan) }
        expect("asobaseru") { i.inflect("asobu", VerbType.godan) }
        expect("yomaseru") { i.inflect("yomu", VerbType.godan) }
        expect("kaeraseru") { i.inflect("kaeru", VerbType.godan) }
        expect("tabesaseru") { i.inflect("taberu", VerbType.ichidan) }
        expect("desaseru") { i.inflect("deru", VerbType.ichidan) }
        expect("kudasaseru") { i.inflect("kudasaru", VerbType.v5aru) }
        expect("ossharaseru") { i.inflect("ossharu", VerbType.v5aru) }
        expect("kuresaseru") { i.inflect("kureru", VerbType.v1s) }
        expect("uturiyukaseru") { i.inflect("uturiyuku", VerbType.v5ks) }
        expect("kotogaaseru") { i.inflect("kotogaaru", VerbType.v5ri) }
        expect("towaseru") { i.inflect("tou", VerbType.v5us) }
        expect("aisaseru") { i.inflect("aisuru", VerbType.vss) }
    }

    @Test
    fun testSaseruInflectionKanji() {
        val i = VerbInflection.Form.LET_HIM
        expect("買waseru") { i.inflect("買u", VerbType.godan) }
        expect("聞kaseru") { i.inflect("聞ku", VerbType.godan) }
        expect("泳gaseru") { i.inflect("泳gu", VerbType.godan) }
        expect("話saseru") { i.inflect("話su", VerbType.godan) }
        expect("待taseru") { i.inflect("待tu", VerbType.godan) }
        expect("死naseru") { i.inflect("死nu", VerbType.godan) }
        expect("遊baseru") { i.inflect("遊bu", VerbType.godan) }
        expect("飲maseru") { i.inflect("飲mu", VerbType.godan) }
        expect("分karaseru") { i.inflect("分karu", VerbType.godan) }
        expect("食besaseru") { i.inflect("食beru", VerbType.ichidan) }
        expect("〆saseru") { i.inflect("〆ru", VerbType.ichidan) }
        expect("kosaseru") { i.inflect("来ru", VerbType.vk) }
        expect("出tekosaseru") { i.inflect("出te来ru", VerbType.vk) }
        expect("出saseru") { i.inflect("出ru", VerbType.ichidan) }
        expect("saseru") { i.inflect("為ru", VerbType.godan) }
        expect("ikaseru") { i.inflect("行ku", VerbType.v5ks) }
        expect("下saseru") { i.inflect("下saru", VerbType.v5aru) }
        expect("仰raseru") { i.inflect("仰ru", VerbType.v5aru) }
        expect("呉resaseru") { i.inflect("呉reru", VerbType.v1s) }
        expect("移ri行kaseru") { i.inflect("移ri行ku", VerbType.v5ks) }
        expect("事gaaseru") { i.inflect("事gaaru", VerbType.v5ri) }
        expect("有seru") { i.inflect("有ru", VerbType.v5ri) }
        expect("花mo実mo有seru") { i.inflect("花mo実mo有ru", VerbType.v5ri) }
        expect("嫌iga在seru") { i.inflect("嫌iga在ru", VerbType.v5ri) }
        expect("訪waseru") { i.inflect("訪u", VerbType.v5us) }
        expect("愛saseru") { i.inflect("愛suru", VerbType.vss) }
    }
}
