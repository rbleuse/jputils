/*
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.inflection

import com.github.mvysny.dynatest.DynaTest
import kotlin.test.expect

class ShortVerbDeinflectorTest : DynaTest({
    test("simple") {
        val d = ShortVerbDeinflector("masu", VerbInflection.Form.POLITE, "ru")
        expect(setOf("着ru")) { d.deinflect("着masu")?.keys }
    }
})
