/*
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.dict

import org.junit.jupiter.api.Test
import sk.baka.aedict.util.Boxables
import sk.baka.aedict.util.Language
import sk.baka.aedict.util.Writables
import java.util.*
import kotlin.test.expect

/**
 * @author mvy
 */
class ExamplesEntryTest {
    private val full = ExamplesEntry(Gloss().add(Language.ENGLISH, "bla").add(Language.ENGLISH, "foo"),
            "kanji",
            "reading",
            mockWords("a", "B", "c"),
            5,
            "reading2")

    @Test
    fun testEquals() {
        expect(bare) { bare }
        expect(false) { bare == full }
        expect(false) { full == bare }
        expect(full) { full }
    }

    @Test
    fun testWritable() {
        expect(bare) { Writables.clone(bare) }
        expect(full) { Writables.clone(full) }
    }

    @Test
    fun testBoxing() {
        expect(bare) { Boxables.testclone(bare) }
        expect(full) { Boxables.testclone(full) }
    }

    @Test
    fun senseOneLiner() {
        expect("bla, foo") { full.getSenseOneLiner(null, false) }
        expect("bla, foo") { full.getSenseOneLiner(null, true) }
    }

    companion object {
        private fun mockWords(vararg words: String): List<ExamplesEntry.Word> =
                words.map { ExamplesEntry.Word(it, it, it) }

        val bare = ExamplesEntry(Gloss().add(Language("en"), "bla"),
                "kanji",
                "reading",
                mockWords(),
                null, null)
    }
}
