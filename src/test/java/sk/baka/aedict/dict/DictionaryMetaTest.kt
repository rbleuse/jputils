/*
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.dict

import org.junit.jupiter.api.Test
import sk.baka.aedict.dict.download.DictionaryMeta
import sk.baka.aedict.util.MD5
import sk.baka.aedict.util.Writables
import sk.baka.aedict.util.cloneByBoxing
import java.util.*
import kotlin.test.expect

class DictionaryMetaTest {

    @Test
    fun createEmpty() {
        // Aedict's FileTypeEnum creates stuff like this:
        DictionaryMeta(0, 0, MD5.EMPTY, Date(), "Jim Breen's JMDict",
                "Copyright Jim Breen and the Electronic Dictionary Research Group",
                "http://www.csse.monash.edu.au/~jwb/jmdict.html",
                DictionaryMeta.DictionaryID.EDICT_JB,
                false)
    }

    @Test
    fun testSerialization() {
        val md5 = MD5(byteArrayOf(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15))
        val meta = DictionaryMeta(25L, 36L, md5, Date(), "bla", "hu", "kvak", DictionaryMeta.DictionaryID.KANJIDIC_JB, false)
        val list = DictionaryMeta.DictionaryMetaList(ArrayList(listOf(meta)))
        expect(list) { Writables.clone(list) }
        expect(list) { list.cloneByBoxing() }
    }
}
