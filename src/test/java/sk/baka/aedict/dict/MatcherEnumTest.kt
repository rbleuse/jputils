/*
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.dict

import org.junit.jupiter.api.Test
import kotlin.test.expect

/**
 * @author mvy
 */
class MatcherEnumTest {
    @Test
    fun startEnd() {
        expect(MatcherEnum.StartsWith) { MatcherEnum.startEnd(true, false) }
        expect(MatcherEnum.EndsWith) { MatcherEnum.startEnd(false, true) }
        expect(MatcherEnum.Substring) { MatcherEnum.startEnd(false, false) }
        expect(MatcherEnum.Exact) { MatcherEnum.startEnd(true, true) }
    }

    private fun mock(reading: String) = JMDictEntry.mock(reading).ref.jmDictEntry!!

    @Test
    fun matchesStartsWith() {
        val m = MatcherEnum.StartsWith
        expect(false) { m.matches(setOf<String>(), mock("foo")) }
        expect(false) { m.matches(setOf("bar"), mock("foo")) }
        expect(true) { m.matches(setOf("f"), mock("foo")) }
        expect(true) { m.matches(setOf("foo"), mock("foo")) }
        expect(true) { m.matches(setOf("bar", "foo"), mock("foo")) }
    }

    @Test
    fun matchesExact() {
        val m = MatcherEnum.Exact
        expect(false) { m.matches(setOf<String>(), mock("foo")) }
        expect(false) { m.matches(setOf("bar"), mock("foo")) }
        expect(false) { m.matches(setOf("f"), mock("foo")) }
        expect(true) { m.matches(setOf("foo"), mock("foo")) }
        expect(true) { m.matches(setOf("bar", "foo"), mock("foo")) }
    }
}
