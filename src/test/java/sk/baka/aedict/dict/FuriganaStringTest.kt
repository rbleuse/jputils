/*
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.dict

import com.github.mvysny.dynatest.DynaTest
import kotlin.test.expect

class FuriganaStringTest : DynaTest({

    fun String.parse() = FuriganaString.parse(this, RubyFurigana.FORMATTER_FURIGANA_VIEW)

    test("format") {
        expect("foo{bar;baz}coo") { FuriganaString(listOf(FuriganaString.Token(null, "foo"), FuriganaString.Token("baz", "bar"), FuriganaString.Token(null, "coo"))).format() }
        expect("") { FuriganaString.EMPTY.format() }
        expect("foo") { FuriganaString.ofUnannotated("foo").format() }
        expect("{foo;ann}") { FuriganaString(listOf(FuriganaString.Token("ann", "foo"))).format() }
    }

    test("parse") {
        expect("foo{bar;baz}coo") { "foo{bar;baz}coo".parse().format() }
        // normalization won't merge adjacent annotated elements by default
        expect("foo{bar;baz}{kvak;prask}") { "foo{bar;baz}{kvak;prask}".parse().format() }
        expect("{見;み}た{目;め}") { FuriganaString.parse("<ruby><rb>見</rb><rp>(</rp><rt>み</rt><rp>)</rp></ruby>た<ruby><rb>目</rb><rp>(</rp><rt>め</rt><rp>)</rp></ruby>", RubyFurigana.FORMATTER_HTML).format() }
    }

    test("normalization") {
        // removes self-annotation with the same thing
        expect("foo") { FuriganaString(listOf(FuriganaString.Token("foo", "foo"))).normalized().format() }

        // by default won't merge annotated tokens
        expect("{foo;bar}{foo;bar}") { FuriganaString(listOf(FuriganaString.Token("bar", "foo"), FuriganaString.Token("bar", "foo"))).normalized().format() }
        expect("{foofoo;barbar}") { FuriganaString(listOf(FuriganaString.Token("bar", "foo"), FuriganaString.Token("bar", "foo"))).normalized(true).format() }
    }

    test("toString") {
        expect("foobarcoo") { "foo{bar;baz}coo".parse().toString() }
        expect("foobarcoo") { "foo{bar;baz}coo".parse().raw }
        expect("foo") { FuriganaString.ofUnannotated("foo").toString() }
    }

    test("length") {
        expect(9) { "foo{bar;baz}coo".parse().length }
        expect(3) { FuriganaString.ofUnannotated("foo").length }
        expect(0) { FuriganaString.EMPTY.length }
    }

    test("substring") {
        expect("foo{bar;baz}coo") { "foo{bar;baz}coo".parse().subSequence(0, 9).format() }
        expect("oo{bar;baz}coo") { "foo{bar;baz}coo".parse().subSequence(1, 9).format() }
        expect("{bar;baz}coo") { "foo{bar;baz}coo".parse().subSequence(3, 9).format() }
        expect("{r;baz}coo") { "foo{bar;baz}coo".parse().subSequence(5, 9).format() }
        expect("coo") { "foo{bar;baz}coo".parse().subSequence(6, 9).format() }
        expect("") { "foo{bar;baz}coo".parse().subSequence(9, 9).format() }

        expect("{bar;baz}") { "foo{bar;baz}coo".parse().subSequence(3, 6).format() }
        expect("{ba;baz}") { "foo{bar;baz}coo".parse().subSequence(3, 5).format() }
        expect("{a;baz}") { "foo{bar;baz}coo".parse().subSequence(4, 5).format() }
        expect("{ar;baz}") { "foo{bar;baz}coo".parse().subSequence(4, 6).format() }
        expect("{ar;baz}c") { "foo{bar;baz}coo".parse().subSequence(4, 7).format() }
    }

    test("furiganaIndexToRawIndex") {
        fun String.i2i(index: Int) = FuriganaString.furiganaIndexToRawIndex(this, index)
        expect(0) { "foo{bar;baz}kvak".i2i(0) }
        expect(1) { "foo{bar;baz}kvak".i2i(1) }
        expect(3) { "foo{bar;baz}kvak".i2i(4) }
        expect(4) { "foo{bar;baz}kvak".i2i(5) }
        expect(5) { "foo{bar;baz}kvak".i2i(6) }
        expect(6) { "foo{bar;baz}kvak".i2i(7) }
        expect(6) { "foo{bar;baz}kvak".i2i(8) }
        expect(6) { "foo{bar;baz}kvak".i2i(9) }
        expect(6) { "foo{bar;baz}kvak".i2i(10) }
        expect(6) { "foo{bar;baz}kvak".i2i(11) }
        expect(6) { "foo{bar;baz}kvak".i2i(12) }
        expect(7) { "foo{bar;baz}kvak".i2i(13) }
        expect(9) { "foo{bar;baz}kvak".i2i(15) }
        expect(10) { "foo{bar;baz}kvak".i2i(17) }
        expect(10) { "foo{bar;baz}kvak".i2i(20) }

        expect(0) { "{bar;baz}kvak".i2i(0) }
        expect(1) { "{bar;baz}kvak".i2i(2) }
        expect(2) { "{bar;baz}kvak".i2i(3) }
        expect(3) { "{bar;baz}kvak".i2i(4) }
        expect(3) { "{bar;baz}kvak".i2i(5) }
        expect(3) { "{bar;baz}kvak".i2i(6) }
        expect(3) { "{bar;baz}kvak".i2i(7) }
        expect(3) { "{bar;baz}kvak".i2i(8) }
        expect(3) { "{bar;baz}kvak".i2i(9) }
        expect(4) { "{bar;baz}kvak".i2i(10) }
        expect(5) { "{bar;baz}kvak".i2i(11) }
    }

    test("plus") {
        expect(FuriganaString.EMPTY) { FuriganaString.EMPTY + FuriganaString.EMPTY }
        expect("foo") { (FuriganaString.EMPTY + "foo").format() }
        expect("foo{bar;baz}cookvak") { ("foo{bar;baz}coo".parse() + "kvak").format() }
        expect("foo{bar;baz}coofoo{bar;baz}coo".parse()) { "foo{bar;baz}coo".parse() + "foo{bar;baz}coo".parse() }
    }

    test("prepend") {
        expect(FuriganaString.EMPTY) { FuriganaString.EMPTY.prepend("") }
        expect("aaa") { FuriganaString.EMPTY.prepend("aaa").format() }
        expect("foo{bar;baz}coo") { "foo{bar;baz}coo".parse().prepend("").format() }
        expect("kvakfoo{bar;baz}coo") { "foo{bar;baz}coo".parse().prepend("kvak").format() }
    }
})
