/*
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.dict

import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import sk.baka.aedict.util.Writables
import sk.baka.aedict.util.cloneByBoxing
import kotlin.test.expect

class SkipCodeTest {
    @Test
    fun testToString() {
        expect("1-2-3") { SkipCode(1, 2, 3).toString() }
    }

    @Test
    fun testParse() {
        expect(SkipCode(1, 2, 3)) { SkipCode.parse("1-2-3") }
    }

    @Test
    fun testParseFailure1() {
        assertThrows(IllegalArgumentException::class.java) { SkipCode.parse("1-2-x") }
        assertThrows(IllegalArgumentException::class.java) { SkipCode.parse("1-2-3-4") }
        assertThrows(IllegalArgumentException::class.java) { SkipCode.parse("hu") }
        assertThrows(IllegalArgumentException::class.java) { SkipCode.parse("2-2") }
    }

    @Test
    fun serialization() {
        expect(SkipCode(3, 5, 7)) { Writables.clone(SkipCode(3, 5, 7)) }
        expect(SkipCode(3, 5, 7)) { SkipCode(3, 5, 7).cloneByBoxing() }
    }
}
