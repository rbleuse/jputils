/*
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.dict

import com.github.mvysny.dynatest.cloneBySerialization
import com.github.mvysny.dynatest.expectList
import org.junit.jupiter.api.Test
import sk.baka.aedict.kanji.Kanji
import sk.baka.aedict.kanji.PitchAccent
import sk.baka.aedict.util.Boxables
import sk.baka.aedict.util.Language
import sk.baka.aedict.util.Writables
import sk.baka.aedict.util.cloneByBoxing
import java.util.*
import kotlin.test.expect

class JMDictEntryTest {

    @Test
    fun testSenseOneLiner() {
        expect("bla, foo; (int) bar") { jmdict.getSenseOneLiner(Language.JAPANESE, true) }
        expect("bla, foo; (int) bar") { jmdict.getSenseOneLiner(Language.ENGLISH, true) }
        expect("bla, foo; bar") { jmdict.getSenseOneLiner(Language.JAPANESE, false) }
        expect("bla, foo; bar") { jmdict.getSenseOneLiner(Language.ENGLISH, false) }
        expect("(P) bla") { jmdictNoKanji.getSenseOneLiner(Language.JAPANESE, true) }
        expect("(P) bla") { jmdictNoKanji.getSenseOneLiner(Language.ENGLISH, true) }
        expect("bla") { jmdictNoKanji.getSenseOneLiner(Language.JAPANESE, false) }
        expect("bla") { jmdictNoKanji.getSenseOneLiner(Language.ENGLISH, false) }
    }

    @Test
    fun writableTest() {
        expect(jmdictNoKanji) { Writables.clone(jmdictNoKanji) }
        expect(jmdictNoKanji) { jmdictNoKanji.cloneByBoxing() }
        expect(jmdict) { Writables.clone(jmdict) }
        expect(jmdict) { jmdict.cloneByBoxing() }
    }

    @Test
    fun writableTest2() {
        val kd = JMDictEntry.KanjiData("bla", true, EnumSet.of(DictCode.adj, DictCode.fem2, DictCode.col), 7, 40, EnumSet.of(JMDictEntry.Pri.News1), (25).toByte())
        expect(kd) { Boxables.testclone(kd) }
        var rd = JMDictEntry.ReadingData("bla", null, EnumSet.of(DictCode.adjno), true, 5, 10, EnumSet.of(JMDictEntry.Pri.Gai1), (3).toByte()).apply {
            accent = ACCENT
        }
        expect(rd) { Boxables.testclone(rd) }
        rd = JMDictEntry.ReadingData("bla", HashSet(Arrays.asList("foo", "bar")), EnumSet.of(DictCode.adjno), true, 8, 2, null, (10).toByte()).apply {
            accent = ACCENT
        }
        expect(rd) { Boxables.testclone(rd) }
    }

    @Test
    fun boxableTest() {
        expect(jmdictNoKanji) { Boxables.testclone(jmdictNoKanji) }
        expect(jmdict) { Boxables.testclone(jmdict) }
    }

    @Test
    fun testSenseBoxing() {
        val sense = JMDictEntry.Sense(
                setOf("bla"),
                HashSet(Arrays.asList("a", "b", "c")),
                EnumSet.of(DictCode.adjno, DictCode.chem),
                EnumSet.of(DictCode.uk, DictCode.uK),
                "kvakykvak",
                Gloss().add(Language.ENGLISH, "bla"),
                listOf(JMDictEntry.LoanWord("foo", Language.DUTCH, true, false)),
                "dialect",
                listOf(),
                listOf())
        // doesn't work, the "misc" field is not present in the Writable format.
        //		assertEquals(sense, Writables.clone(sense));
        expect(sense) { sense.cloneByBoxing() }
    }

    @Test
    fun testIsMocked() {
        expect(true) { JMDictEntry.isMocked(JMDictEntry.mock("hu").ref) }
    }

    @Test
    fun testMockedBoxingUnboxing() {
        expect(JMDictEntry.mock("foobla").ref) { JMDictEntry.mock("foobla").ref.cloneByBoxing() }
    }

    @Test
    fun getKanjis() {
        expect(setOf(Kanji('鬯'))) { jmdict.getAllKanjis() }
    }

    @Test
    fun loanWordTest() {
        var lw = JMDictEntry.LoanWord(null, Language.ENGLISH, true, true)
        expect(lw) { lw.cloneByBoxing() }
        expect(lw) { lw.cloneBySerialization() }
        expect(Language.ENGLISH) { lw.cloneByBoxing().language }
        lw = JMDictEntry.LoanWord("haha", Language.HUNGARIAN, true, true)
        expect(lw) { lw.cloneByBoxing() }
        expect(lw) { lw.cloneBySerialization() }
        expect(Language.HUNGARIAN) { lw.cloneByBoxing().language }
    }

    @Test
    fun xrefAndFieldTest() {
        var sense = JMDictEntry.Sense(
                setOf("bla"),
                HashSet(Arrays.asList("a", "b", "c")),
                EnumSet.of(DictCode.adjno, DictCode.chem),
                EnumSet.of(DictCode.uk, DictCode.uK),
                "kvakykvak",
                Gloss().add(Language.ENGLISH, "bla"),
                listOf(JMDictEntry.LoanWord("foo", Language.DUTCH, true, false)),
                null,
                listOf(),
                listOf())
        expectList() { sense.cloneByBoxing().xref }
        expectList() { sense.cloneByBoxing().fieldsOfApplication }
        sense = JMDictEntry.Sense(
                setOf("bla"),
                HashSet(Arrays.asList("a", "b", "c")),
                EnumSet.of(DictCode.adjno, DictCode.chem),
                EnumSet.of(DictCode.uk, DictCode.uK),
                "kvakykvak",
                Gloss().add(Language.ENGLISH, "bla"),
                listOf(JMDictEntry.LoanWord("foo", Language.DUTCH, true, false)),
                null,
                listOf("food", "sport"),
                listOf("kanji1", "kanji2"))
        expectList("food", "sport") { sense.cloneByBoxing().fieldsOfApplication }
        expectList("kanji1", "kanji2") { sense.cloneByBoxing().xref }
    }

    companion object {
        private val ACCENT = PitchAccent.parse("カンケンカク", "305", "12000")
        @JvmStatic
        fun dummyNoKanji(readingHiragana: String): JMDictEntry {
            val rd = JMDictEntry.ReadingData(readingHiragana, null, EnumSet.noneOf(DictCode::class.java), true, 5, 10, null, (-1).toByte()).apply { accent = ACCENT }
            return JMDictEntry(emptyList<JMDictEntry.KanjiData>(),
                    listOf(rd),
                    listOf(JMDictEntry.Sense(null, null, null, null, null, Gloss().add(Language.ENGLISH, "bla"), listOf(), null, listOf(), listOf())),
                    null, null)
        }

        @JvmField
        val jmdictNoKanji = dummyNoKanji("re")
        private val rd = JMDictEntry.ReadingData("re", null, EnumSet.noneOf(DictCode::class.java), false, 10, 30, null, (-1).toByte()).apply { accent = ACCENT }
        @JvmField
        val jmdict = JMDictEntry(listOf(JMDictEntry.KanjiData("鬯鬯", false, EnumSet.noneOf(DictCode::class.java), 6, 20, null, (-1).toByte())),
                listOf(rd),
                listOf(JMDictEntry.Sense(null, null, null, null, null, Gloss().add(Language.ENGLISH, "bla").add(Language.ENGLISH, "foo"), listOf(), null, listOf(), listOf()),
                        JMDictEntry.Sense(null, null, null, EnumSet.of(DictCode.INT), null, Gloss().add(Language.ENGLISH, "bar"), listOf(), null, listOf(), listOf())
                        ),
                2f, JLPTLevel.N5)
    }
}
