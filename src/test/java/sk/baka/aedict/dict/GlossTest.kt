/*
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.dict

import org.junit.jupiter.api.Test
import sk.baka.aedict.util.Boxables
import sk.baka.aedict.util.Language
import kotlin.test.expect

class GlossTest {

    @Test
    fun testIso23Codes() {
        val g = Gloss()
        g.add(Language("en"), "content")
        expect(1) { g.get(Language("eng")).size }
        g.add(Language("eng"), "bla")
        expect(2) { g.get(Language("en")).size }
    }

    @Test
    fun testMultipleLanguages() {
        val g = Gloss()
        g.add(Language("en"), "content")
        g.add(Language("en"), "content2")
        g.add(Language("ja"), "foo")
        expect(2) { g.get(Language.ENGLISH).size }
        expect(1) { g.get(Language.JAPANESE).size }
        expect(2) { g.getAndPreferLang(Language.ENGLISH).size }
        expect(1) { g.getAndPreferLang(Language.JAPANESE).size }
        expect(2) { g.getAndPreferLang(Language.GERMAN).size }
    }

    @Test
    fun testBox() {
        val g = Gloss()
        g.add(Language("en"), "content")
        g.add(Language("en"), "content2")
        g.add(Language("ja"), "foo")
        expect(g) { Boxables.testclone(g) }
    }

    /**
     * https://github.com/mvysny/aedict/issues/555
     */
    @Test
    fun testWordDistanceFromStart() {
        val g = Gloss()
        // toto je heslo "chigau"
        g.add(Language("en"), "to differ (from), to vary")
        g.add(Language("en"), "to not be in the usual condition")
        g.add(Language("en"), "to not match the correct (answer, etc.)")
        g.add(Language("en"), "to be different from promised")
        expect(0) { g.getWordDistanceFromStart("to") as Int }
        expect(0) { g.getWordDistanceFromStart("to", "correct") as Int }
        expect(16) { g.getWordDistanceFromStart("correct") as Int }
        expect(null) { g.getWordDistanceFromStart("notpresent") }
        g.add(Language("ja"), "hu hu correct")
        expect(2) { (g.getWordDistanceFromStart("correct") as Int) }
        expect(0) { (g.getWordDistanceFromStart("hu", "correct") as Int) }
        expect(null) { g.getWordDistanceFromStart("notpresent") }
    }
}
