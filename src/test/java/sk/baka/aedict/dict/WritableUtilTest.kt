/*
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.dict

import org.junit.jupiter.api.Test
import sk.baka.aedict.util.Writables
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.ObjectInputStream
import java.io.ObjectOutputStream
import java.util.*
import kotlin.test.expect

class WritableUtilTest {
    private fun testEnumSetSerialization(set: EnumSet<DictCode>?) {
        val bout = ByteArrayOutputStream()
        val oout = ObjectOutputStream(bout)
        Writables.writeEnumSet(set, oout)
        oout.close()
        val deserialized = Writables.readEnumSet(DictCode::class.java, ObjectInputStream(ByteArrayInputStream(bout.toByteArray())))
        expect(set) { deserialized }
    }

    @Test
    fun testEnumSetSerialization() {
        testEnumSetSerialization(null)
        val set = EnumSet.noneOf(DictCode::class.java)
        testEnumSetSerialization(set)
        for (i in 0..DictCode.values().size - 1) {
            set.add(DictCode.values()[i])
            testEnumSetSerialization(set)
        }
    }
}
