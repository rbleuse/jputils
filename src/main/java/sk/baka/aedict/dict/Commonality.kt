/**
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.dict

/**
 * Computes word commonality using various algorithms.
 * @author mvy
 */
abstract class Commonality {

    /**
     * Returns the maximum commonality returned by [getCommonality].
     * @return 1 .. [Short.MAX_VALUE]
     */
    abstract val maxCommonality: Short

    /**
     * Computes the word commonality.
     * @param kana the word, should contain kanji/kana only. Not null.
     * @return the commonality, 0..[maxCommonality]; 0= most common.
     */
    abstract fun getCommonality(kana: String): Short

    /**
     * @param kana the word, should contain kanji/kana only. Not null.
     * @return if false, this commonality computation algorithm cannot accurately compute value for given string and will return
     * [maxCommonality]
     */
    abstract fun supports(kana: String): Boolean

    /**
     * Returns the commonality of given entry (a minimum of commonalities of kanjis (or readings if no kanjis are present) as a floating point number, 0..1.
     * @param kanji the kanjis, not null
     * @param reading the readings, not null
     * @return commonality 0..1, 0=most common, 1=least common.
     */
    fun getCommonalityFloat(kanji: List<JMDictEntry.KanjiData>, reading: List<JMDictEntry.ReadingData>): Float {
        return getCommonalityFloat2(kanji.map { it.kanji }, reading.map { it.reading })
    }

    /**
     * Returns the commonality of given entry (a minimum of commonalities of kanjis (or readings if no kanjis are present) as a floating point number, 0..1.
     * @param kanji the kanjis, not null. Should only contain kana.
     * @param reading the readings, not null. Should only contain kana.
     * @return commonality 0..1, 0=most common, 1=least common.
     */
    fun getCommonalityFloat2(kanji: List<String>, reading: List<String>): Float {
        val c = getCommonality2(kanji, reading).toInt()
        val d = c.toFloat() / maxCommonality
        return d.coerceAtMost(1f)
    }

    fun supports(kanji: List<JMDictEntry.KanjiData>, reading: List<JMDictEntry.ReadingData>): Boolean =
            supports2(kanji.map { it.kanji }, reading.map { it.reading })

    /**
     * Checks whether this implementation can compute the commonality of given entry.
     * @param kanji the kanjis, not null. Should only contain kana.
     * @param reading the readings, not null. Should only contain kana.
     * @return if false, this commonality computation algorithm cannot accurately compute value for given input and will return
     * [maxCommonality]
     */
    open fun supports2(kanji: List<String>, reading: List<String>): Boolean {
        require(kanji.isNotEmpty() || reading.isNotEmpty()) { "Parameter reading: invalid value $reading: both kanji and reading is empty" }
        if (kanji.any { supports(it) }) return true
        if (kanji.isEmpty() && reading.any { supports(it) }) return true
        return false
    }

    /**
     * Computes a commonality of given entry - a minimum of commonalities of kanjis (or readings if no kanjis are present).
     * @param kanji the kanjis, not null
     * @param reading the readings, not null
     * @return the commonality, 0..[.getMaxCommonality]; 0= most common.
     */
    fun getCommonality(kanji: List<JMDictEntry.KanjiData>, reading: List<JMDictEntry.ReadingData>): Short {
        return getCommonality2(kanji.map { it.kanji }, reading.map { it.reading })
    }

    /**
     * Computes a commonality of given entry - a minimum of commonalities of kanjis (or readings if no kanjis are present).
     * @param kanji the kanjis, not null. Should only contain kana.
     * @param reading the readings, not null. Should only contain kana.
     * @return the commonality, 0..[maxCommonality]; 0= most common.
     */
    fun getCommonality2(kanji: List<String>, reading: List<String>): Short {
        require(kanji.isNotEmpty() || reading.isNotEmpty()) { "Parameter reading: invalid value $reading: both kanji and reading is empty" }
        var commonality = java.lang.Short.MAX_VALUE.toInt()
        for (kanjiData in kanji) {
            commonality = commonality.coerceAtMost(getCommonality(kanjiData).toInt())
        }
        if (kanji.isEmpty()) {
            for (readingData in reading) {
                commonality = commonality.coerceAtMost(getCommonality(readingData).toInt())
            }
        }
        commonality = commonality.coerceAtMost(maxCommonality.toInt())
        require(commonality >= 0) { "Commonality cannot be less than 0: $commonality" }
        return commonality.toShort()
    }
}
