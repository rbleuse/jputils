/**
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.dict;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;

/**
 * WARNING: ordinal used for Boxing. Don't reshuffle enum constants, don't delete
 * enum constants, add new constants only at the bottom!!!! Don't rename constants - names
 * are written in the Lucene index.
 */
public enum DictCode {
    MA("MA","martial arts term", "Martial Arts"),
    X("X", "rude or X-rated term (not displayed in educational software)", "Rude/XXX"),
    abbr("abbr", "abbreviation", "Abbr"),
    adji("adj-i", "adjective (keiyoushi)", "Adjective"),
    adjna("adj-na", "adjectival nouns or quasi-adjectives (keiyodoshi)", "Adj-na"),
    adjno("adj-no", "nouns which may take the genitive case particle 'no'", "Adj-no"),
    adjpn("adj-pn", "pre-noun adjectival (rentaishi)", "Rentaishi"),
    adjt("adj-t", "'taru' adjective", "Taru Adj"),
    adjf("adj-f", "noun or verb acting prenominally", "Prenominal"),
    adj("adj", "former adjective classification (being removed)", "Adjective"),
    adv("adv", "adverb (fukushi)", "Adverb"),
    advto("adv-to", "adverb taking the 'to' particle", "Adverb+to"),
    arch("arch", "archaism", "Archaic"),
    ateji("ateji", "ateji (phonetic) reading", "Ateji"),
    aux("aux", "auxiliary", "Auxiliary"),
    auxv("aux-v", "auxiliary verb", "Auxiliary Verb"),
    auxadj("aux-adj", "auxiliary adjective", "Auxiliary Adj"),
    buddh("Buddh", "Buddhist term", "Buddhist"),
    chem("chem", "chemistry term", "Chemistry"),
    chn("chn", "children's language", "Children Term"),
    col("col", "colloquialism", "Colloquialism"),
    comp("comp", "computer terminology", "Computer"),
    conj("conj", "conjunction", "Conjunction"),
    ctr("ctr", "counter", "Counter"),
    derog("derog", "derogatory", "Derogatory"),
    eK("eK", "exclusively kanji", "Kanji Only"),
    ek("ek", "exclusively kana", "Kana Only"),
    exp("exp", "expressions (phrases, clauses, etc.)", "Expressions"),
    fam("fam", "familiar language", "Familiar"),
    fem("fem", "female term or language", "Female"),
    food("food", "food term", "Food"),
    geom("geom", "geometry term", "Geometry"),
    gikun("gikun", "gikun (meaning as reading) or jukujikun (special kanji reading)", "Gikun/Jukujikun"),
    hon("hon", "honorific or respectful (sonkeigo) language", "Honorific"),
    hum("hum", "humble (kenjougo) language", "Humble"),
    iK("iK", "word containing irregular kanji usage", "Unusual"),  // tu bolo Unusual Kanji Usage, ale je to uvedene pri kanji, takze to je jasne ;)
    id("id","idiomatic expression", "Idiomatic"),
    ik("ik", "word containing irregular kana usage", "Unusual Kana"),
    INT("int", "interjection (kandoushi)", "Interjection"),
    io("io", "irregular okurigana usage", "Irregular Okurigana"),
    iv("iv", "irregular verb", "Irregular"),
    ling("ling", "linguistics terminology", "Linguistic"),
    msl("m-sl", "manga slang", "Manga Slang"),
    male("male", "male term or language", "Male Term"),
    malesl("male-sl", "male slang", "Male Slang"),
    math("math", "mathematics", "Math"),
    mil("mil", "military", "Military"),
    n("n", "noun (common) (futsuumeishi)", "Noun"),
    nadv("n-adv", "adverbial noun (fukushitekimeishi)", "Adverbial"),
    nsuf("n-suf", "noun, used as a suffix", "Noun, suffix"),
    npref("n-pref", "noun, used as a prefix", "Noun, prefix"),
    nt("n-t", "noun (temporal) (jisoumeishi)", "Noun"),
    num("num", "numeric", "Numeric"),
    oK("oK", "word containing out-dated kanji or kanji usage", "Outdated Kanji"),
    obs("obs", "obsolete term", "Obsolete"),
    obsc("obsc", "obscure term", "Obscure"),
    ok("ok", "out-dated or obsolete kana usage", "Obsolete"),
    oik("oik", "old or irregular kana form", true, true, "Old/Irregular"),
    onmim("on-mim", "onomatopoeic or mimetic word", "Onomatopoia"),
    pn("pn", "pronoun", "Pronoun"),
    poet("poet", "poetical term", "Poetical"),
    pol("pol", "polite (teineigo) language", "Polite"),
    pref("pref", "prefix", "Prefix"),
    proverb("proverb", "proverb", "Proverb"),
    prt("prt", "particle", "Particle"),
    physics("physics", "physics terminology", "Physics"),
    rare("rare", "rare", "Rare"),
    sens("sens", "sensitive", "Sensitive"),
    sl("sl", "slang", "Slang"),
    suf("suf", "suffix", "Suffix"),
    uK("uK", "word usually written using kanji alone", "Kanji-only"),
    uk("uk", "word usually written using kana alone", "Kana-only"),
    v1("v1", "Ichidan verb", "Ichidan verb"),
    v2as("v2a-s", "Nidan verb with 'u' ending (archaic)", "Nidan -u"),
    v4h("v4h", "Yodan verb with 'hu/fu' ending (archaic)", "Yodan -hu/-fu"),
    v4r("v4r", "Yodan verb with 'ru' ending (archaic)", "Yodan -ru"),
    v5("v5", "Godan verb (not completely classified)", "Godan Verb"),
    v5aru("v5aru", "Godan verb - -aru special class", "Godan -aru"),
    v5b("v5b", "Godan verb with 'bu' ending", "Godan -bu"),
    v5g("v5g", "Godan verb with 'gu' ending", "Godan -gu"),
    v5k("v5k", "Godan verb with 'ku' ending", "Godan -ku"),
    v5ks("v5k-s", "Godan verb - Iku/Yuku special class", "Godan iku/yuku"),
    v5m("v5m", "Godan verb with 'mu' ending", "Godan -mu"),
    v5n("v5n", "Godan verb with 'nu' ending", "Godan -nu"),
    v5r("v5r", "Godan verb with 'ru' ending", "Godan -ru"),
    v5ri("v5r-i", "Godan verb with 'ru' ending (irregular verb)", "Godan -ru irregular"),
    v5s("v5s", "Godan verb with 'su' ending", "Godan -su"),
    v5t("v5t", "Godan verb with 'tsu' ending", "Godan -tsu"),
    v5u("v5u", "Godan verb with 'u' ending", "Godan -u"),
    v5us("v5u-s", "Godan verb with 'u' ending (special class)", "Godan -u special"),
    v5uru("v5uru", "Godan verb - Uru old class verb (old form of Eru)", "Godan uru"),
    v5z("v5z", "Godan verb with 'zu' ending", "Godan -zu"),
    vz("vz", "Ichidan verb - zuru verb (alternative form of -jiru verbs)", "Ichidan zuru"),
    vi("vi", "intransitive verb", "Intransitive"),
    vk("vk", "Kuru verb - special class", "Kuru"),
    vn("vn", "irregular nu verb", "-nu Verb"),
    vr("vr", "irregular ru verb, plain form ends with -ri", "-ru Verb"),
    vs("vs", "noun or participle which takes the aux. verb suru", "Noun -suru"),
    vsc("vs-c", "su verb - precursor to the modern suru", "Verb su"),
    /**
     * Nouns ending with suru, e.g. 面suru
     */
    vss("vs-s", "suru verb - special class", "Suru"),
    /**
     * The "suru" verb only.
     */
    vsi("vs-i", "suru verb - included", "Suru incl."),
    kyb("kyb", "Kyoto-ben", "Kyoto-ben"),
    osb("osb", "Osaka-ben", "Osaka-ben"),
    ksb("ksb", "Kansai-ben", "Kansai-ben"),
    ktb("ktb", "Kantou-ben", "Kantou-ben"),
    tsb("tsb", "Tosa-ben", "Tosa-ben"),
    thb("thb", "Touhoku-ben", "Touhoku-ben"),
    tsug("tsug", "Tsugaru-ben", "Tsugaru-ben"),
    kyu("kyu", "Kyuushuu-ben", "Kyuushuu-ben"),
    rkb("rkb", "Ryuukyuu-ben", "Ryuukyuu-ben"),
    nab("nab", "Nagano-ben", "Nagano-ben"),
    vt("vt", "transitive verb", "Transitive"),
    vulg("vulg", "vulgar expression or word", "Vulgar"),
    adjKari("adj-kari", "'kari' adjective (archaic)", "kari Adj"),
    adjku("adj-ku", "'ku' adjective (archaic)", "ku Adj"),
    adjshiku("adj-shiku", "'shiku' adjective (archaic)", "shiku Adj"),
    adjnari("adjnari", "archaic/formal form of na-adjective", "Adj -na archaic"),
    npr("n-pr", "proper noun", "Proper Noun"),
    vunspec("v-unspec", "verb unspecified", "Verb"),
    v4k("v4k", "Yodan verb with 'ku' ending (archaic)", "Yodan -ku"),
    v4g("v4g", "Yodan verb with 'gu' ending (archaic)", "Yodan -gu"),
    v4s("v4s", "Yodan verb with 'su' ending (archaic)", "Yodan -su"),
    v4t("v4t", "Yodan verb with 'tsu' ending (archaic)", "Yodan -tsu"),
    v4n("v4n", "Yodan verb with 'nu' ending (archaic)", "Yodan -nu"),
    v4b("v4b", "Yodan verb with 'bu' ending (archaic)", "Yodan -bu"),
    v4m("v4m", "Yodan verb with 'mu' ending (archaic)", "Yodan -mu"),
    v2kk("v2k-k", "Nidan verb (upper class) with 'ku' ending (archaic)", "Nidan -ku"),
    v2gk("v2g-k","Nidan verb (upper class) with 'gu' ending (archaic)", "Nidan -gu"),
    v2tk("v2t-k", "Nidan verb (upper class) with 'tsu' ending (archaic)", "Nidan -tsu"),
    v2dk("v2d-k", "Nidan verb (upper class) with 'dzu' ending (archaic)", "Nidan -dzu"),
    v2hk("v2h-k", "Nidan verb (upper class) with 'hu/fu' ending (archaic)", "Nidan -hu/-fu"),
    v2bk("v2b-k", "Nidan verb (upper class) with 'bu' ending (archaic)", "Nidan -bu"),
    v2mk("v2m-k", "Nidan verb (upper class) with 'mu' ending (archaic)", "Nidan -mu"),
    v2yk("v2y-k", "Nidan verb (upper class) with 'yu' ending (archaic)", "Nidan -yu"),
    v2rk("v2r-k", "Nidan verb (upper class) with 'ru' ending (archaic)", "Nidan -ru"),
    v2ks("v2k-s", "Nidan verb (lower class) with 'ku' ending (archaic)", "Nidan -ku"),
    v2gs("v2g-s", "Nidan verb (lower class) with 'gu' ending (archaic)", "Nidan -gu"),
    v2ss("v2s-s", "Nidan verb (lower class) with 'su' ending (archaic)", "Nidan -su"),
    v2zs("v2z-s", "Nidan verb (lower class) with 'zu' ending (archaic)", "Nidan -zu"),
    v2ts("v2t-s", "Nidan verb (lower class) with 'tsu' ending (archaic)", "Nidan -tsu"),
    v2ds("v2d-s", "Nidan verb (lower class) with 'dzu' ending (archaic)", "Nidan -dzu"),
    v2ns("v2n-s", "Nidan verb (lower class) with 'nu' ending (archaic)", "Nidan -nu"),
    v2hs("v2h-s", "Nidan verb (lower class) with 'hu/fu' ending (archaic)", "Nidan -hu/-fu"),
    v2bs("v2b-s", "Nidan verb (lower class) with 'bu' ending (archaic)", "Nidan -bu"),
    v2ms("v2m-s", "Nidan verb (lower class) with 'mu' ending (archaic)", "Nidan -mu"),
    v2ys("v2y-s", "Nidan verb (lower class) with 'yu' ending (archaic)", "Nidan -yu"),
    v2rs("v2r-s", "Nidan verb (lower class) with 'ru' ending (archaic)", "Nidan -ru"),
    v2ws("v2w-s", "Nidan verb (lower class) with 'u' ending and 'we' conjugation (archaic)", "Nidan -u/we"),
    archit("archit", "architecture term", "Architecture"),
    anat("anat", "anatomical term", "Anatomical"),
    astron("astron", "astronomy, etc. term", "Astronomy"),
    baseb("baseb", "baseball term", "Baseball"),
    biol("biol", "biology term", "Biology"),
    bot("bot", "botany term", "Botany"),
    bus("bus", "business term", "Business"),
    econ("econ", "economics term", "Economics"),
    engr("engr", "engineering term", "Engineering"),
    finc("finc", "finance term", "Finance"),
    geol("geol", "geology, etc. term", "Geology"),
    law("law", "law, etc. term", "Law"),
    med("med", "medicine, etc. term", "Medicine"),
    music("music", "music term", "Music"),
    Shinto("Shinto", "Shinto term", "Shinto"),
    sports("sports", "sports term", "Sports"),
    sumo("sumo", "sumo term", "Sumo"),
    zool("zool", "zoology term", "Zoology"),
    joc("joc", "jocular, humorous term", "Humor"),
    surname("surname", "family or surname", false, true, "Surname"), // jmnedict
    place("place", "place name", false, true, "Place name"), // jmnedict
    unclass("unclass", "unclassified name", false, true, null), // jmnedict
    company("company", "company name", false, true, "Company Name"),
    product("product", "product name", false, true, "Product Name"), // jmnedict
    masc("masc", "male given name or forename", false, true, "Male Given Name"), // jmnedict
    fem2("fem", "female given name or forename", false, true, "Female Given Name"), // jmnedict
    person("person", "full name of a particular person", false, true, "Full Name"), // jmnedict
    given("given", "given name or forename, gender not specified", false, true, "Given Name"), // jmnedict
    station("station", "railway station", false, true, "Railway Station"), // jmnedict
    organization("organization", "organization name", false, true, "Organization Name"), // jmnedict
    unc("unc", "unclassified", "Unclassified"),
	work("work", "work of art, literature, music, etc. name", false, true, "Art"), // jmnedict
	v1s("v1-s", "Ichidan verb - kureru special class", "Ichidan kureru"),
	copda("cop-da", "copula", "Copula"),
	adjix("adj-ix", "adjective (keiyoushi) - yoi/ii class", "Yoi/ii Adj"),
	yoji("yoji", "yojijukugo", "Yojijukugo"),
    quote("quote", "quotation", "Quote"),
    christn("christn", "Christian term", "Christian Term"),
    netsl("net-sl", "Internet slang", "Internet Slang"),
    dated("dated", "dated term", "Dated Term"),
    hist("hist", "historical term", "Historical Term"),
    lit("lit", "literary or formal term", "Literary or Formal Term"),
    char_("char", "character", "Character"), //jmnedict
    creat("creat", "creature", "Creature"), //jmnedict
    dei("deity", "deity", "Deity"), //jmnedict
    ev("ev", "event", "Event"), //jmnedict
    fict("fict", "fiction", "Fiction"), //jmnedict
    leg("leg", "legend", "Legend"), //jmnedict
    myth("myth", "mythology", "Mythology"), //jmnedict
    obj("obj", "object", "Object"), //jmnedict
    oth("oth", "other", "Other"), //jmnedict
    relig("relig", "religion", "Religion"), //jmnedict
    serv("serv", "service", "Service"), //jmnedict
    rK("rK", "rarely-used kanji form", "Rare Kanji Form"),
    form("form", "formal or literary term", "Formal Term"),
    group("group", "group", "Group"), //jmnedict
    ;
    /**
     * Short code.
     */
	@NotNull
    public final String jmdictCode;
    /**
     * The value as it's present in the JMDict xml.
     */
	@NotNull
    public final String jmdictXml;

    DictCode(@NotNull String jmdictCode, @NotNull String jmdictXml, @Nullable String badge) {
        this(jmdictCode, jmdictXml, true, false, badge);
    }
    private final boolean presentInJMDict;
    private final boolean presentInJMNEDict;
    DictCode(@NotNull String jmdictCode, @NotNull String jmdictXml, boolean presentInJMDict, boolean presentInJMNEDict, @Nullable String badge) {
        this.jmdictCode = jmdictCode;
        this.jmdictXml = jmdictXml;
        this.presentInJMNEDict = presentInJMNEDict;
        this.presentInJMDict = presentInJMDict;
        // vs is technically not a verb - it will only become one after -suru is added.
        // vs is a noun. If I add it here, then filtering by nouns in Aedict will
        // cause Aedict to show that the search is also done by verbs ;)
        this.verb = isGodanVerb() || isYodanVerb() || isNidanVerb() || isIchidanVerb() || isSuruVerb() || isKuruVerb()
            || jmdictCode.equals("iv") || jmdictCode.equals("vt") || jmdictCode.equals("vi")
            || jmdictCode.equals("vn") || jmdictCode.equals("vr");
        this.adjective = jmdictCode.startsWith("adj");
        this.badge = badge;
    }

	/**
	 * Returns true if this dictionary code denotes a godan verb.
	 * @return true for godan verb.
	 */
    public boolean isGodanVerb() {
        return jmdictCode.startsWith("v5");
    }

    public boolean isYodanVerb() {
        return jmdictCode.startsWith("v4");
    }

    public boolean isNidanVerb() {
        return jmdictCode.startsWith("v2");
    }

    /**
     * True if this is a godan, yodan, nidan, ichidan, suru or kuru verb.
     */
    public final boolean verb;

    /**
     * True if this is an adjective.
     */
    public final boolean adjective;

    public static final Set<DictCode> NOUNS = EnumSet.copyOf(Arrays.asList(DictCode.adjno, DictCode.adjf, DictCode.n,
            DictCode.nadv, DictCode.nsuf, DictCode.npref, DictCode.nt, DictCode.pn, DictCode.vs, DictCode.npr));
    public static final Set<DictCode> VERBS;
    public static final Set<DictCode> GODAN_VERBS;
    public static final Set<DictCode> YODAN_VERBS;
    public static final Set<DictCode> NIDAN_VERBS;
    public static final Set<DictCode> ICHIDAN_VERBS;
    public static final Set<DictCode> ADJECTIVES;
    public static final Set<DictCode> I_ADJECTIVES = Collections.singleton(DictCode.adji);
    public static final Set<DictCode> NA_ADJECTIVES = Collections.singleton(DictCode.adjna);
    static {
        {
            final Set<DictCode> verbs = EnumSet.noneOf(DictCode.class);
            for (DictCode dictCode : DictCode.values()) {
                if (dictCode.verb) {
                    verbs.add(dictCode);
                }
            }
            VERBS = Collections.unmodifiableSet(verbs);
        }
        {
            final Set<DictCode> godanverbs = EnumSet.noneOf(DictCode.class);
            for (DictCode dictCode : DictCode.values()) {
                if (dictCode.isGodanVerb()) {
                    godanverbs.add(dictCode);
                }
            }
            GODAN_VERBS = Collections.unmodifiableSet(godanverbs);
        }
        {
            final Set<DictCode> yodanverbs = EnumSet.noneOf(DictCode.class);
            for (DictCode dictCode : DictCode.values()) {
                if (dictCode.isYodanVerb()) {
                    yodanverbs.add(dictCode);
                }
            }
            YODAN_VERBS = Collections.unmodifiableSet(yodanverbs);
        }
        {
            final Set<DictCode> nidanverbs = EnumSet.noneOf(DictCode.class);
            for (DictCode dictCode : DictCode.values()) {
                if (dictCode.isNidanVerb()) {
                    nidanverbs.add(dictCode);
                }
            }
            NIDAN_VERBS = Collections.unmodifiableSet(nidanverbs);
        }
        {
            final Set<DictCode> verbs = EnumSet.noneOf(DictCode.class);
            for (DictCode dictCode : DictCode.values()) {
                if (dictCode.isIchidanVerb()) {
                    verbs.add(dictCode);
                }
            }
            ICHIDAN_VERBS = Collections.unmodifiableSet(verbs);
        }
        {
            final Set<DictCode> verbs = EnumSet.noneOf(DictCode.class);
            for (DictCode dictCode : DictCode.values()) {
                if (dictCode.adjective) {
                    verbs.add(dictCode);
                }
            }
            ADJECTIVES = Collections.unmodifiableSet(verbs);
        }
    }

	/**
	 * Returns true if this dictionary code denotes an ichidan verb.
	 * @return true for ichidan verb.
	 */
    public boolean isIchidanVerb() {
        // don't compare with 'this' - called in the constructor and the static constants are not yet initialized.
        return jmdictCode.equals("v1") || jmdictCode.equals("vz") || jmdictCode.equals("v1-s");
    }
	/**
	 * Returns true if this dictionary code denotes a suru verb.
	 * @return true for suru verb.
	 */
    public boolean isSuruVerb() {
        // don't compare with 'this' - called in the constructor and the static constants are not yet initialized.
        return jmdictCode.equals("vs-i") || jmdictCode.equals("vs-s");
    }
	/**
	 * Returns true if this dictionary code denotes a kuru verb.
	 * @return true for kuru verb.
	 */
    public boolean isKuruVerb() {
        // don't compare with 'this' - called in the constructor and the static constants are not yet initialized.
        return jmdictCode.equals("vk");
    }

    private static final Map<String, DictCode> JMDICT_XML_CODES = new HashMap<String, DictCode>();
    private static final Map<String, EnumSet<DictCode>> JMDICT_CODES = new HashMap<String, EnumSet<DictCode>>();
    static{
        for (DictCode dictCode : DictCode.values()) {
            EnumSet<DictCode> codes = JMDICT_CODES.get(dictCode.jmdictCode);
            if (codes == null) {
                codes = EnumSet.noneOf(DictCode.class);
                JMDICT_CODES.put(dictCode.jmdictCode, codes);
            }
            if (JMDICT_XML_CODES.put(dictCode.jmdictXml, dictCode) != null) {
                throw new RuntimeException("Non-unique XML value: " + dictCode.jmdictXml);
            }
            codes.add(dictCode);
        }
    }

	@Nullable
	public static DictCode fromJmdictCodeNull(@NotNull String jmdictCode, boolean fromJmdict, boolean fromJmnedict) {
        EnumSet<DictCode> codes = JMDICT_CODES.get(jmdictCode);
        if (codes != null) {
            for (DictCode dc: codes) {
                if ((fromJmdict && dc.presentInJMDict) || (fromJmnedict && dc.presentInJMNEDict)) {
                    return dc;
                }
            }
        }
        return null;
    }
	@NotNull
    public static DictCode fromJmdictCode(@NotNull String jmdictCode, boolean fromJmdict, boolean fromJmnedict) {
        final DictCode result = fromJmdictCodeNull(jmdictCode, fromJmdict, fromJmnedict);
        if (result == null) {
            throw new IllegalArgumentException("Parameter jmdictCode: invalid value '" + jmdictCode + "': not present in the enum");
        }
        return result;
    }

    /**
     * Warning: JMDict XML doesn't reference dictionary codes with short codes such as `tsug`
     * - instead the XML entities are expanded to a full description of the code.
     * @param xmlCode {@link DictCode#jmdictXml}.
     * @return enum instance, never null.
     */
	@NotNull
    public static DictCode fromJmdictXml(@NotNull String xmlCode) {
        final DictCode result = JMDICT_XML_CODES.get(xmlCode);
        if (result == null) {
            throw new IllegalArgumentException("Parameter xmlCode: invalid value '" + xmlCode + "': not present in the enum");
        }
        return result;
    }

    @Nullable
    private final String badge;

    /**
     * A short displayable badge.
     * @return badge
     */
    @NotNull
    public String getBadge() {
        // jmdictCode - must not be long otherwise the badges won't look pretty in Aedict.
        return badge != null ? badge : jmdictCode;
    }

    /**
     * A full description of the dictionary code.
     * @return
     */
    @NotNull
    public String getDescription() {
        if (this == yoji) {
            // as requested by the user.
            return "yojijukugo (四字熟語), a four-character idiomatic compound";
        }
        return jmdictXml;
    }
}
