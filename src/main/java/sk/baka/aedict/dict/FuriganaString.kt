/**
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.dict

import sk.baka.aedict.util.contains
import sk.baka.aedict.util.intersection
import java.io.Serializable

/**
 * A string with possible furigana annotations. Immutable.
 *
 * Use [raw] to obtain text without annotations; use [format] to format the string.
 *
 * #### Implementation detail
 * Composed of a stream of tokens, for example
 * parsing `foo{bar;baz}foo` would produce three tokens:
 * * A token with text `foo` and null `ruby`
 * * A token with text `bar` and ruby `baz`
 * * A token with text `foo` and null `ruby`
 */
class FuriganaString(private val tokens: List<Token>) : CharSequence, Serializable {
    data class Token(val ruby: String?, val text: String) : Serializable {
        fun normalize() = if (ruby == text) Token(null, text) else this
        fun substring(startIndex: Int, endIndex: Int) = Token(ruby, text.substring(startIndex, endIndex))
    }

    /**
     * The text without annotations; e.g. for "foo{bar;baz}" this will return "foobar". This is what [toString] will return.
     *
     * Concatenated values of [Token.text].
     */
    val raw: String by lazy(LazyThreadSafetyMode.NONE) { tokens.joinToString("") { it.text } }

    /**
     * Formats this furigana string using given [formatter].
     */
    fun format(formatter: RubyFurigana.Formatter = RubyFurigana.FORMATTER_FURIGANA_VIEW) = buildString {
        for (token in tokens) {
            if (token.ruby == null) {
                append(token.text)
            } else {
                formatter.format(token.text, token.ruby, this)
            }
        }
    }

    override fun toString() = raw

    override val length: Int
        get() = raw.length

    override fun get(index: Int): Char = raw[index]

    override fun subSequence(startIndex: Int, endIndex: Int): FuriganaString {
        require(startIndex <= length) { "startIndex: $startIndex must be $length or less" }
        require(endIndex <= length) { "endIndex: $endIndex must be $length or less" }
        val tokens = mutableListOf<Token>()
        val indexRange = startIndex until endIndex
        if (indexRange.isEmpty()) return FuriganaString.EMPTY
        if (indexRange == indices) return this

        var currentIndex = 0
        for (token in this.tokens) {
            val tokenIndexRange = currentIndex until (currentIndex + token.text.length)
            val intersection = tokenIndexRange.intersection(indexRange)
            if (intersection.isEmpty()) {
                if (tokenIndexRange.start >= indexRange.endInclusive) {
                    break
                } else {
                    // startIndex is way behind this token, poll next token
                }
            } else if (indexRange.contains(tokenIndexRange)) {
                tokens.add(token)
            } else {
                tokens.add(token.substring(intersection.start - currentIndex, intersection.endInclusive + 1 - currentIndex))
            }
            currentIndex += token.text.length
        }
        return FuriganaString(tokens)
    }

    /**
     * Returns a normalized string - merges adjacent tokens which have null [Token.ruby] null. Doesn't merge annotated tokens by default!
     * @param mergeAnnotatedTokens if true, also tokens with [Token.ruby] non-null will be merged. Defaults to false.
     */
    fun normalized(mergeAnnotatedTokens: Boolean = false): FuriganaString {
        if (tokens.isEmpty()) return this
        val tokens = this.tokens.map { it.normalize() }

        val acc = mutableListOf<Token>()
        for (oldToken in tokens) {
            val last = acc.lastOrNull()
            if (last == null) {
                acc.add(oldToken)
            } else {
                if (last.ruby == null && oldToken.ruby == null) {
                    acc.removeAt(acc.size - 1)
                    acc.add(Token(null, last.text + oldToken.text))
                } else if (mergeAnnotatedTokens && last.ruby != null && oldToken.ruby != null) {
                    acc.removeAt(acc.size - 1)
                    acc.add(Token(last.ruby + oldToken.ruby, last.text + oldToken.text))
                } else {
                    acc.add(oldToken)
                }
            }
        }
        return FuriganaString(acc)
    }

    operator fun plus(other: String) = when {
        other.isEmpty() -> this
        isEmpty() -> ofUnannotated(other)
        else -> FuriganaString(tokens + Token(null, other)).normalized()
    }

    operator fun plus(other: FuriganaString) = when {
        other.isEmpty() -> this
        isEmpty() -> other
        else -> FuriganaString(tokens + other.tokens).normalized()
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        other as FuriganaString
        if (hashCode() != other.hashCode()) return false
        if (tokens != other.tokens) return false
        return true
    }

    private val hashCode: Int by lazy { tokens.hashCode() }

    override fun hashCode() = hashCode

    companion object {
        val EMPTY = FuriganaString(listOf())

        /**
         * Produces string with one token with null [Token.ruby].
         */
        fun ofUnannotated(string: String) = FuriganaString(listOf(FuriganaString.Token(null, string)))

        /**
         * Parses a ruby-formatted [string] back into the tokenized representation.
         */
        fun parse(string: String, formatter: RubyFurigana.Formatter): FuriganaString {
            val tokens = mutableListOf<Token>()
            var index = 0
            while (index < string.length) {
                val parsed = formatter.tryParse(string.substring(index))
                if (parsed == null) {
                    tokens.add(Token(null, string.substring(index..index)))
                    index++
                } else {
                    tokens.add(parsed.first)
                    index += parsed.second
                }
            }
            return FuriganaString(tokens).normalized()
        }

        /**
         * Takes an index in the furigana expression and translates it to the index into
         * the furigana-less sentence. For example:
         * * furigana expression `foo{bar;baz}` index 5 points to the 'a' character in bar;
         * the result is therefore 4 (furigana-less sentence is `foobar`).
         * @param furiganaString unparsed furigana string in the form of [sk.baka.aedict.dict.RubyFurigana.FORMATTER_FURIGANA_VIEW]
         * @param furiganaIndex index in the [furiganaString]
         */
        fun furiganaIndexToRawIndex(furiganaString: String, furiganaIndex: Int): Int {
            var result = 0
            var inFurigana = false
            var inFuriganaReading = false
            for (i in furiganaString.indices) {
                if (furiganaIndex == i) break;
                val char = furiganaString[i]
                if (char == '{') {
                    inFurigana = true
                    inFuriganaReading = false
                } else if (char == '}') {
                    inFurigana = false
                    inFuriganaReading = false
                }
                if (char == ';' && inFurigana) {
                    inFuriganaReading = true
                }
                if (!inFuriganaReading && char != '{' && char != '}' && char != ';') {
                    result++
                }
            }
            return result
        }
    }

    fun prepend(prefix: String): FuriganaString = when {
        prefix.isEmpty() -> this
        isEmpty() -> ofUnannotated(prefix)
        else -> ofUnannotated(prefix) + this
    }
}
