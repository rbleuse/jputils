/**
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.dict.download

import org.slf4j.LoggerFactory
import sk.baka.aedict.util.*
import sk.baka.aedict.util.typedmap.Box

import java.io.DataInput
import java.io.DataOutput
import java.io.IOException
import java.io.Serializable
import java.util.*

/**
 * A dictionary metadata. Immutable.
 * @property indexZipFileSize The size of the index zip file, in bytes.
 * @property indexFilesSize The size of unpacked zip file [indexZipFileSize]. The dictionary will use this much space when unpacked on the Android device.
 * @property sourceMD5 md5 of the unpacked source file.
 * @property sourceIndexedAt The source dictionary file was indexed at this time (UTC). Not null.
 * @property dictionaryDisplayableName Displayable dictionary name, for example "Computing &amp; Telecommunications dictionary"
 * @property copyright Copyright which needs to be displayed before the dictionary can be installed.
 * @property homepage The dictionary homepage. Must be a well-formed URL link which a browser can open, e.g. `https://something/foo`
 * @property id unique identifier of this dictionary, within the scope of the Aedict app.
 * @property commercial If false, this is a freely available dictionary such as Kanjidic, JMDict and such.
 * If true, this is a paid dictionary, for example EPWing.
 * @author mvy
 */
data class DictionaryMeta(@JvmField val indexZipFileSize: Long, @JvmField val indexFilesSize: Long, @JvmField val sourceMD5: MD5, @JvmField val sourceIndexedAt: Date,
                     @JvmField val dictionaryDisplayableName: String,
                     @JvmField val copyright: String?,
                     @JvmField val homepage: String?,
                     @JvmField val id: DictionaryID,
                     @JvmField val commercial: Boolean) : Writable, Comparable<DictionaryMeta>, Boxable, Serializable {

    fun isNewerThan(other: DictionaryMeta): Boolean {
        checkID(other.id)
        return sourceIndexedAt.after(other.sourceIndexedAt)
    }

    private fun checkID(id: DictionaryID) {
        require(id == this.id) { "Parameter id: invalid value $id: this dictionary is ${this.id}" }
    }

    fun hasSameSourceMD5As(other: DictionaryMeta): Boolean {
        checkID(other.id)
        return sourceMD5 == other.sourceMD5
    }

    override fun compareTo(other: DictionaryMeta) = id.compareTo(other.id)

    /**
     * General dictionary class. Every class's usage is different and
     * has a particular Lucene Index format.
     *
     * WARNING: new enum constants needs to be added at the bottom, because of Box.
     */
    enum class DictionaryClass {
        /**
         * EDICT od Jima Breena (alebo aj JMdict). Tento typ slovniku obsahuje japonske vyrazy s anglickymi (a dalsimi) prekladmi.
         */
        JMDICT,
        /**
         * Kanjidic alebo kanjidic2, obsahuje zoznam kanji s udajmi o nich (SKIP kody, radikaly, atd).
         */
        KANJIDIC2,
        /**
         * Vzorove japonske vety. bud z TANAKA alebo z TATOEBA (tatoeba uz zahrna tanaku a obsahuje viac viet, takze indexujem tatoeba).
         */
        EXAMPLE_SENTENCES,
        /**
         * SOD kanji drawing order images.
         */
        SOD
    }

    /**
     * unique identifier of this dictionary, within the scope of the Aedict app. Immutable.
     * @property clazz the dictionary class. Not null.
     * @property subclass Dictionary subclass, may be null. For [DictionaryClass.JMDICT]:
     * if the subclass is null, this is the default JMDict dictionary from Jim Breen.
     * If not null, this is an advanced additional dictionary, e.g. COMPDICT.
     * The [DictionaryClass.KANJIDIC2] does not have any subclasses, there's just
     * one dictionary, the Kanjidic from Jim Breen.
     */
    data class DictionaryID(@JvmField val clazz: DictionaryClass, @JvmField val subclass: String?) : Writable, Serializable, Comparable<DictionaryID>, Boxable {

        /**
         * @return string in the form of clazz/subclass (or clazz only, if subclass is null).
         */
        override fun toString() = "$clazz${if (subclass != null) "/" + subclass else ""}"

        @Throws(IOException::class)
        override fun writeTo(out: DataOutput) {
            out.writeByte(clazz.ordinal)
            Writables.writeNullableUTF(subclass, out)
        }

        override fun box(): Box {
            return Box()
                    .putByte("class", clazz.ordinal.toByte())
                    .putString("subclass", subclass)
        }

        override fun compareTo(other: DictionaryID) = compareValuesBy(this, other, { it.clazz }, { it.subclass })

        companion object : Unboxable<DictionaryID>, Readable<DictionaryID> {

            /**
             * Parses the ID back from the form produced by [toString].
             * @param str string in the form of clazz/subclass (or clazz only, if subclass is null).
             * @return parsed ID, not null.
             */
            @JvmStatic fun parse(str: String): DictionaryID {
                val split = str.trim().split('/')
                require(split.isNotEmpty()) { "Parameter str: invalid value $str: blank?" }
                require(split.size <= 2) { "Parameter str: invalid value $str: too many slashes" }
                return DictionaryID(DictionaryClass.valueOf(split[0]), if (split.size == 2) split[1] else null)
            }

            /**
             * Kanjidic from Jim Breen.
             */
            @JvmField val KANJIDIC_JB = DictionaryID(DictionaryClass.KANJIDIC2, null)
            /**
             * Standardny EDICT from Jim Breen.
             */
            @JvmField val EDICT_JB = DictionaryID(DictionaryClass.JMDICT, null)
            /**
             * JMNEDICT from Jim Breen.
             */
            @JvmField val JMNEDICT = DictionaryID(DictionaryClass.JMDICT, "jmnedict")
            @JvmField val SUBCLASS_TANAKA = "tanaka"
            @JvmField val SUBCLASS_TATOEBA = "tatoeba"
            /**
             * Tanaka example sentences. Deprecated by Tatoeba.
             */
            @JvmField val TANAKA = DictionaryID(DictionaryClass.EXAMPLE_SENTENCES, SUBCLASS_TANAKA)
            /**
             * Tatoeba example sentences.
             */
            @JvmField val TATOEBA = DictionaryID(DictionaryClass.EXAMPLE_SENTENCES, SUBCLASS_TATOEBA)

            /**
             * SOD Stroke Order Diagrams.
             */
            @JvmField val SOD = DictionaryID(DictionaryClass.SOD, null)
            /**
             * SOD Stroke Order Diagrams as SVG paths instead of PNGs.
             */
            @JvmField val SOD_SVG = DictionaryID(DictionaryClass.SOD, "svg")

            /**
             * Kotowaza japanese proverbs, see http://english.kotowaza.org/
             */
            @JvmField val KOTOWAZA = DictionaryID(DictionaryMeta.DictionaryClass.EXAMPLE_SENTENCES, "kotowaza")

            @Throws(IOException::class)
            @JvmStatic override fun readFrom(di: DataInput): DictionaryID {
                val clazz = DictionaryClass.values()[di.readByte().toInt()]
                val subclass = Writables.readNullableUTF(di)
                return DictionaryID(clazz, subclass)
            }

            @JvmStatic override fun unbox(box: Box): DictionaryID {
                return DictionaryID(
                        DictionaryClass.values()[box.get("class").byteValue().get().toInt()],
                        box.get("subclass").string().orNull()
                )
            }
        }
    }

    init {
        require(dictionaryDisplayableName.isNotBlank())
        require(indexFilesSize >= 0) { "Parameter indexFilesSize: invalid value $indexFilesSize: must be zero or greater" }
        // might be 0 when the metadata is packed into the zip file - at that time we don't know the zip file size :-)
        require(indexZipFileSize >= 0) { "Parameter indexZipFileSize: invalid value $indexZipFileSize: must be zero or greater" }
    }

    @Throws(IOException::class)
    override fun writeTo(out: DataOutput) {
        out.writeByte(VERSION.toInt())
        box().writeTo(out)
    }

    override fun box(): Box {
        return Box()
                .putLong("indexZipFileSize", indexZipFileSize)
                .putLong("indexFilesSize", indexFilesSize)
                .putBoxable("sourceMD5", sourceMD5)
                .putLong("sourceIndexedAt", sourceIndexedAt.time)
                .putString("dictionaryDisplayableName", dictionaryDisplayableName)
                .putString("copyright", copyright)
                .putString("homepage", homepage)
                .putBoxable("id", id)
                .putBoolean("commercial", commercial)
    }

    /**
     * Zoznam DictionaryMeta, immutable.
     * @property dictionaries Zoznam slovnikov, not null.
     */
    data class DictionaryMetaList(val dictionaries: ArrayList<DictionaryMeta>) : Writable, Boxable, Serializable {
        val jmDict: Set<DictionaryMeta>
            get() = findByClass(DictionaryClass.JMDICT)

        @Throws(IOException::class)
        override fun writeTo(out: DataOutput) {
            out.writeByte(VERSION.toInt())
            box().writeTo(out)
            // v0
            //            Writables.writeListOfWritables(dictionaries, out);
        }

        override fun box(): Box {
            return Box().putListOfBoxables("dictionaries", dictionaries)
        }

        fun toMap(): MutableMap<DictionaryID, DictionaryMeta> = HashMap(dictionaries.associateBy { it.id })

        /**
         * Returns a list of dictionaries which can be upgraded from [remote]. Expects that `this` holds the local set of dictionaries.
         * @param remote remote metadata, not null.
         * @return a list of upgradable dictionaries from the [remote] list, may be empty.
         */
        fun getUpgradable(remote: DictionaryMetaList): List<DictionaryMeta> {
            val map = toMap()
            val ids = map.keys
            val otherMap = remote.toMap()
            ids.retainAll(otherMap.keys)
            val result = ArrayList<DictionaryMeta>()
            for (id in ids) {
                val mLocal = map[id]!!
                val mRemote = otherMap[id]!!
                if (mRemote.isNewerThan(mLocal)) {
                    result.add(mRemote)
                }
            }
            return result
        }

        /**
         * Tries to load dictionary of given class. Fallbacks to default dictionary, then to first available dictionary.
         * @param clazz the class, not null.
         * @param preferredSubclasses a possibly empty list of preferred subclasses
         * @return location of the dictionary file, may be null if no dictionary is available.
         */
        fun findFirst(clazz: DictionaryMeta.DictionaryClass, vararg preferredSubclasses: String?): DictionaryMeta? {
            val dicts = toMap()
            log.info("Searching for " + clazz + "/" + Arrays.toString(preferredSubclasses) + " from available dictionaries: " + dicts.keys)
            for (preferredSubclass in preferredSubclasses) {
                val result = dicts[DictionaryMeta.DictionaryID(clazz, preferredSubclass)]
                if (result != null) {
                    log.info("Found " + result)
                    return result
                }
            }
            // try default dictionary
            var result: DictionaryMeta? = dicts[DictionaryMeta.DictionaryID(clazz, null)]
            if (result != null) {
                log.info("Found " + result)
                return result
            }
            // try any dictionary
            for (id in dicts.keys) {
                if (id.clazz == clazz) {
                    result = dicts[id]
                    log.info("Found " + result!!)
                    return result
                }
            }
            log.info("No dictionary found, returning null")
            return null
        }

        /**
         * Returns a new list with given dictionary added/updated.
         * @param meta dictionary meta to be set over old one, not null.
         * @return new list with given meta overwriting the old one.
         */
        fun set(meta: DictionaryMeta): DictionaryMetaList {
            val map = toMap()
            map.put(meta.id, meta)
            return DictionaryMetaList(ArrayList(map.values))
        }

        /**
         * Returns a new list with given dictionaries added/updated.
         * @param other dictionary meta to be set over the old one, not null.
         * @return new list with given meta overwriting the old one.
         */
        fun setAll(other: DictionaryMetaList): DictionaryMetaList {
            val map = toMap()
            for (meta in other.dictionaries) {
                map.put(meta.id, meta)
            }
            return DictionaryMetaList(ArrayList(map.values))
        }

        /**
         * Counts summary of packed size of dictionaries with given ID.
         * @param ids IDs, not null. All IDs must be present in this list otherwise [NullPointerException] is thrown.
         * @return sum of [DictionaryMeta.indexZipFileSize]
         */
        fun getZipFileSizes(ids: Set<DictionaryID>): Long =
                dictionaries.filter { ids.contains(it.id) } .sumByLong { it.indexZipFileSize }

        /**
         * Counts summary of unpacked size of dictionaries with given ID.
         * @param ids IDs, not null. All IDs must be present in this list otherwise [NullPointerException] is thrown.
         * @return sum of [DictionaryMeta.indexFilesSize]
         */
        fun getFilesSizes(ids: Set<DictionaryID>): Long =
                dictionaries.filter { ids.contains(it.id) } .sumByLong { it.indexFilesSize }

        operator fun get(id: DictionaryID): DictionaryMeta? = dictionaries.find { it.id == id }

        fun findByClass(clazz: DictionaryClass): Set<DictionaryMeta> =
                dictionaries.filter { it.id.clazz == clazz } .toSet()

        /**
         * Returns DictionaryMeta without given dictionary.
         * @param id the ID, not null.
         * @return novy list bez daneho slovnika.
         */
        fun markDeleted(id: DictionaryID): DictionaryMetaList {
            val map = toMap()
            return if (map.remove(id) == null) this else DictionaryMetaList(ArrayList(map.values))
        }

        operator fun contains(id: DictionaryID) = dictionaries.any { it.id == id }

        /**
         * Retains all non-commercial dictionaries; removes all commercial dicts not present in given set.
         * @param allowedCommercialDicts the set, may be empty.
         * @return the new meta with several dictionaries removed.
         */
        fun removeDisallowedCommercialDicts(allowedCommercialDicts: Set<DictionaryID>): DictionaryMetaList {
            val metas = ArrayList(dictionaries)
            val i = metas.iterator()
            while (i.hasNext()) {
                val meta = i.next()
                if (meta.commercial && !allowedCommercialDicts.contains(meta.id)) {
                    i.remove()
                }
            }
            return DictionaryMetaList(metas)
        }

        companion object : Unboxable<DictionaryMetaList>, Readable<DictionaryMetaList> {
            /**
             * Prazdny meta list.
             */
            @JvmStatic val EMPTY = DictionaryMetaList(ArrayList())
            // nedvihat verziu, Box je dopredu kompatibilny.
            private val VERSION: Byte = 1

            @Throws(IOException::class)
            @JvmStatic override fun readFrom(di: DataInput): DictionaryMetaList {
                val version = di.readByte()
                // box je dopredu kompatibilny.
                //            if (version > VERSION) {
                //                throw new IllegalArgumentException("Parameter version: invalid value " + version + ": exceeds supported version " + VERSION);
                //            }
                if (version >= 1) {
                    return unbox(Box.readFrom(di))
                }
                val list = Writables.readListOfWritables(DictionaryMeta::class.java, di)!!
                return DictionaryMetaList(ArrayList(list))
            }

            @JvmStatic override fun unbox(box: Box): DictionaryMetaList {
                return DictionaryMetaList(ArrayList(box.get("dictionaries").boxables(DictionaryMeta::class.java).get()))
            }
        }
    }

    override fun toString(): String {
        return "{" + id + ", " + indexFilesSize + "b (" +
                "zipped " + indexZipFileSize + "b)" +
                ", sourceMD5 " + sourceMD5 +
                ", indexed at " + sourceIndexedAt +
                ", dictionaryDisplayableName='" + dictionaryDisplayableName + '\'' +
                ", copyright='" + copyright + '\'' +
                ", homepage='" + homepage + '\'' +
                ", " + (if (commercial) "commercial" else "free") +
                '}'
    }

    companion object : Readable<DictionaryMeta>, Unboxable<DictionaryMeta> {
        // don't increase the version, Box is forward-compatible.
        private val VERSION: Byte = 1

        @Throws(IOException::class)
        @JvmStatic override fun readFrom(di: DataInput): DictionaryMeta {
            val version = di.readByte()
            // Box is forward-compatible
            //        if (version > VERSION) {
            //            throw new IllegalArgumentException("Parameter version: invalid value " + version + ": max " + VERSION + " supported");
            //        }
            if (version >= 1) {
                return unbox(Box.readFrom(di))
            }
            val indexZipFileSize = di.readLong()
            val indexFilesSize = di.readLong()
            val sourceMD5 = MD5.readFrom(di)
            val sourceIndexedAt = Date(di.readLong())
            val dictionaryDisplayableName = di.readUTF()
            val copyright = Writables.readNullableUTF(di)
            val homepage = Writables.readNullableUTF(di)
            val id = DictionaryID.readFrom(di)
            return DictionaryMeta(indexZipFileSize, indexFilesSize, sourceMD5, sourceIndexedAt, dictionaryDisplayableName, copyright, homepage, id, false)
        }

        @JvmStatic override fun unbox(box: Box): DictionaryMeta {
            return DictionaryMeta(
                    box.get("indexZipFileSize").longValue().get(),
                    box.get("indexFilesSize").longValue().get(),
                    box.get("sourceMD5").boxable(MD5::class.java).get(),
                    Date(box.get("sourceIndexedAt").longValue().get()),
                    box.get("dictionaryDisplayableName").string().get(),
                    box.get("copyright").string().orNull(),
                    box.get("homepage").string().orNull(),
                    box.get("id").boxable(DictionaryID::class.java).get(),
                    box.get("commercial").bool().or(false)
            )
        }

        private val log = LoggerFactory.getLogger(DictionaryMeta::class.java)
    }
}
