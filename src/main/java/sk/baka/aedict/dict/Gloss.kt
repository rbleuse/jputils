/**
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.dict

import sk.baka.aedict.util.*
import sk.baka.aedict.util.typedmap.Box

import java.io.*
import java.util.*

/**
 * A list of english (+ additional) translations of arbitrary japanese entry.
 * The original Gloss data was a group of one meaning translated to multiple languages,
 * however Jim Breen changed the source data. Now, the translations of a single meaning
 * is split into multiple Glosses.
 *
 * See https://github.com/mvysny/aedict/issues/795 for more info.
 */
class Gloss : Writable, Serializable, Boxable {

    val glosses: MutableMap<Language, MutableList<String>> = HashMap()

    /**
     * Returns all glosses, space-separated.
     * @return string, never null.
     */
    val spaceSeparatedGlosses: String
        get() = glosses.values.flatten().joinToString(" ")

    /**
     * Retrieves all glosses for all languages.
     * @return never null, may be empty.
     */
    val all: List<String>
        get() = glosses.values.flatten()

    /**
     * Get a list of languages present in this gloss.
     * @return a set of languages, never null, may be empty.
     */
    val languages: Set<Language>
        get() = glosses.keys

    /**
     * Returns true if this gloss does not contain any translations at all.
     * @return
     */
    val isEmpty: Boolean
        get() = glosses.isEmpty()

    /**
     * Returns gloss for given language. Returns null if there is no such language present in this gloss.
     * @param language the language, not null.
     * @return list of languages, may be null.
     */
    fun getNull(language: Language): List<String>? = glosses[language]

    /**
     * Returns gloss for given language. Fails if there is no such language present in this gloss.
     * @param language the language, not null.
     * @return list of languages, never null.
     */
    operator fun get(language: Language): List<String> =
            glosses[language] ?: throw IllegalArgumentException("Parameter language: invalid value $language: no such sense available. Available language: ${glosses.keys}")

    /**
     * Adds new gloss.
     * @param lang    the lang, not null.
     * @param content the gloss contents, not null.
     * @return this
     */
    fun add(lang: Language, content: String): Gloss {
        glosses.getOrPut(lang) { mutableListOf() } .add(content)
        return this
    }

    @Throws(IOException::class)
    override fun writeTo(out: DataOutput) {
        out.writeByte(glosses.size)
        for (lang in glosses.keys) {
            out.writeUTF(lang.lang3)
            Writables.writeListOfStrings(glosses[lang], out)
        }
    }

    override fun box(): Box {
        val box = Box()
        val langs = HashSet<String>(glosses.size)
        for (lang in glosses.keys) {
            langs.add(lang.lang3)
        }
        box.putListOfBasics("LANGS", langs)
        for (lang in glosses.keys) {
            box.putListOfBasics(lang.lang3, glosses[lang])
        }
        return box
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false
        val gloss = other as Gloss?
        return glosses == gloss!!.glosses

    }

    override fun hashCode(): Int = glosses.hashCode()

    override fun toString() = glosses.toString()

    /**
     * Returns requested glosses, optionally from given language.
     * @param lang if not null, prefer this language.
     * @return glosses, never null, may be empty.
     */
    fun getAndPreferLang(lang: Language?): List<String> {
        var result: List<String>? = null
        if (lang != null) {
            result = getNull(lang)
        }
        if (result == null) {
            result = getNull(Language.ENGLISH)
        }
        if (result == null && !glosses.isEmpty()) {
            result = glosses.values.firstOrNull()
        }
        return result ?: emptyList()
    }

    /**
     * Adds new glosses.
     * @param lang    the lang, not null.
     * @param keywords the gloss contents, not null, may be empty.
     * @return this
     */
    fun addAll(lang: Language, keywords: Collection<String>): Gloss {
        for (keyword in keywords) {
            add(lang, keyword)
        }
        return this
    }

    fun addAll(other: Gloss): Gloss {
        for ((key, value) in other.glosses) {
            addAll(key, value)
        }
        return this
    }

    /**
     * From all [glosses], finds a translation with fewest words preceding any
     * of given [words].
     * @param words LOWER-CASE WORDS!!!!
     * @return 0 or more if there is a translation containing given word;
     * null if none of given [words] are present in [glosses].
     */
    fun getWordDistanceFromStart(vararg words: String): Int? {
        return getWordDistanceFromStart(HashSet(Arrays.asList(*words)), Integer.MAX_VALUE)
    }

    /**
     * From all [glosses], finds a translation with fewest words preceding any
     * of given [words].
     * @param words LOWER-CASE WORDS!!!!
     * @param stopAfter only consider this many preceding words at most.
     * @return 0 or more if there is a translation containing given word;
     * null if none of given [words] are present in [glosses].
     */
    fun getWordDistanceFromStart(words: Set<String>, stopAfter: Int): Int? {
        if (words.isEmpty()) {
            throw IllegalArgumentException("Parameter words: invalid value $words: must not be empty")
        }
        var min = stopAfter
        for (texts in glosses.values) {
            val wordDistanceFromStart = getWordDistanceFromStart(texts, words, min)
            if (wordDistanceFromStart != null) {
                min = min.coerceAtMost(wordDistanceFromStart)
            }
        }
        return if (min >= stopAfter) null else min
    }

    private fun getWordDistanceFromStart(texts: List<String>, words: Set<String>, stopAfter: Int): Int? {
        var wordsEncountered = 0
        for (text in texts) {
            val w = text.toLowerCase().splitByWhitespaces()
            for (s in w) {
                if (words.contains(s)) {
                    return wordsEncountered
                }
                wordsEncountered++
                if (wordsEncountered >= stopAfter) {
                    return null
                }
            }
        }
        return null
    }

    companion object : Unboxable<Gloss>, Readable<Gloss> {

        @JvmStatic
        override fun readFrom(di: DataInput): Gloss {
            val meaningsCount = di.readByte().toInt()
            val result = Gloss()
            for (i in 0 until meaningsCount) {
                result.glosses.put(Language(di.readUTF()), Writables.readListOfStrings(di)!!)
            }
            return result
        }

        @JvmStatic
        override fun unbox(box: Box): Gloss {
            val langs = box.get("LANGS").setOf(String::class.java).get()
            val result = Gloss()
            for (lang in langs) {
                result.glosses.put(Language(lang), box.get(lang).listOf(String::class.java).get())
            }
            return result
        }
    }
}
