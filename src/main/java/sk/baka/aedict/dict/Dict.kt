/**
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.dict

import sk.baka.aedict.kanji.*
import sk.baka.aedict.util.toLinkedSet
import java.util.*
import java.util.regex.Pattern

/**
 * Returns one-liner in form of kanji, kanji, kanji: reading, reading...
 * @param romanization romanization to use, not null.
 * @return one liner, never null.
 */
fun EntryInfo.getJapaneseOneLiner(romanization: IRomanization = IRomanization.IDENTITY) = ref.getJapaneseOneLiner(romanization)

/**
 * Returns one-liner in form of kanji, kanji, kanji: reading, reading...
 * @param romanization romanization to use, not null.
 * @return one liner, never null.
 */
fun IDisplayableEntry.getJapaneseOneLiner(romanization: IRomanization = IRomanization.IDENTITY): String {
    val reading = getJapaneseReading(romanization)
    // niektore radikaly nemaju ziadne citanie...
    val kanji = getJapaneseKanji()
    if (kanji.isBlank()) {
        return if (reading.isBlank()) { "" } else { reading }
    }
    if (reading.isBlank()) {
        return kanji
    }
    return "$kanji: $reading"
}

/**
 * Returns entry with kanji (and kana if it's necessary - e.g. example sentence, jmdict).
 * If the entry has no kanji, returns an empty string.
 * Should not use romanization.
 * @return comma-separated kanji+kana, not null, may be blank. Never returns readings.
 */
fun IDisplayableEntry.getJapaneseKanji(): String = kanjis.joinToString()

/**
 * Returns entry with kanji (and kana if it's necessary - e.g. example sentence, jmdict).
 * If the entry has no kanji, returns an empty string.
 * @return comma-separated kanji+kana, not null, may be blank. Never returns readings.
 */
fun EntryInfo.getJapaneseKanji(): String = ref.getJapaneseKanji()

/**
 * Checks if any kanji or reading matches given globbing string.
 * @param glob globbing string, where '*' matches 0 or many characters, '?' matches 1 character.
 * @return true if the entry matches, false if not.
 */
fun IEntry.matchesJpGlob(glob: String): Boolean {
    return matchesJp(glob.globToPattern())
}

/**
 * Compiles globbing string to pattern. Expects [this] to be the globbing string, where '*' matches 0 or many characters, '?' matches 1 character.
 * @return pattern
 */
fun String.globToPattern(caseInsensitive: Boolean = false): Regex {
    val pattern = mapJoin { c -> when (c) {
        '?' -> "."
        '*' -> ".*"
        else -> Pattern.quote(c.toString()) }
    }
    return if (caseInsensitive) pattern.toRegex(RegexOption.IGNORE_CASE) else pattern.toRegex()
}

/**
 * Checks if any kanji or reading matches given regex pattern.
 * @param pattern the pattern.
 * @return true if the entry matches, false if not.
 */
fun IEntry.matchesJp(pattern: Regex) = kanjis.any { pattern.matches(it) } || readings.any { pattern.matches(it) }

/**
 * A short description which describes the entry.
 * E.g. displayed in the Android Activity title, which displays this entry.
 *
 * For kanjidic it's just a single kanji; for a jmdict entry this is the first kanji
 * or first reading; for example sentences this returns [ExamplesEntry.kanji].
 * @return title
 */
fun IEntry.getTitle(): String = kanjis.elementAtOrElse(0) { readings.elementAtOrElse(0) { "" } }

/**
 * Returns [getJapaneseKanji]. If the kanji is blank, returns [IDisplayableEntry.getJapaneseReading].
 * @param romanization romanization, not null.
 * @return kanji or reading, never null, never empty.
 */
fun IDisplayableEntry.getJapaneseKanjiOrReading(romanization: IRomanization = IRomanization.IDENTITY): String {
    var kanji = getJapaneseKanji()
    if (kanji.isBlank()) {
        kanji = getJapaneseReading(romanization)
    }
    return kanji
}

/**
 * Returns all kanjis present in this entry.
 * @return all kanjis in this entry, each kanji is present exactly once. May be empty.
 */
fun IEntry.getKanjiCharsUnique(): LinkedHashSet<Kanji> = kanjis.map { it.kanjis } .flatten().toLinkedSet()

fun List<EntryRef>.containsKanjidicEntriesOnly() = all { it.kanjidicEntry != null }

/**
 * Returns all kanjis present in this entry.
 * @return all kanjis in this entry, each kanji is present exactly once. May be empty.
 */
fun IEntry.getAllKanjis(): Set<Kanji> = kanjis.flatMap { it.kanjis }.toSet()

/**
 * Returns all kanjis present in this entry.
 * @return all kanjis in this entry, each kanji is present exactly once. May be empty.
 */
fun EntryInfo.getAllKanjis() = ref.getAllKanjis()
