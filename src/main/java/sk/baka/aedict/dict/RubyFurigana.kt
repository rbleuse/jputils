/**
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.dict

import sk.baka.aedict.dict.FuriganaString.Token
import sk.baka.aedict.kanji.Kanji
import sk.baka.aedict.kanji.isKanji
import sk.baka.aedict.kanji.mapKatakanaToHiragana
import sk.baka.aedict.kanji.toKanji
import sk.baka.aedict.util.codePoints
import java.io.Serializable
import java.lang.IllegalArgumentException
import java.lang.UnsupportedOperationException
import java.util.*
import java.util.regex.Pattern

/**
 * An unparsed furigana Japanese word (or an entire sentence), both the kanji version and the reading (if available).
 * It may be possible to guess-assign ruby readings to individual kanjis; use [mapKanjiToReading] to achieve that.
 * It may be possible to guess-convert this into parsed [FuriganaString] - use [toFurigana] to achieve that.
 *
 * @property kanji The word consisting of kanjis and hiragana/katakana reading,
 * may be null if there is no kanji version of the word.
 * @property reading The reading of [kanji], never null, never blank. Must contain
 * no kanjis.
 * @author mvy
 */
data class RubyFurigana(@JvmField val kanji: String?, @JvmField val reading: String) : Serializable {
    init {
        require (reading.isNotBlank()) { "reading is blank" }
    }

    override fun toString() = if (kanji == null) reading else "$kanji[$reading]"

    /**
     * Checks if [entry] contains this furigana:
     * * if [kanji] is not null, entry must contain precisely this kanji, and [reading] must apply for that kanji
     * * if [kanji] is null, entry must not contain any kanjis, and reading must be present in the [entry].
     */
    fun matches(entry: JMDictEntry) = matches(entry.kanji, entry.reading)

    /**
     * Checks if [kanjis]/[readings] match this furigana word:
     * * if [kanji] is not null, [kanjis] must contain precisely this kanji, and the [reading] must apply for that kanji
     * * if [kanji] is null, [kanjis] must be empty and [reading] must be present in [readings].
     */
    fun matches(kanjis: List<JMDictEntry.KanjiData>, readings: List<JMDictEntry.ReadingData>): Boolean {
        val r = readings.firstOrNull { it.reading == reading } ?: return false
        if (kanji != null) {
            if (!kanjis.any { it.kanji == kanji }) {
                return false
            }
            if (r.kanji != null && !r.kanji.contains(kanji)) {
                return false
            }
        } else {
            // no kanji must be present.
            if (!kanjis.isEmpty()) {
                return false
            }
            assert(r.kanji == null)
        }
        return true
    }

    /**
     * Guesses readings for all kanjis.
     *
     * If you need exact instead of guess, use [mapKanjiToReadingExact] or [mapKanjiToReadingExact2].
     * @param greedy if true, greedily assigns most kana to the first kanji. If false, hesitantly tries to assign
     * as few kana as possible to the first kanji.
     * @param forceReading you may force given kanjis to have given readings. The algorithm will then try to detect readings for all other kanjis.
     * @return a map, mapping kanjis present in [kanji] to readings. May return empty map if there are no kanjis or the guess cannot be made.
     * The map is a LinkedHashMap and preserves the kanji ordering.
     */
    @JvmOverloads
    fun mapKanjiToReading(greedy: Boolean, forceReading: Map<Kanji, String> = mapOf()): Map<Kanji, String> {
        if (kanji == null) {
            return emptyMap()
        }
        var result = mapKanjiToReading1(greedy, forceReading)
        if (result.isEmpty()) {
            result = mapKanjiToReading2(greedy, forceReading)
        }
        return result
    }

    /**
     * Doesn't guess the readings: match them exactly. If any kanji is missing, throws an exception
     * If the matching fails because no reading matches, returns an empty map.
     */
    fun mapKanjiToReadingExact2(readings: Map<Kanji, List<String>>): Map<Kanji, String> {
        val result = mutableMapOf<Kanji, String>()
        var r = reading
        for (cp in kanji!!.codePoints) {
            if (cp.isKanji) {
                val kanji = cp.toKanji()
                val krs = readings[kanji] ?: throw IllegalArgumentException("$this: missing reading for $kanji: $readings")
                val kr = krs.firstOrNull { r.startsWith(it) } ?: return emptyMap()
                if (!result.containsKey(kanji)) {
                    result[kanji] = kr
                } else if (result[kanji] != kr) {
                    // two different readings for $kanji: $kr and ${result[kanji]}
                    return emptyMap()
                }
                r = r.substring(kr.length)
            } else {
                if (r.isEmpty() || cp.toChar() != r[0]) return emptyMap()
                r = r.substring(1)
            }
        }
        return result
    }

    /**
     * Doesn't guess the readings:
     */
    fun mapKanjiToReadingExact(readings: Map<Kanji, Kanjidic2Entry>): Map<Kanji, String> =
            mapKanjiToReadingExact2(readings.toListOfReadings())

    /**
     * More accurate, however if the kanji is present 2x in the word and uses
     * a different kana every time, this will match nothing.
     * @param greedy
     * @return If the matching fails because no reading matches, returns an empty map.
     */
    private fun mapKanjiToReading1(greedy: Boolean, forceReading: Map<Kanji, String> = mapOf()): Map<Kanji, String> {
        val result = LinkedHashMap<Kanji, String>()
        val pattern = StringBuilder()
        val groups = HashMap<Kanji, Int>()
        var currentGroup = 1
        for (cp in kanji!!.codePoints) {
            // + je greedy, +? je reluctant, chceme raz greedy raz reluctant, pozri RubyFuriganaTest pre vysvetlenie
            if (cp.isKanji && forceReading.containsKey(cp.toKanji())) {
                pattern.append(Pattern.quote(forceReading[cp.toKanji()]))
            } else if (cp.isKanji) {
                val k = Kanji(cp)
                var group: Int? = groups[k]
                if (group == null) {
                    group = currentGroup++
                    groups[k] = group
                    pattern.append(if (greedy) "($KANA_RANGE+)" else "($KANA_RANGE+?)")
                } else {
                    pattern.append("\\" + group)
                }
            } else if (!cp.isWhitespace()){
                // escape! zacne podporovat podivne znaky v kanji/reading
                pattern.append(Pattern.quote(cp.toChar().toString()))
            }
        }
        val regex = pattern.toString().toRegex()
        val matchResult = regex.matchEntire(reading.filter { !it.isWhitespace() })
        matchResult?.apply {
            for ((kanji, groupIndex) in groups.entries) {
                result.put(kanji, groupValues[groupIndex])
            }
            result.putAll(forceReading)
        }
        return result
    }

    private fun mapKanjiToReading2(greedy: Boolean, forceReading: Map<Kanji, String> = mapOf()): Map<Kanji, String> {
        val result = LinkedHashMap<Kanji, String>()
        val groupToKanji = mutableListOf<Kanji>()
        val pattern = StringBuilder()
        for (cp in kanji!!.codePoints) {
            // + je greedy, +? je reluctant, chceme raz greedy raz reluctant, pozri RubyFuriganaTest pre vysvetlenie
            pattern.append(when {
                cp.isKanji && forceReading.containsKey(cp.toKanji()) -> Pattern.quote(forceReading[cp.toKanji()])
                cp.isKanji -> if (greedy) "($KANA_RANGE+)" else "($KANA_RANGE+?)"
                else -> Pattern.quote(cp.toChar().toString())
            })
            if (cp.isKanji && !forceReading.containsKey(cp.toKanji())) {
                groupToKanji.add(cp.toKanji())
            }
        }
        val p = pattern.toString().toRegex()
        p.matchEntire(reading) ?.let { readingMatcher ->
            check(groupToKanji.size == readingMatcher.groups.size - 1) { "Unexpected: ${groupToKanji.size} and ${readingMatcher.groups.size}" }
            for (i in 1..groupToKanji.size) {
                result.put(groupToKanji[i - 1], readingMatcher.groupValues[i])
            }
            result.putAll(forceReading)
        }
        return result
    }

    /**
     * Guesses: Tries to guess-assign readings to kanjis and produce a properly annotated furigana string. Returns null if that's
     * not possible.
     *
     * If you need exact results, use [toFuriganaExact] or [toFuriganaExact2].
     */
    fun toFurigana(greedy: Boolean, forceReading: Map<Kanji, String> = mapOf()): FuriganaString? {
        if (kanji == null) {
            return FuriganaString.ofUnannotated(reading)
        }
        val kanjisToReadings = mapKanjiToReading(greedy, forceReading)
        if (kanjisToReadings.isEmpty()) return null
        return toFurigana(kanjisToReadings)
    }

    fun toFuriganaExact(readings: Map<Kanji, Kanjidic2Entry>, mergeAnnotatedTokens: Boolean = false): FuriganaString? {
        if (kanji == null) {
            return FuriganaString.ofUnannotated(reading)
        }
        val kanjisToReadings = mapKanjiToReadingExact(readings)
        if (kanjisToReadings.isEmpty()) return null
        return toFurigana(kanjisToReadings, mergeAnnotatedTokens)
    }

    fun toFuriganaExact2(readings: Map<Kanji, List<String>>, mergeAnnotatedTokens: Boolean = false): FuriganaString? {
        if (kanji == null) {
            return FuriganaString.ofUnannotated(reading)
        }
        val kanjisToReadings = mapKanjiToReadingExact2(readings)
        if (kanjisToReadings.isEmpty()) return null
        return toFurigana(kanjisToReadings, mergeAnnotatedTokens)
    }

    /**
     * Produces FuriganaString, with kanjis annotated with given readings. If a reading for any kanji is missing in the
     * map, returns null.
     * @param mergeAnnotatedTokens if true, also tokens with [Token.ruby] non-null will be merged. Defaults to true.
     */
    fun toFurigana(kanjisToReadings: Map<Kanji, String>, mergeAnnotatedTokens: Boolean = true): FuriganaString? {
        if (kanji == null) {
            return FuriganaString.ofUnannotated(reading)
        }
        val tokens = mutableListOf<FuriganaString.Token>()
        for (cp in kanji.codePoints) {
            if (cp.isKanji) {
                val kanji = Kanji(cp)
                val kreading = kanjisToReadings[kanji]
                if (kreading == null) {
                    // moze sa stat, napr. pri ２０歳[はたち], proste to vzdaj
                    return null
                }
                tokens.add(FuriganaString.Token(kreading, kanji.toString()))
            } else {
                // non-kanji obycajny znak bez furigany.
                tokens.add(FuriganaString.Token(null, String(intArrayOf(cp), 0, 1)))
            }
        }

        return FuriganaString(tokens).normalized(mergeAnnotatedTokens)
    }

    /**
     * Formats into furigana.
     */
    interface Formatter {
        /**
         * Formats given non-blank [text] annotated with [ruby] into a string. For example, `FuriganaView` uses the form of `{text;ruby}`.
         */
        fun format(text: CharSequence, ruby: CharSequence, appendTo: StringBuilder)

        /**
         * Tries to parse a ruby furigana starting from the beginning of given [string]. If it fails, just return null.
         * If we succeed, return the parsed furigana token and the number of matching characters in the string.
         */
        fun tryParse(string: String): Pair<FuriganaString.Token, Int>?
    }

    /**
     * Returns the furigana in the form defined by the [formatter]. E.g. for the `FuriganaView` [FORMATTER_FURIGANA_VIEW]: `{kanji;reading}`.
     *
     * WARNING: Uses Greedy Guessing algorithm!
     * @param formatter formats the kanji+reading pair.
     * @return furigana view form, never null, may not contain any {}s and be equal to [reading] in the worst case.
     */
    fun format(formatter: Formatter): String {
        if (kanji == null) {
            return reading
        }
        val furigana = toFurigana(true)
        if (furigana == null) {
            return buildString {
                formatter.format(kanji, reading, this)
            }
        }
        return furigana.format(formatter)
    }

    companion object {

        /**
         * Returns the furigana in the form for the FuriganaView: {kanji;reading}
         */
        @JvmField
        val FORMATTER_FURIGANA_VIEW: Formatter = object : Formatter {
            override fun tryParse(string: String): Pair<FuriganaString.Token, Int>? {
                if (!string.startsWith('{')) return null
                val delimiter = string.indexOf(';', 1)
                if (delimiter < 0) return null
                val closingBracket = string.indexOf('}', delimiter + 1)
                if (closingBracket < 0) return null
                val token = FuriganaString.Token(string.substring(delimiter + 1, closingBracket), string.substring(1, delimiter))
                return token to (closingBracket + 1)
            }

            override fun format(text: CharSequence, ruby: CharSequence, appendTo: StringBuilder) {
                appendTo.append('{').append(text).append(';').append(ruby).append('}')
            }
        }

        /**
         * Returns the furigana in the form for AnkiDroid: `kanji[reading]`. See https://github.com/mvysny/aedict/issues/493 for details.
         *
         * WARNING: [Formatter.tryParse] will fail since it's impossible to parse this unambiguously.
         */
        @JvmField
        val FORMATTER_ANKI: Formatter = object : Formatter {
            override fun format(text: CharSequence, ruby: CharSequence, appendTo: StringBuilder) {
                appendTo.append(text).append('[').append(ruby).append(']')
            }

            override fun tryParse(string: String): Pair<FuriganaString.Token, Int>? =
                    throw UnsupportedOperationException("Cannot parse Anki formatting unambiguously")
        }

        @JvmField
        val FORMATTER_HTML: Formatter = object : Formatter {
            override fun format(text: CharSequence, ruby: CharSequence, appendTo: StringBuilder) {
                appendTo.append("<ruby><rb>").append(text).append("</rb><rp>(</rp><rt>").append(ruby).append("</rt><rp>)</rp></ruby>")
            }

            override fun tryParse(string: String): Pair<FuriganaString.Token, Int>? {
                if (!string.startsWith("<ruby><rb>")) return null
                val delimiter = string.indexOf("</rb><rp>(</rp><rt>", 10)
                if (delimiter < 0) return null
                val closingBracket = string.indexOf("</rt><rp>)</rp></ruby>", delimiter + 19)
                if (closingBracket < 0) return null
                val token = FuriganaString.Token(string.substring(delimiter + 19, closingBracket), string.substring(10, delimiter))
                return token to (closingBracket + 22)
            }
        }

        private const val KANA_RANGE = "[\u3041-\u3096\u30A0-\u30FF]"
    }
}

fun Int.isWhitespace() = this < 0x7FFF && toChar().isWhitespace()

fun Map<Kanji, Kanjidic2Entry>.toListOfReadings(): Map<Kanji, List<String>> =
        mapValues { e ->
            e.value.onyomi.map { it.mapKatakanaToHiragana() } +
                    e.value.kunyomi.filter { !it.contains('-') } .map { it.replaceAfter(".", "").trim('.') } +
                    e.value.namae
        }
