/**
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.dict

import sk.baka.aedict.inflection.VerbDeinflections
import sk.baka.aedict.kanji.IRomanization
import sk.baka.aedict.util.Language

import java.io.Serializable
import java.util.LinkedHashSet

/**
 * Contains an entry and an additional information,
 * such as what kind of deinflection was performed.
 * @property ref The entry, not null.
 * @property deinflections Deinflections used to deinflect this word.
 * May not be null only for jmdict entry search or when the sentence analysis is performed.
 * The deinflection #0 was applied to the original word, then deinflection #1, etc etc until we got
 * the word in base form.
 * @property origin If not null, this is the original word form (either deinflected or not),
 * in the form it appeared in the sentence. If the entry doesn't originate from the sentence analysis,
 * this is null.
 */
class EntryInfo(@JvmField val ref: EntryRef, @JvmField val deinflections: List<VerbDeinflections.Deinflection>?, @JvmField val origin: String?) : Serializable, IDisplayableEntry {

    /**
     * Additional remark.
     */
    @JvmField var remark: String? = null

    /**
     * If this entry originates from the sentence analysis, this
     * is the original position of the word in the sentence.
     */
    @JvmField var analyzedWordPositionInSentence: IntRange? = null

    /**
     * If true, ref was deinflected.
     * @return true if the word was deinflected.
     */
    val isDeinflected: Boolean
        get() = deinflections != null && !deinflections.isEmpty()

    override val kanjis: LinkedHashSet<String>
        get() = ref.kanjis

    override val readings: LinkedHashSet<String>
        get() = ref.readings

    override fun toString(): String =
            (ref.getJapaneseKanjiOrReading(IRomanization.IDENTITY) + ": " + (if (remark == null) "no remark" else remark) + " "
                    + (if (isDeinflected) "deinflected from " else "original: ") + origin + if (!isDeinflected) ": no deinflections" else ": " + deinflections!!)

    override fun getRubyFuriganas(): List<RubyFurigana> = ref.getRubyFuriganas()

    override fun getSenses(): Gloss = ref.getSenses()

    override fun getJapaneseReading(romanization: IRomanization): String = ref.getJapaneseReading(romanization)

    override fun getSenseOneLiner(preferLang: Language?, includeFlags: Boolean): String =
            ref.getSenseOneLiner(preferLang, includeFlags)
}

fun List<EntryRef>.toInfos(): List<EntryInfo> = map { EntryInfo(it, null, null) }
