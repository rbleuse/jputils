/**
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.dict

import sk.baka.aedict.kanji.IRomanization
import sk.baka.aedict.kanji.Kanji
import sk.baka.aedict.kanji.PitchAccent
import sk.baka.aedict.kanji.PitchData
import sk.baka.aedict.util.*
import sk.baka.aedict.util.typedmap.Box

import java.io.DataInput
import java.io.DataOutput
import java.io.IOException
import java.io.Serializable
import java.util.*

/**
 * Captures data of a JMDict entry.
 * @property wordBasedCommonality http://ftp.monash.edu.au/pub/nihongo/00INDEX.html:
 * Another frequency list was created in 2008 by Michiel Kamermans using words from novels. His explanation is:
 * http://www.geocities.jp/ep3797/edict_01.html
 *
 * 0..1. Better than [getKanjiBasedCommonality], always not null in JMDict since 27.5.2014. Null in JMNedict, riverdic, cardic
 * and in all other dicts besides JMDict.
 * @property jlptLevel JLPT level for this word, null if the word does not belong to no JLPT level or it is not known.
 */
class JMDictEntry(kanji: List<KanjiData>, reading: List<ReadingData>, senses: List<Sense>,
                  val wordBasedCommonality: Float?, val jlptLevel: JLPTLevel?) : Serializable, Writable, IDisplayableEntry, Boxable {
    /**
     * The kanji element, or in its absence, the reading element, is
     * the defining component of each entry.
     *
     * The overwhelming majority of entries will have a single kanji
     * element associated with a word in Japanese. Where there are
     * multiple kanji elements within an entry, they will be orthographical
     * variants of the same word, either using variations in okurigana, or
     * alternative and equivalent kanji. Common "mis-spellings" may be
     * included, provided they are associated with appropriate information
     * fields. Synonyms are not included; they may be indicated in the
     * cross-reference field associated with the sense element.
     *
     * This list may be empty. Cannot be modified.
     */
    @JvmField val kanji: List<KanjiData> = kanji.toList()

    val randomSenseLanguage: Language?
        get() = senses.asSequence().map { sense -> sense.gloss.glosses.keys } .flatten().firstOrNull()

    /**
     * Returns all kanjis present in [kanji].
     * @return a list of strings, never null, may be empty.
     */
    override val kanjis: LinkedHashSet<String>
        get() = kanji.map { it.kanji } .toLinkedSet()

    /**
     * Vypocita commonalitu.
     * @return 0 = most common, 1 = least common.
     */
    val commonality: Float
        get() = wordBasedCommonality ?: getKanjiBasedCommonality()

    /**
     * Kanji Reading, never empty. Cannot be modified.
     */
    @JvmField val reading: List<ReadingData> = reading.toList()

    @Transient
    private var common: Boolean? = null

    val isCommon: Boolean
        get() {
            if (common == null) {
                common = kanji.any { it.common } || reading.any { it.common }
            }
            return common!!
        }

    /**
     * The sense element will record the translational equivalent
     * of the Japanese word, plus other related information. Where there
     * are several distinctly different meanings of the word, multiple
     * sense elements will be employed.
     *
     * Never null, never empty. Cannot be modified.
     */
    val senses: List<Sense>

    /**
     * 0..1
     */
    private var kanjiBasedCommonality: Float? = null

    /**
     * Returns union of all dictionary codes present in all kanji data, reading data and senses.
     * @return all dictionary codes, never null, may be empty.
     */
    val allInf: EnumSet<DictCode>
        get() {
            val result = EnumSet.noneOf(DictCode::class.java)
            for (kanjiData in kanji) {
                result.addAll(kanjiData.inf)
            }
            for (readingData in reading) {
                result.addAll(readingData.inf)
            }
            for (sense in senses) {
                if (sense.pos != null) {
                    result.addAll(sense.pos)
                }
            }
            return result
        }

    override val readings: LinkedHashSet<String>
        get() = reading.map { it.reading } .toLinkedSet()

    fun findByKanji(kanji: String): KanjiData? = this.kanji.firstOrNull { it.kanji == kanji }

    fun findByReading(reading: String): ReadingData? = this.reading.firstOrNull { it.reading == reading }

    override fun getSenseOneLiner(preferLang: Language?, includeFlags: Boolean): String {

        fun getSensesForLanguage(lang: Language, includeFlags: Boolean): List<String> {
            val result = ArrayList<String>()
            var prevPosShort: String? = null
            for (sense in senses) {
                if (sense.gloss.glosses[lang] != null) {
                    val list = ArrayList(sense.gloss[lang])
                    if (!list.isEmpty()) {
                        // fixes https://github.com/mvysny/aedict/issues/673
                        if (includeFlags && !sense.inf.isNullOrBlank()) {
                            val last = list.size - 1
                            list[last] = "${list[last]} (${sense.inf.trim()})"
                        }
                        val posShort = sense.getPosShort(this, true)
                        if (includeFlags && posShort.isNotBlank() && posShort != prevPosShort) {
                            list[0] = "$posShort ${list[0]}"
                            prevPosShort = posShort
                        }
                        result.add(list.joinToString())
                    }
                }
            }
            return result
        }

        var result = listOf<String>()
        if (preferLang != null) {
            result = getSensesForLanguage(preferLang, includeFlags)
        }
        if (result.isEmpty()) {
            result = getSensesForLanguage(Language.ENGLISH, includeFlags)
        }
        if (result.isEmpty()) {
            // for wadoku the default language is german
            // fixes https://code.google.com/p/aedict/issues/detail?id=384
            val lang = randomSenseLanguage
            if (lang != null) {
                result = getSensesForLanguage(lang, includeFlags)
            }
        }
        return result.joinToString("; ")
    }

    /**
     * Checks whether given sense is common.
     * @param sense the sense, not null.
     * @return true if this sense is common, false if not.
     */
    fun isCommon(sense: Sense): Boolean {
        if (sense.stagKanji != null && sense.stagKanji.any { findByKanji(it)?.common == true }) return true
        if (sense.stagReading != null && sense.stagReading.any { findByReading(it)?.common == true }) return true
        return isCommon
    }

    /**
     * @property kanji This element will contain a word or short phrase in Japanese
     * which is written using at least one non-kana character (usually kanji,
     * but can be other characters). The valid characters are
     * kanji, kana, related characters such as chouon and kurikaeshi, and
     * in exceptional cases, letters from other alphabets.
     * Never null, never blank.
     * @property common based on the value of the "ke_pri" field.
     * This and the equivalent re_pri field are provided to record
     * information about the relative priority of the entry,  and consist
     * of codes indicating the word appears in various references which
     * can be taken as an indication of the frequency with which the word
     * is used. This field is intended for use either by applications which
     * want to concentrate on entries of  a particular priority, or to
     * generate subset files.
     * The current values in this field are:
     * - news1/2: appears in the "wordfreq" file compiled by Alexandre Girardi
     * from the Mainichi Shimbun. (See the Monash ftp archive for a copy.)
     * Words in the first 12,000 in that file are marked "news1" and words
     * in the second 12,000 are marked "news2".
     * - ichi1/2: appears in the "Ichimango goi bunruishuu", Senmon Kyouiku
     * Publishing, Tokyo, 1998.  (The entries marked "ichi2" were
     * demoted from ichi1 because they were observed to have low
     * frequencies in the WWW and newspapers.)
     * - spec1 and spec2: a small number of words use this marker when they
     * are detected as being common, but are not included in other lists.
     * - gai1/2: common loanwords, based on the wordfreq file.
     * - nfxx: this is an indicator of frequency-of-use ranking in the
     * wordfreq file. "xx" is the number of the set of 500 words in which
     * the entry can be found, with "01" assigned to the first 500, "02"
     * to the second, and so on. (The entries with news1, ichi1, spec1 and
     * gai1 values are marked with a "(P)" in the EDICT and EDICT2
     * files.)
     *
     * Added spec2 because of https://code.google.com/p/aedict/issues/detail?id=278
     * @property inf This is a coded information field related specifically to the
     * orthography of the keb, and will typically indicate some unusual
     * aspect, such as okurigana irregularity.
     * Never null, may be empty.
     * @property kamermansOccurence http://ftp.monash.edu.au/pub/nihongo/00INDEX.html
     * Another frequency list was created in 2008 by Michiel Kamermans using words from novels. His explanation is:
     * http://www.geocities.jp/ep3797/edict_01.html
     *
     * Cislo od 1 do 4088, cim nizsie cislo, tym castejsi vyskyt slova.
     * @property matsushitaOccurence http://www17408ui.sakura.ne.jp/tatsum/English_top_Tatsu.html
     * Cislo od 1 do cca 20302, cim nizsie, tym castejsi vyskyt slova.
     * @property prinf nfxx: this is an indicator of frequency-of-use ranking in the
     * wordfreq file. "xx" is the number of the set of 500 words in which
     * the entry can be found, with "01" assigned to the first 500, "02"
     * to the second, and so on.
     *
     * -1 if [pri] does not contain [Pri.Nf].
     */
    data class KanjiData(@JvmField val kanji: String,
                         @JvmField val common: Boolean, @JvmField val inf: EnumSet<DictCode>,
                         @JvmField val kamermansOccurence: Int?,
                         @JvmField val matsushitaOccurence: Int?,
                         @JvmField val pri: EnumSet<Pri>?,
                         @JvmField val prinf: Byte) : Serializable, Writable, Boxable {

        init {
            require(kanji.isNotBlank())
            require(prinf in -1..99) { "Parameter prinf: invalid value $prinf: must be -1..99" }
        }

        @Throws(IOException::class)
        override fun writeTo(out: DataOutput) {
            // nevymazavat: https://github.com/mvysny/aedict/issues/569
            out.writeUTF(kanji)
            out.writeBoolean(common)
            Writables.writeEnumSet(inf, out)
        }

        override fun box(): Box = Box()
                .putString("kanji", kanji)
                .putBoolean("common", common)
                .putEnumSet("inf", inf)
                .putInt("kamermansOccurence", kamermansOccurence)
                .putInt("matsushitaOccurence", matsushitaOccurence)
                .putEnumSet("pri", pri)
                .putByte("prinf", if (prinf.toInt() == -1) null else prinf)

        override fun toString() = ("$kanji${if (common) " common " else ""}${if (inf.isEmpty()) "" else inf} kam=$kamermansOccurence mat=$matsushitaOccurence pri=$pri prinf=$prinf")

        companion object : Readable<KanjiData>, Unboxable<KanjiData> {

            @JvmStatic @Throws(IOException::class)
            override fun readFrom(di: DataInput): KanjiData {
                // nevymazavat: https://github.com/mvysny/aedict/issues/569
                val kanji = di.readUTF()
                val common = di.readBoolean()
                val inf = Writables.readEnumSet(DictCode::class.java, di)!!
                return KanjiData(kanji, common, inf, null, null, null, (-1).toByte())
            }

            @JvmStatic
            override fun unbox(box: Box): KanjiData = KanjiData(box.get("kanji").string().get(),
                    box.get("common").bool().get(),
                    box.get("inf").enumSet(DictCode::class.java).get(),
                    box.get("kamermansOccurence").integer().orNull(),
                    box.get("matsushitaOccurence").integer().orNull(),
                    box.get("pri").enumSet(Pri::class.java).orNull(),
                    box.get("prinf").byteValue().or((-1).toByte())
            )
        }
    }

    /**
     * @property reading this element content is restricted to kana and related
     * characters such as chouon and kurikaeshi. Kana usage will be
     * consistent between the keb and reb elements; e.g. if the keb
     * contains katakana, so too will the reb.
     * Never null, never blank.
     * @property kanji This element is used to indicate when the reading only applies
     * to a subset of the keb elements in the entry. In its absence, all
     * readings apply to all kanji elements. The contents of this element
     * must exactly match those of one of the keb elements. ([KanjiData.kanji].
     * May be null. If null, this reading applies to all kanjis. If empty, does not apply to any kanji
     * (the `re_nokanji` element, fixes https://github.com/mvysny/aedict/issues/754 ).
     * @property common See [KanjiData.common].
     * @property kamermansOccurence http://ftp.monash.edu.au/pub/nihongo/00INDEX.html
     * Another frequency list was created in 2008 by Michiel Kamermans using words from novels. His explanation is:
     * http://www.geocities.jp/ep3797/edict_01.html
     *
     * Cislo od 1 do 4088, cim nizsie cislo, tym castejsi vyskyt slova.
     * @property matsushitaOccurence http://www17408ui.sakura.ne.jp/tatsum/English_top_Tatsu.html
     * Cislo od 1 do cca 20302, cim nizsie, tym castejsi vyskyt slova.
     * @property inf This is a coded information field related specifically to the
     * orthography of the keb, and will typically indicate some unusual
     * aspect, such as okurigana irregularity.
     * May be empty.
     * @property prinf nfxx: this is an indicator of frequency-of-use ranking in the
     * wordfreq file. "xx" is the number of the set of 500 words in which
     * the entry can be found, with "01" assigned to the first 500, "02"
     * to the second, and so on.
     *
     * -1 if [pri] does not contain [Pri.Nf].
     */
    data class ReadingData(@JvmField val reading: String,
                           @JvmField val kanji: Set<String>?, @JvmField val inf: EnumSet<DictCode>,
                           @JvmField val common: Boolean,
                           @JvmField val kamermansOccurence: Int?,
                           @JvmField val matsushitaOccurence: Int?,
                           @JvmField val pri: EnumSet<Pri>?,
                           @JvmField val prinf: Byte) : Serializable, Writable, Boxable {
        var accent: PitchAccent? = null
        init {
            require(reading.isNotBlank())
            require(prinf in -1..99) { "Parameter prinf: invalid value $prinf: must be -1..99" }
        }

        @Throws(IOException::class)
        override fun writeTo(out: DataOutput) {
            // nevymazavat: https://github.com/mvysny/aedict/issues/569
            out.writeUTF(reading)
            Writables.writeListOfStrings(kanji, out)
            Writables.writeEnumSet(inf, out)
            out.writeBoolean(common)
        }

        override fun box(): Box = Box()
                .putString("reading", reading)
                .putListOfBasics("kanji", kanji)
                .putEnumSet("inf", inf)
                .putBoolean("common", common)
                .putInt("kamermansOccurence", kamermansOccurence)
                .putInt("matsushitaOccurence", matsushitaOccurence)
                .putBoxable("accent", accent?.pitchData)
                .putEnumSet("pri", pri)
                .putByte("prinf", if (prinf.toInt() == -1) null else prinf)

        override fun toString() =
                ("$reading${if (kanji != null) " of " + kanji else ""}${if (common) " common " else ""}${if (inf.isEmpty()) "" else inf} kam=$kamermansOccurence mat=$matsushitaOccurence accent=$accent pri=$pri prinf=$prinf")

        companion object : Readable<ReadingData>, Unboxable<ReadingData> {

            @JvmStatic @Throws(IOException::class)
            override fun readFrom(di: DataInput): ReadingData {
                // nevymazavat: https://github.com/mvysny/aedict/issues/569
                val reading = di.readUTF()
                val kanji = Writables.readSetOfStrings(di)
                val inf = Writables.readEnumSet(DictCode::class.java, di)!!
                val common = di.readBoolean()
                return ReadingData(reading, kanji, inf, common, null, null, null, (-1).toByte())
            }

            @JvmStatic
            override fun unbox(box: Box): ReadingData {
                val reading = box.get("reading").string().get()
                val pitchData = box.get("accent").boxable(PitchData::class.java).orNull()
                return ReadingData(reading,
                        box.get("kanji").setOf(String::class.java).orNull(),
                        box.get("inf").enumSet(DictCode::class.java).get(),
                        box.get("common").bool().get(),
                        box.get("kamermansOccurence").integer().orNull(),
                        box.get("matsushitaOccurence").integer().orNull(),
                        box.get("pri").enumSet(Pri::class.java).orNull(),
                        box.get("prinf").byteValue().or((-1).toByte())
                ).apply {
                    accent = if (pitchData == null) null else PitchAccent(reading, pitchData)
                }
            }
        }
    }

    @Throws(IOException::class)
    override fun writeTo(out: DataOutput) {
        out.writeByte(VERSION.toInt())
        box().writeTo(out)
    }

    override fun box(): Box = Box()
            .putListOfBoxables("kanji", kanji)
            .putListOfBoxables("reading", reading)
            .putListOfBoxables("senses", senses)
            .putFloat("wordBasedCommonalityFloat", wordBasedCommonality)
            .putByte("jlptLevel", jlptLevel?.jlpt)

    init {
        require(reading.isNotEmpty()) { "Parameter reading: invalid value $reading: must not be empty" }
        this.senses = senses.toList()
        require(senses.isNotEmpty()) { "Parameter senses: invalid value $senses: empty" }
    }

    /**
     * Sense.
     * @property stagKanji These elements, if present, indicate that the sense is restricted
     * to the lexeme represented by the kanji. May be null, but must not be empty.
     * @property stagReading These elements, if present, indicate that the sense is restricted
     * to the lexeme represented by the reading. May be null, but must not be empty.
     * @property pos Part-of-speech information about the entry/sense. Should use
     * appropriate entity codes. In general where there are multiple senses
     * in an entry, the part-of-speech of an earlier sense will apply to
     * later senses unless there is a new part-of-speech indicated.
     * @property misc This element is used for other relevant information about
     * the entry/sense. As with part-of-speech, information will usually
     * apply to several senses.
     * Obsahuje napr. uk, co je potrebne na fixnutie bugu https://code.google.com/p/aedict/issues/detail?id=439
     * @property inf The sense-information elements provided for additional
     * information to be recorded about a sense. Typical usage would
     * be to indicate such things as level of currency of a sense, the
     * regional variations, etc.
     * @property gloss Within each sense will be one or more "glosses", i.e.
     * target-language words or phrases which are equivalents to the
     * Japanese word. This element would normally be present, however it
     * may be omitted in entries which are purely for a cross-reference.
     * @property dialect For words specifically associated with regional dialects in
     * Japanese, the entity code for that dialect, e.g. ksb for Kansaiben.
     * @property fieldsOfApplication Information about the field of application of the entry/sense.
     * When absent, general application is implied.
     * @property xref This element is used to indicate a cross-reference to another
     * entry with a similar or related meaning or sense. The content of
     * this element is typically a keb or reb element in another entry. In some
     * cases a keb will be followed by a reb and/or a sense number to provide
     * a precise target for the cross-reference. Where this happens, a JIS
     * "centre-dot" (0x2126) is placed between the components of the
     * cross-reference. ( ・  U+30FB KATAKANA MIDDLE DOT)
     */
    data class Sense(val stagKanji: Set<String>?, val stagReading: Set<String>?,
            val pos: EnumSet<DictCode>?, val misc: EnumSet<DictCode>?, val inf: String?,
            val gloss: Gloss, val loanWords: List<LoanWord>, val dialect: String?,
            val fieldsOfApplication: List<String>, val xref: List<String>) : Serializable, Writable, Boxable {

        val posMisc: EnumSet<DictCode>
            get() {
                val posmisc = EnumSet.noneOf(DictCode::class.java)
                if (pos != null) posmisc.addAll(pos)
                if (misc != null) posmisc.addAll(misc)
                return posmisc
            }

        init {
            require(stagKanji == null || stagKanji.isNotEmpty()) { "Parameter stagKanji: invalid value $stagKanji: empty" }
            require(stagReading == null || stagReading.isNotEmpty()) { "Parameter stagReading: invalid value $stagReading: empty" }
        }

        @Throws(IOException::class)
        override fun writeTo(out: DataOutput) {
            Writables.writeListOfStrings(stagKanji, out)
            Writables.writeListOfStrings(stagReading, out)
            Writables.writeEnumSet(pos, out)
            out.writeUTF(inf ?: "")
            gloss.writeTo(out)
        }

        override fun box(): Box = Box()
                .putListOfBasics("stagKanji", stagKanji)
                .putListOfBasics("stagReading", stagReading)
                .putEnumSet("pos", pos)
                .putEnumSet("misc", misc)
                .putString("inf", inf)
                .putBoxable("gloss", gloss)
                .putListOfBoxables("loanWords", if (loanWords.isEmpty()) null else loanWords)
                .putString("dialect", dialect)
                .putListOfBasics("fieldOfApplication", if (fieldsOfApplication.isEmpty()) null else fieldsOfApplication)
                .putListOfBasics("xref", if (xref.isEmpty()) null else xref)

        /**
         * Returns [pos] in the form of "(adj-i, n)". If the pos is null or empty, empty string is returned.
         * @param owner owning entry, not null.
         * @param includeMisc if true, include also the [misc]
         * @return shorthand POS, never null, may be blank.
         */
        fun getPosShort(owner: JMDictEntry, includeMisc: Boolean): String {
            val list = ArrayList<String>()
            if (owner.isCommon(this)) {
                list.add("P")
            }
            if (pos != null) {
                pos.mapTo(list) { it.jmdictCode }
            }
            if (misc != null && includeMisc) {
                misc.mapTo(list) { it.jmdictCode }
            }
            return if (list.isEmpty()) "" else list.joinToString(prefix = "(", postfix = ")")
        }

        companion object : sk.baka.aedict.util.Readable<Sense>, Unboxable<Sense> {

            /**
             * Empty sense.
             */
            val EMPTY = Sense(null, null, null, null, null,
                    Gloss(), emptyList(), null, emptyList(), emptyList())

            @JvmStatic @Throws(IOException::class)
            override fun readFrom(di: DataInput): Sense {
                val stagKanji = Writables.readSetOfStrings(di)
                val stagReading = Writables.readSetOfStrings(di)
                val pos = Writables.readEnumSet(DictCode::class.java, di)
                var inf: String? = di.readUTF()
                if (inf.isNullOrBlank()) {
                    inf = null
                }
                val gloss = Gloss.readFrom(di)
                return Sense(stagKanji, stagReading, pos, null, inf, gloss, emptyList(), null, emptyList(), emptyList())
            }

            @JvmStatic
            override fun unbox(box: Box) = Sense(
                    box.get("stagKanji").setOf(String::class.java).orNull(),
                    box.get("stagReading").setOf(String::class.java).orNull(),
                    box.get("pos").enumSet(DictCode::class.java).orNull(),
                    box.get("misc").enumSet(DictCode::class.java).orNull(),
                    box.get("inf").string().orNull(),
                    box.get("gloss").boxable(Gloss::class.java).get(),
                    box.get("loanWords").listOf(LoanWord::class.java).or(emptyList()),
                    box.get("dialect").string().orNull(),
                    box.get("fieldOfApplication").listOf(String::class.java).or(emptyList()),
                    box.get("xref").listOf(String::class.java).or(emptyList()))
        }
    }

    override fun toString() = "JMDictEntry{kanji=$kanji, reading=$reading, senses=$senses}"

    override fun getJapaneseReading(romanization: IRomanization): String = reading.joinToString { romanization.toRomaji(it.reading) }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false
        val that = other as JMDictEntry?
        if (kanji != that!!.kanji) return false
        if (reading != that.reading) return false
        return senses == that.senses
    }

    override fun hashCode(): Int {
        var result = kanji.hashCode()
        result = 31 * result + reading.hashCode()
        result = 31 * result + senses.hashCode()
        return result
    }

    /**
     * Computes the Entry commonality (the value of the "kc" field). Uses [sk.baka.aedict.kanji.Kanji.KANJI_BASED_COMMONALITY].
     * Do not mark deprecated - still used for JMNEDict, riverdic a vsetky dalsie slovniky okrem JMDict.
     * @return commonality, 0..1, 0=most common.
     */
    private fun getKanjiBasedCommonality(): Float {
        if (kanjiBasedCommonality == null) {
            kanjiBasedCommonality = Kanji.KANJI_BASED_COMMONALITY.getCommonalityFloat(kanji, reading)
        }
        return kanjiBasedCommonality!!
    }

    override fun getRubyFuriganas(): List<RubyFurigana> {
        val kanjis = kanji.map { it.kanji }
        val result = ArrayList<RubyFurigana>()
        for (readingData in reading) {
            val appliesTo = if (readingData.kanji == null) kanjis else readingData.kanji.toList()
            if (appliesTo.isEmpty()) {
                result.add(RubyFurigana(null, readingData.reading))
            } else {
                appliesTo.mapTo(result) { RubyFurigana(it, readingData.reading) }
            }
        }
        return result
    }

    override fun getSenses(): Gloss {
        val g = Gloss()
        for (sense in senses) {
            g.addAll(sense.gloss)
        }
        return g
    }

    /**
     * Najde skore, 0..pocet slov v sense, ako dobre matchuje text so [.getSenses] v tomto entry. Cim nizsie cislo, tym lepsi match tohto entry k textu.
     * Ide o bug https://github.com/mvysny/aedict/issues/555, heslo "chigau" ma text "correct" az kdesi na konci, a teda matchuje ovela horsie ako napr. heslo "hai".
     * @param words search text, not null, not empty. LOWERCASE!
     * @return 0..pocet slov v sense, cim vyssie, tym horsie entry matchuje s textom. null ak sa ziadne slovo vobec nenachadza v [.getSenses].
     */
    fun getSenseWordDistance(words: Set<String>): Int? {
        var result = Integer.MAX_VALUE
        for (sense in senses) {
            val wordDistanceFromStart = sense.gloss.getWordDistanceFromStart(words, result)
            if (wordDistanceFromStart != null) {
                result = Math.min(result, wordDistanceFromStart)
            }
        }
        return if (result == Integer.MAX_VALUE) null else result
    }

    /**
     * Najde skore, 0..pocet slov v sense, ako dobre matchuje text so [.getSenses] v tomto entry. Cim nizsie cislo, tym lepsi match tohto entry k textu.
     * Ide o bug https://github.com/mvysny/aedict/issues/555, heslo "chigau" ma text "correct" az kdesi na konci, a teda matchuje ovela horsie ako napr. heslo "hai".
     * @param text search text, not null, not blank.
     * @return 0..pocet slov v sense, cim vyssie, tym horsie entry matchuje s textom. null ak sa ziadne slovo vobec nenachadza v [.getSenses].
     */
    fun getSenseWordDistance(text: String): Int? {
        val words = text.toLowerCase().splitToWords(false)
        require(words.isNotEmpty()) { "Parameter text: invalid value $text: must not be blank" }
        return getSenseWordDistance(words.toSet())
    }

    /**
     * Checks whether, in this entry, given kanji may have given reading.
     * @param kanji the kanji
     * @param reading expected reading
     * @return true if in any furigana, the kanji may have given reading.
     */
    fun hasPossiblyReadings(kanji: Kanji, reading: String): Boolean {
        val forceReadings = Collections.singletonMap(kanji, reading)
        for (furigana in getRubyFuriganas()) {
            if (furigana.kanji != null && furigana.kanji.contains(kanji.toString()) && !furigana.mapKanjiToReading(false, forceReadings).isEmpty()) {
                return true
            }
        }
        return false
    }

    /**
     * This element records the information about the source
     * language(s) of a loan-word/gairaigo.
     * @property sourceWord The source word or phrase, may be null.
     * @property language The `xml:lang` attribute defines the language(s) from which
     * a loanword is drawn.  It will be coded using the three-letter language
     * code from the ISO 639-2 standard. When absent, the value "eng" (i.e.
     * English) is the default value. The bibliographic (B) codes are used.
     * @property lsTypeFull The ls_type attribute indicates whether the lsource element
     * fully or partially describes the source word or phrase of the
     * loanword. If absent, it will have the implied value of "full" (true).
     * Otherwise it will contain "part" (false).
     * @property isWasei The ls_wasei attribute indicates that the Japanese word
     * has been constructed from words in the source language, and
     * not from an actual phrase in that language. Most commonly used to
     * indicate "waseieigo".
     */
    data class LoanWord(val sourceWord: String?, val language: Language, val lsTypeFull: Boolean, val isWasei: Boolean) : Boxable, Serializable {

        override fun toString() =
                "LoanWord{$sourceWord from $language ${if (lsTypeFull) "full" else "part"} wasei=$isWasei"

        override fun box(): Box = Box().putString("sourceWord", sourceWord)
                .putString("language", if (language == Language.ENGLISH) null else language.lang3)
                .putBoolean("lsTypeFull", lsTypeFull)
                .putBoolean("isWasei", isWasei)

        companion object : Unboxable<LoanWord> {

            @JvmStatic
            override fun unbox(box: Box): LoanWord {
                val language = box.get("language").string().orNull()
                return LoanWord(box.get("sourceWord").string().orNull(),
                        if (language == null) Language.ENGLISH else Language(language),
                        box.get("lsTypeFull").bool().get(),
                        box.get("isWasei").bool().get()
                )
            }
        }
    }

    /**
     * This and the equivalent re_pri field are provided to record
     * information about the relative priority of the entry,  and consist
     * of codes indicating the word appears in various references which
     * can be taken as an indication of the frequency with which the word
     * is used. This field is intended for use either by applications which
     * want to concentrate on entries of  a particular priority, or to
     * generate subset files.
     *
     * The entries with news1, ichi1, spec1 and
     * gai1 values are marked with a "(P)" in the EDICT and EDICT2
     * files.
     *
     * The reason both the kanji and reading elements are tagged is because
     * on occasions a priority is only associated with a particular
     * kanji/reading pair.
     */
    enum class Pri {
        /**
         * appears in the "wordfreq" file compiled by Alexandre Girardi
         * from the Mainichi Shimbun. (See the Monash ftp archive for a copy.)
         * Words in the first 12,000 in that file are marked "news1" and words
         * in the second 12,000 are marked "news2".
         */
        News1,
        News2,
        /**
         * appears in the "Ichimango goi bunruishuu", Senmon Kyouiku
         * Publishing, Tokyo, 1998.  (The entries marked "ichi2" were
         * demoted from ichi1 because they were observed to have low
         * frequencies in the WWW and newspapers.)
         */
        Ichi1,
        Ichi2,
        /**
         * a small number of words use this marker when they
         * are detected as being common, but are not included in other lists.
         */
        Spec1,
        Spec2,
        /**
         * common loanwords, based on the wordfreq file.
         */
        Gai1,
        Gai2,
        /**
         * this is an indicator of frequency-of-use ranking in the
         * wordfreq file. "xx" is the number of the set of 500 words in which
         * the entry can be found, with "01" assigned to the first 500, "02"
         * to the second, and so on.
         */
        Nf
    }

    companion object : Readable<JMDictEntry>, Unboxable<JMDictEntry> {

        // mame Box, uz netreba dvihat verziu
        private val VERSION: Byte = 1

        @JvmStatic @Throws(IOException::class)
        override fun readFrom(di: DataInput): JMDictEntry {
            val version = di.readByte()
            // with Box, future versions are now supported as well.
            //        if (version > VERSION) {
            //            throw new RuntimeException("Unsupported version " + version + ": max " + VERSION + " is supported");
            //        }
            if (version >= 1) {
                val box = Box.readFrom(di)
                return unbox(box)
            }
            val kanji = Writables.readListOfWritables(KanjiData::class.java, di)!!
            val reading = Writables.readListOfWritables(ReadingData::class.java, di)!!
            val senses = Writables.readListOfWritables(Sense::class.java, di)!!
            return JMDictEntry(kanji, reading, senses, null, null)
        }

        @JvmStatic
        override fun unbox(box: Box): JMDictEntry {
            val jlpt = box.get("jlptLevel").byteValue().orNull()
            return JMDictEntry(
                    box.get("kanji").boxables(KanjiData::class.java).get(),
                    box.get("reading").boxables(ReadingData::class.java).get(),
                    box.get("senses").boxables(Sense::class.java).get(),
                    box.get("wordBasedCommonalityFloat").floatValue().orNull(),
                    if (jlpt == null) null else JLPTLevel.of(jlpt)
            )
        }

        /**
         * Returns true if given entry was created using [.mock]. Alebo je synthetic.
         * @param entry entry, not null
         * @return true if it is a mocked entry.
         */
        fun isMocked(entry: EntryRef): Boolean {
            // pozor, existuju hesla, ktore nemaju ziadne kanjis, napr. ahaha - laughter.
            //        return entry.jmDictEntry != null && entry.jmDictEntry.getKanjis().isEmpty();
            return entry.isSynthetic || entry.jmDictEntry != null && isMocked(entry.jmDictEntry)
        }

        /**
         * Returns true if given entry was created using [.mock]. Alebo je synthetic.
         * @param entry entry, not null
         * @return true if it is a mocked entry.
         */
        fun isMocked(entry: JMDictEntry): Boolean {
            // pozor, existuju hesla, ktore nemaju ziadne kanjis, napr. ahaha - laughter.
            //        return entry.jmDictEntry != null && entry.jmDictEntry.getKanjis().isEmpty();
            return entry.kanjis.isEmpty() && entry.senses.size <= 1 && entry.senses[0].gloss.glosses.isEmpty()
        }

        /**
         * Mocks a [JMDictEntry] which only contains given text, both as reading and as a sense. Ideal for showing error messages.
         * @param text the text, not null.
         * @return entry info wrapping [JMDictEntry]
         */
        fun mock(text: String): EntryInfo {
            val rd = JMDictEntry.ReadingData(text, null,
                    EnumSet.noneOf(DictCode::class.java), false, null, null, null, (-1).toByte())
            val ref = EntryRef.synthetic(JMDictEntry(ArrayList(),
                    ArrayList(Arrays.asList(rd)),
                    ArrayList(Arrays.asList(JMDictEntry.Sense.EMPTY)), null, null), null)
            return EntryInfo(ref, null, text)
        }

        fun synthetic(reading: String, meaning: String, origin: String, dictCode: DictCode? = null): EntryRef {
            val codes: EnumSet<DictCode> = if (dictCode == null) EnumSet.noneOf(DictCode::class.java) else EnumSet.of(dictCode)
            val rd = JMDictEntry.ReadingData(reading, null, codes,
                    false, null, null, null, (-1).toByte())
            val gloss = Gloss().add(Language.ENGLISH, meaning)
            val sense = JMDictEntry.Sense(null, null, if (codes.isEmpty()) null else codes, null, null, gloss, emptyList(), null, emptyList(), emptyList())
            val entry = JMDictEntry(emptyList(), listOf(rd), listOf(sense), null, null)
            return EntryRef.synthetic(entry, origin)
        }
    }
}
