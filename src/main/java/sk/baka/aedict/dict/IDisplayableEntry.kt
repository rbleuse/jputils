/**
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.dict

import sk.baka.aedict.kanji.IRomanization
import sk.baka.aedict.util.Language
import java.util.regex.Pattern

interface IDisplayableEntry : IEntry {
    /**
     * Return the reading with kana ONLY, no kanji. Every entry should always have
     * at least one reading, therefore this function always returns something.
     *
     * Might be formatted, e.g. kanjidic wraps `namae` readings in square brackets.
     * @param romanization romanization, not null.
     * @return comma-separated kana readings, not null, should NEVER be blank.
     */
    fun getJapaneseReading(romanization: IRomanization): String

    /**
     * Returns translated one-liner in form of sense1gloss1, sense1gloss2; sense2gloss1, ...
     *
     * Returns senses from one language only.
     *
     * The gloss separator is `,` and the sense separator (JMDict only) is fixed to `;`
     * since one sense in JMDict commonly has multiple meanings comma-separated: https://github.com/mvysny/aedict/issues/823
     * It's good to use something else than `,` to separate multiple senses.
     *
     * @param preferLang prefer this language. If null or there is no sense for this language, english senses are returned.
     * @param includeFlags if true, prepend the sense with flags, e.g. "(P, n) horse". If false, just return "horse". Only applicable to JMDict entries.
     * @return one-liner, never null.
     */
    fun getSenseOneLiner(preferLang: Language?, includeFlags: Boolean): String
}
