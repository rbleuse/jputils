/**
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.dict

import sk.baka.aedict.dict.download.DictionaryMeta
import sk.baka.aedict.kanji.IRomanization
import sk.baka.aedict.util.*
import sk.baka.aedict.util.typedmap.Box

import java.io.DataInput
import java.io.DataOutput
import java.io.IOException
import java.io.Serializable
import java.util.*

/**
 * References exactly one of [JMDictEntry], [Kanjidic2Entry] or [ExamplesEntry].
 * @property origin Which dictionary file the entry came from, e.g. "Jim Breen's JMDict" or "Buddhdic".
 * Hodnota z [DictionaryMeta.dictionaryDisplayableName]. Non null only for JMDict entries.
 * @property isSynthetic Synthetic EntryRef, used for example to show quick-answers or error messages.
 */
class EntryRef private constructor(@JvmField val jmDictEntry: JMDictEntry?, @JvmField val kanjidicEntry: Kanjidic2Entry?, @JvmField val examplesEntry: ExamplesEntry?,
                                   @JvmField val origin: String?, @JvmField val isSynthetic: Boolean) : Writable, Serializable, IDisplayableEntry, Boxable {

    val type: DictionaryMeta.DictionaryClass
        get() {
            if (jmDictEntry != null) {
                return DictionaryMeta.DictionaryClass.JMDICT
            }
            if (kanjidicEntry != null) {
                return DictionaryMeta.DictionaryClass.KANJIDIC2
            }
            if (examplesEntry != null) {
                return DictionaryMeta.DictionaryClass.EXAMPLE_SENTENCES
            }
            throw RuntimeException("unexpected")
        }

    /**
     * Returns referent, one of [examplesEntry], [kanjidicEntry] or [jmDictEntry].
     */
    val referent: Writable
        get() = when {
            jmDictEntry != null -> jmDictEntry
            kanjidicEntry != null -> kanjidicEntry
            examplesEntry != null -> examplesEntry
            else -> throw AssertionError("null referent???")
        }

    private val entry: IDisplayableEntry get() = referent as IDisplayableEntry

    /**
     * Hashcode is expensive to compute, cache the result.
     */
    @Transient private var hashCode: Int? = null

    override val kanjis: LinkedHashSet<String>
        get() = entry.kanjis

    override val readings: LinkedHashSet<String>
        get() = entry.readings

    override fun getJapaneseReading(romanization: IRomanization): String = entry.getJapaneseReading(romanization)

    override fun getSenseOneLiner(preferLang: Language?, includeFlags: Boolean): String =
            entry.getSenseOneLiner(preferLang, includeFlags)

    override fun getRubyFuriganas(): List<RubyFurigana> = entry.getRubyFuriganas()

    override fun toString() = "ref:$referent"

    @Throws(IOException::class)
    override fun writeTo(out: DataOutput) {
        // v2
        out.writeByte(30)
        box().writeTo(out)
        // v1
        //        out.writeByte(getType().ordinal() + 10);
        //        out.writeByte(VERSION);
        //        Writables.writeNullableUTF(origin, out);
        //        getReferent().writeTo(out);
    }

    override fun box(): Box {
        return Box()
                .putBoxable("jmdict", jmDictEntry)
                .putBoxable("kanjidic", kanjidicEntry)
                .putBoxable("example", examplesEntry)
                .putString("origin", origin)
                .putBoolean("synthetic", isSynthetic)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false
        val entryRef = other as EntryRef?
        if (hashCode() != other.hashCode()) {
            // equals moze byt pomale. hashCode tiez, ale to je aspon cached...
            return false
        }
        if (isSynthetic != entryRef!!.isSynthetic) return false
        if (if (examplesEntry != null) examplesEntry != entryRef.examplesEntry else entryRef.examplesEntry != null)
            return false
        if (if (jmDictEntry != null) jmDictEntry != entryRef.jmDictEntry else entryRef.jmDictEntry != null)
            return false
        return if (if (kanjidicEntry != null) kanjidicEntry != entryRef.kanjidicEntry else entryRef.kanjidicEntry != null) false else true
    }

    override fun hashCode(): Int {
        if (hashCode == null) {
            var result = jmDictEntry?.hashCode() ?: 0
            result = 31 * result + (kanjidicEntry?.hashCode() ?: 0)
            result = 31 * result + (examplesEntry?.hashCode() ?: 0)
            result = 31 * result + if (isSynthetic) 1 else 0
            hashCode = result
        }
        return hashCode!!
    }

    /**
     * Pouzi iba na boxovanie!
     * @property refs The list of refs, never null, may be empty. May not be modifiable.
     */
    data class EntryRefList(val refs: List<EntryRef>) : Writable, Serializable, Boxable {

        @Throws(IOException::class)
        override fun writeTo(out: DataOutput) {
            Writables.writeListOfWritables(refs, out)
        }

        override fun box(): Box = Box().putListOfBoxables("refs", refs)

        companion object : Unboxable<EntryRefList>, Readable<EntryRefList> {

            @JvmStatic @Throws(IOException::class)
            override fun readFrom(di: DataInput): EntryRefList {
                val r = Writables.readListOfWritables(EntryRef::class.java, di)!!
                return EntryRefList(r)
            }

            @JvmStatic
            override fun unbox(box: Box): EntryRefList =
                    EntryRefList(box.get("refs").listOf(EntryRef::class.java).get())
        }
    }

    override fun getSenses(): Gloss = entry.getSenses()

    /**
     * Zisti ci tento EntryRef matchuje s druhym EntryRefom, ci referencuju rovnaky entry.
     *
     * Snazi sa nepouzivat equals, lebo ten vrati false ak porovnavam novsiu verziu entry refu so starsou.
     * @param other druhy entry ref, not null
     * @return true if these two refs match.
     */
    fun matches(other: EntryRef): Boolean {
        if (this === other) {
            return true
        }
        val type = type
        if (type !== other.type) {
            return false
        }
        if (origin != null && other.origin != null && origin != other.origin) {
            // other is from a different dictionary, return false
            // https://github.com/mvysny/aedict/issues/488
            return false
        }
        when (type) {
            DictionaryMeta.DictionaryClass.EXAMPLE_SENTENCES -> return examplesEntry!!.kanji == other.examplesEntry!!.kanji
            DictionaryMeta.DictionaryClass.KANJIDIC2 -> return kanjidicEntry!!.kanji == other.kanjidicEntry!!.kanji
            DictionaryMeta.DictionaryClass.JMDICT -> {
                if (!jmDictEntry!!.kanji.isEmpty() || !other.jmDictEntry!!.kanji.isEmpty()) {
                    if (!jmDictEntry.kanjis.intersects(other.jmDictEntry!!.kanjis)) {
                        return false
                    }
                }
                return jmDictEntry.readings.intersects(other.jmDictEntry.readings)
            }
            else -> throw RuntimeException("Shouldn't happen, entryref can contain only three types of entries")
        }
    }

    companion object : Unboxable<EntryRef>, Readable<EntryRef> {

        fun of(e: JMDictEntry, origin: DictionaryMeta?): EntryRef = of(e, origin?.dictionaryDisplayableName, false)

        // should be private, used by tests
        internal fun of(e: JMDictEntry, origin: String?, synthetic: Boolean): EntryRef =
                EntryRef(e, null, null, origin, synthetic)

        /**
         * Synthetic EntryRef, used for example to show quick-answers.
         * @param e the entry
         * @param origin quick answer which provided the entry; null iba ak je to error hlaska alebo nieco podobne.
         * @return the entry, not null.
         */
        fun synthetic(e: JMDictEntry, origin: String?): EntryRef = of(e, origin, true)

        fun of(e: Kanjidic2Entry): EntryRef = EntryRef(null, e, null, null, false)

        fun of(e: ExamplesEntry, origin: DictionaryMeta?): EntryRef = of(e, origin?.dictionaryDisplayableName)

        internal fun of(e: ExamplesEntry, origin: String?): EntryRef = EntryRef(null, null, e, origin, false)

        // do not increase version, Box is forward-compatible.
        private val VERSION: Byte = 2

        @JvmStatic @Throws(IOException::class)
        override fun readFrom(di: DataInput): EntryRef {
            var ord = di.readByte().toInt()
            if (ord >= 30) {
                return unbox(Box.readFrom(di))
            }
            val version: Byte
            val origin: String?
            if (ord < 10) {
                version = 0
                origin = null
            } else {
                version = di.readByte()
                ord -= 10
                origin = Writables.readNullableUTF(di)
            }
            val type = DictionaryMeta.DictionaryClass.values()[ord]
            return when (type) {
                DictionaryMeta.DictionaryClass.KANJIDIC2 -> EntryRef.of(Kanjidic2Entry.readFrom(di))
                DictionaryMeta.DictionaryClass.JMDICT -> EntryRef.of(JMDictEntry.readFrom(di), origin, false)
                DictionaryMeta.DictionaryClass.EXAMPLE_SENTENCES -> EntryRef.of(ExamplesEntry.readFrom(di), origin)
                else -> throw RuntimeException("Unsupported type: $type")
            }
        }

        @JvmStatic
        override fun unbox(box: Box): EntryRef {
            val origin = box.get("origin").string().orNull()
            val e = box.get("jmdict").boxable(JMDictEntry::class.java).orNull()
            if (e != null) {
                return EntryRef.of(e, origin, box.get("synthetic").bool().or(false))
            }
            val ke = box.get("kanjidic").boxable(Kanjidic2Entry::class.java).orNull()
            return if (ke != null) {
                EntryRef.of(ke)
            } else EntryRef.of(box.get("example").boxable(ExamplesEntry::class.java).get(), origin)
        }
    }
}

fun List<JMDictEntry>.toEntryRefs(origin: DictionaryMeta?): List<EntryRef> = map { EntryRef.of(it, origin) }
@JvmName("examplesToEntryRefs")
fun List<ExamplesEntry>.toEntryRefs(origin: DictionaryMeta?): List<EntryRef> = map { EntryRef.of(it, origin) }
@JvmName("kanjidicToEntryRefs")
fun List<Kanjidic2Entry>.toEntryRefs(): List<EntryRef> = map { EntryRef.of(it) }

/**
 * Removes oldest entries (na konci listu) until there is at most `preserveCount` items present in [refs].
 * @param preserveCount the number of items to preserve, must be 0 or greater.
 */
fun MutableList<EntryRef>.removeOldest(preserveCount: Int) {
    require(preserveCount >= 0) { "Parameter preserveCount: invalid value $preserveCount: must be 0 or greater" }
    if (size > preserveCount) {
        subList(0, size - preserveCount).clear()
    }
}

/**
 * Adds given entry list to this list. Adds only items which are not already present.
 * @param indexAt where the items should be appended.
 * @param other the other list to add.
 * @return number of items added.
 */
fun MutableList<EntryRef>.addUniqueOnly(indexAt: Int, other: Iterable<EntryRef>): Int {
    var indexAt = indexAt
    var count = 0
    val set = HashSet(this)
    for (otherRef in other) {
        if (!set.contains(otherRef)) {
            this.add(indexAt++, otherRef)
            count++
        }
    }
    return count
}

fun MutableList<EntryRef>.addUniqueOnly(e: EntryRef): Int = addUniqueOnly(listOf(e))

fun MutableList<EntryRef>.addUniqueOnly(list: Iterable<EntryRef>): Int = addUniqueOnly(size, list)

/**
 * Moves entries.
 * @param range range
 * @param up true if one up, false if one down
 * @throws java.lang.IllegalArgumentException if range cannot be moved because it is already topmost (or bottom-most).
 */
fun MutableList<EntryRef>.moveEntries(range: IntRange, up: Boolean) {
    if (range.isEmpty()) {
        return
    }
    if (up) {
        require(range.start > 0) { "Parameter range: invalid value $range: cannot move up, already at start" }
        Collections.rotate(subList(range.start - 1, range.endInclusive + 1), -1)
    } else {
        require(range.endInclusive + 2 <= size) { "Parameter range: invalid value $range: cannot move down, already at end" }
        Collections.rotate(subList(range.start, range.endInclusive + 2), 1)
    }
}
