/**
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.dict

/**
 * Enumerates search modes.
 * @author Martin Vysny
 */
enum class MatcherEnum(@JvmField val mustStartWith: Boolean, @JvmField val mustEndWith: Boolean) {
    /**
     * Matches when the query is a substring of given line.
     */
    Substring(false, false) {
        override fun matches(term: String, withWhat: String) = withWhat.contains(term)
    },
    /**
     * Matches when the query matches an entire expression text for given line.
     * A special processing should be used in case of an english Edict matching: for example, foo should
     * match "foo", "foo;bar" but not "foo bar"
     */
    Exact(true, true) {
        override fun matches(term: String, withWhat: String) = term == withWhat

        override fun matches(terms: Set<String>, entry: JMDictEntry): Boolean {
            if (terms.isEmpty()) return false
            return entry.kanji.any { kanjiData -> terms.contains(kanjiData.kanji) } ||
                entry.reading.any { readingData -> terms.contains(readingData.reading) }
        }
    },
    /**
     * Matches when the line starts with the query string. Usable only for japanese search.
     */
    StartsWith(true, false) {
        override fun matches(term: String, withWhat: String) = withWhat.startsWith(term)
    },
    /**
     * Matches when the line ends with the query string. Usable only for japanese search.
     */
    EndsWith(false, true) {
        override fun matches(term: String, withWhat: String) = withWhat.endsWith(term)
    };

    /**
     * Returns true if the term matches with given string. No romaji-kana-halfwidth conversion is performed.
     * @param term the term, not null.
     * @param withWhat not null.
     * @return true if matches, false if not.
     */
    abstract fun matches(term: String, withWhat: String): Boolean

    /**
     * Returns true if at least one term matches with japanese terms in given entry.
     * @param terms a set of terms, not null, may be empty. The terms should not contain romaji. Romaji-Kana conversion is not performed.
     * @param entry the entry, not null.
     * @return true if at least one term matches at least one japanese term in entry, false otherwise.
     */
    open fun matches(terms: Set<String>, entry: JMDictEntry): Boolean {
        if (terms.isEmpty()) return false
        return entry.kanji.any { kanjiData -> terms.any { term -> matches(term, kanjiData.kanji) } } ||
            entry.reading.any { readingData -> terms.any { term -> matches(term, readingData.reading) } }
    }

    companion object {
        @JvmStatic
        fun parse(name: String?, defaultValue: MatcherEnum?) =
            if (name == null) defaultValue!! else valueOf(name)

        private val START_END = arrayOf(arrayOf(Substring, EndsWith), arrayOf(StartsWith, Exact))

        @JvmStatic
        fun startEnd(startWith: Boolean, endWith: Boolean): MatcherEnum =
            START_END[if (startWith) 1 else 0][if (endWith) 1 else 0]
    }
}
