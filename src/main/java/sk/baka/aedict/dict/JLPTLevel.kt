/**
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.dict

import sk.baka.aedict.kanji.KanjiQuiz

/**
 * JLPT level N1..5. N5 is the easiest JLPT level, N1 is the hardest one. For Kanji JLPT level list see
 * See http://www.tanos.co.uk/jlpt/jlpt1/kanji/ for details.
 *
 * For JMDict word JLPT level list: subory vyrobene z DOCov z nasledovnej stranky: http://www.tanos.co.uk/jlpt/skills/vocab/
 * @property jlpt JLPT level 1..5, N5 je najlahsi, N1 je najtazsi.
 */
enum class JLPTLevel(@JvmField val jlpt: Byte) {
    N1(1) {
        override val kanjis: String
            get() = KanjiQuiz.JlptN1.kanjis()
    },
    N2(2) {
        override val kanjis: String
            get() = KanjiQuiz.JlptN2.kanjis()
    },
    N3(3) {
        override val kanjis: String
            get() = KanjiQuiz.JlptN3.kanjis()
    },
    N4(4) {
        override val kanjis: String
            get() = KanjiQuiz.JlptN4.kanjis()
    },
    N5(5) {
        override val kanjis: String
            get() = KanjiQuiz.JlptN5.kanjis()
    };

    init {
        require(jlpt in 1..5) { "Parameter jlpt: invalid value $jlpt: must be 1..5" }
    }

    override fun toString() = "JLPT N$jlpt"

    /**
     * @return kanjis belonging to this JLPT level
     */
    abstract val kanjis: String

    companion object {

        @JvmStatic
        fun of(jlptLevel: Int): JLPTLevel {
            require(jlptLevel in 1..5) { "Parameter jlptLevel: invalid value $jlptLevel: must be 1..5" }
            return JLPTLevel.values()[jlptLevel - 1]
        }

        @JvmStatic
        fun of(jlptLevel: Byte) = of(jlptLevel.toInt())
    }
}
