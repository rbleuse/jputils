/**
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.dict

import java.util.LinkedHashSet

/**
 * Provides basic information about an entry, JMDict, Kanjidic and Tatoeba entry.
 * @author mvy
 */
interface IEntry {
    /**
     * All jp containing a mixture of kanjis and kana. May be empty. Faster than [getRubyFuriganas]
     */
    val kanjis: LinkedHashSet<String>

    /**
     * All jp containing reading ONLY, no KANJIS. Faster than [getRubyFuriganas]. The readings may contain
     * Kanjidic split characters; remove those with [String.kanjidicReadingRemoveSplits].
     */
    val readings: LinkedHashSet<String>

    /**
     * Lists all of ruby furiganas of all kanji/reading combinations.
     * @return ruby furiganas, never null, may be empty. Ak entry nema kanji, vrati vsetky readings s null kanji.
     */
    fun getRubyFuriganas(): List<RubyFurigana>

    /**
     * Get all senses/translations.
     * @return gloss, never null.
     */
    fun getSenses(): Gloss
}
