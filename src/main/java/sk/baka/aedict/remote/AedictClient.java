/*
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.remote;

import java.util.List;
import java.util.Map;

import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Context;
import android.os.Bundle;
import android.os.Message;
import android.os.RemoteException;
import sk.baka.aedict.util.UtilKt;

/**
 * A simple Aedict client. Just create the client in your Activity onStart() method and destroy it in your Activity onStop() method.
 * @author mvy
 */
public class AedictClient {

	private final MessengerClient client;
	private final Listener listener;

	/**
	 * Creates the client.
	 * @param listener notifies you of results computed by Aedict.
	 * @throws RemoteException if Aedict3 is not installed.
	 */
	public AedictClient(@NotNull Context context, @NotNull Listener listener) throws RemoteException {
		this.listener = listener;
		client = new MessengerClient(context, new IncomingHandler(), "sk.baka.aedict3.action.ACTION_SEARCH_SERVICE",
				"sk.baka.aedict3", "Aedict3 3.30");
	}

	public interface Listener {
		/**
		 * Notifies you of result for {@link #SEARCH_JMDICT}, {@link #RESOLVE_KANJI} and {@link #ANALYZE_JP_TEXT}.
		 * @param id the request ID
		 * @param entries a list of result entries, represented as a map, mapping variable name to a value. For example,
		 * the map will contain key "kanji" with the value of "母". See http://aedict.eu/export.html for a list of all variables which
		 * will be present in the map. Never null, may be empty.
		 */
		void onResult(int id, @NotNull List<Map<String, String>> entries);

		/**
		 * Notifies of result for {@link #ANALYZE_KANJIPAD}.
		 * @param kanjis
		 */
		void onKanjis(@NotNull String kanjis);
	}

	/**
	 * http://aedict.eu/api.html
	 * See the ACTION_SEARCH_JMDICT Activity.
	 */
	public static final int SEARCH_JMDICT = 0;

	/**
	 * http://aedict.eu/api.html
	 * See the ACTION_RESOLVE_KANJIDIC2 Activity.
	 */
	public static final int RESOLVE_KANJI = 1;

	/**
	 * Urobi analyzu jp vety, rovnako ako JMDict analysis fallback. Analyzu vsak urobi vzdy.
	 * Ako prvy entry vrati example sentence entry, ktory obsahuje povodny vyhladavany vyraz. Rozdiel je, ze v policku furigana bude povodny vyraz aj s priradenym readingom.
	 */
	public static final int ANALYZE_JP_TEXT = 2;

	/**
	 * Zanalyzuje userovu cmaranicu a vrati kanji ktore matchuju.
	 */
	public static final int ANALYZE_KANJIPAD = 3;

	private class IncomingHandler implements MessengerClient.MessageReceiver {
		@Override
		public void handleMessage(@NotNull Message msg) {
			if (msg.what == ANALYZE_KANJIPAD) {
				final String kanjis = msg.getData().getString("kanjis");
				listener.onKanjis(kanjis);
				return;
			}
			final List<Map<String, String>> result = (List<Map<String, String>>) msg.getData().getSerializable("result");
			log.debug("Got result: #" + msg.arg1 + " " + (result == null ? "null" : result.size() + " item(s)"));
			listener.onResult(msg.arg1, result);
		}
	}

	/**
	 * Performs JMDict/Tatoeba search. See http://aedict.eu/api.html for more information.
	 * @param kanjis the search query, Aedict 3.x accepts anything: kanji, hiragana, katakana, romaji, english, french, etc. Aedict 3.x also automatically deinflects words. See other parameters for details.
	 * @param otherArgs other arguments as specified at http://aedict.eu/api.html
	 * @param id optional message id
	 * @throws RemoteException
	 */
	public void searchJmdict(String kanjis, Bundle otherArgs, int id) throws RemoteException {
		if (otherArgs == null) {
			otherArgs = new Bundle();
		}
		otherArgs.putString("kanjis", kanjis);
		client.send(SEARCH_JMDICT, id, otherArgs);
	}

	/**
	 * Resolves all given kanji characters. All non-kanji characters are ignored.
	 * @param kanji resolve given Kanji characters, e.g. "豆頭"
	 * @param id optional message id
	 * @throws RemoteException
	 */
	public void resolveKanjis(String kanji, int id) throws RemoteException {
		final Bundle b = new Bundle();
		b.putString("kanji", kanji);
		client.send(RESOLVE_KANJI, id, b);
	}

	/**
	 * Urobi analyzu jp vety, rovnako ako JMDict analysis fallback. Analyzu vsak urobi vzdy.
	 * Ako prvy entry vrati example sentence entry, ktory obsahuje povodny vyhladavany vyraz. Rozdiel je, ze v policku furigana bude povodny vyraz aj s priradenym readingom.
	 * @param kanji analyze this sentence. Non-kana znaky (romaji) necha tak, ako je.
	 * @param id optional message id
	 * @throws RemoteException
	 */
	public void analyze(String kanji, int id) throws RemoteException {
		final Bundle b = new Bundle();
		b.putString("kanji", kanji);
		client.send(ANALYZE_JP_TEXT, id, b);
	}

	private static final Logger log = LoggerFactory.getLogger(AedictClient.class);

	/**
	 * @param coords x,y koordinaty zaciatku a konca tahu. Dlzka pola je 4 krat pocet tahov.
	 * @param forceFuzzy1Out ak true, pouzije sa aj fuzzy1 algoritmus pre kanji s +- 1 tahom
	 * @param forceFuzzy2Out ak true, pouzije sa aj fuzzy1 algoritmus pre kanji s +- 2 tahom
	 * @throws RemoteException
	 */
	public void analyzeKanjipad(@NotNull final short[] coords, boolean forceFuzzy1Out, boolean forceFuzzy2Out) throws RemoteException {
		final Bundle b = new Bundle();
		b.putShortArray("strokes", coords);
		b.putBoolean("forceFuzzy1Out", forceFuzzy1Out);
		b.putBoolean("forceFuzzy2Out", forceFuzzy2Out);
		client.send(ANALYZE_KANJIPAD, 0, b);
	}

	/**
	 * Call in your Activity onStop(), to unbind from the search service.
	 */
	public void destroy() {
		UtilKt.closeQuietly(client);
	}
}
