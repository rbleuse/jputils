/*
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.remote;

import java.io.Closeable;
import java.io.IOException;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;

/**
 * Communicates with a remote service. Must be called from the UI thread!
 * @author mvy
 */
public class MessengerClient implements Closeable {
	@NotNull
	private final Context context;
	@NotNull
	private final String serviceAction;
	@NotNull
	private final String appPackage;
	@NotNull
	private final String appName;

	public interface MessageReceiver {
		void handleMessage(@NotNull Message msg);
	}

	/**
	 * Creates the client.
	 * @param context context, not null
	 * @param incomingMessageHandler handles incoming messages from the service.
	 * @param serviceAction the action bound to the remote service in AndroidManifest.xml. For example sk.baka.aedict3.action.ACTION_SEARCH_SERVICE
	 * @param appPackage the app package name which hosts the service. This is required since Jellybean. For example sk.baka.aedict3
	 * @param appName displayable app name which hosts the service, should include required app version. For example "Aedict3 3.30"
	 * @throws RemoteException if the service cannot be bound because it does not exist.
	 */
	public MessengerClient(@NotNull Context context, @NotNull final MessageReceiver incomingMessageHandler, @NotNull String serviceAction,
						   @NotNull String appPackage, @NotNull String appName) throws RemoteException {
		this.context = context;
		this.serviceAction = serviceAction;
		this.appPackage = appPackage;
		this.appName = appName;
		bindService();
		client = new Messenger(new Handler() {
			@Override
			public void handleMessage(@NotNull Message msg) {
				// somar android kludne posle spravu aj zavretemu clientovi.
				if (closed) {
					log.warn("Closed, yet received a message, ignoring: #" + msg.arg1 + " " + msg);
				} else {
					incomingMessageHandler.handleMessage(msg);
				}
			}
		});
	}

	@NotNull
	private Intent getServiceIntent(String appPackageSuffix) {
		final Intent intent = new Intent(serviceAction);
		// the intent must be explicit, otherwise Jellybean will crash
		// http://stackoverflow.com/questions/27981246/dealing-with-implicit-intent-future-deprecation-in-lollipop
		intent.setPackage(appPackage + appPackageSuffix);
		return intent;
	}

	/**
	 * Outgoing message client.
	 */
	@NotNull
	private final Messenger client;

	/**
	 * Kym sa service neconnectne, out spravy sa ulozia tu.
	 */
	@Nullable
	private Message outbox;

	private boolean initialConnectInProgress = true;

	public static boolean isUIThread() {
		return Looper.myLooper() != null;
	}
	public static void checkUIThread() {
		if (!isUIThread()) {
			throw new IllegalStateException("Invalid state: must be invoked in the UI thread");
		}
	}

	/**
	 * Sends a message with given 'what' and given 'data' Bundle.
	 * @param what {@link Message#what} pouzite na rozoznanie typu spravy
	 * @param arg1 {@link Message#arg1} pouzite na ID spravy.
	 * @param data {@link Message#getData()}
	 * @throws RemoteException ak service zrazu neexistuje (niekto odinstaloval appku so servisom?). Nikdy nethrowuje DeadObjectException - ta je catchnuta a sprava je znovudorucena.
	 */
	public void send(int what, int arg1, @Nullable Bundle data) throws RemoteException {
		checkUIThread();
		checkNotClosed();
		final Message msg = Message.obtain(null, what, arg1, 0);
		msg.setData(data);
		msg.replyTo = client;
		if (server == null) {
			log.debug("Service " + appName + "/" + serviceAction + " not yet started, putting message to outbox: " + data);
			outbox = msg;
			if (!closed && !initialConnectInProgress) {
				// niekto stopol servis, zareagovat
				log.warn("The service " + appName + "/" + serviceAction + " is bound, yet stopped? Binding again");
				forceStartService();
			}
		} else {
			log.debug("Service " + appName + "/" + serviceAction + " is started, sending: " + data);
			outbox = null;
			try {
				server.send(msg);
			} catch (DeadObjectException ex) {
				log.info("Recovering from DeadObjectException", ex);
				// service zakapal. musim sa zrecoverovat.
				outbox = msg;
				forceStartService();
			}
		}
	}

	private void forceStartService() throws RemoteException {
		// tu som chcel zavolat context.startService() ale tam sa neda dat ServiceConnection!
		bindService();
	}

	private void bindService() throws RemoteException {
		if (!context.bindService(getServiceIntent(""), connection, Context.BIND_AUTO_CREATE)) {
			if (!context.bindService(getServiceIntent(".underground"), connection, Context.BIND_AUTO_CREATE)) {
				// user musi byt informovany, nesmie sa mu tam len tak nieco tocit bez odpovede.
				// throwni exception
				throw new RemoteException("Failed to connect to " + appName + " Search Service, is " + appName + " or higher installed?");
			}
		}
	}

	private static final Logger log = LoggerFactory.getLogger(MessengerClient.class);

	private boolean closed = false;

	private void checkNotClosed() {
		if (closed) {
			throw new IllegalStateException("Invalid state: closed");
		}
	}

	@Override
	public void close() throws IOException {
		checkUIThread();
		if (!closed) {
			context.unbindService(connection);
			server = null;
			log.info(appName + " connection destroyed");
		}
		closed = true;
	}

	@Nullable
	private Messenger server;

	/**
	 * Class for interacting with the main interface of the service.
	 */
	private ServiceConnection connection = new ServiceConnection() {
		public void onServiceConnected(ComponentName className, IBinder service) {
			if (closed) {
				return;
			}
			initialConnectInProgress = false;
			log.info(appName + " connection established");
			server = new Messenger(service);
			if (outbox != null) {
				final Message msg = outbox;
				outbox = null;
				try {
					log.debug("Sending " + msg.getData());
					server.send(msg);
				} catch (RemoteException e) {
					log.error("Unexpected remote exception sending message", e);
				}
			}
		}

		public void onServiceDisconnected(ComponentName className) {
			server = null;
		}

		@Override
		public String toString() {
			return MessengerClient.class.getName() + "$ServiceConnection";
		}
	};
}
