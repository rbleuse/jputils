/*
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.remote;

import org.jetbrains.annotations.NotNull;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import sk.baka.aedict.util.MiscUtils;
import sk.baka.aedict.util.UtilKt;

/**
 * A simple Kanjipad Ext client. Just create the client in your Activity onStart() method and destroy it in your Activity onStop() method.
 * @author mvy
 */
public class KanjipadExtClient {

	@NotNull
	private final Listener listener;
	@NotNull
	private final MessengerClient client;

	/**
	 * Creates the client.
	 * @param listener notifies you of results computed by Aedict.
	 * @throws RemoteException if KanjiPadExt is not installed.
	 */
	public KanjipadExtClient(@NotNull Context context, @NotNull Listener listener) throws RemoteException {
		this.listener = listener;
		this.client = new MessengerClient(context, new IncomingHandler(), "sk.baka.aedictkanjipadext.action.ACTION_SEARCH_SERVICE",
				"sk.baka.aedictkanjipadext", "Aedict KanjipadExt 1.5");
	}

	public interface Listener {
		/**
		 * Notifies you of result.
		 * @param kanjis a list of kanjis, matching given drawing. Never null, may be empty.
		 */
		void onResult(@NotNull String kanjis);
	}

	private class IncomingHandler implements MessengerClient.MessageReceiver {
		@Override
		public void handleMessage(@NotNull Message msg) {
			final String kanjis = msg.getData().getString("kanjis");
			listener.onResult(kanjis);
		}
	}

	/**
	 * @param coords x,y koordinaty zaciatku a konca tahu. Dlzka pola je 4 krat pocet tahov.
	 * @param forceFuzzy1Out ak true, pouzije sa aj fuzzy1 algoritmus pre kanji s +- 1 tahom
	 * @param forceFuzzy2Out ak true, pouzije sa aj fuzzy1 algoritmus pre kanji s +- 2 tahom
	 * @throws RemoteException
	 */
	public void search(@NotNull final short[] coords, boolean forceFuzzy1Out, boolean forceFuzzy2Out) throws RemoteException {
		final Bundle b = new Bundle();
		b.putShortArray("strokes", coords);
		b.putBoolean("forceFuzzy1Out", forceFuzzy1Out);
		b.putBoolean("forceFuzzy2Out", forceFuzzy2Out);
		client.send(0, 0, b);
	}

	/**
	 * Call in your Activity onStop(), to unbind from the search service.
	 */
	public void destroy() {
		UtilKt.closeQuietly(client);
	}
}
