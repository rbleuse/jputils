/**
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
@file:Suppress("NAME_SHADOWING")

package sk.baka.aedict.inflection

import sk.baka.aedict.kanji.*
import java.util.*

/**
 * Performs a simple verb deinflection.
 * @author Martin Vysny
 */
object VerbDeinflector {

    private fun irregularDeinflector(inflected: String, form: VerbInflection.Form?, base: Map<String, EnumSet<InflectableWordClass>>)
            = EntireWordDeinflector(inflected, true, form, base)
    private fun irregularDeinflector(inflected: String, form: VerbInflection.Form?, vararg base: String)
            = irregularDeinflector(inflected, form, base.toList().associateWith { InflectableWordClass.VERB_GENERIC })

    private class EruDeinflector : AbstractDeinflector {
        // this rule is also required, to correctly deinflect e.g.
        // aetai. list as a last rule. Make the rule produce the old verb and
        // also the deinflected one.

        private val eruDeinflector = EndsWithDeinflector("eru", VerbInflection.Form.ABLE_TO_DO2, mapOf("eru" to InflectableWordClass.VERB_ICHIDAN, "u" to InflectableWordClass.VERB_GODAN))

        override val form: VerbInflection.Form
            get() = VerbInflection.Form.ABLE_TO_DO2

        override fun deinflect(romaji: String): Map<String, EnumSet<InflectableWordClass>>? {
            // do not deinflect -rareru
            if (romaji.endsWith("rareru")) {
                return null
            }
            return if (romaji.length == 4 && !romaji[0].isAsciiVowel()) {
                // do not deinflect e.g. heru, but deinflect aeru
                // https://github.com/mvysny/aedict/issues/676
                null
            } else eruDeinflector.deinflect(romaji)
        }

        override fun stopIfMatch(): Boolean {
            // if the -eru is deinflected, there is nothing more to match
            return true
        }
    }

    /**
     * Beware: this collides with Form.EXPLAIN_PLAN!!
     */
    private class NdaDeinflector : EndsWithDeinflector("nda", true, VerbInflection.Form.PAST_TENSE, "nu", "bu", "mu", "") {

        override fun deinflect(romaji: String): Map<String, EnumSet<InflectableWordClass>>? {
            var result = super.deinflect(romaji)
            // remove dumb stuff like asonda -> aso
            if (result != null) {
                result = result.filterKeys { !it.endsWith("o") }
            }
            return result
        }
    }

    private class Base4Deinflector : AbstractDeinflector {

        override val form: VerbInflection.Form?
            get() = VerbInflection.Form.PLAIN_COMMAND

        override fun deinflect(romaji: String): Map<String, EnumSet<InflectableWordClass>>? {
            if (romaji.length < 3 || !romaji.endsWith("e")) return null
            if (romaji[romaji.length - 2] == romaji[romaji.length - 3]) return null
            if (romaji.endsWith("ite") || romaji.endsWith("ide")) return null
            // this might be "見te" which would be deinflected to "見tu" which is invalid.
            // Leave as-is - the deinflection will run again
            // and the resolver will resolve the reading to mite which I know to ignore.
            if ((romaji.endsWith("te") || romaji.endsWith("de")) && romaji[romaji.length - 3].isKanji) return null
            return mapOf((romaji.substring(0, romaji.length - 1) + "u") to InflectableWordClass.VERB_GODAN)
        }

        override fun stopIfMatch(): Boolean = false
    }

    private class Base5Deinflector : AbstractDeinflector {

        override val form: VerbInflection.Form?
            get() = VerbInflection.Form.LET_S2

        override fun deinflect(romaji: String): Map<String, EnumSet<InflectableWordClass>>? {
            when(romaji) {
                "koyou" -> return mapOf("kuru" to InflectableWordClass.VERB_KURU)
                "siyou" -> return mapOf("suru" to InflectableWordClass.VERB_SURU)
                "zuyou" -> return mapOf("zuru" to InflectableWordClass.VERB_GENERIC)
            }
            if (ShortVerbDeinflector.matches(romaji, "you")) {
                return mapOf("${romaji[0]}ru" to InflectableWordClass.VERB_GENERIC)
            }
            return when {
                romaji.endsWith("you") && romaji.length > 3 ->
                    // this handles both ichidan and aru godan verbs
                    mapOf(romaji.dropLast(3) + "ru" to InflectableWordClass.VERB_GENERIC)
                romaji.endsWith("ou") && romaji.length > 2 && romaji != "you" ->
                    // godan
                    mapOf(romaji.dropLast(2) + "u" to InflectableWordClass.VERB_GENERIC)
                else -> null
            }
        }

        override fun stopIfMatch(): Boolean = true
    }

    /**
     * Helps deinflect short verbs, e.g. 見ました -> 見ru.
     *
     * https://github.com/mvysny/aedict/issues/501
     */
    interface ReadingResolver {
        /**
         * Tries to resolve reading for a verb. Only resolves verbs consisting of two characters, first character being a kanji and
         * the second character being hiragana, such as 見ru.
         * @param verbKanji the verb kanji such as 見
         * @return the resolved reading in hiragana such as "mi". Null if there is no such word.
         */
        fun resolve(verbKanji: Kanji): JpCharacter?
    }

    private fun basicSuffix(endsWith: String, form: VerbInflection.Form?, vararg replaceBy: String): AbstractDeinflector =
            EndsWithDeinflector(endsWith, true, form, *replaceBy)

    private fun basicSuffix(endsWith: String, form: VerbInflection.Form?, replaceBy: Map<String, EnumSet<InflectableWordClass>>): AbstractDeinflector =
            EndsWithDeinflector(endsWith, true, form, replaceBy)

    private fun irregular(endsWith: Array<String>, form: VerbInflection.Form?, replaceBy: String): List<AbstractDeinflector> =
            endsWith.map { irregularDeinflector(it, form, replaceBy) }

    private val DEINFLECTORS: List<AbstractDeinflector>

    init {
        val d = ArrayList<AbstractDeinflector>()
        // adjective deinflections first - more accurate, also I don't want -takakatta to deinflect using the verb -tta rule.
        // -tai a -nai forms are I-adj, I can deinflect them nicely here.
        d.add(IAdjectiveDeinflector())

        // verb deinflections
        d.addAll(irregular(arrayOf("dewaarimasen", "dehaarimasen", "de wa arimasen", "de ha arimasen", "zya arimasen", "zyaarimasen"), VerbInflection.Form.POLITE_NEGATIVE, "desu"))
        d.addAll(irregular(arrayOf("dewaarimasendesita", "dehaarimasendesita", "de wa arimasen desita", "de ha arimasen desita", "zya arimasen desita", "zyaarimasendesita"), VerbInflection.Form.POLITE_PAST_NEGATIVE, "desu"))
        // the -masu deinflector
        d.add(EndsWithDeinflector("masyou", VerbInflection.Form.LET_S2, "masu"))
        d.add(EndsWithDeinflector("masen", VerbInflection.Form.POLITE_NEGATIVE, "masu"))
        d.add(EndsWithDeinflector("masita", VerbInflection.Form.POLITE_PAST, "masu"))
        d.add(EndsWithDeinflector("masendesita", VerbInflection.Form.POLITE_PAST_NEGATIVE, "masu"))
        d.add(EndsWithDeinflector("masen desita", VerbInflection.Form.POLITE_PAST_NEGATIVE, "masu"))
        // the -nakatta deinflector
        d.add(EndsWithDeinflector("nakatta", VerbInflection.Form.NEGATIVE_PAST, "nai"))
        // the -nakereba deinflector
        d.add(EndsWithDeinflector("nakereba", VerbInflection.Form.NEGATIVE_CONDITIONAL, "nai"))
        // the -n desu / -n da deinflectors
        d.add(EndsWithDeinflector("n desu", VerbInflection.Form.EXPLAIN_PLAN, ""))
        d.add(EndsWithDeinflector("ndesu", VerbInflection.Form.EXPLAIN_PLAN, ""))
        d.add(EndsWithDeinflector("n da", VerbInflection.Form.EXPLAIN_PLAN, ""))

        // ochitsukase -> ochitsukaseru -> ochitsuku
        d.add(EndsWithDeinflector("ase", null, "aseru"))

        // tabeteimasu -> tabete
        d.add(EndsWithDeinflector("eimasu", VerbInflection.Form.PROGRESSIVE_TENSE, "e"))
        d.add(EndsWithDeinflector("e imasu", VerbInflection.Form.PROGRESSIVE_TENSE, "e"))
        // now, deinflect -te iru
        d.add(EndsWithDeinflector("eita", VerbInflection.Form.PAST_TENSE, "eiru"))
        d.add(EndsWithDeinflector("eite", VerbInflection.Form.CONTINUATION, "eiru"))
        d.add(EndsWithDeinflector("eiru", VerbInflection.Form.PROGRESSIVE_TENSE, "e"))
        d.add(EndsWithDeinflector("e iru", VerbInflection.Form.PROGRESSIVE_TENSE, "e"))

        // tabenaide -> tabenai
        d.add(EndsWithDeinflector("naide", VerbInflection.Form.MILD_COMMAND, "nai"))

        d.add(EndsWithDeinflector("rareta", VerbInflection.Form.PAST_TENSE, "rareru"))
        d.add(EndsWithDeinflector("rarete", VerbInflection.Form.CONTINUATION, "rareru"))
        d.add(EndsWithDeinflector("rarenai", VerbInflection.Form.NEGATIVE, "rareru"))
        d.add(EndsWithDeinflector("teta", VerbInflection.Form.PAST_TENSE, "teru"))
        d.add(EndsWithDeinflector("tete", VerbInflection.Form.CONTINUATION, "teru"))
        d.add(EndsWithDeinflector("teru", VerbInflection.Form.PROGRESSIVE_TENSE, "te"))

        // desu
        d.add(irregularDeinflector("da", VerbInflection.Form.PLAIN, "desu"))
        d.addAll(irregular(arrayOf("dewanai", "zyanai", "dehanai", "de ha nai", "de wa nai"), VerbInflection.Form.NEGATIVE, "desu"))
        d.add(irregularDeinflector("datta", VerbInflection.Form.PAST_TENSE, "desu"))
        d.add(irregularDeinflector("desita", VerbInflection.Form.POLITE_PAST, "desu"))
        d.add(irregularDeinflector("de", null, "desu"))
        d.addAll(irregular(arrayOf("dehaaru", "de ha aru", "de wa aru", "dewaaru"), VerbInflection.Form.PLAIN, "desu"))

        // tekuru
        d.add(SuffixDeinflector("tekita", VerbInflection.Form.PAST_TENSE, mapOf("tekuru" to InflectableWordClass.VERB_GODAN)))
        d.add(SuffixDeinflector("tekimasu", VerbInflection.Form.TO_FINISH, mapOf("tekuru" to InflectableWordClass.VERB_GODAN, "te" to InflectableWordClass.VERB_GENERIC)))
        d.add(SuffixDeinflector("tekuru", VerbInflection.Form.TO_FINISH, mapOf("tekuru" to InflectableWordClass.VERB_GODAN, "te" to InflectableWordClass.VERB_GENERIC)))
        // teiku
        d.add(SuffixDeinflector("teitta", VerbInflection.Form.PAST_TENSE, mapOf("teiku" to InflectableWordClass.VERB_GODAN)))
        d.add(SuffixDeinflector("teikimasu", VerbInflection.Form.TO_START, mapOf("teiku" to InflectableWordClass.VERB_GODAN, "te" to InflectableWordClass.VERB_GENERIC)))
        d.add(SuffixDeinflector("teiku", VerbInflection.Form.TO_START, mapOf("teiku" to InflectableWordClass.VERB_GODAN, "te" to InflectableWordClass.VERB_GENERIC)))

        // irregulars deinflector
        // suru
        d.add(irregularDeinflector("simasu", VerbInflection.Form.POLITE, mapOf("suru" to InflectableWordClass.VERB_SURU)))
        d.add(SuffixDeinflector("simasu", VerbInflection.Form.POLITE, mapOf("suru" to InflectableWordClass.VERB_SURU_CLASS, "siru" to InflectableWordClass.VERB_ICHIDAN, "su" to InflectableWordClass.VERB_GODAN)))
        d.add(irregularDeinflector("sinai", VerbInflection.Form.NEGATIVE, mapOf("suru" to InflectableWordClass.VERB_SURU)))
        d.add(SuffixDeinflector("sinai", VerbInflection.Form.NEGATIVE, mapOf("suru" to InflectableWordClass.VERB_SURU_CLASS, "siru" to InflectableWordClass.VERB_ICHIDAN)))
        d.add(irregularDeinflector("sita", VerbInflection.Form.PAST_TENSE, mapOf("suru" to InflectableWordClass.VERB_SURU)))
        d.add(SuffixDeinflector("sita", VerbInflection.Form.PAST_TENSE, mapOf("suru" to InflectableWordClass.VERB_SURU_CLASS, "siru" to InflectableWordClass.VERB_ICHIDAN, "su" to InflectableWordClass.VERB_GODAN)))
        d.add(irregularDeinflector("site", VerbInflection.Form.CONTINUATION, mapOf("suru" to InflectableWordClass.VERB_SURU)))
        d.add(SuffixDeinflector("site", VerbInflection.Form.CONTINUATION, mapOf("suru" to InflectableWordClass.VERB_SURU_CLASS, "siru" to InflectableWordClass.VERB_ICHIDAN, "su" to InflectableWordClass.VERB_GODAN)))
        d.add(irregularDeinflector("siyou", VerbInflection.Form.LET_S2, mapOf("suru" to InflectableWordClass.VERB_SURU)))
        d.add(SuffixDeinflector("siyou", VerbInflection.Form.LET_S2, mapOf("suru" to InflectableWordClass.VERB_SURU_CLASS, "siru" to InflectableWordClass.VERB_ICHIDAN)))
        d.add(irregularDeinflector("saseru", VerbInflection.Form.LET_HIM, mapOf("suru" to InflectableWordClass.VERB_SURU)))
        d.add(irregularDeinflector("saserareru", VerbInflection.Form.LET_HIM, mapOf("suru" to InflectableWordClass.VERB_SURU)))
        d.add(SuffixDeinflector("dekiru", VerbInflection.Form.ABLE_TO_DO2, mapOf("suru" to InflectableWordClass.VERB_SURU_CLASS)))
        d.add(SuffixDeinflector("dekita", VerbInflection.Form.PAST_TENSE, mapOf("suru" to InflectableWordClass.VERB_SURU_CLASS, "dekiru" to InflectableWordClass.VERB_ICHIDAN)))
        d.add(irregularDeinflector("sinasai", VerbInflection.Form.SIMPLE_COMMAND, mapOf("suru" to InflectableWordClass.VERB_SURU)))
        d.add(irregularDeinflector("siro", VerbInflection.Form.PLAIN_COMMAND, mapOf("suru" to InflectableWordClass.VERB_SURU)))
        d.add(irregularDeinflector("sareru", VerbInflection.Form.PLAIN, "sareru"))
        d.add(irregularDeinflector("sarenai", VerbInflection.Form.NEGATIVE, "sareru"))
        d.add(irregularDeinflector("sareta", VerbInflection.Form.PAST_TENSE, "sareru"))

        // dekiru, it seems that all verbs containing 来ru are all ichidan (with 出) & kuru verbs, so add special rule for 出来ru
        d.add(SuffixDeinflector("出来masu", VerbInflection.Form.POLITE, mapOf("出来ru" to InflectableWordClass.VERB_ICHIDAN)))
        d.add(SuffixDeinflector("出来nai", VerbInflection.Form.NEGATIVE, mapOf("出来ru" to InflectableWordClass.VERB_ICHIDAN)))
        d.add(SuffixDeinflector("出来ta", VerbInflection.Form.PAST_TENSE, mapOf("出来ru" to InflectableWordClass.VERB_ICHIDAN)))
        d.add(SuffixDeinflector("出来te", VerbInflection.Form.CONTINUATION, mapOf("出来ru" to InflectableWordClass.VERB_ICHIDAN)))
        d.add(SuffixDeinflector("出来i", VerbInflection.Form.PLAIN_COMMAND, mapOf("出来ru" to InflectableWordClass.VERB_ICHIDAN)))

        // kuru, also use kanji because 来 has several readings, so force deinflection with kanji
        d.add(irregularDeinflector("kimasu", VerbInflection.Form.POLITE, mapOf("kuru" to InflectableWordClass.VERB_KURU, "kiru" to InflectableWordClass.VERB_ICHIDAN)))
        d.add(SuffixDeinflector("来masu", VerbInflection.Form.POLITE, mapOf("来ru" to InflectableWordClass.VERB_KURU)))
        d.add(irregularDeinflector("konai", VerbInflection.Form.NEGATIVE, mapOf("kuru" to InflectableWordClass.VERB_KURU)))
        d.add(SuffixDeinflector("来nai", VerbInflection.Form.NEGATIVE, mapOf("来ru" to InflectableWordClass.VERB_KURU)))
        d.add(irregularDeinflector("kita", VerbInflection.Form.PAST_TENSE, mapOf("kuru" to InflectableWordClass.VERB_KURU, "kiru" to InflectableWordClass.VERB_ICHIDAN)))
        d.add(SuffixDeinflector("来ta", VerbInflection.Form.PAST_TENSE, mapOf("来ru" to InflectableWordClass.VERB_KURU)))
        d.add(irregularDeinflector("kite", VerbInflection.Form.CONTINUATION, mapOf("kuru" to InflectableWordClass.VERB_KURU, "kiru" to InflectableWordClass.VERB_ICHIDAN)))
        d.add(SuffixDeinflector("来te", VerbInflection.Form.CONTINUATION, mapOf("来ru" to InflectableWordClass.VERB_KURU)))
        d.add(irregularDeinflector("koyou", VerbInflection.Form.LET_S2, mapOf("kuru" to InflectableWordClass.VERB_KURU)))
        d.add(irregularDeinflector("koi", VerbInflection.Form.PLAIN_COMMAND, mapOf("kuru" to InflectableWordClass.VERB_KURU)))
        d.add(SuffixDeinflector("来i", VerbInflection.Form.PLAIN_COMMAND, mapOf("来ru" to InflectableWordClass.VERB_KURU)))
        d.add(irregularDeinflector("kinasai", VerbInflection.Form.SIMPLE_COMMAND, mapOf("kuru" to InflectableWordClass.VERB_KURU, "kiru" to InflectableWordClass.VERB_ICHIDAN)))
        d.add(SuffixDeinflector("来nasai", VerbInflection.Form.SIMPLE_COMMAND, mapOf("来ru" to InflectableWordClass.VERB_KURU)))
        d.add(irregularDeinflector("korareru", VerbInflection.Form.ABLE_TO_DO, mapOf("kuru" to InflectableWordClass.VERB_KURU)))
        d.add(irregularDeinflector("kosaseru", VerbInflection.Form.LET_HIM, mapOf("kuru" to InflectableWordClass.VERB_KURU)))
        d.add(irregularDeinflector("kosaserareru", VerbInflection.Form.LET_HIM, mapOf("kuru" to InflectableWordClass.VERB_KURU)))

        // zuru
        d.add(irregularDeinflector("zunai", VerbInflection.Form.NEGATIVE, "zuru"))
        d.add(irregularDeinflector("zuta", VerbInflection.Form.PAST_TENSE, "zuru"))
        d.add(irregularDeinflector("zute", VerbInflection.Form.CONTINUATION, "zuru"))
        d.add(irregularDeinflector("zumasu", VerbInflection.Form.POLITE, "zuru"))

        // iku
        d.add(irregularDeinflector("ikimasu", VerbInflection.Form.POLITE, "iku"))
        d.add(irregularDeinflector("ike", VerbInflection.Form.PLAIN_COMMAND, "iku"))
        d.add(irregularDeinflector("行ke", VerbInflection.Form.PLAIN_COMMAND, "行ku"))
        d.add(irregularDeinflector("itta", VerbInflection.Form.PAST_TENSE, "iku"))
        d.add(irregularDeinflector("itte", VerbInflection.Form.CONTINUATION, "iku"))
        d.add(irregularDeinflector("ikareru", VerbInflection.Form.PLAIN, "ikareru"))
        d.add(irregularDeinflector("ikarenai", VerbInflection.Form.NEGATIVE, "ikareru"))
        d.add(irregularDeinflector("ikareta", VerbInflection.Form.PAST_TENSE, "ikareru"))

        // iru
        d.add(irregularDeinflector("imasu", VerbInflection.Form.POLITE, mapOf("iru" to InflectableWordClass.VERB_ICHIDAN)))
        d.add(irregularDeinflector("ita", VerbInflection.Form.PAST_TENSE, mapOf("iru" to InflectableWordClass.VERB_ICHIDAN)))
        d.add(irregularDeinflector("inai", VerbInflection.Form.NEGATIVE, mapOf("iru" to InflectableWordClass.VERB_ICHIDAN)))
        d.add(irregularDeinflector("itai", VerbInflection.Form.WANT, mapOf("iru" to InflectableWordClass.VERB_ICHIDAN)))

        // aru
        d.add(irregularDeinflector("nai", VerbInflection.Form.NEGATIVE, "aru"))
        d.add(irregularDeinflector("nakatta", VerbInflection.Form.NEGATIVE_PAST, "aru"))
        d.add(irregularDeinflector("arimasu", VerbInflection.Form.POLITE, "aru"))
        d.add(irregularDeinflector("atte", VerbInflection.Form.CONTINUATION, "aru", "au"))
        d.add(irregularDeinflector("atta", VerbInflection.Form.PAST_TENSE, "aru", "au"))

        // tou
        d.add(irregularDeinflector("toute", VerbInflection.Form.CONTINUATION, "tou"))
        d.add(irregularDeinflector("touta", VerbInflection.Form.PAST_TENSE, "tou"))

        // kureru - shouldn't deinflect into kuru (InflectableWordClass.VERB_KURU)
        d.add(irregularDeinflector("kure", VerbInflection.Form.POLITE_COMMAND, mapOf("呉れる" to InflectableWordClass.VERB_ICHIDAN, "kuru" to InflectableWordClass.VERB_GODAN)))

        // irregulars honorifics
        d.add(irregularDeinflector("kudasaimasu", VerbInflection.Form.POLITE, mapOf("kudasaru" to InflectableWordClass.VERB_GODAN)))
        d.add(irregularDeinflector("kudasai", VerbInflection.Form.POLITE_COMMAND, mapOf("kudasaru" to InflectableWordClass.VERB_GODAN)))
        d.add(irregularDeinflector("irassyaimasu", VerbInflection.Form.POLITE, mapOf("irassyaru" to InflectableWordClass.VERB_GODAN)))
        d.add(irregularDeinflector("irassyai", VerbInflection.Form.POLITE_COMMAND, mapOf("irassyaru" to InflectableWordClass.VERB_GODAN)))
        d.add(irregularDeinflector("ossyaimasu", VerbInflection.Form.POLITE, mapOf("ossyaru" to InflectableWordClass.VERB_GODAN)))
        d.add(irregularDeinflector("ossyai", VerbInflection.Form.POLITE_COMMAND, mapOf("ossyaru" to InflectableWordClass.VERB_GODAN)))
        d.add(irregularDeinflector("gozaimasu", VerbInflection.Form.POLITE, mapOf("gozaru" to InflectableWordClass.VERB_GENERIC)))
        d.add(irregularDeinflector("gozai", VerbInflection.Form.POLITE_COMMAND, mapOf("gozaru" to InflectableWordClass.VERB_GENERIC)))
        d.add(irregularDeinflector("nasaimasu", VerbInflection.Form.POLITE, mapOf("nasaru" to InflectableWordClass.VERB_GODAN)))
        d.add(irregularDeinflector("nasai", VerbInflection.Form.POLITE_COMMAND, mapOf("nasaru" to InflectableWordClass.VERB_GODAN)))

        // causative passive
        d.add(basicSuffix("esaserareru", VerbInflection.Form.CAUSATIVE_PASSIVE, mapOf("eru" to InflectableWordClass.VERB_ICHIDAN)))
        d.add(basicSuffix("isaserareru", VerbInflection.Form.CAUSATIVE_PASSIVE, mapOf("iru" to InflectableWordClass.VERB_ICHIDAN, "isu" to InflectableWordClass.VERB_GODAN)))
        d.add(basicSuffix("saserareru", VerbInflection.Form.CAUSATIVE_PASSIVE, mapOf("su" to InflectableWordClass.VERB_GODAN, "suru" to InflectableWordClass.VERB_SURU_CLASS)))
        d.add(basicSuffix("waserareru", VerbInflection.Form.CAUSATIVE_PASSIVE, mapOf("u" to InflectableWordClass.VERB_GODAN)))
        d.add(basicSuffix("wasareru", VerbInflection.Form.CAUSATIVE_PASSIVE, mapOf("u" to InflectableWordClass.VERB_GODAN)))
        d.add(basicSuffix("aserareru", VerbInflection.Form.CAUSATIVE_PASSIVE, mapOf("u" to InflectableWordClass.VERB_GODAN)))
        d.add(basicSuffix("asareru", VerbInflection.Form.CAUSATIVE_PASSIVE, mapOf("u" to InflectableWordClass.VERB_GODAN)))

        // rareru -> ru, BUT NOT reru -> ru, conflict with e.g. ikareru.
        // this rule must be after irregular, so that korareru is not deinflected to koru
        // @todo mvy re-run the entire deinflection chain again on -rareru?
        // rareru->ru is PASSIVE: https://github.com/mvysny/aedict/issues/786
        d.add(basicSuffix("rareru", VerbInflection.Form.PASSIVE, "ru"))

        // causative
        d.add(basicSuffix("esaseru", VerbInflection.Form.LET_HIM, mapOf("eru" to InflectableWordClass.VERB_ICHIDAN)))
        d.add(basicSuffix("isaseru", VerbInflection.Form.LET_HIM, mapOf("iru" to InflectableWordClass.VERB_ICHIDAN, "isu" to InflectableWordClass.VERB_GODAN)))
        d.add(basicSuffix("saseru", VerbInflection.Form.LET_HIM, mapOf("su" to InflectableWordClass.VERB_GODAN, "suru" to InflectableWordClass.VERB_SURU_CLASS)))
        d.add(basicSuffix("waseru", VerbInflection.Form.LET_HIM, mapOf("u" to InflectableWordClass.VERB_GODAN)))
        d.add(basicSuffix("aseru", VerbInflection.Form.LET_HIM, mapOf("u" to InflectableWordClass.VERB_GODAN)))

        // regular inflections
        d.add(EndsWithDeinflector("arenai", VerbInflection.Form.NEGATIVE, "areru"))
        d.add(EndsWithDeinflector("areta", VerbInflection.Form.PAST_TENSE, "areru"))
        d.add(basicSuffix("wareru", VerbInflection.Form.PASSIVE, mapOf("u" to InflectableWordClass.VERB_GODAN)))
        d.add(basicSuffix("areru", VerbInflection.Form.PASSIVE, mapOf("u" to InflectableWordClass.VERB_GODAN)))
        d.add(basicSuffix("wanai", VerbInflection.Form.NEGATIVE, mapOf("u" to InflectableWordClass.VERB_GODAN)))
        d.add(basicSuffix("anai", VerbInflection.Form.NEGATIVE, mapOf("u" to InflectableWordClass.VERB_GODAN)))
        // further deinflect -eru
        d.add(EndsWithDeinflector("enai", VerbInflection.Form.NEGATIVE, mapOf("eru" to InflectableWordClass.VERB_ICHIDAN)))
        d.add(ShortVerbDeinflector("nai", VerbInflection.Form.NEGATIVE, mapOf("ru" to InflectableWordClass.VERB_ICHIDAN)))
        // moze byt aj godan aj ichidan pretoze sa to moze aplikovat na -eru form, ktora z godan robi ichidan

        // e.g. minai -> miru
        d.add(basicSuffix("inai", VerbInflection.Form.NEGATIVE, mapOf("iru" to InflectableWordClass.VERB_ICHIDAN)))
        // e.g. mitai -> miru
        d.add(basicSuffix("itai", VerbInflection.Form.WANT, mapOf("u" to InflectableWordClass.VERB_GODAN, "iru" to InflectableWordClass.VERB_ICHIDAN)))
        // further deinflect -eru
        d.add(EndsWithDeinflector("etai", VerbInflection.Form.WANT, mapOf("eru" to InflectableWordClass.VERB_ICHIDAN)))
        d.add(basicSuffix("eba", VerbInflection.Form.IF2, "u"))
        d.add(EndsWithDeinflector("emasu", VerbInflection.Form.ABLE_TO_DO2, mapOf("u" to InflectableWordClass.VERB_GODAN, "eru" to InflectableWordClass.VERB_ICHIDAN)))
        // kakimasu -> kaku, but unfortunately also kakiru
        d.add(EndsWithDeinflector("imasu", true, VerbInflection.Form.POLITE, mapOf("u" to InflectableWordClass.VERB_GODAN, "iru" to InflectableWordClass.VERB_ICHIDAN)))
        d.add(ShortVerbDeinflector("masu", VerbInflection.Form.POLITE, mapOf("ru" to InflectableWordClass.VERB_ICHIDAN)))
        d.add(basicSuffix("outosuru", null, "u"))
        d.add(basicSuffix("ou to suru", null, "u"))

        // this is dangerous - it will deinflect all ichidan verbs. however,
        // this rule is also required, to correctly deinflect e.g.
        // aetai. list as a last rule. Make the rule produce the old verb and
        // also the deinflected one.
        d.add(EruDeinflector())
        // -ite may be a godan -ku but also ichidan -iru verb
        d.add(basicSuffix("ita", VerbInflection.Form.PAST_TENSE, mapOf("ku" to InflectableWordClass.VERB_GODAN, "iru" to InflectableWordClass.VERB_ICHIDAN)))
        d.add(basicSuffix("ite", VerbInflection.Form.CONTINUATION, mapOf("ku" to InflectableWordClass.VERB_GODAN, "iru" to InflectableWordClass.VERB_ICHIDAN)))
        // this is purely for ichidan -eru verb
        d.add(basicSuffix("eta", VerbInflection.Form.PAST_TENSE, mapOf("eru" to InflectableWordClass.VERB_ICHIDAN)))
        d.add(ShortVerbDeinflector("ta", VerbInflection.Form.PAST_TENSE, mapOf("ru" to InflectableWordClass.VERB_ICHIDAN)))
        d.add(basicSuffix("ete", VerbInflection.Form.CONTINUATION, mapOf("eru" to InflectableWordClass.VERB_ICHIDAN)))

        d.add(basicSuffix("ida", VerbInflection.Form.PAST_TENSE, mapOf("gu" to InflectableWordClass.VERB_GODAN)))
        d.add(basicSuffix("ide", VerbInflection.Form.CONTINUATION, mapOf("gu" to InflectableWordClass.VERB_GODAN)))
        d.add(basicSuffix("tta", VerbInflection.Form.PAST_TENSE, mapOf("tu" to InflectableWordClass.VERB_GODAN, "u" to InflectableWordClass.VERB_GODAN, "ru" to InflectableWordClass.VERB_GODAN)))
        d.add(basicSuffix("tte", VerbInflection.Form.CONTINUATION, mapOf("tu" to InflectableWordClass.VERB_GODAN, "u" to InflectableWordClass.VERB_GODAN, "ru" to InflectableWordClass.VERB_GODAN)))
        d.add(NdaDeinflector())
        d.add(basicSuffix("nde", VerbInflection.Form.CONTINUATION, mapOf("nu" to InflectableWordClass.VERB_GODAN, "bu" to InflectableWordClass.VERB_GODAN, "mu" to InflectableWordClass.VERB_GODAN)))

        d.add(basicSuffix("inasai", VerbInflection.Form.SIMPLE_COMMAND, mapOf("u" to InflectableWordClass.VERB_GODAN)))
        d.add(basicSuffix("nasai", VerbInflection.Form.SIMPLE_COMMAND, mapOf("ru" to InflectableWordClass.VERB_ICHIDAN)))
        // -ro -e commands: https://github.com/mvysny/aedict/issues/792
        d.add(basicSuffix("ro", VerbInflection.Form.PLAIN_COMMAND, mapOf("ru" to InflectableWordClass.VERB_ICHIDAN)))
        d.add(Base4Deinflector())
        d.add(Base5Deinflector())
        // nope: would deinflect 待te na 待ru
//        d.add(ShortVerbDeinflector("te", VerbInflection.Form.CONTINUATION, "ru"))
        DEINFLECTORS = d
    }

    /**
     * Attempts to deinflect given verb or adjective.
     *
     * @param verb [sk.baka.aedict.kanji.RomanizationEnum.NihonShiki] romaji or hiragana verb or katakana term. Full-width katakana conversion is performed automatically. Must not be blank.
     * @return deinflected verb(s) or the original word if it couldn't get deinflected.
     */
    private fun deinflectImpl(verb: DeinflectableWord): VerbDeinflections {

        /**
         * @param deinflected NihonShiki romaji deinflected words, produced by `deinflection`
         * @receiver maps NihonShiki romaji deinflection output to the list of respective deinflections
         * @param deinflection the new deinflection to add
         * @param priorDeinflections current list of deinflections
         */
        fun MutableMap<String, List<VerbDeinflections.Deinflection>>.addAll(deinflected: Set<String>,
                                                                            deinflection: VerbDeinflections.Deinflection, priorDeinflections: List<VerbDeinflections.Deinflection>) {
            val d = ArrayList(priorDeinflections)
            d.add(deinflection)
            for (s in deinflected) {
                // if the rule did nothing (s.equals(deinflection.inflected)),
                // don't add it to the "deinflections" chain.
                put(s, if (s == deinflection.inflected) priorDeinflections else d)
            }
        }

        val deinflections = ArrayList<VerbDeinflections.Deinflection>()
        var deinflectedVerbs: MutableMap<String, List<VerbDeinflections.Deinflection>> = HashMap()
        val finalDeinflect = HashMap<String, List<VerbDeinflections.Deinflection>>()
        deinflectedVerbs.put(verb.word, emptyList())
        for (deinflector: AbstractDeinflector in DEINFLECTORS) {
            val newResult = HashMap(deinflectedVerbs)
            for (romaji: String in deinflectedVerbs.keys) {
                var deinflected: Map<String, EnumSet<InflectableWordClass>> = deinflector.deinflect(romaji) ?: mapOf()
                // remove all invalid deinflections, e.g. mite -> mku
                deinflected = deinflected.filterKeys { !IRomanization.NIHON_SHIKI.containsUntranslatableRomaji(it) }
                if (deinflected.isNotEmpty()) {
                    require(deinflected.keys.all { it.isNotEmpty() }) { "$deinflector on $romaji" }
                    // successfully deinflected. remove the old verb and add the
                    // deinflected one.
                    newResult.remove(romaji)
                    val d: VerbDeinflections.Deinflection = VerbDeinflections.Deinflection(romaji, deinflector.form, deinflected)
                    (if (deinflector.stopIfMatch()) finalDeinflect else newResult).addAll(deinflected.keys, d, deinflectedVerbs[romaji]!!)
                    deinflections.add(d)
                }
            }
            deinflectedVerbs = newResult
        }
        deinflectedVerbs.putAll(finalDeinflect)
        // if no deinflections have been performed, at least return the original word.
        if (deinflectedVerbs.isEmpty()) {
            deinflectedVerbs[verb.word] = emptyList()
        }
        // postprocess: modify words: remove x
        // @todo mavi why? x is important to convert romaji losslessly back to hiragana, e.g.
        // hexnai -> へんあい
//        for (entry in HashMap<String, List<VerbDeinflections.Deinflection>>(deinflectedVerbs).entries) {
//            deinflectedVerbs.remove(entry.key)
//            deinflectedVerbs[entry.key.replace("x", "")] = entry.value
//        }
        val originalInflectedVerb = verb.word//.replace("x", "")
        return VerbDeinflections(originalInflectedVerb, deinflectedVerbs, deinflections)
    }

    /**
     * Attempts to deinflect given verb.
     * @param verb [sk.baka.aedict.kanji.RomanizationEnum.NihonShiki] romaji or hiragana verb or katakana term.
     * Full-width katakana conversion is performed automatically. Must not be blank.
     * @param romanization if the entry is romaji, this is the romanization.
     * @param resolver Helps deinflect short verbs, may be null.
     * @return deinflected verb(s) or the original word if it couldn't get deinflected.
     */
    fun deinflect(verb: String, romanization: IRomanization, resolver: ReadingResolver?): VerbDeinflections {
        // fixes https://fabric.io/baka/android/apps/sk.baka.aedict3/issues/596e3abdbe077a4dccc11556
        val verb: DeinflectableWord = DeinflectableWord.of(verb, romanization) ?: return VerbDeinflections.cannotDeinflect(verb)
        return deinflect(verb, resolver)
    }

    /**
     * Attempts to deinflect given verb.
     * @param verb [sk.baka.aedict.kanji.RomanizationEnum.NihonShiki] romaji or hiragana verb or katakana term.
     * Full-width katakana conversion is performed automatically. Must not be blank.
     * @param romanization if the entry is romaji, this is the romanization.
     * @param resolver Pomaha odsklonovat kratke slovesa, moze byt null.
     * @return deinflected verb(s) or the original word if it couldn't get deinflected.
     */
    fun deinflect(verb: DeinflectableWord, resolver: ReadingResolver?): VerbDeinflections {
        var result = deinflectImpl(verb)
        // skus ci to nie je nahodou kratke sloveso, napr. 見ru. Ide o to ze, TAberu viem odsklonovat v pohode, staci mi suffix -beru.
        // ale 見ta odsklonovat neviem, -ta suffix mi nestaci. takze, ak je to taketo kratke sloveso (1 kanji + jeden hiragana znak),
        // skusim to kanji resolvnut na reading a potom by uz algoritmus mohol fungovat.
        if (resolver != null) {
            val kanjis = ArrayList(verb.word.getUniqueKanjis())
            val kanji = if (kanjis.size == 1 && verb.word.startsWith(kanjis[0].kanji())) kanjis[0] else null
            if (kanji != null && !isIrregular(kanji)) {
                // mozeme dostat ako zadanie odsklonovat napr. 見ta. Z toho deinflection nedokaze spravit 見ru, ale ak najprv
                // replacneme 見 za mi, z mita uz dame miru a uz len staci mi replacnut nazad na 見
                val ch: JpCharacter? = resolver.resolve(kanjis[0])
                if (ch != null) {
                    val verbWithReading = DeinflectableWord.of(ch.c + verb.word.substring(kanji.kanji().length), IRomanization.NIHON_SHIKI)
                    if (verbWithReading != null) {
                        val result2 = deinflectImpl(verbWithReading)
                        if (result2.shortestDeinflectionLength <= result.shortestDeinflectionLength || (result2.isSuccess && !result.isSuccess)) {
                            // vyzera ze mame lepsi match
                            result = result2.replacePrefix(ch, kanji)
                        }
                    }
                }
            }
        }

        return result
    }

    /**
     * algoritmus pre kratke slovesa nefunguje s nepravidelnymi slovesami, lebo napr. pre kuru vrati reading ku, ale vysklonovane je kimasita.
     * @param kanji kanji
     * @return true ak je irregular, kuru alebo suru
     */
    private fun isIrregular(kanji: Kanji): Boolean = kanji.`is`('来') // @todo mvy suru
}
