/**
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.inflection

import sk.baka.aedict.inflection.VerbDeinflector.ReadingResolver
import sk.baka.aedict.kanji.*
import java.io.Serializable
import java.util.*

/**
 * Attempts to deinflect a word, either a verb or an adjective.
 */
interface AbstractDeinflector {

    /**
     * Returns the originating form. Returns non-null value only after [deinflect] has been called.
     * @return originating form, may be null if the form is not registered in [sk.baka.aedict.inflection.VerbInflection.Form] list.
     */
    val form: VerbInflection.Form?

    /**
     * Tries to deinflect a verb.
     * @param romaji a verb in lower-case, trimmed NihonShiki romaji. Not blank. Moze obsahovat aj kanji - ak obsahuje a nic sa nedeinflectne,
     * potom je este raz deinflectnute, ale kanji nahradene za reading resolverom, pozri [ReadingResolver].
     * @return deinflected verb, or a multiple verbs if there are multiple
     * possibilities to deinflect. NihonShiki romaji. If this rule cannot be applied to deinflect the verb, null or an empty array should be returned. The set must not contain blank strings.
     * Maps deinflected verb to a set of word classes this deinflection only applies to.
     */
    fun deinflect(romaji: String): Map<String, EnumSet<InflectableWordClass>>?

    /**
     * If true then there is nothing more to deinflect and the process can be safely stopped. Invoked after [deinflect].
     * @return true if there is nothing more to deinflect, false if the deinflection should continue.
     */
    fun stopIfMatch(): Boolean
}

/**
 * Deinflects a short verb such as `着ku/tsuku`.
 */
class ShortVerbDeinflector(
        val endsWith: String,
        override val form: VerbInflection.Form?,
        val replaceBy: Map<String, EnumSet<InflectableWordClass>>
) : AbstractDeinflector {
    constructor(endsWith: String, form: VerbInflection.Form?,
                vararg replaceBy: String) : this(endsWith, form, replaceBy.toList().associateWith { InflectableWordClass.VERB_GENERIC })

    init {
        require(endsWith.isNotBlank()) { "endsWith is blank" }
        require(replaceBy.isNotEmpty()) { "Parameter replaceBy: invalid value: empty list" }
    }

    override fun deinflect(romaji: String): Map<String, EnumSet<InflectableWordClass>>? {
        require(romaji.isNotBlank()) { "romaji is blank" }
        if (!isMatch(romaji)) return null // nothing matched
        val verbPart: String = romaji.substring(0, romaji.length - endsWith.length)
        return replaceBy.mapKeys { verbPart + it.key }
    }

    private fun isMatch(romaji: String): Boolean =
            matches(romaji, endsWith)

    override fun stopIfMatch(): Boolean = false

    companion object {
        fun matches(romaji: String, ending: String): Boolean =
                romaji.length > 1 && romaji[0].isKanji && romaji.substring(1) == ending
    }
}

/**
 * Deinflects a verb if it ends with one of the following strings. Doesn't deinflect if entire verb matches [endsWith] -
 * use [EntireWordDeinflector] for that.
 * @property endsWith a non-empty non-null list of possible endings, lower-case trimmed romaji.
 * @property isStopIfMatch defines the return value of [stopIfMatch].
 * @property form the form of the verb.
 * @param replaceBy the ending is replaced by this string. Not empty.
 */
open class EndsWithDeinflector(private val endsWith: String,
                                       private val isStopIfMatch: Boolean, override val form: VerbInflection.Form?,
                                       private val replaceBy: Map<String, EnumSet<InflectableWordClass>>) : AbstractDeinflector {

    /**
     * Deinflects a verb if it ends with one of the following strings.
     * @param endsWith a non-empty non-null ending, lower-case trimmed romaji.
     * @param form the form of the verb, may be null.
     * @param replaceBy the ending is replaced by this string.
     */
    constructor(endsWith: String, form: VerbInflection.Form?, replaceBy: Map<String, EnumSet<InflectableWordClass>>) : this(endsWith, false, form, replaceBy)

    constructor(endsWith: String, form: VerbInflection.Form?, vararg replaceBy: String) : this(endsWith, false, form, *replaceBy)

    constructor(endsWith: String,
                isStopIfMatch: Boolean, form: VerbInflection.Form?,
                vararg replaceBy: String) : this(endsWith, isStopIfMatch, form, replaceBy.toList().associateWith { InflectableWordClass.VERB_GENERIC })

    init {
        require(endsWith.isNotBlank()) { "endsWith is blank" }
        require(replaceBy.isNotEmpty()) { "Parameter replaceBy: invalid value: empty list" }
    }

    override fun deinflect(romaji: String): Map<String, EnumSet<InflectableWordClass>>? {
        require(romaji.isNotBlank()) { "romaji is blank" }
        if (!isMatch(romaji)) return null // nothing matched
        val verbPart: String = romaji.substring(0, romaji.length - endsWith.length)
        return replaceBy.mapKeys { verbPart + it.key }
    }

    private fun isMatch(romaji: String): Boolean = romaji.endsWith(endsWith) && romaji != endsWith

    override fun stopIfMatch(): Boolean = isStopIfMatch
}

/**
 * Deinflects a verb if it ends with one of the following strings. Also deinflects if entire verb matches.
 * @property endsWith a non-empty non-null list of possible endings, lower-case trimmed romaji.
 * @property isStopIfMatch defines the return value of [stopIfMatch].
 * @property form the form of the verb.
 * @param replaceBy the ending is replaced by this string. Not empty.
 */
class SuffixDeinflector(private val endsWith: String,
                               private val isStopIfMatch: Boolean, override val form: VerbInflection.Form?,
                               private val replaceBy: Map<String, EnumSet<InflectableWordClass>>) : AbstractDeinflector {

    constructor(endsWith: String, form: VerbInflection.Form?, vararg replaceBy: String) : this(endsWith, false, form, *replaceBy)

    constructor(endsWith: String, form: VerbInflection.Form?, replaceBy: Map<String, EnumSet<InflectableWordClass>>) : this(endsWith, false, form, replaceBy)

    constructor(endsWith: String,
                isStopIfMatch: Boolean, form: VerbInflection.Form?,
                vararg replaceBy: String) : this(endsWith, isStopIfMatch, form, replaceBy.toList().associateWith { InflectableWordClass.VERB_GENERIC })

    init {
        require(endsWith.isNotBlank()) { "endsWith is blank" }
        require(replaceBy.isNotEmpty()) { "Parameter replaceBy: invalid value: empty list" }
        require(this.replaceBy.keys.all { it.isNotBlank() }) { "Contains blanks: ${this.replaceBy}" }
    }

    override fun deinflect(romaji: String): Map<String, EnumSet<InflectableWordClass>>? {
        require(romaji.isNotBlank()) { "romaji is blank" }
        if (!isMatch(romaji)) return null // nothing matched
        val verbPart: String = romaji.substring(0, romaji.length - endsWith.length)
        return replaceBy.mapKeys { verbPart + it.key }
    }

    private fun isMatch(romaji: String): Boolean = romaji.endsWith(endsWith)

    override fun stopIfMatch(): Boolean = isStopIfMatch
}

/**
 * Deinflects a verb if it is exactly [verb].
 * @property verb lower-case trimmed romaji.
 * @property isStopIfMatch defines the return value of [stopIfMatch].
 * @property form the form of the verb.
 * @param replaceBy the verb is deinflected to keys in this map. Not empty.
 */
open class EntireWordDeinflector(private val verb: String,
                               private val isStopIfMatch: Boolean, override val form: VerbInflection.Form?,
                               private val replaceBy: Map<String, EnumSet<InflectableWordClass>>) : AbstractDeinflector {

    constructor(endsWith: String, isStopIfMatch: Boolean = false, form: VerbInflection.Form?, vararg replaceBy: String)
            : this(endsWith, isStopIfMatch, form, replaceBy.toList().associateWith { InflectableWordClass.VERB_GENERIC })

    init {
        require(verb.isNotBlank()) { "endsWith is blank" }
        require(replaceBy.isNotEmpty()) { "Parameter replaceBy: invalid value: empty list" }
        require(this.replaceBy.keys.all { it.isNotBlank() }) { "Contains blanks: ${this.replaceBy}" }
    }

    override fun deinflect(romaji: String): Map<String, EnumSet<InflectableWordClass>>? {
        require(romaji.isNotBlank()) { "romaji is blank" }
        return if (romaji == verb) replaceBy else null
    }

    override fun stopIfMatch(): Boolean = isStopIfMatch
}

/**
 * @property word slovo ktore obsahuje len kanji a [sk.baka.aedict.kanji.RomanizationEnum.NihonShiki] romaji, trimmed, not blank.
 */
data class DeinflectableWord(val word: String) {
    init {
        require(word.isNotBlank()) { "word is blank" }
        require(word.replace("x", "").isNotEmpty()) { "$word only contains x characters, nothing to deinflect" }
        require(word.trim() == word) { "$word has not been trimmed" }
        require(!word.containsHiragana() && !word.containsKatakana()) { "$word contains hiragana or katakana" }
    }

    companion object {
        /**
         * Pripravi dane slovo na deinflection.
         * @param word slovo, not blank.
         * @param romanization if [word] is in romaji, this is the romanization.
         * @return deinflectable word or null if the word only contains garbage
         */
        fun of(word: String, romanization: IRomanization): DeinflectableWord? {
            @Suppress("NAME_SHADOWING")
            var word = word
            require(word.isNotBlank()) { "word is blank" }
            // just a bunch of "x" characters. skip.
            if (word.replace("x", "").isEmpty()) return null
            word = word.trim()
            word = word.halfwidthToKatakana()
            // we need the "x" characters, so that we won't deinflect e.g. "へんあい" the wrong way:
            // as henai -> heru.
            word = RomanizationEnum.NihonShiki.r.toRomaji(romanization.toHiragana(word), true)
            if (word.isBlank() || word.containsHiragana() || word.containsKatakana() || word.endsWith("xtu")) {
                // for example a lone xtsu ("っ") will cause the engine to crash :-)
                // https://code.google.com/p/aedict/issues/detail?id=300
                return null
            }
            return DeinflectableWord(word)
        }
    }
}

/**
 * Deinflects words. Works for both verbs and adjectives. Produces [Deinflections].
 */
class Deinflector(val resolver: ReadingResolver?) {
    fun deinflect(word: String, romanization: IRomanization): Deinflections {
        val w: DeinflectableWord = DeinflectableWord.of(word, romanization) ?: return Deinflections.cannotDeinflect(word)
        return deinflect(w)
    }
    fun deinflect(word: DeinflectableWord): Deinflections {
        val verb: VerbDeinflections = VerbDeinflector.deinflect(word, resolver)
        val adj: VerbDeinflections.Deinflection? = deinflectAdjective(word)
        return Deinflections(verb, adj)
    }

    private fun deinflectAdjective(word: DeinflectableWord): VerbDeinflections.Deinflection? {
        val adjectiveDeinflector = IAdjectiveDeinflector()
        val deinflected: Map<String, EnumSet<InflectableWordClass>> = adjectiveDeinflector.deinflect(word.word) ?: return null
        if (deinflected.isEmpty()) return null
        check(deinflected.size <= 2)
        return VerbDeinflections.Deinflection(word.word, adjectiveDeinflector.form, deinflected)
    }
}

/**
 * The outcome of a word deinflection. Use [Deinflector.deinflect] to produce this.
 * @property verb if the word was a verb and was successfully deinflected.
 * @property adj if the word was an adjective and it was possible to deinflect. Note: [VerbDeinflections.Deinflection.deinflected] will contain exactly one entry.
 */
data class Deinflections(val verb: VerbDeinflections, val adj: VerbDeinflections.Deinflection?) : Serializable {
    /**
     * true if there are no deinflections; false if I was able to perform at least one deinflection.
     */
    val isEmpty: Boolean get() = adj == null && verb.isEmpty

    /**
     * All possible terminal deinflections of source word in [sk.baka.aedict.kanji.RomanizationEnum.NihonShiki]
     * romanization. If the expression cannot be deinflected the
     * expression is returned. Never null, never empty, never contains blank strings.
     *
     * Under a special circumstances (e.g. when the input can not be
     * transcribed losslessly to romaji, e.g. small tsu "っ") this will hold the input.
     *
     * Immutable.
     */
    fun getTerminalDeinflections(): Set<String> {
        val set: Set<String> = adj?.deinflected?.keys ?: setOf()
        return set + verb.deinflectedVerbs
    }

    /**
     * @param deinflected the deinflected word from [getTerminalDeinflections], in [sk.baka.aedict.kanji.RomanizationEnum.NihonShiki] romanization.
     * @return list of deinflections, never null, may be empty if the original verb is provided. Unmodifiable.
     * Lists the deinflections in the same order in which they were applied to the input word: first
     * the deinflection #0, then #1 etc.
     */
    operator fun get(deinflected: String): List<VerbDeinflections.Deinflection> {
        if (adj != null && adj.deinflected.keys.first() == deinflected) {
            return listOf(adj)
        }
        return verb[deinflected]
    }

    /**
     * Tries to infer the word class, based on which deinflection rules were used.
     * @param deinflected the deinflected word from [getTerminalDeinflections], in [sk.baka.aedict.kanji.RomanizationEnum.NihonShiki] romanization.
     * @return detected word class. For example if the adjective deinflection succeeded, this will be
     * [InflectableWordClass.Adjective].
     */
    fun getWordClass(deinflected: String): EnumSet<InflectableWordClass> {
        if (adj != null && adj.deinflected.keys.first() == deinflected) {
            return EnumSet.of(InflectableWordClass.Adjective)
        }

        val result = EnumSet.noneOf(InflectableWordClass::class.java)
        for (deinflection in verb[deinflected]) {
            val filtered: Map<String, EnumSet<InflectableWordClass>> =
                deinflection.deinflected.filter { it.key == deinflected.replace("x", "") }
            for (def: EnumSet<InflectableWordClass> in filtered.values) {
                result.addAll(def)
            }
        }
        return result
    }

    companion object {
        fun cannotDeinflect(word: String): Deinflections = Deinflections(VerbDeinflections.cannotDeinflect(word), null)
    }
}

enum class InflectableWordClass {
    Adjective,
    VerbGodan,
    VerbIchidan,
    /**
     * The "Kuru" verb only: [sk.baka.aedict.dict.DictCode.vk]
     */
    VerbKuru,
    /**
     * Nouns ending with suru, e.g. 面suru: [sk.baka.aedict.dict.DictCode.vss]
     */
    VerbSuruClass,
    /**
     * The "Suru" verb only: [sk.baka.aedict.dict.DictCode.vsi]
     */
    VerbSuru;

    companion object {
        /**
         * An adjective.
         */
        val ADJ: EnumSet<InflectableWordClass> = EnumSet.of(InflectableWordClass.Adjective)

        /**
         * Any verb.
         */
        val VERB_GENERIC: EnumSet<InflectableWordClass> = EnumSet.of(VerbIchidan, VerbGodan, VerbKuru, VerbSuru, VerbSuruClass)

        /**
         * Ichidan verb.
         */
        val VERB_ICHIDAN: EnumSet<InflectableWordClass> = EnumSet.of(VerbIchidan)

        /**
         * Godan verbs
         */
        val VERB_GODAN: EnumSet<InflectableWordClass> = EnumSet.of(VerbGodan)
        /**
         * Nouns ending with suru, e.g. 面suru: [sk.baka.aedict.dict.DictCode.vss]
         */
        val VERB_SURU_CLASS: EnumSet<InflectableWordClass> = EnumSet.of(VerbSuruClass)

        /**
         * The "Kuru" verb only: [sk.baka.aedict.dict.DictCode.vk]
         */
        val VERB_KURU: EnumSet<InflectableWordClass> = EnumSet.of(VerbKuru)
        /**
         * The "Suru" verb only: [sk.baka.aedict.dict.DictCode.vsi]
         */
        val VERB_SURU: EnumSet<InflectableWordClass> = EnumSet.of(VerbSuru)
    }
}
