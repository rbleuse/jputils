/**
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.inflection

import sk.baka.aedict.dict.JMDictEntry
import sk.baka.aedict.kanji.IRomanization
import sk.baka.aedict.kanji.JpCharacter
import sk.baka.aedict.kanji.Kanji
import sk.baka.aedict.util.Boxable
import sk.baka.aedict.util.Unboxable
import sk.baka.aedict.util.typedmap.Box

import java.io.Serializable
import java.util.*

/**
 * The product of the VERB deinflection (so no adjectives or anything, just the verbs). Accidentally may deinflect adjectives,
 * ale nie vsetky, takze lepsie je spoliehat sa na [Deinflections.adj].
 * @property originalInflectedVerb The original verb being deinflected. in [sk.baka.aedict.kanji.RomanizationEnum.NihonShiki]
 * romanization
 * @property deinflections Maps deinflected form (in [sk.baka.aedict.kanji.RomanizationEnum.NihonShiki]
 * romanization) to the list of deinflections performed on the original verb. Must not be empty.
 *
 * If the expression cannot be deinflected tak tato mapa obsahuje len jeden kluc [originalInflectedVerb] mapovany na prazdny list.
 * @property allDeinflections Returns all deinflections performed on [originalInflectedVerb], in
 * the order in which they were performed.
 * @author Martin Vysny
 */
data class VerbDeinflections internal constructor(@JvmField val originalInflectedVerb: String, private val deinflections: Map<String, List<Deinflection>>, @JvmField val allDeinflections: List<Deinflection>) : Serializable {
    /**
     * All possible deinflections of source verb in [sk.baka.aedict.kanji.RomanizationEnum.NihonShiki]
     * romanization. If the expression cannot be deinflected, the
     * expression is returned. Never null, never empty, never contains blank strings.
     *
     * In a very special case (e.g. when the input cannot be converted to romaji losslessly, say small tsu "っ")
     * this will contain the un-romanizable character.
     *
     * Immutable.
     */
    val deinflectedVerbs: Set<String>
        get() = Collections.unmodifiableSet(deinflections.keys)

    /**
     * true if there are no deinflections. That means that [deinflectedVerbs] only holds the [originalInflectedVerb].
     * @return true if there are no deinflections, false if at least one deinflection was performed.
     */
    val isEmpty: Boolean get() = deinflections.values.all { it.isEmpty() }

    val shortestDeinflectionLength: Int get() = deinflections.keys.map { it.length }.minOrNull()!!

    /**
     * True if we managed to deinflect to base3.
     */
    val isSuccess: Boolean get() = deinflectedVerbs.any { it.endsWith("u") }

    /**
     * Guesses shortest deinflection chain for given entry.
     * @param e the entry
     * @return deinflection chain or null alebo prazdny list ak ziadny reading nie je medzi [deinflectedVerbs].
     */
    fun guess(e: JMDictEntry): List<Deinflection>? {
        var result: List<Deinflection>? = null
        // prejdi pekne aj kanjis, co ked odsklonuvame 見ru?
        val kanjiAndReading = ArrayList<String>()
        for (kd in e.kanji) {
            kanjiAndReading.add(IRomanization.NIHON_SHIKI.toRomaji(kd.kanji))
        }
        for (readingData in e.reading) {
            kanjiAndReading.add(IRomanization.NIHON_SHIKI.toRomaji(readingData.reading))
        }
        for (s in kanjiAndReading) {
            val n = deinflections[s]
            if (n != null) {
                if (result == null || result.size > n.size) {
                    result = n
                }
            }
        }
        return result
    }

    /**
     * Returns deinflections used to deinflect given word.
     * @param deinflected the deinflected word from [deinflectedVerbs], in [sk.baka.aedict.kanji.RomanizationEnum.NihonShiki] romanization.
     * @return list of deinflections, never null, may be empty if the original verb is provided. Unmodifiable.
     * Na slovo boli postupne aplikovane deinflection 0, potom 1, atd atd, az sme dostali vysledne slovo.
     */
    operator fun get(deinflected: String): List<Deinflection> {
        val result = deinflections[deinflected] ?: throw IllegalArgumentException("Parameter deinflected: invalid value $deinflected: must be one of $deinflectedVerbs")
        return Collections.unmodifiableList(result)
    }

    init {
        require(originalInflectedVerb.isNotBlank())
        require(!deinflections.isEmpty()) { "Parameter deinflectedVerbs: invalid value $deinflections: must not be empty" }
    }

    /**
     * In all inflections and deinflections, try to replace given prefix String with the new one.
     * Deinflections with no such prefixes are removed.
     * @param oldPrefix the old prefix.
     * @param newPrefix the new prefix
     * @return a copy of deinflection list with the new prefix.
     */
    fun replacePrefix(oldPrefix: JpCharacter, newPrefix: Kanji): VerbDeinflections {
        val oldToNew = HashMap<Deinflection, Deinflection>()
        for (d in allDeinflections) {
            val d2 = d.replacePrefix(oldPrefix, newPrefix)
            if (d2 != null) oldToNew.put(d, d2)
        }
        val allDeinflections = map(this.allDeinflections, oldToNew, false)
        val deinflections = HashMap<String, List<Deinflection>>()
        for ((key1, value) in this.deinflections) {
            val key = replacePrefix2(key1, oldPrefix, newPrefix)
            if (key != key1) {
                val map = map(value, oldToNew, true)
                if (map != null) {
                    // fixes NPE in isEmpty() - reported by a user.
                    deinflections[key] = map
                }
            }
        }
        val originalInflectedVerb = replacePrefix2(this.originalInflectedVerb, oldPrefix, newPrefix)
        if (deinflections.isEmpty()) {
            deinflections.put(originalInflectedVerb, ArrayList())
        }
        return VerbDeinflections(originalInflectedVerb, deinflections, allDeinflections!!)
    }

    /**
     * One deinflection step. By applying the rule [inflectedForm] will [inflected] word become [deinflected].
     * @property inflectedForm May be null if inflection form is not listed in the [sk.baka.aedict.inflection.VerbInflection.Form] list.
     * @property
     */
    class Deinflection(inflected: String, @JvmField val inflectedForm: VerbInflection.Form?, deinflected: Map<String, EnumSet<InflectableWordClass>>) : Serializable, Boxable {
        /**
         * [sk.baka.aedict.kanji.RomanizationEnum.NihonShiki] romanized inflected verb. Not blank.
         */
        @JvmField
        val inflected: String = inflected.replace("x", "")
        /**
         * [sk.baka.aedict.kanji.RomanizationEnum.NihonShiki] romanized possible deinflections. Not null, not empty, does not contain blanks
         * nor 'x'.
         */
        @JvmField
        val deinflected: Map<String, EnumSet<InflectableWordClass>>

        init {
            require(this.inflected.isNotBlank()) { "Invalid value $inflected - blank" }
            require(deinflected.isNotEmpty()) { "deinflected is empty" }
            this.deinflected = deinflected.mapKeys { it.key.replace("x", "") }
            require(this.deinflected.keys.all { it.isNotBlank() }) { "Invalid value $deinflected: contains blanks" }
        }

        fun replacePrefix(oldPrefix: JpCharacter, newPrefix: Kanji): Deinflection? {
            val deinflected = mutableMapOf<String, EnumSet<InflectableWordClass>>()
            for (d in this.deinflected.keys) {
                val e = replacePrefix2(d, oldPrefix, newPrefix)
                if (e != d) {
                    deinflected[e] = this.deinflected.getValue(d)
                }
            }
            return if (deinflected.isEmpty()) null else Deinflection(replacePrefix2(inflected, oldPrefix, newPrefix), inflectedForm, deinflected)
        }

        override fun toString() = "$deinflected+$inflectedForm=$inflected"

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other == null || javaClass != other.javaClass) return false
            val that = other as Deinflection?
            if (inflected != that!!.inflected) return false
            return if (inflectedForm !== that.inflectedForm) false else deinflected == that.deinflected
        }

        override fun hashCode(): Int {
            var result = inflected.hashCode()
            result = 31 * result + (inflectedForm?.hashCode() ?: 0)
            result = 31 * result + deinflected.hashCode()
            return result
        }

        override fun box(): Box {
            val box = Box().putString("inflected", inflected)
                    .putEnum<VerbInflection.Form>("inflectedForm", inflectedForm)
                    .putListOfBasics("deinflected", deinflected.keys)
            deinflected.keys.forEach { box.putEnumSet("w_$it", deinflected[it]) }
            return box
        }

        companion object : Unboxable<Deinflection> {
            private const val serialVersionUID = 1L

            @JvmStatic override fun unbox(box: Box): Deinflection {
                val d = box.get("deinflected").listOf(String::class.java).get()
                val d2 = d.associateWith {
                    box.get("w_$it").enumSet(InflectableWordClass::class.java).or(EnumSet.allOf(InflectableWordClass::class.java))
                }
                return Deinflection(box.get("inflected").string().get(),
                        box.get("inflectedForm").enumValue(VerbInflection.Form::class.java).orNull(),
                        d2)
            }
        }
    }

    override fun toString() = "Deinflections{originalInflectedVerb='$originalInflectedVerb', deinflections=$deinflections}"

    companion object {

        /**
         * @param originalVerb nihon-shiki romanization
         * @return
         */
        @JvmStatic
        fun cannotDeinflect(originalVerb: String): VerbDeinflections =
                VerbDeinflections(originalVerb, Collections.singletonMap(originalVerb, emptyList()),
                        emptyList())

        private fun <T> map(originalList: List<T>, map: Map<T, T>, nullIfEmpty: Boolean): List<T>? {
            val result = ArrayList<T>(originalList.size)
            for (t in originalList) {
                val t2 = map[t]
                if (t2 != null) {
                    result.add(t2)
                }
            }
            return if (nullIfEmpty && result.isEmpty()) null else result
        }

        private fun replacePrefix2(nihonSiki: String, oldPrefix: JpCharacter, newPrefix: Kanji): String {
            val r = IRomanization.NIHON_SHIKI.toRomaji(oldPrefix.c)
            return if (nihonSiki.startsWith(r)) newPrefix.kanji() + nihonSiki.substring(r.length) else nihonSiki
        }
    }
}
