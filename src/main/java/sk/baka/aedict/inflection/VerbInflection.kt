/**
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.inflection

import sk.baka.aedict.dict.DictCode
import sk.baka.aedict.dict.JMDictEntry
import sk.baka.aedict.kanji.*
import java.io.Serializable
import java.util.*
import java.util.regex.Pattern

/**
 * Holds rules for verb inflection and also provides examples of inflected
 * verbs.
 *
 * @author Martin Vysny
 */
object VerbInflection {

    /**
     * Lists all inflectors for all 5 bases + TE/TA base.
     */
    @JvmField val INFLECTORS = Collections.unmodifiableList(Arrays.asList(Base1Inflector(), Base2Inflector(), Base3Inflector(), Base4Inflector(),
            Base5Inflector(), BaseTeInflector(), BaseTaInflector()))

    @Deprecated("")
    @JvmStatic fun isPossiblyIchidan(verb: String): Boolean = VerbType.isPossiblyIchidan(verb)

    /**
     * Holds information about a verb inflection form.
     * @property inflector the verb inflector, denotes the required base of the verb.
     * @property suffix additional suffix to add to the inflected verb, only applicable to Godan verbs.
     * @property suffixIchidan additional suffix to add to the inflected verb, only applicable to Ichidan verbs.
     * @property basic If true this expression is widely used.
     * @property explanationResId Explanation of the form (e.g. I don't do something).
     * explanation of the form (e.g. I don't do something).
     * @property examples a new-line-separated list of example sentences, first in
     * Japanese (a [RomanizationEnum.Hepburn]-romanized),
     * then in English.  The
     * Japanese are directly taken from Tim Matheson's site, with very minor
     * alternations. Therefore, you'll have to:
     *
     *  * Convert the jp string to lower case
     *  * Convert a lone-standing wa into ha
     *  * Convert a lone o to wo
     *  * Convert _text_ to katakana
     *
     * @property officialName the inflection name like Informal Past. May be null if not known.
     * @property url the http link with more information on the inflection, e.g. http://ww8.tiki.ne.jp/~tmath/language/jpverbs/lesson8.htm
     * @author Martin Vysny
     */
    enum class Form(private val inflector: AbstractBaseInflector,
                    private val suffix: String,
                    private val suffixIchidan: String?,
                    @JvmField val basic: Boolean,
                    @JvmField val explanationResId: String,
                    @JvmField val examples: String,
                    @JvmField val officialName: String?,
                    @JvmField val url: String?) {
        /**
         * The verb's plain form - I do something:
         * http://www.timwerx.net/language/jpverbs/lesson1.htm
         */
        PLAIN(Base3Inflector(), "", true, "I do something.",
                "Mama wa mise de _banana_ o kau.\nMom buys/will buy bananas at the store.\n_Jimu_ wa manga o yomu.\nJim will read a comic book.\nOjii-san wa sugu kaeru.\nGrandpa will return soon.\nWatashi wa ringo o taberu.\nI'll eat an apple.\n_Naomi_ wa _terebi_ o miru.\nNaomi will watch TV.",
                "Informal Present", lesson2(1)),
        /**
         * The verb's polite plain form (I do something):
         * http://www.timwerx.net/language/jpverbs/lesson2.htm
         */
        POLITE(Base2Inflector(), "masu", true, "I do something. (politely)",
                "Mama wa mise de _banana_ o kaimasu.\nMom buys/will buy bananas at the store.\n_jimu_ wa manga o yomimasu.\nJim will read a comic book.\nOjii-san wa sugu kaerimasu.\nGrandpa will return soon.\nWatashi wa ashita kimemasu.\nI'll decide tomorrow.\n_jeri_ wa sugu heya kara demasu.\nJerry will come out of the room soon.\nAyako wa mainichi _terebi_ o mimasu.\nAyako watches the TV every day.", "Formal Present", lesson2(2)),
        /**
         * The verb's polite negative form (I do not do something):
         * http://www.timwerx.net/language/jpverbs/lesson4.htm
         */
        POLITE_NEGATIVE(Base2Inflector(), "masen", true, "I do not do something. (politely)",
                "Watashi wa kasa o kaimasen.\nI'm not going to buy an umbrella.\nKare wa machimasen.\nHe won't wait.\nKimiko wa oosaka ni ikimasen.\nKimiko isn't going to Osaka.\nWatashi wa ima tabemasen.\nI'm not going to eat now.\nKanojo wa kasa o karimasen.\nShe isn't going to borrow an umbrella.", "Formal Negative", lesson2(4)),
        /**
         * The verb's polite past form (I did something):
         * http://www.timwerx.net/language/jpverbs/lesson5.htm
         */
        POLITE_PAST(Base2Inflector(), "masita", true, "I did something. (politely)",
                "_jon_ wa Sendai ni ikimashita.\nJohn went to Sendai.\nKodomotachi wa kouen de asobimashita.\nThe children played at the park.\nYoshi wa ringo o tabemashita.\nYoshi ate an apple.\nShizu wa manga o kaimashita.\nShizu bought a comic book.\n_Bobbu_ wa sono eiga o mimashita.\nBob saw that movie.", "Formal Past", lesson2(4)),
        /**
         * The verb's polite past negative form (I didn't do something):
         * http://www.timwerx.net/language/jpverbs/lesson6.htm
         */
        POLITE_PAST_NEGATIVE(Base2Inflector(), "masen desita", true,
                "I didn't do something. (politely)",
                "_jon_ wa Sendai ni ikimasen deshita.\nJohn didn't go to Sendai.\nKodomotachi wa kouen de asobimasen deshita.\nThe children didn't play at the park.\nYoshi wa ringo o tabemasen deshita.\nYoshi didn't eat an apple.", "Formal Past Negative", lesson2(4)),
        /**
         * To hell with official names :-) The verb's "want" form:
         * http://www.baka.sk/~tmath/language/jpverbs/lesson8.htm
         */
        WANT(Base2Inflector(), "tai", true, "I want to do something.",
                "Watashi wa kasa o kaitai.\nI want to buy an umbrella.\nKodomotachi wa asobitai.\nThe children want to play.\n_Bobbu_ wa tenpura o tabetai desu.\nBob wants to eat tempura.\n_Miki_ wa sono eiga o mitai desu.\nMiki wants to see that movie.\nWatakushi wa kasa o kaitakunai.\nI don't want to buy an umbrella.\nKodomotachi wa asobitakunai.\nThe children don't want to play.\n_Terebi_ o mitakereba, yuushoku o hayaku tabenasai.\nIf you want to watch TV, hurry and eat your dinner.\nShichiji no densha ni noritakereba, ashita hayaku okimashou.\nIf you want to make the 7:00 train, let's get up early tomorrow.", "Desire", lesson2(6)),
        /**
         * The verb's "Let's do something." form:
         * http://www.timwerx.net/language/jpverbs/lesson9.htm
         */
        LET_S(Base2Inflector(), "masyou", true, "Let's do something (politely)",
                "Ikimashou.\nLet's go.\nTabemashou.\nLet's eat.\nYasumimashou.\nLet's take a break.\nWatashi ga hakobimashou.\nI'll carry this / these [for you].\nEsa o agemashou.\n(To a pet) Let's get you some food.\nAnata no jitensha o naoshimashou.\nI'll fix your bicycle. / I'll help you fix your bicycle.", "Formal Volitional", lesson2(4)),
        /**
         * The verb's "Do something!" form:
         * http://www.timwerx.net/language/jpverbs/lesson10.htm
         */
        SIMPLE_COMMAND(Base2Inflector(), "nasai", false, "Do something!",
                "Tabenasai!\nEat!\nMinasai!\nLook!\nYominasai!\nRead it!\nIinasai!\nTell me!\nSuwarinasai!\nSit down!\nKoko ni kinasai!\nCome here!", "Imperative", lesson2(7)),
        /**
         * The verb's "I'm going to do something" form:
         * http://www.timwerx.net/language/jpverbs/lesson13.htm
         */
        GOING(Base2Inflector(), " ni iku", true, "I'm going to do something.",
                "Watashi wa kasa o kai ni iku.\nI'm going to go buy an umbrella.\nWatashi wa kasa o kai ni ikimasu.\nI'm going to go buy an umbrella.\nChuuka ryouri o tabe ni ikimashou.\nLet's go out and eat Chinese food.\nWatashi wa kouen ni asobi ni ikitai.\nI want to go play in the park.", null, lesson2(10)),
        /**
         * The verb's "I'm going to arrive" form:
         * http://www.timwerx.net/language/jpverbs/lesson13.htm
         */
        ARRIVE(Base2Inflector(), " ni kuru", true, "I'm going to arrive.",
                "_Miki_ wa watashi no atarashii _pasokon_ o mi ni kuru.\nMiki is coming over to see my new PC.\n_Miki_ wa watashi no atarashii _pasokon_ o mi ni kimashita.\nMiki came over to see my new PC.\n_Robbu_ wa jitensha o kari ni kimasen deshita.\nRob didn't come to borrow the bicycle.\nAsobi ni kite ne.\nCome over for a visit, okay? (Asobi ni kuru is a set phrase used to invite someone \"to come for a pleasure visit.\" You may hear it often, but don't take it literally. Most of the time it is just a polite nothing, made obvious by having no date or time attached to it.)", null, lesson2(10)),
        /**
         * The verb's "It is hard to do something" form:
         * http://www.timwerx.net/language/jpverbs/lesson14.htm
         */
        HARD_TO_DO(Base2Inflector(), "nikui", false, "It is hard to do something.",
                "Kono budou wa tabenikui.\nThese grapes are hard to eat.\nKono kanji wa yominikui.\nThese kanji are hard to read.\nSono tatemono wa minikui.\nThat building is hard to see. (Besides the converted verb minikui, which means \"hard to see,\" there is also an adjective minikui meaning \"ugly.\" Accordingly, the sentence sono tatemono wa minikui could also mean \"that building is ugly.\" Be especially careful to make the intended meaning clear when using it to refer to people or their property.)", null, lesson2(11)),
        /**
         * The verb's "It is easy to do something." form:
         * http://www.timwerx.net/language/jpverbs/lesson14.htm
         */
        EASY_TO_DO(Base2Inflector(), "yasui", false, "It is easy to do something.",
                "Kono _pasokon_ wa tsukaiyasui.\nThis PC is easy to use.\nKanojo no namae wa oboeyasui.\nHer name is easy to remember.\nKono kanji wa kakiyasui.\nThis kanji is easy to write.", null, lesson2(11)),
        /**
         * The verb's "I went too far doing something." form:
         * http://www.timwerx.net/language/jpverbs/lesson15.htm
         */
        GO_TOO_FAR(Base2Inflector(), "sugiru", false, "I went too far doing something.",
                "Kare wa itsumo nomisugiru.\nHe always drinks too much.\nKimiko wa tabesugimashita.\nKimiko ate too much.\nKodomotachi wa _terebi_ o misugiru.\nThe kids watch too much TV.", null, lesson2(12)),
        /**
         * The verb's "I did X while I was doing Y." form:
         * http://www.timwerx.net/language/jpverbs/lesson16.htm
         */
        WHILE_DOING(Base2Inflector(), "nagara", false, "I did X while I was doing Y.",
                "_Bobu_ wa hatarakinagara ongaku o kiku.\nBob listens to music while he works.\nKimiko wa benkyou shinagara _terebi_ o mimasen.\nKimiko doesn't watch TV while studying.\nHanashinagara sanpo shimashou.\nLet's take a walk while we talk.", null, lesson2(13)),
        /**
         * The verb's "I do not do something." form:
         * http://www.timwerx.net/language/jpverbs/lesson17.htm
         */
        NEGATIVE(Base1Inflector(), "nai", true, "I do not do something.",
                "_Jon_ wa kasa o kawanai.\nJohn isn't going to buy an umbrella.\n_Jimu_ wa manga o yomanai.\nJim doesn't read comic books.\nOjii-san wa sugu kaeranai desu.\nGrandpa isn't going to return soon.\nWatashi wa _terebi_ o minai desu.\nI'm not going to watch TV.\nSachiko wa konai.\nSachiko won't be coming.", "Informal Negative", lesson2(14)) {

            override fun inflect(verb: String, verbType: VerbType): String {
                return if (verb == "aru") {
                    "nai"
                } else super.inflect(verb, verbType)
            }
        },
        /**
         * The verb's "I probably do not do something." form:
         * http://www.timwerx.net/language/jpverbs/lesson18.htm
         */
        PROBABLE_NEGATIVE(Base1Inflector(), "nai desyou", false, "I probably do not do something.",
                "_Jon_ wa kasa o kawanai deshou.\nJohn probably isn't going to buy an umbrella.\n_Jimu_ wa manga o yomanai deshou.\nJim probably doesn't read comic books.\nYuki wa furanai deshou.\nIt probably won't snow.", null, lesson2(15)),
        /**
         * The verb's "I didn't do something." form:
         * http://www.timwerx.net/language/jpverbs/lesson19.htm
         */
        NEGATIVE_PAST(Base1Inflector(), "nakatta", true, "I didn't do something.",
                "Watashi wa _terebi_ o minakatta.\nI didn't watch TV.\nSachiko wa konakatta.\nSachiko didn't come.\nOjii-san wa shinbun o yomitakunakatta.\nGrandpa didn't want to read the newspaper.", "Informal Past Negative", lesson2(15)),
        /**
         * The verb's "If I do not do something" form:
         * http://www.timwerx.net/language/jpverbs/lesson20.htm
         */
        NEGATIVE_CONDITIONAL(Base1Inflector(), "nakereba", false, "If I do not do something",
                "Ojii-san ga sugu kaeranakereba watashi wa _makudonarudo_ ni ikimasu.\nIf Grandpa doesn't return soon I'm going to McDonald's.\n_Miki_ ga heya o tsukawanakereba Junko wa tsukaitai desu.\nIf Miki isn't going to use the room Junko wants to use it.\nNaoko wa kasa o karinakereba koukai suru deshou.\nIf Naoko doesn't borrow an umbrella she'll probably regret it.", "Informal Negative Conditional", lesson2(15)),
        /**
         * The verb's "I have to do something (It won't go otherwise)" form:
         * http://www.timwerx.net/language/jpverbs/lesson21.htm
         */
        HAS_TO(Base1Inflector(), "nakereba narimasen", true, "I have to do something (It won't go otherwise)",
                "_Jimu_ wa ima kaeranakereba narimasen.\nJim has to return now.\n_Raura_ wa kasa o kawanakereba narimasen.\nLaura has to buy an umbrella.\nKodomotachi wa tabenakereba narimasen.\nThe children must eat.\n_Jimu_ wa ima kaeranakereba naranai deshou.\nJim probably has to return now.\n_Raura_ wa kasa o kawanakereba naranai deshou.\nLaura probably needs to buy an umbrella.\nKodomotachi wa tabenakereba naranai deshou.\nThe children probably need to eat.", null, lesson2(15)),
        /**
         * The verb's "I'll let/have/make him do something" form:
         * http://www.timwerx.net/language/jpverbs/lesson22.htm
         */
        LET_HIM(Base1Inflector(), "seru", "saseru", false, "I'll let/have/make him do something",
                "Obaa-san wa kodomotachi ni soto de asobaseru.\nGrandma lets the children play outside.\nOkaa-chan wa Kimiko ni kasa o kawaseru.\nMom will have Kimiko buy an umbrella.\nSensei wa gakusei ni mainichi shinbun o yomaseru.\nThe teacher makes the students read the newspaper every day.\nRoku ji ni kodomotachi ni yuushoku o tabesaseru.\nI'll have the kids eat dinner at 6:00.\n_Jon_ ni raishuu made ni kimesaseru.\nI'll have John decide by next week.\nKare ni ashita kosaseru.\nI'll have him come tomorrow.\nOtou-san wa _Bobu_ ni benkyou saseru.\nDad will make Bob study.\nKanojo ni saseru.\nI'll have her do it.\nRitsuko wa Kumi ni _pen_ o kawasemashita.\nRitsuko had Kumi buy a pen.\nOjii-san wa kodomotachi ni ame o tabesasemasen.\nGrandpa won't let the children eat candy.\nWatashi wa Kenji ni eigo o benkyou sasetai desu.\nI want to have Kenji study English.\n_Jon_ ni mise ni ikasemashou.\nLet's have John go to the store.\nKodomotachi ni _terebi_ o misemashou ka.\nShall we let the kids watch TV?", "Causative", lesson2(16)) {

            override fun inflect(verb: String, verbType: VerbType): String = when {
                verb.endsWith("suru") || verb.endsWith("saru") -> verb.substring(0, verb.length - 4) + "saseru" // kudasaru -> kudasaseru
                verb.endsWith("為ru") -> verb.substring(0, verb.length - 3) + "saseru"
                else -> super.inflect(verb, verbType)
            }
        },
        /**
         * https://code.google.com/p/aedict/issues/detail?id=322
         */
        PASSIVE(Base1Inflector(), "reru", "rareru", false, "Something is being done",
                "Watashi wa otouto ni keeki wo taberaremashita.\nI got annoyed since the cake was eaten by my little brother. (My little brother ate the cake. Implicates that you somehow got negative feeling out of it. Used also in situations when someone steals something from you etc.)", "Passive", null) {

            override fun inflect(verb: String, verbType: VerbType): String = when {
                verb.endsWith("suru") || verb.endsWith("saru") -> verb.substring(0, verb.length - 4) + "sareru" // kudasaru -> kudasaserareru
                verb.endsWith("為ru") -> verb.substring(0, verb.length - 3) + "sareru"
                else -> super.inflect(verb, verbType)
            }
        },
        /**
         * https://code.google.com/p/aedict/issues/detail?id=322
         */
        CAUSATIVE_PASSIVE(Base1Inflector(), "serareru", "saserareru", false, "I was made to do something",
                "Watashi wa sensei ni _repooto_ wo kakasaremashita / kakaseraremashita.\nI was made to write a report by my teacher.", "Causative Passive", null) {

            override fun inflect(verb: String, verbType: VerbType): String = when {
                verb.endsWith("suru") || verb.endsWith("saru") -> verb.substring(0, verb.length - 4) + "saserareru" // kudasaru -> kudasaserareru
                verb.endsWith("為ru") -> verb.substring(0, verb.length - 3) + "saserareru"
                else -> super.inflect(verb, verbType)
            }
        },

        /**
         * The verb's "I did X without doing Y" form:
         * http://www.timwerx.net/language/jpverbs/lesson23.htm
         */
        DID_X_WITHOUT_DOING_Y(Base1Inflector(), "zu ni", false, "I did X without doing Y",
                "Kare wa yuushoku o tabezu ni nemashita.\nHe went to bed without eating dinner.\nKyou Shizuka wa kyoukasho o motazu ni gakkou ni kimashita.\nToday Shizuka came to school without her textbook.\n_Bobu_ wa maemotte denwa sezu ni _Jon_ no ie ni ikimashita.\nBob went to John's house without calling first.", null, null),
        /**
         * The verb's "I'll probably do something" form:
         * http://www.timwerx.net/language/jpverbs/lesson24.htm
         */
        PROBABLE(Base3Inflector(), " desyou", false, "I'll probably do something.",
                "Raishuu watashi wa Okayama ni iku deshou.\nI'll probably go to Okayama next week.\nKenji wa atarashii kuruma o kau deshou.\nKenji will probably buy a new car.\nAshita wa ame (ga furu) deshou.\nIt will probably rain tomorrow.\nOosaka ni iku deshou?\nYou're going to Osaka, aren't you? (Please note that ka is not added at the end - a rising intonation is used instead)\n_Sue_ wa kuru deshou?\nSue's coming, isn't she?\nTomoka wa eigo no shukudai o suru deshou?\nTomoka will do her English homework, right?", null, lesson2(17)),
        /**
         * The verb's "I plan to do something" form:
         * http://www.timwerx.net/language/jpverbs/lesson25.htm
         */
        PLAN(Base3Inflector(), " hazu", false, "I plan to do something.",
                "Raishuu watashi wa Oosaka ni iku hazu desu.\nI'm supposed to go to Osaka next week.\n_Jon_ wa sugu kuru hazu.\nJohn should be coming soon.\n_Bobu_ mo ikitai hazu.\nBob will probably also want to go.", null, lesson2(18)),
        /**
         * The verb's "I should do something" form:
         * http://www.timwerx.net/language/jpverbs/lesson26.htm
         */
        SHOULD(Base3Inflector(), " hou ga ii", true, "I should do something.",
                "Kanojo ni denwa suru hou ga ii.\nI/we should call her.\nWatashitachi wa sukoshi yasumu hou ga ii.\nWe had better rest a little.\nAnata wa motto nihongo o benkyou suru hou ga ii.\nYou should study Japanese more.\nKyou densha de iku hou ga ii.\nIt would be better to go by train today.\nRaishuu suru hou ga ii.\nIt would be better to do it next week.\nAto de taberu hou ga ii.\nIt would be better to eat later.\nYakiniku no hou ga ii.\nI'd rather have a barbeque.\nInu no hou ga ii.\nI'd rather get a dog.\nHawaii no hou ga ii.\nI'd rather go to Hawaii.", null, lesson2(19)),
        /**
         * The verb's "I don't know whether I do something or not." form:
         * http://www.timwerx.net/language/jpverbs/lesson27.htm
         */
        WHETHER_OR_NOT(Base3Inflector(), " ka dou ka", false, "I don't know whether I do something or not.",
                "Kare wa dekiru ka dou ka kikimashou.\nI'll ask him whether or not he can do it.\nWatashitachi wa iku ka dou ka mada wakarimasen.\nI don't know if we are going yet.\nHideki wa ashita yasumu ka dou ka wakarimasu ka.\nDo you know if Hideki has tomorrow off?", null, lesson2(20)),
        /**
         * The verb's "Maybe I'll do something." form:
         * http://www.timwerx.net/language/jpverbs/lesson28.htm
         */
        MAYBE(Base3Inflector(), " kamo siremasen", true, "Maybe I'll do something.",
                "Watashi wa raishuu Oosaka ni iku kamo shiremasen.\nMaybe I'll go to Osaka next week.\n_Jakku_ mo kuru kamo shiremasen.\nJack may also come.\nAshita yuki ga furu kamo shiremasen.\nIt might snow tomorrow.\nAshita ame ga furu kamo shirenai.\nIt might rain tomorrow.\nKonban watashitachi wa soto de taberu kamo shirenai.\nWe may eat out tonight.", null, lesson2(21)),
        /**
         * The verb's "Because of X..." form:
         * http://www.timwerx.net/language/jpverbs/lesson29.htm
         */
        BECAUSE_OF(Base3Inflector(), " kara", false, "Because of X...",
                "Ame ga furu kara kasa o motte ikimashou.\nSince it is going to rain, let's take umbrellas.\n_Bessu_ wa itsumo okureru kara denwa suru.\nBeth is always late, so I'll call her.\nKasa o motte ikimashou. Ame ga furu kara.\nLet's take umbrellas since it's going to rain.\nWatashi wa _Bessu_ ni denwa suru. Itsumo okureru kara.\nI'll call Beth because she's always late.\nGyuunyuu ga nai kara mise ni ikimasu.\nWe don't have any milk, so I'm going to the store.\nJisho o kaitai kara honya ni ikimasu.\nI'm going to the bookstore because I want to buy a dictionary.\nSuzuki-san no ie ni ikitakunai! Kare wa itsumo iya na mono o tabesaseru kara.\nI don't want to go to Mr. Suzuki's place because he always makes me eat nasty stuff.\nOngaku o kikimasu. _Terebi_ o mitakunai kara.\nI'm going to listen to music because I don't want to watch TV.\nKenji wa _kanada_ no gakkou ni ikimashita kara eigo ga jouzu desu.\nKenji's English is good because he went to a Canadian school.", null, lesson2(22)),
        /**
         * The verb's "He does X, but..." form:
         * http://www.timwerx.net/language/jpverbs/lesson30.htm
         */
        BUT(Base3Inflector(), " keredomo / kedo", false, "He does X, but...",
                "Kare wa nihongo o hanasu keredomo, heta desu.\nHe speaks Japanese, but he's not good at it.\nKeiko wa _piano_ o yoku renshuu suru keredomo, jouzu ni narimasen.\nKeiko practices the piano a lot, but she doesn't get any better.\n_Jakku_ wa kenkou ni ki o tsukeru keredomo, yoku byouki shimasu.\nAlthough Jack is careful about his health, he gets sick a lot.", null, lesson2(23)),
        /**
         * The verb's "I'm able to do something." form:
         * http://www.timwerx.net/language/jpverbs/lesson31.htm
         */
        ABLE_TO_DO(Base3Inflector(), " koto ga dekiru", true, "I'm able to do something.",
                "Watashi wa nihongo o yomu koto ga dekimasu.\nI can read Japanese.\nKeiko wa _piano_ o hiku koto ga dekimasu.\nKeiko can play the piano.\nAshita _Jakku_ wa Tokushima ni iku koto ga dekimasu.\nJack can go to Tokushima tomorrow.\nWatashi wa _furansu_go o yomu koto ga dekimasen.\nI can't read French.\n_Bobbu_ wa Junko ni denwa suru koto ga dekimashita.\nBob was able to call Junko.\n_Richarudo_ wa ika o taberu koto ga dekimasen deshita.\nRichard couldn't eat the squid.\nBoku wa jitensha ni noru koto ga dekiru!\n(one boy to another) I can ride a bicycle!\n_Furansu_go o nihongo ni honyaku dekimasu.\nI can translate French into Japanese.\nKinou, _Jon_ wa benkyou dekimasen deshita.\nJohn wasn't able to study yesterday.", "Potential", lesson2(24)) {
            override fun inflect(verb: String, verbType: VerbType): String {
                return if (verb.endsWith("suru")) {
                    verb.substring(0, verb.length - 4) + " dekiru"
                } else super.inflect(verb, verbType)
            }

        },
        /**
         * The verb's "I decided to do something." form:
         * http://www.timwerx.net/language/jpverbs/lesson32.htm
         */
        DECIDED_TO_DO(Base3Inflector(), " koto ni suru", false, "I decided to do something.",
                "Watashi wa tabun ashita kaimono ni iku koto ni shimasu.\nI'll probably decide to go shopping tomorrow.\n_Jonesu_ sensei wa ashita no suugaku no jugyou no junbi o suru koto ni shimashita.\nMr. Jones decided to prepare for tomorrow's math class.\nWatashi wa mainichi nihongo o benkyou suru koto ni shimashita.\nI've decided to study Japanese every day.", null, lesson2(25)),
        /**
         * The verb's "... until X." form:
         * http://www.timwerx.net/language/jpverbs/lesson33.htm
         */
        UNTIL(Base3Inflector(), " made", false, "... until X.",
                "Yukiko ga kuru made taberu koto wa dekimasen.\nWe can't eat until Yukiko comes.\n_Bobbu_ ga denwa suru made matanakereba narimasen.\nWe have to wait until Bob calls.\nShukudai ga owaru made _terebi_ o misemasen.\nI won't let you watch TV until your homework is finished.", null, lesson2(26)),
        /**
         * The verb's "Don't do X!" form:
         * http://www.timwerx.net/language/jpverbs/lesson34.htm
         */
        NEGATIVE_COMMAND(Base3Inflector(), " na!", false, "Don't do X!",
                "Taberu na!\nDon't eat!\nSuwaru na!\nDon't sit down!\nKoko ni kuru na!\nDon't come here! / Stay away from here! / Stay away from me!\nTerebi o miru na!\nDon't watch TV!\nSawaru na!\nDon't touch!\nEnki suru na!\nDon't put it off!\nShaberu na!\nDon't talk!\nNeru na!\nDon't sleep!", "Negative Imperative", lesson2(27)),

        /**
         * The verb's "If X, then..." form:
         * http://www.timwerx.net/language/jpverbs/lesson35.htm
         */
        IF(Base3Inflector(), " nara", true, "If X, then ...",
                "Isogu nara, tsugi no densha ni noru koto ga dekiru yo.\nIf we hurry we'll be able to make the next train.\nDekakeru nara, kasa o motte ikinasai.\nIf you go out, take an umbrella.\nAme ga furu nara, shiai o enki shinakereba naranai.\nIf it rains we'll have to put off the game.\nTabako o suu nara, soto de suinasai.\nIf you're going to smoke, do it outside.\nKodomotachi wa ima sunakku o taberu nara, yuushoku o tabenai deshou.\nIf the kids eat a snack now, they probably won't eat dinner.", null, lesson2(28)),
        /**
         * The verb's "X which/where/who Y" form:
         * http://www.timwerx.net/language/jpverbs/lesson36.htm
         */
        WHICH_WHERE_WHO(Base3Inflector(), "", true, "X which/where/who Y",
                "watashi ga noru densha\nthe train I'll take\nkare ga iku tokoro\nthe place he'll go\nkanojo no deru jikan\nthe time she'll leave\nwatashitachi ga au kyaku\nthe customer we'll meet", null, lesson2(29)),
        /**
         * The verb's "In order to do something" form:
         * http://www.timwerx.net/language/jpverbs/lesson38.htm
         */
        IN_ORDER_TO(Base3Inflector(), " no ni", false, "In order to do something",
                "Kono tegami o okuru no ni ikura desu ka?\nHow much will it cost to send this letter?\nTokyo yuki no densha ni noru no ni asu hayaku okinakereba narimasen.\nWe'll have to get up early tomorrow in order to make the train for Tokyo.\nHitsuyou na kanji o subete oboeru no ni daibun jikan ga kakaru.\nIt takes quite a long time to learn all of the necessary kanji.", null, lesson2(31)),
        /**
         * The verb's "No wa" form:
         * http://www.timwerx.net/language/jpverbs/lesson39.htm
         *
         * Fixed explanation: https://github.com/mvysny/aedict/issues/822
         */
        NO_WA(Base3Inflector(), " no ha", false, "The act of of doing X",
                "Yomu no wa tanoshii desu.\nReading is enjoyable.\nNihongo o hanasu no wa kantan desu.\nSpeaking Japanese is easy.\nHayaku okiru no wa tokidoki muzukashii desu.\nGetting up early is sometimes difficult.\nKasei ni sumu no wa mada fukanou desu.\nLiving on Mars is not yet possible.\n_Hawai_ ni iku no wa saikou desu!\nGoing to Hawaii is great!", null, lesson2(32)),
        /**
         * The verb's "Because of X" form:
         * http://www.timwerx.net/language/jpverbs/lesson40.htm
         */
        BECAUSE_OF2(Base3Inflector(), " node", false, "Because of ...",
                "Kyaku ga kuru node watashi wa ima deru koto ga dekimasen.\nA guest is coming so I can't go out now.\nAshita hayaku okiru node hayaku neru.\nI have to get up early tomorrow so I'm going to bed early.\nEiko wa eigo o hanasu koto ga dekiru node ii shigoto o mitsukeru deshou.\nSince Eiko can speak English, she'll probably find a good job.", null, lesson2(33)),
        /**
         * The verb's "In spite of X" form:
         * http://www.timwerx.net/language/jpverbs/lesson41.htm
         */
        IN_SPITE_OF(Base3Inflector(), " noni", false, "In spite of X",
                "[Yamenasai] to iu noni, kanojo wa kikimasen.\nDespite my telling her to stop, she won't listen.\nHayaku okita noni okureta.\nI was late even though I got up early.", null, lesson2(34)),
        /**
         * The verb's "I was just about to do X." form:
         * http://www.timwerx.net/language/jpverbs/lesson82.htm
         */
        NEARLY(Base3Inflector(), " tokoro datta", false, "I was just about to do X.",
                "Ima Sachiko ni denwa suru tokoro datta.\nI was just about to call Sachiko.\nWatashi no saifu o wasureru tokoro datta.\nI almost forgot my wallet.", null, lesson2(76)),
        /**
         * The verb's "I heard that X" form:
         * http://www.timwerx.net/language/jpverbs/lesson42.htm
         */
        I_HEARD(Base3Inflector(), " sou desu", false, "I heard that X",
                "Hiru kara ame ga furu sou desu.\nI hear it's going to rain in the afternoon.\nKayo wa raishuu kara resutoran de baito o hajimeru sou desu.\nI heard that Kayo's going to start working part-time at a restaurant next week.\nTakada-san wa yameru sou desu.\nI heard that Mr. Takada's quitting.", null, lesson2(35)),
        /**
         * The verb's "For the purpose of X" form:
         * http://www.timwerx.net/language/jpverbs/lesson43.htm
         */
        FOR_THE_PURPOSE_OF(Base3Inflector(), " tame ni", true, "For the purpose of X",
                "Hiroko wa mensetsu o ukeru tame ni Oosaka ni ikimasu.\nHiroko's going to Osaka for an interview.\nNyuujouken o kau tame ni daibun machimashita.\nI had to wait quite a while to buy tickets.\nNihongo o benkyou suru tame ni atarashii jisho o kaimashita.\nI bought a new dictionary to study Japanese.", null, lesson2(36)),
        /**
         * The verb's "When/If" form:
         * http://www.timwerx.net/language/jpverbs/lesson44.htm
         */
        WHEN_IF(Base3Inflector(), " ni", false, "When/If",
                "Massugu iku to Ritsurin Kouen ga miemasu.\nIf you go straight you'll see Ritsurin Park.\nNatsu ni naru to kodomotachi wa umi ni ikitakunarimasu.\nWhen summer comes the kids want to go to the beach.\nWatashi wa soba o taberu to byouki ni naru.\nI get sick whenever I eat buckwheat noodles.", null, null),
        /**
         * The verb's "I think that X" form:
         * http://www.timwerx.net/language/jpverbs/lesson45.htm
         */
        I_THINK_THAT(Base3Inflector(), " to omou", true, "I think that X",
                "_Bobbu_ wa goji ni kaeru to omoimasu.\nI think Bob will come back at five o'clock.\nEiko wa eigo o hanasu koto ga dekiru to omoimasu.\nI think Eiko can speak English.\nKoji wa okureru to omoimasu.\nI think Koji will be late.\nSasaki-san wa mou sugu kochira ni denwa suru to omou.\nI think Ms. Sasaki will call us soon.\nAshita wa ame ga furu to omou.\nI think it'll rain tomorrow.\nEiko wa eigo o hanasu koto ga dekiru to omoimasen.\nI don't think Eiko can speak English.\nKoji wa okureru to omoimasen deshita.\nI didn't think that Koji would be late.\nKyou ame ga furu to omoimashita.\nI thought it would rain today [and it did.]", null, lesson2(38)),
        /**
         * The verb's "I intent to X" form:
         * http://www.timwerx.net/language/jpverbs/lesson46.htm
         */
        I_INTENT(Base3Inflector(), " tumori", false, "I intend to X",
                "Watashi wa sanji made ni kaeru tsumori.\nI plan to be back by three o'clock.\nSteve wa _kanada_ ni iku tsumori da to omou.\nI think Steve plans to go to Canada.\nKeiko wa Kyoto Daigaku ni hairu tsumori desu.\nKeiko intends to go to Kyoto University.", null, lesson2(39)),
        /**
         * The verb's "It seems to X" form:
         * http://www.timwerx.net/language/jpverbs/lesson47.htm
         */
        IT_SEEMS_TO(Base3Inflector(), " you desu", false, "It seems to X",
                "_Mari_ wa ashita kuru you desu.\nIt seems that Mary will be coming tomorrow.\nSachiko wa _kanada_ ni iku you desu.\nIt looks like Sachiko is going to Canada.\n_Ken_ wa _piano_ o hiku koto ga dekiru you desu.\nIt looks like Ken can play the piano.", null, lesson2(40)),
        /**
         * The verb's "If X, then..." form:
         * http://www.timwerx.net/language/jpverbs/lesson48.htm
         */
        IF2(Base4Inflector(), "ba", true, "If X, then...",
                "Isogeba, tsugi no densha ni noru koto ga dekiru yo.\nIf we hurry we'll be able to make the next train.\nAme ga fureba, shiai o enki shinakereba naranai.\nIf it rains we'll have to put off the game.\nKodomotachi wa ima sunakku o tabereba, yuushoku o tabenai deshou.\nIf the kids eat a snack now, they probably won't eat dinner.\nKyouto ni ikeba?\nWhy don't you go to Kyoto?\nShichiji han ni dereba?\nWhy don't you leave at 7:30?", "Informal Provisional", lesson2(43)),
        /**
         * The verb's "It would be good if X" form:
         * http://www.timwerx.net/language/jpverbs/lesson49.htm
         */
        IT_WOULD_BE_GOOD_IF(Base4Inflector(), "ba ii", true, "It would be good if X",
                "Soto de asobeba ii.\nIt would be good if you played outside.\nIma benkyou sureba ii.\nNow would be a good time to study.\nWatashitachi wa karui shokuji o tabereba ii to omou.\nI think it would be good if we ate a light meal.", null, lesson2(43)),
        /**
         * The verb's "I regret X" form:
         * http://www.timwerx.net/language/jpverbs/lesson49.htm
         */
        I_REGRET(Base4Inflector(), "ba yokatta", false, "I regret X",
                "Hachiji ni kureba yokatta.\nWe should have come at 8:00.\nKouen ni ikeba yokatta.\nI wish we had gone to the park.\n_Suteeki_ o chuumon sureba yokatta.\nI wish I had ordered the steak.", null, lesson2(43)),
        /**
         * The verb's "Do something!" form:
         * http://www.baka.sk/~tmath/language/jpverbs/lesson50.htm
         */
        PLAIN_COMMAND(Base4Inflector(), "", false, "Do something!",
                "Damare!\nShut up!\nIke!\nGo!\nYare!\nDo it!", "Informal Imperative", lesson2(44)) {
            override fun appliesToIchidan() = false
        },
        /**
         * The verb's "Able to do something" form:
         * http://www.timwerx.net/language/jpverbs/lesson51.htm
         * Also called Potential
         */
        ABLE_TO_DO2(PotentialInflector(), "", true, "I'm able to do something",
                "Watashi wa nihongo o yomeru.\nI can read Japanese.\nKeiko wa _piano_ o hikeru.\nKeiko can play the piano.\nAshita _Jakku_ wa Tokushima ni ikeru.\nJack can go to Tokushima tomorrow.\nKeiko wa _piano_ o hikemasu.\nKeiko can play the piano. [polite]\nKeiko wa _baiorin_ o hikemasen.\nKeiko can't play the violin.\n_Jakku_ wa Tokushima ni ikemashita.\nJack was able to go to Tokushima.\nKare wa Oosaka ni ikemasen deshita.\nHe wasn't able to go to Osaka.\nKare wa raigetsu ikeru kamo shiremasen.\nHe might be able to go next month.", "Potential", lesson2(45)) {
        },
        /**
         * The verb's "Let's do something" form:
         * http://www.timwerx.net/language/jpverbs/lesson54.htm
         */
        LET_S2(Base5Inflector(), "", true, "Let's do something",
                "Ikou.\nLet's go.\nTabeyou.\nLet's eat.\nYasumou.\nLet's take a break.\n", "Informal Volitional", lesson2(48)),
        /**
         * The verb's "I wonder if I should..." form:
         * http://www.timwerx.net/language/jpverbs/lesson54.htm
         */
        I_WONDER_IF_I_SHOULD(Base5Inflector(), " ka na", true, "I wonder if I should ...",
                "Kaimono ni ikou ka na.\nI think I'll go shopping.\nKaimono ni ikou ka naa.\nI wonder if I should go shopping.\n_Terebi_ o miyou ka na.\nMaybe I'll watch TV.\n_Bobbu_ ni denwa shiyou ka naa.\nI wonder if I should call Bob.\nKyou wa tenki ga ii kara, arukou ka na.\nI think I'll walk today since the weather's nice.", null, lesson2(48)),
        /**
         * The verb's "Try to do X" form:
         * http://www.timwerx.net/language/jpverbs/lesson54.htm
         */
        TRY_TO(Base5Inflector(), " to suru", false, "Try to do X",
                "_Jon_ wa koyou to suru to omou.\nI think John will try to come.\nNaoto wa hikouki o miyou to shimashita ga, miemasen deshita.\nNaoto tried to see the airplane, but he couldn't.", null, lesson2(48)),
        /**
         * The verb's "Do something." form:
         * http://www.timwerx.net/language/jpverbs/lesson55.htm
         */
        MILD_COMMAND(BaseTeInflector(), "", false, "Do something.",
                "Rokuji ni kite.\nCome at six o'clock.\nMatte.\nWait.\nDouzo, tabete.\nGo ahead and eat.\nKoko ni kite.\nCome here.\nKore o kiite.\nListen to this.\nKore o kitte.\nCut this.\n_Terebi_ o minaide.\nDon't watch the TV.\nIkanaide.\nDon't go.", "Informal Imperative", lesson2(49)),
        /**
         * The verb's "Please do something." form:
         * http://www.timwerx.net/language/jpverbs/lesson55.htm
         */
        POLITE_COMMAND(BaseTeInflector(), " kudasai", true, "Please do X.",
                "Douzo, tabete kudasai.\nGo ahead, please eat.\nChotto matte kudasai.\nPlease wait a bit.\nRokuji ni kite kudasai.\nPlease come at six o'clock.\nRokuji ni kite kudasai.\nPlease come at six o'clock.\nRokuji ni kite kudasaru?\nWill you please come at six o'clock? [plain]\nRokuji ni kite kudasaimasu ka.\nWill you please come at six o'clock? [polite]\nRokuji ni kite kudasaimasen ka.\nWon't you please come at six o'clock?", "Formal Imperative", lesson2(49)),
        /**
         * The verb's "Let me do something for you." form:
         * http://www.timwerx.net/language/jpverbs/lesson56.htm
         */
        LET_ME_DO_SOMETHING_FOR_YOU(BaseTeInflector(), " ageru", true, "Let me do X for you.",
                "Matte ageru.\nI'll wait for you.\nAto de denwa shite ageru.\nI'll call you later.\nTabetakunakereba, tabete ageru.\nIf you don't want to eat it, I'll eat it for you.\n_Bobbu_ ni _pen_ o kashite agete.\nLend Bob your pen.\nShizuka no kutsu no himo o musunde agete.\nTie Shizuka's shoelaces.", null, lesson2(50)),
        /**
         * The verb's "honorably take a look." form:
         * http://www.timwerx.net/language/jpverbs/lesson57.htm
         */
        HONORABLY_TAKE_A_LOOK(BaseTeInflector(), " goran", false, "Honorably take a look.",
                "_Bobbu_ ni kiite goran.\nAsk Bob and see what he says.\nTabete goran.\nTaste it and see if you like it.\nMite goran.\nTake a look.\nSanae ni denwa shite goran.\nTry calling Sanae.\nKare wa sanjuu hachi to kaite aru. Yonde goran nasai.\nIt says he's 38. Read it for yourself.\nTana no ue ni shio ga aru yo. Mite goran nasai.\nThere is some salt on the shelf. See for yourself.\nKouen no kouyou wa ima kirei yo. Itte goran nasai.\nThe autumn leaves in the park are beautiful now. Go and see for yourself.", null, lesson2(51)),
        /**
         * The verb's "I am doing X." form:
         * http://www.timwerx.net/language/jpverbs/lesson58.htm
         */
        PROGRESSIVE_TENSE(BaseTeInflector(), " iru", true, "I am doing X.",
                "Watashi wa koko ni iru.\nI am here.\nWatashi wa aruite iru.\nI am walking.\nKarera wa zasshi o yonde iru.\nThey are reading a magazine.\nWatashitachi wa Takamatsu ni sunde iru.\nWe live in Takamatsu. [We are living in Takamatsu.]\nShizuko wa tabete iru.\nShizuko is eating.\nKanojo wa sushi o tabete iru.\nShe is eating sushi.\n_Birru_ wa nihongo o benkyou shite iru.\nBill is studying Japanese.\nWatashi wa shinbun o yonde imasu.\nI'm reading the newspaper.\nKinou nete imashita.\nYesterday I slept all day. [Yesterday I was sleeping all day.]\nKare wa _furansu_go o benkyou shite imasen.\nHe's not studying French.\nKyou _terebi_ o mite imasen deshita.\nI didn't watch TV today. [I wasn't watching TV today.]\nTeikyou o shite iru ko wa _Bobbu_ no imouto desu.\nThe kid [who is] playing tennis is Bob's [younger] sister.\nSunahama de asonde iru inu wa boku no desu.\nThe dog [that's] playing on the beach is mine.\n_Samu_ wa koko ni imasen.\nSam's not here.\nKarera wa zasshi o yonde imasen.\nThey aren't reading a magazine.\nWatashitachi wa Okayama ni sunde imasen.\nWe don't live in Okayama.\nWatashi wa mada tabete inai.\nI haven't eaten yet.\nSeiko wa mada kaimono ni itte inai.\nSeiko hasn't gone shopping yet.\nYumi ni mada denwa shite inai no?\nHaven't you called Yumi yet?\n_Jon_ wa _terebi_ o mite ita.\nJohn was watching TV.\nKarera wa zasshi o yonde ita.\nThey were reading a magazine.\n_Birru_ wa benkyou shite imashita.\nBill was studying.", "Present Progressive", lesson2(52)),
        /**
         * The verb's "I humbly partake" form:
         * http://www.timwerx.net/language/jpverbs/lesson61.htm
         */
        VERY_POLITE_COMMAND(BaseTeInflector(), " itadaku", true, "I humbly partake",
                "_Jonson_-san ni denwa shite itadakemasu ka.\nWould you please call Mr. Johnson?\nO-namae o oshiete itadakemasu ka.\nMay I please have your name?\nNiji ni kite itadakemasu ka.\nWould you please come at two o'clock?\nAshita watashi ni denwa shite itadakemasen ka.\nWon't you please call me tomorrow?\nKono shorui o kinyuu shite itadakemasen deshou ka.\nCould I possibly get you to fill out these forms?\nMurai-san ni senshuu ginkou ni itte itadakimashita. Oboete imasen ka.\nI had you [Murai-san] go to the bank for me last week. Don't you remember?", "Formal Imperative", lesson2(55)),
        /**
         * The verb's "I humbly receive" form:
         * http://www.timwerx.net/language/jpverbs/lesson61.htm
         */
        I_HUMBLY_RECEIVE_COMMAND(BaseTeInflector(), " morau", true, "I humbly receive",
                "kaasan: Kimiko ni mise ni itte moraitai.\nMom: I want you [Kimiko] to go to the store for me.\nKimiko: Ima shukudai o shite iru. _Ken_ ni itte moratte.\nKimiko: I'm doing homework now. Get Ken to go.\nKaasan: Kimiko ni mise ni itte moraitai.\nMom: I want you [Kimiko] to go to the store for me.\nKimiko: Ima shukudai o shite iru. Ojii-chan ni itte moraimashou ka.\nKimiko: I'm doing homework now. Shall I get Grandpa to go?\nKaasan: _Ken_ ni itte moraou ka naa.\nMom: (not wanting to bother Grandpa) I wonder if I should get Ken to go.\nKimiko: Ken wa ima inai. Ojii-chan ni itte moraimasu.\nKimiko: (thinking that Grandpa needs to get out more) Ken's not here now. I'll get Grandpa to go.\nSuzuki-san: Ginkou ni ikimashou ka.\nSuzuki-san: Shall I go to the bank?\nTanaka-san: Murai-san ni itte moraimashita.\nTanaka-san: I had Ms. Murai go.", null, lesson2(55)),
        /**
         * The verb's "After doing X..." form:
         * http://www.timwerx.net/language/jpverbs/lesson62.htm
         */
        AFTER(BaseTeInflector(), " kara", true, "After doing X...",
                "Tabete kara kaimono ni iku.\nAfter I eat I'm going shopping.\n_Jon_ wa shukudai o shite kara kuru.\nJohn's coming over after he does his homework.\nNaomi ga kaette kara tabemashou.\nLet's eat after Naomi comes back.\nGakkou ga owatte kara yakyuu o yarou.\nLet's play baseball after school.\nShigoto ga owatte kara eiga o mi ni ikimashou.\nLet's go see a movie after work.", null, lesson2(56)),
        /**
         * The verb's "Please do X..." form:
         * http://www.timwerx.net/language/jpverbs/lesson63.htm
         */
        REGULAR_PLEASE(BaseTeInflector(), " kureru", true, "Please do X",
                "Rokuji ni kite kureru?\nWill you please come at six?\nJitensha o kashite kureru?\nWould you please loan me your bicycle?\nDenwa bangou o oshiete kuremasu ka.\nWill you please tell me your phone number?\nRitsuko wa heya o souji shite kuremashita.\nRitsuko kindly cleaned the room.\nKyuukei sasete kurenai ka.\nWon't you please let me take a break?\nWatashitachi to issho ni kite kurenai no.\nWon't you please come with us?\nKite kure.\nPlease come here.\nMatte kure.\nPlease wait.", "Formal Imperative", lesson2(57)),
        /**
         * The verb's "X finished" form:
         * http://www.timwerx.net/language/jpverbs/lesson64.htm
         */
        TO_FINISH(BaseTeInflector(), " kuru", true, "X finished",
                "_Ron_ wa sukoshi zutsu nihongo ga wakatte kimashita.\nLittle by little Ron came to understand Japanese.\n_Doitsu_ no rekishi o benkyou shite kimashita.\nI have been studying German history.\nTabete kita.\nI ate before coming over.\nShirabete kuru.\nI'll go check it [then come back].", null, lesson2(58)),
        /**
         * The verb's "X will start" form:
         * http://www.timwerx.net/language/jpverbs/lesson64.htm
         */
        TO_START(BaseTeInflector(), " iku", true, "X will start",
                "_pasokon_ wa yasuku natte iku deshou.\nPCs will most likely get less and less expensive.\nSono tame, _pasokon_ no shiyousha ga fuete iku to omou.\nBecause of that, I think that the number of PC users will increase.", null, lesson2(58)),
        /**
         * The verb's "I'll try doing X" form:
         * http://www.timwerx.net/language/jpverbs/lesson65.htm
         */
        TO_TRY(BaseTeInflector(), " miru", false, "I'll try doing X",
                "Kono kanji o yonde miru.\nI'll try to read these kanji.\nKono atarashii _pasokon_ o tsukatte miyou.\nLet's give this new PC a try.\nSushi o tabete minai no?\nWon't you try some sushi?\n_Jon_ ni hanashite mimasu.\nI'll try to talk to John.\nKare ni denwa shite mimashita ga, rusu deshita.\nI tried calling him, but he wasn't in.", null, lesson2(59)),
        /**
         * The verb's "even IF (something were to happen), you must remember that (something else)..." form:
         * http://www.timwerx.net/language/jpverbs/lesson80.htm
         */
        EVEN_IF2(BaseTeInflector(), " mo", false, "Even if X, you must remember that Y.",
                "Setsumeisho o yonde mo, kono _sofuto_ ga wakarinikui.\nEven if you read the manual, this software is hard to understand.\nKare wa ikura tabete mo, ippai ni naranai.\nNo matter how much he eats, he never gets full.", null, lesson2(60)),
        /**
         * The verb's "Can I do X? (Do I have a permission?)" form:
         * http://www.timwerx.net/language/jpverbs/lesson66.htm
         */
        CAN(BaseTeInflector(), " mo ii", true, "Can I do X? (Do I have permission?)",
                "Boku no _pasokon_ o tsukatte mo ii yo.\nYou can use my PC.\nGohan o tabete kara _terebi_ o mite mo ii.\nYou can watch TV after you've eaten your dinner.\nJisho o karite mo ii?\nCan I borrow your dictionary?\nRaishuu no getsuyoubi o yasunde mo yoroshii desu ka.\nMay I take off next Monday?\nKyou, hayaku kaette mo yoroshii.\nYou may go home early today.\nWatashi no jisho o tsukatte ii yo.\nSure, you can use my dictionary.\nHai, _terebi_ mite ii yo.\nYeah, you can watch TV.", null, lesson2(60)),
        /**
         * The verb's "All he does is X" form:
         * http://www.timwerx.net/language/jpverbs/lesson66.htm
         */
        ALL_HE_DOES(BaseTeInflector(), " bakari", false, "All he does is X.",
                "Tabete bakari.\nAll you ever do is eat.\nAno ko wa _terebi_ _geemu_ o yatte bakari.\nAll that kid does is play computer games.\nShizuka wa eigo o benkyou shite bakari.\nAll Shizuka ever does is study English.", null, lesson2(68)),
        /**
         * The verb's "I will certainly do X" form:
         * http://www.timwerx.net/language/jpverbs/lesson67.htm
         */
        I_WILL_CERTAINLY_DO(BaseTeInflector(), " oku", false, "I will certainly do X.",
                "_Ron_ ni denwa shite oku.\nI'll call Ron.\nMado o akete oku.\nI'll open the window.\nKasa o katte okimasu.\nI'm going to buy an umbrella.\nKanojo ni ki o tsukeru you ni itte okimasu.\nI'll tell her to be careful.\nShukudai o shite okimashita.\nI [went ahead and] did my homework.", null, lesson2(61)),
        /**
         * The verb's "To complete doing X / To do something unexpected / Something negative may happen" form:
         * http://www.timwerx.net/language/jpverbs/lesson68.htm
         */
        COMPLETE(BaseTeInflector(), " simau", false, "To complete doing X / To do something unexpected / Something negative may happen",
                "Shukudai o shite shimaimashou.\nLet's finish up our homework.\nChoushoku o tabete shimaimashita.\nI've finished eating breakfast.\nHeya o souji shite shimau hou ga ii yo.\nYou should finish cleaning up your room.\nKuruma o katte shimaimashita.\nI bought a car.\n_Bobbu_ wa ude no hone o orete shimaimashita.\nBob broke his arm.\nKanojo wa Oosaka ni itte shimaimashita.\nShe [up and] went to Osaka.\nWatashi no fuku ga yogorete shimau!\nMy clothes'll get dirty!\nAh! Fuku ga yogorete shimaimashita.\nOh, no! My clothes got dirty.\nDensha ni noriokurete shimau yo!\nWe'll miss the train!\nAh! Kippu o nakushite shimaimashita!\nOh, no! I lost my ticket!", null, lesson2(62)),
        /**
         * The verb's "How about doing X?" form:
         * http://www.timwerx.net/language/jpverbs/lesson69.htm
         */
        HOW_ABOUT(BaseTeInflector(), " ha ikaga / dou desu ka", false, "How about doing X?",
                "Ima chuushoku o tabete wa ikaga desu ka.\nHow about having lunch now?\nAshita Ritsurin Kouen ni itte wa ikaga desu ka.\nWhat do you think about going to Ritsurin Park tomorrow?\nAtarashii _terebi_ o katte wa dou desu ka.\nWhat do you say we buy a new TV?", null, lesson2(63)),
        /**
         * The verb's "You must not do X!" form:
         * http://www.timwerx.net/language/jpverbs/lesson70.htm
         */
        FORBIDDEN(BaseTeInflector(), " ha ikemasen", true, "You must not do X",
                "Shashin o totte wa ikemasen.\nYou can't take pictures.\nOkurete wa ikemasen yo.\nDon't be late.\nBoku no _pasokon_ o sawatte wa ikenai!\nDon't touch my PC!\nIkenai! Joushaken o wasurete shimaimashita!\nOh, no! I forgot my ticket!\nIkenai! Kimiko wa kasa o motte iku no o wasuremashita!\nOh, no! Kimiko forgot to take her umbrella!", null, lesson2(64)),
        /**
         * The verb's "I did X, then Y, then Z..." form:
         * http://www.timwerx.net/language/jpverbs/lesson71.htm
         */
        CONTINUATION(BaseTeInflector(), "", true, "I did X, then Y, then Z",
                "Shizu ni denwa shite, heya o katazukete, kaimono ni ikanakereba naranai.\nI've got to call Shizu, straighten up the room, and go shopping.\nKesa watashi wa shichiji ni okite, gohan o tabete, hachiji ni ie o demashita.\nThis morning I got up at seven o'clock, ate breakfast, and left home at eight.", null, lesson2(65)),
        /**
         * The verb's "I did X" form:
         * http://www.timwerx.net/language/jpverbs/lesson72.htm
         */
        PAST_TENSE(BaseTaInflector(), "", true, "I did X.",
                "Watashi shita.\nI did it.\nKami kitta.\nI got a haircut.\nO-hiru tabeta.\nI ate lunch.\n_Terebi_ mita.\nI watched TV.\nHon yonda.\nI read a book.\nBoku no kingyo shinda.\nMy goldfish died.\nWatashi ga karita kasa wa Kimiko no.\nThe umbrella I borrowed is Kimiko's.\nShinda kingyo wa, roku nen kan katta.\nThe goldfish that died I had six years.\n_Joi_ ga yaita keeki wa oishikatta.\nThe cake Joy made was delicious.\nBoku ga katta _pasokon_ wa, juu hachi man en deshita.\nThe PC I bought was one hundred eighty thousand yen.\n_Bobbu_ ga benkyou shita koto wa totemo yakudatta.\nThe things Bob studied were very helpful.", "Informal Past", lesson2(67)),
        /**
         * The verb's "I did X recently" form:
         * http://www.timwerx.net/language/jpverbs/lesson74.htm
         */
        I_DID_RECENTLY(BaseTaInflector(), " bakari", false, "I did X recently.",
                "Okaa-chan wa kaetta bakari.\nMom just got back.\nWatashi wa tabeta bakari.\nI just ate.\n_Jon_ wa deta bakari.\nJohn just left.\nKono heya o souji shita bakari.\nI just cleaned this room.\nSono kasa o katta bakari.\nI just bought that umbrella.", null, lesson2(68)),
        /**
         * The verb's "I experienced X" form:
         * http://www.timwerx.net/language/jpverbs/lesson75.htm
         */
        EXPERIENCED(BaseTaInflector(), " koto ga aru", true, "I have experienced X.",
                "Nihonshoku o tabeta koto ga aru?\nHave you ever eaten Japanese food?\nHai, sushi to sukiyaki o tabeta koto ga aru.\nYes, I've eaten sushi and sukiyaki.\nTako wa tabeta koto ga aru?\nHave you ever eaten octopus?\nIie, tabeta koto ga nai ga, tabete mitai.\nNo, I haven't, but I'd like to try it.\nOkinawa ni itta koto ga arimasu ka.\nHave you ever been to Okinawa?\nHai, arimasu. Nikai ikimashita.\nYes, I have. I've been twice.\nKono hon yonda koto aru?\nHave you read this book?\nIie, mada yonde inai.\nNo, not yet. [No, I haven't read it yet.]", null, lesson2(69)),
        /**
         * The verb's "If X, then" form:
         * http://www.timwerx.net/language/jpverbs/lesson76.htm
         */
        IF3(BaseTaInflector(), "ra", false, "If X, then",
                "Dekaketara, _kouto_ ga hitsuyou ni naru deshou.\nIf you're going out, you'll probably need a coat.\nDenwa shitara, kare wa kuru deshou.\nIf you telephone him, he'll probably come.\nIma kodomotachi wa sunakku o tabetara, o-hiru o tabenai deshou.\nIf the kids eat a snack now, they probably won't eat lunch.", "Conditional", lesson2(70)),
        /**
         * The verb's "If X, then" form:
         * http://www.timwerx.net/language/jpverbs/lesson77.htm
         */
        IT_SEEMS_TO2(BaseTaInflector(), " rasii", false, "It seems to X",
                "Sachiko wa _kanada_ ni itta rashii.\nI hear that Sachiko went to Canada.\n_Bobbu_ wa daibun futotta rashii.\nI hear that Bob has gained a lot of weight.\n_Ken_ wa atarashii _pasokon_ o katta rashii.\nI hear that Ken bought a new PC.", null, lesson2(71)),
        /**
         * The verb's "I roughly did X" form:
         * http://www.timwerx.net/language/jpverbs/lesson78.htm
         */
        ROUGHLY(BaseTaInflector(), "ri", false, "I roughly did X",
                "Kinou no ban watashi wa _terebi_ o mitari, ongaku o kiitari, shukudai o shitari shite imashita.\nLast night I watched TV, listened to some music, and did some homework.\nWatashi wa _terebi_ o mitari shite ita.\nI watched TV and stuff.\nWatashi wa manga o yondari shite, yuushoku o tabeta.\nI read comics and stuff, then ate dinner.\n_Jimu_ wa furui mono o kattari uttari suru.\nJim buys and sells old things.\nAshita watashi wa benkyou shitari, souji shitari, _terebi_ o mitari suru deshou.\nTomorrow I'll probably do some studying, some cleaning, and watch TV.\nKinou no ban watashi wa yuushoku o tabete kara _terebi_ o mite, ni jikan gurai ongaku o kiite, ichi ji made shukudai o shimashita.\nLast night after dinner I watched TV, listened to music for about two hours, then did homework until one o'clock.\nKyou Sachiko wa heya o souji shitari kaimono ni ittari shite, chuushoku o tabete, hiru kara yuujin no ie ni ittari _piano_ o renshuu shitari shite, sore kara yuushoku o tsukutte kureta.\nToday Sachiko cleaned her room and did some shopping, ate lunch, then in the afternoon went to a friend's house, practiced the piano and things, then she made dinner.", null, lesson2(72)),
        /**
         * The verb's "If I were to do X, ..." form:
         * http://www.timwerx.net/language/jpverbs/lesson79.htm
         */
        SUPPOSITION(BaseTaInflector(), " to sitara", false, "If I were to do X, ...",
                "Ashita _Bobbu_ ga kita to shitara, watashi wa hontou ni komaru.\nIf Bob were to come tomorrow, I'd really be at a loss.\nGogo kara ame ga futta to shitara, dou shimashou ka.\nSupposing it rains this afternoon; what shall we do?\nIma oyogi ni itta to shitara, tabun koukai suru deshou.\nIf you were to go swimming now, you'd probably regret it.", null, lesson2(73)),
        /**
         * The verb's "even IF X, you must remember that Y." form:
         * http://www.timwerx.net/language/jpverbs/lesson80.htm
         */
        EVEN_IF(BaseTaInflector(), " to site mo", false, "even IF X, you must remember that Y.",
                "Ashita _Bobbu_ ga kita to shite mo, watashi wa asatte made au koto ga dekimasen.\nEven if Bob were to come tomorrow, I wouldn't be able to see him until the day after tomorrow.\nAnata wa supeingo o benkyou shita to shite mo, shigoto de tsukaenai deshou.\nEven if you studied Spanish, you probably wouldn't be able to use it in your work.\nKenkou shokuhin o takusan tabeta to shite mo, undou shinakereba imi ga nai deshou.\nEven if you were to eat lots of health food, it would be meaningless if you didn't exercise.", null, lesson2(74)),
        /**
         * The verb's "When I did X..." form:
         * http://www.timwerx.net/language/jpverbs/lesson81.htm
         */
        WHEN(BaseTaInflector(), " toki", false, "When I did X...",
                "Watashi wa sore o yonda toki, totemo odorokimashita.\nWhen I read that, I was very surprised.\nSore o kiita toki waratta.\nI laughed when I heard that.\n_Jon_ wa koketa toki zubon ga yabureta.\nJohn's pants tore when he fell.", null, lesson2(75)),
        /**
         * The verb's "I did X just now." form:
         * http://www.timwerx.net/language/jpverbs/lesson82.htm
         */
        I_DID_JUST_NOW(BaseTaInflector(), " tokoro", false, "I did X just now.",
                "Watashi wa ima kaetta tokoro.\nI just got back now.\nKodomotachi wa ima tabeta tokoro.\nThe kids just finished eating.\nKono heya o souji shita tokoro desu.\nI just cleaned this room.", null, lesson2(76)),
        /**
         * http://japanese.about.com/library/blqow24.htm
         */
        EXPLAIN_PLAN(Base3Inflector(), "n da", false, "Planning / Explaining (I am going to do X / Because X)",
                "Ashita doubutsuen ni iku n da.\nI am going to the zoo tomorrow.\nDoushite byouin ni iku n desu ka. Haha ga byouki nan desu.\nWhy are you going to the hospital? Because my mother is sick.\nDoushite tabenai n desu ka. Onaka ga suite inai n desu.\nWhy don't you eat? Because I am not hungry.", null, "http://japanese.about.com/library/blqow24.htm");

        /**
         * Returns the examples as a list of string pairs.
         * @param romanization use optionally this romanization for Japanese sentences. Not null.
         * @return a list of pairs: first pair item is the Japanese sentence,
         * second pair item is the English translation. Never null.
         */
        fun getExamples(romanization: IRomanization?): Array<Array<String>> {
            val e = examples.split('\n')
            val result = ArrayList<Array<String>>(e.size / 2)
            for (i in 0 until e.size / 2) {
                val english = e[i * 2 + 1]
                var japanese = e[i * 2].toLowerCase()
                // fix wa and o
                japanese = WA.matcher(japanese).replaceAll(" ha ")
                japanese = WO.matcher(japanese).replaceAll(" wo ")
                // convert words marked with _ to katakana
                var inUnderscore = false
                val jp = StringBuilder(japanese.length)
                val t = StringTokenizer(japanese, "_", true)
                while (t.hasMoreElements()) {
                    val token = t.nextToken()
                    if (token == "_") {
                        inUnderscore = !inUnderscore
                    } else {
                        jp.append(if (inUnderscore) RomanizationEnum.Hepburn.r.toKatakana(token) else token)
                    }
                }
                japanese = RomanizationEnum.Hepburn.r.toHiragana(jp.toString())
                if (romanization != null) {
                    japanese = romanization.toRomaji(japanese)
                }
                result.add(arrayOf(japanese, english))
            }
            return result.toTypedArray()
        }

        /**
         * Creates a new form object instance.
         *
         * @param inflector
         * the verb inflector, denotes the required base of the verb.
         * @param suffix
         * additional suffix to add to the inflected verb
         * @param basic
         * if true this expression is a basic one
         * @param explanationResId
         * explanation of the form (e.g. I don't do something).
         * @param examples
         * a new-line-separated list of example sentences, first in
         * Japanese (a [RomanizationEnum.Hepburn]-romanized),
         * then in English.
         * @param officialName the inflection name like Informal Past
         * @param url the http link with more information on the inflection.
         */
        constructor(inflector: AbstractBaseInflector, suffix: String,
                            basic: Boolean, explanationResId: String, examples: String,
                            officialName: String?, url: String?) : this(inflector, suffix, null, basic, explanationResId, examples, officialName, url)

        /**
         * Inflects a verb to the appropriate form.
         * @param verb a verb, must be in [RomanizationEnum.NihonShiki] romanization. Must be in Base 3 form. Note that "desu"
         * cannot be inflected. Not blank.
         * @param verbType the verb type
         * @return inflected verb, never null, in the [RomanizationEnum.NihonShiki] romanization. Kanji characters are left as-is. Never blank.
         */
        open fun inflect(verb: String, verbType: VerbType): String {
            check(!verb.containsHiragana()) { "$verb must be in NihonShiki romanization but contains hiragana" }
            val suf = when {
                suffixIchidan != null && (verbType.isIchidan || verbType == VerbType.vk) -> suffixIchidan
                else -> suffix
            }
            return inflector.inflect(verb, verbType) + suf
        }

        /**
         * Bacha zobrazuje sa v Aedict.
         * @return inflector name + suffix
         */
        override fun toString() = inflector.name + if (suffix.isBlank()) "" else " + " + suffix.trim()

        /**
         * Checks if this form is applicable to ichidan verbs.
         * @return true if it is applicable, false otherwise.
         */
        open fun appliesToIchidan(): Boolean = true

        companion object {
            private val WA = Pattern.compile("\\s+wa\\s+")
            private val WO = Pattern.compile("\\s+o\\s+")
        }
    }

}

private fun lessonOld(lesson: Int) = "http://ww8.tiki.ne.jp/~tmath/language/jpverbs/lesson$lesson.htm"

/**
 * Tim zmenil stranku na single-page a precisloval lekcie.
 * @param lesson lesson number, 1..?
 * @return URL s lekciou.
 */
private fun lesson2(lesson: Int) =
        "http://ww8.tiki.ne.jp/~tmath/language/verbs.htm#" + (lesson.toString()).padStart(2, '0')

/**
 * A simple base inflector.
 */
abstract class AbstractBaseInflector : Serializable {

    /**
     * Returns a displayable name of this inflection.
     * @return the displayable name, never null.
     */
    abstract val name: String

    /**
     * Inflects an ichidan verb.
     * @param verb
     * a verb in the Base3 -ru form, with
     * [sk.baka.aedict.kanji.RomanizationEnum.NihonShiki] romanization. Not blank.
     * @return inflected verb, in [sk.baka.aedict.kanji.RomanizationEnum.NihonShiki]
     * romanization, never blank.
     */
    protected abstract fun inflectIchidan(verb: String): String

    protected open fun inflectIchidanKureru(verb: String): String = inflectIchidan(verb)

    /**
     * A special Godan verb: [VerbType.v5ri]
     */
    protected open fun inflectV5ri(verb: String): String = inflectGodan(verb)

    /**
     * A special Godan verb: [VerbType.v5us]
     */
    protected open fun inflectV5us(verb: String) = inflectGodan(verb)

    /**
     * Inflects a godan verb.
     *
     * @param verb
     * a verb in the Base3 form, with
     * [sk.baka.aedict.kanji.RomanizationEnum.NihonShiki] romanization, not blank.
     * @return inflected verb, in [sk.baka.aedict.kanji.RomanizationEnum.NihonShiki]
     * romanization, never blank.
     */
    protected abstract fun inflectGodan(verb: String): String

    /**
     * Inflects a v5aru verb.
     *
     * @param verb
     * a v5aru verb in the Base3 form, with
     * [sk.baka.aedict.kanji.RomanizationEnum.NihonShiki] romanization, not blank.
     * @return inflected verb, in [sk.baka.aedict.kanji.RomanizationEnum.NihonShiki]
     * romanization, never blank.
     */
    protected abstract fun inflectV5aru(verb: String): String

    /**
     * Returns an inflected "kuru" verb.
     *
     * @return inflected kuru verb in [sk.baka.aedict.kanji.RomanizationEnum.NihonShiki]
     * romanization.
     */
    protected abstract fun inflectKuru(): String

    /**
     * Returns an inflected "suru" verb.
     *
     * @return inflected suru verb in [sk.baka.aedict.kanji.RomanizationEnum.NihonShiki]
     * romanization.
     */
    protected abstract fun inflectSuru(): String
    protected abstract fun inflectSuruSpecial(): String

    /**
     * Returns an inflected "zuru" verb (vz, Ichidan zuru verb, for example zyunzuru, kanzuru etc).
     *
     * @return inflected zuru verb in [sk.baka.aedict.kanji.RomanizationEnum.NihonShiki]
     * romanization.
     */
    protected abstract fun inflectZuru(): String

    /**
     * Returns an inflected "iku" verb.
     *
     * @return inflected iku verb in [sk.baka.aedict.kanji.RomanizationEnum.NihonShiki]
     * romanization.
     */
    protected open fun inflectIku(verb: String) = inflectGodan(verb)

    /**
     * Returns given ichidan verb in form 1 (with trailing -ru removed). The
     * function performs several checks that a proper verb is given.
     *
     * @param verb
     * an ichidan verb in [sk.baka.aedict.kanji.RomanizationEnum.NihonShiki]
     * romanization, not blank.
     * @return inflected ichidan verb in [sk.baka.aedict.kanji.RomanizationEnum.NihonShiki]
     * romanization, not blank.
     */
    protected fun getIchidanForm1(verb: String): String {
        // sanity check
        if (!VerbType.isPossiblyIchidan(verb)) throw IllegalArgumentException("$verb is not ichidan")
        return verb.substring(0, verb.length - 2)
    }

    /**
     * Strips trailing -u of given godan verb. The function performs several
     * checks that a proper verb is given.
     *
     * @param verb
     * a godan verb in [sk.baka.aedict.kanji.RomanizationEnum.NihonShiki]
     * romanization, not blank.
     * @return godan verb in [sk.baka.aedict.kanji.RomanizationEnum.NihonShiki]
     * romanization with trailing -u stripped; never blank.
     */
    protected fun stripGodan(verb: String): String {
        // sanity check
        if (!verb.endsWith("u")) {
            throw RuntimeException(verb + " is not base-3 godan")
        }
        return verb.substring(0, verb.length - 1)
    }

    /**
     * Strips the -ru ending in the v5aru verb. Fails if the verb doesn't end with -aru.
     * @param verb  a verb in the Base3 form, with [sk.baka.aedict.kanji.RomanizationEnum.NihonShiki] romanization, not blank. May contain kanjis.
     * @return the verb with -ru ending stripped.
     */
    protected fun stripV5aru(verb: String): String {
        if (!verb.endsWith("aru")) {
            if (verb.endsWith("ru") && verb.length >= 3 && verb[0].isKanji) {
                // okay, fixes https://fabric.io/baka/android/apps/sk.baka.aedict3/issues/59c705ecbe077a4dcc23bac8
            } else if (verb.endsWith("有ru")) {
                // okay: 花mo実mo有ru je v5ri
            } else {
                throw IllegalArgumentException("Parameter verb: invalid value $verb: must end with aru")
            }
        }
        return verb.substring(0, verb.length - 2)
    }

    /**
     * Inflects a verb to the appropriate form.
     *
     * @param verb a verb, may be in kanji+hiragana, or in the
     * [sk.baka.aedict.kanji.RomanizationEnum.NihonShiki] romanization. Must be
     * in Base 3 form. Note that "desu" cannot be inflected. Not blank.
     * @return inflected verb, never null, in the
     * [sk.baka.aedict.kanji.RomanizationEnum.NihonShiki] romanization. Kanji
     * characters are left as-is. Not blank.
     */
    fun inflect(verb: String, verbType: VerbType): String {
        val romanized = RomanizationEnum.NihonShiki.r.toRomaji(verb)
        return when {
            romanized == "kuru" || romanized == "来ru" -> inflectKuru()
            verbType.isVss -> {
                require(romanized.endsWith("suru")) { "$verb is marked as vss yet it doesn't end with suru" }
                romanized.removeSuffix("suru") + inflectSuruSpecial()
            }
            romanized.endsWith("suru") -> romanized.substring(0, romanized.length - 4) + inflectSuru()
            romanized == "為ru" -> inflectSuru()
            romanized.endsWith("zuru") -> romanized.substring(0, romanized.length - 4) + inflectZuru()
            romanized == "iku" || romanized == "行ku" -> inflectIku("iku")
            verbType.isV5ks -> inflectIku(romanized)
            verbType.isV5aru -> inflectV5aru(romanized)
            verbType.isV1s -> inflectIchidanKureru(romanized)
            verbType.isV5ri -> {
                stripV5aru(romanized) // this checks that the verb ends with aru
                inflectV5ri(romanized)
            }
            verbType.isV5us -> inflectV5us(romanized)
            verbType.isIchidan -> inflectIchidan(romanized)
            verbType.isVk -> {
                require(romanized.endsWith("kuru") || romanized.endsWith("来ru")) { "$verb is marked as vk yet it doesn't end with kuru" }
                romanized.removeSuffix("kuru").removeSuffix("来ru") + inflectKuru()
            }
            verbType.isGodan -> inflectGodan(romanized)
            else -> throw IllegalArgumentException("Parameter verbType: invalid value $verbType: unsupported verb type; deinflecting $verb")
        }
    }
}

internal class Base1Inflector : AbstractBaseInflector() {

    override val name: String
        get() = "Base 1"

    override fun inflectIchidan(verb: String) = getIchidanForm1(verb)
    override fun inflectV5aru(verb: String) = stripV5aru(verb) + "ra"

    override fun inflectGodan(verb: String): String {
        if (verb.endsWith("au") || verb.endsWith("eu") || verb.endsWith("iu") || verb.endsWith("uu") || verb.endsWith("ou")) {
            return stripGodan(verb) + "wa"
        }
        return if (verb.length >= 2 && verb.endsWith("u") && verb[verb.length - 2].isKanji) {
            // fixes https://github.com/mvysny/aedict/issues/612
            stripGodan(verb) + "wa"
        } else stripGodan(verb) + "a"
    }

    override fun inflectV5ri(verb: String): String = verb.removeSuffix("ru")
    override fun inflectKuru() = "ko"
    override fun inflectSuru() = "si"
    override fun inflectSuruSpecial() = "sa"
    override fun inflectZuru(): String {
        return "zu" // e.g. zunai - http://www.verbix.com/webverbix/Japanese/en%5C'zuru.html
    }
}

/**
 * Continuative / ren'youkei: http://www.epochrypha.com/japanese/materials/verbs/Verb.aspx?rule=renyoukei
 */
internal class Base2Inflector : AbstractBaseInflector() {

    override val name: String
        get() = "Base 2"

    override fun inflectIchidan(verb: String) = getIchidanForm1(verb)
    override fun inflectGodan(verb: String) = stripGodan(verb) + "i"
    override fun inflectV5aru(verb: String) = stripV5aru(verb) + "i"
    override fun inflectKuru() = "ki"
    override fun inflectSuru() = "si"
    override fun inflectSuruSpecial() = "si"

    override fun inflectZuru(): String {
        return "zu" // e.g. en'zumasu, http://www.verbix.com/webverbix/Japanese/en%5C'zuru.html
    }
}

internal class Base3Inflector : AbstractBaseInflector() {

    override val name: String
        get() = "Base 3"

    override fun inflectIchidan(verb: String) = verb
    override fun inflectGodan(verb: String) = verb
    override fun inflectV5aru(verb: String) = verb
    override fun inflectKuru() = "kuru"
    override fun inflectSuru() = "suru"
    override fun inflectZuru() = "zuru"
    override fun inflectSuruSpecial() = "suru"
}

internal class Base4Inflector : AbstractBaseInflector() {

    override val name: String
        get() = "Base 4"

    override fun inflectIchidan(verb: String) = getIchidanForm1(verb) + "re"
    override fun inflectGodan(verb: String) = stripGodan(verb) + "e"
    override fun inflectV5aru(verb: String) = stripV5aru(verb) + "ri"
    override fun inflectKuru() = "kure"
    override fun inflectSuru() = "sure"
    override fun inflectSuruSpecial() = "sie"
    override fun inflectZuru(): String {
        return "zure"  // en'zureba - http://www.verbix.com/webverbix/Japanese/en%5C'zuru.html
    }
}

internal class Base5Inflector : AbstractBaseInflector() {

    override val name: String
        get() = "Base 5"

    override fun inflectIchidan(verb: String) = getIchidanForm1(verb) + "you"
    override fun inflectIchidanKureru(verb: String) = getIchidanForm1(verb)
    override fun inflectGodan(verb: String) = stripGodan(verb) + "ou"
    override fun inflectV5aru(verb: String) = stripV5aru(verb) + "you"
    override fun inflectKuru() = "koyou"
    override fun inflectSuru() = "siyou"
    override fun inflectZuru() = "zuyou"
    override fun inflectSuruSpecial() = "siro"
}

internal class PotentialInflector : AbstractBaseInflector() {

    override val name: String
        get() = "Potential"

    override fun inflectIchidan(verb: String) = getIchidanForm1(verb) + "rareru"
    override fun inflectGodan(verb: String) = stripGodan(verb) + "eru"
    override fun inflectV5aru(verb: String) = stripV5aru(verb) + "eru"
    override fun inflectKuru() = "korareru"
    override fun inflectSuru() = "dekiru"
    override fun inflectZuru() = "zureru"
    override fun inflectSuruSpecial() = "dekiru"
}

internal abstract class AbstractBaseTeTaInflector(te: Boolean) : AbstractBaseInflector() {
    private val ending: Char = if (te) 'e' else 'a'

    override val name: String
        get() = "Base T$ending"

    override fun inflectIchidan(verb: String) = getIchidanForm1(verb) + "t" + ending
    override fun inflectV5us(verb: String) = verb + "t" + ending

    override fun inflectGodan(verb: String): String {
        val stripped = stripGodan(verb)
        val base = stripped.substring(0, stripped.length - 1)
        if (stripped.endsWith("a") || stripped.endsWith("e") || stripped.endsWith("i") || stripped.endsWith("o")
                || stripped.endsWith("u") || stripped[stripped.length - 1].isKanji) {
            return "${stripped}tt$ending"
        }
        if (stripped.endsWith("t") || stripped.endsWith("r")) {
            return "${base}tt$ending"
        }
        if (stripped.endsWith("k")) {
            return "${base}it$ending"
        }
        if (stripped.endsWith("g")) {
            return "${base}id$ending"
        }
        if (stripped.endsWith("s")) {
            return "${base}sit$ending"
        }
        if (stripped.endsWith("n") || stripped.endsWith("b") || stripped.endsWith("m")) {
            return "${base}nd$ending"
        }
        throw RuntimeException("Not a valid japanese base-3 verb: $verb")
    }

    override fun inflectKuru() = "kit$ending"
    override fun inflectSuru() = "sit$ending"
    override fun inflectZuru() = "zut$ending"
    override fun inflectSuruSpecial() = "sit$ending"
    override fun inflectIku(verb: String) = verb.substring(0, verb.length - 2) + "tt" + ending
    override fun inflectV5aru(verb: String) = stripV5aru(verb) + "tt" + ending
}

/**
 * conjunctive (te-form)
 */
internal class BaseTeInflector : AbstractBaseTeTaInflector(true)

internal class BaseTaInflector : AbstractBaseTeTaInflector(false)

data class VerbType(private val dictCode: DictCode) {
    /**
     * true if this is v5 verb. True when [isV5aru] is true.
     */
    val isGodan get() = dictCode.isGodanVerb
    /**
     * true if we know that the verb is ichidan, false if it is godan or irregular, like kuru/suru. True if [isV1s] is true.
     */
    val isIchidan: Boolean get() = dictCode.isIchidanVerb
    /**
     * true if we know that the verb is v5aru, false if it is ichidan, godan, irregular or we don't know.
     */
    val isV5aru: Boolean get() = dictCode == DictCode.v5aru
    /**
     * true if this is [sk.baka.aedict.dict.DictCode.v1s]
     */
    val isV1s: Boolean get() = dictCode == DictCode.v1s
    /**
     * True if this is Godan verb - Iku/Yuku special class
     */
    val isV5ks: Boolean get() = dictCode == DictCode.v5ks

    /**
     * Godan verb with `ru' ending (irregular verb)
     */
    val isV5ri: Boolean get() = dictCode == DictCode.v5ri
    /**
     * Godan verb with `u' ending (special class)
     */
    val isV5us: Boolean get() = dictCode == DictCode.v5us
    val isVk: Boolean get() = dictCode == DictCode.vk

    /**
     * The noun+suru verb.
     */
    val isVss: Boolean get() = dictCode == DictCode.vss

    /**
     * The noun+suru verb.
     */
    val isSuruClass: Boolean get() = isVss

    /**
     * The 来る/kuru verb
     */
    val isKuru: Boolean get() = isVk

    /**
     * The 為る/suru verb
     */
    val isSuru: Boolean get() = dictCode == DictCode.vsi

    companion object {
        val godan = VerbType(DictCode.v5)
        val ichidan = VerbType(DictCode.v1)
        /**
         * Godan verb - -aru special class
         */
        val v5aru = VerbType(DictCode.v5aru)
        /**
         * Ichidan verb - kureru special class
         */
        val v1s = VerbType(DictCode.v1s)
        /**
         * Godan verb - Iku/Yuku special class
         */
        val v5ks = VerbType(DictCode.v5ks)
        /**
         * Godan verb with `ru' ending (irregular verb)
         */
        val v5ri = VerbType(DictCode.v5ri)
        /**
         * Godan verb with `u' ending (special class)
         */
        val v5us = VerbType(DictCode.v5us)
        val vk = VerbType(DictCode.vk)
        val vss = VerbType(DictCode.vss)

        /**
         * Checks if given verb is possibly ichidan.
         * @param verb the verb in nihon-shiki romaji/hiragana/katakana
         * @return true if the [verb] is possibly ichidan, false if there's no way [verb] can be an ichidan verb.
         */
        fun isPossiblyIchidan(verb: String): Boolean {
            val romanizedVerb = IRomanization.NIHON_SHIKI.toRomaji(verb)
            if (!romanizedVerb.endsWith("ru")) return false
            if (romanizedVerb.length >= 3) {
                // 出ru - okay
                // 〆ru, 乄ru - okay? Yes: https://en.wiktionary.org/wiki/%E3%80%86%E3%82%8B
                val c = romanizedVerb[romanizedVerb.length - 3]
                return c.isKanji || c == '〆' || c == '乄' || c == 'e' || c == 'i'
            }
            return false
        }
    }
}

/**
 * The verb type for inflection purposes.
 * @return the verb type or null if the verb can not be inflected.
 */
internal fun JMDictEntry.getVerbType(): VerbType? {
    fun Iterable<DictCode>.getVerbType(): VerbType? = asSequence().mapNotNull { it.verbType }.firstOrNull()

    reading.asSequence().mapNotNull { it.inf.getVerbType() } .firstOrNull { return it }
    // weird - the "verb" marker is present with sense rather than with the reading.
    // Go through the senses and take the first verb type found
    senses.asSequence().mapNotNull { it.pos?.getVerbType() } .firstOrNull { return it }
    return null
}

internal val DictCode.verbType: VerbType? get() = when {
    isGodanVerb || isIchidanVerb || isKuruVerb || isSuruVerb -> VerbType(this)
    else -> null
}
