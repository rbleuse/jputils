/**
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.inflection

import sk.baka.aedict.dict.DictCode
import sk.baka.aedict.dict.JMDictEntry
import sk.baka.aedict.kanji.RomanizationEnum
import java.io.Serializable

/**
 * Inflectable base. Call [getAdjectiveInflections] to get the list of all possible adjective inflections; call [getVerbBaseInflections]
 * and [getVerbFormInflections] to get all possible verb inflections.
 * @property rd Non-null base-3 verb, in case of a verb; adjective ending with i in case of adj-i; the adjective without the "na" in case of adj-na.
 */
class InflectableBase(private val entry: JMDictEntry, val verbType: VerbType?, val adjType: AdjectiveType?, private val rd: JMDictEntry.ReadingData) : Serializable {

    val isVerb get() = verbType != null
    val isAdj get() = adjType != null

    init {
        require(verbType == null || adjType == null) { "Both $verbType and $adjType are not null" }
        if (verbType != null && verbType.isIchidan) {
            require(VerbType.isPossiblyIchidan(rd.reading)) { "$verbType ale $rd nie je ichidan" }
        }
    }

    /**
     * Returns the inflectable base in hiragana (or kanji + hiragana).
     * @param reading if true, we inflect the reading. If false, we inflect the kanji.
     * @return non-null base-3 verb in hiragana/katakana.
     */
    fun getBase(reading: Boolean): String {
        if (!reading) {
            if (rd.kanji == null || rd.kanji.isEmpty()) {
                // applies to all kanjis
                if (!entry.kanji.isEmpty()) {
                    return entry.kanji[0].kanji
                } else {
                    // fall back to reading, no kanji here.
                }
            } else {
                return rd.kanji.first()
            }
        }
        return rd.reading
    }

    /**
     * Returns all possible forms of an adjective. Only works if [isAdj] is true.
     * @param reading if true, we inflect the reading. If false, we inflect the kanji.
     * @return map mapping inflection type to inflected adjective forms, [sk.baka.aedict.kanji.RomanizationEnum.NihonShiki] romanization.
     * Usually returns just one item, but sometimes it could return 2, e.g. for
     * "ku nai desu"/"ku arimasen"
     * @throws IllegalStateException if [isAdj] is false.
     */
    fun getAdjectiveInflections(reading: Boolean = false): Map<VerbInflection.Form, List<String>> {
        check(isAdj) { "This is not adj: $this"}
        val base = getBase(reading)
        return AdjectiveInflection.FORMS.associateWith { form ->
            AdjectiveInflection.inflect(base, form, adjType!!)
        }
    }

    /**
     * Returns Base1-5 + TE/TA base.
     * @param reading if true, we inflect the reading. If false, we inflect the kanji.
     * @return map mapping [AbstractBaseInflector] to inflected verb, in the
     * [sk.baka.aedict.kanji.RomanizationEnum.NihonShiki] romanization. Kanji
     * characters are left as-is. Not blank.
     */
    fun getVerbBaseInflections(reading: Boolean = false): List<Pair<AbstractBaseInflector, String>> {
        check(isVerb) { "This is not verb: $this" }
        val base = getBase(reading)
        return VerbInflection.INFLECTORS.map { inflector -> inflector to inflector.inflect(base, verbType!!) }
    }

    /**
     * @param reading if true, we inflect the reading. If false, we inflect the kanji.
     * @return form mapped to inflected verb in [RomanizationEnum.NihonShiki] romanization. Kanji characters are left as-is.
     */
    fun getVerbFormInflections(reading: Boolean = false): Map<VerbInflection.Form, String> {
        check(isVerb) { "This is not verb: $this" }
        val base = RomanizationEnum.NihonShiki.r.toRomaji(getBase(reading))
        return VerbInflection.Form.values()
                .filterNot { form -> verbType!!.isIchidan && !form.appliesToIchidan() }
                .associateWith { form -> form.inflect(base, verbType!!) }
    }

    override fun toString() = "InflectableBase(verbType=$verbType, adjType=$adjType, rd=$rd, entry=$entry)"
}

/**
 * Returns the inflectable base in hiragana. Returns null if the entry is not a verb nor adjective.
 * @receiver the entry
 * @return non-null base-3 verb or adjective or null if I cannot inflect this kind of word.
 */
fun JMDictEntry.getInflectableBase(): InflectableBase? {
    for (rd in this.reading) {
        for (dictCode in rd.inf) {
            val verbType = dictCode.verbType
            if (verbType != null) {
                // filter out crazy stuff such as 〆ru: https://fabric.io/baka/android/apps/sk.baka.aedict3/issues/5996f9b7be077a4dcc42aca3
                val isVerb = when {
                    verbType.isIchidan && !VerbType.isPossiblyIchidan(rd.reading) -> false
                    else -> true
                }
                if (isVerb) return InflectableBase(this, verbType, null, rd)
            }
            // adjix: adds support for inflecting ii/yoi as per https://github.com/mvysny/aedict/issues/758
            if (dictCode == DictCode.adjix) {
                return InflectableBase(this, null, AdjectiveType.AdjIX, rd)
            }
            if (dictCode == DictCode.adji) {
                return InflectableBase(this, null, AdjectiveType.AdjI, rd)
            }
            if (dictCode == DictCode.adjna) {
                return InflectableBase(this, null, AdjectiveType.AdjNa, rd)
            }
        }
    }
    // weird - the reading doesn't tell whether the word is verb or not.
    // Go through the senses and find out whether it's a verb or not.
    for (sense in senses) {
        if (sense.pos == null) {
            continue
        }
        for (dictCode in sense.pos) {
            val verbType = dictCode.verbType
            if (verbType != null) {
                // filter out crazy stuff such as 〆ru: https://fabric.io/baka/android/apps/sk.baka.aedict3/issues/5996f9b7be077a4dcc42aca3
                val isVerb = when {
                    verbType.isIchidan && !VerbType.isPossiblyIchidan(reading[0].reading) -> false
                    else -> true
                }
                if (isVerb) return InflectableBase(this, verbType, null, reading[0])
            }
            // adjix: adds support for inflecting ii/yoi as per https://github.com/mvysny/aedict/issues/758
            if (dictCode == DictCode.adjix) {
                return InflectableBase(this, null, AdjectiveType.AdjIX, reading[0])
            }
            if (dictCode == DictCode.adji) {
                return InflectableBase(this, null, AdjectiveType.AdjI, reading[0])
            }
            if (dictCode == DictCode.adjna) {
                return InflectableBase(this, null, AdjectiveType.AdjNa, reading[0])
            }
        }
    }
    return null
}

/**
 * Warning: thus may return true and [getInflectableBase] may still return null.
 * This happens for verbs I don't know how to deinflect, e.g. vz 演ずる.
 */
fun JMDictEntry.isInflectableVerb(): Boolean = getVerbType() != null
fun JMDictEntry.isInflectableAdjective(): Boolean = getInflectableBase()?.isAdj ?: false

/**
 * Checks whether given entry is an ichidan verb.
 * @param e the entry, not null.
 * @return true if the entry is an ichidan verb, false if it is not ichidan verb (or is not a verb at all).
 */
fun JMDictEntry.isIchidan(): Boolean = getVerbType()?.isIchidan ?: false
