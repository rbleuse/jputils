/*
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.util

import java.text.ParseException

/**
 * Zoznam JP eras: http://www.sljfaq.org/afaq/dates.html . Musia byt zoradene od najnizsieho startingYear!
 * @param jpname jp meno ery, napr. 嘉永
 * @param enname en meno ery, napr. Kaei
 * @param startingYearGregorian kedy tato era zacina. V tomto roku je rok 1 tejto ery.
 */
enum class JapaneseEra(val jpname: String, val enname: String, val startingYearGregorian: Int) {
    KAEI("嘉永", "Kaei", 1848),
    ANSEI("安政", "Ansei", 1854),
    MANEN("万延", "Man'en", 1860),
    BUNKYU("文久", "Bunkyū", 1861),
    GENJI("元治", "Genji", 1864),
    KEIO("慶応", "Keiō", 1865),
    MEIJI("明治", "Meiji", 1868),
    TAISHO("大正", "Taishō", 1912),
    SHOWA("昭和", "Shōwa", 1926),
    HEISEI("平成", "Heisei", 1989);
    fun eraToGregorian(imperialYear: Int) = startingYearGregorian + imperialYear - 1
    fun gregorianToEra(gregorianYear: Int) = gregorianYear - startingYearGregorian + 1
    companion object {
        /**
         * Only years in this range are convertible to Japanese Imperial Year
         */
        val validGregorianYearRange = values().first().startingYearGregorian .. values().last().startingYearGregorian + 98
    }
}

/**
 * Represents an Japanese Imperial Year.
 * @param gregorianYear the year in the standard Gregorian calendar. Must be in range of [JapaneseEra.validGregorianYearRange]
 * @throws IllegalArgumentException if given year is not in range [JapaneseEra.validGregorianYearRange]
 */
data class JapaneseImperialDate(val gregorianYear: Int) : Comparable<JapaneseImperialDate> {
    init {
        if (gregorianYear !in JapaneseEra.validGregorianYearRange) {
            throw IllegalArgumentException("Invalid parameter gregorianYear: $gregorianYear must be in ${JapaneseEra.validGregorianYearRange}")
        }
    }
    val era: JapaneseEra
    get() = JapaneseEra.values().reversed().first { era -> era.startingYearGregorian <= gregorianYear }

    override fun toString() = "$gregorianYear (${era.enname} ${era.gregorianToEra(gregorianYear)})"

    override fun compareTo(other: JapaneseImperialDate) = gregorianYear.compareTo(other.gregorianYear)

    /**
     * Formats the year in the JP imperial format, in the form of 平成1年
     */
    val imperial: String
    get() = "${era.jpname}${era.gregorianToEra(gregorianYear)}年"

    companion object {
        private val pattern = "(${JapaneseEra.values().joinToString("|", transform = { it.jpname })})\\s*([0-9$jpdigits]+)\\s*年?.*".toPattern()
        /**
         * Tries to parse a Japanese imperial date
         * @param jp the imperial date, e.g. 平成1 or 明治一 or 平成1年 or 明治二年
         * @throws ParseException when given text is not in a valid imperial year format.
         */
        fun parseJp(jp: String): JapaneseImperialDate {
            val matcher = pattern.matcher(jp)
            if (!matcher.matches()) throw ParseException("$jp: unknown format", 0)
            val match = matcher.toMatchResult()!!
            val eraJpName = match.group(1)
            val era = JapaneseEra.values().first { it.jpname == eraJpName }
            val eraYearUnparsed = matcher.group(2)
            val eraYear =
            if (eraYearUnparsed[0] in '0'..'9') {
                try {
                    eraYearUnparsed.toInt()
                } catch (e: NumberFormatException) {
                    throw ParseException("$jp: failed to parse era year: $e", 0)
                }
            } else eraYearUnparsed.parseJpNumber()
            if (eraYear !in 1..99) throw ParseException("$jp: era year must be 1..99", 0)
            return JapaneseImperialDate(era.eraToGregorian(eraYear))
        }
    }
}

private val jpdigits = "一二三四五六七八九十"
/**
 * Parses this string as a Japanese kanji-based number such as 二十一
 * @return parsed number value
 */
fun String.parseJpNumber(): Int {
    if (length == 0) throw ParseException("Empty string", 0)
    var result = 0
    for (digit in this) {
        if (digit == '十') {
            if (result == 0) result = 10 else result *= 10
        } else {
            val digitValue = jpdigits.indexOf(digit)
            if (digitValue < 0) throw ParseException("$this: contains invalid digit $digit", 0)
            result += digitValue + 1
        }
    }
    return result
}
