/*
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sk.baka.android.DirUtils;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigInteger;
import java.util.*;
import java.util.zip.GZIPOutputStream;

/**
 * Utility metody na stabilnu "serializaciu".
 * <p></p>
 * NEMOZEM ZMAZAT TUTO TRIEDU - kvoli spatnej kompatibilite ju treba, aby Aedict vedel precitat stare Tagy/Notepad/RecentlyViewed...
 * Nie je deprecated, pretoze bude nadalej pouzivana kvoli spatnej kompatibilite, ale primarne ukladat objekty cez Box.
 */
public class Writables {
    /**
     * Writes a list of strings.
     *
     * @param strings a list of strings, may be null.
     * @param out     writes it here. Not null.
     * @throws IOException on I/O error
     */
    public static void writeListOfStrings(@Nullable Collection<String> strings, @NotNull DataOutput out) throws IOException {
        if (strings == null) {
            out.writeInt(-1);
            return;
        }
        out.writeInt(strings.size());
        for (String string : strings) {
            out.writeUTF(string);
        }
    }

    /**
     * Writes a list of writables. Warning: the list can be deserialized back by {@link #readListOfWritables(Class, java.io.DataInput)}
     * only if all writables are of exactly the same class!
     *
     * @param writables a list of writables, may be null.
     * @param out       writes it here, not null.
     * @throws IOException on I/O error
     */
    public static void writeListOfWritables(@Nullable Collection<? extends Writable> writables, @NotNull DataOutput out) throws IOException {
        if (writables == null) {
            out.writeInt(-1);
            return;
        }
        out.writeInt(writables.size());
        for (Writable w : writables) {
            w.writeTo(out);
        }
    }

    /**
     * Writes given enum set to the output. Only enums with up to 2^127 constants are supported.
     *
     * @param set the set, may be null.
     * @param out output stream, not null.
     * @param <E> the enum item type
     * @throws IOException on I/O error
     */
    public static <E extends Enum<E>> void writeEnumSet(@Nullable EnumSet<? extends E> set, @NotNull DataOutput out) throws IOException {
        if (set == null) {
            out.writeByte(-1);
            return;
        }
        BigInteger bi = BigInteger.ZERO;
        for (E c : set) {
            bi = bi.setBit(c.ordinal());
        }
        final byte[] bytes = bi.toByteArray();
        if (bytes.length > 127) {
            throw new IllegalArgumentException("Parameter set: invalid value - contains too much enums");
        }
        out.write(bytes.length);
        out.write(bytes);
    }

    /**
     * Reads enum set from given stream. Callee is responsible to provide correct enum class.
     *
     * @param clazz the enum class, not null.
     * @param in    the input stream, not null.
     * @param <E>   the enum item type.
     * @return enum set, may be null.
     * @throws IOException on I/O error
     */
    @Nullable
    public static <E extends Enum<E>> EnumSet<E> readEnumSet(@NotNull Class<E> clazz, @NotNull DataInput in) throws IOException {
        final int count = in.readByte();
        if (count < 0) {
            return null;
        }
        final byte[] bytes = new byte[count];
        in.readFully(bytes);
        final BigInteger bi = new BigInteger(bytes);
        final EnumSet<E> result = EnumSet.noneOf(clazz);
        for (int i = 0; i < bi.bitLength(); i++) {
            if (bi.testBit(i)) {
                result.add(clazz.getEnumConstants()[i]);
            }
        }
        return result;
    }

    /**
     * Reads set of strings stored by {@link #writeListOfStrings(java.util.Collection, java.io.DataOutput)}.
     *
     * @param in input stream, not null.
     * @return a set of strings, may be null.
     * @throws IOException on I/O error
     */
    @Nullable
    public static Set<String> readSetOfStrings(@NotNull DataInput in) throws IOException {
        final List<String> strings = readListOfStrings(in);
        return strings == null ? null : new HashSet<String>(strings);
    }

    /**
     * Reads list of strings stored by {@link #writeListOfStrings(java.util.Collection, java.io.DataOutput)}.
     *
     * @param in input stream, not null.
     * @return a list of strings, may be null.
     * @throws IOException on I/O error
     */
    @Nullable
    public static List<String> readListOfStrings(@NotNull DataInput in) throws IOException {
        final int count = in.readInt();
        if (count < 0) {
            return null;
        }
        final List<String> result = new ArrayList<String>(count);
        for (int i = 0; i < count; i++) {
            result.add(in.readUTF());
        }
        return result;
    }

    /**
     * Reads a list of writables persisted by previous call to {@link #writeListOfWritables(java.util.Collection, java.io.DataOutput)}.
     *
     * @param clazz all writables must have this class. Not null.
     * @param in the input stream, not null.
     * @param <T> expected writable type
     * @return list of writables, may be null.
     * @throws IOException on I/O error
     */
    @Nullable
    public static <T extends Writable> List<T> readListOfWritables(@NotNull Class<T> clazz, @NotNull DataInput in) throws IOException {
        final int count = in.readInt();
        if (count < 0) {
            return null;
        }
        final List<T> result = new ArrayList<T>(count);
        for (int i = 0; i < count; i++) {
            result.add(read(clazz, in));
        }
        return result;
    }

    /**
     * Writes given writable and returns its serialized representation.
     *
     * @param writable the writable, not null.
     * @return the byte representation.
     */
    @NotNull
    public static byte[] write(@NotNull Writable writable) {
        try {
            final ByteArrayOutputStream output = new ByteArrayOutputStream();
            write(writable, output);
            output.close();
            return output.toByteArray();
        } catch (IOException ex) {
			// do not use IOError, not present on Android 2.2
            throw new RuntimeException(ex);
        }
    }

    /**
     * Writes writable to given output stream.
     * @param writable writable
     * @param out output stream, NOT CLOSED, flushed. Automatically wrapped in {@link BufferedOutputStream}
     */
    public static void write(@NotNull Writable writable, @NotNull OutputStream out) throws IOException {
        // urcite pouzit BufferedOutputStream: na Androide zapis 4000ms bez buffered, 400ms (10x rychlejsi) s buffered.
        // 8192 staci, skusal som 65536 buffer a performance sa nezvysila.
        if (!(out instanceof BufferedOutputStream)) {
            out = new BufferedOutputStream(out, 65536);
        }
        // !!! DO NOT USE ObjectOutputStream - it writes 5 magic bytes in front of the stream.
        final DataOutputStream dout = new DataOutputStream(out);
        writable.writeTo(dout);
        dout.flush();
    }

    /**
     * Reads given writable from a file.
     *
     * @param writable the writable, not null.
     * @param file the target file, not null.
     * @return writable instance, not null.
     * @throws IOException on I/O error
     */
    @NotNull
    public static <T extends Writable> T read(@NotNull Class<T> writable, @NotNull File file) throws IOException {
        return read(writable, new FileInputStream(file));
    }

    /**
     * Reads given writable from a file.
     *
     * @param writable the writable, not null.
     * @param in the source stream, not null. ALWAYS CLOSED.
     * @return writable instance, not null.
     * @throws IOException on I/O error
     */
    @NotNull
    public static <T extends Writable> T read(@NotNull Class<T> writable, @NotNull InputStream in) throws IOException {
        if (!(in instanceof BufferedInputStream)) {
            in = new BufferedInputStream(in, 65536);
        }
        try {
            return read(writable, (DataInput) new DataInputStream(in));
        } finally {
            UtilKt.closeQuietly(in);
        }
    }

    /**
     * Reads the writable from given input. The writable type is not auto-detected: it must instead be provided
     * by the {@code clazz} parameter.
     *
     * @param clazz expected class, not null.
     * @param in input stream, not null. Not closed.
     * @param <T> expected type
     * @return the writable, never null.
     * @throws IOException on I/O error.
     */
    @NotNull
    public static <T extends Writable> T read(@NotNull Class<T> clazz, @NotNull DataInput in) throws IOException {
        try {
            final Method readFrom = clazz.getMethod("readFrom", DataInput.class);
            return clazz.cast(readFrom.invoke(null, in));
        } catch (InvocationTargetException ex) {
            if (ex.getCause() instanceof IOException) {
                throw (IOException) ex.getCause();
            }
            // kokot jebnuty android mi urezal stacktrace a nezobrazil caused-by, tak si to zobrazim tu.
            throw new RuntimeException("Failed to invoke " + clazz.getName() + ".readFrom(DataInput): " + ex.getCause(), ex);
        } catch (Exception ex) {
            throw new RuntimeException("Failed to invoke " + clazz.getName() + ".readFrom(DataInput): " + ex, ex);
        }
    }

    /**
     * Reads the writable serialized by {@link #write(Writable)}. The writable type is not auto-detected: it must instead be provided
     * by the {@code clazz} parameter.
     *
     * @param clazz the writable class, not null.
     * @param bytes serialized representation, not null.
     * @param <T>   the writable type.
     * @return the writable, never null.
     */
    @NotNull
    public static <T extends Writable> T read(@NotNull Class<T> clazz, @NotNull byte[] bytes) {
		try {
			return read(clazz, (DataInput) new DataInputStream(new ByteArrayInputStream(bytes, 0, bytes.length)));
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

    /**
     * Writes string to out. WARNING: not compatible with {@link java.io.DataInput#readUTF()}.
     *
     * @param str string, may be null
     * @param out target, not null.
     * @throws IOException on I/O error
     */
    public static void writeNullableUTF(@Nullable String str, @NotNull DataOutput out) throws IOException {
        out.writeBoolean(str != null);
        if (str != null) {
            out.writeUTF(str);
        }
    }

    /**
     * Reads string. WARNING: not compatible with {@link java.io.DataOutput#writeUTF(String)}.
     *
     * @param in target, not null.
     * @return string, may be null
     * @throws IOException on I/O error
     */
    @Nullable
    public static String readNullableUTF(@NotNull DataInput in) throws IOException {
        final boolean nullable = in.readBoolean();
        if (!nullable) {
            return null;
        }
        return in.readUTF();
    }

	/**
	 * Writes this boxable directly to a file. A temp file is first created; after the write is successful,
	 * the file is moved over the requested file. This is to protect the original file contents, in case of a write error.
	 * @param writable the writable, not null.
	 * @param target the file to write to, not null
     * @param gzip if true, the stream is gzipped.
	 * @throws IOException on i/o error.
	 */
    public static void write(@NotNull Writable writable, @NotNull File target, boolean gzip) throws IOException {
        Check.requireNotNull(writable);
		Check.requireNotNull(target);
		target = target.getAbsoluteFile();
		boolean deleteTmp = true;
		final Timing t = new Timing("create-temp-file");
		// first, write to a temp file. If this succeeds, overwrite the live file.
		// to by malo zabranit tomu, ze userovi na disku vznikne subor s nulovou dlzkou - to nikdy nesmie nastat,
		// lebo tak user efektivne pride o svoje data!
		final File tmp = File.createTempFile("box", null, target.getParentFile());
		try {
			t.mark("open-temp");
            OutputStream out = new FileOutputStream(tmp);
            if (gzip) {
                out = new GZIPOutputStream(out);
            }
			try {
				t.mark("write");
                write(writable, out);
			} finally {
				UtilKt.closeQuietly(out);
			}
			t.mark("rename");
			if (!tmp.exists()) {
				throw new IOException(tmp + ": File is reported to be written successfully but it does not exist");
			}
			if (!tmp.isFile()) {
				throw new IOException(tmp + ": File is reported to be written successfully but it is not a file");
			}
			if (tmp.length() == 0) {
				throw new IOException(tmp + ":File is reported to be written successfully but its size is zero");
			}
            DirUtils.deleteNonRec(target);
            DirUtils.rename(tmp, target);
            // tmp uz neexistuje, lebo bol uspesne premenovany. netreba mazat.
            deleteTmp = false;
            // nie info, sak to zasere log. a je to tam same ze Written Box (ani neviem co sa zapisalo). Aspon v PhotoFrame
			log.debug("Written " + writable.getClass() + " to " + target + " (" + target.length() + "b) in " + t.finish());
		} finally {
			if (deleteTmp) {
				DirUtils.deleteNonRecQuietly(tmp);
			}
		}
    }

	private static final Logger log = LoggerFactory.getLogger(Writables.class);

    @Nullable
    public static <T extends Writable> T clone(@Nullable T writable) {
        return writable == null ? null : (T) read(writable.getClass(), write(writable));
    }
}
