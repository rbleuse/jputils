/*
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.util;

import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Contains *a subset present in Tatoeba* of the ISO 639-3 language codes.
 * @author Martin Vysny
 */
public class Iso6393Codes {

    private static final Map<Language, String> CODES = new HashMap<Language, String>();

    static {
        final String[] packedCodes = {"acm", "Mesopotamian Arabic", "afr", "Afrikaans", "ara", "Arabic", "arz", "Egyptian Arabic", "bel", "Belarusian", "ben", "Bengali", "bos", "Bosnian", "bre", "Breton", "bul", "Bulgarian", "cat", "Catalan", "ces", "Czech", "cha", "Chamorro", "cmn", "Mandarin Chinese", "dan", "Danish", "deu", "German", "ell", "Modern Greek (1453-)", "eng", "English", "epo", "Esperanto", "est", "Estonian", "eus", "Basque", "fao", "Faroese", "fin", "Finnish", "fra", "French", "fry", "Western Frisian", "gle", "Irish", "glg", "Galician", "heb", "Hebrew", "hin", "Hindi", "hrv", "Croatian", "hun", "Hungarian", "hye", "Armenian", "ina", "Interlingua (International Auxiliary Language Association)", "ind", "Indonesian", "isl", "Icelandic", "ita", "Italian", "jbo", "Lojban", "kat", "Georgian", "kaz", "Kazakh", "kor", "Korean", "lat", "Latin", "lit", "Lithuanian", "lvs", "Standard Latvian", "lzh", "Literary Chinese", "mal", "Malayalam", "mon", "Mongolian", "nan", "Min Nan Chinese", "nds", "Low German", "nld", "Dutch", "nob", "Norwegian Bokmål", "non", "Old Norse", "orv", "Old Russian", "oss", "Ossetian", "pes", "Iranian Persian", "pol", "Polish", "por", "Portuguese", "que", "Quechua", "roh", "Romansh", "ron", "Romanian", "rus", "Russian", "scn", "Sicilian", "slk", "Slovak", "slv", "Slovenian", "spa", "Spanish", "sqi", "Albanian", "srp", "Serbian", "swe", "Swedish", "swh", "Swahili (individual language)", "tat", "Tatar", "tgl", "Tagalog", "tha", "Thai", "tlh", "Klingon", "tur", "Turkish", "uig", "Uighur", "ukr", "Ukrainian", "urd", "Urdu", "uzb", "Uzbek", "vie", "Vietnamese", "vol", "Volapük", "wuu", "Wu Chinese", "yid", "Yiddish", "yue", "Yue Chinese", "zsm", "Standard Malay"};
        for (int i = 0; i < packedCodes.length / 2; i++) {
            CODES.put(new Language(packedCodes[i * 2]), packedCodes[i * 2 + 1]);
        }
    }

    public static SortedMap<String, Language> getSortedLangNames() {
        final SortedMap<String, Language> result= new TreeMap<String, Language>();
        for(final Map.Entry<Language, String> e:CODES.entrySet()){
            result.put(e.getValue(), e.getKey());
        }
        return result;
    }
}
