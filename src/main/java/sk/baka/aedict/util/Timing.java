/*
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.LinkedList;
import java.util.List;

/**
 * Measures duration of sequential parts of an algorithm.
 * @author mvy
 */
public final class Timing {
    private long currentMarkStart;
    private final long start;
    @Nullable
    private String currentMarkName;
    private static class Mark {
        @NotNull
        public final String name;
        public final long duration;

        public Mark(@NotNull String name, long duration) {
            this.name = name;
            this.duration = duration;
        }

        public void appendTo(@NotNull StringBuilder sb) {
            sb.append(name).append(": ").append(duration).append("ms");
        }
    }
    private final List<Mark> marks = new LinkedList<Mark>();

    /**
     * Creates the timing object
     * @param markName first part name, not null.
     */
    public Timing(@NotNull String markName) {
        start = System.currentTimeMillis();
        currentMarkStart = start;
        mark(markName);
    }

    /**
     * Marks an end of previous part and a beginning of a next part.
     * @param name the next part name, not null
     * @return this
     */
    public Timing mark(@NotNull String name) {
        finishMark();
        currentMarkName = name;
        return this;
    }

    private void finishMark() {
        if (currentMarkName != null) {
            final long newMarkStart = System.currentTimeMillis();
            marks.add(new Mark(currentMarkName, newMarkStart - currentMarkStart));
            currentMarkStart = newMarkStart;
            currentMarkName = null;
        }
    }

    /**
     * Finishes the timing measuring and returns total string in form of:
     * 152ms (bootup 100ms, processing 50ms, cleanup 2ms)
     * @return summary string, not null.
     */
    @NotNull
    public String finish() {
        finishMark();
        final StringBuilder sb = new StringBuilder();
        for (Mark mark : marks) {
            if (sb.length() > 0) {
                sb.append(", ");
            }
            mark.appendTo(sb);
        }
        return getDuration() + "ms (" + sb + ")";
    }

    /**
     * Return the total timing duration.
     * @return the duration, 0 or greater.
     */
    public long getDuration() {
        return System.currentTimeMillis() - start;
    }

    @Override
    public String toString() {
        return finish();
    }
}
