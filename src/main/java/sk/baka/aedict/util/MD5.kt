/*
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.util

import sk.baka.aedict.util.MD5.Companion.MD5_LEN
import sk.baka.aedict.util.typedmap.Box
import java.io.DataInput
import java.io.DataOutput
import java.io.Serializable
import java.security.MessageDigest
import java.util.*

/**
 * Represents a [MD5_LEN]-byte MD5 checksum. Immutable, thread-safe.
 * @property checksum the checksum, not null, must be exactly [MD5_LEN]-byte long.
 * @author mvy
 */
class MD5(private val checksum: ByteArray) : Serializable, Writable, Boxable {

    /**
     * Returns the MD5 sum as a 32 character long hexa string.
     * @return base-16 string, never null.
     */
    fun toHexString() = checksum.toHex("")

    /**
     * Returns [toHexString].
     * @return base-16 string, never null.
     */
    override fun toString() = toHexString()

    override fun writeTo(out: DataOutput) {
        out.write(checksum)
    }

    init {
        require(checksum.size == MD5_LEN) { "Parameter checksum: invalid value ${checksum.size}: expected $MD5_LEN bytes" }
    }

    override fun box(): Box {
        return Box().putByteArray("md5", checksum)
    }

    override fun equals(other: Any?): Boolean{
        if (this === other) return true
        if (other?.javaClass != javaClass) return false
        other as MD5
        if (!Arrays.equals(checksum, other.checksum)) return false
        return true
    }

    override fun hashCode() = Arrays.hashCode(checksum)

    companion object : Readable<MD5>, Unboxable<MD5> {
        private val MD5_LEN = 16

        /**
         * An empty, all-zeroes md5.
         */
        val EMPTY = MD5(ByteArray(MD5_LEN))

        @JvmStatic
        override fun readFrom(di: DataInput): MD5 {
            val sourceMD5 = ByteArray(MD5_LEN)
            di.readFully(sourceMD5)
            return MD5(sourceMD5)
        }

        @JvmStatic
        override fun unbox(box: Box) = MD5(box.get("md5").byteArray().get())
    }
}

/**
 * Computes the MD5 checksum of given byte array in-memory.
 */
val ByteArray.md5: MD5 get()= MD5(MessageDigest.getInstance("MD5").digest(this))
private val HEX_DIGITS = "0123456789abcdef".toCharArray()
/**
 * Returns this byte array as a hexadecimal lower-case string, e.g. `0025deadbeef`.
 */
fun ByteArray.toHex(separator: String = ":"): String {
    val buf = StringBuilder(size * (2 + separator.length))
    for (i in this) {
        if (buf.isNotEmpty()) buf.append(separator)
        buf.append(HEX_DIGITS[i.toInt() and 0xf0 ushr 4])
        buf.append(HEX_DIGITS[i.toInt() and 0x0f])
    }
    return buf.toString()
}

fun String.hexStringToByteArray() : ByteArray {
    val result = ByteArray(length / 2)
    for (i in 0 until length step 2) {
        val firstIndex = Character.digit(this[i], 16);
        val secondIndex = Character.digit(this[i + 1], 16);
        val octet = firstIndex.shl(4).or(secondIndex)
        result[i.shr(1)] = octet.toByte()
    }
    return result
}
