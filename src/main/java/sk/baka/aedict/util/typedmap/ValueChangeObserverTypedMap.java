/**
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.util.typedmap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;

/**
 * Observes all changes to the delegated typed map.
 * @author mvy
 */
public class ValueChangeObserverTypedMap extends DelegatingTypedMap implements Serializable {

	@NotNull
	public final ValueChangedListener listener;

    /**
     * Creates the observer.
     * @param delegate delegates all calls to this map.
     * @param listener listens for all value changes.
     */
	public ValueChangeObserverTypedMap(@NotNull AbstractTypedMap<?> delegate, @NotNull ValueChangedListener listener) {
		super(delegate);
		this.listener = listener;
	}

	@NotNull
	@Override
	protected DelegatingTypedMap put(@NotNull String key, @Nullable Object value) {
		if (value != null) {
			super.put(key, null);
		}
        super.put(key, value);
		// najprv nastav hodnotu, az potom notifikuj, aby si pripadny reader precital uz novu hodnotu.
		listener.onValueChanged(key, value);
		return this;
	}

	public interface ValueChangedListener extends Serializable {
        /**
         * Fired when the value has been changed.
         * @param key the key, not null.
         * @param value the new value, may be null.
         */
		void onValueChanged(@NotNull String key, @Nullable Object value);
	}
}
