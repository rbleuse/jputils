/**
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.util.typedmap;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.lang.reflect.Array;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import sk.baka.aedict.util.*;

/**
 * An abstract typed map storage, which allows to store various types of objects into a map, and allows
 * casting.
 * @author mvy
 */
public abstract class AbstractTypedMap<T extends AbstractTypedMap<T>> {

	/**
	 * Gets a value from this map.
	 * @param key the key, not null.
	 * @return the value object, use it to further cast the result.
     */
	@NotNull
	public abstract Value<Object> get(@NotNull String key);

	@NotNull
	protected abstract T put(@NotNull String key, @Nullable Object value);

	@NotNull
	public T putString(@NotNull String key, @Nullable String value) {
		return put(key, value);
	}

	@NotNull
	public T putInt(@NotNull String key, @Nullable Integer value) {
		return put(key, value);
	}

	@NotNull
	public T remove(@NotNull String key) {
		return put(key, null);
	}

	@NotNull
	public T putShort(@NotNull String key, @Nullable Short value) {
		return put(key, value);
	}

	@NotNull
	public T putFloat(@NotNull String key, @Nullable Float value) {
		return put(key, value);
	}

	@NotNull
	public T putByte(@NotNull String key, @Nullable Byte value) {
		return put(key, value);
	}

	@NotNull
	public T putBoolean(@NotNull String key, @Nullable Boolean value) {
		return put(key, value);
	}

	@NotNull
	public T putBitSet(@NotNull String key, @Nullable BitSet value) {
		return putByteArray(key, value == null ? null : value.toByteArray());
	}

	@NotNull
	public T putUUID(@NotNull String key, @Nullable UUID uuid) {
		return put(key, uuid);
	}

	@NotNull
    public <E extends Enum<E>> T putEnum(@NotNull String key, @Nullable E value) {
        if (value == null) {
            return remove(key);
        } else {
            if (value.ordinal() > Box.MAX_ENUM_LENGTH) {
                throw new IllegalArgumentException("Parameter value: invalid value " + value + ": has ordinal of " + value.ordinal());
            }
            return putByte(key, (byte) value.ordinal());
        }
    }

	/**
	 * Pozor enum konstanty ukladane takto sa nesmu uz nikdy reorderovat! Je totiz ukladany ordinal, a nie name!
	 * @param key kluc
	 * @param value value
	 * @param <E> enum type
	 * @return this
	 */
	@NotNull
	public <E extends Enum<E>> T putEnumSet(String key, @Nullable EnumSet<E> value) {
		return put(key, BoxedEnumSet.of(value));
	}

	/**
	 * Only enums with up to 2^8^127 constants are supported.
	 */
	protected static class BoxedEnumSet implements Writable {
		@NotNull
		private final byte[] packed;

		@Nullable
		protected static <E extends Enum<E>> BoxedEnumSet of(@Nullable EnumSet<E> set) {
			return set == null ? null : new BoxedEnumSet(set);
		}
		private <E extends Enum<E>> BoxedEnumSet(@NotNull EnumSet<E> set) {
			BigInteger bi = BigInteger.ZERO;
			for (E c : set) {
				bi = bi.setBit(c.ordinal());
			}
			packed = bi.toByteArray();
			if (packed.length > 127) {
				throw new IllegalArgumentException("Parameter set: invalid value - contains too much enums");
			}
		}
		private BoxedEnumSet(@NotNull byte[] packed) {
			this.packed = packed;
		}
		@NotNull
		public <E extends Enum<E>> EnumSet<E> toEnumSet(@NotNull Class<E> clazz) {
			final BigInteger bi = new BigInteger(packed);
			final EnumSet<E> result = EnumSet.noneOf(clazz);
			for (int i = 0; i < bi.bitLength(); i++) {
				if (bi.testBit(i)) {
					// spatna kompatibilita - ak su pridane nove enum konstanty a my ich este nepozname, ignoruj ich.
					if (isValidOrdinal(clazz, i)) {
						result.add(clazz.getEnumConstants()[i]);
					}
				}
			}
			return result;
		}

		@Override
		public void writeTo(@NotNull DataOutput out) throws IOException {
			out.writeByte(packed.length);
			out.write(packed);
		}

		@NotNull
		public static BoxedEnumSet readFrom(@NotNull DataInput in) throws IOException {
			final byte[] packed = new byte[in.readByte()];
			in.readFully(packed);
			return new BoxedEnumSet(packed);
		}

		@Override
		@NotNull
		public String toString() {
			return new BigInteger(packed).toString(16);
		}

		@Override
		public boolean equals(Object o) {
			if (this == o) return true;
			if (o == null || getClass() != o.getClass()) return false;
			BoxedEnumSet that = (BoxedEnumSet) o;
			if (!Arrays.equals(packed, that.packed)) return false;
			return true;
		}

		@Override
		public int hashCode() {
			return Arrays.hashCode(packed);
		}
	}

	protected static class BoxedByteArray implements Writable {
		@NotNull
		private final byte[] bytes;

		@Override
		public boolean equals(Object o) {
			if (this == o) return true;
			if (o == null || getClass() != o.getClass()) return false;
			BoxedByteArray that = (BoxedByteArray) o;
			if (!Arrays.equals(bytes, that.bytes)) return false;
			return true;
		}

		@Override
		public int hashCode() {
			return Arrays.hashCode(bytes);
		}

		public BoxedByteArray(@NotNull byte[] bytes) {
			this.bytes = Check.requireNotNull(bytes);
		}

		@Override
		public void writeTo(@NotNull DataOutput out) throws IOException {
			out.writeInt(bytes.length);
			out.write(bytes);
		}

		@NotNull
		public static BoxedByteArray readFrom(@NotNull DataInput in) throws IOException {
			final byte[] bytes = new byte[in.readInt()];
			in.readFully(bytes);
			return new BoxedByteArray(bytes);
		}

		@Nullable
		public static BoxedByteArray of(@Nullable byte[] bytes) {
			return bytes == null ? null : new BoxedByteArray(bytes);
		}
	}

	/**
	 * Immutable.
	 * @param <T> the value type
	 */
	public static class Value<T> {
		@NotNull
		public final String key;
		@Nullable
		public final T value;

		public Value(@NotNull String key, @Nullable T value) {
			this.key = key;
			this.value = value;
		}

		/**
		 * Returns the value. Fails if the value is null.
		 * @return the value, never null.
		 */
		@NotNull
		public T get() {
			checkPresent();
			assert value != null;
			return value;
		}

		/**
		 * Returns the value
		 * @return the value, may be null.
		 */
		@Nullable
		public T orNull() {
			return value;
		}

		/**
		 * Fails if the {@link #value} is null.
		 * @return this
		 */
		@NotNull
		public Value<T> checkPresent() {
			if (!isPresent()) {
				throw fail("must be present");
			}
			return this;
		}

		/**
		 * Returns the {@link #value} or given default value if the {@link #value} is null.
		 * @param defaultValue optional default value, not null.
		 * @return non-null value.
		 */
		@NotNull
		public T or(@NotNull T defaultValue) {
			return value == null ? defaultValue : value;
		}

		/**
		 * Casts the value to given class.
		 * @param clazz the class, not null.
		 * @param <V> the class type
		 * @return cast value, not null.
		 */
		@NotNull
		public <V> Value<V> cast(@NotNull Class<V> clazz) {
			if (value != null && !clazz.isInstance(value)) {
				throw fail("must be instance of " + clazz.getName());
			}
			clazz.cast(value);
			return (Value<V>) (Value) this;
		}

        @NotNull
        public <E extends Enum<E>> Value<E> enumValue(@NotNull Class<E> clazz) {
            if (value == null) {
                return cast(clazz);
            }
            final int ordinal = byteValue().get();
            return new Value<E>(key, clazz.getEnumConstants()[ordinal]);
        }

		/**
		 * Checks if the {@link #value} is present (is not null).
		 * @return true iff value is not null.
		 */
		public boolean isPresent() {
			return value != null;
		}

		/**
		 * Fails with given cause. Use to say that the value is invalid, e.g.
		 * {@code throw fail("must be greater than 5");}
		 * @param cause the cause, should not be null.
		 * @return never returns
		 */
		@NotNull
		public IllegalStateException fail(@NotNull String cause) {
			throw new IllegalStateException(key + ": invalid value " + value + " of type " + (value == null ? "null" : value.getClass().getName()) + ": " + cause);
		}

		/**
		 * Casts to String.
		 * @return new value, not null.
		 */
		@NotNull
		public Value<String> string() {
			return cast(String.class);
		}
		/**
		 * Casts to Integer.
		 * @return new value, not null.
		 */
		@NotNull
		public Value<Integer> integer() {
			return cast(Integer.class);
		}
		/**
		 * Casts to Short.
		 * @return new value, not null.
		 */
		@NotNull
		public Value<Short> shortValue() {
			return cast(Short.class);
		}
		/**
		 * Casts to Byte.
		 * @return new value, not null.
		 */
		@NotNull
		public Value<Byte> byteValue() {
			return cast(Byte.class);
		}
		/**
		 * Casts to Float.
		 * @return new value, not null.
		 */
		@NotNull
		public Value<Float> floatValue() {
			return cast(Float.class);
		}
		/**
		 * Casts to Boolean.
		 * @return new value, not null.
		 */
		@NotNull
		public Value<Boolean> bool() {
			return cast(Boolean.class);
		}
		/**
		 * Casts to Long.
		 * @return new value, not null.
		 */
		@NotNull
		public Value<Long> longValue() { return cast(Long.class); }
		/**
		 * Casts to byte array.
		 * @return new value, not null.
		 */
		@NotNull
		public Value<byte[]> byteArray() {
			final BoxedByteArray b = cast(BoxedByteArray.class).orNull();
			return new Value<>(key, b == null ? null : b.bytes);
		}

		@NotNull
		public Value<BitSet> bitSet() {
			final BoxedByteArray b = cast(BoxedByteArray.class).orNull();
			return new Value<>(key, b == null ? null : BitSet.valueOf(b.bytes));
		}

		@NotNull
		public Value<boolean[]> booleanArray() {
			if (!isPresent()) {
				return cast(boolean[].class);
			}
			final byte[] bytes = byteArray().get();
			final int size = bytes[0] & 0xFF + ((bytes[1] & 0xFF) * 0x100);
			final boolean[] result = new boolean[size];
			final BitSet bitSet = BitSet.valueOf(ByteBuffer.wrap(bytes, 2, bytes.length - 2));
			for (int i = 0; i < result.length; i++) {
				result[i] = bitSet.get(i);
			}
			return new Value<>(key, result);
		}

		@NotNull
		public <V extends Boxable> Value<V> boxable(@NotNull Class<V> clazz) {
			final BoxedBox<?> box = cast(BoxedBox.class).orNull();
			return new Value<V>(key, box == null ? null : box.unbox(clazz));
		}
		@NotNull
		public <E extends Enum<E>> Value<EnumSet<E>> enumSet(@NotNull Class<E> clazz) {
			final BoxedEnumSet set = cast(BoxedEnumSet.class).orNull();
			return new Value<>(key, set == null ? null : set.toEnumSet(clazz));
		}

		@NotNull
		public <T extends Boxable> Value<List<T>> boxables(@NotNull Class<T> clazz) {
			final BoxedListOfBoxes boxes = cast(BoxedListOfBoxes.class).orNull();
			return new Value<List<T>>(key, boxes == null ? null : boxes.unbox(clazz));
		}
		@NotNull
		public <T> Value<List<T>> listOf(@NotNull Class<T> itemClass) {
			if (Boxable.class.isAssignableFrom(itemClass)) {
				return (Value) boxables(itemClass.asSubclass(Boxable.class));
			}
			return (Value<List<T>>) (Value) cast(List.class);
		}
		@NotNull
		public <T> Value<Set<T>> setOf(@NotNull Class<T> itemClass) {
            if (Boxable.class.isAssignableFrom(itemClass)) {
                throw new IllegalArgumentException("Parameter itemClass: invalid value " + itemClass + ": only basic types are supported");
            }
            if (value == null || value instanceof Set) {
                return (Value<Set<T>>) (Value) cast(Set.class);
            }
            return new Value<Set<T>>(key, new HashSet<T>((Collection<T>) value));
		}
		@NotNull
		public <T> Value<T[]> arrayOf(@NotNull Class<T> itemClass) {
			final List<T> list = listOf(itemClass).orNull();
			return new Value<T[]>(key, list == null ? null : list.toArray((T[]) Array.newInstance(itemClass, 0)));
		}

		@NotNull
		public <K, V> Value<Map<K, V>> mapOfBasics() {
			return (Value<Map<K, V>>) (Value) cast(Map.class);
		}

		@NotNull
		public <K extends Enum<K>, V> Value<EnumMap<K, V>> enumMapOfBasics(@NotNull Class<K> keyClass) {
			final Map<Object, Object> map = mapOfBasics().orNull();
			final EnumMap<K, V> m;
			if (map == null) {
				m = null;
			} else {
				m = new EnumMap<K, V>(keyClass);
				for (Map.Entry<Object, Object> e : map.entrySet()) {
					// spatna kompatibilita - ak su pridane nove enum konstanty a my ich este nepozname, ignoruj ich.
					final int ordinal = ((Byte) e.getKey());
					if (isValidOrdinal(keyClass, ordinal)) {
						final K key = keyClass.getEnumConstants()[ordinal];
						final V value = (V) e.getValue();
						m.put(key, value);
					}
				}
			}
			return new Value<EnumMap<K, V>>(key, m);
		}

		@NotNull
		public Value<UUID> uuid() {
			return cast(UUID.class);
		}

		@Override
		@NotNull
		public String toString() {
			return key + "=" + value;
		}

		@Override
		public boolean equals(Object o) {
			if (this == o) return true;
			if (o == null || getClass() != o.getClass()) return false;
			Value value1 = (Value) o;
			if (value != null ? !value.equals(value1.value) : value1.value != null) return false;
			return true;
		}

		@Override
		public int hashCode() {
			return value != null ? value.hashCode() : 0;
		}
	}

	public abstract boolean contains(@NotNull String key);

	public abstract int size();

	protected static <E extends Enum<E>> boolean isValidOrdinal(@NotNull final Class<E> clazz, int ordinal) {
		return clazz.getEnumConstants().length > ordinal;
	}

	protected static class BoxedBox<T extends Boxable> implements Writable {
		/**
		 * A box, will be null if {@link #boxable} is not null.
		 * <p></p>
		 * Ide o to, nedrzat zbytocne referencie na boxy, aby serializacia nezrala vela pamate.
		 * Boxy sa vytvaraju az ked sa ide serializovat, a hned po ich serializacii su zahodene a GC ich moze
		 * reclaimnut.
		 * <p></p>
		 * Box nie je hned deserializovany, lebo nevieme class.
		 */
		@Nullable
		private final Box box;
		@Nullable
		private final T boxable;
		@Nullable
		public static <T extends Boxable> BoxedBox<T> of(@Nullable T boxable) {
			if (boxable == null) {
				return null;
			}
			return new BoxedBox<T>(null, boxable);
		}

		private Box getBox() {
			if (box != null) {
				return box;
			}
			assert boxable != null;
			return boxable.box();
		}

		private BoxedBox(@Nullable Box box, @Nullable T boxable) {
			if (box == null && boxable == null) {
				throw new IllegalArgumentException("Parameter box: invalid value: both box and boxables is null");
			}
			this.box = box;
			this.boxable = box == null ? boxable : null;
		}

		@Override
		public String toString() {
			return getBox().toString();
		}

		@Override
		public void writeTo(@NotNull DataOutput out) throws IOException {
			getBox().writeTo(out);
		}

		public static <T extends Boxable> BoxedBox<T> readFrom(DataInput in) throws IOException {
			return new BoxedBox<T>(Box.readFrom(in), null);
		}

		@Nullable
		public <T extends Boxable> T unbox(@NotNull Class<T> clazz) {
			if (box == null) {
				return (T) boxable;
			}
			return Box.unbox(clazz, box);
		}

		@Override
		public boolean equals(@Nullable Object o) {
			if (this == o) return true;
			if (o == null || getClass() != o.getClass()) return false;
			BoxedBox<?> that = (BoxedBox<?>) o;
			if (!getBox().equals(that.getBox())) return false;
			return true;
		}

		@Override
		public int hashCode() {
			return getBox().hashCode();
		}
	}

	protected static class BoxedListOfBoxes<T extends Boxable> implements Writable {
		/**
		 * A list of boxes, will be null if {@link #boxables} is not null.
		 * <p></p>
		 * Ide o to, nedrzat zbytocne referencie na boxy, aby serializacia nezrala vela pamate.
		 * Boxy sa vytvaraju az ked sa ide serializovat, a hned po ich serializacii su zahodene a GC ich moze
		 * reclaimnut.
		 */
		@Nullable
		private final ArrayList<Box> boxes;
		@Nullable
		private final List<T> boxables;
		@Nullable
		public static <T extends Boxable> BoxedListOfBoxes<T> of(@Nullable Collection<T> boxables) {
			if (boxables == null) {
				return null;
			}
			return new BoxedListOfBoxes<T>(null, boxables);
		}

		@NotNull
		private ArrayList<Box> getBoxes() {
			if (boxes != null) {
				return boxes;
			}
			assert boxables != null;
			final ArrayList<Box> boxes = new ArrayList<Box>(boxables.size());
			for (Boxable boxable : boxables) {
				boxes.add(boxable.box());
			}
			return boxes;
		}

		private BoxedListOfBoxes(@Nullable List<Box> boxes, @Nullable Collection<T> boxables) {
			if (boxes == null && boxables == null) {
				throw new IllegalArgumentException("Parameter boxes: invalid value: both boxes and boxables is null");
			}
			this.boxes = boxes == null ? null : new ArrayList<Box>(boxes);
			this.boxables = boxes == null ? new ArrayList<T>(boxables) : null;
		}

		@Override
		@NotNull
		public String toString() {
			return getBoxes().toString();
		}

		private int getSize() {
			if (boxes == null) {
				assert boxables != null;
				return boxables.size();
			}
			return boxes.size();
		}

		@Override
		public void writeTo(@NotNull DataOutput out) throws IOException {
			out.writeInt(getSize());
			if (boxes != null) {
				for (Box box : boxes) {
					box.writeTo(out);
				}
			} else {
				assert boxables != null;
				for (Boxable box : boxables) {
					box.box().writeTo(out);
				}
			}
		}

		@NotNull
		public static <T extends Boxable> BoxedListOfBoxes<T> readFrom(@NotNull DataInput in) throws IOException {
			final int size = in.readInt();
			final List<Box> boxes = new ArrayList<Box>(size);
			for (int i = 0; i < size; i++) {
				boxes.add(Box.readFrom(in));
			}
			return new BoxedListOfBoxes<T>(boxes, null);
		}

		@NotNull
		public <V extends Boxable> List<V> unbox(@NotNull Class<V> clazz) {
			if (boxes == null) {
				assert boxables != null;
				//noinspection rawtypes
				return (List) boxables;
			}
			final List<V> list = new ArrayList<V>();
			for (Box box : boxes) {
				list.add(Boxables.read(clazz, box));
			}
			return list;
		}

		@Override
		public boolean equals(Object o) {
			if (this == o) return true;
			if (o == null || getClass() != o.getClass()) return false;
			BoxedListOfBoxes<?> that = (BoxedListOfBoxes<?>) o;
			if (!getBoxes().equals(that.getBoxes())) return false;
			return true;
		}

		@Override
		public int hashCode() {
			return getBoxes().hashCode();
		}
	}

	/**
	 * Stores object under given key. The object must be of supported type otherwise an exception is raised.
	 * @param key the key, not null.
	 * @param value the value of supported type, may be null. Lists are not supported: use either {@link #putListOfStrings(String, Collection)}
	 * or {@link #putListOfBoxables(String, Collection)}.
	 * @return this
	 */
	@NotNull
	public T putSupportedObject(@NotNull String key, @Nullable Object value) {
		if (!isSupportedValue(value)) {
			throw new IllegalArgumentException("Parameter value: invalid value " + value.getClass() + ": this value type is not supported");
		}
		return put(key, convert(value));
	}

	private static Object convert(@Nullable Object value) {
		if (!isSupportedValue(value)) {
			throw new IllegalArgumentException("Parameter value: invalid value " + value.getClass() + ": this value type is not supported");
		}
		if (value instanceof EnumSet) {
			return BoxedEnumSet.of((EnumSet<?>) value);
		} else if (value instanceof Boxable) {
			return BoxedBox.of((Boxable) value);
		} else if (value instanceof byte[]) {
			return BoxedByteArray.of((byte[]) value);
		} else if (value instanceof BitSet) {
			return BoxedByteArray.of(((BitSet) value).toByteArray());
		} else if (value instanceof Collection) {
			final Collection<?> c = (Collection<?>) value;
			final Object item = c.isEmpty() ? null : c.iterator().next();
			if (item instanceof Boxable) {
				return BoxedListOfBoxes.of((Collection<? extends Boxable>) c);
			} else if (item instanceof String) {
				// list of strings is packed as ... a list of strings ;)
				return value;
			} else {
				throw new IllegalArgumentException("Parameter value: invalid value " + value + ": must be either a list of strings or a list of boxables");
			}
		} else {
			return value;
		}
	}

	/**
	 * Checks if given value is of supported type. Box supports the following values:
	 * <ul>
	 *     <li>null</li>
	 *     <li>String</li>
	 *     <li>Primitive values</li>
	 *     <li>EnumSet</li>
	 *     <li>List of Strings</li>
	 *     <li>byte[]</li>
	 *     <li>List of {@link Boxable}s</li>
	 *     <li>{@link Boxable}</li>
	 * </ul>
	 * @param value the value to check. null is supported.
	 * @return true if given value is supported, false if not.
	 */
	public static boolean isSupportedValue(@Nullable Object value) {
		if (value instanceof EnumSet) {
			return true;
		} else if (value instanceof Boxable) {
			return true;
		} else if (value instanceof byte[] || value instanceof BitSet) {
			return true;
		}
		return Box.getTypeNull(value) != Box.VAL_INVALID_TYPE;
	}

	/**
	 * @deprecated Use {@link #putListOfBasics(String, Collection)}.
	 */
	@Deprecated
	@NotNull
	public T putListOfStrings(@NotNull String key, @Nullable Collection<String> strings) {
		// nepresypaj zo Set do List, pri serializacii je to jedno a usetri sa CPU
		// https://code.google.com/p/aedict/issues/detail?id=426
		return putListOfBasics(key, strings);
	}

	/**
	 * Inserts a list of basic objects, that is, anything besides Box.
	 * @param key the key
	 * @param basics a list of basic objects, may be null, may contain nulls.
	 * @return this
	 */
	@NotNull
	public T putListOfBasics(@NotNull String key, @Nullable Collection<?> basics) {
		// nepresypaj zo Set do List, pri serializacii je to jedno a usetri sa CPU
		// https://code.google.com/p/aedict/issues/detail?id=426
		return put(key, basics);
	}

	@NotNull
	public T putMapOfBasics(@NotNull String key, @Nullable Map<?, ?> basics) {
		if (basics instanceof EnumMap) {
			return (T) putEnumMapOfBasics(key, (EnumMap) basics);
		}
		return put(key, basics);
	}

	@NotNull
	public T putBoxable(@NotNull String key, @Nullable Boxable boxable) {
		return put(key, BoxedBox.of(boxable));
	}

	@NotNull
	public T putByteArray(String key, byte[] obj) {
		return put(key, BoxedByteArray.of(obj));
	}

	@NotNull
	public T putBooleanArray(String key, boolean[] obj) {
		if (obj == null) {
			return remove(key);
		}
		if (obj.length > 0xFFFF) {
			throw new IllegalArgumentException("Parameter obj: invalid value " + obj.length + ": must be 0xFFFF or less");
		}
		final BitSet bs = new BitSet();
		for (int i = 0; i < obj.length; i++) {
			if (obj[i]) {
				bs.set(i);
			}
		}
		final byte[] bsba = bs.toByteArray();
		final byte[] value = new byte[bsba.length + 2];
		value[0] = (byte) (obj.length & 0xFF);
		value[1] = (byte) ((obj.length / 0x100) & 0xFF);
		System.arraycopy(bsba, 0, value, 2, bsba.length);
		return putByteArray(key, value);
	}

	@NotNull
	public T putLong(String key, Long value) {
		return put(key, value);
	}

	@NotNull
	public T putListOfBoxables(@NotNull String key, @Nullable Collection<? extends Boxable> boxables) {
		return put(key, BoxedListOfBoxes.of(boxables));
	}

	/**
	 * Pozor enum konstanty ulozene takto nikdy nesmu byt preusporiadane! Pretoze je to ukladane podla ordinal, nie podla mena konstanty.
	 * @param key
	 * @param basics
	 * @param <E>
	 * @return
	 */
	@NotNull
	public <E extends Enum<E>> T putEnumMapOfBasics(@NotNull String key, @Nullable EnumMap<E, ?> basics) {
		final Map<Byte, Object> b = new HashMap<Byte, Object>(basics.size());
		for (Map.Entry<E, ?> e : basics.entrySet()) {
			final int ordinal = e.getKey().ordinal();
			if (ordinal > Box.MAX_ENUM_LENGTH) {
				throw new IllegalArgumentException("Parameter basics: invalid value " + basics + ": contains enums with ordinal larger than " + Box.MAX_ENUM_LENGTH);
			}
			b.put((byte) ordinal, e.getValue());
		}
		return put(key, b);
	}
}
