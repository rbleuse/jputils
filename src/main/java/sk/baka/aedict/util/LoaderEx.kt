/*
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.util

import android.app.Application
import android.os.Handler
import android.os.Looper
import org.slf4j.LoggerFactory
import sk.baka.aedict.util.LoaderEx.Companion.createExecutor
import java.io.Closeable
import java.io.IOException
import java.io.Serializable
import java.util.*
import java.util.concurrent.CancellationException
import java.util.concurrent.Executor
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.atomic.AtomicInteger

interface UIHandler : Executor {
    /**
     * Fails if not called from the UI thread.
     */
    fun checkUIThread()
}

/**
 * My own simple replacement for horribly broken and overcomplicated Android loaders. Simply instantiate one for each activity,
 * and destroy it when the activity is closed (in `Activity#onDestroy()`).
 *
 * An Android [android.app.Application] should simply create one executor for loaders: simply call [createExecutor]
 * in your app's [Application.onCreate] and destroy it in [Application.onTerminate]:
 * ```
 * loader.shutdownNow();
 * loader.awaitTermination(5, TimeUnit.SECONDS);
 * ```
 *
 * @author mvy
 * @property executor executes loader tasks in background. An Android [android.app.Application] should create one executor. See above.
 * @property interruptOnCancel if true, the thread executing the loader is interrupted when canceled.
 * @property handler runs things in the UI thread.
 */
class LoaderEx(private val executor: Executor, private val interruptOnCancel: Boolean, private val handler: UIHandler) : Closeable {

    /**
     * Current (last added) loader. Updated only from UI thread.
     */
    private var current: Loader<*, *>? = null

    /**
     * Implementors should implement this as a static inner class, to avoid invisible references to Fragments, which may create memory leaks.
     * Implementor instances will be passed to the Loader in a Bundle, therefore implementors must be serializable.
     *
     * Implementors SHOULD implement [Object.toString] to provide debug information when the loader crashes.
     * @param I the item type
     * @param T the type of tags attached to the item type
     */
    interface LoaderImpl<I, T> : Serializable {
        /**
         * The loader should periodically monitor the value of `canceled` and should return null or throwing [CancellationException] (NOT [android.os.OperationCanceledException] - this exception was introduced in API 16) ASAP.
         * @param canceled if true, the task has been cancelled.
         * @return computed list, never null.
         * @throws CancellationException may be thrown when canceled instead of returning null.
         * @throws Exception
         */
        @Throws(CancellationException::class, Throwable::class)
        fun loadItemsInBackground(canceled: AtomicBoolean): LoadableItems<I, T>
    }

    /**
     * Represents a loader result. A list of items, each item may optionally have an arbitrary tag value associated.
     * @param <I> the item type in [items]
     * @param <T> the type of tags attached to the item type
     * @param items the items, not null.
     * @param tags optional tags for items, may be empty. Kluc nesmie byt I, co ak v items je viackrat rovnaky prvok, ale mapovany na inu value?
     * To potom robi zlobu:
     * https://github.com/mvysny/aedict/issues/497#issuecomment-112782714
     * Evidentne viackrat je tam rovnaky EntryRef, ale ma rozne EntryInfo; v mape sa to vsak strati.
     */
    class LoadableItems<I, T>(val items: List<I> = ArrayList(), val tags: Map<Int, T> = emptyMap()) {

        /**
         * Returns tag for given item.
         * @param itemIndex the item index from [items].
         * @return the tag or null if the tag is not present or [tags] is null.
         */
        fun getTag(itemIndex: Int): T? {
            items[itemIndex] // check bounds
            return tags[itemIndex]
        }

        override fun toString() = "LoadableItems{items=${items.size}, tags=${tags.size}}"
    }

    /**
     * Creates instance with no items.
     */
    private class Loader<I, T>(private val loader: LoaderImpl<I, T>, private val callback: OnResultCallback<I, T>,
                               private val interruptOnCancel: Boolean, private val handler: UIHandler) : Runnable {
        private val id = idgen.incrementAndGet()
        private val t = Timing("scheduled")
        @Volatile private var thread: Thread? = null
        /**
         * Drzi stack, kde bol tento loader zaradeny.
         */
        private val callSite = RuntimeException("Scheduled stack trace: $loader")

        private val cancelationDurationMs: Long
            get() = if (canceledAt == null) -1 else System.currentTimeMillis() - canceledAt!!

        private val canceled = AtomicBoolean()
        @Volatile private var canceledAt: Long? = null

        override fun run() {
            thread = Thread.currentThread()
            try {
                t.mark("running")
                var result: LoadableItems<I, T>? = if (canceled.get()) null else loader.loadItemsInBackground(canceled)
                val canceled = this.canceled.get()
                if (canceled) {
                    result = null
                }
                var logText = "$this: returned ${if (result == null) "null" else result.items.size.toString() + " item(s)"} in ${t.finish()}"
                if (canceled) {
                    logText += ", canceled, cancelation took ${cancelationDurationMs}ms"
                }
                log.info(logText)
                onResult(result)
            } catch (t: Throwable) {
                if (canceled.get()) {
                    if (t is CancellationException) {
                        // do nothing - loader is expected to throw CancellationException when canceled.
                        log.info(this.toString() + ": Loader was canceled by CancellationException, cancelation took "
                                + cancelationDurationMs + "ms; " + this.t.finish())
                    } else {
                        log.info("$this: Loader was canceled but threw an exception different than CancellationException, cancelation took ${cancelationDurationMs}ms; ${this.t.finish()}", t)
                    }
                    onResult(null)
                } else {
                    log.error("$this: failed; ${this.t.finish()}", t)
                    handler.execute { onFailure(t) }
                }
            } finally {
                thread = null
            }
        }

        private fun onResult(items: LoadableItems<I, T>?) {
            handler.execute {
                try {
                    callback.onResult(items)
                } catch (t: Throwable) {
                    onFailure(t)
                }
            }
        }

        private fun onFailure(t: Throwable) {
            // neloguj callSite ako Throwable: potom ho totiz Crashlytics zaloguje ako samostatny bug. Ak bude zalogovany ako message,
            // potom sa pripoji k nasledovnemu crashu (ked bude "t" zalogovany), co je presne co chcem.
            log.error("Loader failed with $t, this stack trace holds stack where the task was scheduled: ${callSite.getStackTraceAsString()}")
            // onFailure moze thrownut exception, v tom pripade pustit exception do UI, nech spadne aplikacia.
            handler.checkUIThread()
            callback.onFailure(t)
        }

        fun cancel() {
            canceledAt = System.currentTimeMillis()
            canceled.set(true)
            log.info("$this: canceled")
            if (interruptOnCancel) {
                thread?.interrupt()
            }
        }

        override fun toString() = "Loader #$id $loader${if (canceled.get()) " canceled" else ""}"

        companion object {
            private val idgen = AtomicInteger()
        }
    }

    /**
     * Cancels currently ongoing loader. Does nothing if no loader is currently running. Must be invoked in UI thread.
     *
     * callback's onResult() is called only if the loader is currently running.
     */
    fun cancel() {
        handler.checkUIThread()
        if (current != null) {
            current!!.cancel()
            current = null
        }
    }

    interface OnResultCallback<I, T> {
        /**
         * Receives loader result. Also called when the loader has been canceled.
         * Called in the UI thread.
         * @param result the result, null if canceled.
         */
        fun onResult(result: LoadableItems<I, T>?)

        /**
         * If the loader fails, this is called instead of [.onResult].
         * Called in the UI thread. If this method throws an exception, it is propagated to UI and will crash the application.
         * @param cause the failure cause, not null. Not yet logged. However, the loader schedule site stacktrace is logged.
         */
        fun onFailure(cause: Throwable)
    }

    /**
     * Cancels current loader and starts a new one. Invoke in UI thread. Always ignores result of previous loader and will
     * always run this loader (unless it is canceled later on).
     * @param loader the loader, not null.
     * @param callback called if loader is not canceled and
     * @param I the item type in [LoadableItems.items]
     * @param T the type of tags attached to the item type
     */
    fun <I, T> load(loader: LoaderImpl<I, T>, callback: OnResultCallback<I, T>) {
        cancel()
        current = Loader(loader, callback, interruptOnCancel, handler)
        executor.execute(current!!)
    }

    @Throws(IOException::class)
    override fun close() {
        cancel()
    }

    companion object {

        /**
         * Creates an executor specifically for loaders.
         * @return the executor, not null.
         */
        @JvmStatic
        fun createExecutor(): ExecutorService {
            return Executors.newSingleThreadExecutor { r ->
                val t = Thread(r)
                t.name = "LoaderEx"
                t.isDaemon = true
                t
            }
        }

        private val log = LoggerFactory.getLogger(LoaderEx::class.java)
    }
}

object AndroidUI : UIHandler {
    override fun execute(command: Runnable) {
        handler.post(command)
    }

    val handler = Handler(Looper.getMainLooper())

    override fun checkUIThread() {
        if (!isUIThread) throw IllegalStateException("Invalid state: not invoked in the UI thread");
    }
}
