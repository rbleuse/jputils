/*
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.util;

import java.io.*;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import kotlin.collections.CollectionsKt;
import kotlin.io.ByteStreamsKt;
import kotlin.io.TextStreamsKt;
import kotlin.text.StringsKt;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import sk.baka.aedict.kanji.JpCharKt;
import sk.baka.android.DirUtils;

/**
 * Misc utility methods.
 * @author mvy
 */
@Deprecated
public class MiscUtils {
    /**
     * Checks if given character is an ascii letter (a-z, A-Z).
     *
     * @param c
     *            the character to check
     * @return true if the character is a letter, false otherwise.
     */
    public static boolean isAsciiLetter(char c) {
        return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z');
    }

	/**
	 * Returns true if given string is blank.
	 * @param s the string, may be null.
	 * @return true if given string is null, empty or consists of whitespaces only.
     * @deprecated replace with kotlin code
	 */
	@Deprecated
    public static boolean isBlank(@Nullable CharSequence s) {
        return s == null || s.toString().trim().length() == 0;
    }

    /**
     * Returns length in bytes of given file or directory.
     *
     * @param dir
     *            the directory to list. A directory length is set to be 4kb +
     *            lengths of all its children.
     * @return the directory length, 0 if the directory/file does not exist.
     */
    public static long getLength(@NotNull final File dir) {
        return UtilKt.computeLength(dir);
    }

	/**
	 * Joins given list of items with given separator. {@link Object#toString()} is called on each item.
	 * @param items the list of items, not null
	 * @param separator the separator, not null
	 * @return joined items, never null.
     * @deprecated use kotlin
	 */
	@NotNull
    @Deprecated
    public static String join(@NotNull Iterable<?> items, @NotNull String separator) {
	    return CollectionsKt.joinToString(items, separator, "", "", -1, "...", null);
    }

    @Deprecated
    public static byte[] readFully(InputStream in) {
	    try {
            return ByteStreamsKt.readBytes(in, 8096);
        } finally {
	        UtilKt.closeQuietly(in);
        }
    }

    @Deprecated
    public static String readFully(Reader in) {
        try {
            return TextStreamsKt.readText(in);
        } finally {
            UtilKt.closeQuietly(in);
        }
    }

    /**
     * Vrati vsetky codepoints z daneho stringu.
     * @param str string, not null.
     * @return array of codepoints, never null. Empty if the original string is empty. Moze vratit blank string ak povodny string obsahuje medzeru.
     * Vsetky stringy su o dlzke 1 alebo 2 znaky, a presne 1 codepoint.
     */
	@NotNull
    public static List<String> getCodePoints(@NotNull String str) {
        final List<String> result = new ArrayList<String>(str.length());
        for (int offset = 0;offset < str.length();) {
            int nextOffset = str.offsetByCodePoints(offset, 1);
            final String cp = str.substring(offset, nextOffset);
            result.add(cp);
            offset = nextOffset;
        }
        return result;
    }

    @Deprecated
    public static boolean intersects(@NotNull Set<?> s1, @NotNull Set<?> s2) {
	    return UtilKt.intersects(s1, s2);
    }

    /**
     * Najde najdlhsi retazec, ktorym zacinaju retazce a aj b.
     * @param a retazec a, not null.
     * @param b retazec b, not null.
     * @return common prefix, never null, may be an empty string.
     */
    @Deprecated
	@NotNull
    public static String greatestCommonPrefix(@NotNull String a, @NotNull String b) {
	    return StringsKt.commonPrefixWith(a, b, false);
    }
    /**
     * Najde najdlhsi retazec, ktorym koncia retazce a aj b.
     * @param a retazec a, not null.
     * @param b retazec b, not null.
     * @return common suffix, never null, may be an empty string.
     */
    @Deprecated
	@NotNull
    public static String greatestCommonSuffix(@NotNull String a, @NotNull String b) {
        return StringsKt.commonSuffixWith(a, b, false);
    }

    @Deprecated
    public static boolean equals(@Nullable Object a, @Nullable Object b) {
        return (a == b) || (a != null && a.equals(b));
    }

    /**
     * Checks if given character is a vowel (a i u e o A I U E O)
     * @param c the character
     * @return true if it is a vowel.
     */
    public static boolean isVowel(final char c) {
        return JpCharKt.isAsciiVowel(c);
    }

    public static void checkReadableFile(@NotNull File file) throws IOException {
        if (!file.exists()) {
            throw new IOException("the file does not exist");
        }
        if (!file.isFile()) {
            throw new IOException("not a file");
        }
        if (!file.canRead()) {
            throw new IOException("the OS does not allow me to read this file");
        }
    }

    public static Object enumValueOf(@NotNull Class<?> eclass, @NotNull String name) {
        return Enum.valueOf((Class) eclass, name);
    }

    /**
     * Handy function to get a loggable stack trace from a Throwable. As opposed to Android logging mechanism, it logs even UnknownHostExceptions.
     * @param tr An exception to log
     */
    @NotNull
    public static String getStackTraceString(@NotNull Throwable tr) {
        return UtilKt.getStackTraceAsString(tr);
    }
}
