/*
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.zip.GZIPInputStream;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import sk.baka.aedict.util.typedmap.Box;

/**
 * Utility class for {@link Box} and for objects serialized to byte array via Box.
 * @author mvy
 */
public class Boxables {
	private Boxables() {}

	/**
	 * Reads the box contents from given box. The object type is not auto-detected: it must instead be provided
	 * by the {@code clazz} parameter.
	 *
	 * @param clazz expected class, not null.
	 * @param box the box, not null.
	 * @param <T> expected type
	 * @return the unboxed object instance, never null.
	 */
	@NotNull
	public static <T extends Boxable> T read(@NotNull Class<T> clazz, @NotNull Box box) {
		Check.requireNotNull(clazz, "clazz");
		Check.requireNotNull(box, "box");
		try {
			final Method unbox = clazz.getMethod("unbox", Box.class);
			if (!Modifier.isStatic(unbox.getModifiers())) {
				throw new RuntimeException("The method is not static");
			}
			unbox.setAccessible(true);
			return clazz.cast(unbox.invoke(null, box));
		} catch (Exception ex) {
			throw new RuntimeException("Failed to invoke " + clazz.getName() + ".unbox(Box)", ex);
		}
	}

	/**
	 * Reads the box contents from given box. The object type is not auto-detected: it must instead be provided
	 * by the {@code clazz} parameter.
	 *
	 * @param clazz expected class, not null.
	 * @param file the file with the box, not null.
	 * @param <T> expected type
	 * @return the unboxed object instance, never null.
	 */
	@NotNull
	public static <T extends Boxable> T readGZipped(@NotNull Class<T> clazz, @NotNull File file) throws IOException {
		final InputStream in = new BufferedInputStream(new GZIPInputStream(new FileInputStream(file)));
		try {
			return read(clazz, in);
		} finally {
			UtilKt.closeQuietly(in);
		}
	}

	@Nullable
	public static <T extends Boxable> T clone(@Nullable T boxable) {
		return boxable == null ? null : (T) read(boxable.getClass(), boxable.box());
	}

	@Nullable
	public static <T extends Boxable> T testclone(@Nullable T boxable) {
		return boxable == null ? null : (T) read(boxable.getClass(), Writables.clone(boxable.box()));
	}

	/**
	 * Reads the boxed class contents from given box. The object type is not auto-detected: it must instead be provided
	 * by the {@code clazz} parameter.
	 *
	 * @param clazz expected class, not null.
	 * @param bytes the bytes produced by {@link #write(Boxable)}, not null.
	 * @param <T> expected type
	 * @return the unboxed object instance, never null.
	 */
	@NotNull
	public static <T extends Boxable> T read(@NotNull Class<T> clazz, @NotNull byte[] bytes) throws IOException {
		return read(clazz, new ByteArrayInputStream(bytes));
	}

	/**
	 * Reads the boxed class contents from given box. The object type is not auto-detected: it must instead be provided
	 * by the {@code clazz} parameter.
	 *
	 * @param clazz expected class, not null.
	 * @param in the input stream, not null. NOT CLOSED.
	 * @param <T> expected type
	 * @return the unboxed object instance, never null.
	 */
	@NotNull
	public static <T extends Boxable> T read(@NotNull Class<T> clazz, @NotNull InputStream in) throws IOException {
		return read(clazz, Box.readFrom(new DataInputStream(in)));
	}

	/**
	 * Reads the box contents from given box. The object type is not auto-detected: it must instead be provided
	 * by the {@code clazz} parameter.
	 *
	 * @param clazz expected class, not null.
	 * @param file the file with the box, not null.
	 * @param <T> expected type
	 * @return the unboxed object instance, never null.
	 */
	@NotNull
	public static <T extends Boxable> T read(@NotNull Class<T> clazz, @NotNull File file) throws IOException {
		final InputStream in = new BufferedInputStream(new FileInputStream(file));
		try {
			return read(clazz, in);
		} finally {
			UtilKt.closeQuietly(in);
		}
	}

	/**
	 * Writes this boxable directly to a file. A temp file is first created; after the write is successful,
	 * the file is moved over the requested file. This is to protect the original file contents, in case of a write error.
	 * @param file the file to write to, not null
	 * @throws IOException on i/o error.
	 */
	public static void writeGzipped(@NotNull Boxable boxable, @NotNull File file) throws IOException {
		Writables.write(boxable.box(), file, true);
	}

	/**
	 * Serializes the boxable and returns the result as a byte array.
	 * @param boxable the boxable object, not null.
	 * @return serialized box, never null.
	 */
	@NotNull
	public static byte[] write(@NotNull Boxable boxable) {
		return Writables.write(boxable.box());
	}

	/**
	 * Writes this boxable directly to a file. A temp file is first created; after the write is successful,
	 * the file is moved over the requested file. This is to protect the original file contents, in case of a write error.
	 * @param file the file to write to, not null
	 * @throws IOException on i/o error.
	 */
	public static void write(@NotNull Boxable boxable, @NotNull File file) throws IOException {
		Writables.write(boxable.box(), file, false);
	}

	/**
	 * Writes this boxable to an output stream. Automatically wraps the output stream as a buffered output stream.
	 * @param out the output stream write to, not null. NOT CLOSED. Flushed automatically.
	 * @throws IOException on i/o error.
	 */
	public static void write(@NotNull Boxable boxable, @NotNull OutputStream out) throws IOException {
		if (!(out instanceof BufferedOutputStream)) {
			out = new BufferedOutputStream(out);
		}
		final DataOutputStream dout = new DataOutputStream(out);
		boxable.box().writeTo(dout);
		dout.flush();
	}
}
