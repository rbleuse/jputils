/*
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.util

import java.io.*

/**
 * A stable serialization. Serializable/Parcelable format may change, this must not.
 *
 * Low-level API: use [Boxable] for a higher-level serialization API.
 *
 * Anyone implementing this interface must have a static method `readFrom(DataInput in)`.
 */
interface Writable {
    /**
     * Writes the object to given output.
     *
     * Poznamka: vsetky buduce verzie triedy by mali byt schopne citat predosle verzie tried.
     * Ja to robim tak, ze prvy byte je verzia, a parser sa podla toho zachova: novsie verzie rejectne s exception, ale starsie verzie precita.
     * @param out write the object here. Not null. Already buffered.
     * @throws IOException if object write fails with I/O error.
     */
    @Throws(IOException::class)
    fun writeTo(out: DataOutput)
}

/**
 * Writes this writable directly to a file. A temp file is first created; after the write is successful,
 * the file is moved over the requested file. This is to protect the original file contents, in case of a write error.
 * @param writable the writable, not null.
 * @param target the file to write to, not null
 * @param gzip if true, the stream is gzipped.
 */
fun Writable.writeToFile(target: File, gzip: Boolean = false) = Writables.write(this, target, gzip)

/**
 * Writes this writable to given output stream.
 * @param stream output stream, NOT CLOSED, flushed. Automatically wrapped in {@link BufferedOutputStream}
 */
fun Writable.writeToStream(stream: OutputStream): Unit = DataOutputStream(stream.buffered()).let { writeTo(it); it.flush() }

/**
 * Writes this writable to a byte array and returns that very byte array.
 * @return the byte representation.
 */
fun Writable.writeToBytes() = ByteArrayOutputStream().use { writeToStream(it); it }.toByteArray()

/**
 * Implemented by [Writable]'s companion object. Do not forget to annotate [Readable.readFrom] with [JvmStatic] so that it is backward-compatible!
 */
interface Readable<out T: Writable> {
    /**
     * Reads the [Writable] back.
     */
    fun readFrom(di: DataInput): T

    /**
     * Reads the [Writable] from given stream.
     * @param stream the stream to read from. Always closed. Automatically wrapped as buffered
     */
    fun readFromStream(stream: InputStream): T = stream.use { readFrom(DataInputStream(it.buffered())) }

    /**
     * Reads the [Writable] from given file.
     * @param file the file to read the writable from.
     */
    fun readFromFile(file: File): T = readFromStream(file.inputStream())

    /**
     * Reads the [Writable] from given byte array.
     * @param bytes read from here.
     */
    fun readFromBytes(bytes: ByteArray): T = readFromStream(bytes.inputStream())
}

/**
 * Clones this writable, simply by writing it to a byte array and then reading it back.
 * @return the clone
 */
inline fun <reified T: Writable> T.cloneByWrite(): T = Writables.read(T::class.java, writeToBytes());
