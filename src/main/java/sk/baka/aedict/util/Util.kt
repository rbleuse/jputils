/*
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.util

import org.slf4j.LoggerFactory
import sk.baka.aedict.kanji.JpCharacter
import sk.baka.aedict.kanji.isJpChar
import java.io.*
import java.text.BreakIterator
import java.util.*
import kotlin.ranges.IntRange

/**
 * Returns a [Set] of all elements.
 */
fun <T> Iterable<T>.toLinkedSet(): LinkedHashSet<T> {
    val set = toSet()
    return if (set is LinkedHashSet) set else LinkedHashSet<T>(set)
}

/**
 * Iterates over codepoints present in given string.
 * @property string the string to iterate over
 */
private class CodePointIterator(val string: String): Iterator<Int> {

    private var index = 0;

    override fun hasNext() = index < string.length

    override fun next(): Int {
        if (!hasNext()) throw NoSuchElementException()
        val result = string.codePointAt(index)
        index += if (string.hasSurrogatePairAt(index)) 2 else 1
        return result
    }
}

/**
 * Iterates over code points of this string.
 */
val String.codePoints: Iterable<Int>
    get() = Iterable { CodePointIterator(this) }

/**
 * Splits [receiver] into words, no spaces. Doesn't return punctuation unless [punctuation] is set to true.
 * Any japanese character (kanji/hiragana/katakana etc) is considered a single word.
 * @param text the text, not null.
 * @param punctuation defaults to false. ak false, no punctuation is returned.
 * @return a list of words, never null, may be empty.
 */
fun String.splitToWords(punctuation: Boolean = false): LinkedList<String> {
    var text: String = this
    val bi: BreakIterator = BreakIterator.getWordInstance()
    // interestingly this iterator splits the sentence 私は彼女をかわいらしいと思った。 as follows:
    // [私, は, 彼女, をかわいらしいと, 思, った, 。]
    // however we want the jp chars to be split apart.
    bi.setText(text)
    val result = LinkedList<String>()
    while (true) {
        val current = bi.current()
        if (current < text.length) {
            val cp = text.codePointAt(current)
            if (cp.isJpChar) {
                // hack so that we split jp characters apart
                val word = String(intArrayOf(cp), 0, 1)
                result.add(word)
                text = text.substring(current + word.length)
                bi.setText(text)
                continue
            }
        }
        val next: Int = bi.next()
        if (next == BreakIterator.DONE) {
            break
        }
        val word: String = text.substring(current, next).trim()
        if (word.isEmpty()) {
            continue
        }
        val c: Int = word.codePointAt(0)
        if (punctuation || c.isAlphabetic() || Character.isDigit(c)) {
            result.add(word)
        }
    }
    return result
}

/**
 * Determines if the specified character (Unicode code point) is an alphabet.
 * <p>
 * A character is considered to be alphabetic if its general category type,
 * provided by {@link Character#getType(int) getType(codePoint)}, is any of
 * the following:
 * <ul>
 * <li> <code>UPPERCASE_LETTER</code>
 * <li> <code>LOWERCASE_LETTER</code>
 * <li> <code>TITLECASE_LETTER</code>
 * <li> <code>MODIFIER_LETTER</code>
 * <li> <code>OTHER_LETTER</code>
 * <li> <code>LETTER_NUMBER</code>
 * </ul>
 * or it has contributory property Other_Alphabetic as defined by the
 * Unicode Standard.
 *
 * @param   codePoint the character (Unicode code point) to be tested.
 * @return  <code>true</code> if the character is a Unicode alphabet
 *          character, <code>false</code> otherwise.
 * @since   1.7
 */
fun Int.isAlphabetic(): Boolean {
    // do not use Character.isAlphabetic(c) - only present since Android API 19
    val type = Character.getType(this)
    // triedy su zobrate z javadocu k Character.isAlphabetic
    return type == Character.UPPERCASE_LETTER.toInt() ||
            type == Character.LOWERCASE_LETTER.toInt() ||
            type == Character.TITLECASE_LETTER.toInt() ||
            type == Character.MODIFIER_LETTER.toInt() ||
            type == Character.OTHER_LETTER.toInt() ||
            type == Character.LETTER_NUMBER.toInt()
}

fun Iterable<String>.filterNotBlank(): List<String> = filter { it.isNotBlank() }

/**
 * https://youtrack.jetbrains.com/issue/KT-11669
 */
private val REGEX_WHITESPACES = "[\\p{javaWhitespace}\\p{javaSpaceChar}\u2000-\u200f]+".toRegex()

/**
 * Splits words by whitespaces. Removes all blank words. Splits also by the NBSP 160 char.
 */
fun CharSequence.splitByWhitespaces() = REGEX_WHITESPACES.split(this).filterNotBlank()

/**
 * Checks whether this range fully contains the [other] range.
 */
operator fun IntRange.contains(other: IntRange) = other.isEmpty() || (start <= other.start && endInclusive >= other.endInclusive);

fun IntRange.intersection(other: IntRange): IntRange {
    if (this.start > other.endInclusive || this.endInclusive < other.start) {
        return IntRange.EMPTY
    }
    if (contains(other)) {
        return other
    }
    if (other.contains(this)) {
        return this
    }
    val s = this.start.coerceAtLeast(other.start)
    val e = this.endInclusive.coerceAtMost(other.endInclusive)
    assert(s <= e)
    return s..e
}

/**
 * Processes given set of ranges, removing all empty ranges and joining adjacent/overlapping ranges. SORTS this list.
 * @return a set of ranges with no empty ranges and no overlapping/adjacent ranges. The set keeps the ranges sorted, from lowest to highest.
 */
fun MutableList<IntRange>.simplifyRanges(): Set<IntRange> {
    if (isEmpty()) return emptySet()
    sortWith(Comparator<kotlin.ranges.IntRange> { o1, o2 ->
        var result = o1!!.start.compareTo(o2!!.start)
        if (result != 0) {
            result = o1.endInclusive.compareTo(o2.endInclusive)
        }
        result
    })
    val result = LinkedHashSet<IntRange>(size)
    var last: IntRange? = null
    for (range in this) {
        if (range.isEmpty()) {
            continue
        }
        var simplifiedRange = range
        if (last != null) {
            if (last.contains(simplifiedRange)) {
                continue
            }
            if (!last.intersection(simplifiedRange).isEmpty() || last.endInclusive + 1 == simplifiedRange.start) {
                result.remove(last)
                simplifiedRange = last.start..simplifiedRange.endInclusive
            }
        }
        result.add(simplifiedRange)
        last = simplifiedRange
    }
    return result
}

/**
 * Closes this quietly - any exceptions are only logged with INFO level and not propagated further.
 */
fun Closeable.closeQuietly() {
    try {
        close()
    } catch (e: Exception) {
        LoggerFactory.getLogger(javaClass).info("Failed to close $this", e)
    }
}

@Throws(IOException::class)
fun InputStream.readFully(target: ByteArray) {
    var read = 0
    while (read < target.size) {
        val actuallyRead = read(target, read, target.size - read)
        if (actuallyRead < 0) throw EOFException()
        read += actuallyRead
    }
}

@Throws(IOException::class)
fun InputStream.readFully(bytes: Int): ByteArray {
    val result = ByteArray(bytes)
    readFully(result)
    return result
}

/**
 * Discards `n` bytes of data from the input stream. This method
 * will block until the full amount has been skipped. Does not close the
 * stream.
 * @param n the number of bytes to skip
 * @throws EOFException if this stream reaches the end before skipping all the bytes
 * @throws IOException if an I/O error occurs, or the stream does not support skipping
 */
@Suppress("NAME_SHADOWING")
@Throws(IOException::class)
fun InputStream.skipFully(n: Long) {
    require (n >= 0) { "n must be 0 or greater: $n" }
    var n = n
    val toSkip = n
    while (n > 0) {
        val amt = skip(n)
        if (amt == 0L) {
            // Force a blocking read to avoid infinite loop
            if (read() == -1) {
                val skipped = toSkip - n
                throw EOFException("reached end of stream after skipping $skipped bytes; $toSkip bytes expected")
            }
            n--
        } else {
            n -= amt
        }
    }
}

/**
 * Ellipsizes this string: truncates and appends `...` so that the resulting string has maximum length of [maxLength].
 */
fun CharSequence.ellipsize(maxLength: Int): CharSequence = if (length <= maxLength) this else "${subSequence(0, Math.max(1, maxLength - 3))}..."

// musime mat CharSequence.ellipsize aj String.ellipsize pretoze niektory kod ocakava ze to vrati String...

/**
 * Ellipsizes this string: truncates and appends `...` so that the resulting string has maximum length of [maxLength].
 */
fun String.ellipsize(maxLength: Int): String = if (length <= maxLength) this else "${substring(0, Math.max(1, maxLength - 3))}..."

/**
 * Reads/skips the input stream to the end and returns the number of bytes which would be provided by the input stream. Does not close the input stream.
 * @return the number of bytes left in the input stream
 */
fun InputStream.computeSize(): Long {
    var skipped = 0L
    while (true) {
        val skippedNow = skip(10 * 1024 * 1024)
        skipped += skippedNow
        if (skippedNow == 0L) {
            // Force a blocking read to avoid infinite loop
            if (read() == -1) {
                return skipped
            } else {
                skipped++
            }
        }
    }
}

/**
 * Returns true if the two sets share at least one common item.
 */
fun Set<*>.intersects(other: Set<*>): Boolean {
    val (smaller, larger) = if (size < other.size) this to other else other to this
    if (smaller.isEmpty()) {
        return false
    }
    // new HashSet(smaller).retainAll(larger) simply walks over collection. we can do the same, without the overhead of creating new HashSet
    return smaller.any { larger.contains(it) }
}

/**
 * Returns the sum of all values produced by [selector] function applied to each element in the collection.
 */
inline fun <T> Iterable<T>.sumByLong(selector: (T) -> Long): Long {
    var sum: Long = 0
    for (element in this) {
        sum += selector(element)
    }
    return sum
}

/**
 * Returns length in bytes of given file or directory.
 * @receiver dir the directory to list. A directory length is set to be 4kb + lengths of all its children.
 * @return the directory length, 0 if the directory/file does not exist.
 */
fun File.computeLength(): Long = when {
    !exists() -> 0
    isFile -> length()
    isDirectory -> {
        val files = listFiles() ?: arrayOf()
        4096 + files.sumByLong { it.computeLength() }
    }
    else -> 0
}

/**
 * Returns the sum of all values produced by [selector] function applied to each element in the array.
 */
inline fun <T> Array<out T>.sumByLong(selector: (T) -> Long): Long {
    var sum: Long = 0
    for (element in this) {
        sum += selector(element)
    }
    return sum
}

/**
 * Handy function to get a loggable stack trace from a Throwable. As opposed to Android logging mechanism, it logs even UnknownHostExceptions.
 * @param tr An exception to log
 */
fun Throwable.getStackTraceAsString(): String {
    val sw = StringWriter()
    val pw = PrintWriter(sw)
    printStackTrace(pw)
    pw.flush()
    return sw.toString()
}
