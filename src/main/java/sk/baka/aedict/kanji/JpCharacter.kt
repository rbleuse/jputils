/**
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.kanji

import sk.baka.aedict.util.Readable
import sk.baka.aedict.util.Writable

import java.io.DataInput
import java.io.DataOutput
import java.io.IOException
import java.io.Serializable
import java.util.HashSet

/**
 * Contains a single japanese character: a hiragana or a katakana or a kanji.
 * Some kanjis need 2 characters in UTF-16 encoding.
 * @author mvy
 * @property c The jp character, not null, not blank. Contains exactly single character. May be kanji, hiragana, katakana,
 * half-width katakana. Never a brace or other stuff. May be of length 1 or 2 (for
 * kanjis with high codepoint number which cannot be represented as one char in UTF-16 encoding).
 */
data class JpCharacter(@JvmField val c: String) : Serializable, Writable, Comparable<JpCharacter> {
    // needs to be 0 since deserialization will store default value here (0).
    @Transient private var codePointInt = 0

    /**
     * Returns base36-encoded unicode code of given part. Fails if given character is not a part.
     * @return part base36 encoded unicode code, e.g. ef5
     */
    val base36EncodedCodePoint: String
        get() = Integer.toString(codePoint, Character.MAX_RADIX)

    /**
     * Constructs object for given character. Fails if given character is not a kanji.
     * @param c the kanji character.
     */
    constructor(c: Char) : this(c.toString())

    /**
     * Returns the kanji code point.
     * @return kanji code point.
     */
    val codePoint: Int get() {
        if (codePointInt <= 0) {
            codePointInt = c.codePointAt(0)
        }
        return codePointInt
    }

    init {
        require(c.isSingleJpChar) { "Parameter kanji: invalid value $c: not a single kanji, hiragana, katakana or halfwidth katakana character" }
    }

    val isKanji: Boolean
        get() = c.isSingleKanji

    val isHiragana: Boolean
        get() = c[0].isHiragana

    /**
     * True if this character is a katakana character. Returns FALSE for halfwidth katakana.
     * @return true if this is a katakana character, false if not.
     */
    val isKatakana: Boolean
        get() = c[0].isKatakana

    /**
     * Converts this character to half-width katakana.
     * @return halfwidth katakana or this if this character is not a full-width katakana.
     */
    fun toHalfwidthKatakana(): JpCharacter {
        return if (isKatakana) JpCharacter(c.toHalfwidthKatakana()) else this
    }

    /**
     * Converts this character to full-width katakana.
     * @return full-width katakana or this if this character is not a half-width katakana.
     */
    fun toFullwidthKatakana() = if (isHalfwidthKatakana) JpCharacter(c.halfwidthToKatakana()) else this

    /**
     * Returns true if this character is a halfwidth katakana character.
     * @return true if this character is a halfwidth katakana character.
     */
    val isHalfwidthKatakana: Boolean
        get() = c[0].isHalfwidthKatakana

    override fun toString(): String {
        // don't change: lots of code expects this to simply return the kanji itself.
        return c
    }

    /**
     * Returns kanji parts.
     * @return a part list, never null, may be empty if unknown kanji is supplied. May contain the original kanji.
     * Returns empty set if this character is not kanji.
     */
    val parts: Set<JpCharacter>
        get() = if (isKanji) Parts.getParts(Kanji(c)) else emptySet()

    @Throws(IOException::class)
    override fun writeTo(out: DataOutput) {
        out.writeUTF(c)
    }

    fun toKanji() = Kanji(c)

    override fun compareTo(other: JpCharacter) = c.compareTo(other.c)

    /**
     * True if it can be represented as a single java char. All katakana/hiragana characters can be represented as a single Java char.
     * @return false if this is some exotic kanji.
     */
    val isChar: Boolean
        get() = c.length <= 1

    /**
     * Returns the kanji as a character. Fails if the kanji cannot be represented as Java char.
     * @return the kanji
     */
    fun toCharacter(): Char {
        require(isChar) { "$c cannot be represented as a Java char" }
        return c[0]
    }

    /**
     * Returns the hexa code of this character, such as 0xAAAA.
     * @return
     */
    val hexCode: String
        get() = "0x" + Integer.toHexString(codePoint)

    /**
     * Returns true if [c] denotes the same character as this.
     */
    fun isChar(c: Char): Boolean = isChar && toCharacter() == c

    companion object : Readable<JpCharacter> {

        @Throws(IOException::class)
        override fun readFrom(di: DataInput) = JpCharacter(di.readUTF())

        /**
         * Creates the object for given code point. Fails if the code point does not denote a kanji.
         * @param codePoint the code point
         * @return Kanji, never null.
         */
        @JvmStatic
        fun fromCodePoint(codePoint: Int) = JpCharacter(String(intArrayOf(codePoint), 0, 1))
    }
}
