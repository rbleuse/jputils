/**
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.kanji

import sk.baka.aedict.util.codePoints
import sk.baka.aedict.util.toLinkedSet
import java.util.*

private val HIRAGANA_SPECIALS = setOf('っ', 'ゃ', 'ゅ', 'ょ')

/**
 * Returns true if this character is any hiragana character.
 * @return true if this character is a hiragana character, false if not.
 */
val Char.isHiragana get() = this in HIRAGANA_SPECIALS || this in IRomanization.Hepburn.HIRAGANA_CHARACTERS

/**
 * Returns true if this code point is any hiragana character.
 * @return true if this code point is a hiragana character, false if not.
 */
val Int.isHiragana get() = this < 0x8000 && toChar().isHiragana

private val KATAKANA_SPECIALS = setOf('ー', 'ャ', 'ュ', 'ョ', 'ッ', 'ヮ')

/**
 * A very simple check for katakana characters.
 * @return true if this code point is a full-width katakana character, false otherwise. Returns false for half-width katakana.
 */
val Int.isKatakana get() = this < 0x8000 && toChar().isKatakana

/**
 * A very simple check for katakana characters.
 * @return true if character is a full-width katakana character, false otherwise. Returns false for half-width katakana.
 */
val Char.isKatakana get() = this in KATAKANA_SPECIALS || this in IRomanization.Hepburn.KATAKANA_CHARACTERS

/**
 * Checks whether this code point is a kanji character.
 * @return true if it is japanese kanji character (but not kana/hiragana/katakana/...), false otherwise.
 */
val Int.isKanji: Boolean get() {
    // old kanjidic contained kanjis from range 4e00..9fa0
    //return (ch >= 19968) && (ch <= 40864);
    // new kanjidic2 contains kanjis in range 3402..2A6B2
    // this is okay: katakana/hiragana characters span 12800 and lower codepoints. therefore, isKanji will not return true for hiragana/katakana.
    if (this in 0x4E00..0x9FFF) return true  // official CJK range
    return (this == '々'.toInt()) || (this == '〆'.toInt()) || (this in 0x3402..0x4d77) || (this in 0xf91d..0xfa6a) || (this in 0x2000b..0x2a6b2)
}

/**
 * Checks whether given character is a kanji character.
 * @receiver the character to check
 * @return true if it is japanese kanji character (but not kana), false otherwise.
 */
val Char.isKanji get() = toInt().isKanji

fun String.containsHiraganaOnly() = all { it.isHiragana }
fun String.containsHiragana() = any { it.isHiragana }

fun String.containsKatakanaOnly() = all { it.isKatakana }
fun String.containsKatakana() = any { it.isKatakana }

private object HalfwidthKatakana {
    private val HALFWIDTH_KATAKANA_TABLE = "。=｡;「=｢;」=｣;、=､;ー=ｰ;ッ=ｯ;ャ=ｬ;ュ=ｭ;ョ=ｮ;ァ=ｧ;ィ=ｨ;ゥ=ｩ;ェ=ｪ;ォ=ｫ;ア=ｱ;イ=ｲ;ウ=ｳ;エ=ｴ;オ=ｵ;カ=ｶ;キ=ｷ;ク=ｸ;ケ=ｹ;コ=ｺ;サ=ｻ;シ=ｼ;ス=ｽ;セ=ｾ;ソ=ｿ;タ=ﾀ;チ=ﾁ;ツ=ﾂ;テ=ﾃ;ト=ﾄ;ナ=ﾅ;ニ=ﾆ;ヌ=ﾇ;ネ=ﾈ;ノ=ﾉ;ハ=ﾊ;ヒ=ﾋ;フ=ﾌ;ヘ=ﾍ;ホ=ﾎ;マ=ﾏ;ミ=ﾐ;ム=ﾑ;メ=ﾒ;モ=ﾓ;ヤ=ﾔ;ユ=ﾕ;ヨ=ﾖ;ラ=ﾗ;リ=ﾘ;ル=ﾙ;レ=ﾚ;ロ=ﾛ;ワ=ﾜ;ヲ=ｦ;ン=ﾝ;ガ=ｶﾞ;ギ=ｷﾞ;グ=ｸﾞ;ゲ=ｹﾞ;ゴ=ｺﾞ;ザ=ｻﾞ;ジ=ｼﾞ;ズ=ｽﾞ;ゼ=ｾﾞ;ゾ=ｿﾞ;ダ=ﾀﾞ;ヂ=ﾁﾞ;ヅ=ﾂﾞ;デ=ﾃﾞ;ド=ﾄﾞ;バ=ﾊﾞ;ビ=ﾋﾞ;ブ=ﾌﾞ;ベ=ﾍﾞ;ボ=ﾎﾞ;パ=ﾊﾟ;ピ=ﾋﾟ;プ=ﾌﾟ;ペ=ﾍﾟ;ポ=ﾎﾟ"
    private val KATAKANA_TO_HALFWIDTH = HashMap<String, String>()
    private val HALFWIDTH_TO_KATAKANA = HashMap<String, String>()

    init {
        for (entry in HALFWIDTH_KATAKANA_TABLE.split(';')) {
            val mapping = entry.split('=')
            val kana = mapping[0]
            val halfwidth = mapping[1]
            if (KATAKANA_TO_HALFWIDTH.put(kana, halfwidth) != null) throw IllegalArgumentException("Mapping for $kana defined multiple times")
            if (HALFWIDTH_TO_KATAKANA.put(halfwidth, kana) != null) throw IllegalArgumentException("Mapping for $halfwidth defined multiple times")
        }
    }

    /**
     * Converts a string containing half-width katakana to full-width katakana.
     * Non-half-width katakana characters are unchanged.
     * @param halfwidth a string containing half-width characters, not null
     * @return full-width katakana, never null
     */
    internal fun halfwidthToKatakana(halfwidth: String) = translate(halfwidth, HALFWIDTH_TO_KATAKANA, 2)

    /**
     * Converts a string containing full-width katakana to half-width katakana.
     * Non-full-width katakana characters are unchanged.
     * @param katakana a string containing full-width characters, not null
     * @return half-width katakana, never null
     */
    internal fun toHalfwidth(katakana: String) = translate(katakana, KATAKANA_TO_HALFWIDTH, 1)

    internal fun isHalfwidth(ch: Char) = HALFWIDTH_TO_KATAKANA.containsKey(ch.toString())

    private fun translate(`in`: String, table: Map<out String, String>, maxKeyLength: Int): String {
        val result = StringBuilder(`in`.length)
        var i = 0
        while (i < `in`.length) {
            var translated: String? = null
            var prefixLen: Int
            prefixLen = Math.min(maxKeyLength, `in`.length - i)
            while (prefixLen >= 1) {
                val prefix = `in`.substring(i, i + prefixLen)
                translated = table[prefix]
                if (translated != null) {
                    break
                }
                prefixLen--
            }
            result.append(if (translated != null) translated else `in`.substring(i, i + 1))
            if (translated != null && prefixLen > 1) {
                i += prefixLen - 1
            }
            i++
        }
        return result.toString()
    }
}

/**
 * Converts a string containing half-width katakana to full-width katakana.
 * Non-half-width katakana characters are unchanged.
 * @return full-width katakana, never null
 */
fun String.halfwidthToKatakana() = HalfwidthKatakana.halfwidthToKatakana(this)

/**
 * Converts a string containing full-width katakana to half-width katakana.
 * Non-full-width katakana characters are unchanged.
 * @return half-width katakana, never null
 */
fun String.toHalfwidthKatakana() = HalfwidthKatakana.toHalfwidth(this)

/**
 * Checks if given code point denotes halfwidth katakana character.
 * @param codePoint the character, not null
 * @return true if given code point is a halfwidth katakana character.
 */
val Int.isHalfwidthKatakana get() = this < 0x8000 && toChar().isHalfwidthKatakana

/**
 * Checks if given string consists of exactly one halfwidth katakana character.
 * @receiver the character, not null
 * @return true if c is of length 1 and first character is a half-width katakana, false otherwise (string is empty or contains more than one character, etc).
 */
val String.isSingleHalfwidthKatakanaChar get() = length == 1 && this[0].isHalfwidthKatakana

/**
 * Returns true if this character is a halfwidth katakana character.
 * @return true if this character is a halfwidth katakana character.
 */
val Char.isHalfwidthKatakana get() = HalfwidthKatakana.isHalfwidth(this)

/**
 * Returns all kanji characters present in this string. Ak sa kanji v stringu opakuju, budu sa opakovat aj v liste.
 */
val String.kanjis: List<Kanji>
    get() = codePoints.filter { it.isKanji }.map { it.toKanji() }

/**
 * Returns all unique Kanji characters as a linked set.
 * @return unique kanjis in a linked set, in the order as they appeared in the string.
 */
fun String.getUniqueKanjis() = kanjis.toLinkedSet()

/**
 * Checks whether given character is a kana character:
 * [isKatakana]/[isHiragana]/[isHalfwidthKatakana] character.
 * @return true if it is japanese kana character, false if it is [Int.isKanji], romaji, hangul, etc otherwise.
 */
val Int.isKana get() = isKatakana || isHiragana || isHalfwidthKatakana

/**
 * Checks whether given character is a kana character:
 * [isHiragana]/[isKatakana] or [isHalfwidthKatakana] character.
 * @receiver the character to check
 * @return true if it is japanese kana character (not {@link Kanji#isKanji(char)
 *         kanji}), false otherwise.
 */
val Char.isKana get() = isKatakana || isHiragana || isHalfwidthKatakana

fun String.containsKanaOrKanji() = codePoints.any { it.isKana || it.isKanji }

fun String.containsKanaOrKanjiOnly() = codePoints.all { it.isKana || it.isKanji }

/**
 * True if this character is a-zA-Z
 */
val Char.isRomaji get() = this in 'a'..'z' || this in 'A'..'Z'

/**
 * True if this character is a-zA-Z
 */
val Int.isRomaji get() = this < 0x8000 && toChar().isRomaji

/**
 * Checks whether this string contains at least one kanji
 * @return true if at least one code point is kanji ([Int.isKanji])
 */
fun String.containsKanji() = codePoints.any { it.isKanji }

fun String.containsRomaji() = any { it.isRomaji }

val String.isSingleKanji: Boolean get() {
    if (length < 1 || length > 2 || codePointCount(0, length) != 1) {
        return false
    }
    val k = codePointAt(0)
    return k.isKanji
}

private object KatakanaToHiragana {
    private val h2kdef = "あ=ア;い=イ;う=ウ;え=エ;お=オ;か=カ;き=キ;く=ク;け=ケ;こ=コ;ゃ=ャ;ゅ=ュ;ょ=ョ;さ=サ;し=シ;す=ス;せ=セ;そ=ソ;た=タ;ち=チ;つ=ツ;て=テ;と=ト;な=ナ;に=ニ;ぬ=ヌ;ね=ネ;の=ノ;は=ハ;ひ=ヒ;ふ=フ;へ=ヘ;ほ=ホ;ま=マ;み=ミ;む=ム;め=メ;も=モ;や=ヤ;ゆ=ユ;よ=ヨ;ら=ラ;り=リ;る=ル;れ=レ;ろ=ロ;わ=ワ;ゐ=ヰ;ゑ=ヱ;を=ヲ;ん=ン;が=ガ;ぎ=ギ;ぐ=グ;げ=ゲ;ご=ゴ;ざ=ザ;じ=ジ;ず=ズ;ぜ=ゼ;ぞ=ゾ;だ=ダ;ぢ=ヂ;づ=ヅ;で=デ;ど=ド;ば=バ;び=ビ;ぶ=ブ;べ=ベ;ぼ=ボ;ぱ=パ;ぴ=ピ;ぷ=プ;ぺ=ペ;ぽ=ポ;ぁ=ァ;ぃ=ィ;ぅ=ゥ;ぇ=ェ;ぉ=ォ;ゔ=ヴ;っ=ッ"
    private val k2h = HashMap<Char, Char>()
    private val h2k = HashMap<Char, Char>()

    init {
        h2kdef.split(';').forEach { pair ->
            val (hiragana, katakana) = pair.split('=')
            require(hiragana.isSingleHiraganaChar) { "Parameter hiragana: invalid value $hiragana: not a single hiragana character" }
            require(katakana.isSingleKatakanaChar) { "Parameter katakana: invalid value $katakana: not a single katakana character" }
            k2h[katakana[0]] = hiragana[0]
            h2k[hiragana[0]] = katakana[0]
        }
    }

    fun h2k(str: String) = str.mapJoin { h2k[it] ?: it }
    fun k2h(str: String) = str.mapJoin { k2h[it] ?: it }
}

/**
 * Translates string characters effectively.
 * @param f the mapping function
 */
inline fun String.mapJoin(f: (Char) -> Any): String {
    val sb = StringBuilder(length)
    for (c in this) { sb.append(f(c)) }
    return sb.toString()
}

/**
 * Translates string characters effectively.
 * @param f the mapping function, converts a code point to another code point (or null, in that case the code point is deleted).
 */
inline fun String.mapJoinCP(f: (Int) -> Int?): String {
    val cp = IntArray(length)
    var i = 0
    for (c in codePoints) {
        val transformed = f(c)
        if (transformed != null) {
            cp[i++] = transformed
        }
    }
    return String(cp, 0, i);
}

/**
 * Replaces all katakana characters with appropriate hiragana characters. IGNORES HALF-WIDTH KATAKANA. Romaji, kanji, hiragana and other characters are left
 * untouched.
 * @return string with katakana characters replaced with hiragana.
 */
fun String.mapKatakanaToHiragana() = KatakanaToHiragana.k2h(this)

/**
 * Replaces all hiragana characters with appropriate katakana characters. IGNORES HALF-WIDTH KATAKANA. Romaji, kanji, katakana and other characters are left
 * untouched.
 * @return string with hiragana characters replaced with katakana.
 */
fun String.mapHiraganaToKatakana() = KatakanaToHiragana.h2k(this)

fun Int.toKanji() = Kanji(this)

fun Char.toKanji() = Kanji(this)

/**
 * Checks if this code point is a japanese character - kanji, hiragana, katakana (either halfwidth or fullwidth).
 * @receiver the code point
 * @return if given code point is a JP character. False for full-width romaji (Ａ)
 */
val Int.isJpChar get() = isKana || isKanji

/**
 * Checks if this character is a japanese character - kanji, hiragana, katakana (either halfwidth or fullwidth).
 * @receiver the character
 * @return if given code point is a JP character. False for full-width romaji (Ａ)
 */
val Char.isJpChar get() = isKana || isKanji

/**
 * Checks if given string is exactly one japanese character - kanji, hiragana, katakana (either halfwidth or fullwidth).
 * @param c the string, not null
 * @return if given string consists of exactly one jp character, false if the string is blank, has more than 2 length, etc.
 */
val String.isSingleJpChar get() =
        isSingleKanji || isSingleHiraganaChar || isSingleKatakanaChar || isSingleHalfwidthKatakanaChar

val String.isSingleHiraganaChar get() = length == 1 && this[0].isHiragana
val String.isSingleKatakanaChar get() = length == 1 && this[0].isKatakana

/**
 * Checks if this character is a hiragana consonant: あいうえお
 */
val Char.isHiraganaConsonant: Boolean get() = this == 'あ' || this == 'い' || this == 'う' || this == 'え' || this == 'お'

/**
 * Je znak c fullwidth romaji pismeno, ako napr. Ａ ? Funguje len na pismena, pre full-width vykricnik vrati false!
 * @receiver the character
 * @return true ak je to fullwidth pismeno Ａ, false ak je to A, kana, alebo aj ！ (full-width vykricnik).
 */
val Char.isFullwidthRomajiLetter get() = toAsciiRomajiLetter() != this

/**
 * Je znak c fullwidth romaji pismeno, ako napr. Ａ ? Funguje len na pismena, pre full-width vykricnik vrati false!
 * @receiver the character code point
 * @return true ak je to fullwidth pismeno Ａ, false ak je to A, kana, alebo aj ！ (full-width vykricnik).
 */
val Int.isFullwidthRomajiLetter get() = this < 0xFFFF && toChar().isFullwidthRomajiLetter

/**
 * Vrati Ascii counterpart pre fullwidth romaji.
 * @param c char
 * @return A pre Ａ. Konvertuje len pismena, ignoruje napriklad '！' a vsetky dalsie znaky
 */
fun Char.toAsciiRomajiLetter(): Char = when (toInt()) {
    in 0xFF21..0xFF3A -> 'A' + (toInt() - 0xFF21)
    in 0xFF41..0xFF5A -> 'a' + (toInt() - 0xFF41)
    else -> this
}

fun Char.fromFullwidth(): Char = when (toInt()) {
    in 0xFF21..0xFF3A -> 'A' + (toInt() - 0xFF21)
    in 0xFF41..0xFF5A -> 'a' + (toInt() - 0xFF41)
    in 0xFF01..0xFF20 -> """!"#$%&'()*+,-./0123456789:;<=>?@"""[toInt() - 0xFF01]
    in 0xFF3B..0xFF40 -> """[\]^_`"""[toInt() - 0xFF3B]
    in 0xFF5B..0xFF5E -> """{|}~"""[toInt() - 0xFF5B]
    else -> this
}

fun Char.isFullwidth() = this != fromFullwidth()

/**
 * Converts fullwidth to regular ascii; for example converts "abｃｄ！" to "abcd!".
 */
fun String.fromFullwidth(): String = mapJoin { it.fromFullwidth() }

/**
 * Kanjidic: reading moze obsahovat znaky - a ., tato funkcia ich odstrani takze ostane len kana.
 */
fun String.kanjidicReadingRemoveSplits(): String = replace("-", "").replace(".", "")

inline fun String.filterOutCP(filter: (Int) -> Boolean): String =
        mapJoinCP { it -> if (filter(it)) it else null }

fun String.removeKanaKanji(): String = filterOutCP { !it.isKana && !it.isKanji }
fun String.replaceKanaKanjiWith(with: Char): String = mapJoinCP { if (it.isKana || it.isKanji) with.toInt() else it }

/**
 * Splits given string into hiragana/katakana digraphs/monographs, e.g. しゅじん -> [しゅ, じ, ん]. Does not perform romanization/de-romanization.
 * Non-hiragana/katakana characters are kept as-is.
 * @param hiraganaOrKatakana text in hiragana or katakana, not null
 * @return splitted characters
 */
fun String.splitKanaToDigraphs(): List<String> {
    val list = ArrayList<String>(length)
    var i = 0
    while (i < length) {
        // check two consecutive kana characters first - to support stuff
        // like "pyu" etc
        if (i < length - 1) {
            val kana = substring(i, i + 2)
            var matched = IRomanization.Hepburn.KATAKANA.contains(kana)
            if (!matched) {
                matched = IRomanization.Hepburn.HIRAGANA.contains(kana)
            }
            if (matched) {
                // success! skip next kana
                i++
                list.add(kana)
                i++
                continue
            }
        }
        list.add(get(i).toString())
        i++
    }
    return list
}

/**
 * Checks if given character is a vowel (a i u e o A I U E O)
 * @receiver c the character
 * @return true if it is a vowel.
 */
fun Char.isAsciiVowel(): Boolean = this == 'a' || this == 'u' || this == 'e' || this == 'i' || this == 'o' ||
        this == 'A' || this == 'U' || this == 'E' || this == 'I' || this == 'O'
