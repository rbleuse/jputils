/**
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.kanji;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import sk.baka.aedict.util.Check;
import sk.baka.aedict.util.MiscUtils;

import java.io.Serializable;
import java.util.*;

/**
 * Performs romanization.
 */
public interface IRomanization extends Serializable {
    /**
     * Converts given romaji text to hiragana. Ignores non-romaji characters and untranslatable romaji characters and
     * passes them as-is to the output.
     *
     * @param romaji romaji text
     * @return text converted to hiragana, with unknown characters untranslated.
     */
	@NotNull
	String toHiragana(@NotNull final String romaji);

    /**
     * Converts given romaji text to katakana. Ignores non-romaji characters and untranslatable romaji characters and
     * passes them as-is to the output.
     *
     * @param romaji romaji text
     * @return text converted to katakana, with unknown characters untranslated.
     */
	@NotNull
	String toKatakana(@NotNull final String romaji);

    /**
     * Converts a text in hiragana or katakana to romaji. Does not transform any other characters.
     *
     * @param hiraganaOrKatakana hiragana or katakana character
     * @return romaji
     */
	@NotNull
	String toRomaji(final char hiraganaOrKatakana);

    /**
     * Converts a text in hiragana or katakana to romaji. Ignores other characters (kanjis, braces etc) and simply passes
     * them to the output. Equal to {@link #toRomaji(String, boolean)} with useX set to false.
     *
     * @param hiraganaOrKatakana text in hiragana or katakana, not null
     * @return romaji text
     */
	@NotNull
	String toRomaji(@NotNull final String hiraganaOrKatakana);

    /**
     * Converts a text in hiragana or katakana to romaji. Ignores other characters (kanjis, braces etc) and simply passes
     * them to the output.
     *
     * @param hiraganaOrKatakana text in hiragana or katakana, not null
     * @param useX if true, uses X to remove ambiguity e.g. んあ -> xna
     * @return romaji text
     */
    @NotNull
    String toRomaji(@NotNull final String hiraganaOrKatakana, boolean useX);

    /**
     * Returns a hint on how to write given katakana/hiragana character in
     * romaji so that it may be properly translated back. For example querying
     * for づ returns xzu.
     *
     * @param kana the kana character, not blank.
     * @return a writing or null if no such kana is known.
     */
	@Nullable
	String getWriting(@NotNull final String kana);

	/**
     * Detects that there is romaji which cannot be translated to hiragana/katakana (e.g. mtu) in the romaji input. Ignores
     * kanjis and other non-ascii-letter characters.
     * @param romaji the romaji
     * @return true if there is some untranslatable kana, false if not.
     */
    boolean containsUntranslatableRomaji(@NotNull final String romaji);

    final class TransformResult {
        /**
         * The result of the transformation, hiragana, katakana or romaji, depending on the operation.
         */
        @NotNull
        public final String result;
        /**
         * Length, in characters (not codepoints) of the original text which was transformed to {@link #result}.
         */
        public final int originLength;

        public TransformResult(@NotNull String result, int originLength) {
            this.result = result;
            this.originLength = originLength;
        }
    }

    /**
     * Takes first couple of romaji characters from given string and translates first romaji syllabogram (ha, tsu, tte, kkyo, but not kyou - only kyo is translated)
     * to hiragana.
     * @param string the string, not null, may be blank
     * @return transformed result or null if the start of given string is not a part of any romaji syllabogram, or is already hiragana, katakana, kanji, etc.
     */
    @Nullable
    TransformResult getFirstRomajiSyllabogramAsHiragana(@NotNull final String string);

    /**
     * Takes first couple of romaji characters from given string and translates first romaji syllabogram (haa, tsu, tte, kkyo, kyoo, but not kyou - only kyo is translated)
     * to katakana.
     * @param string the string, not null, may be blank
     * @return transformed result or null if the start of given string is not a part of any romaji syllabogram, or is already hiragana, katakana, kanji, etc.
     */
    @Nullable
    TransformResult getFirstRomajiSyllabogramAsKatakana(@NotNull final String string);

    /**
     * Takes first couple of katakana/hiragana characters from given string and translates first syllabogram (haa, tsu, tte, kkyo, kyoo, but not kyou - only kyo is translated)
     * to romaji.
     * @param string the string, not null, may be blank
     * @return transformed result or null if the start of given string is not a part of any kana syllabogram, or is already romaji, kanji, space, etc.
     */
    @Nullable
    TransformResult getFirstKanaSyllabogramAsRomaji(@NotNull final String string);

    final class RomanizationTable {
        // no need for ConcurrentHashMap here: the map is only modified in the constructor;
        // any alterations of properties of a final reference in a constructor happens-before
        // any code after the object is constructed, thus all threads will see the changes.
        // Moreover, enum constants are initialized in a static initializer, which is thread-safe anyway.

		/**
		 * Maps katakana to romaji, e.g. "キャ" -> "kya"
         */
        private final Map<String, String> katakanaToRomaji = new HashMap<>();
		/**
         * Maps hiragana to romaji, e.g. "きゃ" -> "kya"
         */
        private final Map<String, String> hiraganaToRomaji = new HashMap<>();
		/**
         * Inverse to {@link #katakanaToRomaji}, maps "kya" to "キャ"
         */
        private final Map<String, String> romajiToKatakana = new HashMap<>();
		/**
         * Inverse to {@link #hiraganaToRomaji}, maps "kya" to "きゃ"
         */
        private final Map<String, String> romajiToHiragana = new HashMap<>();
        private final int maxRomajiLen;

		/**
         * Parses a string in the form of "あ=a;い=i;..." and fills in given maps.
         * @param kanaTable the string in the form of "あ=a;い=i;..."
         * @param kanaToRomaji filled with "キャ" -> "kya" mappings
         * @param romajiToKana filled with "kya" -> "キャ" mappings
         * @return maximum length of romaji key in romajiToKana
         */
        private int parse(@NotNull final String kanaTable, @NotNull Map<String, String> kanaToRomaji, @NotNull Map<String, String> romajiToKana) {
            int maxRomajiLen = 0;
            for (final Object entry : Collections.list(new StringTokenizer(kanaTable, ";"))) {
                final String[] mapping = ((String) entry).split("=");
                final String kana = mapping[0];
                final String[] romajis = mapping[1].split(",");
                if (kanaToRomaji.put(kana, romajis[0]) != null) {
                    throw new IllegalArgumentException("Mapping for " + kana + " defined multiple times");
                }
                for (final String romaji : romajis) {
                    if (romajiToKana.put(romaji, kana) != null) {
                        throw new IllegalArgumentException("Mapping for " + romaji + " defined multiple times");
                    }
                    maxRomajiLen = Math.max(maxRomajiLen, romaji.length());
                }
            }
            return maxRomajiLen;
        }

        public static @NotNull Set<Character> getKanaChars(@NotNull Set<String> chars) {
            final Set<Character> result = new HashSet<Character>();
            for (String s: chars) {
                for (char c: s.toCharArray()) {
                    result.add(c);
                }
            }
            return result;
        }

        /**
         * Creates a romanization table.
         * @param hiraganaTable a mapping of hiragana characters to the appropriate reading in
         * latin. The format is as follows: KANA=reading;KANA2=reading;...
         * @param katakanaTable a mapping of katakana characters to the appropriate reading in
         * latin. The format is as follows: KANA=reading;KANA2=reading;...
         */
        public RomanizationTable(@NotNull String hiraganaTable, @NotNull String katakanaTable) {
            final int max1 = parse(katakanaTable, katakanaToRomaji, romajiToKatakana);
            final int max2 = parse(hiraganaTable, hiraganaToRomaji, romajiToHiragana);
            maxRomajiLen = Math.max(max1, max2);
        }

        public int getMaxRomajiLen() {
            return maxRomajiLen;
        }
    }

    abstract class AbstractTableBasedRomanization implements IRomanization {
		@NotNull
		protected abstract RomanizationTable getRomanizationTable();

        @Override
		@NotNull
		public final String toHiragana(@NotNull final String romaji) {
            return toKana(getRomanizationTable().romajiToHiragana, getRomanizationTable().getMaxRomajiLen(), romaji, false);
        }

        @Override
		@NotNull
		public final String toKatakana(@NotNull final String romaji) {
            return toKana(getRomanizationTable().romajiToKatakana, getRomanizationTable().getMaxRomajiLen(), romaji, true);
        }

        @Nullable
        @Override
        public final TransformResult getFirstRomajiSyllabogramAsHiragana(@NotNull String string) {
            return getFirstRomajiSyllabogram(string, false);
        }

        private TransformResult getFirstRomajiSyllabogram(@NotNull String string, boolean katakana) {
            if (string.length() == 0) {
                return null;
            }
            final StringBuilder sb = new StringBuilder();
            final int inputLen = toKanaSingleSyllabogram(sb,
                    katakana ? getRomanizationTable().romajiToKatakana : getRomanizationTable().romajiToHiragana,
                    getRomanizationTable().getMaxRomajiLen(),
                    string,
                    0,
                    katakana);
            if (inputLen == 1 && (sb.length() == 0 || sb.charAt(0) == string.charAt(0))) {
                return null;
            }
            return new TransformResult(sb.toString(), inputLen);
        }

        @Nullable
        @Override
        public TransformResult getFirstRomajiSyllabogramAsKatakana(@NotNull String string) {
            return getFirstRomajiSyllabogram(string, true);
        }

        @Override
        public boolean containsUntranslatableRomaji(@NotNull String romaji) {
            return containsUntranslatableRomaji(getRomanizationTable().romajiToHiragana, getRomanizationTable().getMaxRomajiLen(), romaji);
        }

        protected static boolean containsUntranslatableRomaji(@NotNull final Map<String, String> romajiToKana, int maxRomajiLen, @NotNull String romaji) {
            final StringBuilder sb = new StringBuilder();
            for (int i = 0; i < romaji.length(); ) {
                final int consumed = toKanaSingleSyllabogram(sb, romajiToKana, maxRomajiLen, romaji, i, false);
                if (sb.length() == consumed && sb.toString().equals(romaji.substring(i, i + consumed))) {
                    // maybe an untranslatable char?
                    if (MiscUtils.isAsciiLetter(romaji.charAt(i))) {
                        return true;
                    }
                }
                i += consumed;
                sb.delete(0, sb.length());
            }
            return false;
        }

        /**
         * Processes the romaji string and converts romaji tokens into kana if possible.
         * @param romajiToKana the map which maps e.g. "kya" -> "キャ"
         * @param maxRomajiLen max length of romajiToKana key string
         * @param romaji original text to be converted
         * @param isKatakana if true then we are converting to katakana; if false, we are converting to hiragana
         * @return the converted string
         */
        @NotNull
		private static String toKana(@NotNull final Map<String, String> romajiToKana, int maxRomajiLen, @NotNull String romaji, final boolean isKatakana) {
            final StringBuilder sb = new StringBuilder();
            for (int i = 0; i < romaji.length(); ) {
                i += toKanaSingleSyllabogram(sb, romajiToKana, maxRomajiLen, romaji, i, isKatakana);
            }
            return sb.toString();
        }

		/**
         * Converts kana syllabogram at given index. Returns the number of characters consumed by the conversion.
         * @param sb append conversion product here
         * @param romajiToKana the map which maps e.g. "kya" -> "キャ"
         * @param maxRomajiLen max length of romajiToKana key string
         * @param romaji
         * @param romajiIndexAt
         * @param isKatakana
         * @return
         */
        private static int toKanaSingleSyllabogram(@NotNull StringBuilder sb, @NotNull final Map<String, String> romajiToKana, int maxRomajiLen, @NotNull String romaji, int romajiIndexAt, final boolean isKatakana) {
            int i = romajiIndexAt;
            // optimization - only convert ascii letters
            final char c = romaji.charAt(i);
            if (!MiscUtils.isAsciiLetter(c)) {
                if (c == '\'' && i > 0 && MiscUtils.isAsciiLetter(romaji.charAt(i - 1))) {
                    // this is a syllable separator (similar to x) - handle it correctly
                } else {
                    sb.append(c);
                    return 1;
                }
            }
            String kana = null;
            if (isAt(romaji, i, "xtu")) {
                kana = isKatakana ? "ッ" : "っ";
                i += 2;
            }
            if (kana == null && isAt(romaji, i, "xtsu")) {
                kana = isKatakana ? "ッ" : "っ";
                i += 3;
            }
            if (kana == null) {
                for (int matchLen = Math.min(romaji.length() - i, maxRomajiLen); matchLen >= 1; matchLen--) {
                    final String romajiMatch = romaji.substring(i, i + matchLen).toLowerCase();
                    kana = romajiToKana.get(romajiMatch);
                    if (kana != null) {
                        i += matchLen - 1;
                        break;
                    }
                }
            }
            if (kana == null && romaji.substring(i).startsWith("nn")) {
                // check for 'nn'
                kana = romajiToKana.get("nn");
                i += 1;
            }
            if (kana == null && romaji.substring(i).startsWith("n")) {
                // a stand-alone n.
                kana = romajiToKana.get("n");
            }
            boolean smallTsu = false;
            if (kana == null && i < romaji.length() - 1) {
                // check for double consonant: for example "tta" must be
                // transformed to った
                String romajiMatch = romaji.substring(i, i + 2);
                if (isDoubledConsonant(romajiMatch)) {
                    kana = isKatakana ? "ッ" : "っ";
                    smallTsu = true;
                }
            }
            if (kana == null) {
                // give up
                kana = String.valueOf(romaji.charAt(i));
            }
            if (isKatakana) {
                // check for double vowel: in katakana, aa must be replaced by
                // アー instead of アア
                if (i < romaji.length() - 1 && MiscUtils.isVowel(romaji.charAt(i + 1)) && romaji.charAt(i + 1) == romaji.charAt(i)) {
                    kana += "ー";
                    i++;
                }
            }
            if (smallTsu) {
                // try to translate rest of the expression - ak sa to nepodari, nedavaj tam small tsu
                final StringBuilder sb2 = new StringBuilder();
                int len = toKanaSingleSyllabogram(sb2, romajiToKana, maxRomajiLen, romaji, romajiIndexAt + 1, isKatakana);
                if (len == 1 && sb2.charAt(0) == romaji.charAt(romajiIndexAt + 1)) {
                    // no success translating
                    kana = String.valueOf(romaji.charAt(i));
                } else {
                    // translated!
                    kana = kana + sb2;
                    i += len;
                }
            }
            sb.append(kana);
            i++;
            return i - romajiIndexAt;
        }

        private static boolean isAt(String string, int index, String substring) {
            if (index + substring.length() > string.length()) {
                return false;
            }
            return string.substring(index, index + substring.length()).toLowerCase().equals(substring);
        }

        private final static Set<String> DOUBLED_CONSONANTS = new HashSet<String>(Arrays.asList("rr", "tt", "pp", "ss", "dd", "gg", "hh", "jj", "kk", "zz", "cc", "bb", "mm"));

        private static boolean isDoubledConsonant(@NotNull final String str) {
            if (str.length() != 2) {
                throw new AssertionError();
            }
            return DOUBLED_CONSONANTS.contains(str.toLowerCase());
        }

        @Override
		@NotNull
		public final String toRomaji(final char hiraganaOrKatakana) {
            return toRomaji(String.valueOf(hiraganaOrKatakana));
        }

        @Override
		@NotNull
		public final String toRomaji(@NotNull final String hiraganaOrKatakana, boolean useX) {
            final StringBuilder sb = new StringBuilder();
			boolean prevWasN = false;
            for (int i = 0; i < hiraganaOrKatakana.length();) {
				final char c = hiraganaOrKatakana.charAt(i);
				final boolean isN = c == 'ん';
				final boolean isHiraganaConsonant = JpCharKt.isHiraganaConsonant(c);
				if (prevWasN && useX && isHiraganaConsonant) {
					sb.insert(sb.length() - 1, 'x');
				}
                int len = toRomajiSingleSyllabogram(sb, hiraganaOrKatakana, i, useX);
                i += len;
				prevWasN = isN;
            }
            return sb.toString();
        }

		@NotNull
		@Override
		public String toRomaji(@NotNull String hiraganaOrKatakana) {
			return toRomaji(hiraganaOrKatakana, false);
		}

		@Nullable
        @Override
        public TransformResult getFirstKanaSyllabogramAsRomaji(@NotNull String string) {
            if (string.isEmpty()) {
                return null;
            }
            final StringBuilder sb = new StringBuilder();
            int len = toRomajiSingleSyllabogram(sb, string, 0, false);
            if (len == 1 && (sb.length() == 0 || sb.charAt(0) == string.charAt(0))) {
                return null;
            }
            return new TransformResult(sb.toString(), len);
        }

        /**
         * Translates one syllabogram at {@code hiraganaOrKatakana} index {@code index}.
         * @param sb appends translated syllabogram here
         * @param hiraganaOrKatakana the string being translated
         * @param index index of the syllabogram in {@code hiraganaOrKatakana}, 0..hiraganaOrKatakana.length-1
         * @return the number of characters from <code>hiraganaOrKatakana</code> consumed during the translation.
         */
        private int toRomajiSingleSyllabogram(
                @NotNull StringBuilder sb,
                @NotNull final String hiraganaOrKatakana,
                int index,
                boolean useX
        ) {
            if (index < 0 || index >= hiraganaOrKatakana.length()) {
                throw new IllegalArgumentException("Parameter index: invalid value " + index + ": must be 0.." + hiraganaOrKatakana);
            }
            // last kana character was the small "tsu". this means that we have to
            // double next character.
            boolean startsWithXtsu = false;
            int i = index;
            if (hiraganaOrKatakana.charAt(index) == 'っ' || hiraganaOrKatakana.charAt(index) == 'ッ') {
                startsWithXtsu = true;
                i++;
                if (i >= hiraganaOrKatakana.length()) {
                    // just a small "tsu" at the end of the string - return as-is
                    if (useX) {
                        sb.append("x").append(getRomanizationTable().hiraganaToRomaji.get("つ"));
                    } else {
                        sb.append(hiraganaOrKatakana.charAt(index));
                    }
                    return 1;
                }
            }
            // check two consecutive kana characters first - to support stuff
            // like "pyu" etc
            String romaji = null;
            String kana = null;
            if (i < hiraganaOrKatakana.length() - 1) {
                kana = hiraganaOrKatakana.substring(i, i + 2);
                romaji = getRomanizationTable().katakanaToRomaji.get(kana);
                if (romaji == null) {
                    romaji = getRomanizationTable().hiraganaToRomaji.get(kana);
                }
                if (romaji != null) {
                    // success! skip next kana
                    i++;
                }
            }
            if (romaji == null) {
                // nope. convert just a single kana character
                kana = String.valueOf(hiraganaOrKatakana.charAt(i));
                romaji = getRomanizationTable().katakanaToRomaji.get(kana);
            }
            if (romaji == null) {
                romaji = getRomanizationTable().hiraganaToRomaji.get(kana);
            }
            if (romaji != null) {
                // fix xji and nn
                if (romaji.equals("nn")) {
                    romaji = "n";
                }
                while (romaji.startsWith("x")) {
                    romaji = romaji.substring(1);
                }
            }
            // check for katakana "-"
            if (romaji != null && i < hiraganaOrKatakana.length() - 1 && hiraganaOrKatakana.charAt(i + 1) == 'ー') {
                // just repeat last letter if there is one
                romaji += romaji.substring(romaji.length() - 1);
                i++;
            }
            if (romaji == null) {
                romaji = kana;
            }
            if (startsWithXtsu) {
                // consonant duplication, but only if "kana" actually is katakana or hiragana!
                if (JpCharKt.isHiragana(kana.charAt(0)) || JpCharKt.isKatakana(kana.charAt(0))) {
                    sb.append(romaji.charAt(0));
                } else {
                    // xtsu pass-through
                    if (useX) {
                        sb.append("x").append(getRomanizationTable().hiraganaToRomaji.get("つ"));
                    } else {
                        sb.append(hiraganaOrKatakana.charAt(index));
                    }
                }
            }
            sb.append(romaji);
            i++;
            return i - index;
        }

        @Override
		@Nullable
		public final String getWriting(@NotNull final String kana) {
            Check.requireNotBlank(kana, "kana");
            String result = getRomanizationTable().katakanaToRomaji.get(kana);
            if (result == null) {
                result = getRomanizationTable().hiraganaToRomaji.get(kana);
            }
            return result;
        }
    }
    /**
     * The Hepburn romanization.
     */
    final class Hepburn extends AbstractTableBasedRomanization {
        private static final String HIRAGANA_TABLE = "あ=a;い=i;う=u;え=e;お=o;か=ka;き=ki;く=ku;け=ke;こ=ko;きゃ=kya;きゅ=kyu;きょ=kyo;さ=sa;し=shi,si;す=su;せ=se;そ=so;しゃ=sha,sya;しゅ=shu,syu;しょ=sho,syo;た=ta;ち=chi,ti;つ=tsu,tu;て=te;と=to;ちゃ=cha;ちゅ=chu;ちょ=cho;な=na;に=ni;ぬ=nu;ね=ne;の=no;にゃ=nya;にゅ=nyu;にょ=nyo;は=ha;ひ=hi;ふ=fu,hu;へ=he;ほ=ho;ひゃ=hya;ひゅ=hyu;ひょ=hyo;ま=ma;み=mi;む=mu;め=me;も=mo;みゃ=mya;みゅ=myu;みょ=myo;や=ya,xya,'ya;ゆ=yu,xyu,'yu;よ=yo,xyo,'yo;ら=ra;り=ri;る=ru;れ=re;ろ=ro;りゃ=rya;りゅ=ryu;りょ=ryo;わ=wa;ゐ=wi;ゑ=we;を=wo;ん=n,xn,'n;が=ga;ぎ=gi;ぐ=gu;げ=ge;ご=go;ぎゃ=gya;ぎゅ=gyu;ぎょ=gyo;ざ=za;じ=ji,dži,zi;ず=zu;ぜ=ze;ぞ=zo;じゃ=ja,jya;じゅ=ju;じぇ=je;じょ=jo;だ=da;ぢ=xji,di,dji;づ=xzu,du,dzu;で=de;ど=do;ぢゃ=xja;ぢゅ=xju;ぢょ=xjo;ば=ba;び=bi;ぶ=bu;べ=be;ぼ=bo;びゃ=bya;びゅ=byu;びょ=byo;ぱ=pa;ぴ=pi;ぷ=pu;ぺ=pe;ぽ=po;ぴゃ=pya;ぴゅ=pyu;ぴょ=pyo;くゎ=kwa;ぐゎ=gwa;ぁ=xa;ぃ=xi;ぅ=xu;ぇ=xe;ぉ=xo;ふぁ=fa;ふぃ=fi;ふぇ=fe;ふぉ=fo;ゔぁ=va;ゔぃ=vi;ゔ=vu;ゔぇ=ve;ゔぉ=vo";
        private static final String KATAKANA_TABLE = "ア=a;イ=i;ウ=u;エ=e;オ=o;カ=ka;キ=ki;ク=ku;ケ=ke;コ=ko;キャ=kya;キュ=kyu;キョ=kyo;サ=sa;シ=shi,si;ス=su;セ=se;ソ=so;シャ=sha,sya;シュ=shu,syu;ショ=sho,syo;タ=ta;チ=chi,ti;ツ=tsu,tu;テ=te;ト=to;チャ=cha;チュ=chu;チョ=cho;ナ=na;ニ=ni;ヌ=nu;ネ=ne;ノ=no;ニャ=nya;ニュ=nyu;ニョ=nyo;ハ=ha;ヒ=hi;フ=fu,hu;ヘ=he;ホ=ho;ヒャ=hya;ヒュ=hyu;ヒョ=hyo;マ=ma;ミ=mi;ム=mu;メ=me;モ=mo;ミャ=mya;ミュ=myu;ミョ=myo;ヤ=ya,xya,'ya;ユ=yu,xyu,'yu;ヨ=yo,xyo,'yo;ラ=ra;リ=ri;ル=ru;レ=re;ロ=ro;リャ=rya;リュ=ryu;リョ=ryo;ワ=wa;ヰ=wi;ヱ=we;ヲ=wo;ン=n,xn,'n;ガ=ga;ギ=gi;グ=gu;ゲ=ge;ゴ=go;ギャ=gya;ギュ=gyu;ギョ=gyo;ザ=za;ジ=ji,dži,zi;ズ=zu;ゼ=ze;ゾ=zo;ジャ=ja,jya;ジュ=ju;ジェ=je;ジョ=jo;ダ=da;ヂ=xji,di,dji;ヅ=xzu,du,dzu;デ=de;ド=do;ヂャ=xja;ヂュ=xju;ヂョ=xjo;バ=ba;ビ=bi;ブ=bu;ベ=be;ボ=bo;ビャ=bya;ビュ=byu;ビョ=byo;パ=pa;ピ=pi;プ=pu;ペ=pe;ポ=po;ピャ=pya;ピュ=pyu;ピョ=pyo;ァ=xa;ィ=xi;ゥ=xu;ェ=xe;ォ=xo;ファ=fa;フィ=fi;フェ=fe;フォ=fo;ヴァ=va;ヴィ=vi;ヴ=vu;ヴェ=ve;ヴォ=vo";
        private static final RomanizationTable TABLE = new RomanizationTable(HIRAGANA_TABLE, KATAKANA_TABLE);
        public static final Set<Character> HIRAGANA_CHARACTERS = Collections.unmodifiableSet(RomanizationTable.getKanaChars(TABLE.hiraganaToRomaji.keySet()));
        public static final Set<Character> KATAKANA_CHARACTERS = Collections.unmodifiableSet(RomanizationTable.getKanaChars(TABLE.katakanaToRomaji.keySet()));
        public static final Set<String> HIRAGANA = Collections.unmodifiableSet(TABLE.hiraganaToRomaji.keySet());
        public static final Set<String> KATAKANA = Collections.unmodifiableSet(TABLE.katakanaToRomaji.keySet());

        @Override
		@NotNull
		protected RomanizationTable getRomanizationTable() {
            return TABLE;
        }
    }

    /**
     * The Hepburn romanization instance.
     */
    IRomanization HEPBURN = new Hepburn();

    /**
     * The Nihon-Shiki romanization.
     */
    final class NihonShiki extends AbstractTableBasedRomanization {
        private static final String HIRAGANA_TABLE = "あ=a;い=i;う=u;え=e;お=o;か=ka;き=ki;く=ku;け=ke;こ=ko;きゃ=kya;きゅ=kyu;きょ=kyo;さ=sa;し=si,shi;す=su;せ=se;そ=so;しゃ=sya,sha;しゅ=syu,shu;しょ=syo,sho;た=ta;ち=ti,chi;つ=tu,tsu;て=te;と=to;ちゃ=tya;ちゅ=tyu;ちょ=tyo;な=na;に=ni;ぬ=nu;ね=ne;の=no;にゃ=nya;にゅ=nyu;にょ=nyo;は=ha;ひ=hi;ふ=hu,fu;へ=he;ほ=ho;ひゃ=hya;ひゅ=hyu;ひょ=hyo;ま=ma;み=mi;む=mu;め=me;も=mo;みゃ=mya;みゅ=myu;みょ=myo;や=ya,xya,'ya;ゆ=yu,xyu,'yu;よ=yo,xyo,'yo;ら=ra;り=ri;る=ru;れ=re;ろ=ro;りゃ=rya;りゅ=ryu;りょ=ryo;わ=wa;ゐ=wi;ゑ=we;を=wo;ん=n,xn;が=ga;ぎ=gi;ぐ=gu;げ=ge;ご=go;ぎゃ=gya;ぎゅ=gyu;ぎょ=gyo;ざ=za;じ=zi,ji;ず=zu;ぜ=ze;ぞ=zo;じゃ=zya,jya;じゅ=zyu;じぇ=zye,je;じょ=zyo;だ=da;ぢ=di,dji;づ=du,dzu;で=de;ど=do;ぢゃ=dya;ぢゅ=dyu;ぢょ=dyo;ば=ba;び=bi;ぶ=bu;べ=be;ぼ=bo;びゃ=bya;びゅ=byu;びょ=byo;ぱ=pa;ぴ=pi;ぷ=pu;ぺ=pe;ぽ=po;ぴゃ=pya;ぴゅ=pyu;ぴょ=pyo;くゎ=kwa;ぐゎ=gwa;ぁ=xa;ぃ=xi;ぅ=xu;ぇ=xe;ぉ=xo;ふぁ=fa;ふぃ=fi;ふぇ=fe;ふぉ=fo;ゔぁ=va;ゔぃ=vi;ゔ=vu;ゔぇ=ve;ゔぉ=vo";
        private static final String KATAKANA_TABLE = "ア=a;イ=i;ウ=u;エ=e;オ=o;カ=ka;キ=ki;ク=ku;ケ=ke;コ=ko;キャ=kya;キュ=kyu;キョ=kyo;サ=sa;シ=si,shi;ス=su;セ=se;ソ=so;シャ=sya,sha;シュ=syu,shu;ショ=syo,sho;タ=ta;チ=ti,chi;ツ=tu,tsu;テ=te;ト=to;チャ=tya;チュ=tyu;チョ=tyo;ナ=na;ニ=ni;ヌ=nu;ネ=ne;ノ=no;ニャ=nya;ニュ=nyu;ニョ=nyo;ハ=ha;ヒ=hi;フ=hu,fu;ヘ=he;ホ=ho;ヒャ=hya;ヒュ=hyu;ヒョ=hyo;マ=ma;ミ=mi;ム=mu;メ=me;モ=mo;ミャ=mya;ミュ=myu;ミョ=myo;ヤ=ya,xya,'ya;ユ=yu,xyu,'yu;ヨ=yo,xyo,'yo;ラ=ra;リ=ri;ル=ru;レ=re;ロ=ro;リャ=rya;リュ=ryu;リョ=ryo;ワ=wa;ヰ=wi;ヱ=we;ヲ=wo;ン=n,xn;ガ=ga;ギ=gi;グ=gu;ゲ=ge;ゴ=go;ギャ=gya;ギュ=gyu;ギョ=gyo;ザ=za;ジ=zi,ji;ズ=zu;ゼ=ze;ゾ=zo;ジャ=zya,jya;ジュ=zyu;ジェ=zye,je;ジョ=zyo;ダ=da;ヂ=di,dji;ヅ=du,dzu;デ=de;ド=do;ヂャ=dya;ヂュ=dyu;ヂョ=dyo;バ=ba;ビ=bi;ブ=bu;ベ=be;ボ=bo;ビャ=bya;ビュ=byu;ビョ=byo;パ=pa;ピ=pi;プ=pu;ペ=pe;ポ=po;ピャ=pya;ピュ=pyu;ピョ=pyo;ァ=xa;ィ=xi;ゥ=xu;ェ=xe;ォ=xo;ファ=fa;フィ=fi;フェ=fe;フォ=fo;ヴァ=va;ヴィ=vi;ヴ=vu;ヴェ=ve;ヴォ=vo";
        private static final RomanizationTable TABLE = new RomanizationTable(HIRAGANA_TABLE, KATAKANA_TABLE);

        @Override
		@NotNull
		protected RomanizationTable getRomanizationTable() {
            return TABLE;
        }
    }

    /**
     * The Nihon-Shiki instance.
     */
    IRomanization NIHON_SHIKI = new NihonShiki();

    /**
     * The Kunrei-Shiki romanization, see http://en.wikipedia.org/wiki/Kunrei-shiki_romanization for details
     */
    final class KunreiShiki extends AbstractTableBasedRomanization {
        private static final String HIRAGANA_TABLE = "あ=a;い=i;う=u;え=e;お=o;か=ka;き=ki;く=ku;け=ke;こ=ko;きゃ=kya;きゅ=kyu;きょ=kyo;さ=sa;し=si,shi;す=su;せ=se;そ=so;しゃ=sya,sha;しゅ=syu,shu;しょ=syo,sho;た=ta;ち=ti,chi;つ=tu,tsu;て=te;と=to;ちゃ=tya;ちゅ=tyu;ちょ=tyo;な=na;に=ni;ぬ=nu;ね=ne;の=no;にゃ=nya;にゅ=nyu;にょ=nyo;は=ha;ひ=hi;ふ=hu,fu;へ=he;ほ=ho;ひゃ=hya;ひゅ=hyu;ひょ=hyo;ま=ma;み=mi;む=mu;め=me;も=mo;みゃ=mya;みゅ=myu;みょ=myo;や=ya,xya,'ya;ゆ=yu,xyu,'yu;よ=yo,xyo,'yo;ら=ra;り=ri;る=ru;れ=re;ろ=ro;りゃ=rya;りゅ=ryu;りょ=ryo;わ=wa;ゐ=wi,xi;ゑ=we,xe;を=wo,xo;ん=n,xn;が=ga;ぎ=gi;ぐ=gu;げ=ge;ご=go;ぎゃ=gya;ぎゅ=gyu;ぎょ=gyo;ざ=za;じ=zi,ji;ず=zu;ぜ=ze;ぞ=zo;じゃ=zya,jya;じゅ=zyu;じぇ=zye,je;じょ=zyo;だ=da;ぢ=xzi,di,dji;づ=xzu,du,dzu;で=de;ど=do;ぢゃ=xzya;ぢゅ=xzyu;ぢょ=xzyo;ば=ba;び=bi;ぶ=bu;べ=be;ぼ=bo;びゃ=bya;びゅ=byu;びょ=byo;ぱ=pa;ぴ=pi;ぷ=pu;ぺ=pe;ぽ=po;ぴゃ=pya;ぴゅ=pyu;ぴょ=pyo;くゎ=kwa;ぐゎ=gwa;ぁ=xxa;ぃ=xxi;ぅ=xxu;ぇ=xxe;ぉ=xxo;ふぁ=fa;ふぃ=fi;ふぇ=fe;ふぉ=fo;ゔぁ=va;ゔぃ=vi;ゔ=vu;ゔぇ=ve;ゔぉ=vo";
        private static final String KATAKANA_TABLE = "ア=a;イ=i;ウ=u;エ=e;オ=o;カ=ka;キ=ki;ク=ku;ケ=ke;コ=ko;キャ=kya;キュ=kyu;キョ=kyo;サ=sa;シ=si,shi;ス=su;セ=se;ソ=so;シャ=sya,sha;シュ=syu,shu;ショ=syo,sho;タ=ta;チ=ti,chi;ツ=tu,tsu;テ=te;ト=to;チャ=tya;チュ=tyu;チョ=tyo;ナ=na;ニ=ni;ヌ=nu;ネ=ne;ノ=no;ニャ=nya;ニュ=nyu;ニョ=nyo;ハ=ha;ヒ=hi;フ=hu,fu;ヘ=he;ホ=ho;ヒャ=hya;ヒュ=hyu;ヒョ=hyo;マ=ma;ミ=mi;ム=mu;メ=me;モ=mo;ミャ=mya;ミュ=myu;ミョ=myo;ヤ=ya,xya,'ya;ユ=yu,xyu,'yu;ヨ=yo,xyo,'yo;ラ=ra;リ=ri;ル=ru;レ=re;ロ=ro;リャ=rya;リュ=ryu;リョ=ryo;ワ=wa;ヰ=wi,xi;ヱ=we,xe;ヲ=wo,xo;ン=n,xn;ガ=ga;ギ=gi;グ=gu;ゲ=ge;ゴ=go;ギャ=gya;ギュ=gyu;ギョ=gyo;ザ=za;ジ=zi,ji;ズ=zu;ゼ=ze;ゾ=zo;ジャ=zya,jya;ジュ=zyu;ジェ=zye,je;ジョ=zyo;ダ=da;ヂ=xzi,di,dji;ヅ=xzu,du,dzu;デ=de;ド=do;ヂャ=xzya;ヂュ=xzyu;ヂョ=xzyo;バ=ba;ビ=bi;ブ=bu;ベ=be;ボ=bo;ビャ=bya;ビュ=byu;ビョ=byo;パ=pa;ピ=pi;プ=pu;ペ=pe;ポ=po;ピャ=pya;ピュ=pyu;ピョ=pyo;ァ=xxa;ィ=xxi;ゥ=xxu;ェ=xxe;ォ=xxo;ファ=fa;フィ=fi;フェ=fe;フォ=fo;ヴァ=va;ヴィ=vi;ヴ=vu;ヴェ=ve;ヴォ=vo";
        private static final RomanizationTable TABLE = new RomanizationTable(HIRAGANA_TABLE, KATAKANA_TABLE);

        @Override
		@NotNull
		protected RomanizationTable getRomanizationTable() {
            return TABLE;
        }
    }

    /**
     * The Kunrei-Shiki romanization instance.
     */
    IRomanization KUNREI_SHIKI = new KunreiShiki();

    /**
     * Does not perform any romanization.
     */
    class Identity implements IRomanization {

        @Override
		@NotNull
		public String toHiragana(@NotNull String romaji) {
            return romaji;
        }

        @Override
		@NotNull
		public String toKatakana(@NotNull String romaji) {
            return romaji;
        }

        @Override
		@NotNull
		public String toRomaji(char hiraganaOrKatakana) {
            return String.valueOf(hiraganaOrKatakana);
        }

        @Override
		@NotNull
		public String toRomaji(@NotNull String hiraganaOrKatakana) {
            return hiraganaOrKatakana;
        }

		@NotNull
		@Override
		public String toRomaji(@NotNull String hiraganaOrKatakana, boolean useX) {
			return hiraganaOrKatakana;
		}

		@Override
		@NotNull
		public String getWriting(@NotNull String kana) {
            return kana;
        }

        @Override
        public boolean containsUntranslatableRomaji(@NotNull String romaji) {
            return false;
        }

        @Nullable
        @Override
        public TransformResult getFirstRomajiSyllabogramAsHiragana(@NotNull String string) {
            return string.length() == 0 ? null : new TransformResult(string.substring(0, 1), 1);
        }

        @Nullable
        @Override
        public TransformResult getFirstRomajiSyllabogramAsKatakana(@NotNull String string) {
            return getFirstRomajiSyllabogramAsHiragana(string);
        }

        @Nullable
        @Override
        public TransformResult getFirstKanaSyllabogramAsRomaji(@NotNull String string) {
            return getFirstRomajiSyllabogramAsHiragana(string);
        }
    }

    /**
     * Identity instance.
     */
    IRomanization IDENTITY = new Identity();
}
