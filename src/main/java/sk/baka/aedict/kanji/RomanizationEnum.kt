/**
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.kanji

/**
 * Romanizations.
 * @author Martin Vysny
 */
enum class RomanizationEnum(@JvmField val r: IRomanization) {

    /**
     * The Hepburn romanization.
     */
    Hepburn(IRomanization.HEPBURN),
    /**
     * The Nihon-Shiki romanization.
     */
    NihonShiki(IRomanization.NIHON_SHIKI),

    KunreiShiki(IRomanization.KUNREI_SHIKI),

    /**
     * No romanization, pozri https://code.google.com/p/aedict/issues/detail?id=347
     */
    NoRomanization(IRomanization.IDENTITY)
}
