/**
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
@file:Suppress("NAME_SHADOWING")

package sk.baka.aedict.kanji

import java.io.Serializable

import sk.baka.aedict.util.BitSet
import sk.baka.aedict.util.Boxable
import sk.baka.aedict.util.Unboxable
import sk.baka.aedict.util.typedmap.Box

/**
 * Pitch accent. Data taken from https://github.com/javdejong/nhk-pronunciation
 * Find more info on pronounciation here: https://en.wikipedia.org/wiki/Japanese_pitch_accent
 * @property reading Hiragana/katakana reading.
 * @property pitchData Do not modify, do not use for any other purposes than boxing!
 * @author mvy
 */
data class PitchAccent(val reading: String, val pitchData: PitchData) : Serializable {
    /**
     * Returns the pitch at given character: 0, 1 or 2.
     * @param index character index
     * @return 0, 1 or 2.
     */
    fun getPitch(index: Int): Byte = pitchData.getPitch(index, reading.length)

    fun isNasal(index: Int): Boolean = pitchData.isNasal(index, reading.length)

    override fun toString() = pitchData.toString(reading)

    companion object {

        /**
         * Parses the NHK_pronounciation data: https://github.com/javdejong/nhk-pronunciation
         * @param reading the word reading, katakana or hiragana. the "midashigo1" column
         * @param nasalSoundPos the "nasalsoundpos" column
         * @param ac the "ac" column, basically looks like "111200"
         * @return parsed pitch accent, not null.
         */
        fun parse(reading: String, nasalSoundPos: String, ac: String): PitchAccent {

            data class PitchAccentChar(var reading: Char, var nasal: Boolean = false, var pitch: Byte = 0)

            var chars: List<PitchAccentChar> = reading.trim().map { PitchAccentChar(it) }
            var ac = ac.trim()
            require(chars.size >= ac.length) { "Parameter ac: invalid value $ac: longer than $reading" }
            // pad 'ac' for the length of reading
            ac = ac.padStart(reading.length, '0')
            ac.forEachIndexed { index, c -> chars[index].pitch = (c - '0').toByte() }

            if (nasalSoundPos.isNotBlank()) {
                val poss = nasalSoundPos.trim().split('0').filter { it.isNotBlank() }
                for (i in poss.indices) {
                    var pos = poss[i].toInt()
                    if (i == poss.size - 1 && nasalSoundPos.endsWith("0")) {
                        pos *= 10
                    }
                    chars[pos - 1].nasal = true
                }
            }

            // I have to remove ・
            // since in JMDict there is no such entry: カレン・チューキュー/苛斂誅求
            // it's present in JMDict without the dot.
            chars = chars.filter { it.reading != '・' }

            // the end: convert it to pitch data
            val data = PitchData(BitSet())
            for (i in 0 until chars.size) {
                data.setPitch(i, chars.size, chars[i].pitch)
                data.setNasal(i, chars.size, chars[i].nasal)
            }
            return PitchAccent(chars.map { it.reading } .joinToString(""), data)
        }
    }
}

/**
 * @property pitchNasal Packed pitch + nasal info. See [getPitch] and [isNasal] for unpack info.
 */
data class PitchData(private val pitchNasal: BitSet) : Serializable, Boxable {

    /**
     * Returns the pitch at given character: 0, 1 or 2.
     * @param index character index
     * @param wordLength the word length
     * @return 0, 1 or 2.
     */
    fun getPitch(index: Int, wordLength: Int): Byte {
        require(index in 0..(wordLength - 1)) { "Parameter index: invalid value $index: must be 0..${wordLength - 1}" }
        return when {
            pitchNasal.get(index + wordLength) -> 2
            pitchNasal.get(index) -> 1
            else -> 0
        }
    }

    internal fun setPitch(index: Int, wordLength: Int, pitch: Byte) {
        require(index in 0..(wordLength - 1)) { "Parameter index: invalid value $index: must be 0..$wordLength" }
        require(pitch in 0..2) {"Parameter pitch: invalid value $pitch: must be 0..2" }
        when (pitch.toInt()) {
            2 -> pitchNasal.set(wordLength + index)
            1 -> {
                pitchNasal.clear(wordLength + index)
                pitchNasal.set(index)
            }
            else -> {
                pitchNasal.clear(wordLength + index)
                pitchNasal.clear(index)
            }
        }
        if (getPitch(index, wordLength) != pitch) {
            throw RuntimeException("bug in code")
        }
    }

    fun isNasal(index: Int, wordLength: Int): Boolean {
        require(index in 0..(wordLength - 1)) { "Parameter index: invalid value $index: must be 0..${wordLength - 1}" }
        return pitchNasal.get(wordLength * 2 + index)
    }

    internal fun setNasal(index: Int, wordLength: Int, nasal: Boolean) {
        require(index in 0..(wordLength - 1)) { "Parameter index: invalid value $index: must be 0..${wordLength - 1}" }
        if (nasal) {
            pitchNasal.set(wordLength * 2 + index)
        } else {
            pitchNasal.clear(wordLength * 2 + index)
        }
    }

    fun toString(reading: String): String {
        val length = reading.length
        val sb = StringBuilder(length + 4)
        var overlined = false
        for (i in 0 until length) {
            val pitch = getPitch(i, length)
            if (pitch.toInt() != 0 && !overlined) {
                sb.append("ꜛ")
                overlined = true
            }
            if (pitch.toInt() == 0 && overlined) {
                overlined = false
            }
            sb.append(reading[i])
            if (isNasal(i, length)) {
                sb.append(nasalMarker)
            }
            if (pitch.toInt() == 2) {
                sb.append('ꜜ')
                overlined = false
            }
        }
        return sb.toString()
    }

    override fun box(): Box = Box().putBitSet("pitchNasal", pitchNasal)

    companion object : Unboxable<PitchData> {

        @JvmStatic
        override fun unbox(box: Box) = PitchData(box.get("pitchNasal").bitSet().get())
    }
}

/**
 * Used to be °, now it's ~
 *
 * https://github.com/mvysny/aedict/issues/924
 *
 * Don't change unless the change is also performed in Aedict/preferences.xml
 */
val nasalMarker:Char = '~'
