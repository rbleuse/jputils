/**
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.kanji;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sk.baka.aedict.util.MiscUtils;

import java.util.*;

/**
 * Lists all parts of any given kanji. For a list of radicals see [Radicals2].
 * @author Martin Vysny
 */
public final class Parts {
    /**
     * Returns base36-encoded unicode code of given part. Fails if given character is not a part.
     * @param part the part
     * @return part base36 encoded unicode code, e.g. ef5
     */
	@NotNull
	public static String getOrdinal(char part) {
        if (part == '｜') {
            throw new IllegalArgumentException("Parameter part: invalid value " + part + ": not a part");
        }
		// @todo mvy compatibility hack, safe to remove after 1.12.2014
//		if (part != '邦' && part != '阡') {
			if (!PART_TO_KANJILIST.containsKey(part)) {
				throw new IllegalArgumentException("Parameter part: invalid value " + part + ": not a part");
			}
//		}
        return Integer.toString(part, Character.MAX_RADIX);
    }

    /**
     * Computes all available parts and kanjis for which I can perform a part analysis.
     * Essentially all parts and kanjis from {@link #PART_TO_KANJILIST}.
     * @return set
     */
    @NotNull
    public static Set<JpCharacter> getAllReferencedKanjis() {
        final Set<JpCharacter> set = new HashSet<>();
        for (Map.Entry<Character, Part> e : PART_TO_KANJILIST.entrySet()) {
            set.add(new JpCharacter(e.getKey()));
            for (Kanji k : e.getValue().kanjiSet) {
                set.add(k.toChar());
            }
        }
        return set;
    }

    /**
     * Denotes a kanji part.
     * @author Martin Vysny
     */
    public final static class Part {
        /**
         * number of strokes, 1 or greater.
         */
        public final int strokes;
        /**
         * Kanjis ktore obsahuju tento "part". Unmodifiable.
         */
		@NotNull
		private final Set<Kanji> kanjiSet;
        /**
         * Creates new Part object.
         * @param strokes number of strokes
         * @param kanjis a list of kanjis which contain this part.
         */
        private Part(int strokes, @NotNull String kanjis) {
            this.strokes = strokes;
            this.kanjiSet = Collections.unmodifiableSet(toSet(kanjis));
        }
		@NotNull
		private static Set<Kanji> toSet(@NotNull final String str) {
            final Set<Kanji> result = new HashSet<Kanji>(str.length());
            for (final String c : MiscUtils.getCodePoints(str)) {
                if (c.equals("マ") || c.equals("ユ")) {
                    continue;
                }
                result.add(new Kanji(c));
            }
            return result;
        }
    }

    /**
     * Returns parts for given kanji.
     * @param kanji the kanji to analyze
     * @return a part list, never null, may be empty if unknown kanji is supplied. May contain the original kanji.
     * Nemoze byt set of Kanji - moze obsahovat znaky ako マ
     */
	@NotNull
	static Set<JpCharacter> getParts(@NotNull Kanji kanji) {
        if (kanji.kanji().length() > 1) {
            return Collections.emptySet();
        }
        final Set<JpCharacter> parts = new LinkedHashSet<JpCharacter>();
        for(final Map.Entry<Character,Part> r: PART_TO_KANJILIST.entrySet()){
            if(r.getValue().kanjiSet.contains(kanji)){
                parts.add(new JpCharacter(r.getKey()));
            }
        }
        return parts;
    }

    /**
     * Returns all kanjis which contains given kanji part.
     * @param part the part
     * @return list of all kanjis which contains all given part. Never null,
     *         may be empty.
     * @throws java.lang.IllegalArgumentException if given character is not a valid part character.
     */
	@NotNull
	public static Set<Kanji> getKanjisWithPart(char part) {
        final Part p = getPart(part);
        if (p == null) {
            throw new IllegalArgumentException("Parameter part: invalid value " + part + ": not a part");
        }
        return Collections.unmodifiableSet(p.kanjiSet);
    }
    /**
     * Returns all kanjis which contains all given parts.
     * @param parts
     *            a list of parts, must not be null.
     * @return list of all kanjis which contains all given parts. Never null,
     *         may be empty.
     */
	@NotNull
	public static Set<Kanji> getKanjisWithParts(@NotNull final char[] parts) {
        if (parts.length == 1) {
            return getKanjisWithPart(parts[0]);
        }
        Set<Kanji> matchedKanjis = null;
        for (final char part : parts) {
            Set<Kanji> kanjis = getKanjisWithPart(part);
            if (matchedKanjis == null) {
                matchedKanjis = new HashSet<Kanji>(kanjis);
            } else {
                matchedKanjis.retainAll(kanjis);
            }
        }
        return matchedKanjis == null ? Collections.<Kanji>emptySet() : matchedKanjis;
    }

    /**
     * Finds information about a kanji part.
     * @param part the "part" character.
     * @return the "part" object or null if given character is not a "part" character.
     */
	@Nullable
	public static Part getPart(final char part) {
        return PART_TO_KANJILIST.get(part);
    }

    /**
     * Checks whether given character is a kanji part (present in {@link #PART_ORDERING}).
     * @param part the part, not null
     * @return true if given character is a kanji part, false if not.
     */
    public static boolean isPart(final char part) {
        return getPart(part) != null;
    }

    /**
     * Enumerates all parts.
     */
    public final static String PART_ORDERING = "一丨丶丿乙亅二亠人亻个儿入八并冂冖冫几凵刀刂力勹匕匚十卜卩厂厶又マ九ユ乃辶込口囗土士夂夕大女子宀寸小尚尢尸屮山川巛工已巾干幺广廴廾弋弓彐彑彡彳忄扌氵犭艾阝也亡及久老心戈戸手支攵文斗斤方无日曰月木欠止歹殳比毛氏气水火灬爪父爻爿片牛犬礻王元井勿尤五屯巴毋玄瓦甘生用田疋疒癶白皮皿目矛矢石示禸禾穴立衤世巨冊母罒牙瓜竹米糸缶羊羽而耒耳聿肉自至臼舌舟艮色虍虫血行衣西臣見角言谷豆豕豸貝赤走足身車辛辰酉釆里舛麦金長門隶隹雨青非奄岡免斉面革韭音頁風飛食首香品馬骨高髟鬥鬯鬲鬼竜韋魚鳥鹵鹿麻亀滴黄黒黍黹無歯黽鼎鼓鼠鼻齊龠";
    static {
        for (int i = 0; i < PART_ORDERING.length(); i++) {
            verifyPart(PART_ORDERING.charAt(i));
        }
    }
    private static void verifyPart(char kanji) {
        if (kanji == 'マ' || kanji == 'ユ') {
            // accepted
            return;
        }
        new Kanji(kanji);  // verify that all parts are kanjis
    }
    private final static Map<Character, Part> PART_TO_KANJILIST = new HashMap<Character, Part>();
    private static final Logger log = LoggerFactory.getLogger(Parts.class);

    // THIS PART PRODUCED BY THE CLASS ComputeParts
    // AUTOGENERATED, DO NOT EDIT
    static {
        long start = System.currentTimeMillis();
        PART_TO_KANJILIST.put('退', new Part(1, "退褪腿"));
        PART_TO_KANJILIST.put('老', new Part(4, "老考耄者耆躇耇耈耊耋瘏鬐暑漖搘渚褚誟銠瀦撦螧庨覩猪鰭薯宯鐯鮱栲儲栳堵踷墸箸咾侾帾殾闍蓍蕏緒偖荖著曙教嗜廜孝屠奢硣烤姥翥櫧鱪豬佬賭赭楮煮哮蛯鱰署奲潴酵拷藷諸睹鍺鯺陼都"));
        PART_TO_KANJILIST.put('頁', new Part(9, "頁頂頃項順頇須頊蘋頌頍頎頏預頑頒夒頓夓頔瀕頖頗領頙鬚頚頜頞頠頡頣頤頥頦頫瀬頬頭頮頯頰頲頳頴頵頷頸頻頼頽嬾頾潁顄顆顇顊顋題額穎顎顏湏顑顒顓顔顕顖顗願顙顚顛灝類蹞籟顢顣顥顦顧癩煩顪顫顬襭顯顰顱癲籲顳顴蕷噸獺嵿澃纇纈纐澒薠龥澦粨碩嶺嚬熲領嶺傾囂巎廎巓寘盨懶擷藾"));
        PART_TO_KANJILIST.put('送', new Part(1, "送鎹"));
        PART_TO_KANJILIST.put('耂', new Part(1, "老耂考耄者耆躇耇耈耊耋瘏鬐暑漖搘渚褚誟銠瀦撦螧庨覩猪鰭薯宯鐯鮱栲儲栳堵踷墸箸咾侾帾殾闍蓍蕏緒偖荖著曙教嗜廜孝屠奢硣烤姥翥櫧鱪豬佬賭赭楮煮哮蛯鱰署奲潴酵拷藷諸睹鍺鯺陼都"));
        PART_TO_KANJILIST.put('堂', new Part(1, "瞠堂螳"));
        PART_TO_KANJILIST.put('考', new Part(1, "栲考拷"));
        PART_TO_KANJILIST.put('頃', new Part(1, "潁頃頴傾穎"));
        PART_TO_KANJILIST.put('者', new Part(1, "者躇闍暑緒偖著曙渚屠奢瀦覩猪豬賭赭楮煮薯儲署潴堵藷墸箸諸睹都"));
        PART_TO_KANJILIST.put('堅', new Part(1, "慳堅鏗鰹樫"));
        PART_TO_KANJILIST.put('耆', new Part(1, "耆嗜蓍鰭"));
        PART_TO_KANJILIST.put('須', new Part(1, "須鬚"));
        PART_TO_KANJILIST.put('倉', new Part(1, "瘡愴滄創搶鎗倉艙蒼蹌槍"));
        PART_TO_KANJILIST.put('而', new Part(6, "需輀嚅而圌耍褍耎洏耏耐耑儒貒蠕喘碝愞鮞堧粫嬬栭輭猯鴯薷踹檽遄篅湍臑顓壖煗轜瑞濡蝡揣懦襦恧腨擩顬糯端惴鍴髵燸胹孺繻陾"));
        PART_TO_KANJILIST.put('預', new Part(1, "預蕷"));
        PART_TO_KANJILIST.put('耑', new Part(1, "耑端"));
        PART_TO_KANJILIST.put('耒', new Part(6, "誄棅藉籍耒耓耔耕藕耖耗耘耙耜耝耞耟耠耡耤耦耨耬業耮耰"));
        PART_TO_KANJILIST.put('頓', new Part(1, "頓噸"));
        PART_TO_KANJILIST.put('栗', new Part(1, "慄篥栗"));
        PART_TO_KANJILIST.put('領', new Part(1, "領領嶺嶺"));
        PART_TO_KANJILIST.put('通', new Part(1, "通樋"));
        PART_TO_KANJILIST.put('思', new Part(1, "偲鑢鰓顋思慮濾腮"));
        PART_TO_KANJILIST.put('造', new Part(1, "造慥"));
        PART_TO_KANJILIST.put('頡', new Part(1, "頡纈襭"));
        PART_TO_KANJILIST.put('逢', new Part(1, "逢篷縫蓬"));
        PART_TO_KANJILIST.put('連', new Part(1, "連漣鏈嗹縺蓮"));
        PART_TO_KANJILIST.put('逮', new Part(1, "靆逮"));
        PART_TO_KANJILIST.put('堯', new Part(1, "嶢鐃僥澆橈曉驍堯蟯燒饒撓遶蕘翹磽繞"));
        PART_TO_KANJILIST.put('怱', new Part(1, "怱愡葱偬"));
        PART_TO_KANJILIST.put('耳', new Part(6, "最欇欉餌輒輙攝戢褧鬫嘬弭輯椰洱耳耴娵刵耵樶耶娶樷耷茸耹鄹爺耺耻倻儼耼耽耾耿聀摂橄灄聄聆鵈衈噉聊聒湒聖聘聚穝聞蕞驟聟聠聡敢聢聤恥聦聨楫陬聭聯聰聱齱聲聳顳聴聵襵聶鑷職聹葺蕺聽聾誀玁鎁檉斊麛檝躡咡趣冣憨掫撮聆瞰厳嚴銸鮿囁藂緅釅濈巌諏擑巖取揖矙緝闞叢珥毦鯫駬曮廰郰廳揶蟶懾埾諿"));
        PART_TO_KANJILIST.put('耶', new Part(1, "椰耶揶爺"));
        PART_TO_KANJILIST.put('頻', new Part(1, "顰瀕頻蘋"));
        PART_TO_KANJILIST.put('頼', new Part(1, "懶癩獺頼瀬嬾藾籟"));
        PART_TO_KANJILIST.put('血', new Part(6, "血衁衂衃恤衄衅衆衈衉賉衊洫衋侐卹睾"));
        PART_TO_KANJILIST.put('桀', new Part(1, "桀傑磔"));
        PART_TO_KANJILIST.put('遂', new Part(1, "遂邃燧隧"));
        PART_TO_KANJILIST.put('聆', new Part(1, "聆聆"));
        PART_TO_KANJILIST.put('行', new Part(6, "桁裄愆鵆荇行衍絎衎讏衑衒術筕衕衖街哘衘衙衚衛躛銜衜衝衞衟衠衡衢珩垳鴴"));
        PART_TO_KANJILIST.put('衍', new Part(1, "愆衍"));
        PART_TO_KANJILIST.put('道', new Part(1, "道導"));
        PART_TO_KANJILIST.put('達', new Part(1, "韃達燵闥撻"));
        PART_TO_KANJILIST.put('聖', new Part(1, "聖蟶"));
        PART_TO_KANJILIST.put('聚', new Part(1, "聚驟"));
        PART_TO_KANJILIST.put('顛', new Part(1, "癲巓顛"));
        PART_TO_KANJILIST.put('衣', new Part(6, "褁儂蜃蜄唇震儇蠉褎褏簑園褒簔縗攘漘蘘嬛圜褜嬝鬟縟娠嬢脣脤猥褥褧耨截攮振蠰褰褱椳儴戴餵鐵鐶錶褺褻瀼輾猿孃襃襄轅扆蹍偎畏敐展轘襛襞譞鱞驟饟遠穠衣穣驤獧表煨晨畩繯穰衰譲襲鑲衷鵺蕽衾莀袁膂喂還薅隈袈袋麎纎鎒讓纕薗依袞袠嚢袤銥碨喪禯環辰袰榱辱殱農侲醲禳嚷醸宸憹溽碾膿釀哀裁懁裂濃装囅槈壊囊裊壌瓌裏蓐懐擐蓑賑裒裔裘裛裝壞曟裟鋠壤瓤闤曩糫寰櫰裱裳裴俵裵懷勷裹製滾翾"));
        PART_TO_KANJILIST.put('遣', new Part(1, "遣鑓譴"));
        PART_TO_KANJILIST.put('恣', new Part(1, "恣懿"));
        PART_TO_KANJILIST.put('衤', new Part(5, "褂褄褆複褊褌褍褐褓褕褖褘褙褚初褝褞褠褥褦褨褪褫褲褵褶褸褹褾襀襁襂襅襆襉襌襍襏襒襖襗襚襛襜襟襠襡襢襣衤襤襦衩襪衫襫襭襮襯襰衱衲襳襴衵襵襷衹襺衻襻襼衽襽衿袀袂袍袒袖袗袘袙袚袛袜袟袢袨袪被袮袱袴袵袷袺袽袾袿裀裃裄裋裌裍裎裑裓裕裙補裞裡裧裨裯裰裱裲裷裸裼裾"));
        PART_TO_KANJILIST.put('表', new Part(1, "俵表"));
        PART_TO_KANJILIST.put('火', new Part(4, "爀瀅爆爈縈爉鰍爍爐倐蠑倓爓逖爗爚爛爝爟爤鈥阦爨萩樮氮耮瀯琰耿鑅癆塋剡恢偢瑩火湫灮灯灰灵灶灸詼灼災灾炅檆炆撈炉炊炎炒炔炕犖炖炗炘炙炛躞炤炫炬炭炮炯炱炳碳炴炷颷炸檾狄滅談嫈飈烊滎烑烓惔烔盔烕烖烘烙烜勞烟烤裧棪郯烱烺烽愀愁甃焃焅焆焇焋焌焔焙伙焚焜儝焞錟焠餤褧焫焭焯焰焱焸欻焼煁罃煅腅煆煇煉煊煋煌煐畑煒睒啖煖煗歘煙煚煜煠煢煤嵤煥煨煩鍬煬楸煹荻煽罽啾蕿熀熄熅澇熇膋熌熒熔醔熕鶖熚熛禜掞熠薠螢熢疢鎣憥鞦熨榮鶯熯熰熲熳嶸熺熾熿燀燁緂燃燄燈燉秋燋燌燎燐燒燓燔燖燗燙濚燚燜營燠淡燥燦燧揪燬燭燮毯痰燵燸燹燻燼菼燿"));
        PART_TO_KANJILIST.put('灬', new Part(4, "鰀鰂鰄鰆鰈瀉鰉鰊鰋鰌鰍鐎鰏鰐鰑鰒鰓鰔鰕鰖搗鰘鰙鰚鰛鰜鰞鰡鰢鰣鰤鰥鰦鰧鰨鰩鰪鰭鰮鰯鰰鰱鰲鰵鰶鰷鰹鰺鰻鰽鰾鱁鱃鱄鱅鱆鱇鱈鱉鱊灋鱎鱏鱐鱒鱓鱔鱖鱗鱘鱚鱛鱜鱝鱞鱟鱠衡塢鑣鱣瑦顦鱧塨鱨鱩鱪瑪鱫灬摭遮鱮顯鱰鱲鱵鱶鱷鱸鱻偽炁墌璑撚撝梟撡墨撨撫炰墲蒸点為碼颿烈裊烋壎烏糕壗烝僞鳥壥鳦鳧鳩鳫鳬鳰鳲鳳鳴島鳶磶鳷鳹烹礁鴂鴃焄鴆鴇鴈鴉焉鴋鴎焏贐鴑鴒甒鄔鴕贗鴗蔗鴘儘鴛儛鴜鴝鴞鴟礟無鄢鴣鄥蔦鴦焦鴨攩鴪鴫蔫鴬鴯儯鴰鴲鴳鴴儵然鴺鴻儻餻鴼鴽鴾鴿蔿鵁鵂鵃鵄鵅鵆鵇鵈蕉鵊煎鵐鵑鵓鵔煕鵙鵜鵝鵞煞鵟鵠鵡鵢鵣鵤鵥煦照鵩鵪蕪鵫鵬煮鵯鵰鵲鵶鵷鵺鵻鵼鵾慿鶃鶄鶆鶇熈鶉鶊熊薊覊嶋嶌鶍鶎鶏熏憑鶒鶓憔鶕嶕鶖鶗鶘熙鶚熟鶡禡喣鶤薦鶩冩鶪鶫薫鶬熬馬馭鶮憮馮醮鶯馰鶱馱熱鶲馲膲馳鶴膴馴鶵馵鶸鶹馹熹鶺醺馺鶻鶼馼醼馽鶿馿鷁駁鷂鷃燃駃鷄駄駅鷆駆鷇駈鷉駉鷊燋嗎藎鷏駐駑駒鷓駓鷔駔鷕駕燕臕鷖闖鷗駘鷙駙臙嗚鷚駚駛駜槝駝鷞駞凞鷟駟鷠駢鷥鷦鷧駧藨鷩駪鷫駫駬鷭駭鷮駮鷯鷰駰駱槱鷲駲鷳鷴凴駴駵鷸駸鷹駹鷺燻駻燼鷽駽鷾駾燾駿騁鸂騂舃騃戃騄騅鸇蘇爇鸊騋騌蘍鸎騎騏鸐騐鸑騑鸒騒験蘓舔鸕鸖騖鸙騙鸚蘚鸛鸜鸝鸞舞騞騠樢騢騣騤騧騨騫騭騮騰爲騳樵騵騶騷嘸騸蘸騾嘿驀驁驂驃驄癄橅驅驇驊驋驌穌驍噍驎繎驑驔驕驖驗驚驛驝驟蹠驢驤驥驩驪艪橪驫癬穭纁麃纆纒嚕隖隝嚥窯隰隲隳窵庶媽囌廌黑黒櫓黔黕默黙黛曛黜黝點黟黠廡盡嫣黤黥黧黨黬黭黮黯黰黱黲勲勳黴嫵黵黶嫶黷黸嬀漁嬝譌歍靎靏潙譙魚魛魞潟魡魣魥魦魨魪魫魬魭魮魯獯杰魳魴罵魵魷魸魹魿鮀鮃鮄鮅羆鮆鮇羈鮉鮊鮋讌鮍鮎鮏螐鮐鮑鮒鮓鮔羔鮖鮗鮚讜鮝螞鮞鮟鮠鮦鮧瞧鮨鮩鮪鮫鮬鮭掭鮮羮鮰鮱鮲鮴鮷鮸鮹羹鮻鮼鮾鮿鯀鯁叅韅鯆鯇鯈鯉韉鯊鯎鯏鯐鯑鯒鯔濕鯖鯗鯘鯛鯝鯟鯡鯢鯣篤鯤鯥鯧鯨鯪鯫寫蟭鯯鯰蟱鯱鯲鯳鯵篶鯷鯸濹鯹鯺鯽鯿"));
        PART_TO_KANJILIST.put('息', new Part(1, "熄憩息"));
        PART_TO_KANJILIST.put('衰', new Part(1, "衰榱簑蓑簔"));
        PART_TO_KANJILIST.put('灰', new Part(1, "灰恢詼炭"));
        PART_TO_KANJILIST.put('聴', new Part(1, "廰聴"));
        PART_TO_KANJILIST.put('恵', new Part(1, "穂恵"));
        PART_TO_KANJILIST.put('聶', new Part(1, "囁躡摂顳聶鑷攝懾"));
        PART_TO_KANJILIST.put('遷', new Part(1, "韆遷"));
        PART_TO_KANJILIST.put('桼', new Part(1, "漆桼"));
        PART_TO_KANJILIST.put('聽', new Part(1, "廳聽"));
        PART_TO_KANJILIST.put('聿', new Part(6, "肁劃肄肅肆肇肈銉律犍贐侓圕鄘儘粛瀟茟徤津簫鞬傭嘯庸箻冿嵂牅鱅筆衋繍葎葏珒旔壗楗晝鏞盡健畫蕭腱慵鍵揵書建燼滽聿"));
        PART_TO_KANJILIST.put('袁', new Part(1, "遠袁園轅薗猿"));
        PART_TO_KANJILIST.put('梁', new Part(1, "梁簗"));
        PART_TO_KANJILIST.put('肅', new Part(1, "肅簫蕭瀟嘯"));
        PART_TO_KANJILIST.put('肉', new Part(6, "腐臠膐臡胔瘸肉哊臋胾"));
        PART_TO_KANJILIST.put('悉', new Part(1, "悉蟋"));
        PART_TO_KANJILIST.put('肋', new Part(1, "肋筋"));
        PART_TO_KANJILIST.put('炎', new Part(1, "痰淡餤啖談炎毯"));
        PART_TO_KANJILIST.put('邑', new Part(1, "癰邑廱悒扈滬"));
        PART_TO_KANJILIST.put('肖', new Part(1, "梢悄霄哨消削峭稍逍屑宵肖銷蛸鞘趙鮹誚硝"));
        PART_TO_KANJILIST.put('肙', new Part(1, "羂肙絹"));
        PART_TO_KANJILIST.put('備', new Part(1, "備憊"));
        PART_TO_KANJILIST.put('條', new Part(1, "篠滌條"));
        PART_TO_KANJILIST.put('袞', new Part(1, "袞滾"));
        PART_TO_KANJILIST.put('那', new Part(1, "那梛娜"));
        PART_TO_KANJILIST.put('風', new Part(9, "飂飃飄飅飆飈瘋飌嵐偑楓渢風颪颫繭颭颮颯颰颱颴颶諷颷颸颺颻颿"));
        PART_TO_KANJILIST.put('墨', new Part(1, "纒墨"));
        PART_TO_KANJILIST.put('育', new Part(1, "育撤徹轍"));
        PART_TO_KANJILIST.put('肴', new Part(1, "肴淆"));
        PART_TO_KANJILIST.put('為', new Part(1, "為偽"));
        PART_TO_KANJILIST.put('裁', new Part(1, "裁殱戴鐵截纎"));
        PART_TO_KANJILIST.put('胃', new Part(1, "謂胃膚渭喟蝟"));
        PART_TO_KANJILIST.put('僉', new Part(1, "斂檢剣僉儉臉險劍嶮剱瀲劒匳験劔驗鹸倹険歛検瞼簽"));
        PART_TO_KANJILIST.put('郎', new Part(1, "螂榔廊郎瑯"));
        PART_TO_KANJILIST.put('裏', new Part(1, "滾袞裏"));
        PART_TO_KANJILIST.put('烏', new Part(1, "塢嗚烏"));
        PART_TO_KANJILIST.put('僕', new Part(1, "僕濮"));
        PART_TO_KANJILIST.put('棘', new Part(1, "蕀棘"));
        PART_TO_KANJILIST.put('飛', new Part(9, "飛飜"));
        PART_TO_KANJILIST.put('烝', new Part(1, "蒸烝"));
        PART_TO_KANJILIST.put('マ', new Part(2, "伃氄茅踊樋鐍預愑舒鴒紓騖儗愗領礙通霚怜刢昤霧甬笭輮冷猱霱舲踴鈴伶慂蹂詅聆葇鱊譎潏衑鍒繘橘楙獝癡齡齢魣呤蝥湧筩鍪豫桶蕷遹婺楺杼彾恿魿劀瞀亂墅予涌序疑抒璚羚閝鞣袤妤誦澦芧令鶩澪嶺辭羚聆鈴玲零領覶嶷冷岺嶺薿銿柃壄糅勇埇勈姈揉蛉蟊懋野拎俑鯒苓柔務痛矛竛矜凝懝マ矞矟矠泠秢令軨蓪忬擬櫲髳瓴零鷸蛹囹雺"));
        PART_TO_KANJILIST.put('食', new Part(9, "瀁饂餃餅饅餉饉養饋餌饌餐饐饑餒饒餓餔蝕饕饗餘餝餞食餠餡癢飢餤館飩飫餬飭飮餮飯喰飲飴鱶飼飽餽飾餾"));
        PART_TO_KANJILIST.put('惟', new Part(1, "罹惟"));
        PART_TO_KANJILIST.put('飠', new Part(1, "饂餃餅饅餉饉饋餌饌饐饑餒饒餓餔蝕餝餞飠餠餡飢餤館飩飫餬飭飮飯飲飴飼飽餽飾餾"));
        PART_TO_KANJILIST.put('惠', new Part(1, "惠穗"));
        PART_TO_KANJILIST.put('胡', new Part(1, "醐胡蝴湖瑚糊葫楜餬"));
        PART_TO_KANJILIST.put('惡', new Part(1, "惡鐚"));
        PART_TO_KANJILIST.put('胥', new Part(1, "胥壻婿"));
        PART_TO_KANJILIST.put('ユ', new Part(2, "鴂袂駃鴃糇抉瘊癊篌侌鈌叏讏刔炔廕觖矙躛舝堠訣ユ玦憨鬫快夬鍭曮峱緱餱决筷鯸趹決缺睺"));
        PART_TO_KANJILIST.put('部', new Part(1, "蔀部"));
        PART_TO_KANJILIST.put('士', new Part(3, "瀆嘉吉尌舎簓鈓舗耗稠縠鐡頡娤渤蠧週頲鐵樹嘻穀恁驁鑄屆艇硈籌噎桔牘鱚驝鑟繥周表婬幬幮詰聱橲聲衽梃纈躊嚋誌續躕悖嚞犢読咭嚭庭檮檯隯袵皷隷璹袺銺劼調暿曀賃勃装惆竇軇櫉郌鋌囍囏糓壔雕仕鋕僖廚糚裝櫝黠擡郢擣賣蛣糦飪士壬磬拮壮壯声壱売壳黷廷壷擷壹壺髻任壻壼壽霆嬉弉焋鼓蜓鼔儔鼕鼖贖鼗鼙鼚鼛蔜紝鼟夡謦愨匨蜩霪欯匵謷挺款彀轂罄譆絍饎荏結饐獒歖魗荘奘孛罟慠捨彫襭彭酲筳佶譸讀喆疇妊莊澍澎榖憘憙続鮚莛喜涜実覟讟綢禧膨馨殪厫殰禱殸熹薹嶹熺殻醻殼瞽覿韇鷇鏊凋鷔槖志姙藚蟚鯛姞蟢痣濤鏧鷧淫凭觳俵蟶秸臺珽燾懿翿"));
        PART_TO_KANJILIST.put('壬', new Part(1, "恁郢梃賃霆艇妊霪淫壬婬凭庭荏酲袵蟶廷姙挺任衽"));
        PART_TO_KANJILIST.put('郭', new Part(1, "廓槨郭"));
        PART_TO_KANJILIST.put('壮', new Part(1, "装荘壮"));
        PART_TO_KANJILIST.put('壯', new Part(1, "奘弉莊裝壯"));
        PART_TO_KANJILIST.put('声', new Part(1, "声聲謦馨磬"));
        PART_TO_KANJILIST.put('売', new Part(1, "売続涜読"));
        PART_TO_KANJILIST.put('壳', new Part(1, "壳殻"));
        PART_TO_KANJILIST.put('郷', new Part(1, "郷饗嚮響"));
        PART_TO_KANJILIST.put('壹', new Part(1, "饐壹殪噎懿"));
        PART_TO_KANJILIST.put('能', new Part(1, "羆罷擺熊態能"));
        PART_TO_KANJILIST.put('壽', new Part(1, "擣儔濤鑄疇躊籌壽檮"));
        PART_TO_KANJILIST.put('夂', new Part(3, "瀀各逄踆戇倏倐鰒栙稜昝逡逢騣樤縧縫簬蠭稯倰耰稷鰷格落總鑁虁癁虂癃驄婈衉硌額降摓處牕陖晙驚摠履恪鱫噯聰葰陵晷桻詻鉻葼璁隆貉咎璐撒悛抜悠悤麥麦璦皧麨麩麪麬麭梭溭麮咯傯麯麰庱麳皴麴麵麸麹麺窻骼窿賂廈峉糉瓊櫌蓌鋒蛒雒曖烙務櫜拠狢竣蓧髪滫蓬棭胮峯峰擱囱棱苳胳峻狻烽擾僾夂蜂錂輅夅唆蔆夆複変鬉夊夋挌夌焌儍复愎夏夐夒夓夔鼕餕茖謖輘愙崚洚洛愛匛贛鼛嬡蔥霧脧優椱露霳儵椶鬷輹鴼笿終鵅靉鍐鍑鵔嵕睖蕗捘浚絛潞畞佟畟絡条略警杦畧酪蝮畯蕯数絳慶酸腹祾喀鎀憁憂覆薆後榎薐嶐羐羑喒澓掕掖膖鮗鞗禝客熢閣馥厦復鎫冬薮瞹綹鮻疼徼螽徽綾嗄鏅埈鯈俊柊凌巎鏓巙翛珞痠俢処淩鯪路駱菱緵臵篷韸鷺駿"));
        PART_TO_KANJILIST.put('夅', new Part(1, "夅降"));
        PART_TO_KANJILIST.put('愈', new Part(1, "癒愈"));
        PART_TO_KANJILIST.put('焉', new Part(1, "嫣篶焉"));
        PART_TO_KANJILIST.put('脊', new Part(1, "瘠蹐脊鶺"));
        PART_TO_KANJILIST.put('養', new Part(1, "瀁癢鱶養"));
        PART_TO_KANJILIST.put('夋', new Part(1, "唆酸俊夋"));
        PART_TO_KANJILIST.put('复', new Part(1, "履覆複復腹复"));
        PART_TO_KANJILIST.put('夏', new Part(1, "嗄厦廈榎夏"));
        PART_TO_KANJILIST.put('意', new Part(1, "億憶臆噫檍意"));
        PART_TO_KANJILIST.put('夐', new Part(1, "夐瓊"));
        PART_TO_KANJILIST.put('夕', new Part(3, "簃將倇名氎搖列舛舜舞踠瀣怨搩鰩琬頯帵瘵鰶爹桀恀穄繇橉婉豋豌屍驎繎汐鱗遙剜桝塟蹡瑤偧詧橪葬遴穸鉹詺晼誃邎墏傑碗窗銘璘炙撚撛傜麟隣粦梦咧璨檫窰媱粲傺颻粼劽裂哆烈惋盌苑髒磔蛚黟僢蛥飧僯僲拶磷鋺曻椀漈椉謋洌餐夕外茗夘夙多儚儛鴛夛夜夝謠夡夢眢茢蔣夣夤夥餮鄰眵然猺漿蜿腋獎奓轔腕啘蕣卥捥酩奬鵷畹歹鵺死歽潾歾歿殀殂殃斃殄殅殆侈嶈殉殊例残殍趍憐殕掖殖殗殘斘嶙殛宛殞殟殠殢殣殤薤薧殨薨殩殪殫瞬殬醬殭徭殮殯澯殰殱液殲斴涴瞵冽螿菀鷂燃燐鏘懜燦埦迯臰槳姴鏻移迻迾"));
        PART_TO_KANJILIST.put('外', new Part(1, "外迯"));
        PART_TO_KANJILIST.put('多', new Part(1, "夥侈多移"));
        PART_TO_KANJILIST.put('愛', new Part(1, "曖瞹靉愛"));
        PART_TO_KANJILIST.put('夜', new Part(1, "液掖鵺腋夜"));
        PART_TO_KANJILIST.put('感', new Part(1, "轗撼憾感"));
        PART_TO_KANJILIST.put('無', new Part(12, "無廡橅蕪撫憮璑蟱墲甒膴嫵嘸儛舞"));
        PART_TO_KANJILIST.put('夢', new Part(1, "夢儚"));
        PART_TO_KANJILIST.put('焦', new Part(1, "礁憔樵焦鷦蕉"));
        PART_TO_KANJILIST.put('大', new Part(3, "送倁砆蠎耎怏倏逘候倚蠜堠蠢倦堧逩頬蠮頰耷蠺栿偀偆桊桍塍恗灘類聟衡硤偧恩偰遷遼袂傄肄傒墓碕悘梜碝悞袟袠碤颫袱袴炴悷墺裀飆僊裌磎僑棒惓壓僕棙僚烟郟僣惥惨飫棬胭胯胰惷裷椅礇鄈愌儗礙鄚愜礜愞椥椦大儧夨天太夫夬礬鄭夭央儯夯夰愱餱失夲夳餴夵脵夶然夷夸儺鄼夾夿椿奃奄祅奆祆襆奇奈奉腌奎奏奐煐契祑奒奓祓楔奔慕奕襖套煗奘奙奚奛饜奝奞奟奠腠奡奢奣煥奥奧奨奩奪奫襫奬奭奮奯楰楱奲关慻襻醃憃覉禊妋憍膎規妖憖榘榛膜冪醫冭憭醭熯馱観决妷榺燃駄懊燎凑懕駚凝懝燠臡秡秦秧姨秩懩臭觭駰姱釱槻臻姻戁樁舂騃鈇樊騎刔舔訞模訣戣騤鈦稧爨権娭騰刳券戻爽驀驂鉃鉄詇橋橑驕穙剞鉠剦橪艱橳扶穾突犄誇抉媋誒銕媖芖芙銙犬犮劵媵状抶窺芺骻犾檿鋈嫉勌鋏拔鋘勝募竢勧勪勬狭髮英拳狹嫽謄錈鬈猋嬌匏笑錑謑猒猗挟錡茣猤匧挨謨献茯匲猴欵茵笶猷欸欹鬹医挾荂獃魃獄鍈歎獎獏荑獒歓魘獘譛獠獣鍥捧捩鍭捲筴卷獸捺獻殀鎂讃殃宊掎宎殗鮝讞玞箞実殠莢鮧掩莫鮬厭掭鮲厴鎹殹厺箺莽莾参鏃寄揆毉篋篌鏌寏菐換寞揬寮揳菴鯵菶鏷鯸篹鰆簇鐈鐐簒尖尞鐟吠氤簥琦鰧琫吴谿瑃瑍鱎摏汏籐呑籑豓葖籘鑚瑛葜葢豢瑧屩汰葵摸決沃咉璏粏沗貘璙撚岟撟咦撦撩咲撲蒺咽蒽蓁泆糇瓉擌糗賛瓛瓞瓠峡糢擪擬哭賰泰泱擲賸峽攀攁鴃攅洇唉崍崎洑蔘蔟洟攤鴦崦唪紱唳唵鴺洿畉鵊蕎畎畚嵜絝嵠絥敧絪鵪畸浹畻敽薁喉鶏疑涘薙涙喚嶛綟嶠関綣鶪喫趫喬嶴嶷閹趺綺嶽疾薿闃鷄嗅跋闋跌痍淎族淏鷖旖嗘闚鷞鷟藤跨鷮鷯緱旲痴淹添巻嗾渀鸂蘅嘆瘈昊瘊鸑渓瘓渕蘙帙渙瘞踟映踡縢春昦踦昳踳渶嘹渼瘼療繄湊蹊繎幐幕繚陜陝幞癡噢器癱癸智晻蹻陾纂嚈纉皋暌麌暎躑蚕暙溙躚暤蚨麩隩溪暮溱庵暵麸暸躾雉盎囏囐軑滕默黙雞因難黤軮黱滲黶蛺黻軼替圈蜈伏圏朕伕蜘霙漛漠朠漢輦漪漭輭弮蜯輳蜷眷鼷眹缺轃潅齅蝅轎轑佒杕彘佚潜睠蝡潦罨靨齮坱睳睺睽垁羃鞅羇螇侉羍美枎龑螓瞖枖侠螣瞭羮侯徯澳鞵羹达俁埃韆蟆蟇蟒蟜忝俟蟟俠矢矣俣矤知矦矧矩矪快矬短迭矮濮矯矰矱忲翳俸俺韺埼濽"));
        PART_TO_KANJILIST.put('天', new Part(1, "送倁騃謄簇唉昊瘊朕渕儗蜘逘礙候蘙蔟踟堠椥挨天権娭蠮騰愱餱輳昳猴欵笶欸医鉃鉄繄潅湊奏歓彘聟癡鍭关葵癸智睺垁肄喉璏疑誒蚕瞖悘榘涘薙関醫侯咲観嶷鎹殹蒺疾薿埃鏃揆糇嫉雉毉篌族鷖凝懝俟鷟矢竢矣俣矤知矦勧矧矩矪擬矬短迭矮矯矰矱緱翳痴鯸嗾"));
        PART_TO_KANJILIST.put('太', new Part(1, "汰葢駄太"));
        PART_TO_KANJILIST.put('夫', new Part(1, "讃鉄攅嘆畉纉跌歎規鶏渓灘帙芙佚鑚譛賛潜漢僣難攤輦秩麩夫迭失艱扶麸窺儺趺槻軼替"));
        PART_TO_KANJILIST.put('夬', new Part(1, "袂决訣鴃刔抉決缺快夬"));
        PART_TO_KANJILIST.put('鄭', new Part(1, "躑擲鄭"));
        PART_TO_KANJILIST.put('夭', new Part(1, "殀沃橋飫喬嬌夭蕎轎矯僑呑笑驕妖添忝"));
        PART_TO_KANJILIST.put('央', new Part(1, "映泱英殃鞅鴦秧霙瑛央暎怏"));
        PART_TO_KANJILIST.put('失', new Part(1, "失鉄帙秩佚跌軼迭"));
        PART_TO_KANJILIST.put('夲', new Part(1, "夲皋"));
        PART_TO_KANJILIST.put('然', new Part(1, "燃然撚"));
        PART_TO_KANJILIST.put('夷', new Part(1, "銕夷姨痍洟"));
        PART_TO_KANJILIST.put('夸', new Part(1, "瓠刳袴誇夸跨桍匏胯"));
        PART_TO_KANJILIST.put('夹', new Part(1, "峡夹狭挟"));
        PART_TO_KANJILIST.put('示', new Part(5, "漂鬃崇漈尉騌蜍眎途餘標蔚錝蔡倧霨踪瘭琮鰰頴瘵鰶縹示款鰾褾祀祁驃穄祇奈敍慓祓祕幖祗塗佘祘敘余祚襟祟祠除噤票呩畭祭孮彯慰捺祺荼剽摽齽祿禀禁涂悆榊禊斎鶎徐龒徖宗膘際熛斜綜蒜禜禝瞟禦禧熨禪檫禮悰禰徱螱禳螵隷隸薸傺粽飃飄僄俆棕嫖叙淙凛叞察勡瓢鏢擦磦賨柰翲"));
        PART_TO_KANJILIST.put('礻', new Part(4, "禄禅榊禊禋禌禍禎福禑禓禔禕視禖禘禛禝禡禧禨禩禪禫禮禯禰鰰禱禳禴簶鎺礻礼礽社礿祀祁祄祅祆祇祈祉祊祋祏祐祑祓祔祕祖祗祚祛祜祝神祠祢祥祧祩祫杯祲祷祹祺祻祼祾祿"));
        PART_TO_KANJILIST.put('夾', new Part(1, "侠莢篋頬鋏頰筴浹狹陜峽夾挾"));
        PART_TO_KANJILIST.put('儿', new Part(2, "耀逃鐃堄氄尅瀆簆堍瀍鐍頏頑鐕萙琛尠逡尢尣尤倥琥尦尨尩堪倪簪尫頫尭吮堯就怳逵蠶簷逸堺耽頽搾鑁桃桄硄塊灊鱊恌恍瑍術豗鑙鑚葚遞籞摡籩呪聭灮硯瑰葰恱遶遹屼葼鑽傀炁境邃蒄沅沇沈邈邊貌墍貎岏蒐撓悓悗梘梚悛墝悞梡邥撥悦碪肬梭岲梲咷傹撹貺悾況㓁糂磈糉烑擔裔僔泗瓚瓛裞磡胤僥僦僭哯烱胱棱売峴蓺峻磽哾鴆唆蔆崆蔇贊儋夋夌焌椌贍儍唍夔蔕餕贖愖洗甗脘攙甚崚攛攜紞攢愧崧攪褫攬儭洮愰脱唲蔲儳椶洸椹儹褹褻焼餽鄽儿兀允兂元兄充嵆商兆兇酇先光兊克兌界啌免兎兏児奐饒兒兓兔腔饕兕酖絖兗蕘兘浚党酛兜襜祝兟兢浣兤煥祧慨嵬畯襯統酸絸絻浼襼鵼熀綂概鶃薇膈見妋覍規冏覐薐醓覓覔冕覕視覗醗涗綗冘覘続喚覚覛涜覜醜禝薝斟覟冠覠覡嶢疣膣冤覥榥冦覦覧妧喨榨覩親覬趬膮覯覰熱閲覲趲観覴况覵覶覷覺憺覼冼覽膽綾覿禿觀凂槅巉鷉鷊凌巍藐槐旐燒淕臗巘姚闚藝駟痠无旡既跣觥淩駪駫藭姯深鷲痲跳緵鷸槻駾駿觿嘅踆爇税稑刓瘓帘渙舚稜戡阢瘣騣訦娧鈨帨縨娩阮嘵訵帶蘶稷稽晀晁虁晃晄噆橈癈剋幌驍虎蹏虒虓橘牘晙湛晛繞號院虢虣扤幨晩蹴陵穵詵究陸穸穹詹空発虺蹺牻穽穿窀暁突誂窂銃窃皃窄窅窆窈窊溌續抏纏檐窐銑麑窒窓纔隔躔窔窕皖蚖劖窖窗隗劗蚘窘劘纘銚纜皝窟窠犢皢窣芥銧窪芫嚫庬説窬躭読窮亮窯窰窳皴窵窶犹窹窺媺窻躻窼窿媿勀曁狁廃竃竄竅黆廆櫆竆竇囈竈曉滉勉竊囋黋鋎軏廐囐髐黕髖勘四廛櫝竟勢竣髧鋧諬櫬园鋭黮滯盶競諶价黷竸蛻狻黿圀錂霃欃挄蜆猇眈嬈弈鬉猊輐挑漑欑輓霓朓欖謖挖輗鼗輘朙輝欟蜣圥鬩挩朮霱鬲鬳鬴鬵匵鬷鬹匹眺鬺茺蜺鬻鬼挽鬽鼿魁魂魃魄筅魅睆魈齋魋魌杌睍魍獍譎魎魏魑魔魕罕鍖譖魖魗靗魘靚潛獟筦靦睦筧睨彪魫譫杬魭齯坴魷佻獻讀侁澆鞉侊完羌融讒莔枕厖垗鎘垙讚鞚箜莞讞探鎤羦莧控宨玩厩莬微殰箲莵垸鮸澹宺瞻侻鮻殼宼瞾寃寇鯇埈俊韑俒俔揕珖毚俛換寛矞菟鏡鯢鯥珧忨篪鯪翫揬翮蟯述忱鯱菱濳寴埶柶柷翹篼現蟾俿"));
        PART_TO_KANJILIST.put('兀', new Part(1, "兀耀暁晃鐃晄澆薇橈滉光曉幌恍驍燒饒撓絖蕘輝繞嶢僥尭微堯蟯胱遶洸翹焼磽"));
        PART_TO_KANJILIST.put('允', new Part(1, "允逡竣銃充唆俊梭吮統皴酸浚峻悛駿"));
        PART_TO_KANJILIST.put('元', new Part(4, "元蒄沅睆簆寇鯇齋完唍鋎岏抏輐頑刓皖蚖兘脘酛莞籞冠梡院浣筦羦冦忨鈨玩翫芫杬园魭阮蔲盶垸黿"));
        PART_TO_KANJILIST.put('奄', new Part(8, "醃奄黤剦崦罨掩腌菴唵庵殗淹閹奙俺晻"));
        PART_TO_KANJILIST.put('襄', new Part(1, "釀嬢孃穣襄壤驤曩壌穰譲禳讓攘醸"));
        PART_TO_KANJILIST.put('兄', new Part(1, "況兢兄尅悦呪克剋兌説鋭税脱閲况競党蛻祝"));
        PART_TO_KANJILIST.put('充', new Part(1, "統銃充"));
        PART_TO_KANJILIST.put('兆', new Part(1, "挑晁誂桃跳逃窕兆姚眺銚佻"));
        PART_TO_KANJILIST.put('兇', new Part(1, "椶兇"));
        PART_TO_KANJILIST.put('奇', new Part(1, "寄椅奇羇崎掎騎碕猗畸欹倚綺埼剞"));
        PART_TO_KANJILIST.put('先', new Part(1, "銑濳跣筅洗先讚贊鑽"));
        PART_TO_KANJILIST.put('奈', new Part(1, "奈捺"));
        PART_TO_KANJILIST.put('酉', new Part(7, "尊鰌逎輏鐏鄭猶輶猷蘸樽噂罇酉酊酋酌配酎酏鱒遒酒酓酔蕕酖酗酘酙酚酛奠歠酡楢酢酣蝤酤酥酧酩酪酬酭蹲酲酳酴遵酵酷酸酹酺酻醁醂醃梄醅醆醇醉醊醋醍醎醐躑醑醒醓醔醕醗醘撙醜醞嶟醡醢醤醦醨醪墫醫醬醭醮醯醰醱醲醳醴醵醶醸醺醻醼醽醿釀釁釂釃釅僔蓜盦槱擲竴"));
        PART_TO_KANJILIST.put('光', new Part(1, "耀胱晃晄絖洸光滉幌恍輝"));
        PART_TO_KANJILIST.put('奉', new Part(1, "棒捧俸奉"));
        PART_TO_KANJILIST.put('克', new Part(1, "兢尅克剋"));
        PART_TO_KANJILIST.put('酋', new Part(1, "奠噂楢尊酋墫鰌鄭躑擲蹲鱒遒遵蕕猶猷樽"));
        PART_TO_KANJILIST.put('兌', new Part(1, "脱閲悦蛻兌説鋭税"));
        PART_TO_KANJILIST.put('免', new Part(8, "凂寃欃勉巉免堍讒輓兔纔冕劖悗攙梚毚俛菟冤晩娩莬儳逸鮸絻浼挽"));
        PART_TO_KANJILIST.put('兎', new Part(1, "莵兎"));
        PART_TO_KANJILIST.put('奏', new Part(1, "輳湊奏"));
        PART_TO_KANJILIST.put('奐', new Part(1, "奐煥渙喚換"));
        PART_TO_KANJILIST.put('契', new Part(1, "契楔禊喫"));
        PART_TO_KANJILIST.put('兒', new Part(1, "麑兒鯢霓睨鬩倪猊貎"));
        PART_TO_KANJILIST.put('兔', new Part(1, "讒寃兔冤纔巉菟"));
        PART_TO_KANJILIST.put('奚', new Part(1, "渓鷄奚溪蹊谿鶏"));
        PART_TO_KANJILIST.put('楚', new Part(1, "楚礎"));
        PART_TO_KANJILIST.put('神', new Part(1, "鰰榊神"));
        PART_TO_KANJILIST.put('奠', new Part(1, "奠躑擲鄭"));
        PART_TO_KANJILIST.put('入', new Part(2, "退送适逃逄逅倆逆逈鈉瘉簉樋縋逋逌逍導逎透逐逑逓途逕逖逗逘這通逛渝逝逞速樠造逡逢連訥搥逧蘧逨逩逪縫逬蠭逭逮逯踰週鰱進逳逴逵吶逶逷瘸逸逹縺逼鐽逾逿遁鱁遂遃遄遅久遇遉遊運遌遍過遏遐遑癒遒道鑓摓達違遖扖遘遙遛遜瑜遝陝遞穟遠噠遡遢顢遣蹣遥遦籧遧遨適籩遬遭汭遮遯遰遲衲遴遵噵遶遷選繸灸偸遹遺遼遽繾避邀邁蒁邂躂粂邃還邅邇邈邉璉邊蚋邋邌邎随邏抐邐銓躚檛璡隧隨窩肭芮璲暹撻撾蒾曃髄髓磓胔糙蓪蓬諭蓮鳰裲糴胾擿滿愈愉謎儙輛漣圦褪焫謰輸蜹脼蜽杁靆魎蝓襚畝魞楡入慥兩兪鍮譴镹靹腿讁膐膖玖枘薘疚掚鎚瞞熢覦喩辶辷膸辸鎹辺喻辻込达辿迀迁迂韃迄揄迅韆迆鏈迊迋臋鏋槌迍迎蟎运近迒迓返迕迚釞迠巡臡迢迣懣迤闥迥迦燧迨迩柩迪迫迭迮迯述迱迴篴燵迵迶篷迷迸嗹迹迺叺迻追迾"));
        PART_TO_KANJILIST.put('奥', new Part(1, "奥襖"));
        PART_TO_KANJILIST.put('奧', new Part(1, "燠澳奧礇墺懊"));
        PART_TO_KANJILIST.put('全', new Part(1, "栓銓全痊筌詮"));
        PART_TO_KANJILIST.put('票', new Part(1, "漂瓢慓飃驃飄嫖票標縹剽鰾"));
        PART_TO_KANJILIST.put('兩', new Part(1, "裲懣蹣倆兩輛瞞魎滿"));
        PART_TO_KANJILIST.put('兪', new Part(1, "楡揄覦愈喩愉瘉兪諭鍮踰癒蝓偸輸喻瑜渝逾"));
        PART_TO_KANJILIST.put('八', new Part(2, "耀送瀁逃瀆逆瀇倊瀍倎瀑耒耓途耔瀕耕耖耗逗耘耙倛耜耝耞速耟耠耡逡耤倥怦倦耦逧耨倪瀬耬耮瀰倰耰怳瀴逵倶瀷逸耽瀾遂偂灃遃遅遉灊恋恌恍灎偐遒道達灔遖恙偙灝遞聠聡遡遦適恭聭灮恱聳側偵聵遵遶職選遹遺遼傀炁邃邇邈邉傊邊悌傍傎悓悗悚傛悛悞傞傟邠邥肦悦肬債肸肹傹悻傾悾烊僎僐烑僓惓僔僕僖胖烘惘僜郤胤郥僥僦惧僧僨僭烱胱價惻胼儀儈儋愌焌儍鄍儐愖鄖脘儞償儣儧脧愧鄧儨愪儬愬鄭儭鄮儯鄯愰脰脱儳鄴儵鄶愷鄷儹愹鄺愼鄼焼鄽儿兀允煁兂元兄充兆腆兇酇先慈光煉兊慊克酋兌免兎慎煎兏児兒兓兔腔兕酖兗兘党酚酛兜兟兢慣兤煥慨全煩腩八慫公六兮共兵其具酸典慻兼慾兾熀冀膀膁膈憋憎冏憒醓熔冕熕憖醗冘憘憙醜冠憠醡膣冤憤冥冦膨膩醭膮熱熲膳醳醴况醸冸熹憺熺冼膽熿釀臀懀釁凂釈燈臋凌燌燎臏釐燒凒凕臗凘釜釟懟釡燧懩懬懭凱臱凲凳燵懶懿戃刅爆分鈆爇與戇興舉刓鈖舚爛戝戠戡判爤鈨爨舩鈬刱戲戴券船刺爾牂剃剅則剋剌前艏剏扒牓艖牘剘鉙鉛剛艠鉡扤艤剩剪扮剰剴艶鉷艷牻鉼劀劂銃劊抏銑銓劕劖銖劗劘銚択犠犢芥銧犧抨芫芬劵劷犹抹銻勀鋀狁勅鋇勉拌勌鋎狖勘勛勜勝勢勣鋧勩勬鋭狭拱鋲拳苹狻鋼拼狽錂挄猇錈茉猊挑挖挙挟茣錤挩錪錬献匱茱錴匵猶猷匹茺挽猽獃半獍鍐荓鍖獖南獗鍗捘獘鍘卙獝鍞獟獮獯獱捲荳捴捵卷獷獺獻卻荼獼鎂鎈鎊鎌玐莔鎔掕厖鎖鎘玜莞厠鎡探玢鎤厥莧控鎧厨玩厩鎫莬鎭厮鎮鎰莵鎹掽揀揁揃鏆菐鏑揕珖菘珙叙換叛菟鏟珠鏡叡叢揥珧只揬叭菱鏷現萁鐁鐃鐄搆鐉鐍損萍鐏鐐搒搓鐕萙鐙琛搠鐡搤琥吩琪吮鐮萯搴鐽搾鑁鑈呉鑌瑍呎摒鑕葖摘鑙鑚葚鑛摜鑜鑟摡瑢瑣葥呪鑭呮瑰葰瑱味瑳葼呼鑽鑿蒂蒄钄撅撇蒐璒撓撕撙璚璜璞蒡撥撩咩咫璯撰撲咲璲蒴璵咷撹蒹撻璽璿㓁蓂哄蓉瓉蓊瓓擔瓔瓚瓛員擧瓫瓮哯擯瓰蓱擲擴擶瓶擷蓺蓻哾擿攀攁唄攅唆蔆甆蔇唍蔐甑攓攔蔕甖攖甗攘攙甚攛攜攢産攩攪唫攬唲蔲蔶攽蔽蕀商蕆啇界啌畍敍蕒畔蕕敕敗蕘敘敝畟蕡蕢蕨畭畯異整敵蕷蕻啻畻啼镾啿斁喃斃善薇喇薋疎薐斕薘喚喜斜薝斟薠関疣疥閧喨薩薪斯新閲閴旁藇藉痊闌嗌藐旐闐痒闓嗔藕闕旗痘闘旘闚藚藝闝嗞旟嗟痠无闠旡既藤闥闦嗩藭痲嗽藾嗿蘀嘅蘋蘍嘑瘓嘖昖蘚蘛蘡阢瘣瘥昧蘭阮嘳嘵蘶嘶嘻昼嘿晀噀癀虀晁虁噂療晃晄噆癈噌虎晎噎虒虓噔陖虖晙晛癜晜虞號噠噡虡院虢癢虣除癩晩癪晪癬癭普癲噲噴陵噵噸陸発虺噺登暁皃隊隑隔隕皖蚖隗蚘隘皚暛皝暝嚝皟暟蚡皢嚢蚣隤隥隧嚫嚬嚭嚱皴暴嚶皷暸暼暿曀曁囂曂盆囈曉益囋曌囍囎曎囏囐盖四曛蛛曝雟曠曦囧盨曩蛬园蛮雰盶囶蛻盻盼蛽曽曾圀霃蜆朇眈圈蜈蜍圏霓朓圓朔朕朙蜙圚圛眛朞眞蜞期真蜣霣圥未末朮霱朱朳蜷眷眹眺蜺朿着睆睇靊杌坌睍睖靗靚束睠坢蝤靦睦靧睨坪睪杬坴坹靺蝻靽松睿螃螆鞉螉瞋枌融瞑瞔枕垗螘垙鞚瞚螟垟螠螣瞥枩垩瞪垪垬瞭枰枳瞶垸瞻鞼瞽瞾螾韃韆韇埈柈矉韊韑蟖柗蟚矞蟢埧柬短蟯矰埶柶柷基韻蟻蟾頁頂頃堄項順頇須堈頊蠊頌頍堍頎頏預頑頒蠒頓栓頔頖頗領頙蠙頚頜頞栟頠頡頣頤頥頦堪株頫頬頭頮頯堯頰砰栱頲蠲頳頴頵蠵蠶頷頸堺頻頼頽頾塀桃顄桄硄衅顆顇顊塊桊顋題額塍顎顏塏顑塑顒顓術顔顕塕顖衖顗塗願顙顚顛衜類塞衠塡顢顣塤顥顦顧塧顪顫填顬顯硯顰顱硲顳顴碁境墈墊墍梖増碘梘梚墜墝袞墟梡墡袢梥碩碪梪墫梭梯碰墱梲碲墳碵颶梹碽磁壂磅棅磈棊棋磋裋壌磌壎壐壑磑磒裔裕棗棘壙壝裞壡磡棡壤磧棬棱売磳磴裷裹壹磺棻棼磽餅変養夋夌椌夒礒夓夔餕餖餘礠餠椣夤礥礦椦褨礩椪褫椶褹椹褺褻餻餽襀襄襆饈饋饌饍饎襏奐饐饒襒饕奕襗饙襚襜祝楝奠楠楢祥祧楨襭業襯襰襴楴襷祺襼襽祾概見妋禌覍禎規覐覓覔覕榕視首覗馗覘馘禘覚覛禛覜榜禝覟覠榠覡覥榥覦馦覧妧禧榨覩禩親嶺覬禮覯羚覰禰覲観禳妳領覴覵覶覷妹覺榺覼覽覿禿榿觀槅駅槇秉槊槎槐槓様槙姚秚姜駟駢秣秤觥觧駪駫姯槻駾駿觿樀樅稊樌税騏稑稘稜訟騣樣娣訦娧娩横騫娯稯騰訳樴訵稵稷樸稹樹樻稽樽穃驇橈婈驍積穎評橘橙穙橛婞穟穣驤驥穥橧穪詮穰橱橲橳詳穴穵詵究穸穹詹空婻穽穾穿窀突誂窂窃窄誄窅誅窆窈誉窊窋檐窐窑窒窓窔窕窖窗窘媜窞窟檟窠窣誤檥窩窪説窬読窮窯窰檱窳檳窵媵窶誷窹窺媺窻窼窿媿骿竃櫃竄竅櫆竆嫆諆竇竈櫈髈竉櫉竊嫌髐竒體竕櫕髕髖櫝竟諡嫡竣櫤諦竦髧嫩諫諬櫬竴諵競諶竸諺櫻鬀嬁鬂欃欄謄欅鬆謇嬈鬈鬉嬉欉鬋欑鬒欖謖謗欗謙鬙鬚謚欟鬠嬢鬢鬨鬩嬪鬪謫嬭謭嬰鬲欲鬳鬴鬵鬷鬹鬺欺鬻鬼謼鬽嬾魁魂魃孃魄譄筅魅譆魈歉證魋魌筌魍譎魎魏魑歒魔譔歔魕譖魖策歖魗魘識譜歝歟筦筧魫譫魭議譱譲孳歵魵魷譽孾譾讀讁讃鮃殊完宍讒讓箕讕讚箜讜鮝讞殞箞讟宨殨鮩殪殬箭鮮殯殰箲鮷鮸容宺鮻殼宼殿寃寅寇鯇篊寏寐寒篔鯗寘毚寛寡鯢鯥實寨篪鯪寮鯱寳寴寶篼簀鰂氄尅簆尊鰊鰌尌對導氛簛鰜尠尢尣簣尤尦簦鰦簧鰧氧尨簨尩簪尫尭就簱尲簴尵簷谷簸谸谹谺尺谽尽簾谾谿豁鱃豅豆豇豈鱉豉鱊豊豋豌籍豎籏豏屏豐籐豑鱒屓豓豔鱔鱖豗籘豙鱚鱛籜鱝籞籟豢籣層鱧籩屭鱮屰籲豶鱶屼汾沅沇沈粉貌貎岏岔貝貞負粠財岡沢貢貤貧粨貨販貪貫沫責貭貮貯貰粱岲貲貳貴貶買貸貹貺費貼岼貽貿沿賀況賁糂賂賃賄賅賆資賈糉賉賊賋賍糍賎賏賑賓賕糕賖泗賙泙賚賛賜賝糞賞賠賡峡賢賣賤賦糦賨糩峪質賬泬賭泮賯賰賲峴賵賷賸賺峻賻購賽賾賿贁贃贄贅鴆崆贇贈贉贊贋洋贍贏贐贒贓贔贖洗贗崗洙崚紛贛紞崧洪鴪洮崱洸崹嵆絆鵔嵕絖鵙浚浜嵜鵜嵡浣絣鵥嵬赬嵭嵯嵰統嵳浴絸絻浼鵼嵿綂鶃鶇綈綌嶒涕涗綗続涜嶝嶟嶢綣嶧鶫嶫趬嶬涬綱趲鶲網嶺嶼鶼綾鶿鷁鷆淇巇巉鷉鷊巍巎総鷏巓淕巘巙淞淟締跡跣鷧淩鷩差鷯跰深鷲跳練緵巷鷸巻巽踆踈縊鸎踑縑縒渕帘渙鸚縜縝帝渞帟踡縢並縦渧帨縨丫測港縯丵帶縶踸渼績幀蹄湅蹇蹉幌幎蹎乎湏蹏幐繒湓織湔繕乗幘乘繘繚湛蹞繞湞幞蹟繢蹢幣蹤繥幨蹩蹬蹭幮蹰蹲平湳蹴并蹶繸蹺鹺鹻蹼繽湾纁溂躂麄纆纇纈纉亊纊溌續麌纏纐躑麑麒纓躓溓纔躔躕纘纜躝溟庠溠溢麥亦麩麪庬躭溭麭亮躮溯庱溳溶躻躾溿廁滂廃滃黄黆廆軆滇黈滉廉黋滋介黌廎軏滏廐黕滕廚廛廝黟廣黤廥黧黬黭黮滯黰黱黲滴黵价黷黸黹滹軹黻滻黼份滾黿伀輂弈鼈弊輐漑輓鼓演鼔鼕鼖輗鼗輘鼙鼚鼛漛輝輞鼟弟鼢漬弮弯漱伴伵輶伻漾缾鼿輿弿潁罅彅罇齋彌罌彍齎潏轒罔罕余潛彛彜轝齞潢罤彦潦彪彭齯佯潰罱併彸佻潽罾佾侁澃澄澆來徉侊羊羌澌澍羍澎美羏侏徐羑澒従羔龔羖羗羚供羜羝羞龞侠徠羡羢羣辣群龥澦羦澧羨義羪侭羭微羮澮羯澱羲羴羶羸澹羹侻羼翁濂濆俆俊俒俔濔翔俗俛俤翦忨濨忩忪翫濬翮濮述忱濱濳濵迸翹濹迹濺翼濽俿忿"));
        PART_TO_KANJILIST.put('公', new Part(1, "翁聡蚣鬆枩舩蓊公頌瓮総鶲菘松淞訟"));
        PART_TO_KANJILIST.put('六', new Part(1, "榠嬢穣冥壌六宍幎瞑譲醸暝溟螟"));
        PART_TO_KANJILIST.put('祭', new Part(1, "蔡擦際祭察"));
        PART_TO_KANJILIST.put('兮', new Part(1, "諡盻兮"));
        PART_TO_KANJILIST.put('慮', new Part(1, "鑢慮濾"));
        PART_TO_KANJILIST.put('共', new Part(1, "冀臀哄爆饌瀑供癜曝糞驥閧鬨洪蛬恭港撰異共拱澱戴暴巷選翼巽殿"));
        PART_TO_KANJILIST.put('女', new Part(3, "娀威娃鰄娄縅娉萋鰋娌簍嘍娍耍萎娎娑帑怒娒樓娓怓娘嘙娚鸚瀛娜娞頞娟娠蘡娣娤娥娧娨氨娩娪耬倭娭砮娯堰娰瀴娵娶逶蘶縷瘻娼婀婁偃婄婅婆婇案婈婉詉婌晏婐籔恕婕婚桜汝籝婞摟偠屡婢屢婣婥婦婧屨婪婬婭癭葳桵呶婷籹婺婻葽婾婿塿隁媋媐媒纓媓媖媙媚媛媜媞媟媠媢媧銨努媬媱媲媳媵窶嚶媸咹媺媻媼媽悽袽媾媿嫁軁僂嫂嫄嫆嫈嫉諉嫋嫌髏拏嫏嫐瓔廔嫖嫗嫚嫜嫠嫡囡嫣嫥嫦嫩嫪嫮棲嫵嫶嫺胺嫻櫻擻嫽郾嬀嬁椄褄嬈按嬉漊嬋嬌霎挐鴑餒嬖攖嬗嬙嬛嬝蔞嬡嬢蔢崣嬥餧弩嬪嬬嬭伮笯嬰嬲蜲洳鴳嬴崴鼴嬶褸嬸茹鼹椻唼挼匽鴽嬾孀孁孃孅腇孋孌筎魏蝘啛佞蕠孥絮腰数楲女奴奵奶數奸她奻楼奼捼好荽孾捿要妁嶁如妃妄薅安妊妋妌鞍妍妎綏侒妒妓喓妕妖鞖妗妙妛妝侞鮟妟膢妣妤接妥妧妨侫妬妭薮妮妯妰妲妳宴妷妹涹妺螻妻妼宼妾鮾俀姁姃鷃凄姄姆菇姈姉姊始緌巍姍姐駑姑淒姒姓委鯘姙姚姜姞姟寠揠姣翣鏤姤姥姦姧姨菨藪姪姫矮姮姯珱姱姲姴姶姷釹姻痿姿"));
        PART_TO_KANJILIST.put('关', new Part(1, "咲関关"));
        PART_TO_KANJILIST.put('奴', new Part(1, "帑駑怒奴孥呶弩努拏"));
        PART_TO_KANJILIST.put('兵', new Part(1, "鋲兵梹浜"));
        PART_TO_KANJILIST.put('其', new Part(1, "碁淇棊棋厮斯籏騏麒撕箕其嘶旗稘簸基欺祺廝朞期"));
        PART_TO_KANJILIST.put('具', new Part(1, "塡惧填慎鎮鷏癲巓倶颶具槙顛真"));
        PART_TO_KANJILIST.put('典', new Part(1, "椣腆典"));
        PART_TO_KANJILIST.put('兹', new Part(1, "磁慈兹滋"));
        PART_TO_KANJILIST.put('兼', new Part(1, "濂廉歉蒹謙慊賺兼嫌鎌簾"));
        PART_TO_KANJILIST.put('楽', new Part(1, "檪薬楽"));
        PART_TO_KANJILIST.put('襾', new Part(1, "価漂瓢覃飃驃飄蕈票覈覊潭慓鐔嫖標縹譚剽襾鰾簟"));
        PART_TO_KANJILIST.put('西', new Part(6, "尊鰌逎鐏鐔栖栗標堙簟瘭樮蘸縹樽鰾噂驃鱏鱒遒晒硒驔幖偠湮瑮蹲遵遷陻剽摽葽梄躑撙躚粞粟檟溧墫哂飃飄僄賈僊僔嫖蓜勡瓢盦磦拪擲僲竴價廼漂甄鄄贉輏洒茜鄭猶輶猷弻褾慄罇蕈酉酊酋酌配酎酏酒酓慓酔蕕酖酗酘酙煙酚譚酛奠歠酡楢酢酣蝤酤酥酧票酩酪酬酭潭彯腰酲酳酴酵酷酸酹酺酻襾西醁要醂醃覃醅醆覆醇覇覈醉覉醊覊醋禋醍醎醐醑醒醓喓醔垔醕醗醘膘熛醜醞嶟憟瞟醡価醢醤醦醨醪醫禫醬醭醮醯醰醱徱醲醳醴醵螵醶醸薸醺醻醼醽薽醿釀釁釂釃釅韆闉凓鏢篥蟫槱翲跴迺"));
        PART_TO_KANJILIST.put('冀', new Part(1, "冀驥"));
        PART_TO_KANJILIST.put('禀', new Part(1, "禀凛"));
        PART_TO_KANJILIST.put('要', new Part(1, "腰要"));
        PART_TO_KANJILIST.put('禁', new Part(1, "禁噤襟"));
        PART_TO_KANJILIST.put('冂', new Part(2, "耀堂栄倆蠆堈逈而耍逍耎倎耏怏耐耑蠒逓蠕怖倘倜堝瀞蠣耦堧倩倫栭瀰週栴蠵瀹耼瀾偁偂遄桄硄遇偊灊遍塍恍過偏桐灑偑顒顓灕衕遖遘塙偙停桜硝衞顢档恧適恫顬遰衲偶遹衻墁邁悄邇肉傐邐悑悕肖碝梢肦風悩颪颫颭肭颮颯炯颰颱碲炳颴邴颶颷颸颺墺肺颻碻颿碿僀飂飃飄飅飆惆磆棆飈棉飌壐僑胔裔烔壖棗郗惘棘僘惝棠棡裯烱胱裲裳惴胴胹惼製胾飾儁鄅椆礇餇焇餉褊愌褍夐礑儒鄗鄘愚褝愞儞償褠脧礪焫央愰脳褵儷儻脼腆酈光腐奐襒楓襖煗党奝楝奟楠襠腡兤煥奥襦奧腨兩腩慲楴襴慵襷慸襺熀冂冃冄内禅円熇膈冉冊冋憋册再禍憍榍冎冏膏膐冐冑禑冓冕禘覚膛榥醨冪禫覯禰醰禴覶禸禹禺离覼禽榾槁釁釃槅駉姉懊臋構凋姍臑槗臠燠臡懣觥懦秧凧駧駫懯姯秱燸凸觿樀稀戃興鈉稍樔騙爚爛稠樠舡舢訥戦騧爨舨騨刪爯鈰稱舲舴鈵制刷刺舺爾稾稿扁扃艃扄艄牅艅艆削橋艋艎牏艏艑驕橕艖橖詗橘牚剛艜艠艣婦艧驪穪艭剮詷婻婾剿劀銅芇誉檎抐犒誚檛銟媢抦媧銧骨窩骪骬窬骭骮芮骯骰檰骲骴労骵媵骶誷銷骸骹骻媻骼檽媾骾芾骿調髀髁髃髄髆竇髈竊髎髏髐髑苒髒髓櫔體髕論髖勖髗高苚髛髜諞髞嫡櫤髥嫦勦諦勪苪諫竬嫮端髯英勱髵勵諵諷鋼鋿錀挄欄欅鬋嬌欐猘挙講猜匝謞猟錦笧猧鬧謫嬬嬭献猯鬲鬳鬴猵鬵鬷欷鬹鬺鬻茼匾猾獅魈歊鍋孋魍魎譎捎魑筒歒策南鍗獘単獝獣学獮筲魳鍴歸孺獻獼鍽譾讁殃掃掄箆掌箍讏箐箒鎖鎘掚讜鮞讞殢掣鎤鮦莦箪鎫鎬厰厲厳宵鮹莿揀菁毃篅篇珊鏋鏑鯑寓鯖珖寘篙換鯛鏜篝鏞揣揥珦篩珮鯯珱毷菷鯿氄氅搆鐈吊鰊同鐍向琑簓尚搞鰤簥鰧鐫萬搰琱萵吶萹簹鐺瑀瑁鱅鑈鱉鱊瑍鱎鱏屑摘摚瑛摛鑜瑞属瑣籥鱨周豨屩籬汭籭鑮鑰籲蒂璃撇撐撑貒蒒璚沛撟粡岡蒪粫璫蒯撹咼璽撾蒿泂糄蓆蓇哊瓊擋响糏擒峒賙瓛峝賞哨擩峭糯泱糲賵瓻購擿納洏唏蔐洓蔕甗崗崘崙攜洞甞鴦攦攩甩鴬鴯洸崹蔽蕀啁鵃畄絅商啇敇嵎蕎嵐絖敝敞絧嵩鵩嵪鵰敲敵當絺啻啼敽镾喁薁喃斃綃鶄消喎斒薓綗喘趙閙喚趟嶠綢趫鶫喬鶮綱網嶴営薷綸鶻薾綿藁嗃旃病旆藊鷊巋巌淌痌闌巐闒藕嗗巘跚淛緜痟締嗣巣編嗩鷩淪鷮藳鷸緺巾帀市布帆帇瘋帋希帍帑帒瘓帔帕帖嘗帘渙丙鸙帙帚帛帝鸝縞昞渟帟映帠両渢两帥渦渧縨帨師縭席蘭帮帯帰嘱帲帳帵帶帷瘸常踹丹蘺踽帽帾幀蹁晃幃晄蹄幄幅幇幉陋幋幌晌湍幎幐幑晑幔幕乕幖陗幗癘幘繘幛蹛幜晞幞癟幟幡幢噢蹢蹣幣幤幨虩蹩幪幫幬繭幭幮幰湳繻蹻陾満亂隃嚅隅嚆躉蚋暎隔麗隚纚躛皜溝皝暠隠溢蚦躧隩亭嚮溮亹躺暼庽蛃滈滉黋雋蛍黐囐滑雕囘雘軜滞雟廠離蛧黨曬滯滴蛸黹黻黼滽滿需輀伂霄圇鼈霈輈弊圊圌輌圑漓輖霙朙輛輝輞鼡蜩輪輭眮弰漰霱蜹蜻蜽弾朿佈蝉蝋彌坍轎睎潏网当罓罔靕靗蝙睛轜靜蝡彤佩彫杮坰罱齲彲齵蝸靹蝻羀羃螄鞅鞆侊垌螌融枏侖侗鞘枘垙澚徜瞞龞瞠龠龡龢龣螣龥瞥徧垧辭螭螮鞲螳澳鞶鞺柄濅迊蟎俏蟐韑濔蟜韝矞矟濡迥翩矪柬翮翯矯韴柵迵蟷埽柿"));
        PART_TO_KANJILIST.put('如', new Part(1, "如洳恕茹絮"));
        PART_TO_KANJILIST.put('憂', new Part(1, "憂優擾"));
        PART_TO_KANJILIST.put('覃', new Part(1, "覃鐔蕈譚潭簟"));
        PART_TO_KANJILIST.put('妄', new Part(1, "妄侫"));
        PART_TO_KANJILIST.put('内', new Part(1, "柄内病訥鞆蚋陋納肭衲炳吶丙靹"));
        PART_TO_KANJILIST.put('円', new Part(1, "菁円鯖倩睛蜻猜靜瀞"));
        PART_TO_KANJILIST.put('冉', new Part(1, "搆冉構再覯髯稱苒冓遘講購溝篝媾"));
        PART_TO_KANJILIST.put('冊', new Part(5, "錀掄圇冊褊倎斒侖崘崙騙鸙爚龠龡龢龣龥徧笧輪刪倫禴猵綸瀹萹匾扁蹁糄棆篇珊藊遍姍偏艑論蝙跚諞癟嗣籥編翩淪鑰籲柵惼鍽鯿"));
        PART_TO_KANJILIST.put('熊', new Part(1, "羆熊"));
        PART_TO_KANJILIST.put('見', new Part(7, "蜆見覍規覐覓悓夔覔覕視欖覗覘梘覚覛纜覜欟覟覠覡覥覦覧莧覩親攪嚫攬覬儭覯覰覲観覴覵覶覷撹鬹窺覺覼覽覿觀虁睍俔髖臗闚靚寛晛靦筧鋧櫬襯硯哯峴絸槻現"));
        PART_TO_KANJILIST.put('再', new Part(1, "冓搆遘構講購再溝篝媾覯"));
        PART_TO_KANJILIST.put('規', new Part(1, "窺槻規"));
        PART_TO_KANJILIST.put('冏', new Part(1, "烱裔商橘鷸攜譎冏"));
        PART_TO_KANJILIST.put('熏', new Part(1, "勳醺燻熏"));
        PART_TO_KANJILIST.put('冒', new Part(1, "瑁冒帽"));
        PART_TO_KANJILIST.put('冓', new Part(1, "冓搆遘構講購溝篝媾覯"));
        PART_TO_KANJILIST.put('冖', new Part(2, "瀀堂栄瀅倇瀉瀋倌怏蠑蠓蠔瀗蠙頞瀠砣倥蠧倧逭耮瀯耰蠹耽案運塋額偙塚停灞塞硡聢聤顬遰桲聹灾邃碇邉邊傍肎悖碗傛梡傢邥梥您悰碲確悾僀磅磆惋壑壕棕壖棠壡僢壩壱売惲裳壳壷壹壺棺胺壼僾椀愂愃鄆儋褌礌椌鄍礐儐礑餑脖椗脘愙儚愛儜儝礞償鄠夢夣夤愨館優餫焭礭央褰儳礵愹儻酃煇煊楎饐腔腕酖党饛腟襠酡煢楦酧祲楴慶慸膀榁憂膋膏熒醓熔榕冖榖冗冘禘写覚膛榜禜冝冠榠冡冢冣膣冤冥憥冦冨榨冩冪榮憲榷覺醽榾釁臏凕槖臗臛懜駝懞營懥秧懧臱姲觳駸燸臺懿戃騌樒舘舜舝戞訦爨騫樰舵稼穀穃鉈婉橐詑橒牓橕橖牚艜剜驝詝牢婦艨詫割穴穵究婷牸穸穹空穽牽穾穿窀突窂窃窄窅窆窈窊窋窐檑窑窒窓窔窕窖犖誖窗窘窞窟窠窣骨銨窩窪骪檫檬骬窬読骭窮骮窯骯檯窰骰骲檳窳骴労骵窵窶骶骸檸骹窹窺骻窻骼誼窼檽骾檾窿骿髀髁嫁櫁竃髃勃竄髄竅髆嫆竆竇竈髈嫈竉竊櫌鋍髎鋎髏髐鋐髑髒髓體髕拕髖狖髗竚諚櫜勞鋟諠諢諦嫦髧苧狩竩英鋺鬃謇按挓挖謗欛錝欞錠嬡茡鬢錧嬪鬱嬸猽猾孁鍕鍗字孛捥学筦魫孮獰獱歸學荸譹宀鮀宁殃掃它宄宅讅宆宇守授安宊鎊宋鎋完掌宍鎍宎宏宐宑箒宓鎔宔宕宖宗官宙定宛箜宜讜宝莞鮟掟実管探客殢宣鎣室宥宦控宨宩殪宬宭宮殯宯宰宱宲害宴宵鎵家宷宸容宺殻宼殼鮼宿寀菀寁寂寃寄菅寅密寇鯇寉富寍揎寏寐寒寓寔寖寗受寙寚寛鏜寝寞察叠寠叡寡寢寤鏥寥揥實寧寨審菪寫毫揬揮寮寯寰寱寳寴寵寶菷簆搉氎搒萓鰘萙鰚琛氨琬琮琯搰萱鐳搴簹鐺簺搾谾琿豁瑄鑅豌鑌葖摚瑛鑜鱟葠瑢葧鱨瑩鱩籩豪鱫摴葶葷蒂蒄撈沈沉撐撑蒙咜蒡咤璦璫貯沱撹咹粽璿㓁蓂蓇蓉擋糓賓糘賝賞擡擦賨擩泬擯擰泱賷賽擾蓿鴆崆崇攉甍唍攓蔕鴕攛甞紞索蔤鴦攩攪鴪鴬攮甯蔲鴳紵崹紽啌蕓鵓啘浡蕣浣嵤浤嵭畳當鵷浸畹啻啼鵼喀疂斅綅薆疉疊薋綋綍鶎疐薓喗続涜綜斝鶤薧喧喨薨綬鶯綰鶱鶴涴薴営薷嶸薹鶻綻閿淀旁鷃鷇巋跎嗗淙締鷧藭痯深藼鷽藿縈蘎瘒鸖嘗帘帚帝渟映縠踠縡渤渧踪縮帯縯帰渲帵帶常渾陀虂蹄癆蹇噎幎晏蹛蹜院幪噯幰晼繽嚀暄嚅暈暉暎嚏嚔皖隚暝溟嚢皧亭躭亮亳嚳溶皸皹亹隺躻曀滂黆蛇囊黌曌軍蛍滎滑滓黕曖曚滞黨雩滯雯雱雺霂霃霅眈霉鼏輐演霙霚霛蜜輝霝霡霢霣朦霨輨霱鼲霳鼹蜿弿彀靁轂罃靃轄睆佇靉靊彍靎靏罕佗彙靛坨齳睴坹坾睿螃鞅澇鞍瞎澐瞑侒枕徖龗侘瞚鞚羜垞螟瞠螢瞢辤羦垨瞬徬螮螳侵垸瞹鞺侼瞾螾柁矃濅矇矉蟐俒矒韗濘濚濛埞濠埦濬翬濱迱忱濵蟷埽"));
        PART_TO_KANJILIST.put('首', new Part(9, "虁道夔噵首馗馘巙衜導渞艏"));
        PART_TO_KANJILIST.put('冘', new Part(1, "冘沈"));
        PART_TO_KANJILIST.put('香', new Part(9, "馥馨香楿"));
        PART_TO_KANJILIST.put('覚', new Part(1, "撹覚"));
        PART_TO_KANJILIST.put('冠', new Part(1, "冠蒄"));
        PART_TO_KANJILIST.put('冢', new Part(1, "冢塚"));
        PART_TO_KANJILIST.put('冥', new Part(1, "榠瞑冥暝幎溟螟"));
        PART_TO_KANJILIST.put('妥', new Part(1, "餒妥綏"));
        PART_TO_KANJILIST.put('親', new Part(1, "親襯"));
        PART_TO_KANJILIST.put('冫', new Part(2, "耀逃渋爍嘐鸐嘙蘙瀚樛鈞鰥搦搨栨樨鰨栩頫戮蠮鰯瘳戳嘷瀷騸尽晀塁晁摂桃鑃繆扇塉籊蹋塌恌摎噏蹐摔塕聚驟鑠詡遢恣剹蹹摺墀袀誂钃璆躍傓窕銚傟粢躢皥肦咨檪暭咷溺蒻溻璻飂櫂滃鋆資僇蓊嫋瓌髎烑廖磖盗勜曜磟勠飡擢軣嫪廫諮櫰苳糴糶瓷蓼擽賿匀謆蔆餈挑贓朓鼕鼗次嬥挧脧茨礫謬洮冷弱愵褶唹眺漻終兆均轇歙潝嵡轢祧慴嵶嵺佻煽楽譾慿憀疁疂率螉鞉薋趐憑趑榒鮗垗覜膠熠羡趦宨醪冫薬冬侭馮趯冰冱冲鶲决冴况涵冶冷鶸冸鶺冺榻疼閼冼於螽羽冽冾羿冿蟀翀翁凂翃凄翅凅翆准翈凈蟉凉柊翊藋凋翌凌凍翎臎翏减藏鏐旐凑習闒凒寒凓翔翕凕凖凘姚鷚翛凛篛凜珝凝凞翟闟臟翠翡槢毣翣淤寥翥翦臧珧翨翩翫翬翮翯濯翰鯲翲跳翳凴韵翹翺翻翼翽函翾懿姿翿燿"));
        PART_TO_KANJILIST.put('馬', new Part(10, "騁騂騃騄騅騋騌騎騏騐騑騒験騖騙騞礟騠騢鰢騣騤騧騨騫騭騮儯騰騳騵騶騷騸騾驀驁驂驃驄驅驇驊驋驌驍驎驑驔驕驖驗驚驛驝驟驢驤驥驩瑪驪驫罵慿羈覊憑螞禡馬馭馮馰馱隲馲馳馴馵馹馺碼馼媽馽颿馿駁駃駄駅駆駈駉嗎駐駑駒駓駔駕闖駘駙駚駛駜駝駞駟駢篤駧駪駫駬駭駮駰駱駲凴駴駵駸駹駻駽駾駿"));
        PART_TO_KANJILIST.put('冬', new Part(1, "終苳鼕鮗柊冬疼螽"));
        PART_TO_KANJILIST.put('馮', new Part(1, "憑馮"));
        PART_TO_KANJILIST.put('榮', new Part(1, "蠑榮"));
        PART_TO_KANJILIST.put('冷', new Part(1, "冷冷"));
        PART_TO_KANJILIST.put('禸', new Part(5, "邁喁亂璃隅鄅蠆躉檎禑漓愚蠣耦醨礪萬辭縭螭嘱厲褵禸禹禺蘺离禽踽瑀髃遇竊偊嵎黐魑擒顒寓櫔藕灕癘摛属離籬竬勱糲齲勵齵偶"));
        PART_TO_KANJILIST.put('禹', new Part(15, "嘱齲禹属"));
        PART_TO_KANJILIST.put('覺', new Part(1, "覺攪"));
        PART_TO_KANJILIST.put('禺', new Part(1, "邁蠣隅遇礪萬嵎糲寓勵藕偶癘禺愚"));
        PART_TO_KANJILIST.put('妻', new Part(1, "棲淒凄褄妻萋悽"));
        PART_TO_KANJILIST.put('离', new Part(1, "離离籬"));
        PART_TO_KANJILIST.put('覽', new Part(1, "欖攬纜覽"));
        PART_TO_KANJILIST.put('禽', new Part(1, "擒禽檎"));
        PART_TO_KANJILIST.put('禾', new Part(5, "稀稂稃簃蘇琇稇稈稉稊程稌鰍稍萎税透稑蘒蘓稔稕稗稘稙稚稛稜瀝稞稟稠稡稧萩利萪稫倭稭種稯稰稱稲稴稵逶蘶稷稸稹稺稻稼稽頽稾稿穀穂穃穄穅穆穇穈穉湊穌積穎穏穐穕穖乗穗乘穙鑙穜穝穟穠穡穢偢穣除穥瑧穧癧剩穩癪穪穫湫穭剰穰犁檁犂麇和邌傔麕誗誘麘嚟嚦悧犧梨蒩溱悸銹蓁棅壈瓈盉諉囌黍黎黏黐鋓壢曦黧廩櫪諬峲囷磿愀唀愁鬁甃攈蜊攊唎褎蜏笑攟崣餧蜲輳靂轃嵆腇嵊蝌奏魏嵙啝季轣酥鍬浰楸捼啾楿莉綉薐徐螓醔鶖箘香榛馛斜龝馝澟馟莠龢厤馥鞦馦馨薭羲涹禾禿秀私秂秄秇秈秉秊藊秋菌緌懍巍鯏秏俐科秒委秔秕秖秘鯘叙秚凜藜痜秝秞租秠秡痢秢秣秤秥秦秧秩揪秪秫秬秭矮称俰秱藳藶秸臻移秼痿"));
        PART_TO_KANJILIST.put('妾', new Part(1, "椄接妾霎"));
        PART_TO_KANJILIST.put('禿', new Part(1, "頽禿"));
        PART_TO_KANJILIST.put('觀', new Part(1, "觀欟"));
        PART_TO_KANJILIST.put('秀', new Part(1, "秀莠誘綉銹透"));
        PART_TO_KANJILIST.put('釆', new Part(7, "蕃釆鐇礇釈悉釉竊懊蟋瀋釋燔潘旙繙旛飜蟠燠幡奧藩審番播鷭膰澳墺翻"));
        PART_TO_KANJILIST.put('采', new Part(1, "採綵采彩菜"));
        PART_TO_KANJILIST.put('應', new Part(1, "軈應"));
        PART_TO_KANJILIST.put('秋', new Part(1, "愀愁甃鞦楸萩秋湫鍬鰍啾"));
        PART_TO_KANJILIST.put('里', new Part(7, "戃甅理娌瀍鐘攩種踵儵儻鄽霾嘿艃襅穜衝獞艟慟幢董橦腫蕫浬蹱潼鍾罿墅纆貍纏熏纒躔厘熚讜悝撞袞憧墨梩薫瞳薶徸榸醺曈鯉埋里重働糎野裏量釐黑黒黔動黕默黙俚黛廛黜黝點黟黠裡黤壥黥童糧黧黨哩黬黭滭黮僮黯竰黰鋰黱勲黲勳黴黵黶嗶黷黸狸濹燻瓼滾"));
        PART_TO_KANJILIST.put('凌', new Part(1, "蔆凌"));
        PART_TO_KANJILIST.put('重', new Part(1, "勲董動踵腫薫重働衝種鍾慟"));
        PART_TO_KANJILIST.put('野', new Part(1, "墅野"));
        PART_TO_KANJILIST.put('量', new Part(1, "糧量"));
        PART_TO_KANJILIST.put('金', new Part(8, "鐁鐂鐃鐄鐇鐈鐉鐍鐎瀏鐏鐐鐓鐔鐕鐖鐗鐘鐙鐚鐟鐡鐫鐮鐯鐱鐲鐳鐴鐵鐶鐸鐺鐻鐽鐿鑁鑃鑄鑅鑈鑊鑌鑑鑒鑓鑕籙鑙鑚鑛籛鑜鑞鑟鑠鑡鑢鑣鑨鑪鑫鑭鑮鑯鑰鑱鑲鑵鑷鑼鑽鑾鑿钁钃钄撳崟嶔鈴金釓釔釖釗釘釙釚釛釜針釞釟釡釣釤釥釦淦釧釩釪釬釭釮釯釰釱釵釶釷釹釻釼釽釿鈀鈁鈄鈅鈆鈇鈉鈊鈌鈍鈎鈐鈑鈒鈓鈔鈕鈖鈘鈜鈝鈞鈣鈤鈥鈦鈨鈩鈬鈮鈯鈰鈳鈴鈵鈶鈷鈸瘹鈹鈺鈼鈾鈿鉀鉂鉃鉄鉅鉆鉇鉈鉉鉊鉋鉍鉎鉏鉐鉑鉗鉘鉙鉚鉛鉜鉝鉞鉠鉡鉢鉤鉥鉦鉧鉨鉩鉮鉯鉰鉱鉵鉶鉷鉸鉹鉻鉼鉽鉾鉿銀銃銅銈劉銉銊銍銎銑銒銓銕銖銗銘銙銚銛銜銟嚠銠銤銥銧銨銫銭銯銲銶銷銸銹銺銻銼銽銿鋀鋁鋂鋃鋅鋆鋇鋈鋋鋌鋍鋎鋏鋐鋒鋓鋕鋗鋘鋙鋜鋝廞鋟鋠鋡鋣鋤鋥鋧鋨鋩鋪鋬鋭鋮鋰鋲鋳鋸鋹鋺鋻鋼鋿錀錂錆錈錍錏錐錑錔錕錘錙錚錜錝錞錟錠錡錢錣錤錥錦錧錨錩錪錫錬錮錯録錳錴錵錶錷錺錻欽鍄鍇鍈鍉鍋鍍靎鍐鍑鍒鍔鍕鍖鍗鍘鍚鍛鍜鍞鍠鍤鍥捦鍧鍩鍪鍬鍭鍮鍯鍰鍱鍳鍴鍵鍶鍺鍼鍽鍾鍿鎀鎁鎂鎈鎊鎋鎌鎍鎏鎒鎔鎕鎖鎗鎘鎚鎛鎞鎡鎣鎤鎦鎧鎨鎩鎫鎬鎭鎮鎰鎴鎵鎶鎹鎺鏁鏃鏄鏅鏆鏇鏈鏉鏊鏋鏌鏍鏐鏑鏓鏖鏗鏘鏙鏜鏝鏞鏟鏡鏢鏤鏥鏦鏧鏨鏷鏸鏹鏺鏻鏽"));
        PART_TO_KANJILIST.put('科', new Part(1, "科萪蝌"));
        PART_TO_KANJILIST.put('角', new Part(7, "邂埆懈蠏角觔蟕觕觖觗觘觚斛觜觝檞薢解鵤澥觥触觧廨觩葪觫獬觭确嶰觱槲觳嘴觴觶桷觸蟹觹觽觿"));
        PART_TO_KANJILIST.put('委', new Part(1, "委逶倭巍矮萎痿魏"));
        PART_TO_KANJILIST.put('燕', new Part(1, "燕嚥臙讌"));
        PART_TO_KANJILIST.put('觜', new Part(1, "嘴觜"));
        PART_TO_KANJILIST.put('几', new Part(2, "蠃帆訊瘋頏蘙瀛縠瘢渢航搬般阬設吭蠮頽穀繄癈繋幋摋驋汎蹏恐偑虒虓處豛癜籝虝虠虢虣虤牨虩繫繭葮聲發鑿芁麂撃芃沆粇沉肌銎咒投炕抗芟股没亢墢撥劥風颪骪颫颭颮颯骯颰骰颱颴梵颶颷颸颺媺颻媻墼颿飂壂飃飄飅飆飈櫈擊飌竌廏磐糓諕拠髠廢飢盤磤囥鳧蛩磬鳬滮鳳壳諷猇伉蔎贏匓夙猟匟謦愨焭椴嬴机彀轂罄煅慇蝋祋襏嵐坑筑潑楓酘鍛蕟佩杭譭役薇螌閌箎鞏榖瞖羖冗馨鎩疫醫澱醱殳殴段鞶殷殸羸殹殺殻殼殽殾殿臀毀嗁槃毃毄毅毆鷇築毉鷉臋迒秔鷖痜臝緞几凡凢凥処鏧凧凩巩釩凪篪跫燬凭珮凮凰凱凲翳觳凳凴鏺忼蟿俿"));
        PART_TO_KANJILIST.put('凡', new Part(1, "凡帆築蛩跫汎贏鞏恐筑梵羸瀛"));
        PART_TO_KANJILIST.put('臣', new Part(7, "堅熈挋媐贒贓掔儖欖榘嚚纜茝茞朢頣頤礥頥宦覧樫弫攬弬蔵尶鰹檻覽籃緊藍腎豎藏鑑鑒臓煕鏗凞轞臟賢懢臣監襤臤臥擥艦臧臨竪濫姫盬孯慳鍳巸鋻譼鹽賾繿"));
        PART_TO_KANJILIST.put('解', new Part(1, "邂解廨懈蟹蠏"));
        PART_TO_KANJILIST.put('処', new Part(1, "拠處処"));
        PART_TO_KANJILIST.put('秦', new Part(1, "蓁秦榛臻"));
        PART_TO_KANJILIST.put('臧', new Part(1, "贓臧臟藏"));
        PART_TO_KANJILIST.put('自', new Part(6, "瀀憂熄螅邉嚊邊榎導嘎洎夏夒劓夓夔首馗馘戛瘜脜渞戞殠辠皥厦憩優耰咱媳鎴嬶嘷鼻鼼鼽鼾鼿虁齁囂齃嗄齄齅嗅齆齇廈衋櫌巎艏闑道糗巙衜濞癟衟寡奡襣擤籩自臫臬臭息臰偱寱臱臲噵翺擾"));
        PART_TO_KANJILIST.put('臭', new Part(1, "嗅臭"));
        PART_TO_KANJILIST.put('至', new Part(6, "榁挃幄鵄郅輊晊耊屋耋銍桎荎窒倒偓厔喔鰘腟握擡椡庢膣室垤渥咥姪蛭檯到絰至致臵臶齷臸薹臹臺緻臻"));
        PART_TO_KANJILIST.put('致', new Part(1, "致緻"));
        PART_TO_KANJILIST.put('凵', new Part(2, "堀鐁萄逆鰈搊頓倔搖倛簛堞搠尠吨鰩堪琪鐫簱簸鑁顄屆屈葉鱏籏豐塑灕鱖豘遙摛屜灞恟鑡遡顢瑤遥屧籬呭屮遮屯屰瑶汹葼瑾咁碁璃咄撅墈沌墐蒓撕炖炗傜碟邨悩碪肫蒭邯貰蒴颻墼糂棄泄蓆糉擊棊棋泏擒泔棡磡飩壩瓲惵糶胸儁贉儍鄒純蔕愖蔗崗甘甚崛甜甞鄞攤崫愬紲脳褵洶椶椹紺儺画絀煁襅兇饉嵌嵕酗祟煠酣啣煤蕨楪慬楳其慸祹祺啿憇覇覉覊喋禖熚斟憠趨醨斯熯綯醰覲疳斳鶵涵离醼禽鷂懃旆跇淇淈槊鷓闕燕旗凘淘緙臙臡懣嗤緤鷰凵緵凶凷凸凹出函旾凾槿戁鈍騏縐踑世丗稘蘛樠渡戡昢騣渫縭席稯鈯帯踱帶騶嘶踸蘺蹀牃繇湈幉詍牒鉗詘剘噛橛湛詜蹠蹣繫癱艱蹶陶艷噸艸扽幽詾窀満劂窋檎窑媒麒犓芔嚙芚暚躚窟媟嚥度蚩溯窰媱誳暵蚶庶媸皺芻雋滍囏黐拑廑勒囓勘拙雛黜諜廝滞雟離囤勩黮滯櫱苷盹廿滿謀茁謅匈鬉匋漌輌朏漓朔欛朞蜞期謠謡弢錤匩鬯鬱伳缶鬷霸缸笹欺缺猺缻缼弽缽缾缿罃罄罅罇卋魌罌鍍罍罎罏罐鍐魑齒齓齔齕鍖齖獗齗齘卙齚齝齞齟齠齡齢齣齦齧魨齨坩革齩靪齪靫齬靭齭靮齮歯齯齰鍱靱齱齲靳齳靴齵杶蝶靶齶靷齷靸靹齺靺靻孼靼靽齽靿鞀鞁鞄鞅鞆羇羈鞉鞋澌讌鞍鞏掏鞐箕鞕鞖鞗掘鞘鞙鞚鞜箝鞞瞞鞟鞠鞢鞣殣掣厥辥鞦鞨厪鞫鞬鞭徭螭厮鞮鞱鞲鞳鞴玵鞵鞶鞸鞹鞺枻枼鞼枽鞾鞿韁韃韄韅韆韇韈韉韊鏋迍蟎篏某柑揕蟖韘寚迣柮揲忳寶韷基揺蟿"));
        PART_TO_KANJILIST.put('凶', new Part(1, "離璃兇匈悩籬檎黐魑擒漓脳凶椶洶胸离禽恟"));
        PART_TO_KANJILIST.put('臺', new Part(1, "擡薹臺"));
        PART_TO_KANJILIST.put('出', new Part(1, "堀咄屈柮朏倔糶掘拙出崛黜窟祟"));
        PART_TO_KANJILIST.put('臼', new Part(6, "舀舁樁舂舃堄舅欅與興舉瀉猊舊萏霓夓爓輗攛搜謟猟鼠帠餡輡鼡鼢鬣鼦瘦鬩鼪倪攪鼫鼬鼯搯焰鼱鼲唲鼴鼷萸鼹鼺蜺稻輿欿饀轁歃慆蹈蝋摏牐兒桕啗艘癙捜鑞歟潟鍤睨齨瑫譭齯楰腴獵襷陷學譽鑿傁憃鶃膄隉螋瞍貎掐麑窞冩鞱溲嚳覺閻嶼庾毀釁諂惂嫂竄燄藇淊廋黌揑插裒滔臘諛韜叟鯢盥惥擧痩寫燬磶臼鷽臽臾臿"));
        PART_TO_KANJILIST.put('函', new Part(1, "涵函"));
        PART_TO_KANJILIST.put('臾', new Part(1, "腴萸諛臾"));
        PART_TO_KANJILIST.put('刀', new Part(2, "刀刃刄刅分切瘈怊砌蠏蠐樑頒訒栔刕鈖簗氛瘛娜初鰡瘤刧稧吩昭騮刱券刼籀虀衂噄鉊剏湓詔籕遛葜恝籟瑠牣穧扨癩剪扮偰剱屶屻籾汾芀梁貂邂窃纃麄劈粉躋嚌認劑劒劔岔梛溜檞邠蚡那炤肦貧芬劭躮隮粱邵躵沼貿蛁盆苆拐囓軔竕苕招仞擠廨哪髫瓫仭瓰雰囹軺棻棼盼份脃挈紉脋儕茘紛鼢霤鼦弨挪謭鄮洯朷紹霽攽嬾餾彅譅齊齋佋坌齎齏契楔潔留酚彛魛彜絜齠鍥照齧獬靭歰靱卲荵魵獺澀鞀超鮉禊枌綛玢薢冤澥鎦辧喫嶰榴枴鶹薺辺玿釁懈忉韌忍臍釖濟寡迢解叧叨召釰韲揳韶懶蟹釼藾忿"));
        PART_TO_KANJILIST.put('言', new Part(7, "言戀訂訃訄訅訇計訊訌討訏訐蘐訑鸑訒訓訔訕訖託記這舚訛訝鸞訞訟訠訢訣訤訥訦訪訫訬設訯許訳訴訵訶簷訷渹診註蠻証訽訾詀詁詃詅詆詇詈詉詍詎詐詑詒詓詔評詖詗詘詛詜詝詞詠噡詡詢詣灣灤詥試詧幨詩詫詬詭詮詰話該詳癴詵詶詷詹詺詻詼鑾詾詿誀誂誃誄誅誆誇誉誋誌認邎誏檐誐誑誒誓誕誖誗誘誙誚語纞躞誟誠傠誡誣誤誥悥誦誧誨誩説読誮誯誰課誳誶誷誹誻誼誾調諂諃諄諆談諈諉諊請諌諍諏諑諒諓擔諔諕論諗諚諛諜諝諞諟諠諡諢諤諦諧櫧諫曫諬諭諮諰諱諳諴諵黵諶諷滸諸諺狺諼諾諿謀謁唁圁謂謄謅霅謆謇謊儋謋謌贍謎蔎謐謑欒謔謖謗謙謚講謜謝圝謞謟謠謡攣謦謨謫謬謭謰儲謳謷輷謹謼謾譁譂譃靄獄譄譅譆譈證譌孌譍彎譎譏譒譓譔譖識譙譚譛卛譜襜譞譟譣警鍧譫譬譭譯議罰譱譲譴譶護罸譸譹譼譽譾孿讀讁讃讄讅變讋讌讍讎讏讐讒讓讔讕讖讙讚讜薝讞讟鞫辯澹憺瞻嶽膽揈巒矕臠信燮藷藹蟾"));
        PART_TO_KANJILIST.put('威', new Part(1, "威鰄縅"));
        PART_TO_KANJILIST.put('刂', new Part(2, "鬁刂鰂刈愈愉瘉刊蜊刋鬋洌瘌刎唎鬎瀏刑倒划刓刔渕褕刖列刘猘渝椡刢茢判別刨利刪測匬謭刮帰到踰崱刲刳刵制刷輸刹刺刻逾偂剃剄剅彅則剉荊削剋捌剌前煎硎牏癒蝓剔湔剕剖剗剘鍘剚剛瑜剜剝剞剟剠楡剡剣剤剥葥鵥剦腧剩剪葪兪鍮剮副剰罰浰割蝲剳側剴創鉶剷偸剸剹剽罽譾剿劀犁溂劂莂劃隃劅喇劇莉劉薊劊型例劌劍貐劑劓劕劖劗誗劘箚劚厠嚠掣覦悧咧梨辨喩窬箭羭蒯喻冽劽莿廁裂揃揄苅烈鯏俐鋓釗蛚淛痢櫤翦諭班鯯峲姴擶毹惻製迾"));
        PART_TO_KANJILIST.put('樂', new Part(1, "鑠樂轢藥礫擽爍櫟"));
        PART_TO_KANJILIST.put('刃', new Part(1, "刃荵綛忍認靭籾"));
        PART_TO_KANJILIST.put('舄', new Part(1, "舄瀉寫潟"));
        PART_TO_KANJILIST.put('刄', new Part(1, "剱靱衂刄劔扨釼仭剏"));
        PART_TO_KANJILIST.put('分', new Part(1, "寡釁分盆貧吩粉枌芬扮瓰雰頒岔竕氛紛彜汾忿"));
        PART_TO_KANJILIST.put('與', new Part(1, "欅與擧襷舉嶼譽歟"));
        PART_TO_KANJILIST.put('切', new Part(1, "窃切砌"));
        PART_TO_KANJILIST.put('戈', new Part(4, "娀威簂鰄縅戈戉戊戌戍娍戎嘎成踐我戒划戓鰔戔戕或鐖蠘戚減戛蠛戜戝戞戟戠戡鐡戢戣娥戦戧戩截戫縬戮戯戰嘰栰戲戳戴樴爴鐵栽樾堿聀繊衊牋摑顑織籖穖驖幗剗蹙籛鉞機晟幟桟晠穢顣艤籤噦硪顪牫幭牮鑯葳葴鱵職鹹幾找悈肈碊銊劌纎誐纖媙檝誠犠誡璣檥犧銭械嚱碱咸撼裁軄滅哉賊賎惑擑裓諓烕郕烖狘盛盞僟賤哦曦棧泧峨鋨峩棫黬鋮磯諴櫼蛾軾胾儀茂贇載國漍儎伐蔑礒餓贓餞感錢蜮崴蔵茷錻孅浅蕆譏筏饑魕轗荗識鵝鵞鵡絨獩襪筬罭奯議楲歳襳蕺鍼慼畿荿醆薉越喊箋残醎膕讖殘馘垡羢閥禨義莪綫嶬宬殱羲殲箴憾閾熾鞿俄巇韈濈蟈濊城緎鯎藏减臓緘旘蟙域臟淢蟣韤臧毧槭韯觱韱懴珴駴践珹臹濺淺懺蟻翽篾"));
        PART_TO_KANJILIST.put('刈', new Part(4, "苅刈"));
        PART_TO_KANJILIST.put('戉', new Part(1, "戉越鉞"));
        PART_TO_KANJILIST.put('戊', new Part(1, "茂戊"));
        PART_TO_KANJILIST.put('樊', new Part(1, "攀樊礬"));
        PART_TO_KANJILIST.put('戋', new Part(1, "浅践戋残銭桟"));
        PART_TO_KANJILIST.put('舌', new Part(6, "憇筈憩闊舌恬括舍刮舐乱話舒聒舖濶舘活銛甜蛞辞"));
        PART_TO_KANJILIST.put('戌', new Part(1, "威鰄滅縅韈喊戌戍譏蔑饑鰔轗緘減感機穢襪磯歳箴咸鹹撼鍼憾幾畿"));
        PART_TO_KANJILIST.put('舍', new Part(1, "舒舖舘舍"));
        PART_TO_KANJILIST.put('戍', new Part(1, "威鰄縅滅韈喊戌戍譏蔑饑鰔轗緘減感機穢襪磯歳箴咸鹹撼鍼憾幾畿"));
        PART_TO_KANJILIST.put('舎', new Part(1, "捨舎"));
        PART_TO_KANJILIST.put('討', new Part(1, "罸討"));
        PART_TO_KANJILIST.put('戎', new Part(1, "絨賊戎"));
        PART_TO_KANJILIST.put('成', new Part(1, "成誠盛筬城晟"));
        PART_TO_KANJILIST.put('刑', new Part(1, "刑荊型"));
        PART_TO_KANJILIST.put('我', new Part(1, "儀犠俄艤娥哦曦犧峨峩義莪嶬議我礒羲餓蟻鵝蛾鵞"));
        PART_TO_KANJILIST.put('戒', new Part(1, "械誡戒"));
        PART_TO_KANJILIST.put('戔', new Part(1, "錢賤棧牋箋綫賎踐戔殘濺淺盞餞"));
        PART_TO_KANJILIST.put('或', new Part(1, "惑摑膕或幗馘國閾域"));
        PART_TO_KANJILIST.put('列', new Part(1, "裂列烈例洌冽"));
        PART_TO_KANJILIST.put('娚', new Part(1, "嫐嬲娚"));
        PART_TO_KANJILIST.put('戚', new Part(1, "蹙戚槭"));
        PART_TO_KANJILIST.put('舛', new Part(7, "桀椉橉謋驎燐憐傑磔轔鱗璘嶙舛儛撛舜桝舞麟僢蕣隣粦搩瞬僯鄰僲斴遴瞵磷曻鏻粼潾"));
        PART_TO_KANJILIST.put('舜', new Part(1, "蕣舜瞬"));
        PART_TO_KANJILIST.put('稜', new Part(1, "薐稜"));
        PART_TO_KANJILIST.put('舟', new Part(6, "輈螌舟舡瘢舢舨舩航舫搬般舮舲舳舴舵舶鞶舷舸船舺媻艀槃艃鵃艄艅艆艇幋艋坍艎艏磐艑艖艘艙艚艜艝艟艠艢艣盤艤彤艦艧艨艪矪艫艭"));
        PART_TO_KANJILIST.put('稟', new Part(1, "廩凜懍稟"));
        PART_TO_KANJILIST.put('戠', new Part(1, "戠織職識"));
        PART_TO_KANJILIST.put('別', new Part(1, "別捌"));
        PART_TO_KANJILIST.put('利', new Part(1, "俐犁痢悧梨利莉蜊鯏"));
        PART_TO_KANJILIST.put('爪', new Part(4, "嬀舀鰀鸂稃将蜉笊蘐謑餒渓猙錚愛舜爝娞瀞謟謡嬡崢琤猨鰩爪霪爫爬脬爭搯爯爰稱稲爲爴爵鼷猺稻挼谿蔿艀饀偁轁慆婇繇蹈嵈靉蹊捊捋譌穏煖瑗潙奚孚靜睜嵠虢蕣遥楥罦奨彩穩瑫鱫婬睬浮癮噯蝯鍰湲乳桴孵桵瑶酹酻荽乿蕿亂檃薆螇授蒋殍膎箏鶏綏傒抓覓讔暖鞖掙暚媛枛傜撝馟採醤辤妥璦皧莩溪瞬綬辭皭徯粰隱媱鞱綵鞵覶瞹颻嚼墾鮾閿俀寀釂鷄懇采凈棌諍磎埒滔曖受俘嗘郛菜韜鋝僞雞僢淨緩淫竫埰援揺諼寽櫽釽僾"));
        PART_TO_KANJILIST.put('般', new Part(1, "磐瘢槃盤般搬"));
        PART_TO_KANJILIST.put('爭', new Part(1, "崢淨錚靜爭諍瀞箏"));
        PART_TO_KANJILIST.put('到', new Part(1, "到椡倒"));
        PART_TO_KANJILIST.put('爰', new Part(1, "爰湲援暖煖緩媛"));
        PART_TO_KANJILIST.put('許', new Part(1, "許滸"));
        PART_TO_KANJILIST.put('爲', new Part(1, "爲譌僞"));
        PART_TO_KANJILIST.put('鈴', new Part(1, "鈴鈴"));
        PART_TO_KANJILIST.put('爵', new Part(1, "爵嚼"));
        PART_TO_KANJILIST.put('父', new Part(4, "鵁較餃效郊賋挍皎滏纐珓恔窔傚釜絞蛟虠校狡姣交跤斧洨齩鮫咬茭駮俲父胶爸鉸効爹骹爺佼"));
        PART_TO_KANJILIST.put('制', new Part(1, "掣制製"));
        PART_TO_KANJILIST.put('戸', new Part(4, "謆肇昈肈炉褊粐錑妒斒傓傖椖涙騙撝綟芦枦徧肩鈩綮舮掮唳愴搶悷戸阸騸戹萹戻戽匾房馿所扁蹁扃滄糄扆篇雇扇扈扉藊遍偏艑啓牖蝙棙諞顧編棨捩翩滬僱晵惼煽鍽鯿"));
        PART_TO_KANJILIST.put('爻', new Part(4, "攀駁斅邇鑈樊彌黌壐礐覐蠒燓濔蠜欝鷞礟鱟訤棥穪攪礬嬭獮瀰禰嚳學覺爻襻爼獼覼璽鷽爽爾薾镾"));
        PART_TO_KANJILIST.put('戻', new Part(1, "唳捩涙戻綟"));
        PART_TO_KANJILIST.put('爾', new Part(1, "瀰禰濔邇彌璽爾"));
        PART_TO_KANJILIST.put('爿', new Part(4, "冀肅将將嶈弉莊蒋焋讌鼎鼏墏鼐鼑鼒贓戕北妝瀟蔣醤娤嚥匨蠨簫醬嘯斲状窹銺爿漿螿埀牀牁牂牃装牅牆彇背驌獎藏寐鱐燕乖鏘乘荘奘臙潚糚寝裝臟繡蹡寢婣寤驥臧奨剩奫鷫奬蕭壮壯寱槳鏽"));
        PART_TO_KANJILIST.put('婁', new Part(1, "婁僂鏤藪簍髏樓籔窶縷數褸瘻螻楼"));
        PART_TO_KANJILIST.put('扁', new Part(1, "扁篇編翩蝙騙褊遍諞偏"));
        PART_TO_KANJILIST.put('片', new Part(4, "肅片彇版牋牌驌鼎牎鼏牏鼐牐鱐鼑鼒牒牓牕牖牘潚沜瀟繡婣蠨簫奫鷫蕭嘯魸鏽"));
        PART_TO_KANJILIST.put('則', new Part(1, "厠廁側則惻測"));
        PART_TO_KANJILIST.put('扇', new Part(1, "扇煽"));
        PART_TO_KANJILIST.put('扈', new Part(1, "扈滬"));
        PART_TO_KANJILIST.put('手', new Part(4, "儀攀撃欅挈看掌挐誐我撑礒餓掔挙犠掣攣娥檥犧劧義莪嶬羲搴芼搿湃俄揅擊手擎拏擘拜鵝鵞艤擥哦曦藦擧峨鋨峩摩擪硪摯議拲拳珴癴擵襷魹蟻襻蛾承拿"));
        PART_TO_KANJILIST.put('扌', new Part(3, "搆搉搊損簎搏搐搒搓搔搖搗搘搜逝搞搠搢搤搥搦搨搩搪搬搭搯搰搵搶携搽搾籀摂摋摎摏摑摒籒摓摔摘摚摛摜籜摝摟摠摡籡摣摧摭摳摴摶摸摺豺摻摽撅撇撈悊撏撐咑撑撒撓撕撘撙撚撛撝撞撟撡財撣撤撥撦撨撩撫撬播撮撰蒱撲撳撹撻撼撽撾撿擁擂擄擅擇擉擋擌操擐擑擒擔擕擗據擠擡擢擣擤擦擩擬擭擯擰擱哲擲擴擵擶擷蓷擺擻擽擾擿攁攄攅攈攉攊攏攓攔攖攘攙攛攜攝攞攟攢攤攦攩攪攫攬攮浙啦閉嶊蘀踅娎湃扌才扎扐扑扒打扔払扖托扚扛扜扞扠晢扣扤扨扭扮扯扱扳扶批扺扼扽找技抂抃抄抉把抍抎抏抐抑抒誓抓抔投抖抗折抛抜択抦抨庪披抬抱犲抳抵抶抷抹抺抻押抽抾抿拂拄担拆拇拈拉拊拌拍拎拐拑拒拓拔拕拖拗拘拙拚招拜拝拠拡拪括鋬拭拮拯拱拴拵拶拷拼拽拾狾持挂挃挄欅指蜇按挊挋挌挍挑挓挖挘挟挧挨挩挪挫挭振挵挶挹挺挼挽挾挿捁捂捃捄捆捉捊捋捌捍捎捏材捐捒捓捔捕捗捘捛捜捥捦捧捨捩捫捬捭据捱捲捴捵捶捷捸捺捻捼捽捿掀掂掃掄掇授掉箉掊箍掎掏掐排掕掖掘掙掚掛箝掞掟掠採探掤接掦控推掩措掫掬掭掮掯掲掴掵掻掽掾揀揁揃揄揆揈揉揎描提揑插揓揔揕揖揚換揜菝揠握揣揥揩揪揬揮毮揲揳援揵揶揸揹揺篺"));
        PART_TO_KANJILIST.put('剌', new Part(1, "溂喇剌"));
        PART_TO_KANJILIST.put('前', new Part(1, "揃擶翦剪前箭煎"));
        PART_TO_KANJILIST.put('才', new Part(1, "材財犲閉豺才"));
        PART_TO_KANJILIST.put('積', new Part(1, "癪積"));
        PART_TO_KANJILIST.put('牙', new Part(5, "呀概雅嘅竆岈鴉姊琊墍撐砑漑捓迓橕齖牙牚蚜訝既鋣慨厩邪笫冴柹谺芽穿"));
        PART_TO_KANJILIST.put('牛', new Part(4, "蠏猘洙舜鈝造樨株朱茱制眸吽鴾恈穉告桙牛牜牝牞牟靠鵠牠牡牢蕣牣慥晧牧牨浩物牫獬牮牯牱牲遲牴酷剷牷牸特牻牼牽鉾牾牿墀犀犁犂邂窂犄誅犇犉殊犍犎侏梏犒皓犓侔澔銖窖犖犛檞犠犢薢掣誥澥犧犨瞬嶰麰嚳劺懈蛑觕蛛淛珠惣解廨迭鯯件蟹製"));
        PART_TO_KANJILIST.put('牟', new Part(1, "眸桙鉾鴾牟"));
        PART_TO_KANJILIST.put('物', new Part(1, "惣物"));
        PART_TO_KANJILIST.put('艮', new Part(6, "銀退攁概螂稂琅貇很餈踉簋蜋餌誏漑匓榔朖朗娘茛欝喞垠悢莨厩褪羪閬嚮粮爵根嚼眼墾節曁鋃艆懇廊埌俍郎廏限廐籑饔痕饗櫛饜跟狠硠飡既筤豤齦飧慨恨懩浪艮瑯良艱即鱶郷佷桹烺狼飼腿卿響"));
        PART_TO_KANJILIST.put('良', new Part(1, "螂琅莨踉廊浪粮郎良瑯榔朖朗娘狼"));
        PART_TO_KANJILIST.put('色', new Part(6, "脃赩銫栬鑱色灔艴艶絶艷蕝饞"));
        PART_TO_KANJILIST.put('穴', new Part(5, "突窃邃窄窈邊椌窒窓熔鎔榕窕窖窗窘箜窟膣倥控窩窪鴪窮窯窰溶窶容窺搾悾窿穃竃竄竅竇竈蓉竊啌腔穴究穹坹空穽穿"));
        PART_TO_KANJILIST.put('艸', new Part(1, "鄒艸趨皺芻雛蒭"));
        PART_TO_KANJILIST.put('艹', new Part(1, "蠆蠎蠓蠖蠚蠛瀟蠣蠨偀衊灌偌桒塔灘塟硴顴恾邁墓備碤墳墸磁僃僅飌壒僨惹棻鄀儆鄒儚礚鄚椛餝礞夢礪焫礴儺慈饉楉慌煐慕饙楛饛慝襪襺襼襽憊膜憤冪覲膵憼觀燁懂懃臈燌臓燕臗臙懜懞臟懽槿爇舊爗爟模爤樺驀驊驚艧艨驩穫艱剳艸艹艻艽艾艿芀芁芃芄芇芉芊芋芍芎芑芒芔媖芖芘芙芚芛芝芟檠芠芡芣芤芥芦芧芨芩芪芫檬芬芭芮誮芯芰花芲芳檴芴芷芸芹芺芻芼芽芾芿苅苆苐苑苒髒苓苔櫔苕髖苗苙苚苛苜苞募苟苠苡苢苣勤苤若苦苧苨鋩苪苫苭苯英勱苳苴勵苶苷勸苹苺苻苽諾苾茀茁茂欂范茄茅茆茇茈茉權茊謊茋茎茖茗欗茘茛茜茝茞欟茟茡茢茣錨茨謨猫茫茬茭茮茯茰茱茲茳茴錵茵茶茷茸謹茹錺茺茼茽匿荀譁荂荃荄荅荇鍈草荊荍歎荎荏獏荐荑荒荓荔荕獖荖荗荘歡警獦鍩荰獲孳荳荵荷護荸荻荼孽荽荿莀莂莄莅莆莇莉莊讌莍莎莒莓莔莕莖莘讙莙莚莛莜莝莞莟莠莢莦莧莨莩莪莫箬莬莭莱厲莵莽莾莿菀菁菅菇菉菊菌鏌菎描菏菑菓菔菖菘寛菜菝寞菟菠菨菩菪菫華菰菱菲菴菶菷菸菹菻菼菽萁萃萄萆萇萊萋萌萍萎萏萑萓萕萙鰙萠萢萩萪萬搭萯鐯萱萵萸萹萼落搽葅葆籆葇葈葉葊鑊葍葎葏葑葒葖著葘葙葚瑛葛葜鱝葠葡葢董葤葥葦葧葩葪葫葬葭葮鑮葯葰葱葳葴葵鑵葶豶葷摸葸葹葺葼葽瑾蒁蒂蒄钄蒅蒋蒐蒒蒓貓蒔蒕貘蒙貛蒜蒞蒟蒡蒦蒨蒩蒪蒭蒯蒱蒲蒴蒸蒹蒺蒻蒼蒽蒾蒿糀蓀蓁賁蓂蓄蓆蓇蓈蓉蓊蓋蓌蓍哎擎蓏蓐蓑糒蓓蓖瓘蓙蓚糚蓜糢蓧蓪蓬擭蓮蓯蓰蓱糲蓲蓴糵蓷蓺蓻蓼蓽蓿蔀蔂蔃蔆蔇蔌甍蔎蔐蔑蔓蔔蔕蔗蔘蔚蔜蔞蔟蔡蔢蔣攤蔤蔥蔦蔧蔪蔫蔬蔭蔯蔲蔳蔴蔵蔶蔽蔿蕀蕁蕃蕆蕈蕉蕊蕋蕎蕏蕐蕑蕒蕓蕕蕖蕗蕘蕙蕚蕜蕝蕞蕟蕠蕡蕢蕣蕤蕨蕩蕪蕫敬蕭蕯蕷蕹蕺蕻蕽蕾蕿薀薁薄薅薆薇薈薉薊薋薌喏薏薐薑薓鶓薔薗薘薙薛薜薝薟薠薢薤薥薦薧薨趨薩薪薫薬薭薮薯薴薶薷薸薹薺薼薽薾薿藁藂藇藉藊藋藍藎藏藐嗒藕藘藚藜藝藟藠緢藤藥藦藨藩藪藭藳藶藷藹藺藻藼藾藿蘀蘂蘄蘅嘆蘆蘇蘊蘋蘍蘎蘐蘑蘒蘓蘖蘗蘘蘙蘚鸛蘛蘞蘡蘢蘧嘩瘩蘩蘭渮蘯蘰渶蘶蘸蘺瘼蘼蘽蘿虀虁虂虆幕癘幪繭幭噴嚄嚆躇躉暎皣嚥躪暮暱皺嚾庿曄囈滋囌曔雘曚雚雛難蛬滭鼖霙漠朠漢朦漭蜹罐轒轕彠轥睰羃瞄瞢瞱鞳鞴鞾韄蟆濆矇蟇韈韉韊蟒矒埖濛韛韡韤濩韮矱韺"));
        PART_TO_KANJILIST.put('空', new Part(1, "腔倥控空啌椌箜"));
        PART_TO_KANJILIST.put('艾', new Part(3, "蠆蠎蠓蠖蠚蠛瀟蠣蠨偀衊灌偌桒塔灘塟硴顴恾邁墓備碤墳墸僃僅飌壒僨惹棻鄀儆鄒儚礚鄚椛餝礞夢礪焫礴儺饉楉慌煐慕饙楛饛慝襪襺襼襽憊膜憤冪覲膵憼觀燁懂懃臈燌臓燕臗臙懜懞臟懽槿爇舊爗爟模爤樺驀驊驚艧艨驩穫艱剳艸艻艽艾艿芀芁芃芄芇芉芊芋芍芎芑芒芔媖芖芘芙芚芛芝芟檠芠芡芣芤芥芦芧芨芩芪芫檬芬芭芮誮芯芰花芲芳檴芴芷芸芹芺芻芼芽芾芿苅苆苐苑苒髒苓苔櫔苕髖苗苙苚苛苜苞募苟苠苡苢苣勤苤若苦苧苨鋩苪苫苭苯英勱苳苴勵苶苷勸苹苺苻苽諾苾茀茁茂欂范茄茅茆茇茈茉權茊謊茋茎茖茗欗茘茛茜茝茞欟茟茡茢茣錨茨謨猫茫茬茭茮茯茰茱茲茳茴錵茵茶茷茸謹茹錺茺茼茽匿荀譁荂荃荄荅荇鍈草荊荍歎荎荏獏荐荑荒荓荔荕獖荖荗荘歡警獦鍩荰獲孳荳荵荷護荸荻荼孽荽荿莀莂莄莅莆莇莉莊讌莍莎莒莓莔莕莖莘讙莙莚莛莜莝莞莟莠莢莦莧莨莩莪莫箬莬莭莱厲莵莽莾莿菀菁菅菇菉菊菌鏌菎描菏菑菓菔菖菘寛菜菝寞菟菠菨菩菪菫華菰菱菲菴菶菷菸菹菻菼菽萁萃萄萆萇萊萋萌萍萎萏萑萓萕萙鰙萠萢萩萪萬搭萯鐯萱萵萸萹萼落搽葅葆籆葇葈葉葊鑊葍葎葏葑葒葖著葘葙葚瑛葛葜鱝葠葡葢董葤葥葦葧葩葪葫葬葭葮鑮葯葰葱葳葴葵鑵葶豶葷摸葸葹葺葼葽瑾蒁蒂蒄钄蒅蒋蒐蒒蒓貓蒔蒕貘蒙貛蒜蒞蒟蒡蒦蒨蒩蒪蒭蒯蒱蒲蒴蒸蒹蒺蒻蒼蒽蒾蒿糀蓀蓁賁蓂蓄蓆蓇蓈蓉蓊蓋蓌蓍哎擎蓏蓐蓑糒蓓蓖瓘蓙蓚糚蓜糢蓧蓪蓬擭蓮蓯蓰蓱糲蓲蓴糵蓷蓺蓻蓼蓽蓿蔀蔂蔃蔆蔇蔌甍蔎蔐蔑蔓蔔蔕蔗蔘蔚蔜蔞蔟蔡蔢蔣攤蔤蔥蔦蔧蔪蔫蔬蔭蔯蔲蔳蔴蔵蔶蔽蔿蕀蕁蕃蕆蕈蕉蕊蕋蕎蕏蕐蕑蕒蕓蕕蕖蕗蕘蕙蕚蕜蕝蕞蕟蕠蕡蕢蕣蕤蕨蕩蕪蕫敬蕭蕯蕷蕹蕺蕻蕽蕾蕿薀薁薄薅薆薇薈薉薊薋薌喏薏薐薑薓鶓薔薗薘薙薛薜薝薟薠薢薤薥薦薧趨薨薩薪薫薬薭薮薯薴薶薷薸薹薺薼薽薾薿藁藂藇藉藊藋藍藎藏藐嗒藕藘藚藜藝藟藠緢藤藥藦藨藩藪藭藳藶藷藹藺藻藼藾藿蘀蘂蘄蘅嘆蘆蘇蘊蘋蘍蘎蘐蘑蘒蘓蘖蘗蘘蘙蘚鸛蘛蘞蘡蘢蘧嘩瘩蘩蘭渮蘯蘰渶蘶蘸蘺瘼蘼蘽蘿虀虁虂虆幕癘幪繭幭噴嚄嚆躇躉暎皣嚥躪暮暱皺嚾庿曄囈囌曔雘曚雚雛難蛬滭鼖霙漠朠漢朦漭蜹罐轒轕彠轥睰羃瞄瞢瞱鞳鞴鞾韄蟆濆蟇矇韈韉韊蟒矒埖濛韛韡韤濩韮矱韺"));
        PART_TO_KANJILIST.put('犀', new Part(1, "犀遲穉"));
        PART_TO_KANJILIST.put('窄', new Part(1, "窄搾"));
        PART_TO_KANJILIST.put('劉', new Part(1, "嚠劉瀏"));
        PART_TO_KANJILIST.put('芒', new Part(1, "芒鋩"));
        PART_TO_KANJILIST.put('窒', new Part(1, "窒膣"));
        PART_TO_KANJILIST.put('折', new Part(1, "哲晢誓折浙逝"));
        PART_TO_KANJILIST.put('力', new Part(2, "愂茄脅舅脇嘉蘍蜐愑挘娚耞阞耡渤甥別椦霧昮耮夯嬲笳踴愶男朸瘸氻伽癆恊筋捌扐協荔荕虜牞慟艣呦湧葧筯坳橳屴艻幼靿莂莇澇窈袈撈墈箉肋沒鶒咖力抛劜功加劣劤劥憥劦辦劧助努劫劬劭劯劰励労蚴劵架劶枷劷劸効劺劻劼侽劽劾賀勀勁痂仂勃懃勄擄勅勆勇忇珈勈勉嗋勌働勍跏勏嫐泐勑泑勒勔觔駕動狕勖拗勗勘務釛勛勜黝勝勞募勠蛠勡磡勢勣鋤藤勤勥迦勦另勧勨勩勪勬飭苭勰勱勲俲勳勴勵勶勷勸竻哿"));
        PART_TO_KANJILIST.put('加', new Part(1, "加賀痂茄迦珈袈嘉跏笳駕架枷伽"));
        PART_TO_KANJILIST.put('骨', new Part(10, "髀髁髃髄磆髆蓇髈髎髏髐滑髑髒髓體髕髖嗗髗骨骪骬骭骮骯骰搰骲骴骵骶骸骹鶻骻骼榾猾骾骿"));
        PART_TO_KANJILIST.put('助', new Part(1, "耡鋤勗莇助"));
        PART_TO_KANJILIST.put('犬', new Part(4, "嚈宊猋伏倏洑錑鸑猒憖撚讞吠殠鶪犬厭漭献犮茯袱紱鮲厴状然猷悷嶽犾栿檿燃闃獃魃獄齅嗅飆跋獎畎繎囐獒壓祓拔懕糗魘默獘黙棙瓛饜秡獣絥靨擪橪揬哭髮黶獸獻黻"));
        PART_TO_KANJILIST.put('犭', new Part(3, "猂猄猅猇猊猍鸑猓猖逖猗猘猙猛逛猜猝猞猟猢猤猥猧猨猩猪漪猫猬猯猱猲猴猵猶猺猻猽猾猿鱁獄獅獍獏蕏獐蕕獖獗獝獞獟鵟獠獦獧獨獩獪獫獬獮獯獰獱獲潴獵獷獹獺荻獼玀玁玃誑犭犯犱犲犴犹嶽墾犾狁狂狃狄狆懇狇狉狌狎狐狒狕狖狗狘狙狛狟狠狡狢狥狩独狭峱狳狴狷狸狹狺狻狼狽狾"));
        PART_TO_KANJILIST.put('花', new Part(1, "糀花硴錵埖椛"));
        PART_TO_KANJILIST.put('芳', new Part(1, "芳錺餝"));
        PART_TO_KANJILIST.put('劵', new Part(1, "藤劵椦勝"));
        PART_TO_KANJILIST.put('芻', new Part(1, "鄒趨皺芻雛蒭"));
        PART_TO_KANJILIST.put('狂', new Part(1, "誑狂"));
        PART_TO_KANJILIST.put('勃', new Part(1, "勃渤"));
        PART_TO_KANJILIST.put('狄', new Part(1, "狄逖荻"));
        PART_TO_KANJILIST.put('勇', new Part(1, "踴勇湧"));
        PART_TO_KANJILIST.put('立', new Part(5, "樀騂戇鸊倍搒倖蘖瘖蘗鐘帝樟戠縡蘢踣瀧渧報昱瘴樴鐴鐸逹稺鐿蹄婄豅鱆顏偐牓織顔癖剖摘偙豙驛幛穜鉝幟艟籠幢蹢偣橦遧鑨適陪噫摯蹱乵職剷幸繹噺聾避蒂境躃躄咅墇劈璋傍檍粒梓暗檗碚障暜撞蒞麞蒟蒡傡岦隦璧嚨嚫颯暲碲隴傹撻傽壁滂軄磅鋅擇曈髈拉竉立竌竍竎瓏竏勏竑竒滓竓棓蓓竕擗擘站苙竚竛竜嫜滝竝壟竟章賠嫡竡竢泣竣瓣童諦竦曨部竨竩竪竫櫬竬竭僮端黯竰櫱竱諳滴竴糵競郶竸諺僻擿瓿蔀贄億椄圉霎攏意蔐甓愔嬖謗鴗焙贛笠餢産鄣朧謫儭礱儱攱漳霹唼嵃商歆位獍獐歒靖鍗識嵜煜襞慞獞彦啦蕫譬譯襯彰襲楴敵啻啼潼孼孽睾罿膀讁嶂垃螃莅醅掊鎊讋龍龏薏龐龑喑龒龔龕殕瞕鶕龖涖龗禘莘辛薛薜辜榜辝辞辟辠辡辣澤辤辥接辦憧辧辨薩薪親涪徬辭辮辯宰新瞳澵憶徸澼妾熾旁臂巃蟄毅臆柆闇翊釋懌翌鏑旘鷙蟙槞鏟締鏡闢翣闥揥菨菩篭音韴寴寵韵燵韶執韷韸培韺韻鷾響"));
        PART_TO_KANJILIST.put('拑', new Part(1, "拑箝"));
        PART_TO_KANJILIST.put('竒', new Part(1, "竒嵜"));
        PART_TO_KANJILIST.put('動', new Part(1, "勲動働慟"));
        PART_TO_KANJILIST.put('苗', new Part(1, "苗錨猫描"));
        PART_TO_KANJILIST.put('高', new Part(10, "藁槁嗃毃嚆熇滈歊膏傐犒鄗高塙篙髛皜髜停縞髞搞謞渟暠嵩嵪鎬亭鶮翯敲藳碻稾稿蒿"));
        PART_TO_KANJILIST.put('務', new Part(1, "霧務"));
        PART_TO_KANJILIST.put('苛', new Part(1, "苛渮"));
        PART_TO_KANJILIST.put('竜', new Part(10, "巃豅嚨曨鑨竉讋篭攏龏龐礱龑龒龔龖龗竜滝槞"));
        PART_TO_KANJILIST.put('拜', new Part(1, "湃拜"));
        PART_TO_KANJILIST.put('勞', new Part(1, "癆撈勞"));
        PART_TO_KANJILIST.put('髟', new Part(10, "鬀鬁鬂鬃鬄鬅鬆鬈鬉鬋鬌鬍鬎鬐鬒鬖鬘鬙鬚鬛鬜鬟鬠鬢鬣髟髠髢髣髤髥髦髧髩髪髫髬髭髮髯髱髲髳髴髵髷髹髺髻髽髿"));
        PART_TO_KANJILIST.put('苟', new Part(1, "檠警驚敬苟"));
        PART_TO_KANJILIST.put('竟', new Part(1, "鏡境竸竟"));
        PART_TO_KANJILIST.put('章', new Part(1, "章彰嶂瘴鱆璋障樟"));
        PART_TO_KANJILIST.put('勤', new Part(1, "懃勤"));
        PART_TO_KANJILIST.put('若', new Part(1, "若惹慝諾匿"));
        PART_TO_KANJILIST.put('童', new Part(1, "幢瞳童橦憧鐘潼僮撞艟"));
        PART_TO_KANJILIST.put('英', new Part(1, "英霙瑛暎"));
        PART_TO_KANJILIST.put('諸', new Part(1, "儲藷諸"));
        PART_TO_KANJILIST.put('勹', new Part(2, "考萄耇耈瀉搊蠋簌蠍堍砍萏蠏鰐怐預鰑瀞頠萢琤急尦栨栬萯怱栲砲蠲鐲場逷逸吹吻萼逿局恂桅偈汋遌桍瑍顎遏恑偒瑒灔鱖恗聘葛恟衡象葡恣聦葪豫偬屬遬确葯葱鑱塲呴摴桷豹灼偽袀邂钃撅咆炊邌袍沕悗梚蒟沟負咢粢碣岣粤您咨蒭碭炮颮傯炰撳袴粷傷肹颺為惂僃郇資瓈擉瓊峋擎像擔惕峗惚賜胞惞僞瓟瓠泡惣烤飮胯賯飲磶瓷胸惸裼飽崁鄂脃約脆儆椈餈儋愌蔌褐夐鄒愒愓焔蔔愕脗攙褜鴝鴞鄠愡鄢蔫焰餲儳餳洵洶甸夸甹崿洿礿畃浄畇絇楊酌嵌免奐嵑兔饕啗蕚襜絝蕝煞饞襡絢楤鵤煥赥煦蕨赩蕩敬楬煬腭慯襰絶蕷腸祹絻浼畼慾妁趂綉薋鶍趑禓膓嶔冕写鶚喚斛喝薝憠鶡薢喣斣冤薥趦趨冩綯嶰馰疱喲妳趵鶵憺閻憼膽痀姁凂燄臅緆臈懈巉淊跑角駒觔跔觕闕觖觗觘淘燙觚觜藜觝解釣觥触觧巧跨觩跪觫緫旬痬觭燭觱姱槲臲觳觴淴觶觸觹藹跼觽臽嗽觿懿姿昀騁丂縄蘅渇訇娉瘍鈎与刎縐丒昒蘒易爓瘓舓樕樗渙舚昜鈞踢別刨娩昫蘯爲刳嘴騶渹瘹訽婅鉋牎穐陒剔驚扚橛虝號橡噡詢噣鉤陥虧幨鉨晩物詭湯色艴艶陶蹶艷陷晹詹陽詾亀麁劂犂的躅劅麅誇争芍暍犓纔劖庖暘銙劚檞窞嚟檠芡暢銫蚫劬麭皰抱骲芴麴麹皺芻骻竃鋆勉諊雊廌黎囑髑曔狗盗拘雛盜苞廞苟軟諡仢諤狥黧廨勨雩盪囫竫竭嫮諮髱櫲黵拷曷勹雹勺盻勾勿蜀匀謁匁匂欃匃鬄包朅謅匆匈匊匋匌匍匏笏匐匑輓匓蜔眗匛欠次輡欣笣欤欧茨欨錫匫欫欬圬鬮欯漱笱欲猲蜴圴眴欵輵欶欷輷欸欹鬺欺欻缼挽朽圽欽款欿荀齁荂歃齃靄歆歇均杇歉歊捌譌歌筍歍蝎歎坎歐歒潒杓歓鍔歔捔轕歖睗獗歘静歙鍚歛筝歝佝歟潟歠筠歡魡齣靤警獦鍧獨蝪譫獬靮鍯危杴彴齶佹齺佽歾卾蝿歿掀厃鞄构徇殉垉侉掏宐掐鮑讒玓侚澚龜龝垝龞鮠鞠羡龡殤澥厥掦鞨羨鞫莬掬鮬羯箰掲枵侷鮸枸枹瞻玽濁寃埆揈鏉韉菊篏寏揔蟕矚毚揚俛換俜菟鯣珣句矦埩寫毱韵号埸蟹忺忽俽"));
        PART_TO_KANJILIST.put('竹', new Part(6, "簀簁簂簃簆簇簉簋簌簍簎簏簑簒簓簔簗簙簛蠞簟簠簡簣簥簦簧簨簪簫簬簱簳簴簶簷簸簹簺簽簾簿籀籃籆籊籌籍籏籐籑籒籓籔籕籖籘籙籚籛籜籞籟籠籡籣籤癤籥籧籩噬籬籭籮籰籲鱵纂撘檱擌櫛櫤擶竹竺竻竽竾竿笂笄笆笇笈笊笋笏笑笔笘笙笛笞笟笠笣笥符笧笨笩笪笫第笭笮笯笰笱笳笴笵笶笹笽笿筀筁筅筆筇筈等筋筌筍筎筏筐筑筒答筕策筝筠筤筥筦筧筩筪筬筭筮筯筰筱筲筳筴筵筷筺箄箆箇箉箋箍箎箏箐箑箒箔箕箖算箘箙箚箛箜箝箞箟箠管箥澨箪箬箭箯箰箱箲箴箵箶箸箺箻箼箽節篁篂範篅篆篇篈築篊篋篌篏篔篖篗篙篚篛篝篠篤篥篦篨篩篪篭篲篳篴篵篶篷篸篹篺篼篾"));
        PART_TO_KANJILIST.put('勺', new Part(1, "妁杓釣的約豹勺灼酌芍葯"));
        PART_TO_KANJILIST.put('勾', new Part(1, "勾鈎"));
        PART_TO_KANJILIST.put('勿', new Part(4, "犂鬄匆綉鶍瘍刎笏鰑昒易膓愓禓舓沕脗暘昜愡暢踢殤掦錫匫碭蘯怱餳蜴場芴傷逷颺鬺吻圽逿緆楊黎牎偒瑒剔揔惕睗燙揚惚鍚賜藜虝鯣惣楤聦蕩物盪蝪囫緫偬煬痬慯湯鍯葱塲觴淴腸埸晹裼畼陽忽歾勿"));
        PART_TO_KANJILIST.put('匀', new Part(1, "匀韵均鈞"));
        PART_TO_KANJILIST.put('謁', new Part(1, "謁靄藹"));
        PART_TO_KANJILIST.put('匂', new Part(1, "謁匂碣靄歇渇偈臈鞨蠍竭蝎羯遏褐掲曷藹葛喝"));
        PART_TO_KANJILIST.put('包', new Part(1, "鞄包咆垉鉋袍匏鮑庖胞苞泡萢靤蚫麭炮皰抱疱髱砲枹雹飽"));
        PART_TO_KANJILIST.put('匆', new Part(1, "怱愡葱匆偬"));
        PART_TO_KANJILIST.put('匈', new Part(1, "洶匈胸恟"));
        PART_TO_KANJILIST.put('匍', new Part(1, "葡匍"));
        PART_TO_KANJILIST.put('匐', new Part(1, "匐蔔"));
        PART_TO_KANJILIST.put('匕', new Part(2, "老考頃耄者耆耇耈耊耋砌蠍簏砒搘瀘吡砣琥瀦砦堦琨怩吪鰭鐯栲堲栳頴簴堵琵鐻尼頾屁偈恉塊屍遏灑偕偖著鑙籚顚葛鱜摝遞塟屠池塡鑢呢硣摣鑣豦籧鱨鱪鑪豬葬籭聭鱰呰瑰顱瑱鑱硴塵鱸葹豼遽傀悂咃粃肄梍墍傎璏蒐邐貔沘袘炛咜墟碣貤咤撦貨璩沱貲沲邶墸咾傾糀擄磇磈背磌蓍棍瓐胒胔蓖據壚泚瓛烤泥擬賭哮壱擺都能脂攄鴇攈褐崑鴕紕儗甗礙攙褙褚椛餛鄜鴜焜攟攦褦愧紪紫褫儲鴲儳儷椸愼餽紽絁酈嵊態蕏酏饕教饞酡奢嵬赭楮煮慮奲酵楷她奼鵾冀斃喈熊薌疑疕膚禛醜喝妣薧薨覩妮薯覰熲馳疵醵嶷覷薼醼施薿嗁釃緄鷆痆巇槇臈鷉巉闍巍旎跎槐闐緒巓嗔燕臕秕巘藘臙臚嗜觜凝駝懝駞姥旨藨臰鷰釶藷混藹七嘅蘆昆切渇爈瘏爐嘑訑鸕嘗託嘘渚樚訛樝縝鸝瘣阤瘧蘧稭鈮戯戲嘴昵舵蘶踷稹稽帾訾陀陁鉇鉈虍穎虎蹏虐詑虒虓虔處虖乖虗虘托乘虙虚陛虜乜虝牝湝虞號也虠牠癡虡驢虢詣艣虣虤癤驥虧虩剩驪詫艫噱癲虵批陼鹿麀皀庀麁麂窃麃麄麅皆躇劇麇庇麈麋麌皍蚍麎階麏暑纑麑麒麓纔麕麖劖麗隗麘芘纚麛些麝麞誟麟銠麤嚥嚧躧庨蚭誮嚱花媲芲亳抳骴媿曁廆櫆苆蛇滇雌廎廏曏囐廐髒諕拕櫖他拖髗曙廜髢櫧盧諧櫨苨廬曬髭竭滮囮蛯軰黰仳勴狴拷曷諸黸滹竾謁匂欃指猇茈漉鬐欐鬒謔匕錕漖化北匘匙弛匜眞眤眥輥眦猪鬯地鬱鬳錵欵謼鬼鬽潁魁魂譃魃靄魄罅魅轆齇歇鍇魈孋魋魌佌魍蝎魎罏魏魑坒歔魔齔魕荖魖魗佗魘彘孝杝轤此坨彪佬坭魮署彲潴靴彵罷睹獹鍺獻死鮀侂它宅羆鮆箆枇瞋讌箎讒螕侘讞垞鎞箟螧鮨鞨鎭玭宯羯鮱掲莵箸玼侾殾埀柁柂柅迆菎矑柒揓比蟕鏖埖毖毘寘毚鯤迤翥篦揩篪鯱叱迱柴揹鯺鯽濾俿埿"));
        PART_TO_KANJILIST.put('化', new Part(4, "糀花硴靴錵化埖貨椛訛囮"));
        PART_TO_KANJILIST.put('北', new Part(1, "冀埀驥嚥燕乖北乘臙剩讌背"));
        PART_TO_KANJILIST.put('匚', new Part(2, "吂蠃堅怇鰋氓嘔蘙蘛瀛樞耟尠渠戡頣頤頥瘧堪樫蠮堰尶踸鰹衁偃籃繄驅鉅框恇詎豎虐鑑汒鑒葚湛籝艦摳塸虻繼鹽恾陿繿隁誆墈媐芒肓粔悘邙貙嚚纜岠亡碪炬躯暱傴誷蚷劻檻軀糂櫃拒嫗勘惘磡賢監苣擥鋩竪盬軭黮瓯盲磲蓲諶鋻賾郾謊挋甌鴎贏贒贓謔愖儖欖匚漚甚匛望匜愜匝茝匞茞輞匟匠匡弡朢匣匥礥欧匧匨匩匪匫弫茫匬弬攬匭匯匰匱匲匳謳鼴嬴匵蔵眶匸椹匹区医椻匼匽匾甿匿區煁彄奆饇慌魍腎筐歐荒罔煕鍖蕖蝘卙煚慝轞襤奩筪歫孯蝱鍳慳筺敺譼啿妄嶇熈箍掔鮔瞖榘熙斟枢宦榧覧侫醫熰網殴羸殹覽駆毆毉緊篋藍藏臓揕鷖鷗鏗忘忙篚柜臝距凞臟揠懢臣臤臥臧臨巨矩柩濫姫秬釯翳叵巸"));
        PART_TO_KANJILIST.put('匝', new Part(1, "匝箍"));
        PART_TO_KANJILIST.put('欠', new Part(4, "崁餈簌蔌蠍砍預樕欠次欣欤欧茨栨欨欫欬欯漱欲欵欶欷欸吹欹欺欻缼欽款欿歃歆歇歉歊歌嵌歍歎坎歐歒歓歔歖鱖獗歘歙歛橛歝歟歠歡恣赥蕨遬杴蹶蕷佽慾掀劂撅炊趑嶔憠羡芡龡粢厥趦咨羨撳資鏉篏闕盗盜廞惞軟諮飮飲瓷忺嗽俽懿姿"));
        PART_TO_KANJILIST.put('匡', new Part(1, "筐匡框筺"));
        PART_TO_KANJILIST.put('次', new Part(1, "次粢恣瓷盗資咨茨諮懿姿"));
        PART_TO_KANJILIST.put('欣', new Part(1, "掀欣"));
        PART_TO_KANJILIST.put('鬥', new Part(10, "鬥鬧鬨鬩鬪鬮"));
        PART_TO_KANJILIST.put('匪', new Part(1, "榧匪"));
        PART_TO_KANJILIST.put('猪', new Part(1, "潴猪"));
        PART_TO_KANJILIST.put('鬯', new Part(10, "鬱鬯"));
        PART_TO_KANJILIST.put('嬰', new Part(1, "嬰纓瓔嚶鸚櫻"));
        PART_TO_KANJILIST.put('匱', new Part(1, "匱櫃"));
        PART_TO_KANJILIST.put('鬲', new Part(10, "槅膈灊鷊融翮囐鬲鬳隔鬴鬵甗鬷巘鎘鬹鬺獻鬻瓛讞"));
        PART_TO_KANJILIST.put('茲', new Part(1, "茲孳"));
        PART_TO_KANJILIST.put('欲', new Part(1, "欲慾"));
        PART_TO_KANJILIST.put('猶', new Part(1, "蕕猶"));
        PART_TO_KANJILIST.put('匸', new Part(1, "衁蠃妄謊慌魍贏荒芒汒氓肓罔惘忘忙邙望瀛籝臝輞亡鋩侫茫釯蝱網盲嬴誷匸羸虻恾甿"));
        PART_TO_KANJILIST.put('匹', new Part(1, "尠戡糂鍖勘匹椹堪甚碪湛斟"));
        PART_TO_KANJILIST.put('区', new Part(1, "枢殴駆欧区鴎躯"));
        PART_TO_KANJILIST.put('医', new Part(1, "翳医醫"));
        PART_TO_KANJILIST.put('鬼', new Part(10, "傀魁魂魃魄魅廆櫆磈魈塊魋魌巍魍魎魏蒐槐魑魔魕魖隗魗魘醜瘣愧嵬聭瑰莵蘶鬼餽鬽媿"));
        PART_TO_KANJILIST.put('匿', new Part(1, "慝匿"));
        PART_TO_KANJILIST.put('區', new Part(1, "區歐謳傴嘔驅毆嫗嶇奩甌樞"));
        PART_TO_KANJILIST.put('十', new Part(2, "送瀁倅逆個逋倎瀑耕倖逗怘怙瀚倛倝借値怦倨逩倫倬耬逬逴逹瀹瀾遁遂偂灃遃遅灊灌遍灎偏偐遒道達灔遖恙偙做灞聠遡遥遦遧聨適遭恭遮遰偱遲聳聴遵職遼聽避傁邃傅悈悌悍傍傎悑悖邗傘肝傞肢邢悦邯悳傳悴肸悻悼傽僀僂烊働僐惓僔僕僖胖惘郙惜僜惠胡僤僧僨郫郵惵僻胼惼胾儀愂儂儈儎儕脖愖鄞焠鄣鄧愬鄭脯焯儯鄯脰脱鄲鄴儵鄶愷鄷脺儺焼愽脾煁腆慈慊克酋兌慎煎腒酔腗慞慟煠兢煢酣酤煤腩腫慫慬腯慱酵其典慸酺慻兼兾慿膀冀膁憄憇醉膊冊醋憋憎熏醐憖憘憙憚膞憠膢憤膨醭熯醰醳膳醴膵冸熹醺熺膿燀懀臂懃凄凅燈釋懌燌重臍臎燎懐凒燕凖凘臙釜針釟懟臡釡懣懥燧懩釬凱臱凲凳燵燻懽臿懿戁舁戃爆戇刊刋戎刑戒舒舖舗鈘舘爚爛戟戠戡判爤戦截刪爯戰刱戲戴鈷券鈹扁牂牃剃剅剋牌前剏艏牐艑牒牓牖艖鉗艘剘鉘艚鉛剛艜扞艠鉡艤剤鉥剩剪牯剰艱剴艴艶鉶艷鉼技犀劂劈芉芊劊芋抍劑銒劓銖劘犠犧抨芰銲犴劷銻芾勀鋀勃鋅拌鋍拑勒動勘拚拜勝拝苦勩鋪括勬鋭狭勲鋲勳拳苷鋸勸苹鋼拼錀茀猂錈匍錍錘猝猞挟猢錤匥錬献錮錯匰猵挵猶猷匾挿十卂千獃卅卆升午草卉荊半卋卍捍鍍獐卑卒卓荓協捕獖鍖南獗鍗単獘卙博卛捜鍤捭据獯鍱捲荳捶卸荸鍽捽鍾鎂掄莆率厈鎈掉鎊鎌鎍鎔玕莘鎛鎡厥鎧鎨厨厪措鎭厮鎮鎰玵鎹莽掽莾叀揀揃鏄揅鏆菇珊叏菐鏑插叓揕叛揜叟鏟叢古揥叫揲叶鏷鏸揺鐁萃萆萍搏鐏鐐搒搓鐔萕鐙琚搜搠鐡搤琪鐮吱鐴鐵鐸萹鐽呄葉葊呏摒摔摘瑚葡董葥葧葫葬呭鑭鑮摯鑰瑱瑳鑵摶呼瑾鑿咁蒂钄撅撇咈璋璒撕咕撙璞蒡撣璧撩咩蒪璯蒱蒲撲咲璲蒴蒹撻璻蓆擇哉瓓擗擘擠哢瓣擤瓧瓩哮蓱擲蓴哶瓶擶哺蓻哾擿攁甆甎蔐甑甓攔唕蔕蔗甘甚甜甞蔞蔢産攤攩甫唫攮支攱攼蔽唾畀啅故商啇蕈畊敍啐畔蕕敘教蕙啚啛敝畞蕡啣啤蕨異敵敷啻畻啼啿斁喃斃薄善閈斉喋開斎疐斒斕薘薛薜喜閞斟斡関薩薪喪薫薭單斯新閲疳斳薺疿旁旆旉藉藊闊嗌闌闐痒闓嗔闕旗痘闘旘嗞嗟闡闢嗣藤闥闦痩早旰旱痲旴痹痺痼旿蘀瘁蘄昇蘍嘏嘑昔蘖蘗嘙蘚蘛阜阡阢瘥瘦昪蘭阱嘲瘴嘶嘷嘻嘽嘿虀虁噂療癉噌降噎噔癖虖晘晜陞癟噠晡癢晫晬癬普癱癲陲噲噴陴噵虷噺登暁皁蚈隊嚊皋嚌嚏皐蚑隑嚔隘皚暛障暟暤嚥隥隦隧嚩嚭暭隮嚱暲暴暵蚶皷暸暼隼皽暿囀曀蛄囅益囊囍囎曎囏盖盙曛曝離曦盬盰囲曲直曹固曺蛻曽盾圃蜅圇朇圈圉圏朔朕團圛朝朞蜞期真蜣蜱眷蜷霸眹霹霽着睅杆睇蝉靊蝙睟睠睡坢蝤睤睥蝨坩革坪睪靪靫靭靮靱靳蝴靴杵蝶靶靷靸靹靺蝻靻靼靽睾靿鞀鞁垂螃鞄枅鞅螆鞆鞉型螋鞋瞋瞍鞍鞏鞐鞕瞕鞖鞗螘鞘鞙鞚鞜枝鞞瞞垟鞟枠螠鞠枡鞢螣鞣瞥鞦鞨垩瞪垪鞫鞬瞭鞭鞮螮枯枰鞱鞲鞳鞴鞵鞶鞸鞹鞺枻枼鞼瞽枽鞾鞿埀蟀柀韁韃蟄韄韅韆韇韈韉韊某柑蟒韓埔蟖矗韘蟚埠蟢埣埤蟪蟫柬短韯矰韲埴柵執矸基蟻頇堈蠊堌頍蠎蠐研砕頖栞堞栟堪頬頭頮頯砰報蠲栽塀衅顇顊桊塍衎硎塏顏塑桒術顔顗衘衚顛衜衝衠塡塧填顰桲顴衷桺桿碁墇墈墊碎墐碑梓増墜碟墟墠墡袢碪梪墫梯械碰墱碲墳梻碾壁裁磁棄磅棆壊棊棋磋裋磌壎磑裓磚飛飜補棝裞磠磡棡飦裨壩棬棲磳磴裷棹壹裾餅椊褊養植椐椑餑礒餔夔餖餘褝餠礠椣椦褨椪餬夲礴餴椹褺餺餻襆饈饉襌饍饎饐襒奔襗饙襚楛楜祜楝襞楠奠楢襣祥楪業楯襯奲楳楴襴襷奸祺祻襽覃禅覇覉覊禌妌妍榑妓禖首馗馘禘禛榜榦禦馦禧親禪榫禫覬禮羚覲禴覴馵榺离榿觀秊槊構姍槎姑様槙秚姜駢秤姧觧槫駴駵觶槹駻槽駾槿樀騂樅計稈訊稊税騏訐稗稘騙稙樟稡樣娣娧騨娨横種稯騰許樴稵樸稹樹稺樽樾樿詁穂穉詍驔評詖驖穗橙穙驛橛婞穟婢婥驥橧穧驩橱橲詳橳婻穽誉媒誖檗媟誡窣檥誧説骭窯媵誶誷骿髀嫂髆櫈髈櫉嫌竍竎竏竒髒體竕論嫜諜髜諞章嫡櫤嫥諦竦竨諫櫱髲竴諵髷諺竿謀鬀嬁欂笄謄欄欅鬈嬉欉權嬋鬋鬍鬒嬖謗欗謙鬙謚講欛欟鬠謡笧鬪謫謭笰鬳鬴鬵嬶鬷笹鬹欺鬺謼歃譄譆歉證魌魍歒歔歖識譚孛譜歝孝歡歧譬筭譯議譱孳孼孽譾讁鮃箄鮄箇宇讌鮍宑箕讕殖算讙讜箝鮝箞殢殣箥鮩箪殪殫殬箭鮮宰殱箶鮷鯆篇篏寒鯗寘鯝寮篺鯿専尃尅專鰈尊鰊尌鰌對導簒氒簓簙簛鰜簟氟簠尠簦鰦氧鰧尭簱尲簳簸簾簿谿汁鱃鱄居鱆豆豇豈豉鱉豊豋豌籍豎籏鱏屏豏屐豐籐豑鱒鱓豓豔鱔展鱖汗籘豙鱚汛鱛籜屜鱝豢豣籣層籥鱧屧汧汫籬屰籲籵豶鱶汻豻屽粁粉粋貋岐岡岪貭貰粱岵岸粹粺岼沽賁糂糄泄峅賆糊賊糍泔糕泙賚糞糟峡糦糩泮糵賷鳷賸賺賻購糾贄鴇贈贉洋崒崗崘鴘崙贛索鴣紣紲崹紺活紼絆鵇嵌鵓嵜鵜浡絣鵥浦絨鵫嵭鵯嵯嵰鵲嵳嶂綈綍嶒趕涕鶘嶝嶟趠綣嶧鶫嶫涬嶬綱網綷綸涸鶼趼綽鶿鷁跂淇跇巇鷊鷏巐巓鷓緕淖鷙緙巙跚締淠緤鷧編鷩淪淬差鷯跰巷巻渀済鸊縊踑縑縒帔踔渕世丗鸙縛鸛縝帝踞渞縡渡踡縢渤縦並渧帨渫丫席帯港帲踵丵縶帶踸丼渼蹀蹁湃蹄湅湈幉蹉蹎乎幐繒織湔繕湖乖乗乘繚湛幛蹛幞繟蹠蹢蹣幣蹤繥蹩蹬蹭幮蹰干蹲平湳年幵鹵乵并蹶繸幸鹸幹繹鹹乹鹺鹻蹼鹼鹽乾満纁纂躂躃纃躄纆亊庋躋纎躑麒溓躕井準躝溝麞庠溠溢溥麥度麩庪麪麬麭溯溲庳庶躾溿什軁滂軃軆滇廉廋滋滏仐廑軒滓滕黕廚廝滞仟廟黟仠黤廥黧黬黭滭黮滯廰黰黱黲廳滴黵仵滷滸黸黹滹黻滻黼廾廿滿弁异弃弄弆弇鼈弈弉載弊漌伎鼓輔鼔鼕漕鼖鼗鼙鼚鼛漛伜輞鼟弟弢輧輪漭弮估伳漳伴輴輶伻鼻鼼弽鼽弾輾鼾漾缾鼿齁齃齄罅彅齅齆罇齇彈轉齊齋齎齏轒潓罔罕彛彜彝罟形罤彦潦罩潬潭彭潮置佯彰潰罱併潽罾侁澄來羇羈徉羊澌羌澍羍澎美羏龏羑従羔侖羖羗羚辛辜羜羝辝徝羞龞辞辟龠侠徠辠龡御羡辡龢羢龣澣羣辣群澤辤龥辥羦辦徧澧辧羨辨義羪循羭辭羮澮辮羯辯羲農徳羴羶羸羹侻辻羼澼侼羿迀迁濂迂濃迅翅翆濆俌忓翔迕俜濞濟翠迣忤俤翦濨翩忮濮忰翰述迸濹翺翼俾"));
        PART_TO_KANJILIST.put('卂', new Part(1, "卂迅"));
        PART_TO_KANJILIST.put('䍃', new Part(1, "謡䍃揺"));
        PART_TO_KANJILIST.put('千', new Part(1, "埀粁垂千歃刋働重熏竏插動乖乗錘衝慟仟睡阡董瓩腫薫種剰陲勲勳郵踵捶醺燻唾鍾挿"));
        PART_TO_KANJILIST.put('獄', new Part(1, "獄嶽"));
        PART_TO_KANJILIST.put('荅', new Part(1, "剳鞳塔荅搭"));
        PART_TO_KANJILIST.put('卅', new Part(1, "棄卅丗滞帯"));
        PART_TO_KANJILIST.put('卆', new Part(1, "忰枠酔砕卆翆粋伜"));
        PART_TO_KANJILIST.put('升', new Part(1, "枡升昇飛飜陞"));
        PART_TO_KANJILIST.put('歇', new Part(1, "歇蠍"));
        PART_TO_KANJILIST.put('午', new Part(1, "御許啣忤杵禦午滸卸"));
        PART_TO_KANJILIST.put('卉', new Part(1, "暁賁墳噴憤奔濆卉焼尭蠎"));
        PART_TO_KANJILIST.put('半', new Part(1, "袢伴判畔絆胖半叛拌"));
        PART_TO_KANJILIST.put('魏', new Part(1, "巍魏"));
        PART_TO_KANJILIST.put('子', new Part(3, "椁愂稃蜉洊餑鐓耔稕漖脖猛朜焞錞茡渤栫脬錳弴游猻吼艀譈遊捊噋艋啍李轏子荐孑孒鵓孔孕孖字存教孚孛孜遜孝孞孟浡潡季蝣硣孤孥学敦罦葧孨孩孫浮孮孯孰孱桲乳孳桴孵酵學牸荸潺孺酻孼好孽籽塾孾孿斅醇斈鶉犉殍綍序悖誖厚箛憝熟侟誟鞟馟芤綧庨墩莩享宯箰粰悸鞹侼暾侾斿蓀勃諄秄惇燉鋍廓仔俘郛僝囝槨郭哮賯菰淳拵臶惸"));
        PART_TO_KANJILIST.put('卑', new Part(1, "顰髀卑碑婢睥稗裨痺牌俾脾"));
        PART_TO_KANJILIST.put('筑', new Part(1, "筑築"));
        PART_TO_KANJILIST.put('荒', new Part(1, "荒慌"));
        PART_TO_KANJILIST.put('卒', new Part(1, "翠瘁埣萃倅醉淬碎卒悴膵粹猝"));
        PART_TO_KANJILIST.put('卓', new Part(1, "卓啅掉棹罩倬悼綽"));
        PART_TO_KANJILIST.put('孔', new Part(1, "孔吼"));
        PART_TO_KANJILIST.put('答', new Part(1, "答箚"));
        PART_TO_KANJILIST.put('南', new Part(1, "楠喃遖南献"));
        PART_TO_KANJILIST.put('単', new Part(1, "禅戦単騨蝉箪褝弾"));
        PART_TO_KANJILIST.put('存', new Part(1, "荐拵存栫"));
        PART_TO_KANJILIST.put('魚', new Part(11, "鰀鰂鰄鰆蘇鰈鰉鰊鰋鰌鰍鰏鰐鰑鰒蘓鰓鰔鰕鰖鰘鰙蘚鰚鰛鰜鰞鰡鰢鰣鰤鰥鰦鰧鰨鰩鰪鰭鰮鰯鰰鰱鰲鰵鰶鰷鰹鰺鰻鰽鰾鱁鱃鱄鱅鱆鱇鱈鱉鱊穌鱎鱏鱐鱒鱓鱔鱖鱗鱘鱚鱛鱜鱝鱞鱟鱠衡鱣鱧鱨鱩艪鱪鱫癬穭鱮鱰鱲鱵鱶鱷鱸鱻嚕囌櫓漁魚魛魞魡魣魥魦魨魪魫魬魭魮魯魳魴魵魷魸魹魿鮀鮃鮄鮅鮆鮇鮉薊鮊鮋鮍鮎鮏鮐鮑鮒鮓鮔鮖鮗鮚鮝鮞鮟鮠鮦鮧鮨鮩鮪鮫鮬鮭鮮鮰鮱鮲鮴鮷鮸鮹鮻鮼鮾鮿鯀鯁鯆鯇鯈鯉鯊鯎鯏鯐鯑鯒鯔鯖鯗鯘鯛鯝鯟鷠鯡鯢鯣鯤鯥鯧鯨鯪鯫鯯鯰鯱鯲鯳鯵鯷鯸鯹鯺鯽鯿"));
        PART_TO_KANJILIST.put('孚', new Part(1, "艀乳桴孵俘蜉孚郛殍浮"));
        PART_TO_KANJILIST.put('孛', new Part(1, "勃渤悖孛"));
        PART_TO_KANJILIST.put('卜', new Part(2, "琁簁簆逌倏吓怔瀕尗怗瀘頙瀝尟倢瀣倣堤琥砦砧倬鰲簴逴倵鐻耻頻堽頾遉桌題衘籚做桛遞籞鑡鑢聢摣顣屣豦偦籧遨鑪顪呫顰呰顱聳汴偵豵鱸遽邀碇璇璐撒碔粘沚條貞墟悠撤璨璩颪颭肯粲貲傲颴碵点梺貼悼岾沾璿裃擄糈惉瓐壑胔據壚蓚泚瓛峠磠壡壢胥賦飪擬蓯蓰哶棹壻褂攄贅褆贇焉攊唌礎餐椒夒夔外甗儗椗礙鴜脠鄢脩紪褫紫蔫蔬焯攴攸改攻放政故啅祉蕋敍敏兏救敕饕敖敗教楚絛敠鵡散啣敧楨鵫慫敬赬慮奯数敲整赴敵敷敺慼斅薇閇覈趈薉疋斌醍禎疎涎疏疐疑醑喒禔綖鶗覘写膚疜趠冦禦熬薮覰醵疵趵嶷覷趷綹趹綻趻趼綽趾薿淀跀嗁痁姃緃跅跆巇症跇跈鷉跊旋巋巎跎淑跑跔跕淖跗巘藘釙巙跙臚懜觜凝懝跡跤懥秥跥燦跧凪跬槭跰跱懲跲致跴藶嗷緹緻跽踁訃踄樅蘆踆爈渉上蘋下渋踋与丐爐嘑踑踔鸕帖踖嘘戚昝樝爟騠踠踡踢踣鈣縦踦瘧蘧縬騭是戯阯稰昰縱踱戲瘲踳嘴踶踷阷踸踹証阽踽樾訾幀詀蹀繁虁蹁扂鉆蹋婌虍蹍虎蹎蹏虐湑扑虒虓虔處繖虖虗虘蹙虙驚虚噛蹛湜虜蹜虝蹝虞湞蹞號陟虠癡虡蹡驢虢穢蹢艣虣蹤虤婥鉦噦牧虧癧虩乩艫晫蹬蹭扯蹯蹰噱蹱鹵晷鹸晸鹹蹹鹺蹺鹻蹻鹼鹽婿躂抃躃檄隄檆劇劌嚏躐纑躒嚔誕躕店嚙躚些媜躝媞躞檠躢嚦嚧躧躩麪嚬嚱骴芷竅蛅仆拈竊蛋鋋滌雌廎黏囐囓諔諕櫖髗務站諚櫜諝點諟嫣盧櫨竨嫩櫪雫苫廬髭滮勴延滷黸滹企眄鼇猇笇茈挊霑蜑鼑蜒謔鼔笘匙鼛錠圤欤眥眦霧漩欫茮霰鬳朴圷錻謼址靂佂譃罅譅齇鍉佌罏齒卓齓歔齔佔靕齕魖齖捗齗齘齚靛卜齝卞鍞潞齞齟齠占齡卡罡止齢督齣正轣卣轤此卥警齦武卦齧歧齨罩歩獩齩彪齪歪歫罫坫齬齭歮齮歯齯齰歰筱齱佱齲齳歳歴筵齵歵卵孵齶捷齷卸歸獹齺獻齽睿澀征澁掂鮆掉羋箎鮎徏鞐羐従徙枚定莚掛從讞掟御殩微鞮螯掯澯垰徰徴辵玷徹徼玼激揁寂寇鏇濊埏提鯐忐矑忑寔叔蟕韙埞篠迠叡毡鏦翨篪濬忭修迯鯱柴篵篶俶鯷珷菽翽濾柾俿"));
        PART_TO_KANJILIST.put('孝', new Part(1, "酵教孝哮"));
        PART_TO_KANJILIST.put('卞', new Part(1, "抃卞"));
        PART_TO_KANJILIST.put('孟', new Part(1, "猛孟"));
        PART_TO_KANJILIST.put('占', new Part(1, "占砧拈苫鮎黏霑帖店笘粘覘点站貼岾沾點"));
        PART_TO_KANJILIST.put('止', new Part(4, "簁踁踄踆渉渋蘋踋丐踑怔踔瀕踖頙瀝尟踠踡踢踣鈣砦踦騭阯昰踱瘲踳嘴倵踶踷阷踸踹頻耻証堽踽樾訾頾蹀虁蹁蹋蹍蹎蹏衘噛蹛蹜蹝籞蹞陟蹡鑡穢蹢屣虣鉦噦癧顪蹬蹭扯蹯顰呰蹰蹱豵晸蹹蹺蹻躂躃劌璐躐躒碔誕躕嚙沚躚些躝躞躢嚦躧躩麪嚬肯貲骴芷鋋雌囓胔泚壢嫣賦櫪髭蓯蓰延企眄贇茈焉攊唌蜑蜒夒夔鴜脠鄢眥眦紪紫欫蔫蔬錻政址靂佂譅祉蕋佌齒齓齔靕齕齖捗齗齘齚齝潞齞齟齠齡鵡罡止齢齣正啣轣此齦武齧歧齨歩獩齩齪歪歫齬齭歮齮歯齯奯齰歰齱佱齲齳歳整歴筵齵歵齶齷卸歸齺齽澀征澁鮆薉斌涎疎疏徏綖徙莚御禦掯徰疵趵辵趷趹趻玼趼趾跀姃緃跅跆症跇跈濊跊巋巎跎埏鯐跑跔蟕跕跗巙跙觜跡跤懥跥鏦跧凪跬跰跱跲柴跴篵篶藶珷翽跽柾"));
        PART_TO_KANJILIST.put('季', new Part(1, "季悸"));
        PART_TO_KANJILIST.put('正', new Part(1, "征眄症焉涎蜑蜒誕莚御正嫣啣禦鉦歪麪整筵篶延卸証堽柾政"));
        PART_TO_KANJILIST.put('孤', new Part(1, "菰孤"));
        PART_TO_KANJILIST.put('此', new Part(1, "此眥眦砦紫雌髭呰貲嘴柴疵些觜"));
        PART_TO_KANJILIST.put('卦', new Part(1, "褂卦掛罫"));
        PART_TO_KANJILIST.put('武', new Part(1, "鵡武賦贇錻斌"));
        PART_TO_KANJILIST.put('卩', new Part(2, "椀昂范茆脆倇圈踋伌服瀏鴒領夘脚鴛怜蠞踠頠踡鰡眢瘤霤倦怨阨琬冷報堲鈴昴笵帵伶蜷氾餾蜿籀呃桅聆婉聊豌蝍恑腕啘留鉚剜籞瑠睠齡齢啣癤捥卩卬詭卭卮卯印危卲捲即却卵孵卷鵷卸佹卹畹卻扼晼命卾卿厄岇熈劉皍抑碗箙枙羚宛溜垝喞嚠鮠御綣禦令澪嶺莭羚犯聆鈴玲零領榴涴掵冷嶺貿節菀範韆蛉惋珋勌盌迎苑惓苓菔泖峗櫛軛叝令埦叩跪棬仰臲柳零裷囹鋺鯽"));
        PART_TO_KANJILIST.put('歩', new Part(1, "顰瀕捗歩渉蘋頻陟"));
        PART_TO_KANJILIST.put('孫', new Part(1, "孫遜"));
        PART_TO_KANJILIST.put('卬', new Part(1, "仰卬迎"));
        PART_TO_KANJILIST.put('筮', new Part(1, "噬筮"));
        PART_TO_KANJILIST.put('歯', new Part(12, "齒囓齓齔齕齖齗齘齚噛齝齞齟齠齡齢齣齦齧齨齩齪齬齮歯齯齰齱齲齳齵齶齷齺齽"));
        PART_TO_KANJILIST.put('卯', new Part(1, "嚠瑠籀鰡瘤霤茆劉聊卯瀏柳昴榴卵孵留鉚溜餾貿"));
        PART_TO_KANJILIST.put('魯', new Part(1, "櫓艪魯"));
        PART_TO_KANJILIST.put('孰', new Part(1, "孰塾熟"));
        PART_TO_KANJILIST.put('危', new Part(1, "鮠危脆跪詭"));
        PART_TO_KANJILIST.put('孱', new Part(1, "孱潺"));
        PART_TO_KANJILIST.put('即', new Part(1, "節即櫛喞卿"));
        PART_TO_KANJILIST.put('歳', new Part(1, "穢歳"));
        PART_TO_KANJILIST.put('却', new Part(1, "却脚"));
        PART_TO_KANJILIST.put('歴', new Part(1, "靂轣歴癧櫪瀝"));
        PART_TO_KANJILIST.put('卵', new Part(1, "卵孵"));
        PART_TO_KANJILIST.put('卷', new Part(1, "惓綣倦卷蜷圈"));
        PART_TO_KANJILIST.put('卸', new Part(1, "御啣禦卸"));
        PART_TO_KANJILIST.put('歹', new Part(4, "殀殂殃斃殄殅殆殉殊残例洌殍殕殖列殗殘夙殛殞殟殠殢茢殣殤薤咧薧殨薨殩殪殫殬殭餮殮殯殰殱殲冽劽裂烈屍髒蛚塟葬臰姴歹死歽歾迾歿"));
        PART_TO_KANJILIST.put('死', new Part(1, "斃薨死葬屍"));
        PART_TO_KANJILIST.put('宀', new Part(3, "簆倇瀉瀋倌氎萓瀗鰘蠙鰚頞砣倥倧氨琬逭琮琯萱搴搾谾豁瑄案豌鑌額葖塞硡聢瑢籩聹灾邃碇邊碗傛咜梡傢咤梥貯悰沱咹確粽悾蓉惋賓棕糘擦賨泬擯擰棺胺賽蓿椀愃崆崇椌唍儐攓鴕椗脘愙攛儜夤蔤館鴪甯褰蔲鴳紵愹紽煊啌腔腕啘腟酡浣浤楦酧鵷畹鵼喀榁疉綋鶎熔榕綜膣喧榨綰鶱憲鶴涴薴綻淀鷃跎臏臗淙駝懧藭痯臱姲藼騌樒舘帘踠縡踪騫縮縯渲舵帵稼陀穃蹇鉈婉晏詑剜蹜詝院牢詫幰割穴穵究牸穸穹空晼繽穽穾穿嚀窀突窂窃窄暄窅窆窈窊窋窐窑窒窓窔窕窖皖窗窘窞窟窠窣銨窩窪檫窬窮窯窰檳窳窵溶窶檸窹窺窻躻誼窼窿嫁櫁竃竄竅嫆竆竇蛇竈竉竊曌鋎鋐滓拕髕狖髖竚諚諠苧狩竩鋺鬃謇按輐挓演挖蜜錝錠茡鬢錧輨嬪嬸鼹蜿弿轄睆佇彍字佗靛捥筦坨孮獰獱坹坾宀鮀宁它宄宅讅宆宇守安宊宋鎋完鞍宍瞎宎宏宐宑侒宓鎔宔宕宖徖宗侘官宙定瞚鞚宛箜宜羜宝莞垞鮟掟実管客宣室宥宦羦控垨宨宩宬宭宮殯宯宰宱宲害宴宵鎵家宷宸垸容宺宼瞾螾宿寀菀柁寁寂寃矃寄菅寅濅密寇鯇寉矉富寍揎寏寐寒俒寓寔寖寗濘寙寚寛寝寞埞察叠寠寡寢寤鏥寥實埦寧寨審菪寫揬寮寯寰濱寱迱寳寴寵濵寶"));
        PART_TO_KANJILIST.put('厂', new Part(2, "氂栃鰄逅瀇蠊瀍后簏逓鰔瀘蠛瀝頠蠣琥耨搪鐮簴簷砺鐻簾堿砿遁呃遃桅鱅鱇衊灋顏偐恑顑灑顔鱖願塘籚鑛摝遞恢鑢摣顣鑣豦籧摩鑪顪摭瑭籭遮灰顱偱汳葳葴屵鱵塵鱸遽撅傆墉墌傏邐傓梔備墟粧販璩炭貭傭碱岸咸撼擄蓆郈賍糎瓐蓐賑糒壓擔烕郕糖峗壙蓙據壚瓛糜糠賡壢壥磨擪鳫飯磯糲擴擵磿儂攄甅脆唇脈鴈攈攊贋儋贍唐蔑贓崕崖甗贗蔗鄘鄜感攟蔢産脣儣脤褥礦攦椨礪褫褲崴蔴蔵儷餹鄺儼鄽派愿啀嵃蕆慇畈酈絋兏敐腐腑饑饕襛襜饜慝嵟兤蕨襪慮楯奯腯楲慵慶慷慼蕽畿薅膅薉憊喊越鶊醎膚覛薝憠薦薩喭涯禯覰醲醵覷憹憺膺薼膽憾膿醿嗁釃釅巇槈鷉應巌减藏釐臓鷓懕臕巖緘巘藘臚臟懡凢藦臧藨跪懬槭懭觱臲巵藶臹鷹威昃昄縅蘆爈戉嘊戊戌戍娍爐鈑嘑蘑鸕嘘嘙舚戚樚減嘛樝鸝縟娠渡瘧蘧阨阪縬席戯踱戲騵縻蘼昿牅穅版穈虍虎蹏虐虒陒虓虔處乕虖詖虗虘蹙虙虚橛虜虝鉞虞機號虠晠穠蹠虡噡穢驢虢艣虣虤噦虧癧幨晨虩驪艫詬詭幮蹰噱橱鉱扳蹶剷鹹詹詼扼幾广鹿麀庀麁庁麂劂麃広麄庄麅劇麇庇麈皈嚈床纊銊麋庋麌劌麎庎溏麏序纏源檐纑麑麒纒麓躔麕底躕麖庖麗店銗麘劘媙纚庚麛犛府麝嚝麞麟庠庢麤庤庥度暦嚦嚧躧座庨庪庫庬庭嚱庱励庳嚴庵庶劶康庸麻麼庽溽麾庾麿庿檿廁雁廂廃仄嫄滅軅廆軈廈廉櫉廊廋廌廎蛎廏囐廐廑廒廓廔櫔諕廕櫖廖髗廚廛軛廜廝廞廟曟廠曠嫠鋠拡廡廢廣廥盧櫨廨廩櫪廫廬曬鋬黬滮仮曮鋮廰竰仰廱髲廳勴諴勵黵黶黸滹諺滻滽盾茂蜃漄蜄猇震漉伌欐謔漘謜霢漦圧振鬳輴謼靂坂睂譃罅轆齇孋鍍譍譏罏歔魔魖轗獗荗魘睚靡轣轤彦靨獩彪譫捬卬魬卮危捱彲歳歴獷獹佹獻鍼罽板荿莀玁厂侂掂厃厄厇厈螈箎厎龐鎒厓厔鎕垕厖螗厘厙枙厚厝垝讞原鮠厠厡垢厤厥箥厦厨厩循厪厫宬厭厮厯辰厰辱農侲厲厳箴鞴厵殷宸澹瞻柀濂濃韈韉濊反迎鯎矑返鏖篖叛韛応鏞鏟矦篪班俯鯱鯳珹翽濾蟾篾俿"));
        PART_TO_KANJILIST.put('它', new Part(1, "陀柁沱詑它舵鴕佗蛇鉈駝"));
        PART_TO_KANJILIST.put('玄', new Part(5, "蟀滀磁絃痃詃玄蓄荄侅玅賅玆率慈鉉慉滋痎搐鄐衒奒摔陔胘畜呟姟昡畡閡弦氦頦袨眩泫炫欬胲茲孳舷玹牽蚿"));
        PART_TO_KANJILIST.put('厄', new Part(1, "鮠危厄脆阨跪軛扼詭"));
        PART_TO_KANJILIST.put('殄', new Part(1, "殄餮"));
        PART_TO_KANJILIST.put('宅', new Part(1, "咤宅侘詫"));
        PART_TO_KANJILIST.put('率', new Part(1, "蟀率"));
        PART_TO_KANJILIST.put('守', new Part(1, "守狩"));
        PART_TO_KANJILIST.put('安', new Part(1, "案安按鞍晏鮟"));
        PART_TO_KANJILIST.put('玉', new Part(1, "琀瀁琁球琄琅瀅理搆琇琉鰉琊頊對琑栓搓琚琛逛逞砡琢琤琥琦氧琨尩琩琪尫琬堭琭琮琯琰琱琲頲琳琴琵鐵琶琹琺鐽琿瑀瑁恁瑃鱃瑄遅框瑆恇瑇呈瑋瑍遑瑑瑒達鱔瑕聖瑗遘恙瑙瑚瑛瑜瑝瑞瑟偟瑠瑢瑣瑤瑦瑧瑨瑩汪瑪瑫瑭瑮瑯桯瑰瑱瑲瑳瑶鱶鱷聽衽瑾璀璁璃梃璅璆璇璉璋璏璐璑璒璘璙璚璜璞傞璟璠墡璡璢璣璦璧碧璨咩璩璪璫璮璯環璱撲璲袵璵炷璹撻璻璽璿賃瓈瓉瓊烊磋瓌裎瓏僐瓐瓓瓔僕糕瓘瓚瓛郢哢注壬飳惶購儀養洋礒紝椢褨鄯鄴餻饈煌絍饍鵟祥浧全業嵯酲嵳楻善妊再閏斑冓薘閠喤嶫嶺嶬覯羚膳痊構槎駐痒様姙釜姜嗟懟闥觧跧淫燬凭差凰懲燵旺程縒蘚戜丟樣瘥樸鈺主註渼牂艇蹉艎繕艖驖幞湟噠癢艤癥噩癬婬詮詳牷鹺蹼暀抂躂誆皇麈檉隍誑銓媓溝皝犠庠檥犧庭窯劷劻媾躾蛀狂拄黈鋌滏盖鋥曦軭廳拴廷任国弄霆輇欉蜓講望匡朢蜣鬥鬦鬧鬨鬩匩霪鬪鬫鬭鬮挵眶挺漾着荃筌住荏筐蝗罜鍠潤佯議譱筳筺佺彺往澂鎂鎈玉枉徉羊王羌鎌羍讍美玎羏玐羑玓羔宔玕羖玖羗玗玘羚莛羜玜羝宝羞玞垟玟玠羡羢玢羣群玥羦玦徨羨義玩羪玪玫羭玭羮鮮羯羲玲玳掴羴徴玵羶玷羸羹侹玹玻羼玼玽玿珀篁珂韃珅珆珈珉珊珋迋珌珍珎珏珒珓翔埕珖鯗珙寚篝珝珞珠珡叢珣珥珦珧珩珪班濮珮柱珱寳珴珵蟶寶珷鏷珸珹珺蟻珻珽現珿"));
        PART_TO_KANJILIST.put('王', new Part(4, "琀瀁琁球琄琅瀅理搆琇琉鰉琊頊對琑栓搓琚琛逛逞砡琢琤琥琦氧琨尩琩琪尫琬堭琭琮琯琰琱琲頲琳琴琵鐵琶琹琺鐽琿瑀瑁恁瑃鱃瑄遅框瑆恇瑇呈瑋瑍遑瑑瑒達鱔瑕聖瑗遘恙瑙瑚瑛瑜瑝瑞瑟偟瑠瑢瑣瑤瑦瑧瑨瑩汪瑪瑫瑭瑮瑯桯瑰瑱瑲瑳瑶鱶鱷聽衽瑾璀璁璃梃璅璆璇璉璋璏璐璑璒璘璙璚璜璞傞璟璠墡璡璢璣璦碧璧璨咩璩璪璫璮璯環璱撲璲袵璵炷璹撻璻璽璿賃瓈瓉瓊烊磋瓌裎瓏僐瓐瓓瓔僕糕瓘瓚瓛郢哢注壬飳惶購儀養洋礒紝椢褨鄯鄴餻饈煌絍饍鵟祥浧全業嵯酲嵳楻善妊再閏斑冓薘閠喤嶫嶺嶬覯羚膳痊構槎駐痒様姙釜姜嗟懟闥觧跧淫燬凭差凰懲燵旺程縒蘚戜丟樣瘥樸鈺主註渼牂艇蹉艎繕艖驖幞湟噠癢艤癥噩癬婬詮詳牷鹺蹼暀抂躂誆皇麈檉隍誑銓媓溝皝犠庠檥犧庭窯劷劻媾躾蛀狂拄黈鋌滏盖鋥曦軭廳拴廷任国弄霆輇欉蜓講望匡朢蜣鬥鬦鬧鬨鬩匩霪鬪鬫鬭鬮挵眶挺漾着荃筌住荏筐蝗罜鍠潤佯議譱筳筺佺彺往澂鎂鎈玉枉徉羊王羌鎌羍讍美玎羏玐羑玓羔宔玕羖玖羗玗玘羚莛羜玜宝羝羞玞垟玟玠羡羢玢羣群玥羦玦徨羨義玩羪玪玫羭玭羮鮮羯羲玲玳掴羴徴玵羶玷羸羹侹玹玻羼玼玽玿珀篁珂韃珅珆珈珉珊珋迋珌珍珎珏珒珓翔埕珖鯗珙寚篝珝珞珠珡叢珣珥珦珧珩珪班濮珮柱珱寳珴珵蟶寶珷鏷珸珹珺蟻珻珽現珿"));
        PART_TO_KANJILIST.put('完', new Part(1, "院浣皖完莞"));
        PART_TO_KANJILIST.put('宏', new Part(1, "浤宏"));
        PART_TO_KANJILIST.put('厓', new Part(1, "厓涯"));
        PART_TO_KANJILIST.put('宗', new Part(1, "棕宗崇淙踪綜粽"));
        PART_TO_KANJILIST.put('算', new Part(1, "簒纂算"));
        PART_TO_KANJILIST.put('厘', new Part(1, "竰甅厘糎"));
        PART_TO_KANJILIST.put('官', new Part(1, "綰管菅官舘館棺"));
        PART_TO_KANJILIST.put('定', new Part(1, "淀錠聢碇定諚綻掟"));
        PART_TO_KANJILIST.put('宛', new Part(1, "椀腕碗婉鋺宛豌蜿"));
        PART_TO_KANJILIST.put('宜', new Part(1, "萓宜誼"));
        PART_TO_KANJILIST.put('原', new Part(1, "源願原愿"));
        PART_TO_KANJILIST.put('客', new Part(1, "喀客額"));
        PART_TO_KANJILIST.put('宣', new Part(1, "諠萱宣愃暄喧"));
        PART_TO_KANJILIST.put('室', new Part(1, "榁室腟"));
        PART_TO_KANJILIST.put('厥', new Part(1, "厥蹶獗蕨"));
        PART_TO_KANJILIST.put('莧', new Part(1, "莧寛"));
        PART_TO_KANJILIST.put('莫', new Part(1, "漠驀模糢羃蟆蟇謨冪莫暮獏墓幕慕摸貘膜寞募"));
        PART_TO_KANJILIST.put('厭', new Part(1, "壓黶靨魘厭"));
        PART_TO_KANJILIST.put('箭', new Part(1, "擶箭"));
        PART_TO_KANJILIST.put('鮮', new Part(1, "蘚癬鮮"));
        PART_TO_KANJILIST.put('宰', new Part(1, "宰縡滓"));
        PART_TO_KANJILIST.put('玲', new Part(1, "玲嶺"));
        PART_TO_KANJILIST.put('殳', new Part(4, "蔎匓蘙縠瘢謦愨搬般設蠮椴穀彀轂繄罄煅慇癈繋幋摋祋驋襏潑酘鍛豛癜蕟繫譭葮聲役發鑿撃螌投榖瞖羖芟股没墢撥馨鎩疫醫骰澱醱殳殴段鞶殷殸殹殺殻媻殼墼殽殾殿臀毀壂槃毃毄毅毆鷇毉擊臋廏磐糓鷖緞廢盤磤鏧燬磬翳觳鏺蟿"));
        PART_TO_KANJILIST.put('厳', new Part(1, "厳巌"));
        PART_TO_KANJILIST.put('害', new Part(1, "豁割害轄瞎"));
        PART_TO_KANJILIST.put('段', new Part(1, "椴段鍛緞葮"));
        PART_TO_KANJILIST.put('厶', new Part(2, "蠁蠆專琉堉倊耊耋頌瀏倒簒耘逘鰘砝蠞怠怡逡鰡搢蠣耦鰪㐬萬吮怯堲逳鐵谹琺鰺耺耾砿籀瑀鱄遇恈葈塊偊屋灋呍桎灎顒偓豔塕灕籕桙摛遛鱜属瑠聡恡摡硡桧硫籬聭汯葰瑰恱呱摶偶摻塼呿傀邁璃肆沇咍蒐悛傟沟撡撤咥梥傪袪梭邰肱颱炱育傳梳治貽貿裁棄郄峅郅磈蓈蓊蓋胍胎蓏壒泒郒擒泓法磕磚瓜壜糝瓞瓟瓠惠胠擡瓢瓣飣瓤飥飦惨瓮糲飳蓴飴飶擺瓺峻能餂鄅唆蔇餇唉夋焌甎餑餕餖餗紘蔘鴘脚愚礚餚餛紜餜餟椡餢夣褦餦崧脧餧愧礪餫蔬蔭紭鄮餱餲餳餴褵餵餹餺餻餼餽餾鴾紿饀流允襁饁襂鵄充畆饆饇饈兊態絋饍饎嵎蕓鵔套慘兘饘留奙饙浚畚祛饛饞腟饟饠嵡浤公嵬畯絰慱統絵酸镸镹襼嵾镾榁喁熊綋薌閎疏禑憓喔涘醘醜膞閞喞膣醦醨醯鶲榴冶覶禸禹薹鶹禺离榼禽私釂跆旈始巍総槐嗑旒闔藕駘觚藝淞痠懡懥姪槫槮淯至致臵臶臸臹臺緻臻駿樃騃嘅踆鈆縉踋鈎昖蘛鈜爝舝訟丟瘣瘤渥刧舩戩截昪縭娭騮到嘱戴蘶鈶阹蘺刼踽昿縿驂幄穇晉晊癊詒橒詓払陖穗癘晙牟癤繦繧乨陰鉱剸牽鉾亂隂銃広隅躉劉窊銍纎檎抎云窒誒窓嚔隗溘溜亝嚠庢蚣劫抬皭麮檯麰窳皴芸劺庽抾媿囀狁髃滃雄勆廆櫆曇囈竊盍仏嫏曏狐黐鋐竑蛑櫔苔廕拚櫛勜囜髟髠拡離転髢竢竣髣髤髥嫥髦髧髩髪髫竬髬蛭髭髮髯軱勱髱竱雲髲滲黲髳髴勵髵勶髷髹髺髻狻苽髽勾髿鬀伀鬁弁鬂挃鬃弃鬄鬅朅鬆弆鬈鬉輊鬋鬌鬍鬎朎鬐蜐鬒漓匓鬖團鬘弘鬙眙蜙会鬚鬛鬜伝笞笟鬟蜟鬠鬢鬣霤匥錥弧挨強眸欸茺鬼鬽魁魂魃魄魅靆魈轉靉佉魋魌轍魍蝍荎罎魎魏魑潓魔魕魖魗魘捘罛齝孤坮齲即却捴齵齷罷彸松卿侄构羆殆澈螉侌瞌宏鎏澐鮐厔侔宖羗箛玜辝室垤鎦枩辭螭莭瞮辮辯殱枱厲枲莵厶厷厸徹厺去鮻箼厽節叀翁参參埃翃鏄叅珆埈俊翎运毓寓菘寙叝俟握矣柧迨忩忪蟪菰台鯵忶篸鏸鏹俼鯽毿"));
        PART_TO_KANJILIST.put('家', new Part(1, "嫁家糘稼"));
        PART_TO_KANJILIST.put('厷', new Part(1, "雄厷"));
        PART_TO_KANJILIST.put('殷', new Part(1, "殷慇"));
        PART_TO_KANJILIST.put('容', new Part(1, "穃熔鎔榕溶容蓉"));
        PART_TO_KANJILIST.put('去', new Part(1, "却闔法刧溘脚琺去劫蓋盍怯"));
        PART_TO_KANJILIST.put('莽', new Part(1, "蟒莽"));
        PART_TO_KANJILIST.put('宿', new Part(1, "鏥縮宿蓿"));
        PART_TO_KANJILIST.put('殿', new Part(1, "臀澱癜殿"));
        PART_TO_KANJILIST.put('毀', new Part(1, "毀燬"));
        PART_TO_KANJILIST.put('節', new Part(1, "節櫛"));
        PART_TO_KANJILIST.put('参', new Part(1, "参鯵惨"));
        PART_TO_KANJILIST.put('參', new Part(1, "滲驂參慘蔘鰺"));
        PART_TO_KANJILIST.put('寅', new Part(1, "演寅"));
        PART_TO_KANJILIST.put('密', new Part(1, "樒密"));
        PART_TO_KANJILIST.put('又', new Part(2, "鰀堅簆頍倏怒怓搔鰕蠖頗頚搜瀣倣怪搬砮蠮報吱鰲破簸吸鰹鰻蠼籆假豉汊鑊摋豎屐遐桑瑕瑗顙聚做豛葠遨衩葭豭葮籰衱聲汲汳呶鑿邀钁傁墁撃碆岌岐沒撒袚貜條肞悠股没肢墢撤撥蒦璨販被碬撮颰粲傲墼壂惄胈磉擊磐壑糓糔惙蓚壡賢波磤瓪磬擭飯裰郰鳷贅蔎焏餐鴑椒蔓級餟蔢礥愨脩攫支攱紱椴攴椵攸改鄹攻放政故饅慅煅煆慇嵈畈祋経敍腎敏襏救敕敖煖敗酘教絛啜蕞蕟敠慢散楥赧敧鵩敬赮数敲祲慳整奴敵極敷畷浸楹敺敽蕿膄斅綅薇覈醊妓薓榖馛趣冣冦馨疫醫熬綬馭妭薮醱疲熳綴覶馺閿臀跂藂槃緅鷇緊臋駑淑嗓鷖跛緞臤燦痩緩燬燮懲觳致釵嗷駸緻凾槾縀戄昄嘏蘐帑鈑騒鈒帔瘕鈘嘙瘙蘙縠渡瘢騢瘦踧舨阪樫般嘬設訯爰蘰踱娵縵娶樶騷樷鈸鈹穀繁陂繄婆版癈詉繋幋驋婌幔詖繖艘驚癜詜穝驟剟扠牧艧穫繫陬艭扱湲扳發技亂檄嚄暇皈庋蚑投暖媛抜躞亟芟檠蚤度芨躩庪努披麬皮犮芰皰骰溲皴檴皷皸皹皺隻媻皽嫂竅盈廋盋滌拏諏廏諔雘務雙嫚鋟廢盤嫩竪髪鋬仮髮髲軷鋻曼諼軽最欆鼇茇笈欉伋服伎茎鼓匓鼔鼕鼖伖鼗鬘鼙輙鼚鼛霞鼟輟弢錣圣謦霧猨弩漫伮笯霰謾圾彀坂轂蝃罄杈鍍潑鍛捜鍜彠歠坡督孥魥警蝦歧靫魬譭孯蝯鍰筱齱獲護靸役坺彼板歿鞁极玃径掇授螋螌瞍鮍掔瞖羖箙枚殛枝辤箥殩鎩掫辭微螯澯澱殳徴殴段侵鞶殷殸徹殹殺玻殻掻徼殼鮼瞽殽殾殿柀激毀寂毃毄韄翅毅濅毆寇又毉叉及友双矍反韍収叏叒叓返叔菔叕取寖鏗受篗叙叚叛鏝寝叝菝叞叟菠篠叠叡叢寢鏧濩鯫忮修矱翳援俶鏺菽埾蟿"));
        PART_TO_KANJILIST.put('叉', new Part(1, "扠蚤釵騷叉靫"));
        PART_TO_KANJILIST.put('寉', new Part(1, "鶴寉確"));
        PART_TO_KANJILIST.put('及', new Part(3, "极魥笈芨及伋岌訯扱衱汲鈒吸靸級馺圾"));
        PART_TO_KANJILIST.put('毋', new Part(4, "繁栂鋂勄梅姆拇瑇霉毋樌母毎敏毒娒毓莓悔痗纛碡慣晦實鉧誨蘩貫侮塰袰鰵海苺珻"));
        PART_TO_KANJILIST.put('友', new Part(1, "爰湲援暖煖緩髪友媛抜"));
        PART_TO_KANJILIST.put('母', new Part(5, "繁栂鋂勄梅姆拇瑇霉毋樌母毎敏毒娒毓莓悔痗纛碡慣晦實鉧誨蘩貫侮塰袰鰵海苺珻"));
        PART_TO_KANJILIST.put('反', new Part(1, "坂版皈販阪反仮飯鈑汳返叛板"));
        PART_TO_KANJILIST.put('寍', new Part(1, "嚀獰寧檸濘聹寍"));
        PART_TO_KANJILIST.put('毎', new Part(1, "塰繁毓莓悔梅晦海誨毎侮敏"));
        PART_TO_KANJILIST.put('菐', new Part(1, "菐撲僕濮"));
        PART_TO_KANJILIST.put('毒', new Part(1, "毒纛"));
        PART_TO_KANJILIST.put('比', new Part(4, "欃昆攈漉簏欐崑砒紕錕攙樚餛焜鄜鸝攟吡輥堦攦琨稭儳琵儷屁轆鍇酈孋灑坒偕陛摝湝饞鑣驪籭魮鑱彲塵楷批豼鵾鹿麀麁麂悂麃粃麄麅皆箆麇庇枇麈喈麋麌蚍麎階麏邐麑麒讒麓纔貔麕螕麖劖麗麘沘芘纚麛麝麞鎞箟麟妣麤躧玭媲薼釃緄磇巉棍菎比臕秕鏖蓖毖毘毚鯤篦諧藨揩曬仳狴混"));
        PART_TO_KANJILIST.put('叔', new Part(1, "淑寂椒督叔俶菽"));
        PART_TO_KANJILIST.put('取', new Part(1, "最叢趣掫陬撮諏娵取樶娶輙聚驟"));
        PART_TO_KANJILIST.put('受', new Part(1, "受授綬"));
        PART_TO_KANJILIST.put('毛', new Part(4, "旄耄浅橇氈残粍賎竓娓耗毛桟毟髦毫毬銭毯瓱毳践梶尾麾"));
        PART_TO_KANJILIST.put('叟', new Part(1, "嫂溲艘痩捜搜叟"));
        PART_TO_KANJILIST.put('察', new Part(1, "擦察"));
        PART_TO_KANJILIST.put('口', new Part(3, "耀怇耇耈怊怐怗怘怙耜耞耟怠耠怡耬怳恍聒恕聖恖聟聤恩恪恫恬恰聰聵悁悃悄悋悎悒肖悗悚悞悟患悤悥悦悩悪惆惇惉惊胎惑惝惠胡胭惱胱胳胴惹愇愊愍愕脗愙感愪脰愰脱脳愴脵愷愹慁慅慍腒慝腡慥腦慬腭慱腷慾憁膃膅憇膈憍膏憒憓膕憘憙憚膛憝膞膢膨憩憬膳憸憺膻憼膽憾懀懁臂懃臅懆臉臊懍懎臘臙臝臠臡臨臵懶臺懽懿戀戁戃興舌舍舎舏舐舒戓舓舔舖或舗舘舙舚戜戢戦戧戫戰戲舸船扂扃扄艄扈艙艠艢扣艱艶艷抬拈拐拓苔苕拘苛招苟苢若苦苫括拮拾拿茄挄挌挐茖茗挙茣挩挭茴茵挶茹挹茼茽挽捁捂捃荅捆捉捌捎捐捒捛捨捭据荳荷掂莂掊掌掎莒莔莕莙莟掠莦莬掴掵莵掻揀菅揅菇揈菌菏揖揜菟菩菪菫損搔搞搥搪搭萵搵搶萼落搿葆葈葊葍摑摘摚摟摠葦葫摳葴葶摶葺撏撐撑蒒蒕撘蒟撟撣撹撼蒼蒽撾蒿撿擅蓉擉擋操擎擐擑蓑蓓擔蓖擗擘擡擣擱蓲蓴擷擻擿蔀蔃蔌蔎蔐攓蔔攔攘攙攜蔞攟攣攤蔥攩攮蔽蕁故蕎敔敕蕖蕗蕙蕚敝敞蕠蕢敦敧敬敲整敵數敺蕺敽薀斂斃薔斕薗薛薜斝薝薟斣薥薧斫斲斳薹藁旖藪藳藷藹藻藾蘄蘊蘍蘐蘑蘖蘗蘘蘞昫昭蘭虂晃晄晌晑晗虞號晤晧晩虩虫虬虯景虱虵虶虷晷虹虺智虻晾蚈蚊蚋蚌蚍暐蚑蚓蚕蚖蚘蚚蚜暟暠蚡蚣蚤蚦蚧蚨蚩蚪蚫蚭蚯蚰蚱暱蚳蚴蚵暵蚶蚷蚸蚹暻暼暾蚿暿蛀曀蛁蛃蛄蛅蛆蛇蛉蛋蛍蛎蛑蛒蛔曔蛕蛗蛙蛚蛛蛜蛞蛟蛠蛣蛤蛥蛧蛩曩曫蛬蛭蛮曮蛯蛸蛹蛺蛻蛼蛽蛾蜀蜂蜃蜄蜅蜆蜇朇蜈蜉蜊蜋蜍蜎蜏蜐蜑蜒蜓蜔蜘蜙朙蜚蜜朜蜞蜟蜡蜣蜥蜨蜩蜮蜯蜱蜲蜴蜷蜹蜺蜻蜼蜽蜾蜿蝀蝃蝅蝉蝋蝌蝍蝎杏蝓蝕蝗蝘蝙蝝蝟束蝠蝡蝣蝤蝥蝦蝨蝪蝮蝯蝱蝲蝴蝶蝸蝻蝿螂螃螄螅螆螇螈螉螋螌融螐螓螕螗螘螙螞螟螠螢螣螧螫螬螭螮螯枯螱枱枲螳枳枴螵枵架枷枸螺螻螽螾螿蟀蟁蟄蟆蟇蟈蟉蟊蟋蟎蟐蟒蟕蟖柗柘蟙蟚蟜柜蟟蟠蟢蟣蟤蟪蟫柬蟭蟯柯蟱蟲蟳蟶柶蟷柷蟸蟹蟺蟻蟾蟿蠁蠃栄蠅蠆蠉蠊蠋蠍蠎蠏蠐蠑蠒蠓蠔蠕蠖蠘蠙蠚蠛蠜栝蠞蠟蠡蠢蠣蠧蠨蠭蠮蠰蠱蠲蠵蠶蠹蠺蠻蠼格桄衉桐桔衕桘衙衚衛桜衞档桮桯桰衰衷桾袁袈梈梏梚袞梠梢梧梪梮梱梲袷袺袽裀裋裍裎棓裔裕裙裛棝裞棠棫裯裳棺裾椁椅椆椋椐褒褓褘検褝褞椢椥褸襁襃襄楅楉襌楓楛楜襜楝襞襠襡襢楫襭襰襴極襷襺楼襽榀覇覉覊榍榕覗覘覚覠榥榦覬榱榲覴榾榿覿觀槁槅槌槍槑槖槗觥触槨槫觫觭觱槵觶觸觿樀言訂訃訄訅訇計訊訌討訏訐訑訒訓樓訔樔訕樕訖託記訛訝訞樞訟訠訢訣訤訥訦訪訫訬設訯許訳訴訵訶訷樹診樻註証訽訾樿詀詁詃詅詆詇詈詉橋詍詎詐橐詑詒詓詔評橕詖橖詗詘橘橙詛詜詝詞詠詡橡詢詣詥試詧橧詩詫詬詭詮詰話橱該橲詳詵詶詷詹詺詻詼詾橾詿誀檀檁誂誃誄誅誆檆誇誉檉誋誌認誏檐誐誑誒誓誕誖誗檗誘誙誚檛檝語誟檠誠誡檢誣檣誤誥誦誧誨誩説読誮檮誯檯誰課誳誶誷誹誻誼誾調櫁諂諃櫃諄諆談諈櫈諉櫉諊請諌諍諏諑諒諓諔諕論諗諚櫚諛諜櫜諝櫝諞諟諠諡諢諤諦諧櫧諫諬諭諮諰諱櫱櫲諳諴諵諶諷諸諺櫺諼諾諿謀謁謂欃欄謄謅欅謆謇謊權謋謌謎謐謑欒謔謖謗欗謙謚講欛謜謝謞欞欟謟謠謡謦謨欨謫謬謭欯謰欲謳欶謷謹欹謼謾譁譂譃譄譅譆譈證歊譌歌譍譎歎譏歐譒歒譓譔譖歖識譙歙譚譛歛譜譞譟歡譣警譫歫譬譭譯議譱譲譴譶護譸歸譹譼譽譾讀讁讃讄讅殆變讋讌讍讎讏讐讒讓讔讕殕讖讙讚殛讜讞殞讟殣殨殪殫殮殰毃毄毆毚毡毫氄氅氈氉民氓氤氳谷谸谹谺谽谾谿豁豅豆豇豈豉豊豋豌豎豏豐豑豓豔象豪豫豭貂貉貔沖貙貛沰河貴貺治沼貼沽貽沾沿賀況賂泂泅泗賙賞賠泯賲賷贃洄洇贍贏贒贖洛洞洳洸活洽浞浥赧浧浩赮浯浴浼超趈消涑涒涓涗趙趟趦涪趫足趵趷涸趹趺趻趼涼趾跀跂跅跆跇跈跊跋跌淌跎跏跑跔跕跖跗跙跚跛距跟跡淢跣跤跥跧跨跪跫跬路跰跱跲跳淳跴践跼跽跿踁踄踅踆踈踉踊踋踏踐踑踔踖減踝踞踟渟踠踡渢踢踣丣踦渦踧踪中渮踰踱串踳踴踵踶渶踷踸渹踹踽蹀蹁蹂蹄湅蹇蹈蹉湉蹊蹋湋蹌蹍蹎湎蹏蹐湒蹔蹕湖蹙蹛蹜蹝蹞蹟蹠蹡蹢湢蹣蹤乨蹩乩蹬蹭蹯蹰乱蹱蹲蹴蹶蹹蹺蹻湻蹼躁溂躂躃躄躅躇躉躊躋事躍溏躐躑躒躓躔躕躙躚躛亜躝躞亟躡躢溢躧亨躩躪享京亭亮溮亯亳溳躳溶亶溷亹躺軀軁軃滄軆軇滈滉滍滬滭軮仲滴滷滸軹軺軻滾輅漊漌漍輓輖漚輝漢輨漪輬輯估漱伵漶輷伺輻伽佀轀轄轅轉佋轎潏佑潒潓佔何轗轘佝潝潞轡潡佪潬佮潯潰佶佸使侃澄侊澋澍澎澔侗辜辝辞侞辟澟澡辣辥澧澮辯澰侶澶侷澹侻澼濁俁促濇俈濈俉俋俏俗迚俛俜保濠迠信迢俣濤迥迦迨俰迴迵濶濹追倁适逅瀆逈倉個倌逌倍逍逗倘這倚瀛倜逞倞速造逧倨瀬逭瀯週瀲倲倳逸倹瀹逼瀼瀾灃偉灉偊灊灌偌遌灎過遐偑灔違灘做停灝灞遠灣遣灤遦適偪遬遹遺避還邅邉傊邋邎傏傐邑邕傖傛傠炤炯傯邰炱傳傴炴邵点僂郃僅郈僉郌像僐僑郒僓烔僖僘郘烙僜郜烟郡郢郤僤僦部郭烱郶烹僺僻鄀鄂儃焄鄅焅儆焆儇焇儈儉儋焏儔鄖鄗儙焙鄙焞鄞償鄧鄩鄯儲鄲儳儴鄶鄷儺儻儼酃兄光克兌免兎煐煒兔兗兘党兢酤兤煦照酩酪酲酷熀熅醅醇熇冋冎醎冏醐熔冕醕熙熚醞熟醢冤冨熯熰冲醴况冶醶醸熹熺醻醼醽冾釀燀凂凅釅燈燉凉凋减凒燕燖凛凜營燥釦燭燮凱凳凾燾爉爚爛爟爤別爨刮鈳爴鈶鈷牁鉂剅牆鉆鉊削剋剌鉐牕剖牘鉙剚牚鉛剞剠鉠剣鉤剮副牯鉰剱牱割剳剴鉵創剸鉻牾牿鉿剿劀犄劅銅劈犉劊劍劒犒劔劖銗銘劚銛加犢銧劬劭劯労劶銷劼銽鋀勀鋁勅狆勉勍勏勒狗鋗鋘鋙勛鋜鋡狢勤勥鋥勦勨勪独鋭狷勷鋸勸狺鋿猄匌匐猓猗匘猞錞猟錡猢猧錧錮匰匱匲匳匵匼匿猿區獃獄鍄獅鍈鍋鍔単卛獝占獣卣卥鍧獧獨鍩獫卲獵獸獺卻獻鍼卾玁鎋鎔鎕鎖鎗鎘鎚鎞鎤鎧厨厪鎬厰厳鎶玷玽玿珂鏄珆鏆珈鏉鏑叓鏓珖叚鏜珞口鏤古句另珦叧叨叩只叫召叭叮可台叱珱史右叵珵叶号鏸司珸鏹叺珺珿琀吁吂吃各琄合鐈吉吊吋同名鐍后吏吐向琑鐓吓鐙吚琚君吝吟吠鐡吡否琦吧吨吩吪含听吭吮启琯吱琱鐱鐲吴鐴鐵吵吶鐶吸吹鐺吻吼吽吾瑀呀呂呃鑄呄呆呇呈呉告瑋呍呎呏呑鑓瑙瑚鑜呞鑞呟瑢呢瑣呤呦呧周呩呪呫呭瑭鑭呮呰鑰呱鑱瑲鑲味呴呵鑵呶呷呻呼命鑾瑾呿咀咁璁钃咃咄钄咅咆咈咉咋和咍咎咏璐咐咑璒咒咕咖璚咜咟璟咡咢咤咥咦璧咧咨咩咪璪咫璫咬咭咮璮咯璯環咱咲咳咷咸咹璹咺咻咼咽咾咿哀品哂哄哆哇哈哉哊响哎瓓哘瓘瓛哠員哢瓤哥哦哨哩哪哬哭哮哯哲哶哺哼哽哾哿瓿唀唁唄唅唆唇唈唉甌唌唍甎唎唏唐甓唔唕唖甗甜甞唪唫售唯唱唲唳唵唶唸唹唻唼唽唾啀啁啄畄啅商啇啉啊啌啍問啐畐啑啓啖啗畗啘啚啛啜啝啞啠啡啣啤略啦畧當畸啻啼啾啿喀喁喂喃善喆喇疇喈喉喊喋疎喎喏喑喒喓喔喗喘喙喚喜喝喞喟閣喣閤喤喧喨閩喩喪喫閫喬閭喭單喰閲喲疴営喻閾喿痀嗁痁痂嗃嗄嗅嗆闆嗇闈嗉闊嗋闌嗌痌嗎嗑嗒闓嗓嗔嗗痘闘嗘闙嗚嗛嗜痞嗞嗟闟痟闠闡闢嗢嗣嗤闤闦嗩痯痴嗶嗷嗹痼嗽痾嗾嗿嘅嘆嘈嘉嘊瘋瘌嘍嘎嘏嘐嘑嘒嘔瘕嘖嘗嘘瘙嘙嘛瘟瘡嘩瘩嘬嘯嘰嘱嘲嘳嘴嘵嘶嘷嘸瘸嘹嘻瘻嘼嘽阽阿嘿噀噁噂噃噄噆噉癉噋癌噌噍噎噏噐噔癖陗噛噞噠噡噢噣噤噦器癩噩噪陪噫噬噭噯癰噱癱噲癴噴噵噸険噺登嚀嚄嚅嚆嚇嚈嚊嚋嚌嚏隑皓隔嚔隕嚕嚙皚嚚隚皜嚝皝嚞嚟嚠隠嚢隤隥嚥嚦隦嚧嚨嚩險嚫嚬嚭嚮隯嚱嚳嚴嚶皷嚷嚼皽嚾囀囁囂囃囅盅囈囉囊雊囋囌囍囎盎囏囐囑盒雒囓雕雖囗囙囚雚四囜囝雝回囟雟因囡団難囤囥囦囧囨囫盬园囮困囱囲図囶囷囹固国囿圀圁圂圃圄霄霅圇圈圉圊國圌圍圏霑圑園圓圕圖眗團眙圚圜圝霝眠霣眮霱露霸霹靄靈靊睊睏靗靜靠靣靧坧革靪靫坫靭靮坮坰睰靱靳靴靶靷坷靸靹靺靻靼靽靿鞀鞁鞄鞅鞆鞉鞋垌鞍瞎鞏鞐鞕垕鞖鞗鞘鞙垙鞚鞜鞞鞟鞠瞠垢鞢鞣瞥鞦垧鞨瞪鞫鞬鞭鞮鞱鞲鞳鞴鞵瞶鞶鞸鞹鞺瞻鞼瞼瞽鞾鞿韁韃韄韅韆韇韈韉韊韋韌韍韎韐韑韓韔矕埕韗韘韙矚韛韜韝韞矞域矟韠埠韡韤知短矯矰石矴韶矸培韺韻矻埼矼砂堂砅砆砉堌砌堍砍砎砑砒研砕堛頜堝砝砠堡頡砡砢砣砥砦砧頭砭砮砰砲破砵頵頷砷砺頼砿堿硃硄硅硇硈顊硌額顎硎塏顑硒塔顖顗塘塙硜硝硞硠硡硣硤塤顥硨塩硪顫硫硬确硯硲硴顴塸硺硼塾硾塿碁碆碇碊碌碍碎碏墐碑碓碔碕墖碗碘碚碝碞碟墠碡墡碣碤碧風碨碩墩颪碪颫碬颭碭颮颯碯颰碰颱墱碱碲碳颴碵颶颷颸颺確颻墻碻碼碽碾颿碿壁磁飂飃飄飅磅飆磆壇磇飈壈磈磉磊磋飌壌磌磎壎磐壑磑磒磓磔壔壕磕磖磚磛壝磟磠壡磡壤磤磦磧磨壩磪磬磯磲磳磴飴磶壷磷壹磺磻飼磽壽磿礀礁餂礆礇餇餉礌礎礐礑礒餖餗礙礚礜礞礟礠夡餢礥礦礧館礩礪礫餬礬礭礱礴礵餹饂奆奇饇饋饍饎祏饐祐饕饘祜祝奝饞饟祠奩祫奲祻禀如禅禍福禕妕馘禧禪妬禮禱禳禴禹馽姁駉始姑駒駕駘駚駛姞駟姤秥駧駫姯駰駱秱駵姶秸姻駽駾稇程稍税騎騒験稕稛騞稟娟稠騢娧騧騨娩娪稫娯騷稾稿婀婁穃婄驄驅驕驖穗驗驚驝穡穣驤驩穰婷窖窗窘媧窩媬媲窶媸窹窻骼媼嫆竆髏髑竒體嫗高站髛髜髞嫡嫥竦嫦嫩髫竬竱競髺髻嬁嬉嬋嬌鬍鬎嬖嬗笘嬙鬙嬛鬛笞鬟鬠嬢鬣笥鬪鬭笱鬲笳鬳鬴嬴笴鬵鬷鬹鬺鬻嬾笿孁孃筈魈孌筎筒答魗筥学孰筲孼孽孿箇箉鮉鮎鮐鮔宕鮖箘官箚鮚管客鮦箪箬宭宮鮰宲害箴宵箶鮸容鮹宺寃寄富寏篔篖篙鯛鯝寠寤篦鯨篩寰簂簄尅專簉鰊尋尌簌簍鰏鰐簑簓簔鰔鰕鰙尚簣鰤簥簦簬鰮就尵簷簹簽局鱄居鱊籌鱎屑鱓籔鱔鱘屙鱚鱛籝属鱞籟籡屢鱣籣籥鱧鱨屨屩屬籲鱲鱵鱷粔粘岠粡岢岣粤岩粭岲岵岷岾峇峉糊糏峒糔糖糙峝糦糩峪糫峭峮鳴糵崎鴝鴞鴣鴬鴰紹鴼鴽紿鴿崿絅鵅絇嵐結鵑嵒絖絗嵜鵠絡嵢鵣給絧嵩絪嵪絮鵰嵹絹絻絽嶁綂綃嶇鶉綌綑嶒鶒綗鶘鶚嶝嶠綢綧鶫鶬嶮鶮綰嶹綹綺嶽巇巉鷊巋巌緎巐巒鷕巖鷗緘巘緙緝緡巣巤緥鷧鷮緯鷰鷲鷸巸鷺緺縀鸇鸊縋鸑縕帖縗鸙鸛縜鸞縞帥縨帨師縳縷常總幃幅幌繐繒繕幗繘幜繟繢幣幤繥繦幨幬繭幮繯繰鹵鹸鹹鹺鹻鹼鹽繾纁纆纈麌續麏纔麕纕麖店纞庽麿黋黏廑廓廔黕廚點黟廠黠黤黥廥黧黨廩黬黭黮黰廱黱黲黵黷黸廻弇鼈鼉弊鼓鼔鼕鼖鼗鼙鼚鼛鼟鼡弡鼦弨鼫鼯弰弴弶強弾缿彁齁彄齆彈彎罐当齝齞罟罠齠彠齣罥彧齪彫齬彭罭齮罰影齲齶罸罾羂羇羈徊龕龗徜龠龡龢羣龣群龥徫羶羸翀翕忠忡翮翯翾翿"));
        PART_TO_KANJILIST.put('古', new Part(1, "樀讁尅箇個醐蔐怙辜踞鴣倨謫餬錮枯估鈷涸沽詁蛄凅居故糊克剋姑鏑歒湖摘做瑚楜罟嫡胡兢古苦適葫据蝴滴敵鋸固痼裾擿"));
        PART_TO_KANJILIST.put('句', new Part(1, "局檠齣鉤句煦警劬敬怐駒狗拘枸驚跼佝苟蒟"));
        PART_TO_KANJILIST.put('寧', new Part(1, "嚀獰寧檸濘聹"));
        PART_TO_KANJILIST.put('審', new Part(1, "審瀋"));
        PART_TO_KANJILIST.put('只', new Part(1, "枳只咫"));
        PART_TO_KANJILIST.put('菫', new Part(1, "懃勤菫"));
        PART_TO_KANJILIST.put('寫', new Part(1, "瀉寫"));
        PART_TO_KANJILIST.put('召', new Part(1, "齠貂迢超照髫召劭昭詔邵韶紹招沼"));
        PART_TO_KANJILIST.put('可', new Part(5, "婀彁珂寄椅奇羇歌謌崎掎騎竒碕何猗倚苛嵜剞哥渮可柯河呵訶荷畸舸欹綺軻埼痾阿"));
        PART_TO_KANJILIST.put('華', new Part(1, "譁曄嘩樺華"));
        PART_TO_KANJILIST.put('台', new Part(1, "怠怡殆始抬胎台颱詒苔飴冶駘治貽笞紿"));
        PART_TO_KANJILIST.put('史', new Part(1, "史駛"));
        PART_TO_KANJILIST.put('右', new Part(1, "祐佑醢右若惹慝諾匿"));
        PART_TO_KANJILIST.put('毳', new Part(1, "毳橇"));
        PART_TO_KANJILIST.put('号', new Part(1, "饕号號"));
        PART_TO_KANJILIST.put('寸', new Part(3, "封専尃射尅将將專尉尊吋尋尌對討導搏鐏耐簙縛爝縟鰣怤耨縳爵樹樽簿噂時恃附鑄鱄幇籌葑鱒塒湗鱘鉜蹡虢葤陦詩幫幬幮鑮蹰橱蹲遵摶剸特塼蒄傅躊蒋嚋碍犎墏撏咐蒔躕肘撙府麝傠庤溥嚩蒪墫皭檮隯傳璹蚹嚼梼溽囀髆軇櫉拊棏蓐僔壔胕付峙磚廚鋝団擣嫥狩竱鋳蓴竴苻賻壽持紂欂甎圑儔團蔚謝欝蔣弣褥符椨霨鄩鬪礴餺愽漿蕁罇轉等捋獎酎腐腑村祔魗博彠畤酧奨奪奬捬潯慰慱畴祷罸譸酹坿薄待薅疇守嶈膊侍澍榑鮒鎒得涛鎛膞嶟冠醤熨厨垨醬榭榯辱禱螱嶹醻螿釂鏄槈篈柎埒埓痔忖燖跗鏘闘駙懟濤槫俯跱槳蟳寸寺寽対燾寿翿"));
        PART_TO_KANJILIST.put('司', new Part(1, "祠嗣笥覗司伺飼詞"));
        PART_TO_KANJILIST.put('寺', new Part(6, "持時恃畤待等詩侍塒蒔痔特峙寺"));
        PART_TO_KANJILIST.put('寿', new Part(1, "鋳畴陦祷涛梼寿"));
        PART_TO_KANJILIST.put('封', new Part(1, "封幇"));
        PART_TO_KANJILIST.put('萃', new Part(1, "萃膵"));
        PART_TO_KANJILIST.put('尃', new Part(1, "尃博縛"));
        PART_TO_KANJILIST.put('各', new Part(1, "喀賂各輅貉挌額咎茖蕗烙洛珞絡客狢閣略畧恪酪咯路擱駱露鷺格骼落"));
        PART_TO_KANJILIST.put('射', new Part(1, "射謝麝"));
        PART_TO_KANJILIST.put('将', new Part(1, "醤将奨蒋"));
        PART_TO_KANJILIST.put('將', new Part(1, "蔣將鏘奬獎漿"));
        PART_TO_KANJILIST.put('合', new Part(1, "荅峇合哈盒塔答翕龕歙箚蛤閤給搭粭恰剳鞳姶袷洽拾拿鴿"));
        PART_TO_KANJILIST.put('專', new Part(1, "囀慱傳蓴摶專團轉磚槫甎"));
        PART_TO_KANJILIST.put('吉', new Part(1, "纈吉嘉嬉澎舎簓鼓鼔鼕鼖鼗舗鼙憙鼚鼛喜鼟稠鐡頡綢禧膨蜩週鐵皷熹樹薹劼瞽調惆凋結桔雕僖鱚廚鯛黠擡捨周彫襭彭拮詰橲佶臺髻"));
        PART_TO_KANJILIST.put('尉', new Part(1, "慰熨尉蔚"));
        PART_TO_KANJILIST.put('吊', new Part(1, "蟐嫦常吊"));
        PART_TO_KANJILIST.put('尊', new Part(1, "噂蹲鱒遵尊墫樽"));
        PART_TO_KANJILIST.put('尋', new Part(1, "蕁尋潯"));
        PART_TO_KANJILIST.put('同', new Part(1, "桐粡筒胴銅興恫同洞"));
        PART_TO_KANJILIST.put('名', new Part(1, "茗銘酩名"));
        PART_TO_KANJILIST.put('后', new Part(1, "垢逅詬后"));
        PART_TO_KANJILIST.put('小', new Part(3, "砂尉逍小鐐少尓途瀕尖耖尗倘蠙倞尞尟尠瀠倧琮鰰就頴吵鰶鰷頻蠻搽鰾摋鑌塐屑葒偗塗願籘遜灝灣顣瑣灤葤顥呩顪桫籮葯顰遼鑼摽鑾傃傆悆粆邎邏炒蒓沙璙蒜璟您撩袮悰傺粽蓀飃飄僄惄惊賓棕賖僘僚惝裟瓢擦僦磦蓧賨哨擯糸糺系糾紀紂蔂紃約紅紆紇崇焇紈紉紊紋椋納紏紐紑紒褒椒紓純紕紖紗紘餘紙蔚級紛紜紝攞紞素紡蔡索攣紣紦紪紫愫紬紭儭紮累細紱紲紳紵夵儸紹紺示紼紽蔽紾褾紿愿絀祀絁祁終絃組絅絆絇祇絈奈絋経絍敍絎絏結絑絓慓祓祕絖絗祗祘敘絙絚祚絛絜絝蕝敝絞襟祟蕠饠祠絡祢絢絣絥給絧絨票絪畭祭絮慰絰統絲絳酴絵絶絸絹絺襺祺絻慼絽絿祿禀綁禁綂涂趂綃斃綅綆膆綈綉薉榊禊綋綌綍覍斎鶎綏綑經綖綗膘妙継続綛熛嶛綜斜禜綝禝綞綟榡綢綣綦禦綧禧熨綪禪綫綬憬維憭綮禮綯綰禰綱網喲綳禳妳綴綵綶綷綸薸冸綹綺綻涼綽綾綿醿緂緃緄緅緆緇嗉凉緊緋緌緍緎燎総緑淑秒緒巒緕緗緘觘緙淙線凛緜緝緞痟締臠緡緢緤緥鷥釥緦痧編緩嗩緪緫緬緭槭緯鷯称緱緲鷲練緵緶懸緹緺緻戀縀縁縄踄縅縈渉縉縊蘊蘋縋騌稌樏縐娑縑縒鈔縕縗標戚縛縜縝縞鸞縟縠縡縢縣樤縦縧踧縨蘩踪縫縬訬縭瘭騭縮縯瘰蘰縱縲縳縵瘵騵縶縷縹嘹渺縺縻渻總績騾蘿縿繁療繃驃噄繄穄繅艅繆虆穆繇繊繋婌繍繎繐橑繒織繕繖幖繘繙蹙繚幜繝繞陟繟剠繡繢穢幣除噤繥繦繧鉨繩虩鉩繪繫繭繮繯景繰繳蹴癴繸繹繻繼繽剽繾晾繿乿纁纂纃抄纆纇纈纉纊續纍纎纏纐源纑纒纓纔纕纖麖纘隙纚纛際纜纝纞劣麨嚩檫嚫京嚬銯檰亰隲檳隷隸暸暻暼雀滁嫄囉勍諒諔髕嫖櫞勡黥曫勬櫬仯狳苶嫽髿省漂鬃猄眇漈鼈弊県蜍眎欏欒挘謜圝錝鬢弥霨嬪輬茮漯伱弶猻款鍄魈齋孌彎捎轑潔捗佘余卛彝你獠轡督潦魦歩獩杪孫孮彯影筲歳捺荼齽孿玀羂羅玅螈變澋鎍莎徏徐龒徖宗徜瞟原掠瞥鎩瞭辮殯徱螱螵厵澵箵螺徽鯀係寂俆鯊濊鏍珎俏叔矕叙叞毟察蟟鏢鯨篨迩寮毮濰柰濱翲寳寴俶菽翽"));
        PART_TO_KANJILIST.put('氏', new Part(4, "纃帋躋暋茋嚌愍厎氏昏舐氐膐蠐民劑氒氓底儕紙袛羝鴟怟眠霢弤砥芪昬隮蚳抵骶岷邸薺冺阺岻霽涽銽閽抿虀蟁奃敃姄詆祇珉齊齋臍緍齎低齏棔秖祗觗婚惛泜胝觝濟罠擠苠緡柢湣穧呧睧秪泯忯桰韲鯳牴衹扺坻彽惽赿"));
        PART_TO_KANJILIST.put('吏', new Part(1, "吏使"));
        PART_TO_KANJILIST.put('氐', new Part(1, "氐底抵邸"));
        PART_TO_KANJILIST.put('向', new Part(1, "向餉嚮"));
        PART_TO_KANJILIST.put('少', new Part(1, "雀省砂抄眇渉鯊蘋莎少娑炒秒賓鈔瀕捗紗妙沙裟毟陟尠鬢劣歩嬪杪擯殯顰濱緲隲檳渺頻繽"));
        PART_TO_KANJILIST.put('民', new Part(1, "眠罠民緡氓岷愍泯"));
        PART_TO_KANJILIST.put('尓', new Part(1, "称祢尓寳弥迩珎袮"));
        PART_TO_KANJILIST.put('气', new Part(4, "氣氤氦氧氨滊氬氮氳气気氙氛餼汽愾氟"));
        PART_TO_KANJILIST.put('尚', new Part(3, "耀堂戃栄霄挄欅氅焇鼈弊稍逍礑琑樔嘗倘挙尚輝褝甞償猟鼡戦騨縨攩鴬弰愰脳常洸簹鐺儻蔽弾晃晄畄桄硄艄魈光蝉削蝋幌恍捎屑当橕絖橖陗靗単党摚牚桜靜鑜硝敝敞襠档幣獣瑣幤兤学鱨虩筲當襷剿熀斃綃悄禅消誉侊掌榍撐撑鎖肖鞘趙垙誚覚隚膛徜讜皝趟瞠隠梢溢鎤瞥榥莦銧悩箪璫厰螳厳労宵営銷鮹撹躺鞺暼碿滉擋黋巌淌蛍俏糏蟐韑珖僘鏜惝賞痟矟廠棠巣觥嫦勦哨黨嗩駫峭姯胱珱裳蟷蛸鋿"));
        PART_TO_KANJILIST.put('君', new Part(1, "郡羣群窘裙君桾"));
        PART_TO_KANJILIST.put('吝', new Part(1, "悋吝"));
        PART_TO_KANJILIST.put('尞', new Part(1, "療尞寮"));
        PART_TO_KANJILIST.put('尢', new Part(3, "兂霃鴆嵆黆沈眈兓醓枕黕厖酖冘巘蚘萙鑙紞尢疣尤邥僦訦髧尨魫肬庬諬躭就忱鷲蹴魷犹牻稽耽"));
        PART_TO_KANJILIST.put('氣', new Part(1, "氣愾"));
        PART_TO_KANJILIST.put('尤', new Part(4, "疣尤僦嵆尨肬庬諬就鷲蹴厖魷巘蚘犹鑙牻稽"));
        PART_TO_KANJILIST.put('否', new Part(1, "否痞"));
        PART_TO_KANJILIST.put('尨', new Part(1, "厖尨"));
        PART_TO_KANJILIST.put('含', new Part(1, "頷含莟"));
        PART_TO_KANJILIST.put('萬', new Part(1, "邁糲蠣勵癘礪萬"));
        PART_TO_KANJILIST.put('㐬', new Part(1, "流旒梳毓棄琉硫㐬蔬疏醯"));
        PART_TO_KANJILIST.put('尭', new Part(1, "暁焼尭"));
        PART_TO_KANJILIST.put('就', new Part(1, "就鷲蹴"));
        PART_TO_KANJILIST.put('水', new Part(4, "球騄砅漆爆脉踏瀑逑錔漛丞瀟瀠霡樣阥漦簫琭逮頮嘯洯逯褱録水昶氶簶氷永昹怺氺漾尿漿潁求捄顄穅靆鱇呇繍籐救彔浗籙剝汞詠剥灥襮遲湶慷捸腺桼楾祿絿承醁梂澃禄碌邌莍咏沓閖沗溙劚粛鞜膝嚟厡龣冰暴涵隶粶銶康隷蒸隸誻曃俅瓈泉菉黍黎滎黏黐緑滕賕裘様線藜烝曝糠柡棣藤盥淥囦觩毬埭拯泰泳泴巹淼函"));
        PART_TO_KANJILIST.put('氵', new Part(3, "瀀瀁瀅瀆瀇瀉瀋萍瀍瀏瀑瀕簗瀗瀘瀚瀛瀝瀞瀟瀠萢瀣瀦瀧瀬瀯瀰瀲瀴氵瀷瀹琺氻瀼落氾瀾簿氿汀汁灃灄灈灉汊灊汋灋灌衍汍汎灎汏葏汐灑汒籓汔灔汕灕汗塗灘摘汙汚汛汜汝灝灞江池汢灣灤汧汨適汪桫汫汭鑮汯汰塰汲汳汴汶汸汹決汻汽汾梁沁沂沃沅蒅碆沆沇沈沉沌沍沐沒沔沕沖沘沙沚沛沜蒞沟没沢沫沮沰沱粱蒲沲河沴沸油沺治沼沽沾沿況泂泄泅泆惉泊泌壍泍泏泐泑泒泓泔法泖泗泙泚泛泜泝裟泠泡波泣泥泧注泩泪泫泬泮泯泱蓱泲磲泳擿洄愆蔆洇洊洋洌洎洏蔐洑洒洓洗洙洚洛洞洟蔢津洦洧洨洩洪洫洮洱洲洳蔳礴洵洶洸洹鴻活洼洽派洿流慂浄浅酒蕖浗浙浚浜浞浟浡浣浤浥浦浧浩蕩浪奫浬浮浯浰浴敵海浸浹浼薀涂薄涅涇消涌涎涑涒涓薓涔涕涖涗涘涙涛涜涪涬涯液涴涵涷涸薸涹涼涽涿淀淄淅淆淇淈闊淊淋淌淎淏淑淒淕淖淘淙燙淛淝淞淟淠淡淢懣淤淥淦痧淨藩淩淪淫淬淮淯淰深淳淴淵淶混淹淺藻添渀樀鸂渄清渇済渉渊渋娑樑渓渕渙嘙渚減渝渞渟渠渡渢渣渤渥渦渧温渫測渭渮蘯港渲渶游渹渺渻渼渾湃湄湅婆湈湉湊湋湍湎湏湑湒湓湔湖湗湘湛湜湝湞湟湢湣湧湨湫湮湯湲湳湶湻湽湾湿満溂溌溍溏源溓準溘溙溜溝溟溠溢溥溧窪溪溭溮溯溱溲溳溶溷溺溻溽溿滀滁滂滃滄滅滇滈鋈滉滊滋滌滍滏滑滓滔盜滝滞嫡盪滫滬滭滮滯滲滴滷滸滹滻滽滾滿髿漁漂欂霂范霃漄漆霈漈漉漊漌漍漏霑漑漓演漕漖漘漚漛漠漢漣漩霪漪謫漫茫漬漭匯漯漰漱漲漳茳笵漶漸漻漼漾潅潏潑歒潒潓潔潗潘潙潚潛潜潝潞潟潡潢潤潦潨潬潭潮潯潰潴潸潺潼潽潾澀讁澁澂澄澆澇澈澋澌澍莎澎鎏澐澑澒澓箔澔澖澗澚澟澠澡澣澤澥箥澦澧羨澨澪澮澯澰澱箲澳澵澶澹澼垽激濁濂濃濅濆濇濈鯊濊篊菏鏑柒染濔濕寖濘濚濛濞濟菠濠濡濤濨濩濫濬濮濯濰濱濳濵濶濹菹濺濼濽濾埿"));
        PART_TO_KANJILIST.put('谷', new Part(7, "豁穃豅嫆蓉綌壑熔鎔裕榕俗傛壡瑢郤逧峪硲欲浴溶谷谸容愹谹谺卻谽慾谾谿"));
        PART_TO_KANJILIST.put('尸', new Part(3, "堀簄昈尉鸊帍嘏娓倔蘗騙琚君踞瘡渥倨樨怩鈩鈬昬舮鈮启嘱爲訳鐴昵搶刷尸戸阸騸戹萹尹尺瘺稺戻尻昼尼戽尽尾房尿所局塀扁蹁屁扂扃幄扄湄居遅扆屆扇扈屈扉穉届屋蹌遍屍蹍呎屎偏屏屐艑屑摒屓偓展牖癖艙屙屚癜屜属屠屡呢屢屣湣層履顧屧屨屩屬屭聲遲瑲晵桾避犀墀肁躃钃躄肇肈劈炉暋粐傓傖檗窘媚劚撝択窟沢芦隦璧肩咫蚭梮抳誳梶悷碾抿碿壁壂滄糄囅雇糏囑胒擗擘裙棙髛廜僝僞諞苠郡泥棨苨滬磬声僱鳲鋸僻惼惽裾郿謆眉褊伊笋漏椐錑甓椖嬖蔚崛崢眤謦霨崫唳愴猵挶霹攺匾輾敃罄嵋譌轏腒啓蝙襞楣捩鵩譬坭据慰孱齷潺煽鍽榍妒斒喔掘涙薜綟辟羣群枦徧熨馨侭綮掮妮澱螱侷殸冺澼箼羼馿殿臀蟁臂姄駅柅嗆痆篇釈淈珉藊臋旎闙矚叞握闢凥鏧編翩濵跼鯿埿"));
        PART_TO_KANJILIST.put('永', new Part(1, "詠樣泳昶永脉怺漾咏"));
        PART_TO_KANJILIST.put('尹', new Part(1, "郡羣群窘尹裙伊君笋桾"));
        PART_TO_KANJILIST.put('尺', new Part(1, "沢訳駅釈尺咫昼鈬侭尽呎択"));
        PART_TO_KANJILIST.put('氺', new Part(1, "球禄漆爆碌逑瀑粛膝簫逮嘯録暴涵隶康隷隸氺求曃穅靆鱇黍繍黎黏黐籐救緑滕裘様藜剝曝糠棣藤剥毬埭泰慷捸桼函祿"));
        PART_TO_KANJILIST.put('尼', new Part(1, "眤昵泥怩尼"));
        PART_TO_KANJILIST.put('尽', new Part(1, "尽侭"));
        PART_TO_KANJILIST.put('吾', new Part(1, "唔圄寤晤梧珸衙齬吾牾語悟"));
        PART_TO_KANJILIST.put('尾', new Part(1, "梶尾"));
        PART_TO_KANJILIST.put('氾', new Part(1, "范笵氾"));
        PART_TO_KANJILIST.put('局', new Part(1, "局跼"));
        PART_TO_KANJILIST.put('呂', new Part(1, "梠呂筥侶営櫚絽閭宮營麿"));
        PART_TO_KANJILIST.put('求', new Part(1, "救逑求球裘毬"));
        PART_TO_KANJILIST.put('居', new Part(1, "居倨鋸据裾踞"));
        PART_TO_KANJILIST.put('豆', new Part(7, "嬁嬉尌鼓鼔鼕鼖餖鼗逗鼙鐙鼚鼛鼟鐡簦鄧鬪頭脰戲愷鄷樹嘻灃獃剅豆譆豇豈證豉豊靊豋豌噎豎灎饎塏豏豐饐豑豓噔灔豔歖顗橙鱚艠繥鱧蹬彭幮蹰橱橲荳剴艶艷登澄澍澎隑璒躕憘螘憙皚喜嶝暟隥鎧禧澧膨厨殪梪瞪覬嚭禮嚱墱醴覴皷熹熺瞽榿暿曀鋀軆巇燈櫈櫉裋囍囏磑凒闓體僖痘闘廚蟚僜蟢糦闦鷧短凱凳磴壹懿"));
        PART_TO_KANJILIST.put('呆', new Part(1, "棠堡褒褓襃呆葆保"));
        PART_TO_KANJILIST.put('豈', new Part(1, "凱磑剴鎧豈皚覬榿"));
        PART_TO_KANJILIST.put('呈', new Part(1, "郢酲鐵呈程逞"));
        PART_TO_KANJILIST.put('屈', new Part(1, "堀倔屈掘崛窟"));
        PART_TO_KANJILIST.put('呉', new Part(1, "茣誤蜈呉麌虞娯"));
        PART_TO_KANJILIST.put('豊', new Part(1, "醴體艶軆鱧豊禮"));
        PART_TO_KANJILIST.put('告', new Part(1, "造靠鵠皓慥誥窖晧酷浩告梏"));
        PART_TO_KANJILIST.put('屋', new Part(1, "握幄渥齷屋"));
        PART_TO_KANJILIST.put('屎', new Part(1, "屡屎"));
        PART_TO_KANJILIST.put('屏', new Part(1, "塀屏"));
        PART_TO_KANJILIST.put('豐', new Part(1, "豐艷"));
        PART_TO_KANJILIST.put('呑', new Part(1, "呑僑驕橋喬嬌蕎轎矯"));
        PART_TO_KANJILIST.put('豕', new Part(7, "縁圂瘃逐椓蠓蠔褖礞堟蠡琢甤瀦朦蘧鐻稼椽慁鱁遂啄瑑潒豕彖豗豘豙塚豚襚豛饛蝝豝穟象橡豢豣蕤豤豦籧艨豨豩豪幪豫豬豭遯噱豳豵豶繸譹硺遽邃劇隊喙蒙墜冡冢傢隧璩檬璲醵鎵家溷掾涿嫁毅篆矇像諑壕糘曚據濛櫞懞濠盠燧勨櫲篴蟸燹"));
        PART_TO_KANJILIST.put('展', new Part(1, "展碾輾"));
        PART_TO_KANJILIST.put('著', new Part(1, "著躇墸"));
        PART_TO_KANJILIST.put('豚', new Part(1, "豚遯"));
        PART_TO_KANJILIST.put('葛', new Part(1, "臈葛"));
        PART_TO_KANJILIST.put('属', new Part(1, "嘱属"));
        PART_TO_KANJILIST.put('江', new Part(1, "鴻江"));
        PART_TO_KANJILIST.put('象', new Part(1, "象橡豫像"));
        PART_TO_KANJILIST.put('周', new Part(1, "稠週綢簓雕惆周蜩凋彫鯛調"));
        PART_TO_KANJILIST.put('豪', new Part(1, "濠壕豪"));
        PART_TO_KANJILIST.put('豬', new Part(1, "瀦豬"));
        PART_TO_KANJILIST.put('屬', new Part(1, "囑矚屬"));
        PART_TO_KANJILIST.put('屮', new Part(3, "窀劂撅謅逆搊沌鈍縐鄒犓頓蒓朔芔純炖芚憠搠弢厥辥趨邨吨蚩匩肫愬蒭溯蒴鶵騶媸皺芻槊迍塑闕鱖獗豘雛橛詜雟遡嗤囤蕨魨飩屮屯屰櫱瓲忳蹶杶噸艸盹齺孼扽旾"));
        PART_TO_KANJILIST.put('屯', new Part(4, "窀囤邨吨魨飩肫沌鈍迍屯瓲頓忳蒓純杶炖噸豘盹芚扽旾"));
        PART_TO_KANJILIST.put('山', new Part(3, "堀萄堈嘊耑樒倔訔訕搖丗搗氙両昢舢鰩帯蠵踹繃灃遄繇屈豈癌湍塏豐幑豓顓豔汕顗遙剛瑞乢瑤癥遥摧山豳剴屴屵屶瑶陶艷屹屺屻屼籼屽幽満璀咄岇岈岊岌岏岐岑窑隑岒貒岔墔亗皚暚傜岝窟岟暟銟岠皠岡岢岣梣岦岨岩蚩岪岫催岬炭傰窰岱媱岲岳碳岴岵岶岷岸媸媺岺岻颻岼墼岾棄峅峇峉峋泏磑峒峗拙仙峙仚黜峝滞峠棡峡峨峩峪磪峭峮端峯峰峱峲惴黴峴島糶峺峻鋼峽崁眄漄崆崇崋匋輌圌崍褍崎朏崑崒崔崕崖崗崘崙崚崛攜崟謠謡崢弢崣崤蔤崦崧崩匩崫猯漰崱鬱崴缶愷缸圸崹缺猺缻缼漼缽崽缾缿崿嵂罃嵃獃罄罅嵆罇嵈嵊靊嵋罌嵌罍罎嵎罏罐嵐嵑嵒嵕嵙嵜祟嵟嵠嵡嵢杣嵤腨嵩嵪嵬嵭嵯嵰嵳鍴嵶祹嵹嵺嵾嵿嶁嶂澂嶃嶄嶇薇嶈閊嶊嶋嶌掏嶐嶒嶓嶔涔嶕掘喘螘嶙妛嶛嶝疝嶟嶠嶢掣鎧嶧嶫嶺覬嶬徭微嶮綯嶰綱嶲徴嶴嶷嶸嶹嶺嶼徽嶽榿辿鷂巃篅旆密巇淈秈巉巋巌巍巎巐巒凒巓闓巖淘巘巙鏙寚槝揣嗤柮槯凱懲旵寶旹揺出觽觿"));
        PART_TO_KANJILIST.put('米', new Part(6, "娄鐇渊瀋瀟氣嘯噃婅橉繍屎橎驎籓籔鱗繙噛類屡幡噢蹯米遴籵籹籼籽籾粁粂粃粆粇纇悉粉粋麋粍粏粐粒粔粕粗粘璘粛撛粞粟麟璠粠粡粢隣粤皤銤粥墦粦粧粨璨隩咪粫播粭粮粰粱粲粳麴粶粷麹粹墺粺粻粼粽精蒾粿糀糂糄糅糇糈糉糊竊滊諊糍糎糏糒囓糓糔糕糖糗糘糙糚糜飜糝糞糟糠糢糦糧糩糫糯僯糲糴糵糶磷磻礇椈匊謎眯鄰鄱嬸鬻餼愾蕃敉齒譒轓齓齔轔齕襖齖齗潘齘齚彛彜彝齝齞齟齠齡齢齣奥来齦齧奧齨齩齪番齬蕭齮歯齯齰数齱齲齳齵齶齷齺楼齽潾薁讅侎憐嶓継料嶙澚憟鞠宩殩掬断薮澯膰莱澳嶴斴瞵宷釆釈釉懊菊蟋釋燐燔旙旛燠蟠燦藩審鷭毱迷翻鏻"));
        PART_TO_KANJILIST.put('豸', new Part(7, "貂貅貆懇貇邈貉貊貋貌貍貎藐貐貒貓貔貘貙貛貜薶豸豹豺豻豼霾墾豾"));
        PART_TO_KANJILIST.put('命', new Part(1, "掵命"));
        PART_TO_KANJILIST.put('貌', new Part(1, "藐貌"));
        PART_TO_KANJILIST.put('和', new Part(1, "和啝"));
        PART_TO_KANJILIST.put('貍', new Part(1, "貍霾"));
        PART_TO_KANJILIST.put('沓', new Part(1, "沓鞜踏"));
        PART_TO_KANJILIST.put('蒙', new Part(1, "朦矇艨蒙曚濛檬"));
        PART_TO_KANJILIST.put('沙', new Part(1, "娑沙鯊莎裟"));
        PART_TO_KANJILIST.put('沛', new Part(1, "霈沛"));
        PART_TO_KANJILIST.put('粛', new Part(1, "粛繍"));
        PART_TO_KANJILIST.put('貝', new Part(7, "簀頁頂鰂頃項順瀆頇須頊頌頍損頎頏預頑頒頓頔瀕頖頗領頙蠙頚頜頞頠頡頣簣頤頥頦頫瀬頬頭頮頯萯頰頲頳頴瀴頵尵頷頸頻頼頽頾顄顆顇遉顊顋題鑌額顎顏顑顒顓屓顔顕鑕顖顗願顙顚鑚顛摜鑜灝鱝類籟鑟衠塡顢顣瑣塤顥顦遦顧顪顫顬屭顯顰顱籲顳顴側偵聵豶遺鑽傊梖貝貞負財貢貤貧粨貨碩販貪貫責貭貮貯貰貲墳貳貴碵債貶買貸貹貺費貼貽碽傾貿賀賁賂賃賄賅賆資賈瓉賉賊賋磌賍賎賏賑磒賓僓瓔賕賖賙賚瓚賛賜壝賝賞賠員賡賢賣賤郥賦磧僨賨質賬賭擯賯賰賲賵擷賷賸價賺惻賻購賽賾賿贁贃唄贄攅贅贇贈贉贊贋贍贏贐儐夒贒夓贓贔贖甖鄖贗贛償攢礥儧儨礩愪儬鄮崱蔶儹鄼襀蕆酇饋蕒敗鵙饙蕡蕢慣楨煩赬襭襰蕷嵿薋禎憒熕禛薠憤膩嶺熲趲領閴嶺覿槇燌巎臏闐巓槓藚闝闠嗩懶藾嗿戇蘋樌鸎嘖鸚縜戝縝蘡測嘳稹樻績幀則積穎蹎湏牘幘蹞湞蹟繢癩癪癭癲噴噸繽纇纈纉續纐躓纓隕劕劗纘媜檟皟犢隤嚬檱檳溳嚶廁囂櫃竇鋇囋廎囎櫕髕勛櫝勣盨勩黰黷櫻狽蛽欑鬒圓鬚圚鬢霣嬪漬嬰匱匵嬾潁罌齎轒獖鍘鍞靧潰獱歵獺孾讀澃讃澒瞔鎖讚殞讟厠龥澦殨殯殰瞶鞼揁濆鏆韇矉篔寘實埧濱寳濵寶濺韻濽"));
        PART_TO_KANJILIST.put('貞', new Part(1, "幀偵碵遉貞禎"));
        PART_TO_KANJILIST.put('岡', new Part(8, "岡綱棡崗堈剛鋼"));
        PART_TO_KANJILIST.put('貢', new Part(1, "貢槓熕"));
        PART_TO_KANJILIST.put('咢', new Part(1, "鰐咢鄂諤鍔愕齶蕚鶚萼顎"));
        PART_TO_KANJILIST.put('粥', new Part(1, "粥鬻"));
        PART_TO_KANJILIST.put('咨', new Part(1, "咨諮"));
        PART_TO_KANJILIST.put('貫', new Part(1, "慣實貫樌"));
        PART_TO_KANJILIST.put('責', new Part(1, "簀勣債嘖磧癪責漬積績蹟"));
        PART_TO_KANJILIST.put('粲', new Part(1, "粲燦"));
        PART_TO_KANJILIST.put('貳', new Part(1, "貳膩"));
        PART_TO_KANJILIST.put('貴', new Part(1, "潰匱櫃簣貴瞶遺饋"));
        PART_TO_KANJILIST.put('咸', new Part(1, "箴鰔轗咸緘鹹喊減撼鍼憾感"));
        PART_TO_KANJILIST.put('咼', new Part(1, "萵渦蝸窩鍋咼堝禍過"));
        PART_TO_KANJILIST.put('沾', new Part(1, "霑沾"));
        PART_TO_KANJILIST.put('蒿', new Part(1, "藁嚆蒿"));
        PART_TO_KANJILIST.put('哀', new Part(1, "哀滾袞"));
        PART_TO_KANJILIST.put('品', new Part(9, "區品躁驅毆懆嶇甌癌操歐嵒嘔嫗蕚樞髞譟澡燥臨奩噪繰謳傴藻"));
        PART_TO_KANJILIST.put('賁', new Part(1, "賁墳噴憤濆"));
        PART_TO_KANJILIST.put('㓁', new Part(1, "㓁深探"));
        PART_TO_KANJILIST.put('賈', new Part(1, "賈價"));
        PART_TO_KANJILIST.put('泉', new Part(1, "湶泉線腺楾"));
        PART_TO_KANJILIST.put('泊', new Part(1, "箔泊"));
        PART_TO_KANJILIST.put('賓', new Part(1, "濱鬢賓檳嬪繽擯殯"));
        PART_TO_KANJILIST.put('法', new Part(1, "法琺"));
        PART_TO_KANJILIST.put('泙', new Part(1, "泙萍"));
        PART_TO_KANJILIST.put('賛', new Part(1, "讃攅纉鑚賛"));
        PART_TO_KANJILIST.put('瓜', new Part(6, "窊胍蓏狐泒寙觚箛罛瓜瓞瓟笟瓠瓢瓣孤瓤弧柧菰呱軱窳苽"));
        PART_TO_KANJILIST.put('賞', new Part(1, "賞償"));
        PART_TO_KANJILIST.put('員', new Part(1, "員圓隕韻損殞"));
        PART_TO_KANJILIST.put('泡', new Part(1, "泡萢"));
        PART_TO_KANJILIST.put('波', new Part(1, "菠波婆碆"));
        PART_TO_KANJILIST.put('賣', new Part(1, "讀犢賣贖竇黷牘續覿"));
        PART_TO_KANJILIST.put('賤', new Part(1, "賤濺"));
        PART_TO_KANJILIST.put('鳥', new Part(11, "鸂舃鸇鸊鸎鸐鸑鸒鸕鸖搗鸙鸚鸛鸜鸝鸞鰞樢塢瑦隖隝梟窵裊烏嫣鳥鳦鳧鳩鳫鳬鳰鳲鳳鳴島鳶鳷鳹鴂鴃鴆鴇鴈鴉鴋鴎鴑鴒鄔鴕贗鴗鴘鴛鴜嬝鴝鴞鴟鴣鄥蔦鴦鴨鴪鴫鴬鴯鴰鴲鴳鴴鴺鴻鴼鴽鴾鴿鵁鵂鵃鵄鵅鵆鵇鵈鵊歍靎靏鵐鵑鵓鵔鵙鵜鵝鵞鵟鵠鵡鵢鵣鵤鵥鵩鵪鵫鵬鵯鵰鵲鵶鵷鵺鵻鵼鵾鶃鶄鶆鶇鶉鶊嶋嶌鶍鶎鶏螐鶒鶓鶕鶖鶗鶘鶚鶡鶤鶩鶪鶫鶬鶮鶯鶱鶲鶴鶵鶸鶹鶺鶻鶼鶿鷁鷂鷃鷄鷆鷇鷉鷊鷏鷓鷔鷕鷖鷗鷙嗚鷚槝鷞鷟鷠鷥鷦鷧鷩鷫鷭鷮鷯鷰鷲鷳鷴篶鷸鷹鷺鷽鷾"));
        PART_TO_KANJILIST.put('哥', new Part(1, "彁哥歌謌"));
        PART_TO_KANJILIST.put('瓦', new Part(5, "甃甄甅瓦瓧瓩甌甍瓮甎瓰甑瓱瓲甓甕瓶瓷瓸"));
        PART_TO_KANJILIST.put('擧', new Part(1, "欅擧襷"));
        PART_TO_KANJILIST.put('質', new Part(1, "躓質"));
        PART_TO_KANJILIST.put('島', new Part(1, "島搗槝"));
        PART_TO_KANJILIST.put('糸', new Part(6, "戀縀縁縄縅縈縉縊蘊縋樏縐縑縒縕縗縛縜縝縞鸞縟瀠縠縡縢縣縦縧縨蘩縫縬縭縮縯瘰蘰縱縲縳縵縶縷縹縺縻蠻總績騾蘿縿繁繃噄繄繅繆虆繇繊繋繍繎塐繐繒葒織繕繖籘繘繙繚遜繝繞繟繡繢灣灤葤繥繦繧繩繪繫繭籮繮葯繯繰繳癴繸繹繻繼鑼繽鑾繾繿乿纁纂纃傃纆纇纈纉纊續纍纎邎纏邏纐纑纒纓蒓纔纕纖纘纚纛纜纝纞嚩銯檰蓀囉櫞曫勬糸糺系糾紀紂蔂紃約紅紆紇紈紉紊紋納欏紏紐紑欒紒紓純紕紖紗紘紙級紛紜圝紝攞紞素紡索攣紣紦紪紫愫紬紭紮累漯細紱紲紳紵儸紹紺猻紼紽紾紿絀絁終絃組絅絆絇絈絋経孌絍彎絎絏結絑絓潔絖絗絙絚絛卛絜彝絝蕝絞蕠饠轡絡絢絣絥給絧絨絪孫絮絰統絲絳絵絶絸絹絺襺絻絽孿絿玀綁羂綂綃羅綅綆膆綈綉變綋綌綍鎍綏綑經綖綗継続綛綜綝綞綟榡綢綣綦綧綪綫綬維綮辮綯綰綱網喲綳綴綵綶綷綸綹螺綺綻徽綽綾綿醿鯀係緂緃緄緅緆緇嗉緊緋緌緍鏍緎総緑緒巒緕矕緗緘緙線緜緝緞締臠緡緢緤緥鷥緦編緩緪緫緬緭緯濰緱緲練緵緶懸緹緺緻"));
        PART_TO_KANJILIST.put('系', new Part(1, "鯀係縣懸系纛孫遜緜"));
        PART_TO_KANJILIST.put('約', new Part(1, "約葯"));
        PART_TO_KANJILIST.put('贈', new Part(1, "贈囎"));
        PART_TO_KANJILIST.put('贊', new Part(1, "贊讚鑽"));
        PART_TO_KANJILIST.put('唐', new Part(1, "唐糖塘溏"));
        PART_TO_KANJILIST.put('蔑', new Part(1, "蔑韈襪"));
        PART_TO_KANJILIST.put('崔', new Part(1, "崔摧催攜"));
        PART_TO_KANJILIST.put('甘', new Part(5, "謀咁鐁碁憇墈澌騏踑媒麒箕撕愖禖甘稘甚倛簛蘛甜箝朞甞蜞斟期尠戡錤堪碪琪厮斯邯簱疳玵嘶蚶簸踸椹欺紺煁糂淇湈棊棋嵌魌篏籏某柑拑泔揕鍖蟖旗鉗勘凘剘卙湛廝磡酣煤坩黮楳其苷基祺啿"));
        PART_TO_KANJILIST.put('甚', new Part(1, "尠戡糂鍖勘椹甚堪碪湛斟"));
        PART_TO_KANJILIST.put('洛', new Part(1, "洛落"));
        PART_TO_KANJILIST.put('甜', new Part(1, "憇甜"));
        PART_TO_KANJILIST.put('生', new Part(5, "篂徃癃殅隆瑆煋旌狌鉎鮏嶐醒暒姓笙眚夝生星鏟甠甡産甤蕤腥甥甦性甧猩薩泩鼪牲睲霳貹鯹惺滻窿"));
        PART_TO_KANJILIST.put('産', new Part(1, "産薩"));
        PART_TO_KANJILIST.put('用', new Part(5, "圃尃薄傅墉踊憊膊樋逋涌匍搏悑愑榑輔餔舖舗備通縛猟斠鼡溥誦用嚩甪甫甬傭脯甯蒲鞴庸愽簿銿慂僃鯆埇勈蝋俌俑鯒糒埔捔捕博苚痛韛補葡晡佣浦筩鋪蓪懯慵桶敷蛹哺賻黼恿"));
        PART_TO_KANJILIST.put('崩', new Part(1, "繃崩"));
        PART_TO_KANJILIST.put('甫', new Part(1, "圃尃薄傅鯆膊逋匍搏榑埔輔餔捕舖舗博縛補葡溥浦鋪甫脯蒲敷哺賻黼愽簿"));
        PART_TO_KANJILIST.put('甬', new Part(1, "俑慂鯒桶誦蛹踊通樋痛甬涌"));
        PART_TO_KANJILIST.put('支', new Part(4, "技傁庋螋瞍頍鮍伎岐蚑妓鼓鼔帔鼕鼖鼗鈘鼙嘙鼚鼛搜枝鼟肢弢蔢箥瘦庪麬支芰吱攱溲皷鈹瞽皽柀跂嫂翅豉廋叏屐叓詖艘捜叟歧痩忮髲鳷"));
        PART_TO_KANJILIST.put('累', new Part(1, "瘰縲螺騾累"));
        PART_TO_KANJILIST.put('田', new Part(5, "蠆瀇瀋頔瀘堛思蠣耦栧倮瀷砷堺逼瀾塁顆遇塊顋偎顒聘遛衡遦偪聭顰顱偲恵偶傀邁傅碑袖墖増墠墦碨傳颸胃胄飅惈磈壘壙壚磚胛飜棟僤僧裨僵棵裸裹磺磻褁愊礌褏鄐礑愚餜褝儡愢儣夤夥礦愧礧礪鄱礱鄲椳餵鄺愽餽脾餾楅饆煉慉襌腗神襠襣兤煨慮腮奮慱奲腷祼兾冀禅榊膊憎福熏冑榑禑膚憚醜禝冨禩禪妯膰榴醺禺熿燀釉凍槐燔臚臝秞槞懥槫懬懭懯觶燻騁舅爈娉樌樏爐樔稗娚稞戦騨横稫騮戰舳戴稷稸騾鈾鈿樿鉀穂橊牌穌橎穐婐驑婢驢艣驥艪艫穭鉮副橿剿劃檑劓窠課抻押窼抽媿髀髁嫂竃髃櫆髆勇竉諌狎嫐櫐櫑櫓櫔櫖苗髗竜勦櫨勰諰勱勲勳勴勵謂欂欄嬋匐猓謖錙笛匣猥錨猫錬猬鬮匰嬲嬶嬸鬼鬽挿魁魂譂魃魄魅魈魋魌魍魎魏魑卑譒魔魕魖魗魘単魚博魛捜魞魡魣獣魥魦魨魪筪魫魬魭魮魯魳魴魵鍶魷獷魸獸魹獹魿鍿鮀鮃鮄讄鮅讅鮆鮇鮉鮊讋鮋鮍鮎鮏鮐鮑鮒鮓鮔鮖鮗宙鮚鮝鮞鮟鮠鮦鎦鮧鮨鮩箪鮪鮫殫鮬鮭殭鮮鮰鮱鮲厲鮴厴莵鮷鮸鮹鮻鮼鮾鮿鯀叀鯁鏁寅鯆鏆鯇鯈鯉鯊富毌鏍鯎描鯏鯐鯑菑鯒寓菓鯔鯖鯗鯘毘鯛鯝鯟鯡鯢鯣鯤鯥實鯧鯨審鯪鯫篭鯯鯰鯱鯲鯳篳鯵鯷鯸鯹鯺鯽鯿鰀鰂専鐂鰄鐄鰆搆鐇鰈專鰉鰊鰋鰌鰍氎鰏搏鰐搐鰑鰒鰓鰔鰕鰖鰘鰙鰚鰛鰜鰞鰡鰢鰣鰤鰥鰦鰧簧鰨鰩鰪萬鰭鰮鰯鰰鰱鰲鐳鰵鰶鰷鰹簹鰺鐺鰻鰽鰾簿籀鱁鱃鱄豅鱅鱆鱇鱈鱉鱊届葍鱎鱏鱐鱒籒鱓籓鱔籕鱖鱗鱘葘鱚籚鱛鑛鱜摜鱝鱞鱟鱠瑠鑢鱣層鱧鑨鱨鱩鱪鑪鱫鱮瑰鱰鱲鱵鱶摶鱷呷鱸葸鱻呻璅蒐貓璜璠璢撣岫璫岬播油沺粿擂蓄擄擋瓐糞擤糲蓴擴峺賻擻哽蔂攄贈甎攏甑蔔甥鴨鴫紬累田細由甲申紳唵甶男甸甹町画甼崽甽甾甿畀蕃畄畆畇畈畉畊畋界畍嵎畎畏畐畑敒畒畔畗留畚畛畜畝畞畟畠畡畢畤略畦畧畩番畫嵬浬畭鵯畯異畱畳畴當畷敷畸畹畺畻畼畽蕾畾畿喁疁疂喂薄疅疆疇鶇疉薊疊疐薑嶓鶓閘喟單綶鶹巃淄緇巍藕藘旙旛藟鷠淠闡緢巣緦痩藩鷭緭練嗶痹痺縄蘄蘅蘆蘇蘒蘓鸕蘚縛踝瘣瘤渭蘭縯瘰縲踴踵蘶嘼嘽蘽癀噃幅癅繅虆癉噌蹕癘繙虜繟幡湢湧癬繮蹯陳陴湽亀隅隈躉嚊纊纍嚏纑嚔躔嚕隗溜嚝纝隟暢皤溥嚧嚨溭蚰庵庽庿溿囀滀曂軃黄囅廆黆黈黋囌黌囎滝曠廣盧曨廬雷廸軸黸電曽漁鼉演蜔團輜輠霤鼬漯伷伸輺鼺鼻輻鼼鼽弾鼾蜾鼿齁靁齃佃齄齅齆齇彈蝉轉彊罍彍罏潓轓潘彙蝟蝠潢坤轤睤睥潬東齵蝿羀瞄龏龐龑澑龒龔龖龗瞚龜果龝鞞鞭鞸螺侽螾便韁翈矑柙柚俜濞蟠韠迪蟷翻翼濾俾"));
        PART_TO_KANJILIST.put('由', new Part(1, "騁専穂胄寅娉釉届冑演袖聘宙柚迪岫紬鼬蚰由舳恵廸軸油画抽"));
        PART_TO_KANJILIST.put('甲', new Part(1, "禅彈蝉嬋襌狎単閘憚胛褝闡匣戦騨鴨箪禪殫岬單戰甲鄲呷押弾"));
        PART_TO_KANJILIST.put('申', new Part(1, "鰰暢申紳坤伸痩榊呻抻捜神"));
        PART_TO_KANJILIST.put('攴', new Part(1, "邀檄贅薇鼇覈倏撒枚條悠檠倣撤冦霧脩熬微薮螯霰傲鰲攴徴攸徹改攻徼放政激繁竅故寇滌敍敏救敕敖繖敗務教蓚做驚絛篠散警牧遨嫩敬修数筱懲敲整致敵嗷敷緻"));
        PART_TO_KANJILIST.put('攵', new Part(4, "瀀氂各逄氅倏倐鰒鐓栙逡逢倣簬蠭倰耰瀲鰲鰵鰷格落鑁鱉衉硌額摓籔做摠履遨恪鱫聰葰聱桻葼邀璁肇撇貉咎璐撒傚悛條悠撤悤璦墩梭咯傯傲撽賂峉糉瓊僌蓌擎僘烙蓚蓧棨蓬棭胮峯峰擱棱胳擻峻烽擾僾贁夂夅贅唆蔆夆儆複変夊夋夌焌儌儍复愍愎夏夐夒夓夔餕愗愙崚洚洛愛贛蔜蔥脧脩優椱蔲攴攵儵椶收攷攸改攺攻儼鴼攼蔽攽放政終敃鵅故敇效敉畋敍敏敐救敒襒啓鵔敔嵕敕敖蕗敗敘教浚絛敝畞敞煞畟敟浟慠絡敢散略赦敦畧酪敫敬畯蕯数敲絳整敵慶敷酸數腹祾喀憁斁憂斂斃覆薆薇覈憋榎薐嶐喒膖憗禝憝熢閣馥冦憨鶩冬熬薮綮液綹疼憼綾嗄釅燉跋凌巌巎釐鷔巖巙闙闝闞痠処淩鷩藪路懯駱懲致緵臵嗷鷺緻駿踆戇騖稜昝蘞騣樤縧蘩縫稯稷總繁驁虁癁虂癃橄驄婈噉噋降幑處牕繖陖晙驚幣幤癥牧蹩噭噯繳晵陵晷晸婺詻鉻檄隆暋犛抜檠麥皦麦皧麨麩麪麬麭溭麮麯麰庱麳嚴皴麴麵麸麹媺麺窻骼暼暾窿竅廈滌櫌廒鋒蛒雒曔曖務櫜廠嫠拠狢竣嫩髪滫髮曮囱苳黴勶黻狻蜂錂輅鼇鼈鬉弊挌鼕茖謖漖輘霚匛鼛嬡漦霧鬫霰露霳鬷謷輹笿魃坆譈靉轍荍鍐鍑獒睖捘獘孜潞佟条潡蝥警杦鍪蝮筱鎀瞀玁澂澈變後羐羑澓掕掖鮗鞗枚莜龞客瞥厦復鎫螫厫玫微瞮螯厰瞰厳徴瞹徹鮻徼螽徽激鏅寇埈鯈俊柊鏊鏓矙翛珞篠俢鯪修菱篷韸"));
        PART_TO_KANJILIST.put('蔵', new Part(1, "臓蔵"));
        PART_TO_KANJILIST.put('男', new Part(1, "嫐嬲踴甥舅男勇湧娚虜"));
        PART_TO_KANJILIST.put('攸', new Part(1, "悠篠筱攸脩蓚絛滌條修倏"));
        PART_TO_KANJILIST.put('活', new Part(1, "闊活"));
        PART_TO_KANJILIST.put('放', new Part(1, "激邀倣檄竅贅鼇覈遨熬螯傲鰲敖嗷徼放"));
        PART_TO_KANJILIST.put('㔾', new Part(1, "㔾犯"));
        PART_TO_KANJILIST.put('畀', new Part(1, "畀齁襣齃擤齄齅齆齇嚊劓嬶鼻鼼鼽鼾濞鼿"));
        PART_TO_KANJILIST.put('故', new Part(1, "故做"));
        PART_TO_KANJILIST.put('效', new Part(1, "效傚"));
        PART_TO_KANJILIST.put('界', new Part(1, "堺界"));
        PART_TO_KANJILIST.put('敏', new Part(1, "繁敏"));
        PART_TO_KANJILIST.put('畏', new Part(1, "猥隈畏"));
        PART_TO_KANJILIST.put('畐', new Part(1, "畐幅富副福"));
        PART_TO_KANJILIST.put('嵒', new Part(1, "嵒癌"));
        PART_TO_KANJILIST.put('敕', new Part(1, "整敕嫩"));
        PART_TO_KANJILIST.put('敖', new Part(1, "傲鰲贅敖嗷鼇遨熬螯"));
        PART_TO_KANJILIST.put('留', new Part(1, "瑠籀鰡榴瘤霤留溜餾"));
        PART_TO_KANJILIST.put('畜', new Part(1, "蓄畜"));
        PART_TO_KANJILIST.put('絜', new Part(1, "潔絜"));
        PART_TO_KANJILIST.put('敝', new Part(1, "幣斃瞥鼈弊暼敝蔽"));
        PART_TO_KANJILIST.put('敞', new Part(1, "厰廠幤敞"));
        PART_TO_KANJILIST.put('敢', new Part(1, "瞰敢厳嚴橄巖巌儼"));
        PART_TO_KANJILIST.put('畢', new Part(1, "畢篳蹕"));
        PART_TO_KANJILIST.put('散', new Part(1, "霰撒散繖"));
        PART_TO_KANJILIST.put('㕣', new Part(1, "㕣船鉛沿"));
        PART_TO_KANJILIST.put('赤', new Part(7, "爀跡焃赤趄赥赦嚇赧赩繊螫赫赬赭赮弯頳奕迹郝湾"));
        PART_TO_KANJILIST.put('赦', new Part(1, "赦螫"));
        PART_TO_KANJILIST.put('敦', new Part(1, "鐓敦燉暾"));
        PART_TO_KANJILIST.put('浦', new Part(1, "蒲浦"));
        PART_TO_KANJILIST.put('蕩', new Part(1, "蕩蘯"));
        PART_TO_KANJILIST.put('番', new Part(1, "蟠幡蕃鐇審藩番瀋播鷭膰燔潘旙繙旛翻飜"));
        PART_TO_KANJILIST.put('赫', new Part(1, "嚇赫"));
        PART_TO_KANJILIST.put('敫', new Part(1, "激敫"));
        PART_TO_KANJILIST.put('畫', new Part(1, "劃畫"));
        PART_TO_KANJILIST.put('敬', new Part(1, "檠警驚敬"));
        PART_TO_KANJILIST.put('蕭', new Part(1, "蕭瀟"));
        PART_TO_KANJILIST.put('走', new Part(7, "趁寁趂超趈越趍趐鯐啑箑趑徒婕趕趙趞趟趠陡倢徢趣趦趨蜨睫趫趬趯走縱赱趲赳赴起跿赿"));
        PART_TO_KANJILIST.put('数', new Part(1, "数薮"));
        PART_TO_KANJILIST.put('異', new Part(1, "異冀戴驥翼糞"));
        PART_TO_KANJILIST.put('當', new Part(1, "襠礑當蟷鐺"));
        PART_TO_KANJILIST.put('長', new Part(8, "倀鬀鬁鬂鬃鬄鬅肆鬆萇鬈鬉鬋鬌鬍鬎鬐鬒鬖鬘鬙鬚鼚鬛鬜鬟鬠鬢鬣餦漲帳張悵脹粻韔棖套髟髠髢髣髤髥髦髧髩髪髫髬賬髭髮痮髯髱髲髳髴髵長髷髹鋹髺髻蕻髽髿"));
        PART_TO_KANJILIST.put('海', new Part(1, "塰海"));
        PART_TO_KANJILIST.put('數', new Part(1, "籔數藪"));
        PART_TO_KANJILIST.put('絹', new Part(1, "羂絹"));
        PART_TO_KANJILIST.put('門', new Part(8, "礀欄們爓攔欗鐗椚爛鬜簡爤蘭瀾癇襉問蕑繝聞籣潤轥捫鑭襴襽門閂閃钄閄閇閈閉閊開熌閌閍閎閏閑間閔斕讕閖澖澗閘躙閙躝閝閞閟閠閡関躢閣閤閥閦閧閨閩躪憪憫閫閬閭瞯閲閴覵悶閶墹閹閺閻閼閽閾誾閿闃闆闇闈闉闊韊闋闌闍闐闑闒瓓闓闔闕闖燗闘矙闙櫚闚關燜闝闞闟闠闡闢闤闥闦僩擱鷳鷴濶嫺藺嫻"));
        PART_TO_KANJILIST.put('斂', new Part(1, "斂瀲"));
        PART_TO_KANJILIST.put('善', new Part(1, "膳善繕"));
        PART_TO_KANJILIST.put('文', new Part(4, "炆文贇済斈斉蚊紊斊紋悋斌斎顏斐偐斑虔閔緕萕刘吝忞玟芠彣剤憫雯汶閺旻馼対"));
        PART_TO_KANJILIST.put('斉', new Part(8, "剤緕済斉斎"));
        PART_TO_KANJILIST.put('疋', new Part(5, "琁隄樅褆碇璇疋醍礎疎嚏疏丐疐疑醑従嚔禔儗椗鶗礙徙匙定從媞掟尟錠騠倢堤縦漩麪蔬鞮是稰縱颴踶嶷綻樾薿淀鏇糈鍉旋蛋題提湑寔韙諚楚靛湜凝懝諝埞諟癡聢蹤胥懥偦翨慫擬聳捷鯷緹壻婿"));
        PART_TO_KANJILIST.put('斌', new Part(1, "贇斌"));
        PART_TO_KANJILIST.put('涌', new Part(1, "慂涌"));
        PART_TO_KANJILIST.put('閏', new Part(1, "潤閏"));
        PART_TO_KANJILIST.put('疏', new Part(1, "蔬疏"));
        PART_TO_KANJILIST.put('閑', new Part(1, "閑嫻"));
        PART_TO_KANJILIST.put('疑', new Part(1, "疑癡嶷礙擬凝"));
        PART_TO_KANJILIST.put('疒', new Part(5, "瘀瘁瘂瘃瘄瘇瘈瘉瘊瘋瘌瘍瘏瘒贓瘓瘕瘖瘙瘛瘜瘝瘞瘟瘠瘡瘢瘣瘤瘥瘦瘧瘩瘭瘰愱瘲瘳瘴瘵瘸瘹瘺瘻瘼癀癁療癃癄癅癆癇癈癉癊癋癌癒癕癖癘癙癜癟癡癢癤癥癧癨癩癪癬癭癮癯癰癱癲癴疒疓疔疕疙疚疜疝疢疣疤疥疫疱疲疳疴疵疸疹疺蒺疼疽疾疿痀痁痂痃痄病痆症嫉痊痌痍痎痏藏痒痔痕痗痘痙痛痜痞痟臟痠痡痢痣痤臧痧痩痬痮痯痰痱痲痳痴痹痺痼痾痿"));
        PART_TO_KANJILIST.put('間', new Part(1, "簡間澗燗癇墹嫺繝"));
        PART_TO_KANJILIST.put('閔', new Part(1, "閔憫"));
        PART_TO_KANJILIST.put('斗', new Part(4, "魁鈄櫆蝌紏科枓抖斗斘料嵙酙斛斜斝斟斠斡斣鬦萪蚪槲図槹戽"));
        PART_TO_KANJILIST.put('薛', new Part(1, "蘖薛"));
        PART_TO_KANJILIST.put('斛', new Part(1, "槲斛"));
        PART_TO_KANJILIST.put('薜', new Part(1, "蘗薜"));
        PART_TO_KANJILIST.put('喜', new Part(1, "橲僖禧嬉憙熹鱚喜"));
        PART_TO_KANJILIST.put('閣', new Part(1, "擱閣"));
        PART_TO_KANJILIST.put('斤', new Part(4, "鐁鬂栃蘄踅逅蜇脈茊后娎頎伒昕丘簛逝匠訢欣蜥儨礩蔪听鬭訴嘶漸砺圻唽派所遁祈蹔鑕虗齗慙浙慚浜啠晢詬齭楯晰偱晳靳乴兵坵塹噺坼歽掀沂嶃嶄悊澌析誓躓撕劕折炘皙蚚垢斤劤斥斦斧循薪暫斫斬断厮斮蚯斯新邱励斲岳斳岴斴澵斷蚸梹芹垽旂淅跅拆駈壍蛎近蟖凘磛柝泝廝惞槧鏨質鋲哲忻俽盾狾釿"));
        PART_TO_KANJILIST.put('斥', new Part(1, "訴斥拆柝泝"));
        PART_TO_KANJILIST.put('喬', new Part(1, "僑驕橋喬嬌蕎轎矯"));
        PART_TO_KANJILIST.put('斬', new Part(1, "嶄槧漸鏨塹慙慚暫斬"));
        PART_TO_KANJILIST.put('閭', new Part(1, "櫚閭"));
        PART_TO_KANJILIST.put('維', new Part(1, "羅鑼維蘿邏"));
        PART_TO_KANJILIST.put('單', new Part(1, "戰闡鄲彈憚禪嬋殫襌單"));
        PART_TO_KANJILIST.put('斯', new Part(1, "撕嘶廝厮斯"));
        PART_TO_KANJILIST.put('新', new Part(1, "新噺薪"));
        PART_TO_KANJILIST.put('足', new Part(7, "踁踄踅踆踈踉踊踋踏踐踑攓踔踖踝踞踟踠踡踢踣踦踧踪簬踰踱露踳踴踵踶踷踸踹踽蹀蹁蹂虂蹄蹇蹈捉蹉蹊蹋蹌蹍蹎蹏蹐蹔蹕蕗蹙蹛蹜蹝浞潞蹞蹟蹠蹡蹢蹣蹤蹩齪蹬蹭蹯蹰蹱蹲蹴蹶蹹蹺蹻蹼躁躂躃躄躅躇躉躊躋躍璐躐躑躒躓躔躕躙躚躛躝躞躡躢躧躩躪足趵趷趹趺趻趼趾跀跂促跅跆跇跈跊跋跌跎跏跑跔跕跖跗跙跚跛鋜距跟跡跣跤跥跧跨跪跫跬路跰跱跲跳跴践鷺跼跽跿珿"));
        PART_TO_KANJILIST.put('方', new Part(4, "圀瘀鈁堃贅眆簇鼇昉鴋儌搒椖謗蔜餝蔟紡倣漩訪舫簱防鰲謷游椸唹錺放房驁坊遊祊籏獒牓敖楞慠蝣遨籩敫噭嵭聱繳魴敷彷汸葹邀膀膂螃檄璇覈邊鎊枋傍閍膐榜蒡邡皦妨肪厫熬徬螯傲芳方徼於閼施撽斿激旁滂旂旃鏃旄竅磅旅旆鏇旈髈旉鏊旋旌旎族旐旒廒揓旔鷔旖旗旘旙旛棜旟鷟髣淤懯臱雱鯲嗷菸嗾仿"));
        PART_TO_KANJILIST.put('嶺', new Part(1, "嶺嶺"));
        PART_TO_KANJILIST.put('於', new Part(1, "鯲淤唹於閼"));
        PART_TO_KANJILIST.put('施', new Part(1, "葹施"));
        PART_TO_KANJILIST.put('疾', new Part(1, "嫉疾"));
        PART_TO_KANJILIST.put('斿', new Part(1, "遊斿"));
        PART_TO_KANJILIST.put('旁', new Part(1, "膀旁蒡滂磅謗榜傍"));
        PART_TO_KANJILIST.put('旅', new Part(1, "膂旅"));
        PART_TO_KANJILIST.put('嗇', new Part(1, "穡艢檣薔牆嗇墻"));
        PART_TO_KANJILIST.put('旉', new Part(1, "敷旉"));
        PART_TO_KANJILIST.put('闌', new Part(1, "欄襴爛闌蘭瀾"));
        PART_TO_KANJILIST.put('藏', new Part(1, "藏臟"));
        PART_TO_KANJILIST.put('族', new Part(1, "鏃簇嗾族蔟"));
        PART_TO_KANJILIST.put('旗', new Part(1, "旗籏"));
        PART_TO_KANJILIST.put('巛', new Part(3, "踁璅涇爉邋躐徑經樔輕逕邕莖匘錙誙脛輜舝蠟隟鬣碯頸輺窼甾勁鏁剄淄繅齆緇灉菑鯔臘葘陘瑙痙巛硜雝鑞衟巠巡巤腦勦癰廱惱桱鱲獵拶牼災湽鍿剿"));
        PART_TO_KANJILIST.put('川', new Part(3, "流侃紃棄卅順旈琉爉謊邋慌疏鎏躐荒旒訓毓丗鬛川滞州蠟巤釧硫蔬酬㐬帯醯洲駲鱲梳圳馴詶宺甽"));
        PART_TO_KANJILIST.put('藝', new Part(1, "囈藝"));
        PART_TO_KANJILIST.put('州', new Part(1, "洲駲酬州"));
        PART_TO_KANJILIST.put('无', new Part(4, "曁炁概嘅噆蔇灊妋墍廐漑鐕譖潛悞无旡摡既慨厩簪僭鬵蠶"));
        PART_TO_KANJILIST.put('旡', new Part(1, "曁炁概嘅噆蔇灊妋墍廐漑鐕譖潛悞无旡摡既慨厩簪僭鬵蠶"));
        PART_TO_KANJILIST.put('既', new Part(1, "漑既概慨厩"));
        PART_TO_KANJILIST.put('巣', new Part(1, "巣樔勦剿"));
        PART_TO_KANJILIST.put('工', new Part(3, "踁紅項崆戇尋椌訌鬌式弑縒搓儔輕逕鰖脛贛匞欞舡倥瘥褨鄩鬫昮茳缸頸攻鴻栻谾蕁剄鑄牆筇豇靈蹉啌籌恐佐噐鵐筑葒腔楕艖魗陘鱘扛硜汞江杠彠穡杢橢艢試穩噬幬畭卭譭筮癮潯嵯桱瑳嵳譸虹空鹺牼鵼鉽檃疇涇鎈隉躊隋嚋銎鞏撏徑澒經讔薔熕莖侙誙鞚肛暛邛箜憜傞功媠溠覡貢誣檣控隨澨墮檮隯隱禱隳膸嶹璹躻醻墻碽悾瞾勁釅軇嗇築毉磋曌槎揑槓髓壔燖惘痙嗟巠擣濤工左巧瓨蛩巩巫跫拭釭差惰蟳矼壽櫽軾燾翿"));
        PART_TO_KANJILIST.put('日', new Part(4, "蠁者耆蠆蠍倎瀑頔怕瀚怛倝蠞借倡蠢栢蠣堤耤瀦堦耦栧逪倬堭堰倲堲場逴堵蠶砷逷堹蠺瀾逿恂偃顆偆遇偈恉題塌桌灎遏遑恒塒偒顒桓灔顕偕偖聘願衝灝偟遢偣灥顥遧顫硬遭顯塲衵恵偶職遼塼桿塿邀墁邁境炅邅傆墇邈碍悍墍梍碏袒墓傖袖梗増碘袙墠碣碧碭傷墸墹傹颺悼傽胄胆壇郇惈棉働棍棏郒壓棔惕磖僚磚胛惛惜壜烜棟惠僣飣僤飥飦僧飪郫僭棰磳飳飶惶惷惸裸棹裹惺裼都惽郾褁脂餂儂愃儃億褆複餇儈儌鄍复愎意褏褐椑餑愒愓焔愔餕餖餗椙褚鄚餚愚餛焜餜褝褞餟餢椣儣鄣夥餦餧優礪餫礫焯愰椱餱儲餲鄲餳餴餵鄶褶鄷焸餹餺椻餻餼椿愿饀饁饂楂奄饅腆饆慇饇饈煉楊煊腊煋腌煌襌慍饍饎児慕腗饘奙饙煚奛襛饛煜饜兜神慞饞慟饟饠奢慢襢奣兤腥煦楦照楩腫襫煬楬奭楮煮襮慯慱奲腴襴慴楷腸典腹腺楻祼楽楾楿熀憂醃膃覃禅覆覈榊膊醋醍憎熏冐冑醒冒膓憓禓禔醕冕香憚馛膜馝膞妟馟榠熠馥冥榥覥榦馦馨覩冪禪禫憬憭禮妯榯禯醰妲榲醲熳醴憶憹馹醺禺榻膻熾膾膿懀燀釂臆臈釉重凍燎量凕懕燗燙槝秞槢槫凮姮凰觴觶槹燻駻槽槾騁樂樃爆戇稈稉娉爍舓樔爗爛戛爝稞戞戟樟戠騠模鈤戦戧娨騨戩横稭種戰稲舳樴騵樶舶訷舺娼稽鈾樿驀鉀穂穆艎婐穐橑鉑剔驔穗艘婚艚穝穠詢詣婥剦橧艪穭鉮艶橸剸剿檀檄劊媋檍媓檜媞窠檪誯劰檰銲課抻誻押媼窼抽骾檿髃竃嫄担竅勆髆諌拍狎嫏櫓體櫔動勗嫚諛狛嫜髜櫟募竟狟諟章諠竡嫥狥勦櫧諧竨竭竱勲勳諳勵髷諸竸嫺嫽拽謁欂猂欄鬄指嬋錍鬐猒匓錔錕猖嬗鬘匙鬙笛謜嬝鬠匣錦謨猩錩猪笪錪錫匫錬挭鬮錯匰茰猲鬺匽猽謾挿荀筁譂譄魄歆歇鍇草鍉捍筍獍捏獏獐鍑卓譖識魘単譚鍚譛捜譜獠鍠獦獪筪捭魯捵鍺鍾箄莄掉殉鮊鮋箔殗宙厚鎛厝箟殟原厡宣殤鎤掦鮨掩措箪莫殫厭莭箯箰掲宴厴厵箸箺殾宿揀叀珀鏁鯁篁篂鏄珅寅毌鏌菎揎提寓菓寔菖揚鏝寞鯟寠揠鏡鯣珣鯤鏥鯧揩寮篳菴鯷揸鏸鯹篺鯺篼鯽専鰆萆氈鰉鰋萌簎鐐鰑鰒鐔鐕搗搘鰚鰛尞簟尟鐟簠簡搢鰣搨琨鰨琩簪萬鰭鐯鰰尰萱簳萸鰻鰽鐿瑁瑃瑄鱄鱆瑆届豊籍鱏豑瑒鱓著鑙葛鱛鱜瑝摟屠鑠鱠摡屢董鱣層履鱧汨屨瑨鱨葩鱪豬鱰摶呷摸摺呻貆貊璋貋貌沓蒔粕貘璙咟璟撣撦粨撩蒪岫岬撮璮璯粳岶油咺粺撽粿蓂擅蓈泉泊峋蓍賜糟糢糧糩擪賭賯賰糲蓴島瓸峺擻擽蓽哽擾蓿蔇贈贉甎甑崑蔓唕贛甠崦甦洦鴨洩紬蔯唱由甲鴲申紳唵洵唶洹甹画畃啅蕈絈嵎絏蕏嵑敒絙蕞畠絢嵢啤蕩鵪敫鵫赭鵲敷畼蕽鵾薀嶁嶂涅綆鶇薈喈薌鶍薏喑喒嶒間趕鶕鶗閘嶛喝趞趠斡鶡喤喧薫薬鶬薭單斮薯綶閶涷疸閹閺綽涽閽綿鷃緄嗆緆闇藉旉闌闍緍淏藐巐緒闒藕淖旘線嗜緜淟藠闡緡嗢巣日藥旦闦旧旨痩早旬痬旭鷯旰旱旲練旴旵緶藷混淹藹旹緹旺旻旾鷾旿昀昂昃昄瘄蘄縄嘅昆渇昇鸇嘈昈縉昉昊蘊昌瘍嘍昍明踏昏瘏昑昒蘒易昔踔帕昕昖瘖踖嘗渚帛昜昝踝昞星瘟映昡昢踢昣昤春昦昧昨縨温昩昪昫嘬昬蘭昭縮昮蘯是縯蘰昰昱嘲渲昳踳阳瘴昴踵昵縵昶踶昷踷嘹昹昼瘼帽嘽帾昿晀晁癁療時晃晄晅繅噆晆癇晉癉晊晋蹋幌噌晌陌幎晎晏晑晒繒織幔幕晗癘晘繚幛晛幜晜湜蹜繝晝湝晞幟晟繟湟晠晡晢湣晤癤陥晦晧晨晩虩繪晪噫幫晫晬噭蹭普湯景晰噲陳晳繳晴陴晵湶晶晷晸幹乹晹智晻湻晼陼陽白乾百晾湿晿皀暀亀皁暁隁皃暃暄的隄隅皅皆躇皇暇皈暈嚈暉皋暋皌暌隍皍暍溍階皎暎源皐暐暑躒暒皓皕嚕皖暖暗亘暘麘皙隙暙皚暚皛暛障皜暜暝皝嚝麞溟皟暟隟皠暠暢皢躢皣暤皤皥皦暦皧皨皪暫暭皭暮亯麯蚰亰隰暱溲暲庳暴庵暵亶暸暹暻溻暼皽暾暿曀曁曂曃軃曄軄囅軆曇曈滉曉曌囎曎廏曏廐廔曔曖曙曚曛曜廜曝廟盟曟曠黤廥曦曨曩盪曫曬曮黯曲曳更曵黶曷廸軸書曹曺曻電曼曽曾替最鼂會朅朇漊眒演漕圕鼙朝漠蜡輥漫鼬伯蜱漳蜴眴鼴輵伷伸輹鼹弼弾蜾蝀齃靄齄睅但彈蝉蝍罎蝎轑潓轕蝗睗蝘彙潛潜轢坤坦潦睧罨靨罩蝪潬潭潮蝮彰佰齰影東署杲睲杳潴齵睹靼潽罾蝿羃徇螈澋瞑澓澔瞕鞕澗得侚瞚鞜龜果龝螟垣澣澧螧徨鞨復螬鞭瞭澮鞮羯農羶澶鞸徼螾便激濃韅蟆蟇翈柏柑習韓濕柙蟙韙柚韞蟟矠韠埤翥柦翨迪蟪翫迫蟫翰矰音濳韴韵韶韷韸埸俺韺蟺韻濼響"));
        PART_TO_KANJILIST.put('左', new Part(1, "佐惰橢髓楕左膸隨隋墮"));
        PART_TO_KANJILIST.put('旦', new Part(1, "檀愃暄氈瘍碍袒膓得亘暘怛昜暢垣宣殤喧蘯萱妲場亶羶傷疸昼曁擅担但胆壇楊量恒桓揚晝諠旦坦糧蕩盪蝪顫煬慯湯塲觴腸靼陽"));
        PART_TO_KANJILIST.put('旧', new Part(1, "児稲焔陥旧"));
        PART_TO_KANJILIST.put('巨', new Part(5, "渠拒苣鉅巨矩炬秬距"));
        PART_TO_KANJILIST.put('旨', new Part(1, "脂詣嘗指旨鮨稽"));
        PART_TO_KANJILIST.put('早', new Part(1, "嶂覃啅鱆蕈掉草璋卓韓鐔譚瀚障朝簟廟戟樟章斡澣早罩倬潭潮彰翰嘲瘴棹幹悼綽乾"));
        PART_TO_KANJILIST.put('巫', new Part(1, "穡覡艢檣誣牆嗇靈巫噬筮鵐薔墻"));
        PART_TO_KANJILIST.put('淫', new Part(1, "霪淫"));
        PART_TO_KANJILIST.put('旬', new Part(1, "荀恂絢詢洵徇殉旬筍"));
        PART_TO_KANJILIST.put('差', new Part(1, "縒嵳搓瑳蹉磋差槎嗟嵯"));
        PART_TO_KANJILIST.put('淮', new Part(1, "淮匯"));
        PART_TO_KANJILIST.put('路', new Part(1, "露蕗鷺路"));
        PART_TO_KANJILIST.put('己', new Part(1, "紀鈀异脃簄包笆唈錈鬈鐉蘎匏圏夒夔記耙褜弝踠踡萢笣倦紦吧刨簨爬栬圮港圯砲琶蜷爸改挹夿祀噀虁齆扈灉鉋饌配陒灔譔煕汜蕝豝杞饞靤浥葩赩癰鑱捲色艴靶艶絶起遷艷杷選屺妃鞄皅麅咆熈垉把岊誋袍媐鮑邑芑悒梔邕庖玘熙躚綣疤肥蚫銫麭芭炮颮皰撰炰抱疱骲枹韆僊俋忋忌郌僎巎跑郒惓囘巙裛蓜淝雝胞苞凞瓟泡蟤滬己髱廱已巳巴巵郶巷裷巸雹巹巻飽巽跽"));
        PART_TO_KANJILIST.put('旱', new Part(1, "旱稈駻悍捍桿"));
        PART_TO_KANJILIST.put('已', new Part(3, "紀鈀异脃簄包笆唈錈鬈鐉蘎匏圏夒夔記耙褜弝踠踡萢笣倦紦吧刨簨爬栬圮港圯砲琶蜷爸改挹夿祀噀虁齆扈灉鉋饌配陒灔譔煕汜蕝豝杞饞靤浥赩葩癰鑱捲色艴靶艶絶起遷艷杷選屺妃鞄皅麅咆熈垉岊把誋袍媐鮑芑邑悒梔邕庖玘熙躚綣疤肥蚫銫麭芭炮颮皰撰炰抱疱骲枹韆僊俋忋忌郌僎巎跑郒惓囘巙裛蓜淝雝胞苞凞瓟泡蟤滬髱己廱已巳巴巵郶巷裷巸雹巹巻飽巽跽"));
        PART_TO_KANJILIST.put('巳', new Part(1, "祀靤咆熈垉僊蚫鉋饌袍麭炮匏皰疱髱鮑巳煕囘枹熙雹苞"));
        PART_TO_KANJILIST.put('巴', new Part(4, "鈀脃簄皅笆唈把岊蘎邑悒梔邕耙疤肥紦吧銫爬栬芭琶爸挹夿齆扈灉俋郌郒灔裛淝蕝豝雝饞浥葩赩滬癰廱鑱色巴艴巵艶絶郶靶艷杷"));
        PART_TO_KANJILIST.put('巵', new Part(1, "梔巵"));
        PART_TO_KANJILIST.put('巷', new Part(1, "巷港"));
        PART_TO_KANJILIST.put('藺', new Part(1, "藺躪"));
        PART_TO_KANJILIST.put('巻', new Part(1, "捲巻圏"));
        PART_TO_KANJILIST.put('巽', new Part(1, "撰選饌巽"));
        PART_TO_KANJILIST.put('巾', new Part(3, "稀帀市布帆帇吊鰊帋希帍帑帒逓帔帕怖帖帘帙帚帛爛帝帟帠鰤帥渧帨師席蘭帮帯帰鈰帲帳帵制帶刷帷常刺帽帾瀾幀幃蹄幄幅幇幉鱉幋幌幎幐幑幔幕乕幖幗幘偙幛蹛幜艜晞衞幞幟幡幢幣幤婦幨豨蹩幪幫幬幭幮幰遰蒂芇蒒悕沛躛銟溮檰碲肺暼芾僀蓆棉壐棗郗棘滞嫦諦諫滯瓻製飾伂欄霈鼈弊唏圑蔕猘匝錦鬧欷崹蔽蕀獅佈睎策鍗敝楝佩杮慲魳楴襴歸慸絺啻啼掃羃斃螄憋箍讏箒薓禘閙龞殢掣瞥冪鶫螮綿揀濅旆姉迊巋闌蟐鯑淛緜締揥凧篩鷩柬珮韴菷埽巾柿"));
        PART_TO_KANJILIST.put('一', new Part(1, "耀考耊耋怍怎耎怏耒耓怔耔耕怖耖耗耘耙怚怛怜耜耝耞耟耠耡耤怦耦耨耬耮耰怵耵耷耹耺耼耾恆聆恍恒恗聘恙恝聟恠聠恡聤恩恫恭恰聳恵聵職聹肁肄悆肇肈悌悍悑悕悗悘悚肝悞悟悥悦肧肩悪悰肱悳肴悷肹悻悽情胆惎惑惓胖惘胙胚惜胝惠惡惥惧惨胭胮胯惰胰胱胴惵惷惹惼胼惽愃愈愉愊脊愌愍愓愕愖愗脘脛脜愜愞感愫愬愰脰愱脱愴脵愷脼腆慈慊腊腌慎慓慕慝慟腟腠慧腧腩腫慫慬腭慮慯慰慱慲腷腸慸慻慼膀膁憂憃膆憇膈憋憍膎憎膐憒憓膓膕膖憖憘膘憙憚膚憜膜膞憠膣憨膨膩憭憮膮膳膴膸憸膻憾膿臀懀懁懂懃臉懊懋臋懌臍臏懕臙臚臛懜懝臝懞懟臡懢懣懧懩臭懯臰臱至致懴臵臶懶懷臸懸臹臺懺臻臿懿戁舂戃與興舉戌戍舒舔或舘戚戛舝舞戞戠舡戡舢戣戦舨戩戫舮戯戰舲戲戴舴戸舸戹舺戻戽房所扁扃艃艄艅扆艆扇扈扉艋才艎艏艑打扔艖托艚扚艜扜扞艠艢艣扤艤艦艧艫艭艱扱扶艶艷承艿抃芉抉芊芋抎抏抒抔芖芙抜芣芦抦芧抨芨芩芫抵抶抷芸抹抺芺芼芾抿芿担苆拌拑苒拒苓拔拖苚苛拜拝苣苤若苧苪苯拯拱英拲拳苴拵拷苷苹拼拾拿挃挄茅茉挊茊挓挟茟茢茣挨挪挫茬挭茮茯茱茵茼挽挾捁捂荂捄荅荊捍荎荐荑荓捧捩捭捲荳捶荷捺捻荼捿掄莄莇莍掎莓莖掚莚莞莟莠莢掣掩措莫莬掭掮莱掵莽掽莾揀菁揃揄揅揆揉揎菏菐揓揕揚換揜菟握揥菫揬華揲揳援菴揵菶揸揹菹揺菽萁搆萋萍搒萓搓搠搢搤搭萱搴搶萹携萼搽搾搿葅葇葉葊摋葍摏葏摑摒摓葖葘摘葚摜葜摟葢董摣葥葬摭摯葳摴葴葵葶摶摸葹摽蒂蒄撅撇蒒撓撕撘撙撚蒜撝撟蒡撥撦蒨蒩撩蒪撫撬撮撰撲蒴蒸蒹蒺撻撼蒽撿蓁擄擅蓆擇擌擐蓑蓙據蓜擠擡擣擥擦擧蓧擪蓬擬擰蓱擲蓴擶擻蓻蓽擾蓿擿攀攁攄攅攉攏蔐蔑攓蔔攔蔕蔗攘蔘攙蔚蔟蔡攤攦蔧攩蔫蔭攮蔲蔳蔶攼蔽政蔿蕀敃敍蕎蕐救蕓敔蕕敕蕘敘蕚敝敟蕢敧蕨蕩蕪蕯整敵敷蕷數蕻敽蕿斁薁斂斃斅薇薉斉斌斎薑斒薔斕薘薙斜斟薟斠薤薧薨薩薪薫断斮斯新斲斳薴薷薸薹薺施薽薾斿薿旁旂旃旆藇藉旉藊藍旎藎族旐旔藕旖旗藘旘藝旟藤旦藪旰旱旲旴藼藾旿藿昀蘀蘅蘆昈昊蘍蘎蘐昔蘘蘙蘚蘛昜昞蘞映昤春昦蘧昧昨昩蘩昬蘭蘯昰昳蘸昼虀虁虂晃晄晅晉晊晋虍虎晎虐虒虓虔處虖虗晗虘晘虙虚虜晜晝虝虞晞號虠虡晡虢虣晤虤虧晩虩晪普虱晴晵虶虷晸虺智晻暁暄蚈暋蚌暌暎蚕暖蚖暘暙暛暟暢暤蚦蚨蚩暭暮蚯蚱蚳暴蚵暵蚶暸暼暿曀曁曂蛃曄蛆曇曉蛉曎蛎蛕蛚曛蛛曝蛤曦曨曩蛬曬蛭更曹蛺蛻曽替最蜂朇蜈有蜍朎蜏蜑蜒朔朕蜘朞蜞期朠蜡蜣未末本朮蜮蜯朱朶蜷蜻朽蜽朾朿蝅杅杆杇蝉蝋杌材蝓杔杕杖蝙束蝠蝡蝤杤蝥来蝨蝪杬杮杯蝯東杵蝶蝻杼极螃螄枅螆螇螌融枎枏枑螓枖螘螠螣枦螬螮枰螱枵螵螻枻枼枽螾柂柃柄蟄蟆蟇柈蟈蟊蟎某柑柒蟒柔蟖蟚蟜柞蟟柡蟢柢柤蟤柦蟪柬蟯柯柰蟱柵蟺蟻査柾栃蠉蠊蠎蠐蠒蠓栔蠙栙蠚蠜栞栟蠢栢栨株栫蠭蠮栯蠰栱蠲栲栴蠹蠺栿衃衄桄衅桊衋桍桎衎桐桓術衕衖桗衘衙桛衜衝衟桟衠衡桧表桮衰桱衱桺桻衻桾衾衿桿梂袂梄梆梈梍袒梔袖梗袘梚袛梛袜梜袟袠梡袢梣袤梧梪梯袱袴袷梹梺梼袾裀裂裃棄棅棆棈棊棋裋裌棏棒棕棗棘裘棙裙裞棟棡棨棫棬棯裯棰裱裲棲裷製褄椅褊植褎椏褏椒褕椖褙検褝褠椡椣椥椦褨椪褫褰椱椵褸椸椹褺褾椿襀楂襄楅襅襆楉楊襌襒楔楕襖襗楗楙襚楝襟楠楡楢襢襤楥楦楩襪楪襫業襮襯楰楱楲襳楳襴楴極襷楹楺襻楼襼襽榁覇覉榊覊榎規覔榖覗榘榛榜榡覡榥覥覦覧榨親覬覯覰榱覲観覴覷榺覼榿槅槇槊構槎槗様槙觝槥觥觧觩槫槭觭槱觱觳觴槹槻槽槿樀樁訂樅樊訏訐樓訖託樗標樝訞樠模訣樣樤権横訯樰許樲樴訶樶樸樹樺樻証樽樾橅詅橆詆詇橈橊橋詍橐詐橑橒評橘橙橛詛詝詞機橡橢詥橧橪詫詬橱橲橳詳詵詷橿檀誄誅誇誉檋檑誒誕誘誙語檢檣誣檥檫説檮檯檱誷檸檻誼檽檿櫃諆諈櫈櫉請諌櫌諔諕櫕論櫖諗櫜諜諞諠諡櫤諤諦櫨諫櫬諭櫲諴諵諶諺櫼諼諾謀欃謄欄欅謆謇欉謌欐謑謔欖謗欗謙謚講欛欞謡欤謨謫欫謭欵欷欸欹謹欺謼款譁譃譄譆歉證歌歎譎譏歒歓譓歔譔歖識歙歛譛譜歝譞歟歠正譣武歪譯議譱譲歳譴歵譸歹死譼譽歽歾譾歿殀讁殂讃殃殄殅殆殉殊残讌殍讏讒讓殕讕殖讖殗殘殛讜讞殞殟殠殢殣殤殨殩殪殫殬殭殮殯殰殱殲殹殻殽殿毉毌毒毚毡毣毦毧毬毮毱毷毹毿氂氄氅氈氉氍氎氐民氓氤氧氬氶谸谹谽谿汀求豆豇豈豉豊豋豌豎汏豏豐豑豓汔豔豗汗豙汙汚汛象豢豣豦汧豨豫汫豭汯汰汲汴豺決汻豻豾沃沅貆貊貋沍貐沔沗貘財貪沫沮貮貯貰沲河貳貴賄泄泆賆泍賎泔賕賖泙賙賚賛泜賦賨泮泯賰泰泱賵賸賺購賽賾贄贇洇贈贉洊洋洌洑洙洞洟洦洧洪洯洸洹洽洿浄浅浗浜浣浤浯浹浼涂涇涎涔趕涕涘涙涛趞趫涬趬涵趷趺趼淆淇跇跊跋跌淎淏淑淒跙淙跚距淟淠淢跥跨淪淰跰跲淵践淹添一渀丁踁丂七丄丅清万踈丈済三上下丌不与丐丑踑丒渓且渕丕世踖丗丘渙丙減渝丞渞踟渟渠踡両渡渣丣两渥踦並渧踪渫丫渮丮港踰丰踱渲踳踵丵渶踸丹渼丼蹀蹁乁蹂湃乃蹄湅蹇乇湈蹉湊蹊乍蹍乎湎蹏蹐湔蹕乕乗蹙湛蹛蹜乞蹠湢蹢蹣蹤蹩蹬蹭湯蹰蹲湲湳乴蹶蹹蹺蹻蹼湽乾満溂躂了予争躊亊事躋二溌亍溍于溏躑云互溓五躕井亖亗亘溙亙躚些躛亜溝躝亝亞亟溠溢躧亨溪亭溮溯亯躰溱躱亳亶亹躾溿仁滁滂仃滄滅軆滇軇滉今滋仍滍滏軏軑軒滕仗仛滞仠仡転令仨軨滬滭滮軮滯軰仱滲滴仵滸滹軻滻軼滽仾滿仿漂輂伂伃漈輈輊伊漊伋輌漌伍漍輏伏輐輓漕伕輕伖会輛漛輝伝輞漠漢輦漦輧漩輪漪漭輭輮漱輳伳伴輶伶輸伹伺輺伻輻輾漾伾佂轃潅但佇佈轉轎低潏佐佑轑佒体潓潔何轗佘轘余佚潜作潝轝佞轞你佣轤潦佮佯佰潰佱併潽佽使侁澂澄侄澆來侉侊例澌侌澍澎侏澐侑澑侓澓澔侖侗侘澚供侟侠辣澣澤澦澧澪澮侯澰澱農澳澵侶澶辷辸侻侼达便迀俁濂迂濃迄俅俆俉迊俌俎运俒濔迕濘俛俜俟濟俠俣迣濤俤迤濨濫迭濮迮述俵迵俶迶俸迸濹俺濽濾迾俿瀀送倁瀁倄逄逅倆逆倎逎倏透瀑逑倒逓途逕倖瀗逗逘瀘候倚倛倜瀞速借倢逢瀣値倦倧倩逩逪倫逬瀬瀰倲瀲倳倵倶瀷逸瀹倹逹逼瀼逾瀾逿偀偁偂遂灃遃偅遅偆灊遊灋偌遌遍灎偏偐灑偒遒偓道達灔遖遘灘偙停灞遞遡遣遥偧適偪遭遮灯偰遰偱遲遵遶遷偸選遹遺遼遽傃邃還傄邅邇炉傍邐傒傓傔傖炗邗邘傞邢那邦炬邯邱炳邳傳邴炴邶傷邸炸傺邾僀郁僂郃郄僄郅僅郈烈僉僊烊働僎像僐僑僓僔烔烕僕僖郗烘僚僜烜郜烝烟郟郡僣烤僥僧僱郵僵烹烽儀鄀鄂儂儃儇儈鄈儉焉焏儐儔儕儖儗鄘儙鄚儛儜儞鄞鄠無鄢儣儧鄧優儬儭鄭儯鄯儱鄲儳儴鄴儵鄶然儷鄷儺鄼焼兀煁兂元酃煅煆酈光煉酉酊煊酋酌兌配免酎煎酏兏煐酒酓兓酔兔兕酖煖酗煗酘兘酙酚煚酛煠酡酢酣酤兤煤酥煥酧酩兩酪兪酬煬酭共酲酳关酴酵兵其酷具酸典酹煹酺酻兼煽兾冀熀醁醂醃冃冄醅醆円醇醉冉醊冊醋册醍再醎醐冐醑冑醒醓冓醔醕冕醗醘写熚熛醜冝醞冠醡冡醢熢醤冤醦冦醨冨熨醪冪醫醬醭冭醮醯熯醰醱冱醲醳决醴醵醶冷醸冸熹醺冺熺醻醼醽冽冾醿冿釀釁燁釂凂釃燃凄釅燈釋重凍燎野量减釐凑燒凒燕釘凘燙凛釜凝釟燠釡燧釪釬凮釮釱凱凲凳燵凸燸函燾凾刁爆鈇切爈刊鈐爐刑鈒刓刔鈕爗列爚爛鈜爝鈞爟鈣判爤鈦爨鈨鈩刪爯到爰刱鈳刳鈴爴鈵制券刺刼鈼爼爽爾牁牂牃鉃剃剄鉄剅牅鉅牆鉇剌前牏鉏剏牒牓牖鉗剘剚剛鉛剞鉠鉡鉢剣剤鉦剦剪副剰鉰剱牱剳牴剴鉶剷鉷剸牼鉼剽牾鉿犀劀劂劃犄銅劇銉劊銊劌劍銍犍劑劒銒劓劔銕劖銖銗劘銙犛犠劥銧犧助犬銭犮励犲銲犴劵劶銶状劷銹銻劽犾鋀勁鋂狃勄勅鋆鋈勉狉勌鋎鋏鋐鋒勒動勖勗鋘勘狙鋙務勝狟募勡鋤勤勧勩勪勬狭鋭鋲勲狳鋳勴勶勷狹鋼錀匀猂錆猇錈猋匌錏匏匐錑猒猓猗錘猘猜錝猞錡猤錤猥匧猨錬匬献錯匱猱匲匳猴猵猶錶猷錻医匼匾匿獃獄卅獅鍈午半卋卌卍鍍獎獏鍑獒鍒鍔鍖南獗鍗単獘卙鍚獝卞獟獠卡卣獣鍥獧獩鍩鍪獫鍭鍮獮獯獰鍰獱鍱鍳卷獸卸獹獺獻鍼獼鍽鍾卾鍿玁鎂厇厈鎈鎊鎋鎌玎厔鎔玕玗鎘厝玞鎡鎤厥厦鎧厨鎩玩厪鎭厭厮鎮鎰玲厴玵鎶厷鎹鎺厺叀珂参鏃鏄鏇珊鏊及友鏋鏌鏑珒叔珖叙珙叚叛叞鏞鏟叠珠叢鏢鏤鏥叮可叱右号珷鏷司鏸珸珻琁吁鐁吂鐂鐃球吃琇合鐈鐉同鐍后鐏吏鐐吓鐙鐚君鐟吟吠鐡琥否琦琪琫含鐮琮鐱鐳吴琴吸鐸鐻鐽吽吾瑁瑃瑄鑄呄瑇鑈鑌瑍呍呑鑑瑒鑒鑓瑗鑚瑛瑜呞鑡鑢瑧鑨瑨呩鑪呭鑭鑮鑯鑰瑱瑲鑲瑳味呵呼命瑾鑿咀咁钄璇咉咋璏璑咑璒璙璚璞咟璢咢咤咥咦咧璩咩咮璮璯環咲璲璵咸璹咺璽咽璿哄哈瓉哊瓌瓐瓓瓛瓞瓠瓢瓤哥瓦瓧瓨瓩瓪哪瓫哬哭瓮瓯瓰瓱瓲瓴瓶哶瓷瓸瓺瓻哼瓼哽哾哿瓿唀甃甄甅唅甆唉甌甍唍甎唏唐甑甒甓唔甕唖甖甗甘甚甜甞産甦甩唪唫唳唵唶唸甹町画甼唾啁商畇啇畉啊畊畎畏畐啓畔畚啛啞畢啣畫畭異畱畳畴畸镸镹畺畻啻畼啼畽镾畿啿閂喂疂喃善疅疆疇閇喇閈閉喉喊疊開喋疎閎疑疓喔疔喘疙喚疜喜関閤閧喧喩喪喫喬單喲閲疳疴疸閹喻疽閾疾嗁闃嗄痄病嗅嗆症嗇嗉闋嗌痌闌痍痏闐嗒痒闒闓闕痗嗘痘闘痙闚嗛痞嗞闞闟嗟闠闡痡嗣闤嗤闥闦痴嗶嗽痾嗾瘂瘄嘆瘇嘈瘈瘉瘊瘍嘍嘎嘏嘑嘒瘓瘕嘘瘛瘞瘠阢瘥瘧嘩瘩瘭阮阱嘳嘵瘵嘶阷嘷嘸阸嘹阻瘻嘻嘼阼瘼嘽阿嘿噀噁癁陁噂療癃噄癅癈噉癊癋陋陌噌降噎陏噏癒噔陘癜陜陝噞癟噠癡噢癢院除噤癥噦陦器癩癪噬癬陰噱癱癲陲噲陳噵癸険噺発登陽百陾嚀隂隃嚅隈嚈隊隋嚋皋嚌皌随皐隑隔皕皖隘嚙皚際嚝皝皟嚢皢皣隤嚥隥皥嚧隧隨嚨嚩隩險嚫嚭皭隮隯嚱嚳隳嚷皷隷隸皽囀盁囂盂盃雄囅雇盈囈雉益囊囍雎盎囎囏囐囑盒囓盔盖雘囙雞因難監囥盦盧囨雩盪雫盬园雯盰盱雱雲囲直零盶囹雺盻囿霂霃圄眄霅圇圈霉圉霊圊眊國看眎圏圑圕團霙圚霚圛眛霛圜眜霝真眠霡圢霢霣霧在霨圩圪圬眮霰霱霳圴圷眷霸眹霽圾着靁靃坅睅靆睆均睇靈靉靊坍睎靎坏靏坐青靕靖靗靘静靚睛靛靜睠睡坢督靣坥坦坧靧靨坩革睪靪坪靫靭靮坯睰坱靱睳靳靴坵靶坷靷靸靹靺睺坻靻靼睽靽坾睾靿瞀鞀垁鞁垂鞄鞅鞆鞉型鞋垌鞍瞎鞏鞐瞔垕鞕瞖鞖鞗鞘垙鞙瞚鞚垜鞜瞞垞鞞垟瞟鞟鞠垢鞢垣鞣垤瞥鞦鞨垩瞪垪鞫垬鞬鞭瞭鞮垰瞱鞱鞲鞳鞴鞵瞶鞶垸鞸鞹鞺瞼鞼瞽鞾鞿韁矃埃韃韄韅韆韇韈矉韉韊韎韐矑韑埕矗韘矚矛韛矜韝矞域矟韠矠埡韡矢矣矤知矦矧埧矩矪矬短韭矮韮矯韯矰矱韱韲埴矴韴韵埵執矸韸基韺矻埼頂砆頇堈砉堊砌堍預頑研堕頖領堙頙堛頜堞堠砠砢砥堧堪頬頭堯砰頰報場頴砵頷頸堹砺頼堽堿塀硃硄塉塍硎顎塏顏塐顑塑塔顔顗塗塘顛硜塞類塟塡硡顢顣硤顧塧塨顪填顫硬顬顱塲硾墀碁墁墅墈墊墌碍碏墐墓碔碕墖増碘墜墝碝墟碟墠墡碡碤碪颪墫颫碬碭墮碰碱墱墲碲碳颴颶颺墺墻碾磁壂飃壄飄飅磅飆壇磋磌壌壎磎壐磑壓壔壖壙壚磚壜壞壡磡飡飣壤飥飦磦飧磧壩飪飫磯壳飳磳磴壷壹壺飼壼磽壽餂餅夅夆礆餇礇餈養礌夏礐餑夒礒夓夔餕餖餗餘礙夙餚餛礜餜礞礠餠夤餧大夨天太夫夬礬夭礭冷央餮夯夰餱失礱夲餲夳餳餴夵餵礵夶夷夸餺示餻餼礽夾夿礿饀祀饁祁奃奄祅奆祆饆奇祇奈饈奉饉祊饋饌饍奎饎奏祐奐饐饑契祑饒奒奓祓奔饔祔饕奕祕祖套祗奘饘祘奙奚祚奛饛祛饜祜奝奞饞奟饟祟奠祠饠奡奢奣奥祥奧祧奨票奩祩奪奫祫奬奭祭奮奯奲祲奵奶祷奸祹祺奼祼祿禀禁禅禊妋禋妌禌妍福妒禓禔禕妖首禖妗馗馘禘妛禛禜禝禡妤馦禦禧妧令禩禪嶺禫禮羚聆禰妰鈴馱禱妲零馲禳妳禴領妷妹妺馺妻馿秀姃駄姈駈秉姍秏姐駓駔駚秚姜姝駞租秠秡駢秣姤秤秦秧姧駧姨秩姪秪秫駫秬姮姯駰称姱秱姴姶姷姻駻秼駾稀娀威騁騃稈娉稉稊稌騌騎税騏騐娒験稔騖稘騙稙娜騞騢娣騤稧娧騨娨娩娪騫稫娭種騰稱稴稵騸稹婀驀婁驁穂驂驃穄穉驊婌驍驑驕驗穗穙驛婞穟穡穢驢穣驤驥穥婧穧穪驪婭穰婷婺婻穽穾婾突窃窄媋窒媒媖窘媙媛媟媠媢窬骬骭窯媵骵窶骶媸窹窺媺骻媻媾骾骿髄竆髈嫉嫌髏髐竑竒髒髓體髕竕嫖髗竚嫠竡嫡竢嫣嫥髥竦嫩竩髩髪髬髮嫮髯竱髳竴嫵竺嫽竽竿鬀嬁鬂鬃笄鬈嬈笈嬉嬋鬋笋嬌鬌笑鬒笔嬗鬙嬛嬝鬟鬠嬢笥笧鬨笨笪鬪嬭笭笮鬲鬳鬴嬴笴鬵笶鬷鬹笹鬺鬻嬾孁魂魃孃孅魅孋魌魍魎孑筒孒答孕魖策魗魘存筝筠魣魥筦筭魭筮孮筰孳魳筴筵魹孺宁鮃宅鮇宇宊完箎宎宏宐箐宑鮓箕宗箚宜鮝箝箞実箠宣室宥鮦鮧鮩箪鮪鮬箬箭鮮箯宱鮲箴鮷鮸箺箻箼宼箽宿鯁寂寃寄篇寇鯇篊篋富篌寏篏寐鯑寒鯖寗鯗寘篝寞察寠寡寤實寧寨篨篩篪寮寰鯰鯱篲篳鯳寴鯵篶篷鯸篹篼鯿寿簀鰀簂鰄鰆簆簇專鰈尉簉尊鰊鰌尌簍對導簎鰏鰐簑鰑簒鰔簔尖鰖尗鰘鰙鰚簛鰜尞尟尠簣鰤簥鰦簦鰧簨尫尬尭尮尰鰰簱尲簳簴尵鰵尶鰶簶鰷簸尹簺簽鰽簾鰾鱁鱃籃鱄鱅鱉鱊屋籌籍屍鱎籏鱏屏籐籑鱒籒籔鱔展籖鱖籘屙籚鱚鱛屜籜籝鱞籞籟鱟籡屢鱣籣籤層籥籧鱧屧屨屩鱩屬籭鱮屰籲鱵鱶鱸屹屼屽粁粉岌粏岏粐岑粗岝岟粠粡岡岢粤粨岨粭粱粳岳岴岷岸岺岻岼粽精糂糄糅峇糇糍峒糓糕糖糗糙峝糞糟峠峡糢糦糧糩糫峯峰峱峺峽鴃紆紇崇崋崍崎紐紑鴒紓崗崘紘崙級紜鴞鴟素崤鴦崦紱紲崴紵崹鴺紺鴿崿絁嵂鵃鵄組絆鵇嵈鵊嵊嵌鵐嵑絑絖絙絚嵜絜鵜絝嵠鵡絣絥鵥給絧絨絪鵪嵭嵯絰嵰鵰鵲嵳絳絵鵶絺絻嵿絿嶁綁鶄綆鶇綈綉綋鶎鶏嶐嶒經鶚嶛綜嶝綞嶟綟嶠嶢綣綦嶧鶩鶪綪鶫嶫鶬嶬嶮綮綱鶱網嶴嶷綸嶹綺嶺鶺嶼鶼嶽鶿鷁巃鷄巇鷇巉鷉鷊巎緎鷏巓鷓緕鷖緘巘巙鷙緙鷞鷟巠締緡緤左巧鷧編巨緩巩鷩緪巫鷮差鷯鷰緱練巵緶巷鷸巹緻巻巽帀縀鸂布縅鸇縉縊希鸑縑鸒縒鸕鸖縗鸙帙縝鸝帝縠縢縣縦縧縨帨縫師縬席縮帮帯縯帲縳帶縶縷縹幄繄幅幉幋幌繎幐繐繒織幕繕繖幖幗幘繘繚繞幞繢幣繥繧幪幬繮幮繯幰干平年幵并鹸繸幸鹹繹幹鹺鹻鹼幾繾繿庁纁纂纃纆纉麌纎序纑麒纔底纕纖麗纚纛纜庠庢麥度座麩麪麭庵庶麸廃廈廉黋廌廑黔廔黕廕默黙廚廝黟廡黤廥黧廬黬黭黮黰黱黲廳黵黶延黸黹黻黼黿廿鼂弇鼈鼉弊弌弍弎鼎鼏鼐弐鼑鼒鼓鼔鼕鼖弖鼗鼙弙鼚鼛鼟弟弮鼯鼱鼷缺弻缻弼弽缽弾鼾缾弿缿鼿彀彁齄罅齅彅罇齇彈齊彊齋彌彍罎齎罏齏齓罔罕齕齖彗齗彘罘齘齚彜罝齝齞齟罠齡罡形齢罤彤彦齧彧罨齨齩彪齬彭罭齭置齮彯齯齰罱齱彲齳齵齶齷齺齽彽罾彾羀征徂羃羇羈徉羊羋羌羍美羏龏徐羐龐龑羑徑龒従羔龔龕羖徖龖得羗龗羚羜羝徝羞龞龠徠龡御羡龢羢龣羣群徤龥羦徧羨義羪羭微羮徯羯徰徱羲羴羶羸羹羼翃忈翎忐忑忓翔忔翕忝忢忤翦忨翩快翫忬翮忲翲翳忳念忶忸翹翺翼翽翾翿"));
        PART_TO_KANJILIST.put('丁', new Part(2, "嚀丁庁訂頂椅羇謌崎掎騎耓疔碕猗倚渟亭渮貯河紵訶檸舸欹町綺甼阿婀汀彁珂寄佇奇酊歌竒打何濘釘竚苛停嵜剞哥寧苧叮可柯灯獰呵荷畸聹軻埼痾"));
        PART_TO_KANJILIST.put('縁', new Part(1, "縁櫞"));
        PART_TO_KANJILIST.put('丂', new Part(1, "丂栲考拷巧雩朽"));
        PART_TO_KANJILIST.put('市', new Part(1, "市旆鬧霈姉閙肺沛柿"));
        PART_TO_KANJILIST.put('七', new Part(1, "七攄蘆切猇爈砌爐嘑謔鸕託甗嘘瀘樝琥瘧蘧褫戯戲鬳簴鐻謼譃罅齇虍虎罏蹏虐虒虓歔虔饕處虖魖虗虘托虙虚籚虜虝遞虞號虠虡鑢驢虢摣艣虣轤虤豦虧籧虩彪鑪詫艫慮顱噱鱸獹獻遽窃宅劇梍箎纑侘膚讞墟咤嚧璩覰嚱亳醵覷嗁擄巇鷉囐瓐矑諕櫖髗巘藘據臚壚瓛盧櫨篪廬滮鯱叱勴黸滹濾俿"));
        PART_TO_KANJILIST.put('布', new Part(1, "稀鯑布怖欷希晞唏"));
        PART_TO_KANJILIST.put('昆', new Part(1, "崑鯤昆混焜棍菎箟"));
        PART_TO_KANJILIST.put('万', new Part(1, "励栃杤万砺蛎"));
        PART_TO_KANJILIST.put('丈', new Part(1, "杖仗丈使吏"));
        PART_TO_KANJILIST.put('三', new Part(1, "舂蜂搆鰆謇三攘講素嬢蠢逢春縫騫輳搴樸謹椿孃襄浅蹇奉饉湊奏彗遘塞桟穣驤陦捧慧齧穰譲畴祷蹼瑾承残蚌讓冓榛涛纛溝璞実邦銭羮覯撲覲禳醸梼媾釀蓁懃僅構壌賎棒寒毒鋒囓僕篝拝壤勤秦寨曩菫蓬濮峯峰泰鋳践篷惷俸臻購烽賽寿槿"));
        PART_TO_KANJILIST.put('上', new Part(1, "峠寂督裃上槭垰鞐淑椒叔俶蹙戚菽"));
        PART_TO_KANJILIST.put('下', new Part(1, "峠抃裃閇颪下雫丐垰鞐圷梺卞"));
        PART_TO_KANJILIST.put('希', new Part(1, "稀鯑欷希晞唏"));
        PART_TO_KANJILIST.put('昌', new Part(1, "倡唱猖菖椙昌娼"));
        PART_TO_KANJILIST.put('不', new Part(1, "盃抔丕否罘歪胚不痞坏杯"));
        PART_TO_KANJILIST.put('与', new Part(1, "写与"));
        PART_TO_KANJILIST.put('明', new Part(1, "萌明盟"));
        PART_TO_KANJILIST.put('昏', new Part(1, "棔婚昏"));
        PART_TO_KANJILIST.put('丑', new Part(1, "紐丑狃衄鈕忸羞"));
        PART_TO_KANJILIST.put('易', new Part(1, "易鯣剔蜴錫裼賜"));
        PART_TO_KANJILIST.put('昔', new Part(8, "鵲昔藉措醋惜籍借錯"));
        PART_TO_KANJILIST.put('且', new Part(1, "咀徂疂組蛆莇疊俎雎姐萓且祖勗粗狙詛宜租齟砠耡渣柤鋤岨助沮畳苴査阻誼爼疽"));
        PART_TO_KANJILIST.put('丕', new Part(1, "丕歪胚"));
        PART_TO_KANJILIST.put('世', new Part(5, "蹀牃泄跇鰈葉幉喋詍牒世韘諜屜堞媟碟煠鞢迣緤屧勩楪渫呭貰鍱紲揲伳惵蝶笹枻枼弽"));
        PART_TO_KANJILIST.put('丗', new Part(1, "棄丗滞帯"));
        PART_TO_KANJILIST.put('丘', new Part(1, "邱鋲岳兵丘駈梹浜蚯"));
        PART_TO_KANJILIST.put('丙', new Part(1, "炳柄病鞆丙陋"));
        PART_TO_KANJILIST.put('帚', new Part(1, "帰箒掃婦菷歸帚"));
        PART_TO_KANJILIST.put('帛', new Part(1, "錦棉帛緜綿"));
        PART_TO_KANJILIST.put('阜', new Part(1, "埠阜"));
        PART_TO_KANJILIST.put('昜', new Part(1, "暢殤蕩楊盪蝪煬瘍蘯慯湯塲膓場觴傷暘腸揚昜陽"));
        PART_TO_KANJILIST.put('阝', new Part(3, "蠁簄琊堕娜阜阝阞阡阢阤阥阦阨阪阬騭帮阮阯阱防阳耶阷阸阹爺阺倻阻阼阽阿婀陀陁陂癃附扈灉癊陋陌降陏限陒陔陖陗陘屙陛鱜陜陝陞陟陡院橢陣除陥陦陪陬癮陮瑯陰陲陳陴陵陶陷陸険陻陼陽陾陿隁隂檃隃隄隅梆隆隈隉隊隋隍階随躑隑隔邕隕隖邗隗邘隘邙隙隚梛邛際墜障隝隟邠隠邡邢那隣隤邥隥邦隦隧隨邨隩邪險墮嚮隮邯隯邰隰邱隱邲隲邳隳邴隴邵邶邸邽邾窿郁滁郃郄郅郇蓈郈郊廊郌郎嫏曏泐郒廓郕廕郗郘郙郛裛郜郝郟郡郢鋣郤郥部哪郫滬郭郯郰擲郴郵郶郷都櫽郾郿蔀鄀鄂鄄鄅鄆唈鄈鄍鄐鄒鄔鄖鄗鄘鄙鄚鄜鄞鄠鄢鄣鄥鄧鄩挪蔭鄭鄮鄯蔯椰鄰鄱鄲霳鄴鄶鄷挹鄹鄺鄼鄽祁酃齆筇酇酈啊捓饗浥蕯綁鎁螂薌嶐榔讔憜薩鞹俋埠槨篨揶痾響"));
        PART_TO_KANJILIST.put('帝', new Part(1, "締蒂楴蹄諦啻啼帝"));
        PART_TO_KANJILIST.put('丞', new Part(1, "蒸烝丞拯"));
        PART_TO_KANJILIST.put('星', new Part(1, "醒腥猩惺星"));
        PART_TO_KANJILIST.put('両', new Part(1, "満両輌"));
        PART_TO_KANJILIST.put('縢', new Part(1, "縢籘"));
        PART_TO_KANJILIST.put('縣', new Part(1, "縣懸纛"));
        PART_TO_KANJILIST.put('春', new Part(1, "蠢春鰆惷椿"));
        PART_TO_KANJILIST.put('並', new Part(1, "並椪譜普"));
        PART_TO_KANJILIST.put('丨', new Part(1, "逄倆倌怍怎倎倏怏透倐瀑耒耓逓耔倔耕耖怖耗瀗耘耙候倛耜倜耝耞瀞耟速瀟借耠耡逢倢耤値耦耨倩逪倫怫耬瀬逭耮耰瀰怵瀷逸瀹瀼瀾偁灃恆假灊偊灋遍灎偏遐偑灔聘遘偙恝遝灞恠遣健遭恭遮聯偰遰遲聵恵選遺聿肁傁邂肄肅肆肇邇肈悌傏悑悕悗悚悠患邦肧悪傭肭邯邳炳傳悳邶炸肺悽僀僂胄働僎惎胏僐僓僕胖郗烘胙胚胛惜郜惠僢胮郵惵胼惼烽郿儂鄅儐愑焔愖鄘儘儙儛儞鄞無儣脩愫脯鄲儳儴鄴儵鄶鄷鄹儺脼焼煁煅慅腆煆煉慊腊免兎酏児兔兕腗慟煠酢酣兤煤慧兩腫酬慬腱共慱慲腴慵其典慸煹兼兾冀膁膄膅内円膆憇冉冊醋憋再熏冑憒冓憓冕憖膖憚熚膞醡熢冤憤憨冪醭憮熯冲醲膴醴冸醸憹醺醼冿膿臀釀懀燁凂釂懃凄臅懈釉臋重凍懏臏釐燕凘臙懜懟臡懣凧燭懯凲懶臶釶凸燸懸出燻燼凾臾戁戃爆舉舏鈒鈕爗爚爛舝舞爟舡戡舢判戦舨刪爯鈯戰鈰刱舲舳戴舴制刷刺舺鈼爾鈾鉀扁剃牃艃艄牅艅艆鉇艋剌艎剏艏艑牒扔艖鉗剘艘艚剛艜鉞牠艠鉡鉢艣剤艧剩扭艭鉮剰扱艱鉵艶鉷剸剿艿犀劃抃劅芇銉劊犍劓抔劖銖劘劚銟芣芥芨励抷抹銹抻銻押抽芾芿拂狃勅狆勉狉拌狎苐拑苒鋒狒勒動拖勘拙苚拜拝苡苢苤勤勥勦勩独拱英勲拲勳拵勷苷錀茁茉錏猓錘猘猜匜匝茟匣錤錦錧錪挫錬茬錯茱匱猵錶挽茽匾挿捁獅卅卉半卋卌卍荍鍍荐鍖鍗獘単卙鍛捜鍜卞鍥捧獨獬獮獯印獱鍱鍵捵捶獺捻獼鍽鍾捿鎀玁掃掄鎋鎌鎏鎕掘鎚掚莜厝玠莠掣措厪莬鎭厮莱厴莵玵掻掽揀叀菁鏄菅珅鏅珊鏊及珋鏋収叏菐珒叓揓菓揕珙叚叛鏞菟叟珠鏤揥鏧叫菫珮華揲史揳揵菶鏷菷鏸揹鏹鏽萁鐁鐂搆琇鐉吊萋向搔搜搢搥否搪琪琫鐮琯鐲搴吶萸吸萹携吽搾瑀呄瑇鑈葉葎葏摒鑓摓瑕瑛葜摟董瑨葪葭呭摭瑭葮鑰鑲味摴摶呷葹呻瑾咁蒂咃钃咄撇咈咋璑撒蒒撕璢蒪撫璯撰撲璵蒹璽哄蓆擉瓌擕蓙蓚瓤蓧蓬瓴蓴哶瓺擻瓻蓽唀蔃攉甎攏唏唐甒攓蔕唖蔗攘甘攙甚甜甞攤蔧唪攮由甲申收唶蔶攸唸甹画蔽唾蔿蕀啁畃畊界畍蕐敒畔敕蕙啛敝啞敟畢蕢散蕩蕪畫蕭蕯異畱整敷數镸镹蕻啻啼蕽镾啿斃斅喇閇斉喋斎疎喎斒薓疓閘喘閙薛斛斟斠薢斣疥薥閧閩喪薫断單斮斯斲疳斳斷施薾藂痄病旆藇旈藉嗉旉藊闌藎旔藕旗嗛關痞旟闠闡痡嗣藤嗤闦旧痩藪痯嗶旹藼嗽藾瘂瘄嘈瘈瘋嘍蘍嘒昔瘕蘖蘘瘙瘛蘛瘝映昢昤阤昦昧昨嘩蘭嘯蘯嘱嘳嘴嘶嘸瘻阼嘽嘿噀虀噁虁陁噄癅噉陋癋降晎癜晜晝晞癟晡虡噣陥噦晩癩晪虫虬普虯虱癱陲噲陳噴虵虶虷虹虺虻暁隂嚅蚈蚊蚋蚌蚍暎蚑蚓蚕蚖蚘蚚蚜嚝皟蚡暢嚢蚣皣蚤隤皥嚥蚦蚧蚨蚩嚩蚪蚫蚭嚭嚮蚯蚰蚱蚳嚳暴蚴蚵暵蚶蚷嚷蚸蚹暼蚿囀蛀蛁盁曂蛃盃曄蛄蛅囅盅蛆蛇盈蛉囊蛋蛍蛎囏囑蛑蛒蛔蛕雖蛗雘蛙蛚蛛蛜曝蛞蛟蛠盡蛣蛤蛥蛧囨曨蛩曩盪雫蛬蛭蛮蛯曲囲曳直曵書蛸蛹曹蛺蛻蛼蛽蛾蜀蜂蜃蜄蜅蜆圇蜇朇蜈霈蜉眉蜊霊圊蜋蜍蜎朎朏蜏蜐蜑圑蜒眒蜓蜔眔圕團蜘蜙霙蜚圚眛圛蜜蜞朞蜟期蜡蜣蜥蜨在蜩未末本蜮朮蜯霰地蜱朱蜲蜴朶蜷圷霸蜹蜺蜻蜼蜽蜾眾圾蜿朿圿蝀睂蝃蝅睇蝉靊蝋蝌蝍蝎睎坏坐蝓蝕靕蝗蝘蝙睛靜蝝杝蝟束蝠蝡睡坢蝣坤蝤杤蝥来蝦靧蝨坩革蝪靪睫靫靭蝮杮靮蝯杯坯蝱東靱蝲靳蝴靴蝶靶靷蝸靸靹靺蝻杻靻靼靽蝿靿鞀极鞁螂垂螃螄鞄螅鞅鞆螆螇螈螉鞉螋鞋螌融鞍鞏螐鞐螓瞔螕鞕鞖螗鞗螘鞘螙鞙瞚鞚果垜鞜瞞螞鞞螟鞟螠鞠螢鞢螣鞣瞥鞦螧鞨垩垪螫鞫鞬螬垬螭鞭螮鞮螯垰螱瞱鞱鞲螳鞳鞴螵鞵瞶鞶鞸鞹螺鞺螻枻鞼枼螽螾鞾螿鞿蟀柀蟁韁柂韃柄蟄韄韅蟆埆韆蟇韇蟈韈蟉矉韉蟊韊蟋蟎蟐某柑蟒蟕埕蟖矗韘蟙柙柚矚蟚韛蟜韝柞蟟蟠埠矠韠埡韡蟢蟣蟤矧蟪矪蟫柬蟭柮蟯矰蟱蟲蟳埴韴柵蟶蟷韷蟸韸蟹蟺基蟻埼埽蟾蟿柿堀蠁蠃栃蠅蠆蠉砉蠊蠋堍蠍蠎砎蠏蠐蠑蠒蠓蠔研栔頔蠕蠖頖蠘蠙堙栙蠚蠛蠜蠞堞蠟蠡蠢蠣頥蠧蠨堪株栫蠭砭蠮蠰蠱栱蠲蠵蠶砷蠹堹堺蠺砺蠻蠼頼塀衃衄衅顆顊衋塐顒術衖桗塘桘衝衞顢塨表桮确衱衲衷桷桺桻硾塿墀碁墈墉墌碏墐碕袖碘袘梚條墟碟墠碡袢風碪颪颫碬颭颮颯梯颰碰颱墲碲墳颴颶颷颸颺梺颻碾颿飂壂飃裃棄飄棅飅棆飆飈棉棊棋飌壌壎壐棒磓壖壗棗棘壙磚壝棟磡壤壩飮裯棯棰裱裲棲磳壷裸裹製飾褄餅夅夆餉褊植褎褏礐褙礜褝餠褠椣夤夥椪央夰褰椴椵褸椸椹餹礽襀襁襄祄祅襅祆襆饆饈奉饉祊饋饌襌襒楓奔祔楗祚祛襛祜楝奝神祟饟襡奡楣祧楪祫業襮奯祲楳襴楴奶她祹楹襺祺祼楼禅覇覉榊覊禋妍妎禔覔妕禕禖禘禛榡禡覥馦榨禩禪禫禮覯妯禯禰妰禱覲禳馳禴覷禹妹馺妻覼馽秀槇姈秉姉姊構槌姍角駓觔觕觖觗觘觚秚駛觜觝秞駞秠駢秣解觥槥触秧觧觩槫觫秫觭秭觱槲駲觳觴槵觶觸觹槽觽觿槿稀騁娉稊騏訑騒樓樔稔稘騙稙騞樠訠騢娣樤訥稧騨樨娩横騫種訯稱稲稴騷訷樺樻樾婁驁穂橅穉橊驊驌詍詐驑橒評穗詘穙詜穠橡穣驤驥穥婦橧穪婭穰穽誄窄誅檇檋窋檑媒誘媚檞窟媟檰檱課誳窶媸媻檽媾櫁嫂櫃髃諆諈嫌諌髏髑體櫔論諛櫜諜諞髢嫥竦嫦諦嫩髩諫竬髬嫮髯櫰竱櫱髴嫵諶諷髷竾鬀謀欃欄謄笄謇笈欉謊嬋謙鬙講笛欛嬝欞鬠嬢鬦笧鬧鬨笨鬫第嬭笭鬭笮欷笹欺嬾譁孁筁譂孃譃譄魅筆歉魌魎譓譔歔孕策魖存譜魥歪筪魪筭筰筱譲魳譴歵歸孺孼孽殃殊鮋讌箍讏箐讒箒讓鮓箕殖宙箝箠殢殣殨箪殫宱段鮷鮸殸宺箻殿寃寅篇鯈篊篏寐鯑寒鯖篖寗毚篝篠寠寨篩寯鯰篲篳篷鯿専專鰈簉鰊簍簎鰕簛鰜尠簣鰤鰥簨簫尬氬尭尮鰰尰簱尲簴尵簶鰷簸簺鰽簾鱃鱄鱅屈鱉届豊籍屏豏籏鱐豑籒豓籔豔展鱛屜属籟鱟池籥鱧屧汧屨豨豫汫屬豭鱮籲汲豾岌沔沖粛沛岝粠貤岫沫岬貰沲貴沸油岺費岼賁糂糄泄泏糔泔糖糙賙賚糞糟峠泠泧糩泮峯泰峰泱糵糶賺購賽糾賾贃贉洊崋納崎贐紐紑紒贒紖崘崙洙洚級崛津鴦鴨洩洪崫紬洯洲紲紳崹紺絀絁嵂鵃絆嵊嵋嵌絏嵐絛鵜絜浟絣鵤鵥赧鵪赮嵰鵰鵲赳鵶嵹絺絻浼嶁鶄鶇綈綉嶒涕継綞趞綦鶫嶫嶰綰鶱嶲綸鶼綿跇淇淈巉巋淎淒鷓緕緙跚淛緜緞州淟淠締巣緤跥編鷩淪鷫鷰練淵巷巸巽巾帀縀市布丄丅帆帇万済踈渊帋下縋希丌帍不丐帑丑縑踑帒鸒帔帕丕帖世踖丗帘丙鸙帙帚帛踝帝帟帠両渡渢縣丣帥渦並渧縧丨帨縫師渫丫席中帮丮港帯丯縯帰丰丱帲串縳帳踵帵帶縷帷常踸丼踽帽帾幀蹀蹁湃幃乃蹄幄湄幅幇蹇湈幉幋幌繍乍蹍幎繐幐幑乑繒幔蹕幕乕繖幖幗乗幘湛幛蹛幜幞幟也蹠幡繡繢幢蹣幣幤繦幨蹩幪幫幬繭幭蹭幮湯幰鹻繼幽繾湿満纁溂躅溍庎溏麒溓纔井纕躛纛亜溝麥度座麩麪麭溮麯躰躱溲庶庸亹庽溿軆廉介滌廌滍仍廑廔他黜廝滞廡以廥廨軨滫滭黮滯軰仲价廸軸建滽滿廿伂輂鼈輈弊伊漊伋輌漌鼎鼏鼐鼑鼒輓演弔漕引弗輛弛弝弟輨輪鼬漱伳伴漶強伷伸似鼼弽弾輾伾弿佀罄彇彈佈轉彌彍潒潓体潔彗罘齘彙潚齚佛作轝佣罤潨佩置潰齰齲彵併潽彾彿羀羃來羇羈律羋澌侏澐羐澑侓澔龔侖供龞羞侟龠徠龡龢龣辣徤龥澥辥徧澧澮澱農侶辸便翀濁濂濃濅濆迆翈迊俌翎濔迚俛翛俜忠忡迣迤俤翩迪迭濮修迮述俵念濵俸忸迸濹翼追"));
        PART_TO_KANJILIST.put('丩', new Part(1, "丩叫糾"));
        PART_TO_KANJILIST.put('温', new Part(1, "薀温"));
        PART_TO_KANJILIST.put('个', new Part(2, "琀合倉砎栓途領瀚怜頜吟耠逧含倫尬搭鐱瀲簳琴搶頷谷谸谹倹瀹耹谺鰺堺谽簽搽谾逾谿搿豁聄豅聆葊摎塔塕塗瑜鱠籡瑢呤籥桧恰鑰硲瑲籲偸摻遼桼命衾衿悆璆邌貐岑傖墖袗傘撘傛撡梣貪傪粭璯沴袷傺蒼撿璿飂郃棆峇僇哈瓈蓉僉壑擒裕賖胗棜糝磟泠壡飡飣郤飥飦擦飧糩峪飪飮棯飳瓴飶鳹飼蓼棽賿餂唅礆餇愈儈餈儉愉脊餑鴒紒褕餕餖餗崘餘蔘崙餚餛検餜餟蔡餢餦餧唫餫蔭餮冷餱餲餳愴餴餵鄶唸愹唹餹餺餻餼洽紾鴿饀饁襂祄饆饇饈界敍畍饎酓饔慘敘饘饙畛饛饜饞饟饠楡嵢給腧全兪祫祭畭浴酴絵嵺慾憀趁疁斂涂薈薋綌妎熔涔榕妗斜膝閝薟膠斡閤疥覦榦醦閦令喩醪嶺鶬嶮羚聆鈴零領禴醶冷綸憸疹嶺鶺喻趻於閼禽膾冾懀嗆姈跈臉痊槍嗒鷚藜闟秢淤跧淪槮鷯淰凳姶嗿瘀瘉稌舍舎嘐鈐騐昑舒験稔舖舗舘鸙爚樛渝蘞瘠瘡刢昣昤戧瘩个戮踰瘳鈴診刼縿療驂穃艅詅繆穇癊蹌噏牏蹐癒驗晗艙噞癟剣除詥鉩繪詮陰剱噲剳創牷鹸幹剹蹹険鹼鉿隂隃劊劍檎庎劒銓劔際檜嚟檢芥蚧芩險窬芲隳溶滁滄嫆蛉今介拎髎鋏仐盒苓黔廕論廖諗仚竛勠鋡蛤令廥盦軨嫪軫諭鋭黭仱黲滲狳拴零苶价囹拾拿錀企會漆圇弇朇輇匌蜍朎眕鬖众会錜猞鬠輪謬匬笭伱欲匳錵伶茶輸漻匼圿睂荃荅坅轇筌蝓答佘齘歙余歛潝齝齡齢譣捨獪魪獫鍮佮佱佺卻捻荼彾睿魿殄掄侌徐鎔龕侖鎗箚羚莟龠玠龡龢澣龣龥澪玪瞭羭殮澮澰玲鞳掵容瞼參柃揄叅俆蟉珍翎翏鏐韐翕俗叙矜揜埝察叡珡寥鏥篨濬寮鯰翰鯲念篸菸毹毿"));
        PART_TO_KANJILIST.put('師', new Part(1, "鰤獅篩師"));
        PART_TO_KANJILIST.put('中', new Part(1, "蠁蠃蠅蠆專蠉蠊蠋簍蠍蠎蠏蠐蠑蠒蠓搔蠔蠕蠖蠘蠙蠚蠛蠜蠞蠟蠡蠢簣蠣蠧蠨蠭蠮蠰蠱蠲鐲尵蠵蠶蠹蠺蠻蠼瑀鱄偊偑鑓籔属遣屬聵摶遺钃沖患風颪颫颭颮颯颰颱傳颴貴颶颷颸颺颻颿僂飂飃飄飅飆飈擉飌僓糔磚壝惠蓴蔃鄅甎儙褸襁慅饋嵐楓蕙襡蕢慱數嵹襺楼憒憓膞斣薥閩冲禹臅闠嗤触藪槫燭觸瘋騒樓瘙渢中嘱串嘳縳縷騷瘻樻踽婁繐穗繢噣繦虫虬繭虯虱虵鉵虶虷剸虹虺虻繾躅劅蚈蚊蚋蚌蚍蚑蚓蚕蚖蚘劚蚚蚜蚡蚣蚤隤蚦蚧蚨蚩蚪蚫蚭蚯蚰蚱蚳蚴蚵窶蚶蚷媸蚸蚹庽蚿囀蛀櫁蛁櫃蛃蛄蛅蛆狆蛇蛉蛋蛍滍蛎髏囑髑蛑蛒蛔蛕雖蛗蛙蛚蛛蛜蛞蛟蛠蛣蛤勥嫥蛥蛧蛩独蛬竬蛭蛮蛯竱仲諷蛸蛹蛺蛻蛼蛽蛾蜀蜂蜃蜄蜅蜆蜇蜈蜉蜊蜋蜍蜎蜏蜐蜑蜒蜓蜔蜘團蜙蜚圚蜜蜞蜟蜡蜣蜥蜨蜩蜮蜯匱蜱蜲蜴強蜷蜹蜺蜻蜼蜽蜾蜿蝀蝃蝅蝉轉蝋蝌蝍蝎蝓潓譓蝕蝗蝘蝙蝝蝟蝠蝡蝣蝤蝥蝦靧獨蝨蝪蝮蝯潰蝱齲蝲譴蝴蝶蝸蝻蝿螂螃螄螅螆螇螈螉螋螌融螐螓螕螗螘螙螞螟螠螢螣螧殨螫螬螭螮螯螱螳螵瞶螺螻掻鞼螽螾螿蟀濁蟁蟄鏄蟆蟇蟈蟉蟊蟋蟎蟐蟒蟕蟖蟙矚蟚迚蟜蟟蟠忠蟢蟣鏤蟤蟪蟫蟭蟯蟱蟲蟳蟶蟷蟸鏸蟹鏹蟺蟻蟾蟿"));
        PART_TO_KANJILIST.put('席', new Part(1, "蓆席"));
        PART_TO_KANJILIST.put('昭', new Part(1, "照昭"));
        PART_TO_KANJILIST.put('帯', new Part(1, "滞帯"));
        PART_TO_KANJILIST.put('是', new Part(1, "提堤寔匙題醍是"));
        PART_TO_KANJILIST.put('丰', new Part(1, "丰峰蜂邦縫"));
        PART_TO_KANJILIST.put('丱', new Part(1, "丱歔關墟聯"));
        PART_TO_KANJILIST.put('串', new Part(1, "婁僂患鏤藪簍髏串樓籔窶縷數褸瘻螻楼"));
        PART_TO_KANJILIST.put('縵', new Part(1, "蘰縵"));
        PART_TO_KANJILIST.put('丶', new Part(1, "瀁倄瀅逋倏逑瀛逛逞怟怱倵怵耹怺聀恁偂遃遅恆恇恌恐遑達灕恖聖遘遙偙恙遝恟偟恡聦偬職灼偽聽肁傄傅肈悈邋邎邐悑悕傖炙傜肞傞悢肧悩肬傯邳肴悷炷邸傺為郄胈烊僐烑惑郒烕郕僕烖胖郗郙胚僟郢僦僱惶胸烺胾儀儇儈鄈脉儍儎儞感愡鄧脯鄯儳脳儴鄴愵然鄹愽慅兇酈酌煌兎酓兔酗酙全酬酲酺慼膁醆膊再醎冓膕憖熟冢冤醨膩冬醬冭憭醭熯熱醱膳憾熾燃釃釈减凑臓懕臗釜臝懟臟凡懢釣臧懩釩燬凭懯凰懲懴釵燵凶臹懺釻釼刃鈄刅爇戈戉爉戊戌戍戎成鈐我戒划鈒戓戔戕或舖舗戚戛戜戝戞鈞戟戠戡舡戢刢舢戣判戦鈦戧舨戩截戫鈬戮戯戰刱戲舲戳戴爴舴鈸戹舺鈺刼戽扂鉂牂扃艃扄艄艅扆艆艇牋艋牎艎牏艏艑牖艖剗扚艜鉞扠艠艣艤鉥艧扨牫艭牮鉯剱扱剷牷牻鉽找剿抂芃芄銊劊劌芍銎劒銓劔抔抖劚芝択犠芣劥犧芨芩犬銭犮犱抵状銶抷劷犹芺劻犾狂鋃拄拆勆狉拌鋌拎拔狘鋡苡勢苤鋥勦鋨鋪勪狭拭鋮苳狳拴勶勺匀匁茂匆茇匈錈猋茋匍錑猒匘匛錜挟匡錢猤匩挭献茯茰匲猵挵茷猷区猺挺錻匾獃荃獄捄半獎荏鍐獒捕荗獘単博卛獠鍠獣卥獩鍯鍳荵卵獸獻鍼鍽荿鎂莄莆鎈玉王鎌莍玎厎玐玓玕玖厖掖玗玘莛鎛玜玞鎞玟玠玢玥玦玩莪玪玫鎫厭玭掭掮玲玳厴掴莵玵玷玹玻玼玽掽玿珀珂珅叅珆珈珉叉珊及珋珌双珍珎珏菐珒珓揔叕珖珙叛珝菝珞菟珠珡叢珣珥珦珧珩珪揬班珮珱珴珵珷鏷珸菸珹珺鏺珻珽現珿琀琁吂球琄琅理搆琇琉琊吏搏琑搓搔鐖琚琛吠鐡琢搢琤搤琥琦否琨琩萪琪琬琭琮鐮琯启琰琱琲琳琴鐵琵琶吸琹萹琺鐽琿瑀瑁瑃瑄瑆瑇呈瑋摋瑍呎摑瑑瑒摔瑕瑗瑙鑙瑚瑛摛瑜瑝瑞瑟瑠葡葢瑢瑣瑤呤瑦瑧呧瑨瑩瑪瑫瑭瑮鑮鑯葯瑯摯瑰葱瑱瑲葳瑳葴瑶葼瑾璀璁蒁璃钃璅璆璇撇璉璋璏咏璐璑璒璘璙撚璚璜璞璟璠璡撡璢璣璦璧璨咩璩璪蒪璫咫璮璯蒯環璱蒱撲璲蒲璵咷咸璹撻璻撼璽璿瓈蓈哉瓉瓊瓌哎瓏瓐擑擒瓓瓔瓘瓚瓛瓞哢擥哦瓦瓧瓨瓩擪瓪瓫哭瓮瓯瓰瓱瓲瓴瓶瓷瓸哺瓺蓺瓻蓻瓼瓿攁甃甄甅唅甆攉甌甍甎蔑甑甒甓甕甖甗攙攞蔣攦唪甫蔵唹畃蕆畇畎救畔蕟敷蕺畿善薄薉喊斌閏斑斒斕斗薘斘料斛斜斝閝斟閠斠斡疣斣喤閥斥閬喭喲疺疼於閼薽閾藂闃旃嗅嗆旉痊藊闋藏旐痒旘闙嗛藝痞嗟痡闥旺嗿昀瘀瘃昈嘎昑瘙蘚瘝昤瘥嘰瘵昶阸嘹昹瘹蘺阺昼晀療癊晗晟晠噠晡虡癢虣癥噦噩癬癱晵暀隃的皇嚈隍蚘皝蚤皥隥嚩蚪嚭暭嚱蚳蚸蛀盃囈囉盋囍曏囐盖雘盙盛盞囟離囥曦盦蛧蛩雫曫曬囱図曵国蛾圃蜅霆國蜋朎圑蜓朓眔望蜜霡朢蜣眨霪蜮朮圴眶眽圾着坅均杈蝉蝌坍睎坏杓杖蝗杙睠靨靫靭靮杯坯靸坺坻坼睽极垈枉鞉螌鞏枓螕鞕垗垟垡枢螣垩瞪螭枰鞶螽鞿螿韃柃蟄韈蟈柉柊埌韍城埔埕蟙柝埝域蟟蟣韤矪韯韱柱韵蟶埶執蟻蠃蠊頊堍栓蠘蠛蠟砡頫砭堭頯栰頲栴栻栽栿堿衂衃框硇塉桊衊塍衑顑術顖塚桟硠顣塨衩硪顪桮桯衱桹硺塼衽塾墀梁梂梃墊碊袋墏碔袚袛墡袢梣碧碰颰械袱碱梵袵颻裁飆磇磋裎壐壓裓裘棙補棜磠飡飣飥飦飧棧飪棫壬棭磯飳飶裷棽餂餇餈養餑礒椓餓餔餕椖餖餗餚餛餜褝餞餟餢椢餦餧褨太餫褰餱餲餳礴餴褵餵椶褹餹褺餺褻餻餼礿饀饁奃襆饆饇饈饍饎襏饑襒祓饔祕饘饙饛饜饞奟饟饠楤祥祧楩襪業襮奯楲襳楻襼妁禅妊榑妒榒覔妗馘覛馛覜馦禨嶺妭覯羚馰覴覷馺离禽馿駄駅駆秇姈構槎駐科姒駓秘様姙秚姜秠秡秢觧觩秪秫槭觱駲槲槳駴駹槹稀娀樁威稂樃稉程娍騐樑樒樔樣騣騤娥樨騨稯訯娰樲訳樴訴稴騷騸樸註稽樾穄詅驇驋橑穖驖穙機詠穢試詧橪婬詮詳詶婾詾窆誆檉檋認檎誏誐誑媓媙檝誠誡檥誧骪檫窬骮窯媱媲媵骶媻骾媾檿櫁髆櫆嫏諑諓髖諗竛櫤諬髬髮櫰諴櫼嫽笂欂欃謆鬈笈欉鬉鬋謐欐鬛講鬥鬦欧鬧鬨鬩笩鬪鬫笭鬭鬮嬴鬴鬷魁魃譃孅譅孋筌譏筏筐筑魑魕魖識魘筠魡筤魥武歪筬議孰譱歳筳孵魷筺譼譾魿宊箋残讍宐讒宔讖鮗殘宝讞箞殠宨箪宬鮮箯殱殲鮲箴殴殺箺殽殾篁鯁寃密鯆築鯎寒鯗寚篝毧毬寮鯲寳鯳寶篾鯿簂尃鰄簄鰉對氐鰔簗気簙尞簠尠尤尦簦氧鰧尨尩鰩尫就尲簴尶鰶氷永尺簺尽谽簿鱁求鱃汊汋豋汍汎豏籑豓鱔籖籛鱜籝籤豨汪籬籭汰汲鱲汴鱵鱶鱷豹汹籾豾岌粏岒沢貮岱粱貳貶貸岺賃糉賊泌賎糔糕賕糗泛泜泝泠賡賤賦泧峨注峩泳鳶鳹賻購約贄贇紈洋鴎贏紏洑紑贓級紝崤洮紱洲崴洶終嵃鵃浅絆嵆絍嵕浗嵙絚鵝鵞鵟鵡嵢絥浦浧絨鵩嵯嵰嵳絺絿綆嶈越涔綛嶛綦鶪綫嶫嶬鶬鶱趵趻鶼嶽涿跅淆巇巉跋緎緘巘鷙州淢巣淤巤跧巩緪跫淫緫差淰鷲践緵緶淺縅帆丈不帍踐鸑縒丒鸒帒丕減縛鸝丟帟丫縬縭丵丶縶丸丹主渼丼乀乁蹁乄繇蹉繊之幋繎乏乑織繕幗蹙幞幟湟蹡蹬幭蹴鹹鹺鹻蹼幾躂麈纎躐溓纔底纖亙纚溝庠溥躧庬庭躯亯溻躾滃軄滅黈滏黐廕仗默黙黛仞仢代以廥軨黬軭仭仱廳黶廷軷滹黻任滻黼軾仿弄輇弈漈輈載伋弋弌漍弍弎伏式伐弐弑漓輔鼕鼗漛鼟弤漭漰伴缻似弾漾伾齅住网潑轑罓轗罘罜轞佟潤彤彧潨罭佯彲彴佺彺彽彾使往澀羀澂徉羊羌侌羍美羏羑羔羖羗侙羚羜羝羞龞羡羢羣群羦徧徨羨義羪羭侭羮羯羲羴徴羶羸羹侹羼俁俄俅必濈濊迋俌忍俍忒翔翥濮述忲濺翺翽"));
        PART_TO_KANJILIST.put('帶', new Part(1, "蔕帶滯"));
        PART_TO_KANJILIST.put('丸', new Part(1, "勢笂蟄贄囈摯孰熱埶執丸鷙褻藝塾熟"));
        PART_TO_KANJILIST.put('常', new Part(1, "蟐嫦常"));
        PART_TO_KANJILIST.put('丹', new Part(1, "旃栴丹"));
        PART_TO_KANJILIST.put('主', new Part(1, "往駐柱注麈主註住"));
        PART_TO_KANJILIST.put('丿', new Part(1, "耀考耇耈耊耏耒耓耔耕耖耗耘耙耜耝耞耟耠耡耤耦耨耬耮耰耽耾聀聄聊聘聟聠聦聨聭聯聰聱聲聳職肄肅肇肈肌肜肞股肧肫肬肱肴肹胍胏胖胗胙胚胞胤胮胯胱胲胳胸胼胾脃脆脈脗脘脠脧脩脬脰脱脳脾腆腇腔腗腠腦腩腫腭腯腸腹膀膁膂膈膎膐膓膕膖膚膜膠膣膨膮膰膳膴膵膸膽膿臀臂臅臈臋臍臏臓臗臘臙臚臝臟臡臧臨臱臲致臵臶臹臻臽臿舀舁舂舊舒舓舚舜舞舨航般舴艀艇艏艑艖艙艚艜艠艣艤艧艨艫艱色艴艶艷艸艻艽艾艿芀芁芃芄芇芉芊芋芍芎芑芒芔芖芘芙芚芛芝芟芠芡芣芤芥芦芧芨芩芪芫芬芭芮芯芰花芲芳芴芷芸芹芺芻芼芽芾芿苅苆苐苑苒苓苔苕苗苙苚苛苜苞苟苠苡苢苣苤若苦苧苨苪苫苭苯英苳苴苶苷苹苺苻苽苾茀茁茂范茄茅茆茇茈茉茊茋茎茖茗茘茛茜茝茞茟茡茢茣茨茫茬茭茮茯茰茱茲茳茴茵茶茷茸茹茺茼茽荀荂荃荄荅荇草荊荍荎荏荐荑荒荓荔荕荖荗荘荰荳荵荷荸荻荼荽荿莀莂莄莅莆莇莉莊莍莎莒莓莔莕莖莘莙莚莛莜莝莞莟莠莢莦莧莨莩莪莫莬莭莱莵莽莾莿菀菁菅菇菉菊菌菎菏菐菑菓菔菖菘菜菝菟菠菨菩菪菫華菰菱菲菴菶菷菸菹菻菼菽萁萃萄萆萇萊萋萌萍萎萏萑萓萕萙萠萢萩萪萬萯萱萵萸萹萼落葅葆葇葈葉葊葍葎葏葑葒葖著葘葙葚葛葜葠葡葢董葤葥葦葧葩葪葫葬葭葮葯葰葱葳葴葵葶葷葸葹葺葼葽蒁蒂蒄蒅蒋蒐蒒蒓蒔蒕蒙蒜蒞蒟蒡蒦蒨蒩蒪蒭蒯蒱蒲蒴蒸蒹蒺蒻蒼蒽蒾蒿蓀蓁蓂蓄蓆蓇蓈蓉蓊蓋蓌蓍蓏蓐蓑蓓蓖蓙蓚蓜蓧蓪蓬蓮蓯蓰蓱蓲蓴蓷蓺蓻蓼蓽蓿蔀蔂蔃蔆蔇蔌蔎蔐蔑蔓蔔蔕蔗蔘蔚蔜蔞蔟蔡蔢蔣蔤蔥蔦蔧蔪蔫蔬蔭蔯蔲蔳蔴蔵蔶蔽蔿蕀蕁蕃蕆蕈蕉蕊蕋蕎蕏蕐蕑蕒蕓蕕蕖蕗蕘蕙蕚蕜蕝蕞蕟蕠蕡蕢蕣蕤蕨蕩蕪蕫蕭蕯蕷蕹蕺蕻蕽蕾蕿薀薁薄薅薆薇薈薉薊薋薌薏薐薑薓薔薗薘薙薛薜薝薟薠薢薤薥薦薧薨薩薪薫薬薭薮薯薴薶薷薸薹薺薼薽薾薿藁藂藇藉藊藋藍藎藏藐藕藘藚藜藝藟藠藤藥藦藨藩藪藭藳藶藷藹藺藻藼藾藿蘀蘂蘄蘅蘆蘇蘊蘋蘍蘎蘐蘑蘒蘓蘖蘗蘘蘙蘚蘛蘞蘡蘢蘧蘩蘭蘯蘰蘶蘸蘺蘼蘽蘿虀虁虂虆虍虎虐虒虓虔處虖虗虘虙虚虜虝虞號虠虡虢虣虤虧虩虱虺蚈蚊蚖蚘蚜蚧蚩蚫蚯蚱蚶蛎蛒蛕蛗蛛蛜蛧蛩蛬蛮蛻蛾蜀蜂蜆蜉蜊蜏蜑蜒蜓蜔蜘蜜蜞蜣蜮蜱蜲蜴蜷蜹蜺蝋蝌蝎蝙蝤蝥蝨蝪蝮蝯蝶蝻蝿螃螄螆螇螉螌融螓螕螘螠螣螧螫螭螮螯螽蟄蟆蟇蟈蟉蟊蟋蟒蟕蟖蟙蟚蟜蟟蟠蟢蟣蟯蟱蟶蟹蟻蟾蟿蠃蠆蠊蠋蠍蠎蠏蠐蠒蠓蠖蠘蠚蠛蠜蠟蠣蠨蠭蠮蠲蠶衂衃衄衅衉衊術衘衜衝衡衫衱衽袀袍袗袘袚袟袠袢袤袮袴袵袾裁裋裎裓裔裙裞裟裨裷裼製複褊褎褐褚褜褨褫褰褵褹褺褻襂襆襏襒襖襗襚襜襞襡襣襪襮襯襰襳襴襷襺襻襼襽覆覇覈覉覊見覍規覐覓覔覕視覗覘覚覛覜覟覠覡覥覦覧覩親覬覯覰覲観覴覵覶覷覺覼覽覿觀角觔觕觖觗觘觚觜觝解觥触觧觩觫觭觱觳觴觶觸觹觽觿訄訅訇訊訒訖託訝訞訤訦訬設訯許訴訵診訽詍詐評詖詜詡詢詫詬詭詳詵詹詻詾誂誄誅誇誉認誐誒誕誗誘誟誠誡誥誨説読誮誷諈諉諊諌諍諓諕論諜諞諡諤諦諫諬諮諴諵諶諷諺諼諾謀謁謄謅謆謊謐謑謔謖謗謙謚謟謡謦謨謫謬謭謷謹謼譁譃譄譆譈證譌譎譏譒譖識譛譜警譫譬譭議譱護譼譾讀讁讃讅變讌讒讔讕讖讙讚讜讞谸谹谺谿豆豇豈豉豊豋豌豎豏豐豑豓豔豗豘豙豛象豢豣豦豨豫豶豹豺豾貉貌貎貓貔貘貛負財貰貶貺費貿賁賂賃賄賅賆資賊賎賓賚賛賜賡賤賯賸賺賿贁贄贅贇贈贊贍贏贓贖贛赥赦赩趁趂越趑趦趨趫趬趯趲趵趷趺趼跇跈跋跌跑跔跚跡跣跥跨跪跫路跰跳践跼丂七踄踆万踈丈丌不与踐丑踑丒丕世丗丘丞踟踡踢丣並丫丯丱踵丵踷丸踸丹丼踽丿蹀蹁蹂乂乃乄蹄久乇蹈蹉蹊蹋蹌乍乎蹏乏乑乕乖乗乘蹙蹛九乞蹠蹢蹣蹤蹩蹬蹭蹯蹰蹲乳蹴乴蹶蹺蹻蹼乾乿亀亂躂躄躅躇躉争亊躋躐躑躔井躕亙躝亢亦躪躭亮躯躱亳躳躵亹躻躾軄軆仇介軌仍軏軔仗仛仟軟仡仢軫仭仯軱仵价任軼軾仿輅載伉伊伋伐輐輓伖輗輘伜輝輞輡輦輧輪輮輳伳伴輵輶輷輹伾輾轁轂轃轇佈轍轎佐佑轑轒轓轕佖轗佚佛作佝轞佟你轣轤轥佩佬佯併佹佻佽使侁侂侅來侉侊侏侑侖侘侚侟辟侠辣辤辥辨辭侮侯農辵侶侷辸侻侾便俀俁迁係俄迄俈俊迍俐俒迒迓俔迕俘俛俜俟俢迣俤迤迩迭修迮俰述迶迸俸迹追俽俾俿倁送逃逄倄逅逆逈倉倎倏透倐逓逗逘候倛借速造逡逢倣倥逩倪倫逬倭倰逵逶逷逸逿偀偁遁遂偂遃偅遅偈遊偊偌遌遍偏遏偐偑偒遒道達遖偗偙做遜遝遞遡偢遥遨適偬遬遭遮遰偰遲遵遶遹遼遽偽避邀傀邁傁邂邃傄邇邈邊邋邌傍邎傒傓傔備傚傜傞邢邥邦邨邪傪傯邯邱傲邲邳傷傹邾僀郁僃郄僅郇僇僌働像僐僑僔郕僕僖郗僘郛郜僜僞僟郡郢僢僣僥僦僧僨郫僭郵僻僾儀鄀儂鄂鄅儆儇儈鄈儋儌儍儎儐鄒儕儗儚鄚儛儞鄞鄠鄢鄧優儭鄭鄮儯鄯鄱儳鄴儵鄶鄷儹鄹儺儼鄽儿兀允兂元兄充兆兇酇先光兊克酋兌酌免兎兏児兒兓酔兔兕酖兗酗酘兘党酛兜兟酢兢酣兤酥酪酭酲兵其酷酸典酹酻兼兾冀醆冊醋册醎冏醓醔冕醗冗冘写醜冠醡醢冤醤冦醦醨冩冪醪醫冬冭醭醱决醳醴冴醵况冸醺冼凂釂釅釆采釈凈釉釋凌凍重野减釐凑凒凘釚釜凜凝凞釟几凡釡凢釣釤凥処凧凩釩凪凭凮釮凰釰凱凲凳凴凶釻釼釽刃刄刅切刈刋鈍刎鈎刑划鈒刓鈓鈔鈕刘鈜鈞判別鈨刨利刪刱刳制刷券刹鈹刼鈼鉂剃鉃鉄剅鉇剋鉋剌鉍前剏剔剗鉗鉘剘鉚鉛剛鉞鉡剤鉤鉥鉨剩剪剰剱剳剴創鉶剷剹鉻鉼劀劂銃劅劇劈劉銊劊劌銎銑劑劓劔銕銖劖劗銗劘銙銚劚銠劣劥銧銫劬銭励劷銹銻勀鋀鋂勄勅鋆鋈勉鋋鋌鋎鋏鋐鋒勒鋓動勘務鋝勝募勠勢鋣勤鋧勧鋨勨鋩勩勪勬鋭鋮勱鋲勲勳鋳勴勵勶勸勹勺鋼勾勿錀匀匁錂匂匃包匆匈錈匊匋匌匍錍匏匐匑匓錘匘錚匛匟錢錤匥錨匩錫匫錬匭錯匲錵匵匹錺区錻医匾匿千卅卆升鍈午卉半鍍鍐鍑卑鍒鍔鍖南鍗卙鍚鍛卛鍤卥鍧鍩鍪鍬鍭鍯卯鍰印危鍱鍳卵卸鍺鍼鍽鍾卾卿鎀鎂厃厇鎈鎊鎋鎌鎏鎔厖鎗鎘鎚鎞鎡鎤厤厥厦鎦鎧厨厩鎩厪鎫厫鎭鎮厮厰鎰厲厳厷鎹参參鏃鏅叅鏇鏉鏊及友鏌鏐鏑鏓叕受叙叛鏟鏡叢句鏧叱史右号鏷鏺鐁吃鐃各鐇鐈鐍后吏鐏鐐向鐓鐕鐖鐙吚君吝鐡否吨吪吭吮鐮鐯鐲鐵吵吸吹鐻吻鐽呀鑁呂呄鑈鑊告鑌呏呑鑙鑚鑞鑢鑪呪鑭呭鑮鑯鑰鑱呱味呴鑵呼鑽鑿咁钃钄咆咈咋和咎咏咒咢咤咨咩咫咮咯咲咷咸咿哉哊响哎哠哢哦哯哽哾唀唆唉唌唍唎唏唪唫唲唾商啇啌啓啗啝啣啤镹啻啼镾啾啿喀喃善喇閉喉喊喋開閌閎喏喒閔喚喜喝閞閟閡関閣喣閥喨喬閭喭閲喲営閺閻閾閿嗁嗄嗆闊闋闌嗌嗒闒闓闕嗘闘闙嗚闚嗛關闝闞嗞嗟闢嗣嗤闥闦嗷嗽嗾嘅嘆嘎嘐嘑嘘阜阡阢嘩阬阮嘯嘰嘱阱嘴嘵嘶嘸嘹嘻阼嘿陁噂噃噆噉噋噌降噎陏陒陔噔陖陜陞陟噠噡院噢噣除陥噦陦噭噯噱陲噲陳噴陴陵噵陶陷陸噸噺陼陽嚄嚆隆嚊隊隋嚌随隑隔隗隘際嚟嚠隠嚥隥嚦嚧隧隨隩嚫嚬嚭嚮隮嚱隱隲嚳隳嚴嚼嚾雀雄雅囈囉雉雊囋囌囍囎囏囐囑雑雒囓雘雚雛四雞囟雟離難囤囥囨雩囫园雯囱囲図囷雹雺囿圀霃霆圇圈霉國圏霓霙霚圛霤圥霧在霪圪圬霰霱露霳圴霸霹圽霽圾圿靂靄坆均靉靊坎坏坑靗静靚靜靠坢靣靤靦坧坩革坪靪靫靭靮坯靱靳坴靴靶靷靸靹靺靻靼靽靿鞀垁鞁垂鞄鞅鞆鞉垉型鞋鞍鞏鞐垕鞕鞖鞗垗鞘垙鞙鞚垜鞜垝垞鞞垟鞟鞠垡垢鞢鞣鞦垧鞨垩垪鞫鞬鞭鞮鞱鞲鞳鞴鞵鞶垸鞸鞹鞺鞼鞾鞿埀韁埃韃韄韅埆韆韇韈埈韉韊韌城埏韑埒埓埕埖韘韛韜域埠韡韤埤埩韮韯埰韱韲韵埵埶執韸埸韺基堄須堈堍頏預頑頓堕頖堞頠堠頦堪頫頬頭頮堯頯頰頲場頵堹堺頻頼頽頾堿塀塉塊顊塌額塍顎顏塏顑塑塔顔顖顗願塟塢顣塧塨顪顰塰顱塲顴塾墀境墅墈墊墍墐墓増墜墝墟墡墢墦風墩颪颫墫颭颮墮颯颰颱墱墲墳颴颶颷墸颸墺颺颻墼墾颿壁壂飂飃飄壄飅飆飈壈飌壎壐壒壚飛飜飢壢飥飩壩飪飫壬飮売飲壳飶壹飽飾夂夅餅夆餈変餉夊養夋夌复夏夐夒餒餓夓夔餕餖夙餚餝餞餠夢餧夫夭餮央夰餱失餲餳餴夷夸餻餽餾夾饀饈饉奉饍饎奏奐饐饑饒奔饕奕饙奚饛饞奠奡奥奧奨奯奲奶奼妁妊妋妌妍妎妒妖首馗馘妙香馛馝馟妤馥妥馦妧馨馰妰馲妳馵妷妹馺馼妼駁姁駆駈姊姍駒姒駓委姙姚駛駜姜姝駞駟駢姤姨駪駫姯駱姱駲駴駵姷駹駾駿姿娀威騁騃娉娍騏娑娒騖騙娞騣娣騤娥娧娩娭騭騮娰騰騶騸驀驁驂驄婅驇婇婈驊驋驍驕驖驚婞驢婢驥驩婬婺婻婾媒媖媙媚媛媟媠骪骯骰媱骲媲媵媸媺媻骻骼骾媿骿髀髄髈嫉嫌髎髐髑髒髓體髕髖髗髛髜髟嫠髠嫡髢髣髤髥髦髧嫩髩髪嫪髫髬髭髮嫮髯髱髲髳髴嫵髵髷髹髺髻髽嫽髿嬀鬀鬁嬁鬂鬃鬄鬅鬆嬈鬈鬉嬉鬋嬌鬌鬍鬎鬐鬒嬖鬖鬘鬙鬚鬛鬜鬟鬠嬡鬢鬣嬥鬩嬪鬪鬫嬭鬮鬱鬲鬳鬴嬴鬵嬶鬷嬸鬹鬺鬻鬼鬽嬾魁魂魃魄孅魅魈魋魌魍魎魏魑魔魕孕魖魗魘存孚孜魡魣季孤魥魦魨魪魫孫魭孰孳孵魷學魹孼孽鮃宄鮄宅鮅完鮍宎宏宐鮑宑鮓宓鮗鮝鮠客宥宨鮩鮪宬鮬宭宮鮮宯宱鮱宷鮷鮸宺鮻宼鮾寀鯀鯁寃密寇鯇鯈鯊鯎鯏寏寐鯑寒寗鯗鯘寙寛寞察鯢鯣鯥寥審鯪寫寮鯱寳寴鯵鯸鯺寽対鯿寿鰀鰄尅将鰈尉鰊尊鰌尌鰍對導鰐鰑少鰒尓鰔鰖鰙鰜尞尟尠鰡尢尣尤鰤尦鰦鰧尨尩鰩尫尬尭尮尰就鰲尲鰵尵尶鰷尹鰺尻局鱁鱃鱉鱊鱎屏鱒鱔展鱖鱚鱛屜鱝属鱟層履鱧屧屩鱪鱫屬屮屯屰鱰鱲鱵鱶鱸屹屻屼岇岈岌岏岝岡岣岪岲岳岼峅峉峋峗峡鳧峨峩鳩鳬峮峯峰峲鳳峴島峺峻峽崁鴆崆鴉鴎崗崘鴘崙崚鴝鴞崢崣崤鴦崧崴崹鴼崿嵃鵅嵆鵇嵈嵊嵋嵌嵐嵑鵔嵕嵙鵜嵜鵝鵞嵠鵠鵡嵡嵢鵤鵥嵬嵭嵯鵯嵰鵲嵳嵺鵼嵾鶃鶇鶍鶏嶐嶒鶓嶓嶔鶖鶚嶛嶝嶟嶠鶡嶢嶧鶩鶫嶫嶬鶬嶰鶱嶴鶵嶷鶹鶼鶿鷁鷄巇鷇鷉巉鷊巋巌巍巎巐鷓鷔巖鷖巘巙鷙鷚鷞鷟巤左巧鷧鷩巩鷭差鷮鷯鷲巵巷鷸鷺巻鷽鸂布帆希鸒帔鸕帘鸙帙鸛帝帟帥帨師席帮帯帲帶帾幉幋幌幐幑幕幗幞幟幡幣幤幨幪幭幮平年并鹹鹺鹻幾麁麂麅麇庎麏麑麒麕庖麘庠麥麦度麨庨麩麪麬庬麭庭麮麯麰庱麳庳麴麵庶麸麹麺庿麿廃黆廆廈廉黋黌廌黍黎廏黏廐黐廑廒黕廖廚廛廜廝廞黟廠廡廢黤廥黧廨廩廫黬廬黭黮黰黱黲黴黵延黷廷黸黹黻黼廾黿廿弁异弃弄弆鼇弇鼈弈弉弊鼐弑鼒鼓鼔鼕鼖鼗弗鼙鼚鼛鼟弟弢弥弧弮弯鼷鼻鼼弼鼽弽鼾鼿彀齁齃齄彅齅齆齇齊齋彌齎齏齕齖彘齘齚彛彜彝彠彡形彣齣彤彦彧彩彪彫彬彭齭齯彯彰影齲彲彴齶役齺彿徇徉後龏徏徐従龜龝龞徠龠龡御龢龣龥徧復循微徯徴徹徼徽必忍忔忝忞忤忨忬忰忱忳忸忺忼忽怍怎怏怐怖急怦怫怭怱怳怵恁恂恆恋恌恍恐恑恖恗恙恝恟恠恡恣恪恭恱恾悈悉悋悌悎悓悔悕悗悘悚悛悞悠悤悦悧您悩悸悻悾惂惑惓惕惘惚惜惞惣惨惰惱惵惸惹惼愀愁愌愍愎愒愓愕愖愗愙愛感愡愧愨愬愰愱愴愷慆慇慈慊慌慕慘慝慟慠慥慨慫慬慮慯慰慶慸慻慼慾慿憀憁憂憇憊憋憍憎憖憗憘憙憜憝憠憤憨憫憭憮憺憼憾懀懂懃懇懈懊懋懍懜懝懞懟懡懢懣懩懯懲懴懶懸懺懽懿戁戃戇戈戉戊戌戍戎成我戒戓戔戕或戚戛戜戝戞戟戠戡戢戣戦戧戩截戫戮戯戰戲戳戴扁扄才扔托扚扤扨扱扳扶扽找承抄抍抏抒抓抔投抗抛抜抨抱抶抷抹拂拆拌拑拔拖拘拚拜拠括拯拲拳拵拷拼拽挄挌挑挓挖挘挟挨挩挭挵挺挼挽挾挿捁捃捊捋捌捓捔捗捘捧捭捲捶捼掀掄授掏掐掕掖掙採探掣掦控措掬掲掽揀揃揅揈揉描插揓揔揕揚換揜揥揪揬揲援揺搊搒搓搗搘搠搢搤搥搬搭搯搶携搽搾摋摎摑摒摓摔摘摛摠摡摣摯摴摸摻撃撅撇撐撒撓撕撙撝撟撤撥撦撩撫撬播撲撳撹撻撼撽擄擉擊擎擑擒擔擘據擠擤擥擦擬擭擯擱擲擶擻擾擿攀攁攄攅攈攊攔攙攛攜攟攢攤攩攪攬攴攵收攷攸改攺攻攼攽放政敃故敇效敉敍敏敐救敒敔敕敖敗敘教敝敞敟敢散敦敫敬数敲整敵敷數敽斁斂斃斅文斈斉斊斌斎斐斑斒斕斛斜斟斣斥斫斯新斲斳施斿旁旂旃旅旈旎族旐旖旗旘旙旛旟无旡既旬旭旻旾旿昀昄昇昒易昔昜昝映昣昦昧昨昪昫昳昴晀晁晃晄晌晑晙晛晜晞晟晠晦晧晩普晵晷晸晹智暁暋暍暎暖暘暚暛暟暢暭暮暱暴暵暸暼暾暿曀曁曄曉曎曔曖曚曛曝曦曫曮曲曳更曵曷曹曽替朅朇有朓朔朕朙朞期朠朦未末朮朱朶机朽杇杉杌材杓杔杖束条杤来杦杪杬杭杯東杴杵杶杼极构枅枕枖枚枛枠枡枢枰枵枸枹枻枼柀柂柉柊某柑染柔柝柞柧柩柬柲柳柵柶柷柹栃栙栟栧栨株栫栬栯栰栲栳栴格栽桃桄桅桊桍桒桗桘桟桫桮桴桵桷桺桻桾梃梅梆梍梏梔梗梘梚條梟梠梡梨梪梭梯械梲梵梹梻梼棄棅棆棊棋棌棒棘棟棡棥棧棨棫棬棭棰棱棻椈椌椑椛椣椥椦椪椱椴椶椸椹楉楊楓楕楙楛楝楠楢楤楥楩楪楬業楯楲楳楴楸楹楺楿概榎榖榘榛榜榥榨榭榴榺榿槃槅槊槌槍槎槐槗様槝槢槭槮槲槻槽槿樀樁樅樊樑樒樕樗樛樝模樣樤樨権横樴樸樹樺樽樾橄橅橆橈橋橎橑橕橘橙橛機橡橢橧橱橲橳檁檃檄檋檎檐檗檝檞檠檥檬檳檴櫁櫆櫈櫉櫌櫔櫖櫚櫜櫝櫤櫧櫨櫪櫬櫰櫱櫲櫼櫽欂欃欄欅欉權欑欖欗欛欝欟欠次欣欤欧欨欫欬欯欲欵欶欷欸欹欺欻欽款欿歃歆歇歉歊歌歍歎歐歒歓歔歖歘歙歛歝歟歠歡歩歪歰歳歾歿殀殃殄殉殊残殍殘殢殣殤殪殬殯殰殱殲殳殴段殷殸殹殺殻殼殽殾殿毀毃毄毅毆毉毎毓毖毚毟毡毣毦毧毮毱毷毹毿氂氄氅氉氍氎氒気氟氦氧氿汋汍汎汔汛汧汫汯汲汶汹汻沃沅沆沇沈沉沌沕沙沟没沫沲沴沸況泄泆泌泒泔泖泗泙泛泝泡泧泮泱泲洊洋洗洙洚洛洟洧洩洮洯洵洶洸活派洿浄浅浚浜浟浣浤浩浮浰海浹浼涎涒涕涗涘涛涜涬液涹淆淇淊淕淘淠淢淨淩淪淫深淴淵淺添渀渇済渉渊渓渕渙減渞渡渢渧渫渮港渶渹渺渻渼湃湅湈湊湔湛湫湯湲湳湾満溂溌溓溜溠溢溪溭溮溯溱溿滂滃滄滅滉滋滌滏滔滕滞滫滭滮滯滲滴滸滹滻滿漌漍漑漓漕漖漛漠漢漦漩漭漱漻漾潅潏潑潒潘潙潛潜潞潟潡潦潨潽澂澃澄澆澈澌澍澎澓澔澚澟澥澦澧澮澱澳澹激濁濂濃濆濈濊濔濛濞濟濨濩濫濮濱濳濹濺濾瀀瀁瀆瀉瀋瀍瀏瀑瀕瀘瀛瀝瀞瀟瀬瀰瀲瀹瀾灃灊灌灎灔灕灘灞灮灸灼炁炆炊炒炕炖炛炮炰炸為烊烏烑烕烖烙烝烤烱烽焄焅焌焔無焫焭焰焼煁煅煉煎煐煖煞煠煤煥煦煬熀熈熏熟熢熨熬熯熱熹熺熾燁燄燈燉燌燎燒燓燔燕燙營燠燧燬燭燵燻爆爇爈爉爐爓爗爚爛爝爟爤爪爫爬爭爯爰爲爴爵爻爼爽爾牂牃牋牌牎牏牐牒牓牕牘牙牚牣牧牨物牫牮牻牿犀犁犂犓犛犠犢犧犮犱犲犹狁狃狉狐狒狗狘狢狥狭狳狹狻猇猊猘猙猟猤猨猫献猱猲猴猵猶猷猺獃獅獍獏獒獖獗獘獝獟獠獦獨獩獬獮獯獱獲獵獹獺獻獼玁玅玓玖玟玠玩玫玵玽珊珋珌珍珎珖珞珠珣珦珧珮珴珵珹珺珻珽現琁琇琊琛琤琥琪瑀瑍瑒瑗瑙瑛瑟瑠瑧瑨瑫瑰瑳瑶瑾璁璃璆璇璏璐璑璒璙璚璞璠璣璦璧璩璯璱璲璻璽㓁瓈瓊瓌瓐瓓瓘瓚瓛瓜瓞瓟瓠瓢瓣瓤瓩瓪瓯瓲瓶瓷瓻甃甆甍甑甒甓甗甘甚甜甞産甦甪甶甸甹畀畃畇畈畉畊畋界畍畔留畛畝畞畟畡略畧番畯異畴畻畼畽畿疁疎疑疓疙疚疣疥疫疱疳疹疺疼疾疿痀痄痍痎痏痒痗痘痜痞痠痢痧痬痲痴痹痺痿瘇瘊瘋瘍瘏瘓瘛瘝瘡瘢瘣瘤瘥瘧瘩瘳瘹瘼癁療癃癈癖癘癜癟癡癢癥癧癩癪癬癮癱発登發皃的皓皖皚皝皢皣皤皥皦皧皭皰皴皷皺皽盁盃盈盉益盔盖盗盛盜盞盤盧盨盪盬盶盹盻盾省眇眈眉眊看眔眕眗眛眨眴眷眹眺眽眾着睆睇睍睎睖睗睜睠睡睤睥睦睨睪睬睰睺睽睾瞀瞄瞖瞞瞢瞥瞪瞬瞭瞮瞰瞱瞹瞻瞽瞾矇矉矑矒矙矚矛矜矞矟矠矢矣矤知矦矧矩矪矬短矮矯矰矱矻砂砉砌砍砎砑研砕砭砰砲砺硃硄硇硌硎硡硣硪硬确硯硴硾碁碊碑碟碣碤碪碭碯碰碱碲碳碾磁磅磇磈磋磎磐磑磓磟磠磡磤磬磯磳磴磶磻磽磿礇礐礒礙礚礜礞礟礠礪礬礴礽礿祄祅祆祊祋祐祑祓祔祕祚祛祜祝祢祥祧祩祫祲祷祹祺祼祾禋禌禓禔禕禖禘禛禝禡禦禧禨禩禫禮羚禰禱禴禹离禽禾禿秀私秂秄秇秈秉秊秋秏科秒秔秕秖秘秚秝秞租秠秡秢秣秤秥秦秧秩秪秫秬秭称秱秸移秼稀稂稃稇稈稉稊程稌稍税稑稔稕稗稘稙稚稛稜稞稟稠稡稧稫稭種稯稰稱稲稴稵稷稸稹稺稻稼稽稾稿穀穂穃穄穅穆穇穈穉穌積穎穏穐穕穖穗穙穜穝穟穠穡穢穣穥穧穩穪穫穭穰穵究穸穹空穽穾穿窀突窂窃窄窅窆窈窊窐窒窓窔窕窖窗窘窞窟窠窣窪窬窮窯窰窳窵窶窹窺窻窼窿竃竄竅竆竇竈竊竌竎竏竑竒竕竟竢竣竦竫竬竭竴競竸笂笄笈笊笋笏笑笔笟笣笧笫第笮笰笱笶笹笿筅筍筏筑筝筠筥筦筧筬筭筰筱筳筴筵箄箋箎箏箕算箘箛箜箝箞箠箥箬箭箯箰箲箴箵箺箽篇築篋篌篏篠篦篩篪篷篸篺篼篾簂簃簆簇簉簌簒簓簛簥簦簪簫簬簱簴簶簷簸簺簾籀籆籍籏籐籓籔籕籖籘籚籛籜籝籞籟籣籤籥籧籩籬籲籾粁粂粆粇粉粋粛粢粤粰粱粳粷粺糀糂糄糅糇糉糍糒糓糕糙糚糝糞糟糢糦糩糲糵系約紇紈紉紊紋紐紑紒紓純紗紘級紝紞紣紱紲紺紼紾絁終絆絇絍絏絑絖絛絝絡絢絣絨統絳絶絸絺絻絽綁綂綆綈綉綋綏綖綗続綛綞綣綦綫綬綮綯綱網綵綸綹綾緆緌緎緕緘緙緜緞締緢緤編緩緫緱緲練緵緶緻縄縅縊縋縐縑縒縠縢縣縦縧縨縫縬縭縶總縿繁繄繆繇繊繋繍繒織繕繖繘繙繚繞繥繫繭繳繸繽纁纂纃纆纉續纎纏纑纔纖纘纛纜缺缼缾缿罄罅罇罏罐网罓罔罕罘罛罤罦罭罱罾羀羃羇羈羊羌羍美羏羐羑羔羖羗羚羜羝羞羡羢羣群羦羨義羪羭羮羯羲羴羶羸羹羼羿翃翆翎翏翔翛翟翣翥翦翩翫翬翮翯翲翳翹翺翻翼翽翾翿"));
        PART_TO_KANJILIST.put('阿', new Part(1, "婀痾阿"));
        PART_TO_KANJILIST.put('乂', new Part(1, "瀀逄蠆蠎倏倐蠒蠓蠖栙蠚蠛蠜瀟逡逢倣蠣蠨蠭瀰倰耰瀲格偀衂衉衊灌偌硌額顏偐桒塔灘做恟塟遨恪硬聰聱硴顴桻恾邀邁炆邇肇悋墓梗備傚悛條悠碤悤悩墩梭碯傯傲墳肴墸僃僅飌僌壐壒僘烙棥僨棨棭胮惱棱胳胸惹棻烽僾鄀夂夅儆夆複変夊夋夌焌儌愍儍复愎夏礐夐鄒夒夓夔餕愗愙儚礚鄚椛愛餝礞礟夢脧脩礪優焫礬椱脳礴儵椶儺儼兇饉楉慌煐襒慕饙楛饛慝煞慠腦襪酪慶酸腹襺襻襼襽祾憁憂覆覈憊憋榎覐膖憗膜禝憝熢憤馥冦憨冪憫冬熬禰覲膵覺离覼憼馼禽觀駁燁懂懃釅駆臈燉燌凌釐燓臓燕臗臘臙駛懜懞臟処懯駱懲致臵凶釼懽槿駿稀刄爇戇刈樊舊騖爗刘稜爟模騣訤爤樤稯稷刹樺爻爼爽爾驀驁橄驄婈驊剏牕驚剤牧艧扨艨驩穪穫剱艱剳艸婺艻詻鉻艽艾艿芀芁芃芄檄芇芉芊芋芍芎檎芑芒劔芔媖芖芘芙芚芛犛抜芝芟檠芠芡芣芤芥芦芧芨芩芪芫檬芬芭芮誮芯芰花芲芳檴芴芷芸芹芺媺芻窻芼骼芽芾芿窿苅竅苆櫌苐苑苒髒鋒苓苔櫔苕髖苗苙務苚苛苜櫜苞募苟苠嫠拠苡苢狢苣竣勤苤若苦苧苨鋩嫩苪髪苫苭髮苯英勱苳苴勵苶勶苷勸苹苺苻狻苽諾苾茀茁匁茂欂錂范茄茅茆茇茈匈茉鬉權茊謊茋挌茎茖謖茗欗茘茛匛茜欝茝茞欟茟茡嬡茢茣鬣欧錨茨謨猫茫鬫茬嬭茭茮茯茰茱茲茳茴錵茵茶欷茷鬷謷茸謹茹錺茺区茼茽匿笿荀譁荂荃魃荄荅荇鍈譈草荊荍歎荎荏獏荐鍐荑鍑魑荒獒荓荔荕獖荖荗荘捘獘孜歡警獦鍩鍪獮荰筱獲孳荳荵獵荷護學荸荻獼荼孽荽荿莀鎀玁莂莄莅莆莇莉莊變讌莍莎莒莓莔莕掕莖掖鮗莘讙莙莚莛莜莝莞莟玟莠莢客莦厦莧莨莩莪莫鎫厫玫箬莬莭厰莱厲厳殴莵殺鮻莽莾莿菀菁菅鏅菇寇鯈菉菊鏊菌鏌菎描菏鯑菑菓鏓菔菖菘寛菜菝寞珞菟菠篠篦菨菩菪鯪菫華菰菱菲史菴菶菷篷菸菹菻菼菽対萁氂萃萄各氅萆萇萊萋萌萍萎吏萏萑鰒鐓萓萕気萙鰙吝萠萢萩萪萬簬搭萯鐯萱鰲萵鰵鰷萸萹萼落搽鑁葅葆籆葇鑈葈葉鱉葊鑊葍葎葏葑葒摓籔葖著葘瑙葙葚瑛葛葜鱝鑞鱟葠摠葡葢董葤葥履葦葧葩葪葫鱫籬葬葭葮鑮葯葰葱葳葴葵鑵葶豶汶葷摸葸葹葺葼葽瑾蒁璁蒂璃蒄钄蒅撇貉蒋咎蒐璐蒒撒蒓貓蒔貔蒕貘蒙貛蒜蒞蒟蒡撤蒦璦蒨蒩蒪蒭蒯咯蒱蒲粳蒴蒸蒹蒺蒻蒼璽蒽撽蒾蒿糀蓀蓁賁蓂賂蓄蓆蓇蓈蓉峉糉蓊瓊蓋蓌蓍哎擎蓏蓐蓑糒擒蓓蓖瓘蓙蓚糚蓜糢蓧蓪蓬擭蓮蓯峯蓰峰蓱擱糲蓲蓴糵蓷蓺峺蓻擻峻蓼蓽哽擾蓿攀蔀贁蔂蔃贅蔆唆蔇贇紊紋蔌甍蔎鴎唏蔐蔑蔓蔔蔕蔗蔘蔚崚洚洛贛蔜蔞蔟蔡蔢蔣攤蔤蔥蔦甦蔧攪蔪蔫蔬蔭蔯蔲蔳蔴攴蔵攵蔶洶收攷攸改攺攻鴼攼蔽攽放蔿政蕀蕁終蕃敃鵅故蕆敇蕈效蕉敉蕊蕋畋敍蕎蕏敏蕐敐蕑救蕒敒蕓啓鵔敔蕕嵕敕蕖敖蕗敗蕘敘蕙教蕚浚絛蕜蕝敝蕞畞敞蕟畟敟浟蕠蕡絡蕢敢蕣散蕤略赦敦畧蕨蕩蕪蕫敫敬蕭蕯畯数敲絳整敵蕷敷數蕹蕺蕻蕽镾蕾蕿薀喀薁斁斂斃薄斅薅薆薇文薈斈薉斉薊斊薋薌斌斎喏薏薐嶐斐薑斑喒薓鶓薔閔薗薘薙薛薜薝薟薠薢閣薤薥薦薧趨薨薩鶩薪薫薬薭薮綮薯液薴薶薷薸薹綹薺閺薼疼薽薾綾薿藁藂嗄淆藇藉藊藋跋巌藍藎巎藏藐嗒鷔藕緕巖藘巙闙藚藜藝闝鷞闞藟藠痠緢藤藥藦藨藩淩鷩藪藭路藳緵藶藷嗷藹藺鷺藻緻旻藼鷽藾藿蘀蘂蘄蘅嘆蘆踆蘇丈済蘊蘋希蘍蘎蘐蘑蘒蘓蘖蘗蘘蘙蘚鸛蘛昝蘞蘡蘢蘧縧嘩瘩蘩縫蘭渮蘯蘰渶蘶蘸蘺瘼蘼蘽總蘿虀繁虁癁乂虂癃虆噉噋降幑虔幕處繖陖癘晙晞幣幤癥蹩幪繭幭噭噯繳噴晵陵晷晸嚄嚆隆躇躉蚊暋暎皣嚥麥皦麦皧麨麩躪麪麬麭溭暮麮躯麯麰暱庱嚳麳嚴皴麴麵麸麹皺麺暼嚾暾庿曄囈廈黌囌滌黐廒蛒雒曔曖仗雘曚雚雛廠離難滫蛬仭滭曮雯囱図更黴黻蜂輅鼇鼈弊弑漓鼕鼖漖輘霙霚鼛漠朠漢朦漦霧漭霰露霳蜹輹坆靉彌轍罐网轒轕杖睖潞佟彠条潡彣轥蝥杦蝮睰靱使瞀澂羃瞄澈後羐羑澓鞗枚龞瞢枢瞥復螫鞭微瞮螯瞰瞱鞳鞴徴瞹徹徼螽徽鞾便激韄蟆濆蟇矇韈埈韉韊俊柊蟒矒濔埖矙濛韛翛忞韡俢韤濩韮修矱韸韺"));
        PART_TO_KANJILIST.put('時', new Part(1, "時塒蒔"));
        PART_TO_KANJILIST.put('乃', new Part(2, "唀极琇笈綉伋岌透蜏鼐鈒疓誘級垜莠芨尮訯躱朶吸辸銹馺携礽圾芿蔿秀盁乃呄盈及仍扔孕桗魥跥扱衱汲奶靸楹艿"));
        PART_TO_KANJILIST.put('晃', new Part(1, "晃滉幌"));
        PART_TO_KANJILIST.put('久', new Part(3, "粂久玖灸柩镹疚畝"));
        PART_TO_KANJILIST.put('乇', new Part(1, "咤宅乇託侘詫"));
        PART_TO_KANJILIST.put('晉', new Part(1, "晉縉"));
        PART_TO_KANJILIST.put('之', new Part(1, "貶之泛芝乏"));
        PART_TO_KANJILIST.put('虍', new Part(6, "攄蘆猇爈爐嘑謔鸕甗嘘瀘樝琥瘧蘧褫戯戲鬳簴鐻謼譃罅齇虍虎罏蹏虐虒虓歔虔饕處虖魖虗虘虙虚籚虜虝遞虞號虠虡鑢驢虢摣艣虣轤虤豦虧籧虩彪鑪艫慮顱噱鱸獹獻遽劇箎纑膚讞墟嚧璩覰嚱醵覷嗁擄巇鷉囐瓐矑諕櫖髗巘藘據臚壚瓛盧櫨篪廬滮鯱勴黸滹濾俿"));
        PART_TO_KANJILIST.put('乍', new Part(1, "酢窄昨咋乍怎筰詐鮓炸胙祚作搾柞"));
        PART_TO_KANJILIST.put('虎', new Part(1, "鯱琥饕彪褫虎遞號"));
        PART_TO_KANJILIST.put('乎', new Part(1, "罅呼乎"));
        PART_TO_KANJILIST.put('乏', new Part(1, "貶泛乏"));
        PART_TO_KANJILIST.put('陏', new Part(1, "堕陏随"));
        PART_TO_KANJILIST.put('虐', new Part(1, "虐謔瘧"));
        PART_TO_KANJILIST.put('乕', new Part(1, "逓乕"));
        PART_TO_KANJILIST.put('幕', new Part(1, "羃幕冪"));
        PART_TO_KANJILIST.put('乖', new Part(1, "埀乖"));
        PART_TO_KANJILIST.put('乗', new Part(1, "剰乗"));
        PART_TO_KANJILIST.put('乘', new Part(1, "乘剩"));
        PART_TO_KANJILIST.put('乙', new Part(1, "氂吃蠃氄簄氅氉砌堍氍氎頓砕瀘萙耙琥吧栧吨吪尬栬砭尮尰尲簴耴尵怵尶琶鐻尻吼氾氿豅灉汍汔灔豘籚汛豝遞池鑢摣豦籧鑨葩鑪摯屯遰顱鑱鱲鱸屹葹遽塾咃蒅沈墊岊粋邋沌梍墍邑悒蒓梔邕炖袘炛墟咤貤邥肥邨璩肫撬沲貶僀擄郌瓐郒據壚瓛泛裛胤飥鳦瓨鳩飩瓪瓫瓯瓲瓴郶蓺糺瓺蓻瓻瓼惽瓿脃攄贄甆紇紈唈攏甒純甖甗紞紣崦紦洩甩褫紮礱儱儳唵椸褹褺褻礼夿絁兂敃奄腌酏絏酔饕奙蕝饞煢浥兦赩慮絶慸她絻襼奼醃醓馗冘疙膚熟疤覰熱馲馳醵覷趷閹疺施薽嗁凂巃巇秇旈鷉秏釓釔巘藘鷙臚釚藝淝駞槞凢巤巩秫臫旭巴巵釶駹淹釻旾鈀七縄訄嘅訅蘆切爇爈爉訊鈍蘎爐嘑訑蘒鸕訖託嘘樝阤訦瘧蘧爬戯戲縶爸阸丸戹陁乇鉇驇扈之虍虎扎蹏乏虐穐虒虓虔處虖虗虘托乙虙虚乚蹛虜乜艜虝九虞乞號也虠牠虡驢虢乢艣虣乣虤鉥剦虧乨虩乩詫艫虬癰噱乱虱色乳蹴乴艴虵乵穵究艶艷噸乹晻艽扽乾乿窀亀亂窃芄皅窆劇把躐纑劖芚抛劜芝芤嚧嚨銫躭芭誮犯嚱犱芲亳庵銸芼抿竃苆黆仇囈竉軋軌囐雑諕黕櫖他拖髗髛仛竜滝雝仡勢髢囤黤盧髧櫨曨廬滬滮廱曳勴曵黸滹盹電拽竾笂范霃笆猇眊謊輒挓謔笔挖弛鬛伜匜圠蜣眨圪匭札鬮地鬳笵錷挹缻謼鼽卂譃罅卆齆齇罏齓歔孔杔齕魖杝轤魨罨蝨彪魫孰彵獵杶靶杷獹魹獻蝿宄宅厇讋箎龏鎏龐龑龒龔枕龖龗殗羗侘龜龝讞垞箞龞枠垡殢掩螮鮿柂蟄迄範迅翆迆柉俋迍矑柒染揓忔毡毣迤毦毧篪篭毮忰鯱叱毱忳菴埶執毷毹篹俺矻濾俿毿"));
        PART_TO_KANJILIST.put('虚', new Part(1, "歔嘘虚墟戯"));
        PART_TO_KANJILIST.put('九', new Part(2, "笂贄宄芄訄蒅訅爇紈墊粋砕馗抛伜熟枠紣匭熱犱縶丸褹褺褻尻鼽氿蟄卆翆仇秇驇囈軌汍雑染虓酔鷙釚髛九藝勢鳩旭摯孰忰埶究執駹蓺蓻釻襼艽塾"));
        PART_TO_KANJILIST.put('乞', new Part(1, "吃迄訖屹乞乾"));
        PART_TO_KANJILIST.put('也', new Part(3, "絁陁柂咃迆鉇酏訑揓他拖袘弛匜杝駞也池牠髢貤迤阤地沲馳彵虵釶椸葹她施竾"));
        PART_TO_KANJILIST.put('號', new Part(1, "饕號"));
        PART_TO_KANJILIST.put('虫', new Part(6, "蠁蠃蠅蠆專蠉蠊蠋蠍蠎蠏蠐蠑蠒蠓搔蠔蠕蠖蠘蠙蠚蠛蠜蠞蠟蠡蠢蠣蠧蠨蠭蠮蠰蠱蠲鐲尵蠵蠶蠹蠺蠻蠼瑀鱄偊偑属屬聵摶钃風颪颫颭颮颯颰颱傳颴颶颷颸颺颻颿飂飃飄飅飆飈擉飌僓糔磚壝惠蓴蔃鄅甎儙襁慅嵐楓蕙襡蕢慱嵹襺憒憓膞斣薥閩禹臅闠嗤触槫燭觸瘋騒瘙渢嘱嘳縳騷樻踽繐穗繢噣繦虫虬繭虯虱虵鉵虶虷剸虹虺虻繾躅劅蚈蚊蚋蚌蚍蚑蚓蚕蚖蚘劚蚚蚜蚡蚣蚤隤蚦蚧蚨蚩蚪蚫蚭蚯蚰蚱蚳蚴蚵蚶蚷媸蚸蚹庽蚿囀蛀櫁蛁蛃蛄蛅蛆蛇蛉蛋蛍滍蛎囑髑蛑蛒蛔蛕雖蛗蛙蛚蛛蛜蛞蛟蛠蛣蛤勥嫥蛥蛧蛩独蛬竬蛭蛮蛯竱諷蛸蛹蛺蛻蛼蛽蛾蜀蜂蜃蜄蜅蜆蜇蜈蜉蜊蜋蜍蜎蜏蜐蜑蜒蜓蜔團蜘蜙蜚圚蜜蜞蜟蜡蜣蜥蜨蜩蜮蜯蜱蜲蜴強蜷蜹蜺蜻蜼蜽蜾蜿蝀蝃蝅轉蝉蝋蝌蝍蝎蝓潓譓蝕蝗蝘蝙蝝蝟蝠蝡蝣蝤蝥蝦靧獨蝨蝪蝮蝯蝱齲蝲蝴蝶蝸蝻蝿螂螃螄螅螆螇螈螉螋螌融螐螓螕螗螘螙螞螟螠螢螣螧殨螫螬螭螮螯螱螳螵螺掻螻鞼螽螾螿蟀濁蟁蟄鏄蟆蟇蟈蟉蟊蟋蟎蟐蟒蟕蟖蟙矚蟚蟜蟟蟠蟢蟣蟤蟪蟫蟭蟯蟱蟲蟳蟶蟷蟸鏸蟹鏹蟺蟻蟾蟿"));
        PART_TO_KANJILIST.put('普', new Part(1, "譜普"));
        PART_TO_KANJILIST.put('景', new Part(1, "影憬景"));
        PART_TO_KANJILIST.put('湯', new Part(1, "蕩盪湯蘯"));
        PART_TO_KANJILIST.put('陰', new Part(1, "陰蔭"));
        PART_TO_KANJILIST.put('干', new Part(3, "送瀁逆頇堈蠊研倖頖逗栞栟怦逬頬頭砰報蠲逹瀾塀遂偂灃遃遅衅桊灊塍衎灎塏顏偐塑遒道達灔顔遖顗衘恙偙衜聠遡遥塧適聳遵職遼桿邃墊悌傍悍増邗墜肝傞墟墡袢悦梪墫梯碰墱碲悻磁磅烊磋裋壎僐磑惓僔僕僖胖惘僜裞棡飦僧棬磳磴裷壹胼儀餅儈養礒夔餖餘餠礠椦鄧褨椪愬鄭儯鄯脰脱鄴儵鄶愷鄷褺餻襆慈饈慊酋兌饍煎饎饐襒襗襚楝楠奠楢祥腩慫業襯楴襴襷奸慻兼襽兾膀膁憋禌妍憎首馗馘憘禘憙榜憠禦馦禧膨親覬醭禮羚膳醳醴覴冸熹榺熺榿懀燈槊釋懌槎燎凒様秚釜姜釟懟釡駢秤燧觧姧懩釬凱臱凲凳燵駻駾懿臿樀戃樅稈刊稊税訐刑舒舘爛戠樣娣判爤娧娨騰許戲樴稵券樸樹樽牂剃剅前剏艏牓評艖橙穙驛鉛剛橛扞婞穟艠鉡艤橧剪橱橲詳橳剴艶艷婻鉼劂誉芉劊芊芋銒銖犠檥犧抨説骭窯銲犴媵劷誷銻骿鋀櫈髈櫉嫌拌竒體竕拜勝拝嫡櫤諦竦諫勬鋭狭鋲拳竴諵苹諺鋼拼竿鬀嬁猂笄謄欄欅錈鬈嬉欉鬋謗欗謙鬙謚猞挟鬠謡鬪謫錬謭献鬳鬴鬵猶猷鬷鬹鬺謼獃譄譆午歉證荊半捍魍歒荓歔歖南獗鍗識獘譜歝譯獯議譱捲孳荳卸譾讁鎂鮃宇鎈厈鎊鎌鎔讕玕讜鮝箞鎡厥鎧厨鮩殪殬箭鎭鮮鎮鎰鮷鎹掽揀揃菐鏑鯗叛鏟叢揥寮鏷揺尊鰊鰌尌萍對導鐏鐐搒搓鐙鰜搠鐡搤簦鰦氧鰧鐮尲簳鐸鐽簾谿鱃豆豇豈豉鱉豊豋豌豎屏豏豐籐豑鱒摒豓豔鱔鱖汗摘籘豙鱚鱛籜豢豣籣層葥鱧鑭摯屰瑳鱶汻豻呼屽鑿粁蒂钄撅撇粉貋璒撙璞岡蒡撩咩璯粱撲咲璲蒴岸蒹撻岼賆擇糍瓓糕泙糞峡糦糩泮蓱擲擶瓶哶賸賺蓻哾擿攁贄甆贈洋蔐甑攔崗産攩唫崹攼蔽絆商鵇啇敍畔蕕敘嵜鵜敝啣絣鵥蕨嵭嵯嵰嵳敵啻畻啼斁喃斃善綈閈開嶒涕斕趕薘喜嶝嶟関綣嶧薩薪鶫嶫嶬涬新綱閲網鶼鶿旁鷁旆巇鷊嗌闌痒闓闕痘闘旘巙鷙嗞嗟締藤闥闦鷧鷩差鷯跰旰旱旴巻旿蘀縊蘍嘑縑縒渕蘚帝渞踡縢瘥縦並渧帨丫蘭帲丵縶嘻渼嘿虀虁噂療湃蹄湅蹉噌噎乎幐繒織噔湔繕虖晘繚晜幞噠癢蹢幣蹤繥蹩癬蹬蹭幮普蹰干蹲噲平湳年幵噵并蹶虷繸幸繹幹噺鹺登鹻蹼纁躂纆隊亊躑隑溓躕隘皚暛躝暟庠溠溢隥隧嚭溯嚱皷暸暼躾暿溿曀滂軆廉益滋囍囎曎囏滏軒滕黕盖廚曛黟仠黤廥曦黧黬盬黭黮黰盰黱黲滴黵仵滸黸黹滹蛻黻滻黼曽朇鼈圈圉弊圏鼓鼔朔鼕朕鼖鼗鼙鼚圛鼛漛輞鼟弟蜣輧弮伴輶眷蜷眹伻鼾漾缾着睅罅彅杆睇罇靊罔罕彜睠形坢罤蝤彦潦坪睪彭佯罱杵併蝻潽靽罾睾螃澄枅螆徉羊型羌澍羍澎美羏羑従羔羖羗螘羚羜羝羞龞垟侠螠御羡羢羣螣澣群澤瞥羦澧羨義垩瞪羪垪羭瞭羮澮羯枰羲羴羶羸羹侻羼瞽迀濂迂韃蟄韊忓翔迕蟚蟢忤俤翦濨柬短濮矰執迸矸濹蟻"));
        PART_TO_KANJILIST.put('平', new Part(1, "平鮃秤評怦泙苹坪岼萍"));
        PART_TO_KANJILIST.put('鹵', new Part(11, "磠鹵滷鹸鹹鹺鹻鹼鹽"));
        PART_TO_KANJILIST.put('幵', new Part(1, "餠刑形研笄幵枅荊型開妍栞"));
        PART_TO_KANJILIST.put('并', new Part(2, "送瀁逆堈蠊頖逗栟怦頬頭砰蠲瀾塀遂偂灃遃遅衅桊灊塍灎塏顏偐塑遒道達灔顔遖顗恙偙衜聠遡塧適聳遵職遼邃墊悌傍増墜傞墟墡袢悦梪墫梯碰墱碲悻磁磅烊磋裋壎僐磑惓僔僕僖胖惘僜裞棡僧棬磳磴裷壹胼儀餅儈養礒夔餖礠餠椦鄧褨椪愬鄭儯鄯脰脱鄴儵鄶愷鄷褺餻襆慈饈慊酋兌饍煎饎饐襒襗襚楝奠楠楢祥腩慫業襯楴襴襷慻兼襽兾膀膁憋禌憎首馗馘憘禘憙榜憠馦禧膨親覬醭禮羚膳醳醴覴冸熹榺熺榿懀燈槊槎燎凒様秚釜姜釟懟釡駢秤燧觧懩凱臱凲凳燵駾懿樀戃樅稊税爛戠樣娣判爤娧騰戲樴稵券樸樹樽牂剃剅前艏剏牓評艖橙穙鉛剛橛婞穟艠鉡艤橧剪橱橲詳橳剴艶艷婻鉼劂誉劊銖犠檥犧抨説窯媵劷誷銻骿鋀櫈髈櫉嫌拌竒體竕勝嫡櫤諦竦諫勬鋭狭鋲拳竴諵苹諺鋼拼鬀嬁謄欄欅錈鬈嬉欉鬋謗欗謙鬙謚挟鬠鬪謫錬謭献鬳鬴鬵猶猷鬷鬹鬺謼獃譄譆歉證半魍歒荓歔歖南獗鍗識獘譜歝獯議譱捲孳荳譾讁鎂鮃鎈鎊鎌鎔讕讜鮝箞鎡厥鎧厨鮩殪殬箭鎭鮮鎮鎰鮷鎹掽揀揃菐鏑鯗叛鏟叢揥寮鏷尊鰊鰌尌對萍導鐏鐐搒搓鐙鰜搠鐡搤簦鰦氧鰧鐮尲鐽簾谿鱃豆豇豈豉鱉豊豋豌豎豏屏豐籐豑鱒摒豓豔鱔鱖摘籘豙鱚鱛籜豢籣層葥鱧鑭屰瑳鱶呼鑿蒂钄撅撇粉璒撙璞岡蒡撩咩璯粱撲咲璲蒴蒹撻岼賆糍瓓糕泙糞峡糦糩泮蓱擲擶瓶賸賺蓻哾擿攁甆贈洋蔐甑攔崗産攩唫崹蔽絆商啇畔蕕嵜鵜敝絣鵥蕨嵭嵯嵰嵳敵啻畻啼斁喃斃善綈嶒涕斕薘喜嶝嶟関綣嶧薩薪鶫嶫嶬涬新綱閲網鶼鶿旁鷁巇鷊嗌闌痒闓闕痘闘旘巙嗞嗟締藤闥闦鷧鷩差鷯跰巻蘀縊蘍嘑縑縒渕蘚帝渞踡縢瘥縦並渧帨丫蘭丵縶嘻渼嘿虀虁噂療蹄湅蹉噌噎乎幐繒織噔湔繕虖繚晜幞噠癢蹢幣蹤繥蹩癬蹬蹭幮普蹰蹲噲平湳噵并蹶繸噺鹺登鹻蹼纁躂纆隊亊躑隑溓躕隘皚暛躝暟庠溠溢隥隧嚭溯嚱皷暸暼躾暿溿曀滂軆廉益滋囍囎曎囏滏滕黕盖廚曛黟黤廥曦黧黬黭黮黰黱黲滴黵黸黹滹蛻黻滻黼曽朇鼈圈弊圏鼓鼔朔鼕朕鼖鼗鼙鼚鼛圛漛輞鼟弟蜣弮伴輶眷蜷眹漾缾着罅彅睇罇靊罔彜睠坢罤蝤彦潦坪睪彭佯罱併蝻潽靽罾螃澄螆徉羊羌澍羍澎美羏羑従羔羖羗螘羚羜羝羞龞垟侠螠羡羢羣螣群瞥羦澧羨義垩瞪羪垪羭瞭羮澮羯枰羲羴羶羸羹侻羼瞽濂韃韊翔蟚蟢俤翦濨柬短濮矰迸濹蟻"));
        PART_TO_KANJILIST.put('癶', new Part(5, "嬁廃澄揆燈癈櫈證脊溌暌蹐噔醗鐙橙際僜嶝察瘠蔡廢墢戣撥擦祭墱凳磴葵癶癸鶺発登發"));
        PART_TO_KANJILIST.put('晶', new Part(1, "晶橸"));
        PART_TO_KANJILIST.put('幸', new Part(1, "澤蟄贄擇圉釋懌摯譯報倖執幸鐸鷙繹逹驛睾"));
        PART_TO_KANJILIST.put('癸', new Part(1, "葵揆癸"));
        PART_TO_KANJILIST.put('幹', new Part(1, "澣幹"));
        PART_TO_KANJILIST.put('幺', new Part(3, "蠁搐鐖瀠氦頦鰦蠻谿塐衒葒摔籘遜鱜呟鑠灣灤葤呦聨籮聯葯顯鑼鑾傃邎邏傒蒓璣袨炫蓀磁擁蓄賅壅糍磎泑胘僟胤泫磯胲郷糸糺系擽糾紀紂蔂紃約紅紆甆紇紈紉紊紋納紏紐鄐紑紒紓純紕甕紖紗紘紙級紛紜紝攞紞素礠紡索攣紣紦紪紫愫礫紬紭紮累細紱紲紳紵儸紹紺紼紽紾紿絀絁終絃組絅絆絇絈慈慉絋経絍絎絏結絑饑奒絓饔絖饗絗絙絚奚絛絜畜絝蕝絞蕠饠嵠絡畡絢絣絥給絧絨絪絮絰統絲絳酳絵絶絸絹蕹絺襺絻絽絿畿綁綂綃綅綆膆綈綉綋綌禌薌綍膎綏鶏綑經綖綗継続綛綜綝綞綟榡閡綢綣綦綧禨綪綫綬維綮綯綰綱網喲綳綴綵綶綷斷綸綹綺綻綽綾綿醿鶿緂緃痃臃緄鷄緅緆緇嗉緊緋緌緍緎痎総緑緒巒緕緗緘嗘緙線緜關緝緞嗞姟締臠緡緢緤緥鷥藥緦編緩緪緫緬緭緯緱緲練緵緶懸緹緺緻戀縀縁樂鸂縄縅縈縉縊蘊縋爍樏縐縑縒渓縕縗縛縜縝縞鸞縟縠縡昡縢縣縦縧縨蘩縫縬縭縮縯瘰蘰嘰縱縲縳縵稵縶縷舷稸縹縺縻總績騾蘿縿繁繃詃噄繄繅繆虆繇鉉繊蹊繋繍繎繐繒織陔繕癕繖穖繘繙繚繝繞繟機繡繢乣繥繦繧繩繪繫繭繮繯繰繳癴繸繹幺繻幻繼幼繽幽牽繾幾繿乿纁纂纃纆纇纈窈纉纊續纍纎纏纐纑纒躒纓纔纕纖纘纚纛纜纝纞嚩溪皪嚮銯檰隰蚴麼蚿滀囉滋雍曏狕拗黝櫞雞櫟曫勬苭欏謑欒圝弦眩欬漯茲鼷猻荄孌彎譏潔魕卛彝轡轢孫孳坳孿靿玀羂玄羅侅玅玆螆率螇變後鎍鎡辮徯鞵玹螺徽鞿鯀蟀係韅鏍矕濕蟣濨濰濼響"));
        PART_TO_KANJILIST.put('発', new Part(1, "廃醗発溌"));
        PART_TO_KANJILIST.put('登', new Part(1, "澄磴燈橙證鐙登嶝"));
        PART_TO_KANJILIST.put('幼', new Part(1, "拗窈幼黝"));
        PART_TO_KANJILIST.put('發', new Part(1, "廢撥癈發"));
        PART_TO_KANJILIST.put('白', new Part(5, "蠁樂嘅萆鰉爍怕帕搗帛戛戞栢堦堭稭縮堲騵舶穆陌艎遑鉑偕願蹜鱜湝瑝湟偟鑠癤灥葩幫噭繳湶摺白百皀邀皁皃的檄皅皆傆皇皈邈貊皋貌皌梍隍墍皍階皎源皐躒皓媓粕皕皖皙袙皚皛皜皝咟皟皠皢皣暤皤皥皦碧皧粨皨檪皪暭皭劰檰庳岶粺撽曁嫄竅棉泉泊拍廏曏廐磖狛櫟竡諧郫惶島瓸擽擾蓿朇儌錍椑唕鼙謜嬝錦洦優礫伯蜱褶弼愿魄鍇絈煌蝗兜鍠畠轢啤敫襫奭佰慴楷腺楻楽楾憂箄覈喈螈鮊薌箔澔原熠厡喤徨薬薭厵徼綿宿激珀篁柏藐習線緜槝藠槢藥鏥揩翫迫凮凰槹濼篼鯽"));
        PART_TO_KANJILIST.put('幾', new Part(1, "饑幾機磯譏"));
        PART_TO_KANJILIST.put('百', new Part(1, "竡憂栢鏥粨優貊陌縮佰瓸弼百擾戞宿蓿"));
        PART_TO_KANJILIST.put('广', new Part(3, "瀇蠊瀍簏蘑樚嘛鸝瀝渡搪席鐮踱縻蘼簾砿昿牅穅鱅鱇穈灋灑塘鑛摝蹠鑣癧摩驪摭瑭籭遮幮鉱塵广鹿麀庀麁庁麂麃広麄庄麅麇庇麈墉床纊麋庋麌墌麎庎麏序纏溏傏邐麑麒麓躔麕底躕麖庖麗店麘劘纚庚麛府麝嚝麞麟庠庢麤庤庥度暦躧粧座庨庪庫庬庭傭庱庳庵庶康庸麻麼庽麾庾麿庿廁廂廃蓆廆廈軈廉廊廋廌賍廎廏廐廑廒廓廔廕糖廖蓙壙廚廛糜廜廝廞廟糠廠曠拡廡賡廢廣廥磨廨廩櫪廫曬廬廰廱廳擴擵滽攈漉欐唐蔗鄘鄜攟儣攦礦椨褲蔴儷餹鄺鄽靂轆酈孋絋鍍譍腐腑魔靡轣兤捬彲歴慵慶慷獷侂掂膅鶊龐鎕螗薦膺薼醿濂釃應韉鷓臕鏖篖応鏞懡凢藦藨懬懭俯鯳鷹"));
        PART_TO_KANJILIST.put('鹿', new Part(11, "麀麁麂麃麄麅麇麈攈漉麋麌麎簏麏欐邐麑麒麓麕麖麗麘樚纚麛鄜麝鸝麞麟攟麤攦躧儷薼釃轆酈孋灑臕鏖摝鑣藨驪曬籭彲塵鹿"));
        PART_TO_KANJILIST.put('亀', new Part(11, "亀穐蘒竃縄龜龝鬮蝿"));
        PART_TO_KANJILIST.put('皀', new Part(1, "皀廐曁梍廏"));
        PART_TO_KANJILIST.put('広', new Part(1, "拡鉱広絋昿砿"));
        PART_TO_KANJILIST.put('皃', new Part(1, "藐皃貌"));
        PART_TO_KANJILIST.put('庄', new Part(1, "庄粧賍"));
        PART_TO_KANJILIST.put('亅', new Part(1, "吁頂氄逅琊鐍后倎預砑耓研途耕倚瀞砢琤琦倫鐫倳栴耵瀹萹谺汀呀葇遊鱊灋恋塌遍偏塗屙汙停呞聤籥塨豫灯鑰籲呵葶聹遹豺呼墅梈岈墍撐咑沔碕邘璚撝財撡岢袤邪梮貯河蒸岺糄壄糅棆胏擕郕烝泠飣磤哥哦峨峩哬郭擰泲瓴烹飼哼惼哿儀儁椅紆変鴉褊崎焏礒餓紓愗餘崘崙儜崢蔫紵町甼浄奇酊啊敍奕敘楙嵜鵝鵞祠敧慨畭奵蕷畸楺嵿概閉覉冊斒疔覗斜妤鶩嶬嶲妳禴疴薴冴綸綺斿旃凈姈藊姊懋姍野懏旖釘跚跡既嗣懧槨編淨淪釪秭觭釮旴鷸臹觹觽痾凾丁刁訂蘅嘅娍騎訏我嘑舒騖騙鸙爚減訝丞渟帟鈣昤娥踦刪爭渮舲鈳訶舸丹刺阿婀扁蹁牁驁蹂詅才乎艑打橕虖橘繘牙牚剚扜詝剞詞癟晠艤除詬鉰牱乴虶婷婺湾穿承嚀劀庁犄亅了檇予争亊事芋劌亍于序抒蚜犠亦犧芧亨嚬骬亭犲蚵檸隸芽盂仃雅竆雋諍竒廓論務竚苛諞鋣曦苧軨髩竫蛮鋮拯盱櫲髳狳滹雺諺軻竽蛾錀嬀伃眄茅圇謌蜍朎漑鼒猗弙猙錚霚嬝錡圢欤笥弥鬦笧霧鬨圩輪漪笫鬫笭鬭輮弯猱霱笴猵挶欹伺謼匾朾彁罅杅佇歌譎潏材孑鍒孒捓何齖策蝙静余潙靜睜獝筝魣蝥鍪靪齮獰議荷坷杼荼鍽坾彾荿瞀宁掄羇宇掎玎箏徏徐侖玗掙殛羜箞龠龡垢龢龣鞣龥澦徧義厩莪宬掭羲鎶珂迂柃矃寄俄叅俆篇揉珊蟊鏊翎鯎菏寐迓柔濘叙矛矜矞矟矠寧翩埩忬叮可柯寯矴柵司柹迹珹篹蟻埼鯿"));
        PART_TO_KANJILIST.put('了', new Part(1, "劀瞀氄墅茅了予鐍序預抒舒紓騖愗璚霚丞鞣袤妤澦霧芧亨鶩輮猱霱蒸蹂壄糅葇揉蟊鱊懋譎野潏孑鍒柔橘繘務楙矛矜獝烝矞矟矠魣蝥鍪豫忬拯櫲髳蕷鷸遹烹婺楺雺杼凾承"));
        PART_TO_KANJILIST.put('隆', new Part(1, "嶐隆窿"));
        PART_TO_KANJILIST.put('皆', new Part(1, "偕皆楷諧揩階"));
        PART_TO_KANJILIST.put('皇', new Part(1, "凰鍠篁遑惶皇蝗徨鰉煌隍湟"));
        PART_TO_KANJILIST.put('予', new Part(1, "劀瞀氄墅茅予鐍序預抒舒紓騖愗璚霚鞣袤妤澦霧芧鶩輮猱霱蹂壄糅葇揉蟊鱊懋譎野潏鍒柔橘繘務楙矛矜獝矞矟矠魣蝥鍪豫忬櫲髳蕷鷸遹婺楺雺杼"));
        PART_TO_KANJILIST.put('争', new Part(1, "浄争静筝瀞"));
        PART_TO_KANJILIST.put('隊', new Part(1, "隊墜"));
        PART_TO_KANJILIST.put('隋', new Part(1, "橢隨隋墮"));
        PART_TO_KANJILIST.put('二', new Part(2, "送瀁逄逆怍瀑耒逓耓途耔耕耖瀗逗耗耘耙倜耜耝耞瀞耟借耠耡逢耤倦怦耦倧耨倩逪耬耮耰瀷耺瀼耼瀾偁遂偂灃遃遅偆恆灊遌灎偐恒遒道達灔遖恗灘遘恙偙恝灞聠遡適恭偰遲聳遵職選遼肁傃邃悆肈悌傍悑邘傞悟那悥邦悦悪炬悰傺悻僄情僅烊僎惎僐惓僔僕胖僖烘惘惜郜僜惡僣僧胮胯僵惷胼烽儀鄂愃儈脊愕儕儖脘鄘鄞鄠儣鄧愫儬愬鄭儯鄯脰脱儴鄴儵鄶愷鄷儺兂元酃慈腊慊酋兌煎慓兘酛兤慧腩慫慬腭慰共酴煹慻兼兾冀膀膁冃憃冄膆醋憋憎冐冑冓冕膖醗膘憘憙熛冠憠熢冦熨膨膩醭冱决膳醳醴冸醸熹熺醽冿臀釀懀懃燈臋臍燎釐凑凒凛臛釜釟懟釡懢燧懩釪懯凱臱凲凳燵懷燸臻懿舂戃爆舉刓刔爗爛舝舞鈞戠判爤鈨戩刱戲刳戴制券牂剃鉄牅鉅剅前剏艏牓艖剘鉛剛扜艠鉡剤艤艦剪艱剴扶艶鉷艷鉼剽牾承犀劂抉銉劊芋犍抎抏劑銖芙犠犧抨芫銭劵劷芸銻芼鋀鋆拌勌鋎拒鋒勖鋙苚勝拝勡苣苤勤勬狭鋭拱拲鋲狳拳鋳勷苹鋼拼匀錆錈匏錏猜錝挟茟錤猨錬献錯茱錶猶猷捁捂荂獃半荓鍔南獗鍗獘鍥捧獯鍰捲荳捶卷捺荼卾鎂鎈鎊鎋鎌鎔玗厝莞鎡掣厥鎧厨玩措厪鎭鎮鎰莱鎹掽揀菁揃揆鏊揎菐鏑珒叙珙叚叛叞鏞鏟珠鏢叢揥菫揳揵菶鏷珸吁萁鐁搆鐉萍鐏鐐搒搓鐙鐚搠鐡搢搤琪琫琮鐮萱鐳搴萼吽鐽吾瑁瑃瑇呍葏鑑鑒摒摓瑗摘鑚葜葥瑧瑨呩鑭鑮味瑳摴葵呼摽瑾鑿蒂蒄钄撅撇璒撙蒜璞蒡咢撥蒨撩咩蒪撬璯撰撲咲璲蒴蒹撻蓁哄瓓瓞瓠擠瓢瓤擦哪蓬蓱擲哶擶瓶蓻蓽哾擿攁攅甆唍蔐甑攓唔攔唖攘蔚蔡産攤蔧甩攩唪唫蔭攮蔲蔳唶蔶蔽啁商啇畉畊敍蕐蕓畔敔蕕敘蕚敝蕨畭蕯異畴敵蕻啻畻啼蕿斁喃斃善斉斎薑斕薘斜喜斠関閧喧薩薪斮新閲斳薷薸薺薽旁藉嗉嗌闌藍闒痒闓旔闕藕痘闘旘藝嗞嗟藤闥闦旴藼藿昀蘀瘄蘅嘆蘍蘎嘏蘐嘑嘒昔蘘蘚瘠春瘥昧瘭蘭阮瘵嘷嘻嘿噀虀噁虁虂噂療噄癈晉癋晋噌降晎噎噔虖癜晜噠院癢除噤晤陦虧癪癬普陰癱噲晴噵虶癸噺発登暄隊蚌嚌皐隑皖蚖隘暙皚際暛嚝暟皟皣隥皥蚦隧嚭暭隮嚱暴隷皷嚷隸暸暼暿曀盂曂曄囅曇囈益囊囍囎曎囏囑囓盖曛蛛曝難監曦曩雩蛬园雯盱雱雲囲盶雺蛻曽替蜂霂霃圄霅朇圈霉霊圊看蜍眎圏圑朔朕圕霚圛眛霛霝蜞蜡霡霢蜣霣霨圩未圬蜯霰朱霱霳蜷眷眹蜻霽着靁靃杅睆靆均睇杇靉靊靎靏青靕靖靗靘静靚睛靛靜睠坢蝤来坪睪杬蝯蝻靽螃螆瞎枏螓瞔螘瞟垟螠螣垣瞥垩瞪垪垬鞬瞭枰螱瞱鞲螵垸鞸瞽鞾韃柈韊埕蟖矚蟚韛韝矠韠柡韡蟢蟤矩柬短柰矰韲韵韸蟻堈砉堊蠊蠐頑栔頖栙栟蠢株頬蠭頭蠰砰栱蠲頴塀衅塉桊衋桍塍顎塏顏塐塑桓顔衖塗顗衙衜塞桟桧塧塨表顬桻衻硾墀碁墁袂墈墊碏墐増梛墜墟梡碡墡袢梧梪墫梯碰墱碲袴梼碾磁壂飃飄磅棅棈磋裋壌壎磑棒棕壙壜壞裞棡壤磦磧壩棬裯棰裱磳磴壷裷壹壺壼製餅夅夆養礌椏礒夔餖餘褠礠餠椦褨椪夫夬礭褰失夳礵夸示褺餻褾椿祀襀祁襄祅祆襆祇奈饈奉饉祊饌饍饎奏饐襒祓祔祕祗楗襗祘祚襚祛祜奝楝襟祟饟祠奠楠楢襤楥祥祧票祫祭業襮襯楱祲楴襴極祷襷祹祺祼襼襽祿禀禁榊禊妋禋妌禌妍規禔禕禖首馗禘馘榛禛禜榜禝榡禡禦馦禧覧妧禩禪親禫覬禮覯羚禰妰禱覲禳禴覴妹榺榿槊構槎秏様秚姜秠駢秤秦觧秩秬姱槹槻駾槿樀樁樅稊騌税訏樗標娜騞訣樣娣娧横娪騫騰樰樲樴稵樸樹樽驁驃穄穉驊橒評穙橙橛婞穟穣驤驥婧橧穧穰橱橲詳橳婻穽誄誅誇誉媋檋檑語媢檥檫骬説窯檱媵誷窹窺檻骻檽媾骿諆櫈髈櫉請嫌竒體竕嫖諠嫡諤櫤髥諦竦諫髬嫮竴諵竺諺諼竽鬀嬁鬃笄謄欄欅謇鬈嬉欉鬋欖謗欗謙鬙謚欛講欞鬠嬢鬨鬪欫謫謭鬳鬴鬵鬷謹鬹鬺謼款孁魂孃譄魅譆歉證魌魍歎歒譔歔歖識譛譜歝筦魭筭孮議譱譲孳歵魹譾讁讃鮃宇殊残完箐讓讕宗讜鮝箞実箠殣宣鮩殪鮬殬箭鮮鮷箺箻宼殿寇鯇篊寐寒毒鯖鯗篝察毡毣寤毦毧篨寨毮寮毱篲篷毷毹寿毿簀鰀氂氄氅簆鰆尉氉簉尊鰊鰌尌氍對氎簎導鰐簛鰜簦鰦氧鰧簨氬鰰尰簱尲鰶簶簺鰾簾谿籃鱃鱅豆豇豈豉鱉豊豋豌籍豎屏豏籐豐豑鱒豓豔鱔展鱖籘汙豙汚鱚鱛籜籞豢豣籣層鱧鱩汫屬屰鱶決沅粉沍岏岝粠岡沫粱貳岼粽精賆糍賎糕賖糙賙泙賛糞峡糦賨糩泮峯泰賰峰賵賸賺購賽賾鴃紆崇贈洋崗洙紜素洪洯崹崿洿嵂浅絆嵈絚絜嵜鵜絝浣絣鵥嵭浯嵯鵰嵰鵲絳嵳絵涂綈鶎鶏嶒涕鶚涛綜嶝趞嶟綣綦嶧綪鶫嶫嶬涬鶱綱網鶺趺鶼鶿鷁巇鷊跌淎緕淙巙距締鷧跨巨鷩緪差鷯跰践巷巻巽清済三縉縊踑縑縒渓渕踖鸖帙帝渞渠踡縢並縦渧帨踪縫丫丮帮港丰踳丵縶縹丼渼蹄湅蹇蹉湊蹍乎蹐幐繒織湔乕繕幖繖幘繚幞蹢幣蹤繥繧蹩蹬蹭幮幰蹰湲蹲平湳并蹶繸鹺鹻蹼繿満纁躂纃纆纉亊躋二溌亍溍于躑云互溓五躕井纕亖亗亘亙溙纛些纜亜溝躝亝亞亟庠溠溢麩溯溱麸躾溿仁滂廃軆廉滋滏廑滕黕廚黟転黤廥黧仨黬黭滭黮黰黱黲滴黵黸黹滹黻滻軼黼滽黿漂輂漈鼈弊漌伍弍弎輐弐鼓鼔鼕伕鼖鼗鼙弙鼚会鼛漛伝輞鼟弟漢輦弮鼯鼱輳伴輶缺輾漾缾弿轃罅彅罇齊齋彍罎齎齏潔罔彗佘余佚潜彜佞罤彦潦齧齬彭彯佯齰罱併齶齽潽罾澄徉侉羊羋羌澌澍羍澎美羏侏徐澐羑龒従侓羔澔龔徖羖羗龗羚供羜羝羞龞侠羡羢羣群徤羦澧羨義羪羭羮澮羯徱澱羲羴羶羸羹侻羼濂迂俆忈俉俌运翔濟忢俣俤翦忨濨快翫濫迭濮翲俵忶俸迸濹翺翼"));
        PART_TO_KANJILIST.put('于', new Part(1, "吁盂迂紆宇芋于"));
        PART_TO_KANJILIST.put('皐', new Part(1, "皐槹"));
        PART_TO_KANJILIST.put('云', new Part(1, "転魂靆曇桧繧囈靉蔭罎陰云雲絵耘芸会壜紜伝藝"));
        PART_TO_KANJILIST.put('互', new Part(1, "冱互恆亙沍"));
        PART_TO_KANJILIST.put('五', new Part(4, "捂圄俉伍五唔敔衙鋙絚語悟忢寤晤梧娪緪齬浯鼯珸窹吾牾"));
        PART_TO_KANJILIST.put('井', new Part(4, "冀驥爆畊恭港異刱瀑囲寒戴暴井耕巷翼丼曝穽糞"));
        PART_TO_KANJILIST.put('麗', new Part(1, "灑麗儷驪"));
        PART_TO_KANJILIST.put('亘', new Part(1, "諠萱恒垣宣愃桓暄喧亘"));
        PART_TO_KANJILIST.put('亙', new Part(1, "恆亙"));
        PART_TO_KANJILIST.put('亜', new Part(1, "唖壷悪亜"));
        PART_TO_KANJILIST.put('府', new Part(1, "腐腑椨府俯"));
        PART_TO_KANJILIST.put('亞', new Part(1, "惡鐚堊壺壼亞椏錏"));
        PART_TO_KANJILIST.put('亟', new Part(1, "極亟"));
        PART_TO_KANJILIST.put('亠', new Part(2, "蠃堃倅堉蠉倍頏蠐逑耒耓蠔耔耕倖耖耗耘耙瀛耜耝瀞倞耞耟耠校耡倣耤頦耦瀧耨倩株耬耮蠰耰報逳核逹瀼瀾衁遃顇遊恋偎顏偐衒顔恔灕灘塙偙停灝遠衣偣聤顥遧表遨適硫顫衰聱衷職衾塾恾聾避邀袁境還邅炆墇袈梈邊袋悋傍碎傐肓梓炕邙傚碚袞袠傡邡袤悥碨袨墩肪炫颯袰育碲傲梳悴債炷傹肺碻梼傽碾裁壁磁裂棄装情壅磅棅壇惇壈棈壊裊郊惊壌裏裒棓裔壕棗裘棘胘惘磚裛棜裝壞裟壟僣壤僦磧部郭僭棭僮裱胲裳飳裴裵胶郶裹烹僻製褁椁儂餃儃億椄儇変椉椊椋儌鄍褎褏意鄐褒愔儕椖鄗焙鄙褜夜餝焞焠餢脣夣鄣脤褥褧愫儬儭褰褱礱儱椳儴餵褵椸褺脺儺褻襃襄充慈慉祊腋慌契奒楔饔奕兗饘襛煜楝襞楞慞饟慠襢兤慧煨六業襯慱襲楴襴祷禀膀膂妄醅円醇熇覈醉禊膏膐醕禘榜憝熟榠醡冥憧醨妨親憫憬熬禯醯榱醲憲禳膵憶醸憹妹离膻馼禽妾熾膿釀懁槁臂臃臆槈姉凉燉釋懌懍臍燎臎懐駐釐秔凕凛凜臝槞姟秣姣槨駭駮釯懯臱燵懷樀鈁騂戇稕刘爛稟樟娠戠稡截航訪舫鈰戴樴舷稸稺刻註爽稾稿房驁詃婄扆鉉積牓剖驛穜鉝驟艟穠剠穣驤剤穥橦婧穧牨驪穰該割剷婷鉸牽檀檁銃抃檄誄誅劈犉檍檎劑犒芒窔銖抗檗抜芝芠窣銥劥骯芳誶誷骸効抹骹劾芾諄拄竅鋅髈拉竉請立竌竍勍竎竏勏竑竒諒竓竕高站苙竚竛髛竜嫜髜竝髞竟鋠章嫡竡狡竢竣勣髣童諦竦竨鋩竩竪竫諫櫬竬竭狭端櫰竰櫱竱諳鋳竴競勶勷竸諺猄欄錆笇茉謊挍嬖謗嬗嬛猜嬝猝謞錞鬟挟匟笠嬢猥錥鬧謫茫欬茭振茱茲嬴錶謷錺茺猽猿孃鍄荄魅歆譈歊獍魍獐魑歒卒荒獒鍗識譛譞獞卞獧孩譬譯孰譲孳魴譹孼孽捽莀讁讃玄莅玅玆率掊鎊殊讋宍鎏箐鎒讓殕掖莘玟掠接鮫厫鎬宰殱莱害宸玹揀菁毃鏃毅鏇鏊寐鏑毒毓珓揓鯖篙鏟珠鏡揥菨鯨菩毫毬篭寰鯲寴寵菸対簀吂萃簇氈琉鰊搐鐐簑搒鐓氓簔萕鐘吝搞氦㐬吭就簱鰲鐴鐵鐶鐸鐿豁豅鱆籍籏汒摔展摘豙鑚摛籝鱞呟籠鱣鑨籩豪籬鱮摯鑲味汴葶汶汸葹蒂璃咅沆粇璇璋粒沛撞蒞蒟璟蒡撤岦璧蒨撩沫責咬璮環咳貶粹撻璻撽精蒿哀擁蓂蓄賅擅擇賋瓌瓏蓐擐蓑賑擒蓓擗擘賛泛賠擠峡泣瓣瓤注糫泫糵哼擿瓿蔀贄攅贅唇贇紊紋鴋甎攏贏蔐崒甓甕鴗攘洙贛蔜蔟素紡産攤洨蔬攮攱蔳崹唹唼放甿蕀流鵁嵃絃商畆啇效啍畏敐啐畒敖畗嵜畜畝絞畡啦敦畩嵩嵪蕫敫嵭統敲敵敷蕹鵺啻啼蕽喂嶂綂薅喇文斈斉鶉斊斌閌閍斎薏疏斐喑斑閔鶕涖薗閙薛涛薜閡綧薧喨薩喪薪涪綪喫鶫喭鶮新液網綷方閺薺於閼涼施斿旁藁旂巃嗃痃旃旄旅旆闇藇旈旉藉旋旌闌痎旎族旐巐旒旔鷔緕藕旖旗旘鷙旙旛旟鷟締跡闢闤淤跤闥淬鷯淯鷲藳淳嗷旹旻鷾嗾瘀瘁市清鸇済昉鸊蘖瘖嘖縗蘗蘘蘛帝縞縟渟帟縡昡蘢踣渧昧阬縭蘭昱防瘴游蘺主績虀蹄之噋蹍幎乏織陔虔癕癖繚幛幜幟蹟虠幢蹢晨癪陪噫晬噭繯景蹱繳蹴晴乵幸繹噺虻湻湾晾暀躃纃躄嚆隈麈纉蚊躋嚌麎纎皎纐纕麖暗纛障暜皜暝麞溟亠暠亡嚢亢交亥隦皦亦嚨亨嚫享京亭亮隮亯亰暲亳隴亶嚷暸麸麹亹暻溽皽暾蚿滀滂囃軄囅曈滈囊滋雍黐廒廓滓囓蛛雜滝曟蛟離難黥囥曨曩廩蛮黯雯雱盲滴滻滾仿替圀蜃較弃蜄眆震鼇霈弈圉伉圊霎園漓圖漘望眛圜朜輞蜟弦朧眩漩未末漬輬弯朱鼱漳弴弶霹蜻霽輾轄轅坊齊齋轍位齎住齏坑青罔潔靕靖靗彗轘靘静靚睛靛靜潜轝睟潡彣蝣来彦潦齧齩杭彰影蝱彷靺潼佼睾罿往垃螃侅澈枋澋龍瞎龏侏龐瞑龑龒垓龔龕瞕龖龗辛辜依辝辞螟辟澟鞟辠侠辡辣澤辤辥辦辧辨侫徬辭螭辮瞮辯螯辰辱農侲瞳澵羶澶徸羸徹鞹澼徼蟀激濃蟄柆翊翌迒忘蟙忙忞濟翠濠翣埣柬忭翯柱韲俲音韴俵韵韶執韷韸培迹韺蟺韻俼忼翾響柿"));
        PART_TO_KANJILIST.put('亡', new Part(3, "衁蠃妄謊慌魍贏荒芒汒氓肓罔惘忘忙邙望瀛籝臝輞亡鋩侫茫釯蝱網盲嬴誷羸虻恾甿"));
        PART_TO_KANJILIST.put('亢', new Part(1, "坑亢抗伉航吭杭頏"));
        PART_TO_KANJILIST.put('蚤', new Part(1, "蚤騷"));
        PART_TO_KANJILIST.put('交', new Part(1, "校狡鵁較餃交效郊鮫咬皎駮纐効傚佼絞蛟"));
        PART_TO_KANJILIST.put('亥', new Part(1, "該咳垓亥核骸孩刻駭劾"));
        PART_TO_KANJILIST.put('麥', new Part(1, "麥麩麪麭"));
        PART_TO_KANJILIST.put('溥', new Part(1, "薄溥簿"));
        PART_TO_KANJILIST.put('麦', new Part(7, "麥麦麸麩麹麪麺麭"));
        PART_TO_KANJILIST.put('亦', new Part(1, "跡奕亦変迹恋湾蛮弯"));
        PART_TO_KANJILIST.put('度', new Part(1, "渡度鍍"));
        PART_TO_KANJILIST.put('座', new Part(1, "座蓙"));
        PART_TO_KANJILIST.put('亨', new Part(1, "亨烹"));
        PART_TO_KANJILIST.put('蚩', new Part(1, "嗤蚩"));
        PART_TO_KANJILIST.put('身', new Part(7, "軀軁軃射軄軅軆軇軈匑裑謝麝鵢身躬藭躭窮躮躯躰躱躳躵躺躻躾"));
        PART_TO_KANJILIST.put('享', new Part(1, "椁諄敦惇醇槨燉鶉享郭孰鐓廓淳暾塾熟"));
        PART_TO_KANJILIST.put('京', new Part(1, "掠鍄黥鯨凉椋京憬勍景影就鷲諒蹴涼"));
        PART_TO_KANJILIST.put('躬', new Part(1, "躬窮"));
        PART_TO_KANJILIST.put('亭', new Part(1, "停亭渟"));
        PART_TO_KANJILIST.put('皮', new Part(5, "菠坡鞁波陂婆碆披被皮皰疲破皴頗皷簸皸皹皺玻跛彼"));
        PART_TO_KANJILIST.put('亮', new Part(1, "喨亮"));
        PART_TO_KANJILIST.put('嚴', new Part(1, "嚴巖儼"));
        PART_TO_KANJILIST.put('暴', new Part(1, "瀑暴爆曝"));
        PART_TO_KANJILIST.put('隶', new Part(8, "糠棣曃禄穅靆鱇簫碌繍埭逮嘯緑録隶康慷隷隸捸粛祿"));
        PART_TO_KANJILIST.put('亶', new Part(1, "檀擅亶羶壇氈顫"));
        PART_TO_KANJILIST.put('庶', new Part(1, "蹠鷓庶蔗遮"));
        PART_TO_KANJILIST.put('康', new Part(1, "糠康慷鱇"));
        PART_TO_KANJILIST.put('庸', new Part(1, "慵庸傭"));
        PART_TO_KANJILIST.put('隹', new Part(8, "耀戁戄騅堆搉舊氍鐎鸐萑蠖鸖蠘稚鸛鸜爟権截鐫進戳樵蠵帷蘸携蘺蠼蘿鑃癄籆灈灉籊鑊灌噍灕癕穕灘衢顦顧摧虧艧癨驩穫籬艭籮陮癯癰籰癱顴鑵鑼璀钁嚄檇躍邏碓墔準躙貛貜皠璡蒦撨犨躩躪催誰檴隹暹確隺隻隼嚾雀雁擁櫂囃雄軅雅壅集雇軈雉囉雊雋雌飌雍雎雑雒雕擕雖瓘雘雙雚雛曜雜雝雞惟雟擢離難勧磪擭廱僱糴糶嫶蓷勸儁礁欆攉權贋霍椎欏錐崔甕攜攞欟攤嬥焦攫礭售唯匯儸儺漼蜼靃潅蕉魋襍譍靏罐歓饔潗譙奞嵟彠饠歡睢轥奪奮獲護罹蕹鵻玀玃羅嶊讎讐憔嶕薙讙鮠瞧推鎨榫維醮趯膲嶲観鶴榷膺瞿觀臃韄准應寉燋藋矍懏鷕凖篗鏙臛臞翟臡鷦濩蟭淮濯寯槯濰矱鷹觹藺懼懽觽燿藿觿"));
        PART_TO_KANJILIST.put('人', new Part(2, "送倁倅倆倉耎怏倏途逘候倚瀚怜倝耠倢倦逧逨逩倫瀲耷倹瀹耹逾偀聄聆偆偈遏恗灘聟聢恣偦偧恩遬恰偰聳遷偸遼傄肄悆肉炊邌傒炔傖傘悘傛悞傪肭炳悴邴炴悷傺郃僇僉僊僑惓胔僕胗僚惞僟郟烟僣郤惥胥惨胭胯胰惷胾鄈愈儈儉愉脊愌愒儗鄚愜脞愞焠儧焫鄭儯愱愴脵然鄶愹儺脺鄼腌慍腐煐酓慕煗慘腠煥兦腧全兪慫慲关腴酴慻慾憀醃憃膃内熅醉熌憍醍膎膐醑熔憖憗膜膝醞憠膠醦冪醪醫冭憭醭熯决膵醶冷憸膾冾懀燃臈臉懊臋燎懎臎凑懕凝懝臠燠臡懣懥臥懩臭釱凳懺臻臾懿戁舂鈇鈉鈌舍舎鈐舒刔舔舖舗舘爚刢戣鈦戧爨戮舲刳鈴鈵券戻刻刼爽鉃鉄艅牆剉牏艙剞鉠艢剣剦鉩鉯剱艱剳創扶牷剹鉿劂犄抉劊劍抐劒銓劔銕芖芙銙芡芥抦芩犬犮芮芲劵状抶芺銼犾劾鋈勌拎鋏勑苓拔鋘勝募勠鋡苡勧勪苪勬狭鋭英拳狳拴苶狹拾拿錀匃錈猋匌猍匏錑猒猗匙錜猝猞挟錠錡茣猤匧挨茨挫匬献茯匲猲匳猴茵錵茶猷医匼挾荂獃荃獄荅鍈鍉獎獏荑獒卒獗獘獠獣鍥獦捧捨捩獪獫鍭鍮捲卷捷獸捺獻卻捻荼捽掀鎂掄掎鎔鎗莝玞莟掟玠莢厥玦掩玪莫厭掭玲掲厴掵鎹厺莽莾参鏃參揄叅揆鏇鏉鏌珍菐提鏐叙換揜叡珡鏥鏦揬揳菴菶鏷菸琀琁萃合鐈萊鐐鐖吟鐟吠琦含琫搭鐱琴吴搵搶吶萸吹搽搿瑃葅葊瑍摎摏呑葖鑚葛瑛葜瑜鑡葢瑢呤瑧鑯鑰瑲葵摸摻命撅璆璇咉咎璏蒕撘璙撚璞撟撡璣咦撦咨撩璯咲撲咳撳蒺璻蒼咽蒽撿璿蓁哈瓈瓉蓉哊瓊擌蓌擒蓙瓛瓞瓠擦擪擬哭蓯擲瓴瓷蓼攀攁攅唅唉蔌蔘蔟蔡攤唪唫蔬蔭唳唵唸唹唻畉界敍畍蕎畎啐蕐畒敘畚畛敧蕨畭蕷畸畻敽薀薁疁斂閃閄薈喉疋薋疎疏疐疑薔薙喚斜喝閝薟斡関閤疥閦喩喫喬閹疹喻於閼疾薿闃嗅病嗆嗇痊闋旋痍族嗒闕旖嗘闚藜闟嗢藤痤旲痴藹嗽嗾嗿瘀瘁昃蘅嘆瘈瘉昊瘊蘊嘐昑瘓蘙瘞昞蘞瘟瘠映瘡昣昤春昦瘩是嘰瘲昳瘳瘸嘹瘼療癊陋噏癒晗陜陝噞癟癡噢除器噬晬陰癱噲晷癸険智晻陾陿隂隃隄嚈皋蚋暌暍暎嚏嚔蚕暙嚙際嚟暤蚧蚨隩險暮隳暵暸囃蛃雉蛉蛋盎囏囐盒囓盗囚雜盜雞因難蛤盦零曷囹蛺替會朅圇朇圈蜈蜍朎圏朕眕蜘霙朠蜯蜷眷眹蜹眾圿睂靄蝅坅靈蝎坎坐蝓杕靛睞睟睠蝡靨坱睳杴靹睺睽睿垁鞅鞆螇枎螓垓瞖枖枘螣鞨瞭鞮鞳鞵瞼鞿埃柃柄韆蟆蟇蟉韐蟒韙蟜矜埝埞韞蟟矢矣埣蟣矤韤知矦矧矩矪矬短矮矯矰矱韱韺埼砆砍蠍蠎砎預栓領蠜頜堠蠢堤堧栨頬蠮頰頷核堺蠺栿顇塉桊題桍塍衑塔塕塗類衡硤桧衲硲桼衾衿袂碇碎墓碕墖袗梜碝袟袠碣梣碤颫袱袴颴袷墺墻裀飂棆飆裌磎壑棒壓裕棙棜磟壡飡飣飥飦飧飪飫棬飮棯磯飲飳棶飶裷壻飼棽餂椅礆褆礇餇餈椊礎夐褐餑褕餕餖椗餗餘礙餚餛検礜餜褞餟餢椥椦餦大餧夨天太夫餫夬礬夭央冷餮夯夰餱失夲餲夳餳餴夵餵夶夷夸餹餺餻餼夾夿椿饀饁饂襂奃奄祄祅奆祆襆饆奇饇奈饈奉奎饎奏奐契祑饑奒奓祓楔奔饔奕襖套奘饘奙饙奚楚奛饛饜奝奞饞奟饟奠饠奡楡奢奣奥奧奨奩奪奫襫祫奬楬奭祭奮奯楰楱奲襳襻覉禊妋妎規禔榕妖妗榘榛覡覦榦禨令嶺羚聆馱鈴榲零観領禴妷榺禽秂駃駄姈槍觖駚秡秢秦秧姨秩觭駭槮駰姱姶槻姻姿樁騃樅樊騋稌騎騐験稔樕樛訞騠模稡訣騤訥稧権娭騰稰樸診樾驀驂穃詅詇穇橋橑驕穖驗穙橛機穡詥橪詮該橳穾婾婿突誇媋檎誒媖檜媞檢檣誣窣窬媵誶骸窺骻媼檿嫆嫉髎論諗諚諛竛諝諟竢嫪諭竭髮諮櫼嫽髽謁謄鬈嬌笑謑鬖嬙欞鬠欠次欣欤欧謨欨欫謬欬笭欯欲欵笶欶欷欸欹鬹欺欻欽款欿魃歃孅歆歇歉歊筌歌歍歎譏歐歒歓答歔魕歖魘歘歙歛譛歝歟歠歡譣孩魪筮筴筷魿殀讃殃殄宊宎讖殗箚定鮝讞箞実殠鮧鮬殮鮲殲殹容箺寄毉篋篌寏篏寔寞察寥篨寮鯰鯲鯵篵鯷鯸篸篹毹毿鰆簇簌簒尖尞尟氤簥鰧尬鰮氳簳谷谸谹谺鰺簽谽谾谿豁豅鱎汏籐籑豓鱖籘鱠籡豢籤籥屩汭汰籲豵決沃粏貐岑岒沗貘岟粢貪粭沴粹岺泅泆峇糇資糈賖糗賚賛糝泠峡糢糩峪泰賰泱賸鳹峽賿崁鴂鴃洇納崍崎洑崒鴒紒崘崙洟鴦崦紱鴺洽紾鴿洿鵊嵌鵐嵑嵜絝嵠嵢絥赥給絪鵪浴絵浹嵺嵾趁涂鶆鶊綌鶏趑嶔涔鶗涘涙嶛綟嶠鶡綣趦鶪趫鶬嶮嶴嶷綷綸趹嶺鶺趺綺綻趻嶽淀緃鷄跈跋跌淎淏鷖鷚鷞鷟淤跧跨淪巫淬鷮鷯淰緱跲淶淹緹添巻渀鸂渇丐鸑渓渕縕帙渙丙鸙渝踟踡縢两踦縦个踰縱踳渶踶渼縿繄繆湊蹊蹌繎蹐幐湑幕繚湜幞蹤繪幭蹶鹸幹蹹蹻鹼蹼幾纂纉麌庎躑纖溙躚庚亥麥座麨麩溪麪麬麭麮麯麰溱麳麴庵麵溶麸人躾庾滁滄仄今介从仐軑黔滕廕廖默黙仚軜廞軟黤令廥以軨軫廫黭軮黱仱滲黲黶价黻軼滿企漆弇輇伏伕众会漛漠漢輦漩漪輪漭輭弮伱漱輳輵伶鼷輸缺漻弻似缼轀轃齃潅齅轇轎轑佒齒齓齔轕齕齖齗彘佘齘余佚齚潜潝齝齞齟齠齡齢齣潦齦齧罨齨齩齪齬齭齮佮齯齰佱齱齲齳齵齶齷佺齺佽齽彾羃侅來羇侉侌羍美徐龑従龕侖徙羚從侠龠徠龡羡龢澣龣龥羨澨澪羭羮澮侯徯羯澰澳羹达俁俆濇翎俎翏翕俗忝俟俠翠俣翨快濬迭濮翰忲翳念俸俺忺濽俽"));
        PART_TO_KANJILIST.put('亻', new Part(2, "倀耀倁倄倅倆堆倇倊個倌倍倎倏倐們倒倓倔倖蠖倗倘蠘候倚倛倜倞借堠倡堡倢倣値怤倥倦倧倨倩倪倫栫倬倭倮倰栰倲進倳倵蠵倶倹倻蠼栿偀恁偁桁偂偃偅偆衆假偈灈偉灉偊偌灌行衍衎偎偏偐衑偑衒偒術偓衕偕灕衖偖街偗衘灘衙偙衚做聚衛衜停衝衞衟偟衠偠恠衡衢偢偣健偦顦偧顧偪偬偭偰偱偲聳側硴顴偵偶恷偸偽傀傁傃傄傅傆傊袋傍傎傏邏傐傑傒傓碓傔墔傖備傚傛炛傜條傞傟悠傠傡傢您傪催傭傯傰袱傲傳傴債袵傷傹傺確梻傽傾僀僂僃裄僄僅壅僇僊烋僌飌働僎像僐僑僓僔僕胕僖僘僚僜僝僞僟惟僢僣僤僥僦僧僨僩磪僭棭僮僯僱僲僵僶價僺僻僾儀儁礁儂儃億愆儆儇儈脈儉儋儌儍儎椎儒褒褓儔儕儖儗儘儙儚椛儛夜儜儝儞償儡儣焦儧椨儨脩優儬儭礭儯儱餱儲儳儴儵儷儸儹儺儻儼腋襍腐腑祔饔奞饠楩奪慫奮膂憄覆憊憔禦榫醮膲観榷膺觀臃准應燋懏凖姙駙臛臞臡凭槯懲觹懼懽觽燿觿戁戄樅騅舊稚訛爟樤権截娰戳樵穕鉜驟艧驩穫艭牮檇銜犨媬誮誰花檴芿櫂拊勧拵嫶勶勸髹苻欆權欏錐化欟嬥符笩茯匯猴錵茷茽荇魋譍荏筏荐歓筕存譙歡捬鍭筰筱獲荷護鎀玀玃莅讎讏讐鮒掖讙莜鮠推鎨箯宱鮲玳鮴箻宿鏅鯈寉篌篗鏙篠鏥鏦珩寯篵鯸簁搉氍鐎萑吪鐫鰷携鑃葆籆籊鑊葎屐屜籞葠屣履摧屧屨屩籬籮葰籰鑵豵鑼璀钁咃貅咐貛貜蒞璡蒦貨蒨撨岱貸咻咿糀擁賃糇蓓擕哘瓘蓚擢蓧哬擭蓯蓰賲糴糶蓷蓿鴈攉贋洑崔甕贗攜攞攤攫售唯鴴攸派鵂嵂鵆蕉絍絎敒絛浟嵟絥蕹鵺鵻薇喉嶊嶕涖薙閥維趯液嶲鶴旅藋鷕跗緥鷦淮緱緶鷹藺藿蘅瘊鸐帒鸖鸛鸜縦縧縮縱瘲帷蘸蘺蘿癁附癄噍幑癕蹜蹝蹤癥虧癨陮癯癰癱暀嚄躍準躙躛府皠庥躩躪蚹暹隹隺亻隻隼嚾雀什雁仁仂囃仃雄軅雅集仆雇仇軈雉囉雊雋滌雌雍仍雎仏雑雒仔雕仕雖他仗雘付雙仙雚雛黛仛曜雜蛜雝雞仞雟仟仠仡離仢難代仨滫仭囮仮仯仰廱仱仲仳黴仵件价任份仾仿伀伂伃伈伉伊伋伌霍伍伎伏伐休伒伕伖伙伜伝弣輦在伮伯估伱伳伴伵伶伷伸伹伺伻漼蜼似伽伾眾佀佂靃佃潅但佇佈佉佋佌位低靏住罐佐佑佒体佔何佖潗佗佚佛作佝佞佟彠你睢佣轥佩佪佬佮佯佰佳彳靴彴彵併佶彷佷彸佸罹役佹彺佺睺佻彼佼彽佽彾佾坿彿使往征侁徂侂徃侃径侄羅待侅徇垈很侈徉侉徊侊律例後徍侍侎徏侏徐侐徑侑徒侒従澓侓侔徖得侗鞗徘侘徙侙侚供徜徝依從侞侟徠侠御価垡徢徤瞧徧徨復循徫侫徬徭鞭侭微侮徯侯徰徱侲垳徳徴侵侶侷徸徹侹侻徼侼徽侽侾瞿便俀俁係促韄俄俅俆俈俉俊俋俌矍俍柎俏俐俑俒俔埖俗俘俚俛翛俜保翟俟俠信俢俣俤俥濩蟭濮修俯濯濰俰矱俲俳俵俶俸俺俼俽俾俿"));
        PART_TO_KANJILIST.put('麻', new Part(11, "靂穈蘑魔嘛糜瀝靡懡轣暦藦癧磨摩櫪歴蔴擵麻縻麼蘼麾麿醿"));
        PART_TO_KANJILIST.put('隻', new Part(1, "钁獲蠖護隻攫穫矍"));
        PART_TO_KANJILIST.put('隼', new Part(1, "凖準隼"));
        PART_TO_KANJILIST.put('隽', new Part(1, "携隽"));
        PART_TO_KANJILIST.put('皿', new Part(5, "蘆縊蘊簋爐謐贐縕鸕儖儘瀘謚礚猛鰛褞瘟簠搤温鰪洫鰮蘯蠱蠲氳錳搵尶昷笽血轀衁饁衂饂衃籃衄衅衆橆衈衉衊衋艋慍灎罏鑑湓豓豔籚轞孟敟葢驢恤襤轤艦塧潨塩鑪艫獰顱鱸卹楹獹聹鹽睾繿薀嚀膃熅瞌侐纑醓蒕溘隘醘醞殟螠溢醢嚧醯鎰榲薴檸檻媼榼皿鷁盁盂盃矃盅盆盈賉盉益蓋盋嗌盌藍盍寍盎藎瓐嗑矑盒壒闔盔磕盖盗壗髗濘盙臚壚盛盜盞韞盟盠盡諡嗢懢監盤盥盦寧盧懧櫨盨盪濫廬盬擰泴黸燼"));
        PART_TO_KANJILIST.put('仁', new Part(1, "仁佞"));
        PART_TO_KANJILIST.put('雁', new Part(1, "雁軅軈應鷹膺贋"));
        PART_TO_KANJILIST.put('黃', new Part(1, "曠黃廣擴礦簧壙鑛黌"));
        PART_TO_KANJILIST.put('黄', new Part(11, "癀黄鐄搆黆瀇黈纊黋黌壙鑛璜嚝曠潢廣儣兤礦簧横懬懭擴獷磺鄺熿"));
        PART_TO_KANJILIST.put('仄', new Part(1, "昃仄"));
        PART_TO_KANJILIST.put('集', new Part(1, "集襍"));
        PART_TO_KANJILIST.put('雇', new Part(1, "雇顧"));
        PART_TO_KANJILIST.put('曇', new Part(1, "曇壜罎"));
        PART_TO_KANJILIST.put('盈', new Part(1, "盈楹"));
        PART_TO_KANJILIST.put('雉', new Part(1, "雉薙"));
        PART_TO_KANJILIST.put('廉', new Part(1, "濂廉簾"));
        PART_TO_KANJILIST.put('車', new Part(7, "輀輂較輅鄆瘇輇輈載輊褌輌儎輏輐輒瘒輓輔輕輖輗輘輙輛輜輝輞輟輠鐡輡漣連輣輥輦輧輨輩輪蔪餫輬匭輭輮輯尰謰鰱褲鼲輳輴鐵輵輶輷錷漸輸輹堹縺輺輻渾輾輿琿轀轁轂轃轄轅偅轆煇轇轉運繋轌轍轎楎轏轑轒轓蹔轔轕鍕轗轘慙慚轜轝轞轟轡轢陣轣轤轥硨繫齳睴葷塹塼畽撃嶃嶄暈暉璉檋喗厙鶤庫暫斬皸皹墼箽囀懂範毄鏈車擊軋軌僌軍壍壎軏軑軒軔韗軛曛磛軜軟諢転軣俥槧鏨軨軫翬軭蓮揮軮軰軱惲埵軷軸嗹軹軺軻軼蛼軽軾蟿"));
        PART_TO_KANJILIST.put('今', new Part(1, "今貪含蔭棯陰鯰岑琴稔黔念頷唸捻矜衾莟吟衿"));
        PART_TO_KANJILIST.put('益', new Part(1, "鎰鷁溢隘益縊謚"));
        PART_TO_KANJILIST.put('介', new Part(1, "疥芥价堺介界畍"));
        PART_TO_KANJILIST.put('雋', new Part(1, "儁雋鐫"));
        PART_TO_KANJILIST.put('黍', new Part(12, "黐藜黍黎黏"));
        PART_TO_KANJILIST.put('軍', new Part(1, "諢鶤暈暉運褌軍揮葷皸皹輝渾琿"));
        PART_TO_KANJILIST.put('雍', new Part(1, "擁壅甕雍"));
        PART_TO_KANJILIST.put('盍', new Part(1, "闔溘蓋盍"));
        PART_TO_KANJILIST.put('从', new Part(1, "瘁萃倅樅來醉碎薔憖纖讖傘猝從挟徠侠覡莢檣誣麥座麩麪挫頬麭頰縱殲悴膵粹墻夾挾囃孅牆嗇靈篋从鋏坐鵐卒齒囓齔蓙賚陜雜齟翠齠穡齡峡艢埣齣蹤籤齦齧齪慫巫淬齬噬狭筮齲聳筴齶齷浹狹懺峽"));
        PART_TO_KANJILIST.put('黎', new Part(1, "藜黎"));
        PART_TO_KANJILIST.put('黑', new Part(1, "黠壥黥黨熏黯黑勳黔黴黶黷默醺儻燻黜黝點"));
        PART_TO_KANJILIST.put('黒', new Part(11, "戃纆黒纒黔黕默黙黛黜讜黝點黟黠黤壥黥黧墨黨攩黬黭黮黯黰黱黲黴儵黵黶黷黸濹儻嘿"));
        PART_TO_KANJILIST.put('滕', new Part(1, "籐藤滕"));
        PART_TO_KANJILIST.put('囗', new Part(3, "耀怇耇耈怊怐怗怘怙耜耞耟怠耠怡耬怳恍聒恕聖恖聟聤恩恪恫恬聰恰聵悁悃悄悋悎悒肖悗悚悞悟患悤悥悦悩悪惆惇惉惊胎惑惝惠胡胭惱胱胳胴惹愇愊愍愕脗愙感愪脰愰脱脳愴脵愷愹慁慅慍腒慝腡慥腦慬腭慱腷慾憁膃膅憇膈憍膏憒憓膕憘憙憚膛憝膞膢膨憩憬膳憸憺膻憼膽憾懀懁臂懃臅懆臉臊懍懎臘臙臝臠臡臨臵懶臺懽懿戀戁戃興舌舍舎舏舐舒戓舓舔舖或舗舘舙舚戜戢戦戧戫戰戲舸船扂扃扄艄扈艙艠艢扣艱艶艷抬拈拐拓苔苕拘苛招苟苢若苦苫括拮拾拿茄挄挌挐茖茗挙茣挩挭茴茵挶茹挹茼茽挽捁捂捃荅捆捉捌捎捐捒捛捨捭据荳荷掂莂掊掌掎莒莔莕莙莟掠莦莬掴掵莵掻揀菅揅菇揈菌菏揖揜菟菩菪菫損搔搞搥搪搭萵搵搶萼落搿葆葈葊葍摑摘摚摟摠葦葫摳葴葶摶葺撏撐撑蒒蒕撘蒟撟撣撹撼蒼蒽撾蒿撿擅蓉擉擋操擎擐擑蓑蓓擔蓖擗擘擡擣擱蓲蓴擷擻擿蔀蔃蔌蔎蔐攓蔔攔攘攙攜蔞攟攣攤蔥攩攮蔽蕁故蕎敔敕蕖蕗蕙蕚敝敞蕠蕢敦敧敬敲整敵數敺蕺敽薀斂斃薔斕薗薛薜斝薝薟斣薥薧斫斲斳薹藁旖藪藳藷藹藻藾蘄蘊蘍蘐蘑蘖蘗蘘蘞昫昭蘭虂晃晄晌晑晗虞號晤晧晩虩虫虬虯景虱虵虶虷晷虹虺智虻晾蚈蚊蚋蚌蚍暐蚑蚓蚕蚖蚘蚚蚜暟暠蚡蚣蚤蚦蚧蚨蚩蚪蚫蚭蚯蚰蚱暱蚳蚴蚵暵蚶蚷蚸蚹暻暼暾蚿暿蛀曀蛁蛃蛄蛅蛆蛇蛉蛋蛍蛎蛑蛒蛔曔蛕蛗蛙蛚蛛蛜蛞蛟蛠蛣蛤蛥蛧蛩曩曫蛬蛭蛮曮蛯蛸蛹蛺蛻蛼蛽蛾蜀蜂蜃蜄蜅蜆蜇朇蜈蜉蜊蜋蜍蜎蜏蜐蜑蜒蜓蜔蜘朙蜙蜚蜜朜蜞蜟蜡蜣蜥蜨蜩蜮蜯蜱蜲蜴蜷蜹蜺蜻蜼蜽蜾蜿蝀蝃蝅蝉蝋蝌蝍蝎杏蝓蝕蝗蝘蝙蝝蝟束蝠蝡蝣蝤蝥蝦蝨蝪蝮蝯蝱蝲蝴蝶蝸蝻蝿螂螃螄螅螆螇螈螉螋螌融螐螓螕螗螘螙螞螟螠螢螣螧螫螬螭螮螯枯螱枱枲螳枳枴螵枵架枷枸螺螻螽螾螿蟀蟁蟄蟆蟇蟈蟉蟊蟋蟎蟐蟒蟕蟖柗柘蟙蟚蟜柜蟟蟠蟢蟣蟤蟪蟫柬蟭蟯柯蟱蟲蟳蟶柶蟷柷蟸蟹蟺蟻蟾蟿蠁蠃栄蠅蠆蠉蠊蠋蠍蠎蠏蠐蠑蠒蠓蠔蠕蠖蠘蠙蠚蠛蠜栝蠞蠟蠡蠢蠣蠧蠨蠭蠮蠰蠱蠲蠵蠶蠹蠺蠻蠼格桄衉桐桔衕桘衙衚衛桜衞档桮桯桰衰衷桾袁袈梈梏梚袞梠梢梧梪梮梱梲袷袺袽裀裋裍裎棓裔裕裙裛棝裞棠棫裯裳棺裾椁椅椆椋椐褒褓褘検褝褞椢椥褸襁襃襄楅楉襌楓楛楜襜楝襞襠襡襢楫襭襰襴極襷襺楼襽榀覇覉覊榍榕覗覘覚覠榥榦覬榱榲覴榾覿榿觀槁槅槌槍槑槖槗觥触槨槫觫觭觱槵觶觸觿樀言訂訃訄訅訇計訊訌討訏訐訑訒訓樓訔樔訕樕訖託記訛訝訞樞訟訠訢訣訤訥訦訪訫訬設訯許訳訴訵訶訷樹診樻註証訽訾樿詀詁詃詅詆詇詈詉橋詍詎詐橐詑詒詓詔評橕詖橖詗詘橘橙詛詜詝詞詠詡橡詢詣詥試詧橧詩詫詬詭詮詰話橱該橲詳詵詶詷詹詺詻詼詾橾詿檀誀檁誂誃誄誅誆檆誇誉檉誋誌認誏檐誐誑誒誓誕誖誗檗誘誙誚檛檝語誟檠誠誡檢檣誣誤誥誦誧誨誩説読誮檮誯檯誰課誳誶誷誹誻誼誾調櫁諂諃櫃諄諆談諈櫈諉櫉諊請諌諍諏諑諒諓諔諕論諗諚櫚諛諜櫜諝櫝諞諟諠諡諢諤諦諧櫧諫諬諭諮諰諱櫱櫲諳諴諵諶諷諸諺櫺諼諾諿謀謁謂欃欄謄謅欅謆謇謊權謋謌謎謐謑欒謔謖謗欗謙謚講欛謜謝謞欞欟謟謠謡謦謨欨謫謬謭欯謰欲謳欶謷謹欹謼謾譁譂譃譄譅譆譈證歊譌歌譍譎歎譏歐譒歒譓譔譖歖識譙歙譚譛歛譜譞譟歡譣警譫歫譬譭譯議譱譲譴譶護譸歸譹譼譽譾讀讁讃讄讅殆變讋讌讍讎讏讐讒讓讔讕殕讖讙讚殛讜讞殞讟殣殨殪殫殮殰毃毄毆毚毡毫氄氅氈氉民氓氤氳谷谸谹谺谽谾谿豁豅豆豇豈豉豊豋豌豎豏豐豑豓豔象豪豫豭貂貉貔沖貙貛沰河貴貺治沼貼沽貽沾沿賀況賂泂泅泗賙賞賠泯賲賷贃洄洇贍贏贒贖洛洞洳洸活洽浞浥赧浧浩赮浯浴浼超趈消涑涒涓涗趙趟趦涪趫足趵趷涸趹趺趻趼涼趾跀跂跅跆跇跈跊跋跌淌跎跏跑跔跕跖跗跙跚跛距跟跡淢跣跤跥跧跨跪跫跬路跰跱跲跳淳跴践跼跽跿踁踄踅踆踈踉踊踋踏踐踑踔踖減踝踞踟渟踠踡渢踢踣丣踦渦踧踪中渮踰踱串踳踴踵踶渶踷踸渹踹踽蹀蹁蹂蹄湅蹇蹈蹉湉蹊蹋湋蹌蹍湎蹎蹏蹐湒蹔蹕湖蹙蹛蹜蹝蹞蹟蹠蹡蹢湢蹣蹤乨蹩乩蹬蹭蹯蹰乱蹱蹲蹴蹶蹹蹺蹻湻蹼躁溂躂躃躄躅躇躉躊躋事躍溏躐躑躒躓躔躕躙躚躛亜躝躞亟躡躢溢躧亨躩躪享京亭亮溮亯亳溳躳亶溶溷亹躺軀軁軃滄軆軇滈滉滍滬滭軮仲滴滷滸軹軺軻滾輅漊漌漍輓輖漚輝漢輨漪輬輯估漱伵漶輷伺輻伽佀轀轄轅轉佋轎潏佑潒潓佔何轗轘佝潝潞轡潡佪潬佮潯潰佶佸使侃澄侊澋澍澎澔侗辜辝辞侞澟辟澡辣辥澧澮辯澰澶侶侷澹侻澼濁俁促濇俈濈俉俋俏俗迚俛俜保濠迠信迢俣濤迥迦迨俰迴迵濶濹追倁适逅瀆逈倉個倌逌倍逍逗倘這倚瀛倜逞倞速造逧倨瀬逭瀯週瀲倲倳逸倹瀹逼瀼瀾灃偉灉偊灊灌偌遌灎過遐偑灔違灘做停灝灞遠灣遣灤遦適偪遬遹遺避還邅邉傊邋邎傏傐邑邕傖傛傠炤傯炯邰炱傳傴炴邵点僂郃僅郈僉郌像僐僑郒僓烔僖僘郘烙僜郜烟郡郢郤僤僦部郭烱郶烹僺僻鄀鄂儃焄鄅焅儆焆儇焇儈儉儋焏儔鄖鄗鄙儙焙焞鄞償鄧鄩鄯儲鄲儳儴鄶鄷儺儻儼酃兄光克兌免兎煐煒兔兗兘党兢酤兤煦照酩酪酲酷熀熅醅醇熇冋冎醎冏醐熔冕醕熙熚醞熟醢冤冨熯熰冲醴况冶醶醸熹熺醻醼醽冾釀燀凂凅釅燈燉凉凋减凒燕燖凛凜營燥釦燭燮凱凳凾燾爉爚爛爟爤別爨刮鈳爴鈶鈷牁鉂剅牆鉆鉊削剋剌鉐牕剖牘鉙剚牚鉛剞剠鉠剣鉤剮副牯鉰剱牱割剳剴鉵創剸鉻牾牿鉿剿劀犄劅銅劈犉劊劍劒犒劔劖銗銘劚銛加犢銧劬劭劯労劶銷劼銽鋀勀鋁勅狆勉勍勏勒狗鋗鋘鋙勛鋜鋡狢勤勥鋥勦勨勪独鋭狷勷鋸勸狺鋿猄匌匐猓猗匘猞錞猟錡猢猧錧錮匰匱匲匳匵匼匿猿區獃獄鍄獅鍈鍋鍔単卛獝占獣卣卥鍧獧獨鍩獫卲獵獸獺卻獻鍼卾玁鎋鎔鎕鎖鎗鎘鎚鎞鎤鎧厨厪鎬厰厳鎶玷玽玿珂鏄珆鏆珈鏉鏑叓鏓珖叚鏜珞口鏤古句另珦叧叨叩只叫召叭叮可台叱珱史右叵珵叶号鏸司珸鏹叺珺珿琀吁吂吃各琄合鐈吉吊吋同名鐍后吏吐向琑鐓吓鐙吚琚君吝吟吠鐡吡否琦吧吨吩吪含听吭吮启琯吱琱鐱鐲吴鐴鐵吵吶鐶吸吹鐺吻吼吽吾瑀呀呂呃鑄呄呆呇呈呉告瑋呍呎呏呑鑓瑙瑚鑜鑞呞呟瑢呢瑣呤呦呧周呩呪呫呭瑭鑭呮呰鑰呱鑱瑲鑲味呴呵鑵呶呷呻呼命鑾瑾呿咀咁璁钃咃咄钄咅咆咈咉咋和咍咎咏璐咐咑璒咒咕咖璚咜咟璟咡咢咤咥咦璧咧咨咩咪璪咫璫咬咭璮咮咯璯環咱咲咳咷咸咹璹咺咻咼咽咾咿哀品哂哄哆哇哈哉哊响哎瓓哘瓘瓛哠員哢瓤哥哦哨哩哪哬哭哮哯哲哶哺哼哽哾哿瓿唀唁唄唅唆唇唈唉甌唌唍甎唎唏唐甓唔唕唖甗甜甞唪唫售唯唱唲唳唵唶唸唹唻唼唽唾啀啁啄畄啅商啇啉啊啌啍問啐畐啑啓啖啗畗啘啚啛啜啝啞啠啡啣啤略啦畧當畸啻啼啾啿喀喁喂喃善喆喇疇喈喉喊喋疎喎喏喑喒喓喔喗喘喙喚喜喝喞喟閣喣閤喤喧喨閩喩喪喫閫喬閭喭單喰閲喲疴営喻閾喿痀嗁痁痂嗃嗄嗅嗆闆嗇闈嗉闊嗋闌嗌痌嗎嗑嗒闓嗓嗔嗗痘闘嗘闙嗚嗛嗜痞嗞嗟闟痟闠闡闢嗢嗣嗤闤闦嗩痯痴嗶嗷嗹痼嗽痾嗾嗿嘅嘆嘈嘉嘊瘋瘌嘍嘎嘏嘐嘑嘒嘔瘕嘖嘗嘘瘙嘙嘛瘟瘡嘩瘩嘬嘯嘰嘱嘲嘳嘴嘵嘶嘷嘸瘸嘹嘻瘻嘼嘽阽阿嘿噀噁噂噃噄噆噉癉噋癌噌噍噎噏噐噔癖陗噛噞噠噡噢噣噤噦器癩噩噪陪噫噬噭噯癰噱癱噲癴噴噵噸険噺登嚀嚄嚅嚆嚇嚈嚊嚋嚌嚏隑皓隔嚔隕嚕嚙皚嚚隚皜嚝皝嚞嚟嚠隠嚢隤隥嚥嚦隦嚧嚨嚩險嚫嚬嚭嚮隯嚱嚳嚴嚶皷嚷嚼皽嚾囀囁囂囃囅盅囈囉囊雊囋囌囍囎盎囏囐囑盒雒囓雕雖囗囙囚雚四囜囝雝回囟雟因囡団難囤囥囦囧囨囫盬园囮困囱囲図囶囷囹固国囿圀圁圂圃圄霄霅圇圈圉圊國圌圍圏圑霑園圓圕圖眗團眙圚圜圝霝眠霣眮霱露霸霹靄靈靊睊睏靗靜靠靣靧坧革靪靫坫靭靮坮坰睰靱靳靴靶靷坷靸靹靺靻靼靽靿鞀鞁鞄鞅鞆鞉鞋垌鞍瞎鞏鞐鞕垕鞖鞗鞘鞙垙鞚鞜鞞鞟鞠瞠垢鞢鞣瞥鞦垧鞨瞪鞫鞬鞭鞮鞱鞲鞳鞴鞵瞶鞶鞸鞹鞺瞻鞼瞼瞽鞾鞿韁韃韄韅韆韇韈韉韊韋韌韍韎韐韑韓韔矕埕韗韘韙矚韛韜韝韞矞域矟韠埠韡韤知短矯矰石矴韶矸培韺韻矻埼矼砂堂砅砆砉堌砌堍砍砎砑砒研砕堛頜堝砝砠堡頡砡砢砣砥砦砧頭砭砮砰砲破砵頵頷砷砺頼砿堿硃硄硅硇硈顊硌額顎硎塏顑硒塔顖顗塘塙硜硝硞硠硡硣硤塤顥硨塩硪顫硫硬确硯硲硴顴塸硺硼塾硾塿碁碆碇碊碌碍碎碏墐碑碓碔碕墖碗碘碚碝碞碟墠碡墡碣碤碧風碨碩墩颪碪颫碬颭碭颮碯颯颰碰颱墱碱碲碳颴碵颶颷颸颺確墻颻碻碼碽碾颿碿壁磁飂飃飄飅磅飆磆壇磇壈飈磈磉磊磋飌壌磌磎壎磐壑磑磒磓磔壔壕磕磖磚磛壝磟磠壡磡壤磤磦磧磨壩磪磬磯磲磳磴飴磶壷磷壹磺磻飼磽壽磿礀礁餂礆礇餇餉礌礎礐礑礒餖餗礙礚礜礞礟礠夡餢礥礦礧館礩礪礫餬礬礭礱礴礵餹饂奆奇饇饋饍饎祏饐祐饕饘祜祝奝饞饟祠奩祫奲祻禀如禅禍福禕妕馘禧禪妬禮禱禳禴禹馽姁駉始姑駒駕駘駚駛姞駟姤秥駧駫姯駰駱秱駵姶秸姻駽駾稇程稍税騎騒験稕稛騞稟娟稠騢娧騧騨娩娪稫娯騷稾稿婀婁穃婄驄驅驕驖穗驗驚驝穡穣驤驩穰婷窖窗窘媧窩媬媲窶媸窹窻媼骼嫆竆髏髑竒體嫗高站髛髜髞嫡嫥竦嫦嫩髫竬竱競髺髻嬁嬉嬋嬌鬍鬎嬖嬗笘嬙鬙嬛鬛笞鬟鬠嬢鬣笥鬪鬭笱鬲笳鬳鬴嬴笴鬵鬷鬹鬺鬻嬾笿孁孃筈魈孌筎筒答魗筥学孰筲孼孽孿箇箉鮉鮎鮐鮔宕鮖箘官箚鮚管客鮦箪箬宭宮鮰宲害箴宵箶鮸容鮹宺寃寄富寏篔篖篙鯛鯝寠寤篦鯨篩寰簂簄尅專簉鰊尋尌簌簍鰏鰐簑簓簔鰔鰕鰙尚簣鰤簥簦簬鰮就尵簷簹簽局鱄居鱊籌鱎屑鱓籔鱔鱘屙鱚鱛籝属鱞籟籡屢鱣籣籥鱧鱨屨屩屬籲鱲鱵鱷粔粘岠粡岢岣粤岩粭岲岵岷岾峇峉糊糏峒糔糖糙峝糦糩峪糫峭峮鳴糵崎鴝鴞鴣鴬鴰紹鴼鴽紿鴿崿絅鵅絇嵐結鵑嵒絖絗嵜鵠絡嵢鵣給絧嵩絪嵪絮鵰嵹絹絻絽嶁綂綃嶇鶉綌綑嶒鶒綗鶘鶚嶝嶠綢綧鶫鶬嶮鶮綰嶹綹綺嶽巇巉鷊巋巌緎巐巒鷕巖鷗緘巘緙緝緡巣巤緥鷧鷮緯鷰鷲鷸巸鷺緺縀鸇鸊縋鸑縕帖縗鸙鸛縜鸞縞帥縨帨師縳縷常總幃幅幌繐繒繕幗繘幜繟繢幣幤繥繦幨幬繭幮繯繰鹵鹸鹹鹺鹻鹼鹽繾纁纆纈續麌麏纔麕纕麖店纞庽麿黋黏廑廓廔黕廚點黟廠黠黤黥廥黧黨廩黬黭黮黰廱黱黲黵黷黸廻弇鼈鼉弊鼓鼔鼕鼖鼗鼙鼚鼛鼟鼡弡鼦弨鼫鼯弰弴弶強弾缿彁齁彄齆彈彎罐当齝齞罟罠齠彠齣罥彧齪彫齬彭罭齮罰影齲齶罸罾羂羇羈徊龕龗徜龠龡龢羣龣群龥徫羶羸翀翕忠忡翮翯翾翿"));
        PART_TO_KANJILIST.put('付', new Part(1, "附符椨拊柎俯咐腐腑鮒付苻府坿"));
        PART_TO_KANJILIST.put('囚', new Part(1, "薀榲饂膃泅囚蘊媼慍褞鰮瘟"));
        PART_TO_KANJILIST.put('四', new Part(1, "圀泗四駟"));
        PART_TO_KANJILIST.put('廛', new Part(1, "躔廛纏"));
        PART_TO_KANJILIST.put('雜', new Part(1, "囃雜"));
        PART_TO_KANJILIST.put('回', new Part(1, "檀禀擅牆嗇壇氈徊懍薔蛔圖鄙凛凜回稟穡艢檣廩顫茴迴亶羶墻廻"));
        PART_TO_KANJILIST.put('因', new Part(1, "因氤茵恩姻咽烟"));
        PART_TO_KANJILIST.put('盡', new Part(1, "贐盡壗儘燼"));
        PART_TO_KANJILIST.put('離', new Part(1, "離籬"));
        PART_TO_KANJILIST.put('代', new Part(1, "岱代玳垈貸袋黛"));
        PART_TO_KANJILIST.put('難', new Part(1, "難攤灘儺"));
        PART_TO_KANJILIST.put('廣', new Part(1, "曠廣擴礦壙鑛"));
        PART_TO_KANJILIST.put('監', new Part(1, "鑑監籃襤儖艦檻濫藍繿"));
        PART_TO_KANJILIST.put('令', new Part(1, "聆蛉鴒苓領羚怜齡齢令令澪嶺冷羚聆鈴玲零領鈴伶零冷囹嶺"));
        PART_TO_KANJILIST.put('以', new Part(1, "苡以似"));
        PART_TO_KANJILIST.put('盧', new Part(1, "驢轤蘆盧櫨鑪艫廬爐顱瀘鱸臚"));
        PART_TO_KANJILIST.put('雨', new Part(8, "需霂霃霄霅霆震霈攉霉霊礌霍霎蘎漏霏霑儒霓蠕霖鸖樗霙霚欛霛霜霝霞欞鄠霡霢霣霤霧霨霪嬬礭霰樰霱露鐳霳礵霸霹瘺霽霾孀孁靁靂虂酃靃靄靆鱈靈靉靊轌罎靎靏橒蕓屚轜艝灞襦繧癨鱩顬摴孺繻蕾嚅澐檑龗膤澪零薷檽醽擂曇臑壖臛壜濡懦雨壩擩雩雪雫嫮糯雯雰雱雲零雷燸雹櫺雺電藿"));
        PART_TO_KANJILIST.put('黨', new Part(1, "黨儻"));
        PART_TO_KANJILIST.put('雪', new Part(1, "膤鱈雪轌艝"));
        PART_TO_KANJILIST.put('目', new Part(5, "瀀頁頂頃項順瀆頇須頊頌頍頎頏預頑頒頓頔瀕頖頗領頙蠙頚怚倛頜耝頞頠砠頡耡頣頤値頥頦頫瀬頬頭頮頯頰耰頲頳頴瀴頵倶頷頸頻頼蠼頽頾遁顄顆顇灈遉顊顋衋題額顎顏顑顒顓道顔顕顖顗偗願顙顚顛衜灝類衟衠塡顢衢顣塤顥顦遦顧顪顫填顬顯息硯顰顱偱顳顴側偵聵遺傄邉傊邊傎悓梖梘碩墳悳債碵颶碽傾磌磒僓壝郥惧磧僨想價惻郿植夏夐儐夒夓夔愖鄖脜償礥儧儨礩愪優儬儭鄮礵儹愼鄼襀楂酇饋慎祖饙奡慣楣襣楨煩襭楯腯襯襰具慻憂熄憄見覍榎禎規覐冒憒覓覔覕熕視首覗馗覘馘覚覛禛覜冝覟覠覡憤覥覦覧憩覩膩親嶺覬榭覯覰熲覲観領覴覵覶覷覺覼覽覿觀槇燌臏姐槓駔臗槙臞租自臫臬臭臰臱臲懶懸槻懼戄戇樌稙戛戝樝戞稹樻爼則積穎艏鉏牘詛窅劓劕劗媚媜檟犢媢助檱媳檳窺誼櫃竇鋇櫌櫕髕髖勖勗狙勛苜櫝勣鋤鋧勩竩櫬苴櫻狽欑鬒欖鬚欟鬢嬪嬰匱匵嬶鬹嬾孀獖鍘鍞筧筯獱歵獺孾讀殂讃玃莇鎖殖算讚宜殞讟殠厠厦莧殨鎭鎮殯殰箱鎴箵鎺揁鏆篔寘寛叠寡叡實寱寳寴寶毷揸篹菹現簀鰂氈損氍導簒萓簣萯尵瑁葅鑌籑屓鑕葙鑚葚摜鑜鱝籟鑟瑣摣籩屭籰瑱籲豶鑽咀钁粗貜貝貞負財貢貤貧粨貨岨販蒩貪貫責貭貮沮貯貰咱貲貳貴貶買貸撹貹貺費貼貽貿璿賀賁賂賃賄賅賆資賈瓉賉瓊賊賋賍賎賏賑賓瓔賕賖糗賙賚瓚賛賜賝賞賠員賡賢賣擤賤賦賨質泪賬賭哯擯賯賰賲峴賵擷賷賸賺賻購賽擾賾賿贁贃唄贄攅贅贇贈贉贊贋贍洎贏贐贒贓贔贖甖攖贗贛攢攪攫攬崱蔶組蕆嵋蕒敗鵙蕡蕢赬畳蕷絸嵿疂趄疊薋薠鶪趲閴嶺疽闃嗄嗅鷆巎鷏闐闑巓嗔緗巙跙闚藚闝闠嗩緲藾嗿蘋嘎鸎且嘖鸚瘜縜鸜縝渞蘡渣縣測嘳嘷渺阻渻帽績幀虁湄蹎湏幘湘虘晛蹞湞癟蹟繢湨癩癪癭癯癲噴噵噸繽纂纇纈纉嚊續纐纓躓隕纘纛纜皟隤皥躩嚫嚬溳嚶亹廁囂廂蛆滇廈囋廎囎雎盨目黰盰盱盲直盶黷相盹盻盼蛽盾眀省眄蜆眆眇眈眉眊看県鼎眎鼏鼐鼑鼒眒圓眕眗眙圚眚眛霜眜眞真眠眢霣眤眥眦眨眩漬眭眮眯眴輴眵眶眷眸伹眹眺鼻鼼眼鼽眽鼾鼿着潁齁睂齃齄齅睅齆睆齇睇睊罌睍齎睎睏轒睒睖睗靚睚睛睜罝睞齟睟睠睡睢督睤睥坥靦睦靧睧睨靨睫睬置潰睰睲睳睴睹睺靻睽睾睿瞀徂澃瞄螅瞋瞌瞍瞎瞑澒瞔瞕瞖瞚徝瞞瞟辠瞠瞢龥瞥澦瞧循瞪瞬瞭瞮瞯瞰瞱瞳瞵瞶瞹瞻鞼瞼瞽瞾瞿矃濆韇矇矉矍俎矑矒俔矕矗矙矚濞柤埧濬濱埴濵翺濺韻査濽"));
        PART_TO_KANJILIST.put('曰', new Part(4, "謁鬄朇蔇餇渇昍鄍蠍褐鰑昒鐔鐕昖倝昝蠞簟鐟餟戠昣鈤昦鰨昩匫倬鐯椱猲瘴餴鄶昷踷焸昹餹鬺鰽逿晀齃齄靄噆晆饇歇蕈偈晊煋蝎遏嵑偒織驔睗識晘鑙饙譚煚饛葛幜獠遢楦瑨潭晵職晹鍺晼畼陼晿嶂墇掉璋貋溍碏澓鶕鶗麘暚嶛暛馛暜馝喝趞暟殟馟趠躢碣撦鞨斮璮亯誯羯掲傹箺溻誻傽涽曂篂曃緄韅勆緆臈曌緍揎曎郒曔凕蟙廜狟廥柦飦翨曫竭曮賯曰磳曵旵飶曷藹鯹蟺嫽旾旿"));
        PART_TO_KANJILIST.put('困', new Part(1, "困梱悃"));
        PART_TO_KANJILIST.put('雲', new Part(1, "雲靆曇繧靉壜罎"));
        PART_TO_KANJILIST.put('曲', new Part(1, "儂椣濃腆軆鱧豊遭禮曲農醴體漕艶髷典曹艚槽膿糟"));
        PART_TO_KANJILIST.put('曳', new Part(1, "曳曵洩絏"));
        PART_TO_KANJILIST.put('廴', new Part(3, "梃霆艇之鋋唌鋌犍涎乏埏蜑蜒蜓旔誕綖楗莚泛莛芝脠徤健鞬庭腱頲筳廴鍵筵揵貶延廷廸侹挺建廻廼珽"));
        PART_TO_KANJILIST.put('滴', new Part(11, "樀蔐嫡鏑讁歒滴敵摘適謫擿"));
        PART_TO_KANJILIST.put('更', new Part(1, "粳更甦梗峺硬哽鞭便"));
        PART_TO_KANJILIST.put('直', new Part(1, "悳直値埴殖矗稙植置"));
        PART_TO_KANJILIST.put('零', new Part(1, "零零澪"));
        PART_TO_KANJILIST.put('延', new Part(1, "蜑蜒筵誕延莚涎"));
        PART_TO_KANJILIST.put('雷', new Part(1, "擂雷蕾"));
        PART_TO_KANJILIST.put('廷', new Part(1, "梃霆廷艇挺庭"));
        PART_TO_KANJILIST.put('曷', new Part(1, "謁碣靄歇渇偈臈鞨蠍竭蝎羯遏褐掲曷藹葛喝"));
        PART_TO_KANJILIST.put('相', new Part(1, "孀箱廂想相湘霜"));
        PART_TO_KANJILIST.put('黹', new Part(12, "黹黻黼"));
        PART_TO_KANJILIST.put('曹', new Part(1, "漕曹艚槽遭糟"));
        PART_TO_KANJILIST.put('固', new Part(1, "凅箇涸固個痼錮"));
        PART_TO_KANJILIST.put('建', new Part(1, "腱健鍵建"));
        PART_TO_KANJILIST.put('任', new Part(1, "恁賃袵姙任凭荏"));
        PART_TO_KANJILIST.put('曼', new Part(1, "蘰慢蔓幔縵饅鬘漫鰻曼鏝謾"));
        PART_TO_KANJILIST.put('黽', new Part(13, "澠鼂鼃蠅僶鼇竈鼈繩鼉黽黿"));
        PART_TO_KANJILIST.put('国', new Part(1, "椢掴国"));
        PART_TO_KANJILIST.put('曽', new Part(1, "甑層僧増贈噌曽囎憎"));
        PART_TO_KANJILIST.put('廾', new Part(3, "鐁鰈蠎倎蠐瀑簒研萕耕倛簛堞借栟氟尠逩堪琪倫尭頮頯簱簸瀹萹塀葉葊豊籍遍硎偏屏呏籏摒展屜灞聠籥鱧汧屧聨汫葬遭恭呭遮遰鑰籲桺瑾咁碁咈悈墈墐撕碟邢岪碪邯械貰墳梻碾僀賁糂棄糄泄峅賆棆蓆棊棋裓泔飛惜飜糞糟擠磡哢擤壩蓱惵胼惼儂餅褊蔕儕愖蔗甘鴘崘崙甚甜甞鄞椣攤紲餴椹紺儺焼紼畀煁腆饉畊嵌奔腗煠酣襣絣煤楪慬異鵲楳其典慸祺啿慿冀憇覇覉斉覊冊醋喋開妌妍斎斒禖閞斟憤禮斯熯覲疳斳醴禴馵綸薺趼膿疿懃跇淇藉藊臍姍巐鷓燕緕旗凘緙臙跚淠臡駢懣嗣緤編淪跰駴駵巷痹槽槿渀舁戁爆昇済騏刑踑戒昔世丗稘鸙騙爚蘛戡渡阢横昪刪渫席帯港阱刱戴嘶帶踸丼蹀蹁扁牃湈幉詍剏艑牒鉗鉘剘艚蹛湛艜陞癟蹠蹣剤驥穧癱艱艴噴艶鉶鉼穽満暁纂纃蚈嚊躋嚌抍劑媒麒劓井媟誡嚥度隮暴暵蚶庶骿軆竎囏廑拑髒勒體論勘拚髜諜曝廝滞諞勩黭黮滯囲曲髷苷曹拼廾廿滿謀茀錀弁异弃弄笄弆圇弇弈弉弊漌漕鼖欛朞蜞期錤匥笧輪漭錯笰伳挵猵嬶霸笹欺鼻鼼弽鼽霽輾鼾匾缾鼿齁齃齄齅卅齆齇升卉荊齊齋魌鍍齎齏荓鍖蝙卙彛彜彝形睤坩革靪靫靭筭靮鍱靱靳靴併蝶靶靷靸靹靺靻靼靽鍽靿鞀鞁鞄掄鮄鞅枅鞆羇羈鞉型鞋澌讌鞍鞏龏鞐宑箕鞕鞖侖鞗算鞘鞙鞚鞜箝鞞瞞鞟鞠龠枡龡鞢龢殢鞣殣龣龥鞦徧鞨鮩措厪垪鞫鞬鞭厮鞮螮鞱農鞲鞳鞴玵鞵鞶鞸鞹鞺枻枼鞼莽鞾莾鞿羿韁濃韃韄韅揅濆韆篇韇韈韉韊珊篏某柑寒蟒揕蟖韘揜濞濟迣翩揲韲柵迸基翼鯿"));
        PART_TO_KANJILIST.put('曾', new Part(1, "曾囎"));
        PART_TO_KANJILIST.put('盾', new Part(1, "遁循盾楯"));
        PART_TO_KANJILIST.put('廿', new Part(1, "鐁戁鰈騏踑世丗稘倛簛蘛堞尠戡渡堪琪渫席帯簱嘶帶簸踸蹀牃湈葉幉詍籏牒鉗剘湛屜灞蹠蹣屧呭遮癱艱瑾満咁碁墈墐媒麒撕媟碟嚥度碪邯貰暵蚶庶糂泄棄蓆棊棋囏拑廑勒泔勘諜廝滞磡勩壩黮滯惵苷廿滿謀漌蔕愖蔗甘甚欛甜朞甞蜞鄞期錤攤紲伳霸笹椹欺紺儺弽煁饉嵌魌鍍鍖卙煠酣煤坩革楪靪靫慬靭靮鍱靱楳靳靴蝶其靶靷靸靹祺靺靻靼靽啿靿鞀鞁鞄鞅鞆憇覇羇羈覉鞉覊喋鞋澌讌鞍鞏鞐箕鞕禖鞖鞗鞘鞙鞚鞜箝鞞瞞斟鞟鞠鞢鞣殣鞦鞨厪鞫鞬鞭厮鞮斯熯鞱鞲覲疳鞳斳鞴玵鞵鞶鞸鞹鞺枻枼鞼鞾鞿韁韃懃韄韅韆跇淇韇韈韉韊篏某柑鷓揕燕蟖旗韘凘緙臙臡迣懣緤揲基槿"));
        PART_TO_KANJILIST.put('替', new Part(1, "僣譛潜替"));
        PART_TO_KANJILIST.put('滿', new Part(1, "懣滿"));
        PART_TO_KANJILIST.put('最', new Part(11, "最樶撮"));
        PART_TO_KANJILIST.put('蜀', new Part(1, "蜀囑濁髑躅獨觸矚屬燭"));
        PART_TO_KANJILIST.put('需', new Part(1, "需濡臑儒蠕懦襦孺繻嬬轜糯"));
        PART_TO_KANJILIST.put('弁', new Part(1, "弁弃峅"));
        PART_TO_KANJILIST.put('會', new Part(1, "鱠會薈獪繪檜膾"));
        PART_TO_KANJILIST.put('弄', new Part(1, "哢弄"));
        PART_TO_KANJILIST.put('眇', new Part(1, "緲眇渺"));
        PART_TO_KANJILIST.put('月', new Part(4, "蠃倄琄將堉堋萌逍蠏琑萓堕鰖搖倗鐗簙瀛瀞萠搠簠砠耡瀧鰧倩栯搰逳耼逾偁偂豅鱅塉塍籐屑塑籘遙瑚豚衚瑜硝籝灞籠遡瑤葥偦鑨葪葫确遯桷偸衻硼聾咀悁邂悄墉肋肌肎貐悑撒肓肖粗肘炙肚撚肛肜肝肞股梢肢撤肥肦肧蒨岨肩肪肫肬肭墮沮肯蒯傰肱蒱育肴蒴肸肹肺精碿郁胃賄胄情胆磆蓇棈糈胈糊哊背胍胎瓏糏胏胒壓胕胖胗胘胙郙棚胚胛胝胞壟胠胡胤胥擦哨僩壩擪峭胭胮胯惰胰胱胲胳胴擶胶胸賸胹擺胺壻胼能礀脂脃脅脆焆脇焇脈愈愉脉脊脋贏攏愑褕脖脗脘鄘褙脚餚脛脜褜脞脠褠蔡蔣脣崤脤褦洧甧脧崩脩甩餬愬儬脬脯脰脱儱礱脳蔳礴脵愶然脹脺餺脼脾組腅腆腇襉腊態腋腌腎煎腑鵑蕑腒腓腔楕腕祖腗奛楜饜腟奟腠楡腡散奣鵤腥腦腧腨腩鵩兪腫奬鵬腭酭祭腮腯腰腱襲酳畳腴腷腸絹腹煹腺酺腿膀膁膂疂膃綃膄鶄膅膆覇消膈熊膊疊膋榍膎膏醐冐冑醑涓膓冕膕膖膘鶘趙膚膛斛膜憜膝膞喟膠斠膢薢膣膤覦膨喩膩憪綪膮膰嶰膲膳綳膴膵覵膸鶺膺榺喻鶻膻膽疽榾膾膿臀跀臂鷂燃巃臃臅淆臆臈懈臉槊臊嗋臍臎痏臏姐臑角臓觔觕懕臕觖觗燗嗗臗觘臘臙觚臚臛觜觝淝臝臞槞臟痟租痡解藤觥触觧觩觫觭緭淯觱槲觳鷳觴觶姷觸觹觽駽觿樃清鈅瘉稍明蘎且刖蘛渝娟瘠縢蘢渣戫渭爯踰騰稰嘲嘴阻爼繃艄癇削前陏牏蹐幐湑癒湔湖繖牖陗詛繝晡橢婧剪橳晴婾婿隃嚈隋随誚際檞媠蚦誧隨骨嚨助骪骬窬骭骮骯溯骰窰骲隳骴隴骵媵骶銷骸骹骻誼骼骾骿檿髀髁髃髄蛆髆髈竉請曌雎髎髏髐滑髑髒髓體髕滕蛕髖勗髗鋗狙盙竜勝諝滝盟廟鋤櫤髥廨曨廫滫諭黱苴勶黶狷蛸嫺囿眀謂霄謄蜅鬅錆月有圊朋鬋鬌服鬍朎蜎朏猒朓朔朕朖朗漘朙望欛猜朜鬜朝朞期蜟朠謠霡朢猢霢輣錥朦朧匬猬謭霰弰漰鼱嬴鬴輸弸霸蜻猾漿彅魈睊筋轍獎捎捐青蝓靕荕靖靗靘魘静靚睛靛靜蝟齟罥靨獬鍮潮筲蝴罷潸佾譾羂徂羆莆莇澈讋龍龏枏箐龐侑龑龒龔龕龖龗鞘箙鞙宜螣掤澥宥玥莦鮪箭厭羭徭掮瞮掯瞯鞲厴宵箶羸鮹徹殽菁揃揄鏅埆俎俏菔蟕鯖寗鏘韝鏞察矟柤翦篭寵迶蟹揹毹査俼"));
        PART_TO_KANJILIST.put('載', new Part(1, "鐡鐵載"));
        PART_TO_KANJILIST.put('有', new Part(1, "郁賄髄宥淆有鮪陏随侑肴堕囿"));
        PART_TO_KANJILIST.put('眉', new Part(1, "眉媚嵋"));
        PART_TO_KANJILIST.put('弋', new Part(3, "娀威簂鰄縅戈戉戊戌戍娍戎嘎成踐我戒划帒戓鰔戔戕或鐖蠘戚減戛蠛戜戝戞戟戠戡鐡戢戣娥戦戧戩截戫縬戮戯戰嘰栰戲樲戳戴樴爴鐵倵鈸栻栽樾堿聀繊衊牋摑顑織籖穖驖幗剗蹙籛鉞機晟幟桟晠穢顣虣艤籤噦試硪顪牫幭牮鑯葳葴鱵職鹹鉽幾找悈肈碊銊袋劌纎誐碔纖媙袚檝誠犠誡璣檥犧銭貮犮骮械颰嚱碱岱貳咸貸撼裁軄滅胈哉賊盋賎惑擑裓諓烕郕烖狘盛黛盞僟代賤哦曦賦棧泧峨鋨峩棫黬拭鋮磯諴曵鳶軷櫼蛾軾胾儀茂贇茇載弋國弌漍弍儎弎式伐弐蔑弑礒餓贓餞感錢笩蜮紱崴蔵茷錻孅浅蕆譏筏饑魕轗荗識杙鵝鵞鵡武彧絨獩襪筬罭奯議楲歳襳蕺坺鍼慼畿荿醆垈薉越喊箋残斌醎膕讖殘馘侙馛垡羢閥禨義膩莪綫嶬宬妭殱羲殲玳箴憾閾熾鞿俄巇韈濈蟈濊韍城緎鯎藏减忒臓緘旘蟙菝域臟淢蟣韤臧毧槭韯觱韱懴珴駴践珷珹臹濺淺懺蟻翽篾"));
        PART_TO_KANJILIST.put('國', new Part(1, "摑膕幗國"));
        PART_TO_KANJILIST.put('朋', new Part(1, "萠繃弸崩棚朋堋硼鵬"));
        PART_TO_KANJILIST.put('県', new Part(1, "縣懸纛県"));
        PART_TO_KANJILIST.put('霍', new Part(1, "癨霍"));
        PART_TO_KANJILIST.put('弍', new Part(1, "貳膩弍"));
        PART_TO_KANJILIST.put('服', new Part(1, "箙服"));
        PART_TO_KANJILIST.put('鼎', new Part(13, "鼐鼑鼒鼎鼏"));
        PART_TO_KANJILIST.put('伏', new Part(1, "袱伏茯"));
        PART_TO_KANJILIST.put('式', new Part(1, "弑試拭軾式"));
        PART_TO_KANJILIST.put('伐', new Part(1, "伐閥筏"));
        PART_TO_KANJILIST.put('休', new Part(1, "休鮴貅恷烋"));
        PART_TO_KANJILIST.put('園', new Part(1, "園薗"));
        PART_TO_KANJILIST.put('弓', new Part(3, "茀鬀蔃稊丐匑弓弔引弖紖弗弘弙弛霛弜弝弟洟氟訠弡弢娣弣弤弥弦搦弧弨弩怫弫第弬弭紭弮鰯弯瀰弰笰弱漲弴張愵弶夷強弸鴺鬻弻弼紼弽弾弿彀襁彁剃彄彅睇彇彈癈彊驋彌彍彎襏潑荑豑鉘佛鵜晜蕟灣罤繦兮艴嵶靷穹嵹發獼湾彿鮄疆宆咈綈悌芎榒蚓涕銕宖麛墢撥粥咦鮧岪躬窮梯醱鮷沸鶸溺費蒻梻銻疿拂嫋痍苐狒泓篛諡廢俤矤勥矧姨藭胰髴鏹鏺盻"));
        PART_TO_KANJILIST.put('鼓', new Part(13, "鼓鼔鼕鼖皷鼗鼙鼚鼛瞽鼟"));
        PART_TO_KANJILIST.put('弔', new Part(1, "剃弔俤涕睇悌第鵜弟梯"));
        PART_TO_KANJILIST.put('朔', new Part(1, "塑遡朔槊愬溯"));
        PART_TO_KANJILIST.put('引', new Part(1, "蚓引矧"));
        PART_TO_KANJILIST.put('朕', new Part(1, "騰謄朕"));
        PART_TO_KANJILIST.put('弗', new Part(1, "拂狒髴弗沸佛怫費彿"));
        PART_TO_KANJILIST.put('弘', new Part(1, "泓弘"));
        PART_TO_KANJILIST.put('会', new Part(1, "絵桧会"));
        PART_TO_KANJILIST.put('蜜', new Part(1, "櫁蜜"));
        PART_TO_KANJILIST.put('霜', new Part(1, "孀霜"));
        PART_TO_KANJILIST.put('朝', new Part(1, "嘲朝潮廟"));
        PART_TO_KANJILIST.put('眞', new Part(1, "塡癲嗔鷆槇瞋愼鎭眞"));
        PART_TO_KANJILIST.put('土', new Part(3, "堀老堂耂考堃耄堄者堅耆堆耇耈堈堉耊堊耋堋堌堍瀍倒耕堕倖蠘堙頚堛倜堝砝瀞堞堟造堠堡倢堤瀦堦堧倩怪堪堭怯堯堰倰週報栲堲栳頳場堵逵堹逹堺栽堽堿塀塁桂恃硅塉塊塋灋塌塍桎灎塏塐塑塒偓達塔塕偖街塗塘遘塙塚恚塞硞塟遠恠塡塢硣塤塧遨塨塩填塰聱塲塵遶灶塸塹塼塾塿墀袁墁悂境傃墅墇墈墉墊墌墍悎梏墏墐墓墔墖増肚墜墝墟墠墡墢墦墨墩袪墫墮墱傲墲墳債炷墸墹墺墻悻梼墼邽墾袿裁壁壂壄惄僅情壅郅惆壇壈棈壊壌壍壎裎壐壑壒壓僓烓壔壕磕壖烖壗壘壙壚壜郜壝郝壞壟胠壡壢僣烤壤僥壥磧壩裯棱裱飳郵壻都磽胾褂焃鄄夅焅椆夌儎褚脚礚脞椡愫儬儲儹褹褺褻鄽社饁酇先奎契饒楔奕襗煙祛奝腟兟慠奢慥慧襭楮煮奲慳酵酷祷襼祾榁冉禊禋再規冓醘膛膣膨覩熬膮覯榯熱憲覲醳榸冼榼懃構凋釋凌懌懎燒姥秩姪駪燬至致懴燵臵臶凷釷臸臹臺槻臻槿爀娃爇舎稑舗稜稠刧截樫到稱刲戴樹樾驁牆驇橈婈剉驍積詓婕驖驛婞牡穡艢婧詩割詵剷特牿詿銈銍犎窐銑窒窖劗誟銠檣誥窪劫檯劸窺銼媾抾調竃拄鋆竈請鋌髐苒勢勣勤櫧髯鋳狴拷諸髽持挂錂挃錆挈嬈茎鬐欑錘嬙講猜猪挫茬錴謷謹猿筀捁筅等荎捏獒荖孝歝獟筠卡卦捨譯荰捱筳却捶鍺箐箑厓厔掕讚掛莛莝室厫殬鮭宯鮱殱箲害箸去殼箼殾毀菁寁篈鏊鯐毒鯖鏗鯛鏜篝叝握鯥珪鯪菫班菱珵鯺寺珽簀封鐃搆簉吐簓搘鰘鐡鰣鰪鰭鐯琱鰲鐵鐸鰹琺簺鐽豁屆告屋籍葑豔籖著摚籜屠汢周鱪豬摯鱰鑽呿撓蒔咥撦粧蒨責撻咾精哇擇哉蓋蓌蓍賍法峙蓙糙賙瓚哠擡賭哮蓺蓻購崁甄贄攅贅蔆贊崕崖洗洙崚素攢唫蔳洼唾啀啁鵄経蕏啑絓敖蕘教鵠畤赤赥赦畦赧浩赩赫赬浬赭赮絰走鵰赱赳赴起畻赿趁斁趂趄鶄超涅疆趈越趍鶏薐趐趑喔薔趕薗薘趙涛趞趟趠嶢綢趣趦嶧趨閨綪喫趫趬涬薯涯趯趲薹薼薽綾嗇闉跌闍嗑緒闔痔淕鷙嗜藝跡跣痤闥淩跬跱藷嗷緻跿蘀清嘊踋瘏渓嘖渚瘞渥踧縱嘵縶踷阹帾績時幄晆幇晊繊降湗幘陛繞陞蹟噠陡陦晧癪幫湮陲晴陵陸幸繹蹺陻陼湾暀躂庄纆躇嚇隉纎纏暑纒皓躔溘纘隚纛溝庢皢庤座庨皨麮庱嚳麸麹囈曉囋盍曎廒囓雕黕曙蛙廛廜黟黤黧黬蛭黭黮蛯黰黱黲黵囶黸軼軽鼃漄朅弆鼇圉載輊圊蜐園蜓漖輖輘圛土圠圢圣圤圥圦圧蜨在蜩圩圪漬圬圭眭圮弯圯地鼱圳圴圷圸圻蜻圽圾圿址坂轄轅坅坆均佉坊坌坍坎坏坐坑青坒潔靕靖睖靗彗靘静睚靚睛靛靜潜杜靠坡睡坢罣坤坥坦睦齧坧坨坩坪睪罫彫睫坫佬彭坭坮坯坰坱署佳坳睳潴坴坵齷坷睹坹坺坻坼睾坾坿侁垁垂垃径侄待澆垈垉鞋型垌瞌侍徍瞎澎羐徒垓垔澔垕垗垙垚垜垝垞垟瞠垠垡徢垢垣垤澤螧垧垨垩垪螫垬螯垰螳垳垸鞺垽侾埀韃埃蟄埆埇濇俈埈埋埌城埏埒埓埔埕埖埜埝埞域埠埡埣埤翥埦埧埩矬埭蟯韯埰濳埴埵埶執埸翹迹培濹基埼埽埾埿"));
        PART_TO_KANJILIST.put('弟', new Part(1, "剃俤涕睇悌鵜弟梯"));
        PART_TO_KANJILIST.put('真', new Part(1, "癲巓槙填顛慎鎮真鷏"));
        PART_TO_KANJILIST.put('鼠', new Part(13, "鼠鼢鬣竄鼦鼪鼫鼬鼯鼱鼲鼴獵鼷臘癙鼹鼺攛鑞"));
        PART_TO_KANJILIST.put('鼡', new Part(1, "鼡蝋猟"));
        PART_TO_KANJILIST.put('圣', new Part(1, "圣径怪経軽茎"));
        PART_TO_KANJILIST.put('木', new Part(4, "栂栃栄透蠑耒耓栓耔途栔耕耖栖耗栗耘耙栙耜蠜耝瀝栝耞栞堞耟速栟耠耡校堡栢耤耦栧耨栨逨栩株栫耬栬瀬栭倭耮倮栯耰栰頰栱栲倲栳栴怵砵逶核根栻格頼栽頽瀾栿桀桁桂桃硃桄桅框顆案桊桌桍桎桐桑桒桓術桔桕塗桗桘桙顙桛桜桝桟偢档灤桧桫遬桮桯桰桱桲桴桵桶恷桷桹桺桻桼桾桿梁梂悃梃梄梅梆梈邌梍梏傑梓梔傔梖梗梘悚梚梛梜袜條梟碟梠梡梢梣梥梦梧悧梨梩梪梭梮梯械梱梲梳梵梶悸梹梺梻梼袾邾棄壄棅棆壈惈棈棉磉棊棋烋棌棍裍惏棏棐棑棒棓磔棔棕棖棗棘棙棚棜棝棟棠棡壢棣棥棧磨棨棪棫棬棭森棯棰棱棲磲想郴惵棵棶裸棹裹棺僺棻棼棽磿椀愀椁褁愁椄椅椆椈椉椊椋椌植礎椎褎椏椐椑椒褒褓椓椖椗餗椙焚椚椛検餜儝椡椢椣夥椥椦餧椨椪礫礬椰椱椳椴椵礵椶椸椹椻椽椿楂襃慄楅腇煉楉楊襍楎奏楓楔楕楗楙楚楛楜楝楞襟楠煠楡楢楣煤楤楥酥楦楨楩祩楪楫楬業楮楯襯楰襰楱楲楳楴襴極楷楸楹楺楻襻楼祼楽襽楾楿榀禁榁概醂榊榍榎榑榒榔醔榕憖榖禖憗榘香榛馛榜膝馝馟榠榡馥榥榦馦榧馨榨親榫榭榮榯榱榲榴榷榸妹妺榺榻榼禾榾禿榿醿秀私槁秂槃秄槅懆秇采槇秈槈秉秊槊臊秋構懋槌懍槍凍槎秏槐釐科槑秒槓凓燓委秔秕秖槖槗秘様槙秚凜秝槝姝臝秞槞租秠秡懡秢槢秣秤秥燥槥秦秧槧槨秩凩秪秫槫觫秬臬秭槭槮槯称秱槱槲臲槳槵懶秸槹臻移槻秼槽槾槿稀樀樁稂樂稃樃樅稇稈稉稊樊程樋騋稌樌稍爍税樏稑樑樒樓稔樔稕樕稗樗稘稙標稚樚稛樛爛稜樝稞樞稟樟稠樠稡模樢樣樤爤稧爨樨利権横稫樫稭種樮稯稰樰稱稲樲稴樴稵樵樶稷樷稸樸稹樹刹稺樺刺稻樻稼稽樽稾樾稿樿穀牀穂穃牃穄橄穅橅穆橆穇橇婇穈橈穉橉橊橋穌剌積穎橎穏穐婐橐橑牒橒穕橕穖橖穗橘穙橙橛穜穝驝穟機穠穡橡穢鉢橢穣橤穥鉥橦穧橧剩穩穪婪橪穫穭剰穰橱橲橳橸橾橿剿檀犁檁犂檃誄檄誅檆檇檉檋檍檎檐檑媒銖誗檗誘劘檛犛檜檝檞媟檟檠窠檢檣檥犧檪檫檬媬檮檯檰檱課檳檴骵檸銹抹抺檻窼檽檾檿櫁髁櫂櫃諃勅櫆狇櫈諉櫉諌櫌櫐櫑勑鋓櫓櫔櫕櫖櫚櫛諜櫜櫝櫞髞櫟嫠櫤髤勦竦櫧櫨嫩櫪諫諬櫬苯櫰櫱櫲髹櫺嫻櫻櫼櫽謀鬁欂欃欄欅鬆欆欇茉欉權謋猍鬎欏欐笑欑欒猓欖欗欛欝欟笨錬鬱茱猱茶欶嬾孀魅捆魏捒鍒魔策譟季鍬荰鍱歴獺捼鮇莉殊宋莕讕箖箘莠採探厤厯箱莱宲鮴殺莿揀寀鏁築揉鏉菌鯏寐菓鯘菘叙菜鯟篠珠篥寨揪寱揲揸菻氂簃琇鰈氉鰊萊簌鰍萎簗萙琛萩搩萪尮琳鰷琹搽鑅呆葆葇葈葉籍籕鑙葙籟鑠籣瑧屧摩鑭瑮味蒁钄貅璅蒅和沐蒩璪沫咮咻粿蓁糅瓈操泍瓓賚糜賝蓧峲賲擵擽唀攀甃攈攊蔌唎洓攔洙攟崣崧紮蔯蔴唻唽蕀鵂嵆敇啉嵊絑敕蕖嵙啝鵣浰整啾鶆喇鶇綉喋疎薐閑涑綑鶒斕鶖斜綝薪鶫嶫閫薬薭新綵綶涷嶸涹喿藁淅藉藊跊淋緌闌巍闑嗓藕緗藜痜淞痢巣緤藥緥跥藦深痲藳痳練鷴藶淶藻嗽藾痿蘂蘇踈瘌蘑蘒蘓蘖蘗嘛踝渠渣昧縧昩渫蘭蘶縻蘼蘽蹀蹂湅繅湈幉湊乗乘湘噤除癧湨癩癪噪湫繰晰陳晳噺陿躁溂麇床皌躒麓麕麘皙躝嚟隟庥麥嚦暦溧麨麩麪皪麬麭麮麯躰麰溱躱麳麴麵隸麻麼麾麿廂囃集盉滌囌黍黎黏黐雑蛛雜曦黧廩困囷相霂漆蜊蜏休弑霖蜙眛霜眜輠蜥漦木未末本札朮輮朱漱蜲輳朳朴朶朷朸机朽弽缽朾蜾朿蝀杁靂轃齄杅杆杇杈杉蝌杌李杏睏材村体杓杔杕杖潗彙杙杜杝杞睞束杠靡条轢杢轣杣杤来杦罧彩杪彬杬睬杭杮杯杰東杲蝲杳杴杵蝶杶杷潸靺杻杼齽松板极构枅來枇枉枋枌枎侏枏徐析枑螓枓枕枖澖林枘枙螙枚枛果垜龝枝澟徠枠枡澡龢枢鞢辣鞣鞦枦枩枯枰枱羲枲枳枴枵澵架枷枸枹枻枼枽柀柁柂柃柄柅柆柈柉柊韊柎韎柏俐某柑柒染柔柗柘韘柙柚濚埜柜保柝柞柡柢柤柦柧柩柬矮柮柯俰述埰柰柱柲柳柴柵柶柷柹査濼柾柿"));
        PART_TO_KANJILIST.put('在', new Part(1, "恠在"));
        PART_TO_KANJILIST.put('未', new Part(1, "誄誅魅棅藉殊籍侏寐釐耒耓耔耕藕耖銖耗耘耙洙眛蛛耜耝耞耟耠珠耡耤耦昧耨未株耬業耮耰朱茱味妹"));
        PART_TO_KANJILIST.put('末', new Part(1, "秣抹茉靺末沫"));
        PART_TO_KANJILIST.put('本', new Part(5, "躰鉢体笨本"));
        PART_TO_KANJILIST.put('圭', new Part(1, "啀封挂桂褂娃硅幇哇鞋奎崕崖街蛙恚睚掛卦畦閨珪窪罫圭鮭涯佳袿"));
        PART_TO_KANJILIST.put('札', new Part(1, "札紮"));
        PART_TO_KANJILIST.put('朮', new Part(1, "述術朮"));
        PART_TO_KANJILIST.put('弯', new Part(1, "湾弯"));
        PART_TO_KANJILIST.put('弱', new Part(1, "弱嵶搦鶸溺嫋蒻鰯"));
        PART_TO_KANJILIST.put('朱', new Part(1, "珠朱茱誅銖洙株殊蛛侏"));
        PART_TO_KANJILIST.put('張', new Part(1, "漲張"));
        PART_TO_KANJILIST.put('缶', new Part(6, "鷂罃萄罄罅旆繇罇匋罌罍罎掏罏罐窑搖淘遙寚暚傜謠謡掣瑤遥鰩徭綯窰鬱媱缶陶寶瑶缸祹缺猺揺缻颻缼缽缾缿"));
        PART_TO_KANJILIST.put('朶', new Part(1, "躱朶"));
        PART_TO_KANJILIST.put('強', new Part(1, "襁繦強"));
        PART_TO_KANJILIST.put('鼻', new Part(14, "齁襣齃擤齄齅齆齇嚊劓嬶鼻鼼鼽鼾濞鼿"));
        PART_TO_KANJILIST.put('朿', new Part(1, "蕀策棗棘刺朿"));
        PART_TO_KANJILIST.put('轉', new Part(1, "囀轉"));
        PART_TO_KANJILIST.put('杉', new Part(1, "杉彬"));
        PART_TO_KANJILIST.put('齊', new Part(14, "擠纃穧齊齋躋嚌臍齎隮齏蠐劑韲儕薺霽濟"));
        PART_TO_KANJILIST.put('彌', new Part(1, "瀰彌"));
        PART_TO_KANJILIST.put('位', new Part(1, "莅位"));
        PART_TO_KANJILIST.put('彎', new Part(1, "灣彎"));
        PART_TO_KANJILIST.put('彐', new Part(3, "耀縁騄帇蠊尋萋縑嘒鸒帚吚君鰜瀞瀟堟帠蠡倢琤急蠨搪簫爭琭逮鐮嘯逯帰樰尲戳倳稴頵簶尹簾鱃牅穅鱅恆鱇鱈衋驌繍穏葏豏鱐瑑婕塘鱘籙艝剝鱟葠繡档剥婦穩扭瑭癮鱮灵鹻桾傁肁檃肅肈争墉銉亊事碌躍沍犍溏傏撏璏麏互溓傔窘亙庚粛芛隠傭隱嚳隶粶康隷隸庸蒹亹悽咿櫂曃棅廉嫌諍糖壗裙曜蛜櫞鋟糠盠郡盡賡擢棣磤雪竫峮棲糴糶賺櫽滽仾褄焄伊笋贐唐夓圕褖儘鄘謙猙錚夛茟崢蔧蜨鄩挪録餹椽蕁嵂捃浄靆彇饈歉慊轌彐彑啑当彔彖彗楗彘静彙潚啛彛靜彜睜筝彝蝝轝煞彠慧睫蕭潯嵰祲慵慷捷歸捸浸杻兼祿捿膁醁掃禄斅綅膅疉鶊鎌箏宐枑箑箒涒侓薓讔鎕螗喙掙莙羞龞覠徢羣龣群膤徤馦鞬宭冱侵妻箻鮼鶼掾冿寁濂凄濅篆凈菉秉巋藎緑淒珒旔寖燖篖嗛寝鏞旟寢淥槥淨埩鷫埭濯凲篲蟳揵菷駸珺燼埽鏽燿"));
        PART_TO_KANJILIST.put('坐', new Part(1, "坐座蓙挫"));
        PART_TO_KANJILIST.put('彑', new Part(3, "縁恆篆菉碌沍璏宐彑枑瑑緑互彔彖褖彘亙喙彙籙彜彝蝝剝櫞堟盠蠡淥逯冱録粶椽掾仾祿"));
        PART_TO_KANJILIST.put('罒', new Part(5, "蘀瀆蠉鰊蠋瀗蠛爝瘝鰥蘰蠲鐲爵縵鐶鐸鰻堽蘿詈衊鑒幔牘驛籜遝鱞鑟鱠噣繪屬幭籮繯幰鱰聴繹聹鑼聽嚀墁钃還躅劅續邏劚檜纜犢皭環買檸嚼擇竇囉擉壊瓌曎擐囑髑惘曙嫚櫝壞賣擥諫糫廰櫰廳黷擺曼蜀會儇甍欏蔑蔓眔贖欖鬘儚圛嬛圜儜欝攞鬟夢夣漫攬褱匵儸謾眾饅魍罒蕒罔罕襗罘轘罛罜歝罝楞譞罟罠饠罡襡慢罣罤罥罦罧獧獨罨潨罩獪襪罪睪罫罭置譯罰獰罱署罵罷罸罹譼罽睾罾罿讀玀斁羂羃羅羆羇薈羈讟瞢斣澤薥嶧薨鶫殬薯殰網憲徳熳醳覽膾覿濁懁釂臅韇韈釋懌懐矒濘矚藚懜鏝闤韤寧燭寰懷觸藼槾篾翾"));
        PART_TO_KANJILIST.put('青', new Part(8, "菁情清錆棈圊請箐青靕鯖靖靗靘静靚睛靛猜靜瀞婧蒨倩綪儬鼱蔳晴蜻精"));
        PART_TO_KANJILIST.put('齒', new Part(1, "齠齡齣齦齧齪齬齒齲囓齔齶齷齟"));
        PART_TO_KANJILIST.put('当', new Part(1, "当档"));
        PART_TO_KANJILIST.put('彔', new Part(1, "緑録彔"));
        PART_TO_KANJILIST.put('罔', new Part(1, "網罔惘魍"));
        PART_TO_KANJILIST.put('何', new Part(1, "何荷"));
        PART_TO_KANJILIST.put('彖', new Part(1, "縁蠡彖篆喙椽櫞掾"));
        PART_TO_KANJILIST.put('彗', new Part(1, "彗慧"));
        PART_TO_KANJILIST.put('潘', new Part(1, "潘藩"));
        PART_TO_KANJILIST.put('睘', new Part(1, "環還睘"));
        PART_TO_KANJILIST.put('余', new Part(1, "除敍畭蜍徐途塗敘餘余叙斜荼"));
        PART_TO_KANJILIST.put('静', new Part(1, "静瀞"));
        PART_TO_KANJILIST.put('作', new Part(1, "筰作"));
        PART_TO_KANJILIST.put('非', new Part(8, "暃渄猅纎霏斐騑排纖讖徘劘蜚瀣薤榧輩匪悱殱悲琲殲誹蘼虀孅扉緋齏棐棑腓剕籖篚蕜非靠翡鯡靡啡籤罪韭韮鑯韯痱韱韲菲俳襳懴裴裵懺櫼"));
        PART_TO_KANJILIST.put('束', new Part(1, "揀溂欄勅喇踈鰊剌闌疎敕悚爛楝束籟速辣竦嫩癩諫鶫柬瀬蘭漱襴整懶獺頼嗽瀾嬾藾"));
        PART_TO_KANJILIST.put('彡', new Part(3, "鬀鬁鬂鬃鬄鬅鬆須鬈砉鬉鬋鬌鬍鬎耏嘐鬐鬒簓眕鬖蔘鬘鬙鬚樛瘛鬛鬜鬟鬠鬢鬣昣尨謬戮餮丯洯鬱瘳診鰺漻鬽紾頾縿驂襂嵃遃聄穆繆穇轇杉摎顏偐顔慘畛恝彡形彣彤彦彧彩彪彫衫彬彭彯彰偰影彲剹嵺摻牻嵾憀鎀趁疁澃殄璆澎羏厖袗肜膠醦膨醪傪庬喭沴辵疹参飂參僇跈蟉珍髎翏鏐囓廖胗蓚蟚鷚糝髟磟髠勠髢俢髣髤釤髥寥髦髧惨盨髩髪嫪髫軫廫髬髭髮修槮滮髯髱滲髲黲髳髴鯵髵髷篸髹駹諺髺髻蓼髽髿毿賿"));
        PART_TO_KANJILIST.put('面', new Part(9, "愐面靤勔麵靦靧靨麺緬偭湎"));
        PART_TO_KANJILIST.put('来', new Part(1, "莱来"));
        PART_TO_KANJILIST.put('彦', new Part(1, "偐顔彦諺顏"));
        PART_TO_KANJILIST.put('齧', new Part(1, "囓齧"));
        PART_TO_KANJILIST.put('革', new Part(9, "鞀鞁戁鞄鞅鞆覇羇羈覉鞉覊鞋漌鞍鞏鞐墐鞕鞖鞗鞘鞙鞚欛鞜鄞鞞鞟鞠鞢鞣殣鞦鞨厪鞫鞬鞭鞮熯鞱鞲鞳斳鞴暵鞵鞶霸鞸鞹鞺鞼鞾鞿韁韃韄韅韆韇韈韉韊囏廑勒緙灞臡革壩靪靫慬靭靮靱癱靳靴靶靷靸靹靺靻靼靽靿"));
        PART_TO_KANJILIST.put('彫', new Part(1, "簓彫"));
        PART_TO_KANJILIST.put('彭', new Part(1, "膨彭澎"));
        PART_TO_KANJILIST.put('佰', new Part(1, "佰鏥縮宿蓿"));
        PART_TO_KANJILIST.put('東', new Part(1, "東陳練鶇煉諌錬凍蘭棟"));
        PART_TO_KANJILIST.put('署', new Part(1, "署曙薯"));
        PART_TO_KANJILIST.put('彳', new Part(3, "簁樅蘅愆縦縱瘲鴴桁癁嵂鵆荇行衍絎衎葎屐衑幑衒術筕衕衖街衘衙衚衛衜屜衝蹝衞籞衟衠衡衢屣蹤履癥屧屨屩慫彳聳彴彵豵彷彸役彺彼彽彾彿往暀征徂徃径憄待覆徇薇很徉徊律後徍讏徏徐徑徒従澓徖得徘徙躛銜徜徝從徠御徢徤禦徧徨復循徫徬徭微徯徰徱垳徳徴徸徹箻徼徽裄哘鏦珩蓯蓰懲黴篵勶"));
        PART_TO_KANJILIST.put('坴', new Part(1, "坴陵陸"));
        PART_TO_KANJILIST.put('罷', new Part(1, "羆罷擺"));
        PART_TO_KANJILIST.put('松', new Part(1, "鬆菘松淞"));
        PART_TO_KANJILIST.put('睿', new Part(1, "叡濬睿"));
        PART_TO_KANJILIST.put('垂', new Part(1, "睡垂陲郵捶錘唾"));
        PART_TO_KANJILIST.put('羅', new Part(1, "羅鑼蘿邏"));
        PART_TO_KANJILIST.put('來', new Part(1, "徠麥來憖麩麪賚麭"));
        PART_TO_KANJILIST.put('羊', new Part(6, "儀瀁養洋對礒縒搓蘚樣蜣氧褨鄯鄴樸鐽漾着牂鱃遅饈蹉饍達鱔繕艖恙幞噠癢艤祥癬業嵯佯議譱瑳詳嵳鱶鹺蹼躂善鎈徉羊羌羍美羏羑羔羖羗薘羚羜羝璞羞傞垟犠庠墡羡羢羣群羦犧羨義咩羪嶫嶬羭羮鮮窯羚羯撲羲膳羴羶劷羸羹撻羼躾韃烊磋槎僐痒翔僕盖様姜嗟懟叢闥曦觧濮差燵蟻"));
        PART_TO_KANJILIST.put('律', new Part(1, "律葎"));
        PART_TO_KANJILIST.put('龍', new Part(1, "巃豅竉讋龍瓏攏龏龐龑龒龔龕龖龗竜滝槞壟籠蘢朧瀧嚨曨鑨篭礱襲隴寵聾"));
        PART_TO_KANJILIST.put('美', new Part(1, "羹美躾"));
        PART_TO_KANJILIST.put('析', new Part(1, "析晰淅蜥皙"));
        PART_TO_KANJILIST.put('従', new Part(1, "従縦"));
        PART_TO_KANJILIST.put('羔', new Part(1, "羔羹羮窯"));
        PART_TO_KANJILIST.put('侖', new Part(1, "侖棆論崘綸崙淪輪倫"));
        PART_TO_KANJILIST.put('林', new Part(1, "禁醂礎蘑麓霖林焚嘛瀝暦梦爨琳蔴梵梺縻麻麼蘼麾麿醿靂穈淋魔楚埜糜襟靡懡轣噤藦癧罧磨摩婪櫪彬森痲痳歴擵潸菻"));
        PART_TO_KANJILIST.put('羚', new Part(1, "羚羚"));
        PART_TO_KANJILIST.put('辛', new Part(7, "騂躃贄躄劈圉鸊梓甓倖嬖蘖檗蘗莘辛薛薜辜辝辞辟辠縡辡辣澤辤辥辦隦璧辧辨薪親辭辮辯宰新報鐴鐸霹逹稺撻澼壁臂蟄鋅擇釋懌滓癖擗擘鷙驛襞闢瓣闥譬襯摯譯櫱乵糵執幸繹噺僻孼孽睾避"));
        PART_TO_KANJILIST.put('龜', new Part(1, "龜龝鬮"));
        PART_TO_KANJILIST.put('果', new Part(1, "巣夥勦顆課菓樔裸彙裹果踝剿"));
        PART_TO_KANJILIST.put('從', new Part(1, "縱聳蹤樅慫從"));
        PART_TO_KANJILIST.put('辟', new Part(1, "壁臂闢躄璧劈譬甓嬖癖檗蘗擘霹僻薜襞辟避"));
        PART_TO_KANJILIST.put('龠', new Part(17, "龠鑰龡籲龢龣禴籥龥瀹鸙爚"));
        PART_TO_KANJILIST.put('御', new Part(1, "御禦"));
        PART_TO_KANJILIST.put('澡', new Part(1, "澡藻"));
        PART_TO_KANJILIST.put('復', new Part(1, "履覆復"));
        PART_TO_KANJILIST.put('義', new Part(1, "儀犠議礒艤義蟻嶬"));
        PART_TO_KANJILIST.put('微', new Part(1, "薇微"));
        PART_TO_KANJILIST.put('侯', new Part(1, "猴喉篌侯"));
        PART_TO_KANJILIST.put('辰', new Part(7, "莀儂濃蜃蜄薅唇震槈麎蓐敐賑鎒漘襛縟曟娠穠鋠脣脤褥晨耨振禯辰辱農侲醲宸憹溽蕽膿"));
        PART_TO_KANJILIST.put('辱', new Part(1, "蓐辱褥耨溽縟"));
        PART_TO_KANJILIST.put('農', new Part(1, "農儂濃膿"));
        PART_TO_KANJILIST.put('羲', new Part(1, "羲曦犧"));
        PART_TO_KANJILIST.put('徴', new Part(1, "懲徴"));
        PART_TO_KANJILIST.put('辶', new Part(3, "退送适逃逄逅逆逈簉樋縋逋逌逍逎導透逐逑逓途逕逖逗逘這通逛逝逞速造逡逢連搥逧蘧逨逩逪縫逬蠭逭逮逯週鰱進逳逴逵逶逷逸逹縺逼鐽逾逿遁鱁遂遃遄遅遇遉遊運遌遍過遏遐遑遒道鑓摓達違遖遘遙遛遜遝遞穟遠噠遡遢遣遥遦籧遧遨適籩遬遭遮遯遰遲遴遵噵遶遷選繸遹遺遼遽繾避邀邁蒁邂躂邃還邅邇邈邉璉邊邋邌邎邏随邐躚檛璡隧隨璲暹撻撾蒾曃髄髓磓糙蓪蓬蓮擿謎儙漣褪謰靆襚慥譴腿讁膖薘鎚熢辶辷膸辸鎹辺辻込达辿迀迁迂韃迄迅韆迆鏈迊迋槌迍迎运近迒迓返迕迚迠巡迢迣迤闥迥迦燧迨迩迪迫迭迮迯述迱迴篴燵迵迶篷迷迸嗹迹迺迻追迾"));
        PART_TO_KANJILIST.put('込', new Part(3, "退送适逃逄逅逆逈簉樋縋逋逌逍導逎透逐逑逓途逕逖逗逘這通逛逝逞速造逡逢連搥逧蘧逨逩逪縫逬蠭逭逮逯週鰱進逳逴逵逶逷逸逹縺逼鐽逾逿遁鱁遂遃遄遅遇遉遊運遌遍過遏遐遑遒道鑓摓達違遖遘遙遛遜遝遞穟遠噠遡遢遣遥遦籧遧遨適籩遬遭遮遯遰遲遴遵噵遶遷選繸遹遺遼遽繾避邀邁蒁邂躂邃還邅邇邈邉璉邊邋邌邎随邏邐躚檛璡隧隨璲暹撻撾蒾曃髄髓磓糙蓪蓬蓮擿謎儙漣褪謰靆襚慥譴腿讁膖薘鎚熢辶辷膸辸鎹辺辻込达辿迀迁迂韃迄迅韆迆鏈迊迋槌迍迎运近迒迓返迕迚迠巡迢迣迤闥迥迦燧迨迩迪迫迭迮迯述迱迴篴燵迵迶篷迷迸嗹迹迺迻追迾"));
        PART_TO_KANJILIST.put('羽', new Part(6, "耀謆嘐鸐蘙瀚樛嬥挧搨鰨栩謬戮蠮瘳戳褶瀷騸漻鑃繆扇轇籊蹋塌摎噏塕歙潝嵡詡遢慴剹蹹摺嵺煽譾憀疁璆螉躍趐傓傟膠熠躢醪趯鶲榻溻璻羽羿翀翁飂櫂滃翃翅翆僇翈蟉蓊翊藋翌翎臎髎翏鏐習闒翔翕廖磖鷚翛勜曜珝磟翟闟翠勠翡槢擢毣翣寥翥翦翨翩嫪翫廫翬翮翯濯翰翲翳糴糶翹翺翻翼蓼翽翾翿賿燿"));
        PART_TO_KANJILIST.put('便', new Part(1, "鞭便"));
        PART_TO_KANJILIST.put('瞿', new Part(1, "钁衢攫懼矍瞿"));
        PART_TO_KANJILIST.put('翁', new Part(1, "翁鶲蓊"));
        PART_TO_KANJILIST.put('心', new Part(4, "戀瀀戁蘂戇爈倊鈊怎騐怒樒鰓稔瀗怘鐚瘛瘜思怠怤急怨訫怭耰怱總鐿恁噁穂驄顋恋癋鉍牎穏恐繐癒恕牕恖顖穗恙虙恚恝瑟摠聡鑢恣橤恥聦恧恩穩噫鱫偬恭癮息噯聰幰葱偲聴恵恷葸聹聽恿嚀沁璁悆悉悊誋誌認檍窓悘纞隠悠患悤悥璦皧您悪芯傯隱璱悲邲悳媳躵悶檸颸窻撼蒽櫁惄軈惉泌櫌惎惑鋕曖櫖諗惚惠惡惢惣惥棯廰勰擰諰廳想勴飶惷惹櫽擾僾苾愁愂贃億攄愆愈伈愍意謐贒愗愙愚愛蜜儜錜感礠愡嬡愢蔤蔥愨優愬甯漶唸崽愿慁慂慇慈靉蕊態潓譓慕祕佖轗慙蕙蕜慝孞楤慧慫慮腮鍯慰獰捴荵慶鍶慸捻慼慾慿憁憂憃熄憄螅鮅薆憇憊憋薏憑宓憓讔覕憖憗憙綛憝馝覟閟憠憥憨憩厯憲徳薴鎴憶瞹妼憼憾心懃矃必臆密懇忈應蟋懋忌珌忍寍総忐忑忒鏓揔懕毖志寗秘濘忘藘応燜駜忝埝忞懟忠忢痣懣緦寧懧濨忩蟪緫懬懯鯰淰懲柲淴念槵懸鏸添藼忽跽濾鷾懿忿"));
        PART_TO_KANJILIST.put('忄', new Part(3, "戃戄怇怊怍怏怐怓怔怕怖怗怙怚怛怜怟怡怦性怩怪怫怭怯怳怵怺恀恂恃恆恇恈恉湉恊恌恍恑恒恔恗恟恠恡恢恤恨恪恫恬恰恱恾悁悂悃悄悈悋悌悍悎悑悒悓悔悕悖悗悚悛悝悞悟悢悦悧悩悰悱悴悵悷悸悻悼悽悾惂情惆惇惈惊惋惏惓惔惕惘惙惚惛惜惝惞惟惧惨惰惱惲惴惵惶惸惺惻惼惽愀愃愇愉愊愌愎愐愑愒愓愔愕愖愜愞愡愢愧愪愫愰愱愴愵愶愷愹愼愽愾慄慅慆慉慊慌慍慎慓慘慚慞慟慠慢慣慥慨慬慯慱慲慳慴慵慷筷罹慻憀憁憍憎憐憒憓憔憘憚憜憟憤憧憪憫憬憭憮憶憸憹憺憾懀懁懂忄懆忇懈忉懊忋懌懍懎懏懐忓忔忖忙懜懝懞忡懡懢忤懥懦懧忨懩忪緪快忬忭懭忮忯忰忱忲忳懴懶忶懷忸懺忺忻懼忼懽懾"));
        PART_TO_KANJILIST.put('必', new Part(1, "謐櫁樒必祕密秘泌蜜瑟"));
        PART_TO_KANJILIST.put('韋', new Part(10, "幃愇闈偉韋湋瑋韌圍韍韎暐韐韑煒韓韔違禕韗褘韘韙衛韛韜韝韞韠韡韤葦徫緯諱"));
        PART_TO_KANJILIST.put('忍', new Part(1, "荵綛忍認"));
        PART_TO_KANJILIST.put('矍', new Part(1, "钁攫矍"));
        PART_TO_KANJILIST.put('某', new Part(1, "某謀媒楳煤"));
        PART_TO_KANJILIST.put('習', new Part(1, "習慴褶摺翫"));
        PART_TO_KANJILIST.put('柔', new Part(1, "蹂鞣柔糅揉"));
        PART_TO_KANJILIST.put('翕', new Part(1, "翕歙"));
        PART_TO_KANJILIST.put('志', new Part(1, "痣志誌"));
        PART_TO_KANJILIST.put('矛', new Part(5, "劀瞀氄茅墅鐍舒抒紓騖愗璚霚鞣袤妤澦霧芧鶩輮猱霱蹂壄糅葇揉蟊鱊懋野譎潏鍒柔橘繘務楙矛矜獝矞矟矠魣蝥鍪豫忬櫲髳蕷鷸遹婺楺雺杼"));
        PART_TO_KANJILIST.put('保', new Part(1, "堡褒褓葆保"));
        PART_TO_KANJILIST.put('忝', new Part(1, "添忝"));
        PART_TO_KANJILIST.put('矢', new Part(5, "倁垁騃肄簇喉唉瘊璏疑誒瞖儗蜘悘榘涘逘礙候薙蘙蔟踟堠椥挨権醫娭蠮侯愱餱観昳猴欵笶嶷欸殹蒺医疾薿埃鏃鉃鉄繄潅糇嫉雉毉篌族歓鷖彘凝懝俟聟鷟癡矢竢矣矤知矦勧矧矩矪擬矬短迭鍭矮矯矰矱緱翳痴鯸智睺嗾"));
        PART_TO_KANJILIST.put('矣', new Part(1, "竢矣埃挨欸俟"));
        PART_TO_KANJILIST.put('知', new Part(1, "痴知椥蜘智聟踟"));
        PART_TO_KANJILIST.put('忩', new Part(1, "忩総"));
        PART_TO_KANJILIST.put('柬', new Part(1, "揀欄襴鰊爛諫鶫柬闌蘭楝瀾"));
        PART_TO_KANJILIST.put('韭', new Part(9, "籤薤孅韭韮纎齏殱殲韲懴籖纖讖懺"));
        PART_TO_KANJILIST.put('修', new Part(1, "蓚修"));
        PART_TO_KANJILIST.put('翰', new Part(1, "翰瀚"));
        PART_TO_KANJILIST.put('蟲', new Part(1, "蠱蟲"));
        PART_TO_KANJILIST.put('石', new Part(5, "礀礁砂砅砆礆礇砉砌礌砍礎砎礐礑砑蘑砒礒研砕礙礚礜砝礞騞礟砠礠砡砢砣砥礥砦礦砧蠧礧礩礪礫鼫礬砭礭砮砰礱砲破礴砵礵砷蠹砺砿硃硄硅硇硈硌硎祏鉐橐硒硜硝驝硞硠硡硣硤坧硨硪硫硬确硯硲硴硺硼硾碁碆碇碊碌碍碎喏碏碑碓碔碕宕鮖碗碘碚碝碞碟碡碣碤碧碨岩碩碪斫妬碬碭碯劯沰碰暱碱碲碳碵確碻碼碽碾碿磁磅磆磇磈磉磊磋磌磎磐磑磒拓磓磔磕跖槖磖柘磚磛磟磠磡磤磦磧磨磪菪磬磯磲石磳磴矴磶磷矸磺矻磻矼磽磿"));
        PART_TO_KANJILIST.put('音', new Part(9, "嶂境億璋檍意喑瞕瘖暗障麞樟戠鄣漳瘴憶傹傽熾鐿軄臆鱆歆闇獍獐織識嫜幟竟章鏡偣噫黯彰音諳韴韵韶職韷竸韸韺韻鷾響"));
        PART_TO_KANJILIST.put('念', new Part(1, "鯰稔念唸捻棯"));
        PART_TO_KANJILIST.put('埶', new Part(1, "熱埶"));
        PART_TO_KANJILIST.put('迷', new Part(1, "迷謎"));
        PART_TO_KANJILIST.put('執', new Part(1, "蟄贄執鷙摯"));
        PART_TO_KANJILIST.put('査', new Part(1, "渣査"));
        PART_TO_KANJILIST.put('追', new Part(1, "鎚縋槌追"));
        PART_TO_KANJILIST.put('忽', new Part(1, "惚忽"));
        log.info("Initialized in " + (System.currentTimeMillis() - start) + "ms");
    }


























































    // prints out a list of kanji-kanji; the first kanji contains the second one as a part.
    // this is an attempt to fix https://code.google.com/p/aedict/issues/detail?id=326
    // but there are too many combinations...
    public static void main(String[] args) {
        final Set<Kanji> kanjisWithParts = new HashSet<Kanji>();
        for (Map.Entry<Character, Part> entry : PART_TO_KANJILIST.entrySet()) {
            if (JpCharKt.isKanji(entry.getKey())) {
                kanjisWithParts.add(new Kanji(entry.getKey()));
            }
            kanjisWithParts.addAll(entry.getValue().kanjiSet);
        }
        int i = 0;
        for (Kanji outer : kanjisWithParts) {
            for (Kanji inner : kanjisWithParts) {
                if (outer.equals(inner)) {
                    continue;
                }
                Set<JpCharacter> outerParts = getParts(outer);
                if (outerParts.contains(inner.toCharacter())) {
                    continue;
                }
                Set<JpCharacter> innerParts = getParts(inner);
                if (!outerParts.containsAll(innerParts)) {
                    continue;
                }
                System.out.println(outer + "," + inner + ",");
                i++;
            }
        }
        System.out.println("Number of combinations: " + i);
    }
}
