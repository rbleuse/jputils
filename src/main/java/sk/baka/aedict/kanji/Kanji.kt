/**
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.kanji

import sk.baka.aedict.dict.Commonality
import sk.baka.aedict.dict.JLPTLevel
import sk.baka.aedict.dict.isWhitespace
import sk.baka.aedict.util.Boxable
import sk.baka.aedict.util.*
import sk.baka.aedict.util.Writable
import sk.baka.aedict.util.typedmap.Box

import java.io.DataInput
import java.io.DataOutput
import java.io.IOException
import java.io.Serializable
import java.util.*

/**
 * Represents a single kanji.
 * @property codePoint The kanji code point.
 * @author mvy
 */
data class Kanji(val codePoint: Int) : Serializable, Writable, Boxable {

    @Transient private var kanjiCached: String? = null

    /**
     * Checks whether this is a kanji part ([Parts.isPart] returns true).
     * @return true if this is a kanji part, false if not.
     */
    val isBasePart: Boolean
        get() = kanji().length <= 1 && Parts.isPart(toCharacter())

    @Transient private var _commonality = 0

    /**
     * Returns JLPT level of given kanji.
     * @return JLPT level or null if the kanji is not present in any of the JLPT test.
     */
    val jlptLevel: JLPTLevel?
        get() = if (isChar) JLPT_LEVEL[toCharacter()] else null

    /**
     * Returns kanji parts.
     * @return a part list, never null, may be empty if unknown kanji is supplied. May contain the original kanji.
     */
    val parts: Set<JpCharacter>
        get() = Parts.getParts(this)

    /**
     * Returns kanji parts, sorted according to https://code.google.com/p/aedict/issues/detail?id=323
     *
     *  * a kanji should precede its parts in the part list. For example, 大 should precede both 一 and 人;
     *  * also, 一 and 人 should follow 大 as closely as possible.
     *
     * @return a part list, never null, may be empty if unknown kanji is supplied. Never contains the original kanji.
     */
    val partsSortedContainerFirst: String
        get() = partTree.printKanjisPreorder().replace(kanji(), "")

    /**
     * Returns the part tree, with this kanji as its root.
     * @return the part tree, never null.
     */
    val partTree: PartTree
        get() {
            val tree = PartTree(toChar(), null)
            tree.cramSimilarStuffTogether()
            return tree
        }

    /**
     * True if it can be represented as a single java char.
     */
    val isChar: Boolean
        get() = codePoint <= Character.MAX_VALUE.toInt()

    /**
     * Returns the hexa code of this kanji, such as `0xAAAA`.
     */
    val hexCode: String
        get() = "0x${Integer.toHexString(codePoint)}"

    /**
     * The kanji, not null, not blank. Contains exactly single kanji.
     */
    fun kanji(): String {
        if (kanjiCached == null) {
            kanjiCached = String(intArrayOf(codePoint), 0, 1)
        }
        return kanjiCached!!
    }

    /**
     * Constructs object for given character. Fails if given character is not a kanji.
     * @param kanji the kanji character.
     */
    constructor(kanji: Char) : this(kanji.toInt())

    init {
        require(codePoint.isKanji) { "Parameter codePoint: invalid value " + String(intArrayOf(codePoint), 0, 1) + ": not a kanji" }
    }

    constructor(kanji: String) : this(kanji.singleKanjiGet()) {
        this.kanjiCached = kanji
    }

    /**
     * Returns [kanji]
     * @return [kanji]
     */
    override fun toString(): String {
        // don't change - lots of code expects this to simply return the kanji itself.
        return kanji()
    }

    @Throws(IOException::class)
    override fun writeTo(out: DataOutput) {
        out.writeUTF(kanji())
    }

    override fun box(): Box = Box().putInt("codePoint", codePoint)

    /**
     * Returns the kanji commonality. Most common kanji has commonality of 2.
     * @return the kanji commonality.
     */
    val commonality: Int get() {
        if (_commonality == 0) {
            _commonality = KanjiBasedCommonality.getCommonality(codePoint).toInt()
        }
        return _commonality
    }

    fun `is`(kanji: Char) = codePoint == kanji.toInt()

    private class KanjiBasedCommonality : Commonality() {

        override fun getCommonality(kana: String): Short {
            // add malus - otherwise random stuff from this commonality would overpower the occurence-based commonality
            var result = 1000
            kana.trim().codePoints.filter { !it.isWhitespace() }.forEach {
                val s = getCommonality(it)
                result += s
                if (result >= Short.MAX_VALUE) {
                    return maxCommonality
                }
            }
            return result.coerceAtMost(maxCommonality.toInt()).toShort()
        }

        override val maxCommonality: Short get() {
            // originally this was Short.MAX_VALUE, but then these commonalities
            // were too low and overpowered OccurenceBasedCommonality
            // this is the way to decrease importance of this.
            return 3000
        }

        override fun supports(kana: String) = true

        override fun supports2(kanji: List<String>, reading: List<String>) = true

        companion object {
            /**
             * A kanji list, ordered by its commonality (most common one to least common one). Only first 1000 most common kanji characters are stored here.
             */
            private val COMMONALITY = "日一国会人年大十二本中長出三同時政事自行社見月分議後前民生連五発間対上部東者党地合市業内相方四定今回新場金員九入選立開手米力学問高代明実円関決子動京全目表戦経通外最言氏現理調体化田当八六約主題下首意法不来作性的要用制治度務強気小七成期公持野協取都和統以機平総加山思家話世受区領多県続進正安設保改数記院女初北午指権心界支第産結百派点教報済書府活原先共得解名交資予川向際査勝面委告軍文反元重近千考判認画海参売利組知案道信策集在件団別物側任引使求所次水半品昨論計死官増係感特情投示変打男基私各始島直両朝革価式確村提運終挙果西勢減台広容必応演電歳住争談能無再位置企真流格有疑口過局少放税検藤町常校料沢裁状工建語球営空職証土与急止送援供可役構木割聞身費付施切由説転食比難防補車優夫研収断井何南石足違消境神番規術護展態導鮮備宅害配副算視条幹独警宮究育席輸訪楽起万着乗店述残想線率病農州武声質念待試族象銀域助労例衛然早張映限親額監環験追審商葉義伝働形景落欧担好退準賞訴辺造英被株頭技低毎医復仕去姿味負閣韓渡失移差衆個門写評課末守若脳極種美岡影命含福蔵量望松非撃佐核観察整段横融型白深字答夜製票況音申様財港識注呼渉達良響阪帰針専推谷古候史天階程満敗管値歌買突兵接請器士光討路悪科攻崎督授催細効図週積丸他及湾録処省旧室憲太橋歩離岸客風紙激否周師摘材登系批郎母易健黒火戸速存花春飛殺央券赤号単盟座青破編捜竹除完降超責並療従右修捕隊危採織森競拡故館振給屋介読弁根色友苦就迎走販園具左異歴辞将秋因献厳馬愛幅休維富浜父遺彼般未塁貿講邦舞林装諸夏素亡劇河遣航抗冷模雄適婦鉄寄益込顔緊類児余禁印逆王返標換久短油妻暴輪占宣背昭廃植熱宿薬伊江清習険頼僚覚吉盛船倍均億途圧芸許皇臨踏駅署抜壊債便伸留罪停興爆陸玉源儀波創障継筋狙帯延羽努固闘精則葬乱避普散司康測豊洋静善逮婚厚喜齢囲卒迫略承浮惑崩順紀聴脱旅絶級幸岩練押軽倒了庁博城患締等救執層版老令角絡損房募曲撤裏払削密庭徒措仏績築貨志混載昇池陣我勤為血遅抑幕居染温雑招奈季困星傷永択秀著徴誌庫弾償刊像功拠香欠更秘拒刑坂刻底賛塚致抱繰服犯尾描布恐寺鈴盤息宇項喪伴遠養懸戻街巨震願絵希越契掲躍棄欲痛触邸依籍汚縮還枚属笑互複慮郵束仲栄札枠似夕恵板列露沖探逃借緩節需骨射傾届曜遊迷夢巻購揮君燃充雨閉緒跡包駐貢鹿弱却端賃折紹獲郡併草徹飲貴埼衝焦奪雇災浦暮替析預焼簡譲称肉納樹挑章臓律誘紛貸至宗促慎控"
            private val COMMONALITY_MAP = HashMap<Kanji, Short>(COMMONALITY.length)

            /**
             * Maximum commonality value.
             */
            private val MAX_COMMONALITY = (COMMONALITY.length + 2).toShort()

            init {
                var commonality: Short = 2
                for (ch in COMMONALITY.toCharArray()) {
                    COMMONALITY_MAP.put(Kanji(ch), commonality++)
                }
            }

            /**
             * Returns commonality of given japanese character. Katakana and hiragana
             * characters receive commonality of 1, kanji characters get commonality
             * from the F field of the KANJIDIC + 1. Unknown kanji receive commonality
             * of 1002.
             * @param ch the character.
             * @return the commonality.
             */
            private fun getCommonality(ch: Char): Short {
                if (ch.isKana) {
                    return 1
                }
                if (ch.isKanji) {
                    val c = COMMONALITY_MAP[Kanji(ch)]
                    return c ?: MAX_COMMONALITY
                }
                return MAX_COMMONALITY
            }

            internal fun getCommonality(codePoint: Int): Short =
                    if (codePoint > Character.MAX_VALUE.toInt()) MAX_COMMONALITY else getCommonality(codePoint.toChar())
        }
    }

    /**
     * Returns the kanji as a character. Fails if the kanji cannot be represented as Java char.
     * @return the kanji
     */
    fun toCharacter(): Char {
        if (!isChar) {
            throw IllegalStateException(kanji() + " cannot be represented as a Java char")
        }
        return kanji()[0]
    }

    fun toChar() = JpCharacter(kanji())

    class PartTree(val part: JpCharacter, val parent: PartTree?) : Serializable {
        val parts = mutableListOf<PartTree>()

        private val kanjisPreorder: Set<JpCharacter>
            get() {
                val result = LinkedHashSet<JpCharacter>()
                printKanjisPreorder(result)
                return result
            }

        private val partsMaxDepth: Map<JpCharacter, Int>
            get() {
                val result = LinkedHashMap<JpCharacter, Int>()
                getPartsMaxDepth(result, 0)
                return result
            }

        init {
            var parent = parent
            val parts = part.parts.toMutableSet()
            parts.remove(part)
            // avoid cycles, napr. 囗.getParts() obsahuje 口, a 口.getParts() obsahuje 囗
            while (parent != null) {
                parts.remove(parent.part)
                parent = parent.parent
            }
            val sortedParts = ArrayList(parts)
            Collections.sort(sortedParts)
            for (p in sortedParts) {
                this.parts.add(PartTree(p, this))
            }
            // reduce the tree and remove parts which are present in some subtree.
            // That can be achieved by removing
            // all parts with depth 1 (parts), for which there is a part with depth 2 or bigger.
            // Do this one-by-one - if there are children such as 囗[口[]] a 口[囗[]],
            // uncareful algorithm would remove both of them.
            while (true) {
                val map = partsMaxDepth
                var again = false
                for ((key, value) in map) {
                    if (value > 1) {
                        if (removePart(key)) {
                            again = true
                            break
                        }
                    }
                }
                if (!again) {
                    break
                }
            }
        }

        internal fun cramSimilarStuffTogether() {
            if (parts.size < 2) {
                return
            }
            for (part in parts) {
                part.cramSimilarStuffTogether()
            }
            val first = parts[0].kanjisPreorder
            val next = ArrayList<PartTree>()
            val last = ArrayList<PartTree>()
            for (i in 1 until parts.size) {
                val part = parts[i]
                if (first.intersects(part.kanjisPreorder)) {
                    next.add(part)
                } else {
                    last.add(part)
                }
            }
            parts.subList(1, parts.size).clear()
            parts.addAll(next)
            parts.addAll(last)
        }

        fun printKanjisPreorder(): String = kanjisPreorder.joinToString("")

        private fun printKanjisPreorder(sb: MutableSet<JpCharacter>) {
            sb.remove(part) // make sure the kanji is present in sb at most once.
            sb.add(part)
            parts.forEach { part -> part.printKanjisPreorder(sb) }
        }

        private fun removePart(part: JpCharacter): Boolean = parts.removeAll { it.part == part }

        private fun getPartsMaxDepth(map: MutableMap<JpCharacter, Int>, currentDepth: Int) {
            val d = map[part]
            if (d == null || d < currentDepth) {
                map.put(part, currentDepth)
            }
            for (part in parts) {
                part.getPartsMaxDepth(map, currentDepth + 1)
            }
        }

        override fun toString() = "$part[${parts.joinToString()}]"
    }

    companion object : Unboxable<Kanji>, Readable<Kanji> {

        @JvmStatic @Throws(IOException::class)
        override fun readFrom(di: DataInput): Kanji {
            return Kanji(di.readUTF())
        }

        @JvmStatic
        override fun unbox(box: Box): Kanji {
            val codePoint = box.get("codePoint").integer().orNull()
            return if (codePoint != null) {
                Kanji(codePoint)
            } else Kanji(box.get("kanji").string().get())
        }

        /**
         * Computes word commonality using kanji-based commonality.
         * Only japanese characters are
         * expected. A commonality of a string is defined as a sum of
         * [getCommonality] of all its characters.
         *
         * Don't use for JMDict - there it's better to use a big corpus frequency data. See OccurenceBasedCommonality.
         * For name dictionary, river dictionary, car dictionary or others there is no corpus; at least we have this kind of commonality.
         */
        val KANJI_BASED_COMMONALITY: Commonality = KanjiBasedCommonality()

        private val JLPT_LEVEL = HashMap<Char, JLPTLevel>()

        init {
            for (level in JLPTLevel.values()) {
                val jlpt = level.kanjis
                for (j in 0 until jlpt.length) {
                    val kanji = jlpt[j]
                    val prev = JLPT_LEVEL.put(kanji, level)
                    if (prev != null) {
                        throw RuntimeException("Kanji $kanji present in levels $prev and $level")
                    }
                }
            }
        }

        private fun String.singleKanjiGet(): Int {
            require(isSingleKanji) { "Parameter kanji: invalid value $this: not a single kanji" }
            return codePointAt(0)
        }
    }
}
